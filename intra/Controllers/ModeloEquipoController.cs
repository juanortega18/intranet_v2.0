﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Models.TallerPgr;

namespace intra.Controllers
{
    [Authorize(Roles = "TA")]
    public class ModeloEquipoController : Controller
    {
        private TallerPgrDM db = new TallerPgrDM();

        // GET: ModeloEquipo
        public ActionResult Index()
        {
            //incluir los valores de las tablas
            //var modelos = (from modelo in db.ModeloEquipo
            //              .Include(m => m.MarcaEquipo)
            //              .Include(t => t.TipoEquipo)
            //               select modelo).ToList();

            var modelos = db.ModeloEquipo.ToList();

            return View(modelos);
        }

        // GET: ModeloEquipo/Create
        public ActionResult Create()
        {
            ViewBag.Tipos = new SelectList(db.TipoEquipo, "TipoEquipoId", "TipoEquipoDescripcion");
            return View();
        }

        // POST: ModeloEquipo/Create
        [HttpPost]
        public JsonResult Create(string modeloNombre, int tipoid, int marcaid)
        {
            var modelo = new ModeloEquipo();
            modelo.ModeloEquipoDescripcion = modeloNombre.ToUpper();
            modelo.TipoEquipoId = tipoid;
            modelo.MarcaEquipoId = marcaid;

            var resultado = ValidarModelo(modelo);

            if (resultado == "Valido") {

                db.ModeloEquipo.Add(modelo);
                db.SaveChanges();
                return Json("Guardado");
            }
            return Json(resultado);
        }

        // GET: ModeloEquipo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            ModeloEquipo modeloEquipo = db.ModeloEquipo.Find(id);
            if (modeloEquipo == null)
            {
                return HttpNotFound();
            }
            ViewBag.tipos = new SelectList(db.TipoEquipo, "TipoEquipoId", "TipoEquipoDescripcion", modeloEquipo.MarcaEquipoId);
            ViewBag.marcas = new SelectList(db.MarcaEquipo, "MarcaEquipoId", "MarcaEquipoDescripcion", modeloEquipo.MarcaEquipoId);
            return View(modeloEquipo);
        }

        // POST: ModeloEquipo/Edit/5
        [HttpPost]
        public ActionResult Edit(string modeloNombre, int tipoid, int marcaid, int id)
        {
            var modelo = db.ModeloEquipo.Find(id);

            if (modelo == null) { return Json("A ocurrido un error, Trate de actualizar la pagina e intente otra vez."); }

            modelo.ModeloEquipoDescripcion = modeloNombre.ToUpper();
            modelo.MarcaEquipoId = marcaid;
            modelo.TipoEquipoId = tipoid;

            var resultado = ValidarModelo(modelo);

            if (resultado == "Valido") {

                db.SaveChanges();

                return Json("Actualizado");
            }
            return Json(resultado);
        }

        [HttpPost]
        public JsonResult CargarModelos(int tipoid, int marcaid)
        {

            var modelos = db.ModeloEquipo.Where(x => x.TipoEquipoId == tipoid && x.MarcaEquipoId == marcaid).Select(x => new {
                Value = x.ModeloEquipoId,
                Text = x.ModeloEquipoDescripcion
            }).ToList();

            return Json(modelos);
        }

        // GET: ModeloEquipo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModeloEquipo modeloEquipo = db.ModeloEquipo.Find(id);
            if (modeloEquipo == null)
            {
                return HttpNotFound();
            }
            return View(modeloEquipo);
        }

        // POST: ModeloEquipo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ModeloEquipo modeloEquipo = db.ModeloEquipo.Find(id);
            db.ModeloEquipo.Remove(modeloEquipo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private string ValidarModelo(ModeloEquipo modelo)
        {
            if (db.TipoEquipo.Find(modelo.TipoEquipoId) == null) { return "Ingrese un tipo de equipo valido."; }
            if (db.MarcaEquipo.Find(modelo.MarcaEquipoId) == null) { return "Ingrese una marca de equipo valida."; }
            if (modelo.ModeloEquipoDescripcion == null) { return "Ingrese un el nombre del modelo."; }
            if (db.TipoEquipo.Find(modelo.TipoEquipoId).MarcaEquipo.Where(x => x.MarcaEquipoId == modelo.MarcaEquipoId).Count() == 0)
            { return "El tipo y la marca de equipo que selecciono no estan relacionados."; }
            if (db.ModeloEquipo.Where(x => x.ModeloEquipoDescripcion == modelo.ModeloEquipoDescripcion.ToUpper() && x.TipoEquipoId == modelo.TipoEquipoId && x.MarcaEquipoId == modelo.MarcaEquipoId).Count() != 0)
            { return "Este modelo ya a sido ingresado."; }
            return "Valido";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
