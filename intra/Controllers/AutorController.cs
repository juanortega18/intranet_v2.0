﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
    [CustomAuthorize]
    [Authorize(Roles = "RRHHA")]
    public class AutorController : Controller
    {
        private dbIntranet db = new dbIntranet();
        // GET: Autor
        public ActionResult Index()
        {
            return View(db.AutorLibroBiblioteca.Where(x => x.AutorLBId>1).ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AutorLBId,AutorLBNombre")] AutorLibroBiblioteca autor)
        {
            if (ModelState.IsValid)
            {
                autor.AutorLBNombre = autor.AutorLBNombre.ToUpper();
                db.AutorLibroBiblioteca.Add(autor);
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            return View();
        }

        public ActionResult Edit(int? id)
        {
            if (id == null || id == 1)
            {
                return RedirectToAction("Index");
            }
            AutorLibroBiblioteca autor = db.AutorLibroBiblioteca.Find(id);
            if (autor == null)
            {
                return HttpNotFound();
            }
            return View(autor);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AutorLBId,AutorLBNombre")] AutorLibroBiblioteca autor)
        {
            var autoresultado = db.AutorLibroBiblioteca.Find(autor.AutorLBId);
            if (autoresultado != null && autor.AutorLBId != 1)
            {
                if (ModelState.IsValid)
                {
                    autoresultado.AutorLBNombre = autor.AutorLBNombre.ToUpper();
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(autoresultado);
            }
            return HttpNotFound();
        }

        [HttpPost, ActionName("Eliminar")]
        public ActionResult Eliminar(int? id)
        {
            if (id == null || id == 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AutorLibroBiblioteca autor = db.AutorLibroBiblioteca.Find(id);
            if (autor == null)
            {
                return HttpNotFound();
            }
            var autores_libros = db.LibroBiblioteca.Where(x => x.AutorLBId == id);
            if (autores_libros != null) { foreach (LibroBiblioteca libro in autores_libros) { libro.AutorLBId = 1; } }
            db.AutorLibroBiblioteca.Remove(autor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult AgregarAutorModal(AutorLibroBiblioteca autor)
        {
            if(ModelState.IsValid)
            {
                autor.AutorLBNombre = autor.AutorLBNombre.ToUpper();
                db.AutorLibroBiblioteca.Add(autor);
                db.SaveChanges();
                return Json("Se ha agregado el autor correctamente");
            }
            return Json("El autor ingresado no es válido");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}