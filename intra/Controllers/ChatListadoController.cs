﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;


namespace intra.Controllers
{
    public class ChatListadoController : Controller
    {
        private SolicitudModel smodel = new SolicitudModel();


        private dbIntranet obj = new dbIntranet();

        #region MostrarListadoChat
        public void MostrarListadoChat(int CodigoEmpleados)
        {
            Session["CodigoSolicitudes"] = null;

            var Detalle = new List<Vw_Mostrar_Lista_de_Asignacion_Chat>();
            
            Detalle = obj.Sol_Mostrar_Lista_de_Asignacion_Chat.Where(cd => cd.Estado == true && cd.CodigoUsuario == CodigoEmpleados).ToList();

            if (Detalle.Count > 0)
            {
                smodel.Tb_Vw_Mostrar_Lista_de_Asignacion_Chat = Detalle;
            }
            else
            {
                smodel.Tb_Vw_Mostrar_Lista_de_Asignacion_Chat = Detalle;
            }
        }
        #endregion

        [CustomAuthorize]
        // GET: ChatListado
        [HttpGet]
        public ActionResult ListadoChat()
        {
            //Variables Usuario

            int Cdg = 0;

            if (HttpContext.Session["empleadoId"]!=null)
            {
                //Variable de Secciones
                Cdg =  int.Parse(HttpContext.Session["empleadoId"].ToString());

                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Chat");

                MostrarListadoChat(Cdg);
            }
            else
            {
                return RedirectToAction("Index", "Home");

            }
            
            return View(smodel);
        }
    }
}