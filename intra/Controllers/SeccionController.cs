﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Data.Entity;
using intra.Models.GestionHumana;
using intra;
using System.IO;

namespace intra.Controllers
{
    [CustomAuthorize]
    [Authorize(Roles = "RRHHA")]
    public class SeccionController : Controller
    {
        private dbIntranet db = new dbIntranet();
        public ActionResult Index()
        {
            return View(db.SeccionBiblioteca.Where(x => x.SeccionBibliotecaId>1).ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SeccionBiblioteca seccion)
        {
            if (ModelState.IsValid)
            {
                seccion.SeccionBibliotecaDescripcion = seccion.SeccionBibliotecaDescripcion.ToUpper();

                if (seccion.SeccionVista != null)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        seccion.SeccionVista.InputStream.CopyTo(ms);
                        seccion.SeccionBibliotecaImagen = ms.GetBuffer();
                    }
                }

                db.SeccionBiblioteca.Add(seccion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Edit(int? id)
        {
            if (id == null || id == 1)
            {
                return RedirectToAction("Index");
            }
            SeccionBiblioteca seccion = db.SeccionBiblioteca.Find(id);
            if (seccion == null)
            {
                return HttpNotFound();
            }
            return View(seccion);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SeccionBiblioteca seccion)
        {
            var seccionresultado = db.SeccionBiblioteca.Find(seccion.SeccionBibliotecaId);
            if (seccionresultado != null && seccion.SeccionBibliotecaId != 1)
            {
                if (ModelState.IsValid)
                {
                    if (seccion.SeccionVista != null)
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            seccion.SeccionVista.InputStream.CopyTo(ms);
                            seccionresultado.SeccionBibliotecaImagen = ms.GetBuffer();
                        }
                    }
                    seccionresultado.SeccionBibliotecaDescripcion = seccion.SeccionBibliotecaDescripcion.ToUpper();
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(seccion);
            }
            return HttpNotFound();
        }

        [HttpPost, ActionName("Eliminar")]
        public ActionResult Eliminar(int? id)
        {
            if (id == null || id == 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeccionBiblioteca seccion = db.SeccionBiblioteca.Find(id);
            if (seccion == null)
            {
                return HttpNotFound();
            }
            var seccion_libros = db.LibroBiblioteca.Where(x => x.SeccionBibliotecaId == id);
            if (seccion_libros != null) { foreach (LibroBiblioteca libro in seccion_libros) { libro.SeccionBibliotecaId = 1; } }
            db.SeccionBiblioteca.Remove(seccion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public FileResult DesgarcarImagen(int id)
        {
            var imagen = db.SeccionBiblioteca.Find(id);
            return File(imagen.SeccionBibliotecaImagen, "image/jpg", imagen.SeccionBibliotecaDescripcion + ".jpg");
            //return File(@"~\\Content\\images\\img_noDisponible.jpg", "image/jpg");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}