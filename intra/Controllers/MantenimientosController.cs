﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.Empleados;

namespace intra.Controllers
{
    public class MantenimientosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: Mantenimientos
        public ActionResult ListaAfp()
        {
            return View(db.AFP.ToList());
        }

        // GET: Mantenimientos
        public ActionResult ListaArs()
        {
            return View(db.ARS.ToList());
        }
        // GET: Mantenimientos
        public ActionResult ListaProfesiones()
        {
            return View(db.Profesiones.ToList());
        }
        // GET: Mantenimientos
        public ActionResult ListaUniversidades()
        {
            return View(db.Instituciones.ToList());
        }

        // GET: Mantenimientos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFP aFP = db.AFP.Find(id);
            if (aFP == null)
            {
                return HttpNotFound();
            }
            return View(aFP);
        }

        // GET: Mantenimientos/Create
        public ActionResult CrearAFP()
        {
            return View();
        }

        // POST: Mantenimientos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearAFP([Bind(Include = "AfpId,Descripcion,Estado,FhCreacion")] AFP aFP)
        {
            if (ModelState.IsValid)
            {
                aFP.FhCreacion = DateTime.Now;
                aFP.Estado = true;
                db.AFP.Add(aFP);
                db.SaveChanges();
                return RedirectToAction("ListaAfp", "Mantenimientos");
            }

            return View(aFP);
        }

        // GET: Mantenimientos/Create
        public ActionResult CrearARS()
        {
            return View();
        }

        // POST: Mantenimientos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearARS([Bind(Include = "ArsId,Descripcion,Estado,FhCreacion")] ARS ars)
        {
            if (ModelState.IsValid)
            {
                ars.Estado = true;
                ars.FhCreacion = DateTime.Now;
                db.ARS.Add(ars);
                db.SaveChanges();
                return RedirectToAction("ListaArs", "Mantenimientos");
            }

            return View(ars);
        }


        // GET: Mantenimientos/Edit/5
        public ActionResult EditarAfp(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFP aFP = db.AFP.Find(id);
            if (aFP == null)
            {
                return HttpNotFound();
            }
            return View(aFP);
        }

        // POST: Mantenimientos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarAfp([Bind(Include = "AfpId,Descripcion,Estado,FhCreacion")] AFP afp)
        {
            if (ModelState.IsValid)
            {
                db.Entry(afp).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListaAfp", "Mantenimientos");
            }
            return View(afp);
        }
        // GET: Mantenimientos/Edit/5
        public ActionResult EditarArs(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
             ARS ars = db.ARS.Find(id);
            if (ars == null)
            {
                return HttpNotFound();
            }
            return View(ars);
        }

        // POST: Mantenimientos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarArs([Bind(Include = "ArsId,Descripcion,Estado,FhCreacion")] ARS ars)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ars).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListaArs", "Mantenimientos");
            }
            return View(ars);
        }

        // GET: Mantenimientos/Create
        public ActionResult CrearProfesiones()
        {
            return View();
        }

        // POST: Mantenimientos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearProfesiones([Bind(Include = "ProfesionId,Descripcion,Estado,FhCreacion")] Profesiones Profesiones)
        {
            if (ModelState.IsValid)
            {
                Profesiones.Estado = true;
                Profesiones.FhCreacion = DateTime.Now;
                db.Profesiones.Add(Profesiones);
                db.SaveChanges();
                return RedirectToAction("ListaProfesiones", "Mantenimientos");
            }

            return View(Profesiones);
        }


        // GET: Mantenimientos/Edit/5
        public ActionResult EditarProfesiones(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Profesiones profesiones = db.Profesiones.Find(id);
            if (profesiones == null)
            {
                return HttpNotFound();
            }
            return View(profesiones);
        }

        // POST: Mantenimientos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarProfesiones([Bind(Include = "ProfesionId,Descripcion,Estado,FhCreacion")] Profesiones Profesiones)
        {
            if (ModelState.IsValid)
            {
                Profesiones.FhCreacion = DateTime.Now;
                Profesiones.Estado = true;
                db.Entry(Profesiones).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListaProfesiones", "Mantenimientos");
            }
            return View(Profesiones);
        }



        // GET: Mantenimientos/Create
        public ActionResult CrearUniversidades()
        {
            return View();
        }

        // POST: Mantenimientos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearUniversidades([Bind(Include = "InstitucionId,Siglas,Descripcion,CentroId,Estado,FhCreacion")] Institucion Instituciones)
        {
            if (ModelState.IsValid)
            {
          
                Instituciones.Estado = 1;
                Instituciones.FhCreacion = DateTime.Now;
                db.Instituciones.Add(Instituciones);
                db.SaveChanges();
                return RedirectToAction("ListaUniversidades", "Mantenimientos");
            }

            return View(Instituciones);
        }


        // GET: Mantenimientos/Edit/5
        public ActionResult EditarUniversidades(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Institucion Instituciones = db.Instituciones.Find(id);
            if (Instituciones == null)
            {
                return HttpNotFound();
            }
            return View(Instituciones);
        }

        // POST: Mantenimientos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarUniversidades([Bind(Include = "UniversidadId,Siglas,Descripcion,Estado,FhCreacion")] Institucion Instituciones)
        {
            if (ModelState.IsValid)
            {
                Instituciones.FhCreacion = DateTime.Now;
                Instituciones.Estado = 1;
                //Instituciones.CentroId = 
                db.Entry(Instituciones).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListaUniversidades", "Mantenimientos");
            }
            return View(Instituciones);
        }





        // GET: Mantenimientos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFP aFP = db.AFP.Find(id);
            if (aFP == null)
            {
                return HttpNotFound();
            }
            return View(aFP);
        }

        // POST: Mantenimientos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AFP aFP = db.AFP.Find(id);
            db.AFP.Remove(aFP);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
