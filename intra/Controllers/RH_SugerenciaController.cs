﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models;
using intra.Models.GestionHumana;
using intra.Models.GestionHumana.Sugerencias;
using Rotativa;
using Rotativa.Options;

namespace intra.Controllers
{
    public class RH_SugerenciaController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [Authorize(Roles = "SU")]
        public ActionResult Index()
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }

            DateTime un_anio_atras = DateTime.Now.AddYears(-1);

            //var rH_Sugerencia = db.RH_Sugerencia.OrderByDescending(x => x.SugerenciaID).ToList();
            var rH_Sugerencia = db.RH_Sugerencia.Where(x => x.Estado && x.FechaCreacion > un_anio_atras).OrderByDescending(x => x.SugerenciaID).ToList();

            ViewBag.MotivoID = new SelectList(db.RH_SugerenciaMotivo, "MotivoID", "Descripcion");
            ViewBag.RegionID = new SelectList(db.Sol_DependenciasRegionales, "DependenciaId", "DependenciaNombre");

            return View(rH_Sugerencia);
        }

        [Authorize(Roles = "SU")]
        public ActionResult Supervisor()
        {
            var supervisor = db.RH_SugerenciaSupervisor.FirstOrDefault(x => x.Estado == true);

            sp_users_ldapID usuario = Code.Utilities2.obtenerSp_UserPorCodigo(supervisor.EmpleadoID.ToString()); 

            ViewBag.correo = usuario.mail;
            ViewBag.nombre = usuario.displayName;

            ViewBag.historial = GetHistorialSugerenciaSupervisorLog();

            return View(supervisor);
        }

        public ActionResult Sugerencia_Partial() {

            var rH_Sugerencia = db.RH_Sugerencia.Where(x => x.Estado && x.SugerenciaEstadoID == 1).OrderByDescending(x => x.SugerenciaID).ToList();

            return View(rH_Sugerencia);
        }

        public JsonResult CargarSugerencias(int MotivoID, int RegionID, int SugerenciaEstadoID, string fechainicio_string, string fechafinal_string) {

            //string codigo = HttpContext.Session["empleadoId"].ToString();

            //var resultado = Opcion != "asignados";

            #region Convertir los valores a tipo fecha

            bool fecha_filtro_valido = true;

            DateTime Fecha_Inicio, Fecha_Final; Fecha_Inicio = Fecha_Final = DateTime.Now;

            if (!string.IsNullOrEmpty(fechainicio_string) && !string.IsNullOrEmpty(fechafinal_string))
            {
                if (Code.Utilities2.convertirStringAfecha(fechainicio_string, out Fecha_Inicio) &&
                    Code.Utilities2.convertirStringAfecha(fechafinal_string, out Fecha_Final)) {

                    if (Fecha_Inicio > Fecha_Final) return Json("fecha final invalida");
                    else fecha_filtro_valido = false;

                }
                else
                    return Json("fecha invalidas");
                Fecha_Final = Fecha_Final.AddDays(1).AddSeconds(-1);
            }

            #endregion

            var sugerencias = db.RH_Sugerencia.Where(x => x.Estado == true &&

                   (x.MotivoID == MotivoID || MotivoID == 0)
                && (x.RegionID == RegionID || RegionID == 0)
                && (x.SugerenciaEstadoID == SugerenciaEstadoID || SugerenciaEstadoID == 0)
                && (x.FechaCreacion >= Fecha_Inicio && x.FechaCreacion <= Fecha_Final || (fecha_filtro_valido))
            )
            .Select(x => new
            {
                x.SugerenciaID,
                x.MotivoID,
                x.RegionID,
                x.SugerenciaEstadoID,
                x.FechaCreacion,
                Motivo_Descripcion = x.SugerenciaMotivo.Descripcion,
                Region_Descripcion = x.Dependencia.DependenciaNombre,
                Estado_Descripcion = x.SugerenciaEstado.Descripcion,
                x.Descripcion
            }).OrderByDescending(x => x.SugerenciaID).ToList();

            return Json(sugerencias);
        }

        [AllowAnonymous]
        public ActionResult EnviarSugerenciaModal() {

            //ViewBag.SugerenciaEstadoID = new SelectList(db.RH_SugerenciaEstado, "SugerenciaEstadoID", "Descripcion");
            ViewBag.MotivoID = new SelectList(db.RH_SugerenciaMotivo, "MotivoID", "Descripcion");
            ViewBag.RegionID = new SelectList(db.Sol_DependenciasRegionales, "DependenciaId", "DependenciaNombre");

            return View();
        }

        [AllowAnonymous]
        public ActionResult Detalle(int? id)
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RH_Sugerencia rH_Sugerencia = db.RH_Sugerencia.Find(id);
            if (rH_Sugerencia == null)
            {
                return HttpNotFound();
            }
            return View(rH_Sugerencia);
        }

        public ActionResult Create()
        {
            ViewBag.SugerenciaEstadoID = new SelectList(db.RH_SugerenciaEstado, "SugerenciaEstadoID", "Descripcion");
            ViewBag.MotivoID = new SelectList(db.RH_SugerenciaMotivo, "MotivoID", "Descripcion");
            return View();
        }

        [AllowAnonymous]
        public JsonResult Agregar(RH_Sugerencia rH_Sugerencia)
        {
            try
            {
                List<string> Errores = new List<string>();

                if (rH_Sugerencia.MotivoID == 0) Errores.Add("Ingrese el motivo");
                if (rH_Sugerencia.RegionID == 0) Errores.Add("Ingrese la región");
                if (string.IsNullOrWhiteSpace(rH_Sugerencia.Descripcion)) Errores.Add("Escriba la descripcion de la sugerencia");

                if (Errores.Count > 0) return Json(Errores);

                string motivo = db.RH_SugerenciaMotivo.Find(rH_Sugerencia.MotivoID).Descripcion;
                string region = db.Sol_DependenciasRegionales.Find(rH_Sugerencia.RegionID).DependenciaNombre;

                rH_Sugerencia.SugerenciaEstadoID = 1;
                rH_Sugerencia.FechaCreacion = DateTime.Now;
                rH_Sugerencia.Estado = true;

                db.RH_Sugerencia.Add(rH_Sugerencia);
                db.SaveChanges();

                string CorreoSupervisor =  Get_SugerenciasSupervisor().mail;

                try
                {
                    EnviarCorreoServicioRepatriado(new string[] { CorreoSupervisor }, "USTED HA RECIBIDO UNA SUGERENCIA!",
                        Descripcion("INFORMACIÓN DE LA SUGERENCIA RECIBIDA", motivo, region, rH_Sugerencia.Descripcion, rH_Sugerencia.SugerenciaID));
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

                return Json("guardado");

            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json("error");
            }
        }

        public JsonResult GuardarSupervisorSolicitud(int codigoEmpleado) {

            try
            {
                var empleado = db.VISTA_EMPLEADOS.FirstOrDefault(x => x.empleadoid == codigoEmpleado.ToString());

                if (empleado == null) return Json("no encontrado");

                var registroPrevio = db.RH_SugerenciaSupervisor.FirstOrDefault(x => x.EmpleadoID.ToString() == empleado.empleadoid);

                var ultimoRegistro = db.RH_SugerenciaSupervisor.FirstOrDefault(x => x.Estado);

                if (registroPrevio != null && registroPrevio.Estado) return Json("registrado");

                if (registroPrevio != null)
                {

                    ultimoRegistro.Estado = false;

                    registroPrevio.Estado = true;

                    db.RH_SugerenciaSupervisor_log.Add(new RH_SugerenciaSupervisor_log() { EmpleadoID = codigoEmpleado, SugerenciaSupervisorID = registroPrevio.SugerenciaSupervisorID, FechaCreacion = DateTime.Now });
                }
                else
                {
                    ultimoRegistro.Estado = false;

                    var registroNuevo = new RH_SugerenciaSupervisor() { EmpleadoID = codigoEmpleado, Estado = true, FechaCreacion = DateTime.Now };

                    db.RH_SugerenciaSupervisor.Add(registroNuevo);

                    db.SaveChanges();

                    db.RH_SugerenciaSupervisor_log.Add(new RH_SugerenciaSupervisor_log() { EmpleadoID = codigoEmpleado, SugerenciaSupervisorID = registroNuevo.SugerenciaSupervisorID, FechaCreacion = DateTime.Now });
                }

                db.SaveChanges();

                return Json("guardado");
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json("error");
            }
        }


        private sp_users_ldapID Get_SugerenciasSupervisor()
        {
            var SugerenciaSupervisor = db.RH_SugerenciaSupervisor.FirstOrDefault(x=>x.Estado);

            return Code.Utilities2.obtenerSp_UserPorCodigo(SugerenciaSupervisor.EmpleadoID.ToString());
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RH_Sugerencia rH_Sugerencia = db.RH_Sugerencia.Find(id);
            if (rH_Sugerencia == null)
            {
                return HttpNotFound();
            }
            ViewBag.SugerenciaEstadoID = new SelectList(db.RH_SugerenciaEstado, "SugerenciaEstadoID", "Descripcion", rH_Sugerencia.SugerenciaEstadoID);
            ViewBag.MotivoID = new SelectList(db.RH_SugerenciaMotivo, "MotivoID", "Descripcion", rH_Sugerencia.MotivoID);
            return View(rH_Sugerencia);
        }

        public JsonResult EvaluarSugerencia(int SugerenciaID, int Decision) {

            try
            {
                var sugerencia = db.RH_Sugerencia.Find(SugerenciaID);

                sugerencia.SugerenciaEstadoID = Decision;

                int codigo = int.Parse(HttpContext.Session["empleadoId"].ToString());

                db.RH_SugerenciaEvaluacion_log.Add(new RH_SugerenciaEvaluacion_log() {
                    SugerenciaID = sugerencia.SugerenciaID, EvaluadoPor = codigo,
                    FechaCreacion = DateTime.Now, SugerenciaEstadoID = Decision });

                db.SaveChanges();

                return Json("guardado");
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json("error");
            }
        }

        public ActionResult Actualizar(RH_Sugerencia rH_Sugerencia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rH_Sugerencia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SugerenciaEstadoID = new SelectList(db.RH_SugerenciaEstado, "SugerenciaEstadoID", "Descripcion", rH_Sugerencia.SugerenciaEstadoID);
            ViewBag.MotivoID = new SelectList(db.RH_SugerenciaMotivo, "MotivoID", "Descripcion", rH_Sugerencia.MotivoID);
            return View(rH_Sugerencia);
        }

        [Authorize(Roles = "SU")]
        public ActionResult ReporteSugerenciasPDF(int MotivoID, int RegionID, string fechainicio_string, string fechafinal_string, string tipo_reporte) {

            #region Convertir los valores a tipo fecha

            bool fecha_filtro_valido = true;

            DateTime Fecha_Inicio, Fecha_Final; Fecha_Inicio = Fecha_Final = DateTime.Now;

            if (!string.IsNullOrEmpty(fechainicio_string) && !string.IsNullOrEmpty(fechafinal_string))
            {
                if (Code.Utilities2.convertirStringAfecha(fechainicio_string, out Fecha_Inicio) &&
                    Code.Utilities2.convertirStringAfecha(fechafinal_string, out Fecha_Final))
                {
                    fecha_filtro_valido = false;
                }
                else
                Fecha_Final = Fecha_Final.AddDays(1).AddSeconds(-1);
            }

            #endregion

            if (tipo_reporte == "0") {

                var sugerencias_estadisticas = db.RH_Sugerencia.ToArray().Join(db.Sol_DependenciasRegionales, RH_Sugerencia => RH_Sugerencia.RegionID,

                Sol_DependenciasRegionales => Sol_DependenciasRegionales.DependenciaId, (RH_Sugerencia, Sol_DependenciasRegionales) =>
                new { full_name = Sol_DependenciasRegionales.DependenciaNombre, RH_Sugerencia = RH_Sugerencia }

                ).Where(x => x.RH_Sugerencia.Estado == true &&

                   (x.RH_Sugerencia.MotivoID == MotivoID || MotivoID == 0)
                && (x.RH_Sugerencia.RegionID == RegionID || RegionID == 0)
                && (x.RH_Sugerencia.FechaCreacion >= Fecha_Inicio && x.RH_Sugerencia.FechaCreacion <= Fecha_Final || (fecha_filtro_valido))
                ).GroupBy(x => new { x.full_name, x.RH_Sugerencia.MotivoID }).Select(x => new { full_name = x.Key.full_name, quantity = x.Count(), state_id = x.Key.MotivoID }).ToList();

                var first_chart_data = new List<Code.ChartData>();
                var names = new List<string>();

                for (int i = 0; i < sugerencias_estadisticas.Count; i++)
                {
                    string item_name = sugerencias_estadisticas[i].full_name;

                    if (i > 0)
                    {
                        int repeated = 0;

                        for (int j = i - 1; j >= 0; j--)
                        {
                            if (sugerencias_estadisticas[j].full_name == sugerencias_estadisticas[i].full_name)
                            {
                                repeated++;
                            }
                        }

                        if (repeated == 0)
                        {
                            names.Add(sugerencias_estadisticas[i].full_name);
                        }
                    }
                    else if (i == 0)
                    {
                        names.Add(sugerencias_estadisticas[i].full_name);
                    }
                }

                foreach (var item in names)
                {
                    var rows = sugerencias_estadisticas.Where(x => x.full_name == item);

                    var first_chart_item = new Code.ChartData();

                    foreach (var item2 in rows)
                    {
                        if (item2.state_id == 1)
                        {
                            first_chart_item.pending = item2.quantity;
                        }
                        else if (item2.state_id == 2)
                        {
                            first_chart_item.closed = item2.quantity;
                        }
                        else if (item2.state_id == 3)
                        {
                            first_chart_item.canceled = item2.quantity;
                        }
                    }

                    first_chart_item.name = item;
                    first_chart_item.total = first_chart_item.pending + first_chart_item.closed + first_chart_item.canceled;

                    first_chart_data.Add(first_chart_item);
                }

                ViewBag.lista = first_chart_data;
            }
            else
            {
                var sugerencias = db.RH_Sugerencia.Where(x => x.Estado == true &&
                   (x.MotivoID == MotivoID || MotivoID == 0)
                && (x.RegionID == RegionID || RegionID == 0)
                && (x.FechaCreacion >= Fecha_Inicio && x.FechaCreacion <= Fecha_Final || (fecha_filtro_valido))).OrderByDescending(x => x.SugerenciaID).ToArray();

                ViewBag.lista = sugerencias;
            }

            ViewBag.begin_date = string.IsNullOrEmpty(fechainicio_string) ? "" : fechainicio_string; 
            ViewBag.end_date = string.IsNullOrEmpty(fechafinal_string) ? "" : fechafinal_string;

            ViewBag.motivo = MotivoID == 0 ? "0" : db.RH_SugerenciaMotivo.Find(MotivoID).Descripcion;
            ViewBag.RegionID = RegionID;

            ViewBag.tipo = tipo_reporte;

            return new ViewAsPdf("ReporteSugerenciasPDF")
            {
                PageOrientation = Orientation.Portrait,
                PageSize = Rotativa.Options.Size.Letter,
                PageMargins = { Left = 0, Right = 0, Bottom = 12, Top = 10 }
            };
        }

        [Authorize(Roles = "SU")]
        public ActionResult ReporteSugerenciasPDF2(int MotivoID, int RegionID, string fechainicio_string, string fechafinal_string, string tipo_reporte)
        {

            #region Convertir los valores a tipo fecha

            bool fecha_filtro_valido = true;

            DateTime Fecha_Inicio, Fecha_Final; Fecha_Inicio = Fecha_Final = DateTime.Now;

            if (!string.IsNullOrEmpty(fechainicio_string) && !string.IsNullOrEmpty(fechafinal_string))
            {
                if (Code.Utilities2.convertirStringAfecha(fechainicio_string, out Fecha_Inicio) &&
                    Code.Utilities2.convertirStringAfecha(fechafinal_string, out Fecha_Final))
                {
                    fecha_filtro_valido = false;
                }
                else
                    Fecha_Final = Fecha_Final.AddDays(1).AddSeconds(-1);
            }

            #endregion

            if (tipo_reporte == "0")
            {

                var sugerencias_estadisticas = db.RH_Sugerencia.ToArray().Join(db.Sol_DependenciasRegionales, RH_Sugerencia => RH_Sugerencia.RegionID,

                Sol_DependenciasRegionales => Sol_DependenciasRegionales.DependenciaId, (RH_Sugerencia, Sol_DependenciasRegionales) =>
                new { full_name = Sol_DependenciasRegionales.DependenciaNombre, RH_Sugerencia = RH_Sugerencia }

                ).Where(x => x.RH_Sugerencia.Estado == true &&

                   (x.RH_Sugerencia.MotivoID == MotivoID || MotivoID == 0)
                && (x.RH_Sugerencia.RegionID == RegionID || RegionID == 0)
                && (x.RH_Sugerencia.FechaCreacion >= Fecha_Inicio && x.RH_Sugerencia.FechaCreacion <= Fecha_Final || (fecha_filtro_valido))
                ).GroupBy(x => new { x.full_name, x.RH_Sugerencia.MotivoID }).Select(x => new { full_name = x.Key.full_name, quantity = x.Count(), state_id = x.Key.MotivoID }).ToList();

                var first_chart_data = new List<Code.ChartData>();
                var names = new List<string>();

                for (int i = 0; i < sugerencias_estadisticas.Count; i++)
                {
                    string item_name = sugerencias_estadisticas[i].full_name;

                    if (i > 0)
                    {
                        int repeated = 0;

                        for (int j = i - 1; j >= 0; j--)
                        {
                            if (sugerencias_estadisticas[j].full_name == sugerencias_estadisticas[i].full_name)
                            {
                                repeated++;
                            }
                        }

                        if (repeated == 0)
                        {
                            names.Add(sugerencias_estadisticas[i].full_name);
                        }
                    }
                    else if (i == 0)
                    {
                        names.Add(sugerencias_estadisticas[i].full_name);
                    }
                }

                foreach (var item in names)
                {
                    var rows = sugerencias_estadisticas.Where(x => x.full_name == item);

                    var first_chart_item = new Code.ChartData();

                    foreach (var item2 in rows)
                    {
                        if (item2.state_id == 1)
                        {
                            first_chart_item.pending = item2.quantity;
                        }
                        else if (item2.state_id == 2)
                        {
                            first_chart_item.closed = item2.quantity;
                        }
                        else if (item2.state_id == 3)
                        {
                            first_chart_item.canceled = item2.quantity;
                        }
                    }

                    first_chart_item.name = item;
                    first_chart_item.total = first_chart_item.pending + first_chart_item.closed + first_chart_item.canceled;

                    first_chart_data.Add(first_chart_item);
                }

                ViewBag.lista = first_chart_data;
            }
            else
            {
                var sugerencias = db.RH_Sugerencia.Where(x => x.Estado == true &&
                   (x.MotivoID == MotivoID || MotivoID == 0)
                && (x.RegionID == RegionID || RegionID == 0)
                && (x.FechaCreacion >= Fecha_Inicio && x.FechaCreacion <= Fecha_Final || (fecha_filtro_valido))).OrderByDescending(x => x.SugerenciaID).ToArray();

                ViewBag.lista = sugerencias;
            }

            ViewBag.begin_date = string.IsNullOrEmpty(fechainicio_string) ? "" : fechainicio_string;
            ViewBag.end_date = string.IsNullOrEmpty(fechafinal_string) ? "" : fechafinal_string;

            ViewBag.motivo = MotivoID == 0 ? "0" : db.RH_SugerenciaMotivo.Find(MotivoID).Descripcion;
            ViewBag.RegionID = RegionID;

            ViewBag.tipo = tipo_reporte;

            return View("ReporteSugerenciasPDF");
        }

        [Authorize(Roles = "SU")]
        public ActionResult ReporteSugerenciasExcel(int MotivoID, int RegionID, string fechainicio_string, string fechafinal_string, string tipo_reporte)
        {
            try
            {
                #region Convertir los valores a tipo fecha

                bool fecha_filtro_valido = true;

                DateTime Fecha_Inicio, Fecha_Final; Fecha_Inicio = Fecha_Final = DateTime.Now;

                if (!string.IsNullOrEmpty(fechainicio_string) && !string.IsNullOrEmpty(fechafinal_string))
                {
                    if (Code.Utilities2.convertirStringAfecha(fechainicio_string, out Fecha_Inicio) &&
                        Code.Utilities2.convertirStringAfecha(fechafinal_string, out Fecha_Final))
                    {
                        fecha_filtro_valido = false;
                    }
                    else
                        Fecha_Final = Fecha_Final.AddDays(1).AddSeconds(-1);
                }

                #endregion

                var grid = new System.Web.UI.WebControls.GridView();

                string TablaExcel = "<head><meta charset=\"utf-8\"></head><style>table{font-size:15px;}</style>";
                string Tabla = "";
                StringWriter sw = new StringWriter();

                if (tipo_reporte == "0")
                {
                    var sugerencias_estadisticas = db.RH_Sugerencia.ToArray().Join(db.Sol_DependenciasRegionales, RH_Sugerencia => RH_Sugerencia.RegionID,

                        Sol_DependenciasRegionales => Sol_DependenciasRegionales.DependenciaId, (RH_Sugerencia, Sol_DependenciasRegionales) =>
                        new { full_name = Sol_DependenciasRegionales.DependenciaNombre, RH_Sugerencia = RH_Sugerencia }

                        ).Where(x => x.RH_Sugerencia.Estado == true &&

                           (x.RH_Sugerencia.MotivoID == MotivoID || MotivoID == 0)
                        && (x.RH_Sugerencia.RegionID == RegionID || RegionID == 0)
                        && (x.RH_Sugerencia.FechaCreacion >= Fecha_Inicio && x.RH_Sugerencia.FechaCreacion <= Fecha_Final || (fecha_filtro_valido))
                    ).GroupBy(x => new { x.full_name, x.RH_Sugerencia.MotivoID }).Select(x => new { full_name = x.Key.full_name, quantity = x.Count(), state_id = x.Key.MotivoID }).ToList();

                    var first_chart_data = new List<RH_SugerenciaReporteVM>();
                    var names = new List<string>();

                    for (int i = 0; i < sugerencias_estadisticas.Count; i++)
                    {
                        string item_name = sugerencias_estadisticas[i].full_name;

                        if (i > 0)
                        {
                            int repeated = 0;

                            for (int j = i - 1; j >= 0; j--)
                            {
                                if (sugerencias_estadisticas[j].full_name == sugerencias_estadisticas[i].full_name)
                                {
                                    repeated++;
                                }
                            }

                            if (repeated == 0)
                            {
                                names.Add(sugerencias_estadisticas[i].full_name);
                            }
                        }
                        else if (i == 0)
                        {
                            names.Add(sugerencias_estadisticas[i].full_name);
                        }
                    }

                    int total = 0;

                    foreach (var item in names)
                    {
                        var rows = sugerencias_estadisticas.Where(x => x.full_name == item);

                        var first_chart_item = new RH_SugerenciaReporteVM();

                        foreach (var item2 in rows)
                        {
                            if (item2.state_id == 1)
                            {
                                first_chart_item.SUGERENCIAS = item2.quantity;
                            }
                            else if (item2.state_id == 2)
                            {
                                first_chart_item.QUEJAS = item2.quantity;
                            }
                            else if (item2.state_id == 3)
                            {
                                first_chart_item.MEJORAS = item2.quantity;
                            }
                        }

                        first_chart_item.DEPENDENCIAS = item;
                        first_chart_item.TOTAL_EN_DEPENDENCIAS = first_chart_item.SUGERENCIAS + first_chart_item.QUEJAS + first_chart_item.MEJORAS;

                        total = total + first_chart_item.TOTAL_EN_DEPENDENCIAS;

                        first_chart_data.Add(first_chart_item);
                    }

                    if (MotivoID == 0) { grid.DataSource = first_chart_data; }
                    else {
                        grid.DataSource = first_chart_data.Select(x => new  { DEPENDENCIAS = x.DEPENDENCIAS , motivo_nombre = x.TOTAL_EN_DEPENDENCIAS });
                    }

                    grid.DataBind();

                    System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);

                    grid.RenderControl(htw);

                    Tabla = (sw.ToString().Replace("<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t",
                                        "<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t<th colspan=\" " + (MotivoID == 0 ? "5" : "2") + "\" >" +
                                        " REPORTE DE SUGERENCIAS </ th></tr>") + "<br>");

                    if (MotivoID != 0) {

                        string motivo = db.RH_SugerenciaMotivo.Find(MotivoID).Descripcion.ToUpper();

                        Tabla = Tabla.Replace("motivo_nombre</th>", db.RH_SugerenciaMotivo.Find(MotivoID).Descripcion.ToUpper()+ "</th>");
                    }

                    if (first_chart_data.Count > 1) {

                        Tabla = Tabla.Replace("\r\n\t</table>\r\n",
                                        "<tr style=\"font-weight:bold;font-size:20px;\">\r\n\t\t\t<td colspan=\"" + (MotivoID == 0 ? "4" : "1") + "\" style=\"border-right:none\">TOTAL</td>" +
                                        "<td style=\"border-left: none;\">" + total + "</td>\r\n\t\t</tr>\r\n\t</table>\r\n");

                    }

                }
                else {

                    var sugerencias = db.RH_Sugerencia.Where(x => x.Estado == true &&
                       (x.MotivoID == MotivoID || MotivoID == 0)
                    && (x.RegionID == RegionID || RegionID == 0)
                    && (x.FechaCreacion >= Fecha_Inicio && x.FechaCreacion <= Fecha_Final || (fecha_filtro_valido))).OrderByDescending(x => x.SugerenciaID).ToArray();

                    grid.DataSource = sugerencias.Select(x=>new {

                        REGIÓN = x.Dependencia.DependenciaNombre,
                        MOTIVO = x.SugerenciaMotivo.Descripcion,
                        FECHA_RECIBIDA = x.FechaCreacion,
                        DESCRIPCIÓN_SUGERENCIA = x.Descripcion

                    });

                    grid.DataBind();

                    System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);

                    grid.RenderControl(htw);

                    Tabla = sw.ToString();

                    Tabla = (sw.ToString().Replace("<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t",
                    "<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t<th colspan=\"4\" >" +
                    " REPORTE DE SUGERENCIAS </ th></tr>") + "<br>");

                }

                TablaExcel = TablaExcel + Tabla;
                
                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachement; filename=Reporte de sugerencias.xls");
                Response.ContentType = "application/excel";

                Response.Output.Write(TablaExcel);
                Response.Flush();
                Response.End();

                return Index();
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "error";
                ex.ToString();
            }
            return RedirectToAction("Index");
        }

        private void EnviarCorreoServicioRepatriado(string[] Correos, string Sujeto, string Descripcion)
        {
            Correo correo = new Correo();

            for (int i = 0; i < Correos.Length; i++)
            {
                Correos[i] = (Correos[i] != "") ? Correos[i] : "";
            }

            //string PrimerCorreo = (Responsable != "") ? Responsable + "@PGR.GOB.DO" : "";

            string Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            string LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            string Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            correo.EnviarCorreoServicioRepatriado_(Correos, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
        }

        private string Descripcion(string Titulo, string motivo, string region, string sugerencia_descripcion, int ID)
        {
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/Sugerencia-nuevaSugerencia.html");

            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", Titulo);
            Archivo = Archivo.Replace("$motivo", motivo);
            Archivo = Archivo.Replace("$region", region);
            Archivo = Archivo.Replace("$sugerencia_descripcion", sugerencia_descripcion);
            Archivo = Archivo.Replace("$pagina", $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RH_Sugerencia/Detalle/" + ID);

            return Archivo;
        }

        private dynamic GetHistorialSugerenciaSupervisorLog()
        {
            var logs = db.RH_SugerenciaSupervisor_log.OrderByDescending(x=>x.EmpleadoID).ToList();

            var empeladoIds = logs.Select(x => x.EmpleadoID.ToString()).ToList();

            var empleados = db.VISTA_EMPLEADOS.Where(x => empeladoIds.Contains(x.empleadoid)).ToList();

            foreach (var log in logs)
            {
                log.VISTA_EMPLEADO = empleados.FirstOrDefault(x=>x.empleadoid == log.EmpleadoID.ToString());
            }

            return logs;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
