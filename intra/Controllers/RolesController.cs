﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.RolesUsuarios;

namespace intra.Controllers
{
    public class RolesController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: Roles
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            //
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Mantenimiento de Roles");
            return View(db.Roles.ToList());
        }

        // GET: Roles/Details/5
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roles roles = db.Roles.Find(id);
            if (roles == null)
            {
                return HttpNotFound();
            }
            return View(roles);
        }

        // GET: Roles/Create
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Create()
        {
            ViewBag.Grupos = new SelectList(db.Grupos.ToList(), "GrupoId", "GrupoNombre");
            return View();
        }

        // POST: Roles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Create([Bind(Include = "RolId,RolNombre,RolDescripcion,GrupoId")] Roles roles)
        {
            if (ModelState.IsValid)
            {
                db.Roles.Add(roles);
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo el Rol llamado ("+roles.RolNombre+")");
                return RedirectToAction("Index");
            }

            return View(roles);
        }

        // GET: Roles/Edit/5
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roles roles = db.Roles.Find(id);
            ViewBag.Grupos = new SelectList(db.Grupos.ToList(), "GrupoId", "GrupoNombre");
            if (roles == null)
            {
                return HttpNotFound();
            }
            return View(roles);
        }

        // POST: Roles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Edit([Bind(Include = "RolId,RolNombre,RolDescripcion,GrupoId")] Roles roles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roles).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito el Rol id (" + roles.RolId + ")");
                return RedirectToAction("Index");
            }
            return View(roles);
        }

        // GET: Roles/Delete/5
        [CustomAuthorize]
        [Authorize(Roles = "RUA")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roles roles = db.Roles.Find(id);
            if (roles == null)
            {
                return HttpNotFound();
            }
            return View(roles);
        }

        // POST: Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA")]
        public ActionResult DeleteConfirmed(int id)
        {
            Roles roles = db.Roles.Find(id);
            db.Roles.Remove(roles);
            db.SaveChanges();
            string empId = HttpContext.Session["empleadoId"].ToString();

            RegisterLogs rl = new RegisterLogs(empId, "Elimino el Rol llamado (" + roles.RolNombre + ")");
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
