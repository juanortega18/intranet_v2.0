﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rotativa;
using intra;
using intra.Models.Inventario;

namespace intra.Controllers
{
    public class TallerController : Controller
    {
        private dbIntranet db = new dbIntranet();
   
        // GET: Taller
        [CustomAuthorize]
        [Authorize(Roles = "IEA,IEWR")]
        public ActionResult Index()
        {
            return View(db.VistaTaller.ToList());
        }
        public PartialViewResult vTaller()
        {
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            return PartialView(db.VistaTaller.ToList());
        }
        //Get Provincia
        [HttpGet]
        public JsonResult GetProvincia(int id)
        {
            List<SelectListItem> listProvincia = new List<SelectListItem>();
            if (id < 1)
            {
                RedirectToAction("Create");
            }
            var Provinces = (from r in db.Provinces
                             from i in db.Region
                             from rp in db.RegionProvincia
                             where i.RegionId == rp.RegionId
                             where r.Province_ID == rp.Province_ID
                             where i.RegionId == id
                             orderby r.Province_Name ascending
                             select new
                             {
                                 r.Province_Name,
                                 r.Province_ID

                             }).ToList();

            foreach (var Province in Provinces)
            {
                listProvincia.Add(new SelectListItem { Text = Province.Province_Name.ToString(), Value = Province.Province_ID.ToString() });
            }
            return Json(new SelectList(listProvincia, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }
        //Get Municipio
        public JsonResult Municipio(string Provincia)
        {
            List<SelectListItem> Municipios;

            int cl = int.Parse(Provincia);
            Municipios = db.Municipalities.Where(x => x.Province_ID == cl).Select(x => new SelectListItem() { Text = x.Municipality_Name, Value = x.Municipality_ID.ToString() }).ToList();

            return Json(new SelectList(Municipios, "Value", "Text"));
        }

        //Get Localidad
        public ActionResult Localidad(string Municipio)
        {
            var municip = int.Parse(Municipio);
            var Localidad = new List<SelectListItem>();
            var localidad = db.Localidad.Where(x => x.Municipality_ID == municip).Select(x => new SelectListItem() { Text = x.LocalidadNombre, Value = x.LocalidadId.ToString() }).ToList();
            return Json(new SelectList(localidad, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        //Generar pdf con rotativa

        // GET: Taller/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Taller taller = db.Taller.Find(id);
            if (taller == null)
            {
                return HttpNotFound();
            }
            return View(taller);
        }

        // GET: Taller/Create
        [CustomAuthorize]
        [Authorize(Roles = "IEA,IEWR")]
        public ActionResult Create()
        {
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            ViewBag.Modelo = new SelectList(db.ModeloInventario.ToList(), "ModeloId", "ModeloDescripcion");
            ViewBag.Estatus = new SelectList(db.EstatusInventario.ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Departamentos = new SelectList(db.Sol_Departamento, "DepartamentoId", "Descripcion");

            return View();
        }

        // POST: Taller/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "IEA,IEWR")]
        public ActionResult Create([Bind(Include = "TallerId, TallerFhCreacion,TallerFhSolucion,Serie,TallerFhEntrega,TallerRazonEntrada,UsuarioRecibe,TallerDescripcionSolucion,Serie,MarcaId,ModeloId,LocalidadId,RegionId,Provincia_ID,Municipality_ID, EstatusId")] FormCollection fh, Taller taller)
        {
            if (taller.DepartamentoId != null)
            {
                taller.DepartamentoId = int.Parse(fh["DepartamentoId"].ToString());
            }

            taller.TallerFhCreacion = DateTime.Now;
            taller.UsuarioRecibe = Session["usuario"].ToString();

            if (ModelState.IsValid)
            {
                taller.TallerFhEntrega = DateTime.Parse("01/01/2000");
            
                db.Taller.Add(taller);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                var error = ModelState.Values.SelectMany(v => v.Errors);
                ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
                ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
                ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
                ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
                ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
                ViewBag.Modelo = new SelectList(db.ModeloInventario.ToList(), "ModeloId", "ModeloDescripcion");
                ViewBag.Estatus = new SelectList(db.EstatusInventario.ToList(), "EstatusId", "EstatusDescripcion");
                ViewBag.Departamentos = new SelectList(db.Sol_Departamento, "DepartamentoId", "Descripcion");
            }

            return View(taller);

        }

        public JsonResult Modelo(string marca)
        {   
            List<SelectListItem> modelos;

            int cl = int.Parse(marca);
            modelos = db.ModeloInventario.Where(x => x.MarcaId == cl).Select(x => new SelectListItem() { Text = x.ModeloDescripcion, Value = x.ModeloId.ToString() }).ToList();

            return Json(new SelectList(modelos, "Value", "Text"));
        }
        public ActionResult Validar()
        {
            using (dbIntranet at = new dbIntranet())
            {

            }
            return View("");
        }

        // GET: Taller/Edit/5
        [CustomAuthorize]
        [Authorize(Roles = "IEA,IEWR")]
        public ActionResult Edit(int? id)
        {
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            ViewBag.Modelo = new SelectList(db.ModeloInventario.ToList(), "ModeloId", "ModeloDescripcion");
            ViewBag.Estatus = new SelectList(db.EstatusInventario.ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Departamentos = new SelectList(db.Sol_Departamento, "DepartamentoId", "Descripcion");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Taller taller = db.Taller.Find(id);
            if (taller == null)
            {
                return HttpNotFound();
            }
            return View(taller);
        }

        // POST: Taller/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "IEA,IEWR")]
        public ActionResult Edit([Bind(Include = "TallerId,TallerFhSolucion,DepartamentoId, TallerFhCreacion, TallerFhEntrega,TallerRazonEntrada, TallerRazonSalida,UsuarioRecibe,TecnicoOResponsable,TallerDescripcionSolucion,Serie,MarcaId,ModeloId,LocalidadId,RegionId,Province_ID,Municipality_ID, EstatusId, DeptoLocal")] Taller taller)
        {
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            ViewBag.Modelo = new SelectList(db.ModeloInventario.ToList(), "ModeloId", "ModeloDescripcion");
            ViewBag.Estatus = new SelectList(db.EstatusInventario.ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Departamentos = new SelectList(db.Sol_Departamento, "DepartamentoId", "Descripcion");


            //taller.UsuarioEntrega = Session["usuario"].ToString();

            if (ModelState.IsValid)
            {

                db.Entry(taller).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(taller);
        }

        // GET: Taller/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VistaTaller VistaTaller = db.VistaTaller.Find(id);
            if (VistaTaller == null)
            {
                return HttpNotFound();
            }
            return View(VistaTaller);
        }

        // POST: Taller/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Taller taller = db.Taller.Find(id);
            db.Taller.Remove(taller);
            db.SaveChanges();
            return RedirectToAction("Index", "Taller");
        }

        [AllowAnonymous]
        public ActionResult ReporteTaller(int id, string usuario, bool salida)
        {
            //ViewBag.TallerFhSolucion = new SelectList(db.VistaTaller.ToList(), "TallerFhSolucion");
            //ViewBag.TallerFhSolucion = db.VistaTaller.ToList();
            //ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID");
            ViewBag.Usuario = usuario;
            var fecha = DateTime.Now.ToShortDateString();
            ViewBag.Fecha = fecha;
            ViewBag.salida = salida;
            var VistaTaller = db.VistaTaller.FirstOrDefault(x => x.TallerId == id);
            return View(VistaTaller);
        }
        
        public ActionResult ReporteTallerRotativa(int id, bool salida = true)
        {    
            return new ActionAsPdf("ReporteTaller", new { id = id, usuario = Session["usuario"].ToString(), salida });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
