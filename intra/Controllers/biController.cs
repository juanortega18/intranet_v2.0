﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;


namespace intra.Controllers
{
    public class biController : Controller
    {
        private inv db = new inv();

        // GET: incautaciones
        [Authorize(Roles = "BI,BIA,BIWR")]
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo BI");
            return View(db.VISTA_INCAUTACIONES.ToList());
        }

        // GET: incautaciones/Details/5
        [Authorize(Roles = "BI,BIA,BIWR")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INV_INCAUTACIONES iNV_INCAUTACIONES = db.INV_INCAUTACIONES.Find(id);
            if (iNV_INCAUTACIONES == null)
            {
                return HttpNotFound();
            }
            return View(iNV_INCAUTACIONES);
        }

        // GET: incautaciones/Create
        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult Create()
        {

            ViewBag.Clases = new SelectList(db.INV_CLASES.ToList(), "CLAS_ID", "CLAS_NOMBRE");
            ViewBag.Tipos = new SelectList(db.INV_TIPOS.ToList(), "TIPO_ID", "TIPO_NOMBRE");
            ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 1).ToList(), "ESTA_ID", "ESTA_NOMBRE");
            ViewBag.ProvInc = new SelectList(db.INV_PROVINCIAS.ToList(), "ID_Provincia", "Descripcion");
            ViewBag.ProvInv = new SelectList(db.INV_PROVINCIAS.ToList(), "ID_Provincia", "Descripcion");
            ViewBag.Colores = new SelectList(db.INV_COLORES.ToList(), "COLOR_ID", "COLOR_NOMBRE");
            ViewBag.Casos = new SelectList(db.INV_CASOS.ToList(), "CASO_ID", "CASO_NOMBRE");

            return View();
        }

        // POST: incautaciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult Create([Bind(Include = "CASO_ID, COLOR_ID, INCA_ACUERDO, INCA_CHASIS, INCA_ID,INCA_NO,INCA_FHCREACION,INCA_LOCALIDADINC,INCA_LOCALIDADINV,INCA_FHENTRADA,INCA_CASO,INCA_CASO_ESTATUS,TIPO_ID,INCA_MODELO,INCA_MARCA,INCA_ANO,INCA_ESTATUS,INCA_SUJETO,INCA_CEDULA,INCA_VALOR,INCA_NOTA,USUA_ID,CLAS_ID")] INV_INCAUTACIONES iNV_INCAUTACIONES)
        {
            iNV_INCAUTACIONES.INCA_NO = Convert.ToChar(64 + iNV_INCAUTACIONES.TIPO_ID).ToString() + iNV_INCAUTACIONES.INCA_FHENTRADA.ToString("yyyyMM") + "-" + DateTime.Now.ToString("ddmmss");
            iNV_INCAUTACIONES.INCA_FHCREACION = DateTime.Now;
            iNV_INCAUTACIONES.USUA_ID = Session["usuario"].ToString();


            if (ModelState.IsValid)
            {
                db.INV_INCAUTACIONES.Add(iNV_INCAUTACIONES);
                db.SaveChanges();

                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo un registro en el modulo BI con el inca_no ("+ iNV_INCAUTACIONES.INCA_NO + ")");
                return RedirectToAction("Index");
            }
            var errors = ModelState.Values.SelectMany(v => v.Errors);


            ViewBag.Clases = new SelectList(db.INV_CLASES.ToList(), "CLAS_ID", "CLAS_NOMBRE");
            ViewBag.Tipos = new SelectList(db.INV_TIPOS.ToList(), "TIPO_ID", "TIPO_NOMBRE");
            ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 1).ToList(), "ESTA_ID", "ESTA_NOMBRE");
            ViewBag.ProvInc = new SelectList(db.INV_PROVINCIAS.ToList(), "ID_Provincia", "Descripcion");
            ViewBag.ProvInv = new SelectList(db.INV_PROVINCIAS.ToList(), "ID_Provincia", "Descripcion");
            ViewBag.Colores = new SelectList(db.INV_COLORES.ToList(), "COLOR_ID", "COLOR_NOMBRE");
            ViewBag.Casos = new SelectList(db.INV_CASOS.ToList(), "CASO_ID", "CASO_NOMBRE");


            return View(iNV_INCAUTACIONES);
        }


      
        public JsonResult dbTipos(string clase)
        {
            List<SelectListItem> tipos;

            using (inv dbi = new intra.inv())
            {
                int cl = int.Parse(clase);
                tipos = dbi.INV_TIPOS.Where(x => x.CLAS_ID == cl).Select(x => new SelectListItem() { Text = x.TIPO_NOMBRE, Value = x.TIPO_ID.ToString() }).ToList();
            }

            return Json(new SelectList(tipos, "Value", "Text"));
        }

        // GET: incautaciones/Edit/5
        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.Clases = new SelectList(db.INV_CLASES.ToList(), "CLAS_ID", "CLAS_NOMBRE");
            ViewBag.Tipos = new SelectList(db.INV_TIPOS.ToList(), "TIPO_ID", "TIPO_NOMBRE");
            ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 1).ToList(), "ESTA_ID", "ESTA_NOMBRE");
            ViewBag.ProvInc = new SelectList(db.INV_PROVINCIAS.ToList(), "ID_Provincia", "Descripcion");
            ViewBag.ProvInv = new SelectList(db.INV_PROVINCIAS.ToList(), "ID_Provincia", "Descripcion");
            ViewBag.Colores = new SelectList(db.INV_COLORES.ToList(), "COLOR_ID", "COLOR_NOMBRE");
            ViewBag.Casos = new SelectList(db.INV_CASOS.ToList(), "CASO_ID", "CASO_NOMBRE");

            INV_INCAUTACIONES iNV_INCAUTACIONES = db.INV_INCAUTACIONES.Find(id);
            if (iNV_INCAUTACIONES == null)
            {
                return HttpNotFound();
            }
            return View(iNV_INCAUTACIONES);
        }

        // POST: incautaciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult Edit([Bind(Include = "INCA_ID,INCA_NO,INCA_FHCREACION,INCA_LOCALIDADINC,INCA_LOCALIDADINV,INCA_FHENTRADA,INCA_CASO,INCA_CASO_ESTATUS,TIPO_ID,INCA_MODELO,INCA_MARCA,INCA_ANO,INCA_ESTATUS,INCA_SUJETO,INCA_CEDULA,INCA_VALOR,INCA_NOTA,USUA_ID,CLAS_ID")] INV_INCAUTACIONES iNV_INCAUTACIONES)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iNV_INCAUTACIONES).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito el registro en el modulo BI con el inca_no (" + iNV_INCAUTACIONES.INCA_NO + ")");
                return RedirectToAction("Index");
            }
            return View(iNV_INCAUTACIONES);
        }

        // GET: incautaciones/Delete/5
        [Authorize(Roles = "BIA")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INV_INCAUTACIONES iNV_INCAUTACIONES = db.INV_INCAUTACIONES.Find(id);
            if (iNV_INCAUTACIONES == null)
            {
                return HttpNotFound();
            }
            return View(iNV_INCAUTACIONES);
        }

        // POST: incautaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "BIA")]
        public ActionResult DeleteConfirmed(int id)
        {
            INV_INCAUTACIONES iNV_INCAUTACIONES = db.INV_INCAUTACIONES.Find(id);
            db.INV_INCAUTACIONES.Remove(iNV_INCAUTACIONES);
            string empId = HttpContext.Session["empleadoId"].ToString();

            RegisterLogs rl = new RegisterLogs(empId, "Elimino el registro en el modulo BI con el inca_no (" + iNV_INCAUTACIONES.INCA_NO + ")");
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
