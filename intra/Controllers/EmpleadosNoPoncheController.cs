﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;
using intra.Code;
using System.Net;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
    public class EmpleadosNoPoncheController : Controller
    {
        private intra2 db = new intra2();
        private dbIntranet db2 = new dbIntranet();
        private string salt = "ergukdfnmjdypo";

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        // GET: EmpleadosNoPonche
        public ActionResult Index()
        {
            var employees = db.EmpleadosNoPonche.ToList();

            foreach (var item in employees)
            {
                var employee = db2.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == item.EmpNoPoncheCodigo.ToString()).FirstOrDefault();
                
                if(employee!=null)
                {
                    string name = employee.NombreCompleto;

                    name = name == null || name == "" ? "" : name;

                    item.EmpNoPoncheEmpleado = name;
                }else
                {
                    item.EmpNoPoncheEmpleado = "";
                }   
            }

            string employee2 = Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(employee2, "Ingresó al módulo de empleados con privilegios de ponches");

            return View(employees);
        }

        [CustomAuthorize]
        // GET: EmpleadosNoPonche/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        // GET: EmpleadosNoPonche/Create
        public ActionResult Create()
        {
            List<Complete> dropdown_values = new List<Complete>();
            dropdown_values.Add(new Complete { chain = "Activo", value = 1 });
            dropdown_values.Add(new Complete { chain = "Inactivo", value = 0 });

            ViewBag.StatusList = new SelectList(dropdown_values, "value", "chain");

            return View();
        }

        // POST: EmpleadosNoPonche/Create
        [HttpPost]
        public ActionResult Create(EmpleadosNoPonche view_object)
        {
            try
            {
                string emp_code = "";

                if(view_object!=null)
                {
                    if(ModelState.IsValid)
                    {
                        string employee_code = db2.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.NombreCompleto == view_object.EmpNoPoncheEmpleado).FirstOrDefault().Codigo;
                        emp_code = employee_code;


                        if (employee_code == null || employee_code == "")
                            return View();

                        view_object.EmpNoPoncheCodigo = Convert.ToInt32(employee_code);
                        
                        db.EmpleadosNoPonche.Add(view_object);
                        db.SaveChanges();
                    }
                }
                // TODO: Add insert logic here

                string employee2 = Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(employee2, "Agregó al empleado " + emp_code + " como un empleado con privilegios de no ponchar");

                return RedirectToAction("Index");
            }
            catch(Exception)
            {
                List<Complete> dropdown_values = new List<Complete>();
                dropdown_values.Add(new Complete { chain = "Activo", value = 1 });
                dropdown_values.Add(new Complete { chain = "Inactivo", value = 0 });

                ViewBag.StatusList = new SelectList(dropdown_values, "value", "chain");

                return View();
            }
        }

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        // GET: EmpleadosNoPonche/Edit/5
        public ActionResult Edit(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //.
            }

            EmpleadosNoPonche empleadosnoponche = db.EmpleadosNoPonche.Find(id);

            if(empleadosnoponche == null)
            {
                return HttpNotFound();
            }

            var employee = db2.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == empleadosnoponche.EmpNoPoncheCodigo.ToString()).FirstOrDefault();

            if (employee != null)
            {
                string name = employee.NombreCompleto;

                name = name == null || name == "" ? "" : name;

                empleadosnoponche.EmpNoPoncheEmpleado = name;
            }
            else
            {
                empleadosnoponche.EmpNoPoncheEmpleado = "";
            }

            List<Complete> dropdown_values = new List<Complete>();
            dropdown_values.Add(new Complete { chain = "Activo", value = 1 });
            dropdown_values.Add(new Complete { chain = "Inactivo", value = 0 });

            ViewBag.StatusList = new SelectList(dropdown_values, "value", "chain");
            empleadosnoponche.jomh = UtilityMethods.SetSHA1(empleadosnoponche.EmpNoPoncheId.ToString() + salt + empleadosnoponche.EmpNoPoncheEmpleado);

            return View(empleadosnoponche);
        }

        // POST: EmpleadosNoPonche/Edit/5
        [HttpPost]
        public ActionResult Edit(EmpleadosNoPonche view_object)
        {
            var security_code = UtilityMethods.SetSHA1(view_object.EmpNoPoncheId.ToString() + salt + view_object.EmpNoPoncheEmpleado);

            if (view_object.jomh != security_code)
            {
                List<Complete> dropdown_values = new List<Complete>();
                dropdown_values.Add(new Complete { chain = "Activo", value = 1 });
                dropdown_values.Add(new Complete { chain = "Inactivo", value = 0 });

                ViewBag.StatusList = new SelectList(dropdown_values, "value", "chain");

                return View();
            }
            
            try
            {
                if (view_object != null)
                {
                    if (ModelState.IsValid)
                    {
                        db.Entry(view_object).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                // TODO: Add insert logic here
                string employee2 = Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(employee2, "Actualizó los datos del empleado con privilegios de no ponchar " + view_object.EmpNoPoncheId.ToString() + ".");

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                List<Complete> dropdown_values = new List<Complete>();
                dropdown_values.Add(new Complete { chain = "Activo", value = 1 });
                dropdown_values.Add(new Complete { chain = "Inactivo", value = 0 });

                ViewBag.StatusList = new SelectList(dropdown_values, "value", "chain");

                return View();
            }
        }

        [CustomAuthorize]
        // GET: EmpleadosNoPonche/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EmpleadosNoPonche/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public JsonResult GetEmployees(string employee_name)
        {
            List<string> employees_names = db2.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.NombreCompleto.StartsWith(employee_name) && x.CodigoDependencia == "10001").Select(y => y.NombreCompleto).ToList();
            //List<string> employees_names2 = new List<string>();
            
            if (employees_names.Count <= 0)
            {
                employees_names.Add("No existe este usuario");
            }

            return Json(employees_names, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
