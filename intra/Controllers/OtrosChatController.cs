﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
    public class OtrosChatController : Controller
    {
        //Variable WebConfig
        public static string Direccion = ConfigurationManager.AppSettings["URLINTRACHAT"];

        public static SqlConnection Enlace = new SqlConnection(ConfigurationManager.ConnectionStrings["intra"].ConnectionString);


        private SolicitudModel smodel = new SolicitudModel();


        private dbIntranet obj = new dbIntranet();

        private Correo ObjCorreo = new Correo();

        #region MostrarInformacionChat
        private void MostrarInformacionChat(int CodigoSolicitud)
        {
            smodel.Tb_Chat = obj.Sol_Chat.Where(cd => cd.SolicitudId == CodigoSolicitud && cd.Estado == true).ToList();
        }
        #endregion

        #region Mostrar_Registro_Solicitud
        private void Mostrar_Registro_Solicitud(int CodigoSolicitud)
        { 
            var DetalleSolicitud = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitud == CodigoSolicitud).FirstOrDefault();

            smodel.DEUsuarioDominioSolic = DetalleSolicitud.UsuarioDominioSolic;
            smodel.DEDescripcionSolicitudes = DetalleSolicitud.DescripcionSolicitud;
            smodel.DEFechaCreacion = DetalleSolicitud.FechaCreacion;
            smodel.DETipoSolicitud = DetalleSolicitud.TipoSolicitud;
            smodel.DENombreSolicitante = DetalleSolicitud.NombreSolicitante;
            smodel.DECodigoSolicitante = DetalleSolicitud.CodigoSolicitante;


        }
        #endregion

        #region correoUsuario
        private string correoUsuario(string cedula)
        {

            string Resultado = "";

            if (Enlace.State == ConnectionState.Closed)
            {
                Enlace.Open();
            }

            //SqlCommand cmd = new SqlCommand("sp_users_ldapID", Enlace);
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Clear();
            //cmd.Parameters.AddWithValue("@id", cedula);
            //SqlDataAdapter Sda = new SqlDataAdapter(cmd);
            //DataTable Dta = new DataTable();
            //Sda.Fill(Dta);

            String Datosusuario = "";

            var usuario = obj.Database.SqlQuery<sp_users_ldapID>("exec sp_users_ldapID {0}",cedula).FirstOrDefault();

            if (usuario == null)
                Datosusuario = "notfound";
            else
                Datosusuario = usuario.samAccountName;

            Resultado = Datosusuario;

            return Resultado;




        }
        #endregion

        #region EnviarCorreo
        private void EnviarCorreo(string Supervisor, string Responsable, string Sujeto, string Descripcion)
        {



            string PrimerCorreo = "";

            string SegundoCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            if (Supervisor != "")
            {
                PrimerCorreo = Supervisor + "@PGR.GOB.DO";
            }
            else
            {
                PrimerCorreo = "";
            }

            SegundoCorreo = Responsable + "@PGR.GOB.DO";

            //ObjCorreo.EnviarCorreo(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion);

            ObjCorreo.EnviarCorreo_(new string[] { PrimerCorreo.Trim(), SegundoCorreo.Trim() }, Sujeto, Descripcion, Logo, Flecha, LogoDTI);


        }


        #endregion

        #region Mostrar_Codigo_Tipo_Solicitud
        private int Mth_Mostrar_Codigo_Tipo_Solicitud(int CodigoSolicitud)
        {
            int Codigo = 0;

            var A = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitud == CodigoSolicitud && cd.Estado == true).FirstOrDefault();

            Codigo = A.CodigoTipoSolicitud;

            return Codigo;

        }
        #endregion

        #region ObtenerEtiquetaHTML
        public string DescripcionCorreo(string Tema,string Departamento, string NombreCompleto, string Problema, string Link, string descripcion)
        {

            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/Solicitud-nuevoServicio.html");


            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", Tema);
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", descripcion);



            return Archivo;
        }

        #endregion

        [HttpGet]
        [CustomAuthorize]
        public ActionResult Download(string file)
        {

            var filepath = System.IO.Path.Combine(Server.MapPath("~/Content/Archivos/"), file);

            return File(filepath, MimeMapping.GetMimeMapping(filepath), file);
        }

        // GET: OtrosChat
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Chat(int id)
        {

            int Cdg = 0;

            Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

            MostrarInformacionChat(id);

            Mostrar_Registro_Solicitud(id);

            Session["CodigoSolicitudes"] = id;



            return View(smodel);
        }

        [HttpPost]
        public ActionResult Proceso(FormCollection Fc, SolicitudModel model, HttpPostedFileBase SubirArchivo)
        {
            if (Fc["Enviar"] != null)
            {
                //Variables Usuario

                string Cdg = "";

                string Usuario = "";

                string DomainUserSolicitante = "";

                string NombreCompleto = "";

                string Departamentos = "";

                string Cedula = "";

                int CodigoSolicitud = 0;

                DomainUserSolicitante = HttpContext.Session["usuario"].ToString();


                //Variable de Secciones
                Cdg = HttpContext.Session["empleadoId"].ToString();
                NombreCompleto = HttpContext.Session["nombre"].ToString();
                Cedula = HttpContext.Session["cedula"].ToString();
                Usuario = HttpContext.Session["usuario"].ToString();
                CodigoSolicitud = int.Parse(Session["CodigoSolicitudes"].ToString());
                Departamentos = Session["departamento"].ToString();

                //Insertar Usuario

                if (Fc["DescripcionChat"] != null)
                {
                    Sol_Chat Tb = new Sol_Chat();


                    //Subir Archivo
                    if (SubirArchivo != null && SubirArchivo.ContentLength > 0)
                    {
                        var NombreArchivo = Path.GetFileName(SubirArchivo.FileName);
                        var extension = Path.GetExtension(SubirArchivo.FileName);

                        //C:\PGRRepositorio\Intranet_PGR\intra\Content\Archivos\)
                        var path = Path.Combine(Server.MapPath("~/Content/Archivos"), NombreArchivo);
                        SubirArchivo.SaveAs(path);

                        if (model.DescripcionChat != null)
                        {

                            var Extensiones = new[] { ".doc", ".xlsx", ".txt", ".jpeg" };

                            if (!Extensiones.Contains(extension))
                            {
                                Tb.SolicitudId = CodigoSolicitud;
                                Tb.Empleadoid = Cdg;
                                Tb.Usuario = Usuario;
                                Tb.Descripcion = model.DescripcionChat;
                                Tb.Estado = true;
                                Tb.Archivo = NombreArchivo;
                                Tb.FhCreacion = DateTime.Now;
                                obj.Sol_Chat.Add(Tb);
                                obj.SaveChanges();


                            }
                            else
                            {
                                ViewBag.MSJG = "DEBE SER UN ARCHIVO EN FORMATO DE WORD, EXCEL, TXT Y JPG";
                            }
                        }
                        else
                        {
                            ViewBag.MSJG = "DEBE DIGITAR UN MENSAJE.";
                        }

                    }
                    else
                    {


                        Tb.SolicitudId = CodigoSolicitud;
                        Tb.Empleadoid = Cdg;
                        Tb.Usuario = Usuario;
                        Tb.Descripcion = model.DescripcionChat;
                        Tb.Estado = true;
                        Tb.Archivo = "NoContiene";
                        Tb.FhCreacion = DateTime.Now;
                        obj.Sol_Chat.Add(Tb);
                        obj.SaveChanges();
                    }
                    //Metodo EnviarCorreo

                    int CdgTipoSolicitud = Mth_Mostrar_Codigo_Tipo_Solicitud(CodigoSolicitud);

                    var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                    var Codigo = obj.Sol_Mostrar_Otros_Tecnicos.Where(cd => cd.Estado == true && cd.SolicitudId == CodigoSolicitud).GroupBy(x => new { x.Cedula }).Select(group => new { U = group.Key, Count = group.Count() });


                    int Conteo = 0;

                    foreach (var item in Codigo)
                    {
                        Conteo++;

                        if (Conteo == 1)
                        {
                            if (item.Count > 0)
                            {
                                // EnviarCorreo(Supervisor.Correo, correoUsuario(item.U.Cedula), "INFORMACION CHAT", Fc["DescripcionChat"].ToString());
                                EnviarCorreo(Supervisor.Correo, correoUsuario(item.U.Cedula), "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("INFORMACIÓN MENSAJERIA", Departamentos, NombreCompleto, Fc["DescripcionChat"].ToString(),Direccion, Supervisor.Descripcion));
                            }

                        }
                        else
                        {
                            if (item.Count > 0)
                            {
                                //EnviarCorreo("", correoUsuario(item.U.Cedula), "INFORMACION CHAT", Fc["DescripcionChat"].ToString());
                                EnviarCorreo(Supervisor.Correo, correoUsuario(item.U.Cedula), "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("INFORMACIÓN MENSAJERIA",Departamentos, NombreCompleto, Fc["DescripcionChat"].ToString(),Direccion, Supervisor.Descripcion));
                            }
                        }


                    }

                    Conteo = 0;

                    Mostrar_Registro_Solicitud(CodigoSolicitud);
                    MostrarInformacionChat(CodigoSolicitud);
                }
                else
                {
                    ViewBag.MSJG = "DEBE DIGITAR UN MENSAJE.";
                }

            }

            smodel.Descripcion = "";
            return View("Chat", smodel);
        }
    }
}