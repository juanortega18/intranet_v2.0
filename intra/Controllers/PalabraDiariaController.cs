﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models;
using System.IO;
using System.Xml.Linq;
using System.Net;
using intra.Models.GestionHumana;
using intra.Models.ViewModel;

namespace intra.Controllers

{
    public class PalabraDiariaController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [AllowAnonymous]
        public ActionResult Index()
        {
            if (HttpContext.Session["empleadoId"] != null) ViewBag.access = "public_user";

            try
            {
                int contador = 0;

                List<bible_fts_full> versiculos = new List<bible_fts_full>();

                while (versiculos.Count < 5)
                {
                    DateTime fecha = DateTime.Now;

                    int day = (int)(fecha.AddDays(-contador) - new DateTime(2000, 1, 1)).TotalDays;
                    Random rnd = new Random(day);
                    int id = rnd.Next(1, 31101);

                    versiculos.Add(db.bible_fts_full.Find(id));

                    contador++;
                }

                return View(versiculos);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return View(new List<bible_fts_full>());
            }

            #region Codigo Viejo

            //string EmpId = string.Empty;

            //try
            //{
            //    if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
            //    {
            //        EmpId = HttpContext.Session["empleadoId"].ToString();
            //    }
            //    else
            //    {
            //        EmpId = "public_user";
            //    }
            //}
            //catch (Exception)
            //{
            //    EmpId = "public_user";
            //}

            //ViewBag.access = EmpId;

            //WebRequest webRequest = WebRequest.Create("http://evangeliodeldia.org/rss/v2/evangelizo_rss-sp.xml");
            //NetworkCredential nc = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["Usuario"], System.Configuration.ConfigurationManager.AppSettings["Contrasena"]);
            //webRequest.Proxy = new WebProxy("http://172.18.0.1:8080/", true, null, nc);
            //WebResponse response = webRequest.GetResponse();
            //Stream dataStream = response.GetResponseStream();
            //StreamReader reader = new StreamReader(dataStream);

            ////   string lecturaSatanas = reader.ReadToEnd();

            //XDocument doc = XDocument.Load(reader);

            //string nowFecha = DateTime.Now.ToShortDateString();
            //string linkAbsoluto = "main.php?language=SP&module=readings&localdate=" + nowFecha;

            //var query = (from feed in doc.Descendants("item")
            //             select new Feed
            //             {
            //                 titulo = feed.Element("title").Value,
            //                 link = feed.Element("link").Value + linkAbsoluto,
            //                 descripcion = feed.Element("description").Value,

            //             }).ToList();

            #endregion
        }
    }
}