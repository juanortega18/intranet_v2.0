﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using intra;
using intra.Models.GestionHumana.CatalogoDescuento;

namespace intra.Controllers.CatalogoDescuento
{
    public class RH_EmpresaCatalogoController : Controller
    {
        private dbIntranet db = new dbIntranet();

        public ActionResult Index()
        {
            ActualizarDescuentos();

            ViewBag.EmpresaCatalogoId = new SelectList(db.RH_EmpresaCatalogo.ToList(), "EmpresaCatalogoId", "EmpresaNombre");

            List<RH_CatalogoDescuentoTipo> CatDescuentoTipo = db.RH_CatalogoDescuentoTipo.ToList();

            //foreach (var item in CatDescuentoTipo) {

            //    var data = item.RH_EmpresaCatalogo.Where(x => x.Estado && x.RH_CatalogoDescuentoTipo.Select(t => t.CatDescuentoTipoId).Contains(item.CatDescuentoTipoId)).ToList();

            //}

            ViewBag.CatDescuentoTipoId = CatDescuentoTipo.Where(x=>x.Estado);

            return View(CatDescuentoTipo);
        }

        [Authorize(Roles = "CB")]
        public ActionResult Empresas()
        {
            ViewBag.CatDescuentoTipoId = db.RH_CatalogoDescuentoTipo.Where(x=>x.Estado).ToList();

            List<RH_EmpresaCatalogo> RH_EmpresaCatalogo = db.RH_EmpresaCatalogo.OrderByDescending(x=>x.EmpresaCatalogoId).ToList();

            return View(RH_EmpresaCatalogo);
        }

        [Authorize(Roles = "CB")]
        // GET: RH_EmpresaCatalogo
        public ActionResult List()
        {
            var rH_EmpresaCatalogo = db.RH_EmpresaCatalogo.Include(r => r.RH_CatalogoDescuentoTipo);
            return View(rH_EmpresaCatalogo.ToList());
        }

        [Authorize(Roles = "CB")]
        public ActionResult CrearEmpresa()
        {
            ViewBag.CatDescuentoTipoId = new SelectList(db.RH_CatalogoDescuentoTipo, "CatDescuentoTipoId", "CatDescuentoDescripcion");
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult CrearCatalogoTipo(RH_CatalogoDescuentoTipo rH_CatalogoDescuentoTipo, FormCollection fc, HttpPostedFileBase file)
        {

            rH_CatalogoDescuentoTipo.CatDescuentoDescripcion = fc["CatDescuentoDescripcion"].ToUpper();
            rH_CatalogoDescuentoTipo.CatInformacionAdicional = fc["CatInformacionAdicional"];
            rH_CatalogoDescuentoTipo.Estado = true;
            rH_CatalogoDescuentoTipo.FechaCreacion = DateTime.Now;

            if (string.IsNullOrWhiteSpace(rH_CatalogoDescuentoTipo.CatDescuentoDescripcion)) return Json("Ingrese el nombre de la sección");
            if (db.RH_CatalogoDescuentoTipo.Any(x => x.CatDescuentoDescripcion.ToUpper() == rH_CatalogoDescuentoTipo.CatDescuentoDescripcion.ToUpper()))
                return Json("Esta sección ya fue registrada");

            try
            {

                if (file != null && file.ContentLength > 0)
                {
                    string path = Path.Combine(Server.MapPath(HttpRuntime.AppDomainAppVirtualPath + "/Content/images/GH_IMAGES/CatalogoTipoIconos"),
                        Path.GetFileName(file.FileName));

                    file.SaveAs(path);

                    rH_CatalogoDescuentoTipo.DescuentoTipoIcono = file.FileName;
                }
                else
                {
                    //ViewBag.Catalogo = "Catalgo";
                    //return Json("");
                }

                db.RH_CatalogoDescuentoTipo.Add(rH_CatalogoDescuentoTipo);
                db.SaveChanges();

                return Json("guardado");
            }
            catch (Exception e)
            {
                e.ToString();
                return Json("error");
            }
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult CrearCatalogoEmpresa(RH_EmpresaCatalogo rH_EmpresaCatalogo, FormCollection fc, HttpPostedFileBase file)
        {
            try
            {
                rH_EmpresaCatalogo.EmpresaNombre = fc["EmpresaNombre"].ToUpper();

                string[] descuentotipos = fc["descuentotipos"].Split(',');

                AgregarTipoDescuento(descuentotipos, rH_EmpresaCatalogo);

                //rH_EmpresaCatalogo.CatDescuentoTipoId = int.Parse(fc["CatDescuentoTipoId"].ToString());

                rH_EmpresaCatalogo.Estado = true;
                rH_EmpresaCatalogo.FechaCreacion = DateTime.Now;

                if (string.IsNullOrWhiteSpace(rH_EmpresaCatalogo.EmpresaNombre)) return Json("Ingrese el nombre de la empresa");

                if (db.RH_EmpresaCatalogo.Any(x => x.EmpresaNombre.ToUpper() == rH_EmpresaCatalogo.EmpresaNombre.ToUpper()))
                    return Json("Esta empresa ya fue registrada");


                if (file != null && file.ContentLength > 0)
                {
                    string path = Path.Combine(Server.MapPath(HttpRuntime.AppDomainAppVirtualPath + "/Content/images/GH_IMAGES/EmpresasLogo"),
                        Path.GetFileName(file.FileName));

                    file.SaveAs(path);

                    rH_EmpresaCatalogo.EmpresaLogo = file.FileName;
                }
                else
                {
                    //ViewBag.Catalogo = "Catalgo";
                    //return Json("");
                }

                db.RH_EmpresaCatalogo.Add(rH_EmpresaCatalogo);
                db.SaveChanges();

                return Json("guardado");
            }
            catch (Exception e)
            {
                e.ToString();
                return Json("error");
            }
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult CrearDescuentoEmpresa(RH_CatalogoDescuento rH_CatalogoDescuento, FormCollection fc, HttpPostedFileBase file)
        {
            try
            {
                #region Convertir los valores a tipo fecha

                DateTime Fecha_Expiracion = DateTime.Now;

                if (Code.Utilities2.convertirStringAfecha(fc["FechaExpiracion"], out Fecha_Expiracion))
                    rH_CatalogoDescuento.FechaExpiracion = Fecha_Expiracion;
                else
                    return Json("fecha invalidas");

                if (rH_CatalogoDescuento.FechaExpiracion < DateTime.Now) return Json("fecha expirada");

                #endregion

                rH_CatalogoDescuento.CatInformacionSucursal = fc["CatInformacionSucursal"];
                rH_CatalogoDescuento.CatDescripcion = fc["__contenido"];

                rH_CatalogoDescuento.EmpresaCatalogoId = int.Parse(fc["EmpresaCatalogoIdModal"].ToString());
                rH_CatalogoDescuento.CatDescuentoTipoId = int.Parse(fc["CatDescuentoTipoId_descuento_modal"].ToString());

                if (rH_CatalogoDescuento.EmpresaCatalogoId == 0) return Json("empresa invalida");

                rH_CatalogoDescuento.CatUsuarioId = int.Parse(Session["empleadoId"].ToString());

                rH_CatalogoDescuento.Estado = true;
                rH_CatalogoDescuento.FechaCreacion = DateTime.Now;

                if (rH_CatalogoDescuento.CatInformacionSucursal.Length > 150) return Json("informacion exedida");

                if (db.RH_CatalogoDescuento.Any(x => x.Estado && x.EmpresaCatalogoId == rH_CatalogoDescuento.EmpresaCatalogoId && x.CatDescuentoTipoId == rH_CatalogoDescuento.CatDescuentoTipoId))
                    return Json("Ya existe un descuento con esta empresa en la sección");

                db.RH_CatalogoDescuento.Add(rH_CatalogoDescuento);
                db.SaveChanges();

                return Json("guardado");
            }
            catch (Exception e)
            {
                e.ToString();
                return Json("error");
            }
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult RemoverCatalogoTipo(int CatDescuentoTipoId)
        {
            try
            {
                var CatalogoTipo = db.RH_CatalogoDescuentoTipo.Find(CatDescuentoTipoId);

                int DescuentosActivos = CatalogoTipo.CatalogoDescuentos.Where(x => x.Estado).ToList().Count;
                int EmpresasActivas = CatalogoTipo.RH_EmpresaCatalogo.Where(x => x.Estado).ToList().Count;

                return Json(new { descuentos  = DescuentosActivos, empresas = EmpresasActivas });
            }
            catch (Exception ex)
            {
                ex.ToString();

                return Json("error");
            }
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult RemoverCatalogoTipoConfirmado(int CatDescuentoTipoId)
        {
            try
            {
                var CatalogoTipo = db.RH_CatalogoDescuentoTipo.Find(CatDescuentoTipoId);

                CatalogoTipo.CatalogoDescuentos.Where(x => x.Estado).ToList().ForEach(x => x.Estado = false);

                //foreach (var descuento in CatalogoTipo.CatalogoDescuentos.Where(x => x.Estado)) { descuento.Estado = false; }

                CatalogoTipo.RH_EmpresaCatalogo.ToList().ForEach(x => x.RH_CatalogoDescuentoTipo.Remove(CatalogoTipo));

                //foreach (var empresa in CatalogoTipo.RH_EmpresaCatalogo.Where(x => x.Estado)) { empresa.RH_CatalogoDescuentoTipo.Remove(CatalogoTipo); }

                CatalogoTipo.Estado = false;

                db.SaveChanges();

                return Json("removido");
            }
            catch (Exception ex)
            {
                ex.ToString();

                return Json("error");
            }
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult ActivarTipoDescuento(int CatDescuentoTipoId)
        {
            try
            {
                var CatalogoTipo = db.RH_CatalogoDescuentoTipo.Find(CatDescuentoTipoId);

                CatalogoTipo.Estado = true;

                db.SaveChanges();

                return Json("activo");
            }
            catch (Exception ex)
            {
                ex.ToString();

                return Json("error");
            }
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult ActivarDescuentoCatalogo(int CatDescuentoId)
        {
            try
            {
                var CatalogoDescuento = db.RH_CatalogoDescuento.Find(CatDescuentoId);

                if(db.RH_CatalogoDescuento.Any(x => x.Estado && x.CatDescuentoId != CatalogoDescuento.CatDescuentoId &&
                x.CatDescuentoTipoId == CatalogoDescuento.CatDescuentoTipoId && x.EmpresaCatalogoId == CatalogoDescuento.EmpresaCatalogoId)) return Json("repetido");

                if (CatalogoDescuento.FechaExpiracion < DateTime.Now) CatalogoDescuento.FechaExpiracion = DateTime.Now;

                CatalogoDescuento.Estado = true;

                db.SaveChanges();

                return Json("activo");
            }
            catch (Exception ex)
            {
                ex.ToString();

                return Json("error");
            }
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult RemoverDescuentoCatalogo(int CatDescuentoId)
        {

            try
            {
                var CatalogoDescuento = db.RH_CatalogoDescuento.Find(CatDescuentoId);

                CatalogoDescuento.Estado = false;

                db.SaveChanges();

                return Json("removido");
            }
            catch (Exception ex)
            {
                ex.ToString();

                return Json("error");
            }
        }

        [Authorize(Roles = "CB")]
        public ActionResult EditarCatalogo(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RH_EmpresaCatalogo rH_EmpresaCatalogo = db.RH_EmpresaCatalogo.Find(id);
            if (rH_EmpresaCatalogo == null)
            {
                return HttpNotFound();
            }
            ViewBag.CatDescuentoTipoId = new SelectList(db.RH_CatalogoDescuentoTipo, "CatDescuentoTipoId", "CatDescuentoDescripcion");
            return View(rH_EmpresaCatalogo);
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public ActionResult EditarCatalogo(RH_EmpresaCatalogo rH_EmpresaCatalogo, HttpPostedFileBase file, FormCollection fc)
        {
            string directory = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/CatalogoDescuentoImagenes";

            try
            {
                //rH_EmpresaCatalogo.CatDescuentoTipoId = rH_EmpresaCatalogo.CatDescuentoTipoId;
                rH_EmpresaCatalogo.EmpresaNombre = rH_EmpresaCatalogo.EmpresaNombre;
                rH_EmpresaCatalogo.Estado = rH_EmpresaCatalogo.Estado;
                rH_EmpresaCatalogo.FechaCreacion = DateTime.Now;

                if (file != null && file.ContentLength > 0)
                {
                    string path = Path.Combine(Server.MapPath(directory), Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    rH_EmpresaCatalogo.EmpresaLogo = directory + file.FileName;
                }
                else
                {
                    rH_EmpresaCatalogo.EmpresaLogo = fc["EmpresaLogo"];
                    ViewBag.CatDescuentoTipoId = new SelectList(db.RH_CatalogoDescuentoTipo, "CatDescuentoTipoId", "CatDescuentoDescripcion");
                    return View();
                }

                db.Entry(rH_EmpresaCatalogo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                e.ToString();
                ViewBag.CatDescuentoTipoId = new SelectList(db.RH_CatalogoDescuentoTipo, "CatDescuentoTipoId", "CatDescuentoDescripcion");
                ViewBag.CatalogoError = "error editar";
                return View();

            }
            //return View(rH_EmpresaCatalogo);
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult ActualizarCatalogoEmpresa(RH_EmpresaCatalogo rH_EmpresaCatalogo, FormCollection fc, HttpPostedFileBase file)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(rH_EmpresaCatalogo.EmpresaNombre)) return Json("El nombre de la empresa no puede estar vacío");

                var EmpresaCatalogo = db.RH_EmpresaCatalogo.Find(int.Parse(fc["EmpresaCatalogoId"].ToString()));

                EmpresaCatalogo.EmpresaNombre = fc["EmpresaNombre"].ToUpper();

                if (db.RH_EmpresaCatalogo.Any(x => x.EmpresaNombre.ToUpper() == EmpresaCatalogo.EmpresaNombre && x.EmpresaCatalogoId != EmpresaCatalogo.EmpresaCatalogoId))
                    return Json("Esta empresa ya fue registrada");

                string[] descuentotipos = fc["descuentotipos"].Split(',');

                if (file != null && file.ContentLength > 0)
                {
                    string path = Path.Combine(Server.MapPath(HttpRuntime.AppDomainAppVirtualPath + "/Content/images/GH_IMAGES/EmpresasLogo"),
                        Path.GetFileName(file.FileName));

                    file.SaveAs(path);

                    EmpresaCatalogo.EmpresaLogo = file.FileName;
                }

                AgregarTipoDescuento(descuentotipos, EmpresaCatalogo);

                db.SaveChanges();

                return Json("actualizado");
            }
            catch (Exception e)
            {
                e.ToString();
                return Json("error");
            }
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult ActualizarDescuentoCatalogo(RH_CatalogoDescuento rH_CatalogoDescuento, FormCollection fc, HttpPostedFileBase file)
        {
            try
            {
                #region Convertir los valores a tipo fecha

                DateTime Fecha_Expiracion = DateTime.Now;

                if (Code.Utilities2.convertirStringAfecha(fc["FechaExpiracion"], out Fecha_Expiracion))
                    rH_CatalogoDescuento.FechaExpiracion = Fecha_Expiracion;
                else
                    return Json("fecha invalidas");

                if (rH_CatalogoDescuento.FechaExpiracion < DateTime.Now.Date) return Json("fecha expirada");

                #endregion

                if (rH_CatalogoDescuento.CatInformacionSucursal.Length > 150) return Json("informacion exedida");

                var registro_CatalogoDescuento = db.RH_CatalogoDescuento.Find(rH_CatalogoDescuento.CatDescuentoId);

                registro_CatalogoDescuento.FechaExpiracion = rH_CatalogoDescuento.FechaExpiracion;
                registro_CatalogoDescuento.CatInformacionSucursal = rH_CatalogoDescuento.CatInformacionSucursal;
                registro_CatalogoDescuento.CatDescripcion = fc["__contenido"];

                db.SaveChanges();

                return Json(new { success = true, registro = rH_CatalogoDescuento });
            }
            catch (Exception e)
            {
                e.ToString();
                return Json("error editar");

            }
            //return View(rH_EmpresaCatalogo);
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult ActualizarDescuentoTipo(RH_CatalogoDescuentoTipo rH_CatalogoDescuentoTipo, FormCollection fc, HttpPostedFileBase file)
        {
            try
            {
                var CatalogoDescuentoTipo = db.RH_CatalogoDescuentoTipo.Find(rH_CatalogoDescuentoTipo.CatDescuentoTipoId);

                CatalogoDescuentoTipo.CatDescuentoDescripcion = fc["CatDescuentoDescripcion"]?.ToUpper();
                CatalogoDescuentoTipo.CatInformacionAdicional = fc["CatInformacionAdicional"];

                if (string.IsNullOrWhiteSpace(CatalogoDescuentoTipo.CatDescuentoDescripcion)) return Json("Ingrese el nombre de la sección");

                if (db.RH_CatalogoDescuentoTipo.Any(x => x.CatDescuentoDescripcion.ToUpper() == CatalogoDescuentoTipo.CatDescuentoDescripcion.ToUpper()
                && x.CatDescuentoTipoId != CatalogoDescuentoTipo.CatDescuentoTipoId))
                    return Json("Existe una sección con este mismo nombre");

                if (file != null && file.ContentLength > 0)
                {
                    string path = Path.Combine(Server.MapPath(HttpRuntime.AppDomainAppVirtualPath + "/Content/images/GH_IMAGES/CatalogoTipoIconos"),
                        Path.GetFileName(file.FileName));

                    file.SaveAs(path);

                    CatalogoDescuentoTipo.DescuentoTipoIcono = file.FileName;
                }

                db.SaveChanges();

                return Json("actualizado");
            }
            catch (Exception e)
            {
                e.ToString();
                return Json("error");
            }
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult CambiarEstadoEmpresa(int EmpresaCatalogoId)
        {
            try
            {
                var CatalogoEmpresa = db.RH_EmpresaCatalogo.Find(EmpresaCatalogoId);

                var descuentos = CatalogoEmpresa.RH_CatalogoDescuento.Where(x=>x.Estado).ToList().Count;

                return Json(descuentos);
            }
            catch (Exception ex)
            {
                ex.ToString();

                return Json("error");
            }
        }

        [HttpPost]
        [Authorize(Roles = "CB")]
        public JsonResult CambiarEstadoEmpresaConfirmado(int EmpresaCatalogoId)
        {
            try
            {
                var CatalogoEmpresa = db.RH_EmpresaCatalogo.Find(EmpresaCatalogoId);

                CatalogoEmpresa.Estado = !CatalogoEmpresa.Estado;

                if(!CatalogoEmpresa.Estado) CatalogoEmpresa.RH_CatalogoDescuento.ToList().ForEach(x => x.Estado = false);

                db.SaveChanges();

                return Json(CatalogoEmpresa.Estado ? "Activo" : "Inactivo");
            }
            catch (Exception ex)
            {
                ex.ToString();

                return Json("error");
            }
        }

        private List<string> ValidarCatalogoDescuentoTipo(RH_CatalogoDescuentoTipo rH_CatalogoDescuentoTipo)
        {
            List<string> Errores = new List<string>();

            if (string.IsNullOrWhiteSpace(rH_CatalogoDescuentoTipo.CatDescuentoDescripcion)) Errores.Add("Ingrese el nombre del catalogo");

            return Errores;
        }

        public JsonResult CargarEmpresa(int EmpresaCatalogoId)
        {
            var Empresa = db.RH_EmpresaCatalogo.Where(x=>x.EmpresaCatalogoId == EmpresaCatalogoId).Select(x => new {

                    EmpresaCatalogoId = x.EmpresaCatalogoId,
                    EmpresaNombre = x.EmpresaNombre,
                    EmpresaLogo = x.EmpresaLogo,
                    FechaCreacion = x.FechaCreacion,
                    RH_CatalogoDescuentoTipo =  x.RH_CatalogoDescuentoTipo.Select(y=>y.CatDescuentoTipoId)

            }).FirstOrDefault();

            return Json(Empresa);
        }

        public JsonResult CargarEmpresas(int CatDescuentoTipoId)
        {
            var ListaEmpresas = db.RH_CatalogoDescuentoTipo.FirstOrDefault(x => x.CatDescuentoTipoId == CatDescuentoTipoId && x.Estado)
                .RH_EmpresaCatalogo.Where(x=>x.Estado).OrderBy(x=>x.EmpresaNombre).
                Select(x => new SelectListItem { Value = x.EmpresaCatalogoId.ToString(), Text = x.EmpresaNombre.ToUpper() }).ToList();

            return Json(ListaEmpresas);
        }

        public JsonResult CargarDescuentoTipo(int CatDescuentoTipoId) {

            var DescuentoTipo = db.RH_CatalogoDescuentoTipo.Where(x => x.CatDescuentoTipoId == CatDescuentoTipoId).Select(x => new {

                CatDescuentoTipoId = x.CatDescuentoTipoId,
                Estado = x.Estado,
                FechaCreacion = x.FechaCreacion,
                CatDescuentoDescripcion = x.CatDescuentoDescripcion,
                CatInformacionAdicional = x.CatInformacionAdicional,
                DescuentoTipoIcono = x.DescuentoTipoIcono

            }).FirstOrDefault();  

            return Json(DescuentoTipo);
        }

        public JsonResult CargarDescuentos(int CatDescuentoTipoId) {

            var Descuentos = db.RH_CatalogoDescuento.Where(x => x.CatDescuentoTipoId == CatDescuentoTipoId).
                Select(x => new {
                    CatDescuentoId = x.CatDescuentoId,
                    Encabezado = x.CatInformacionSucursal,
                    Descripcion = x.CatDescripcion,
                    Empresa = x.RH_EmpresaCatalogo.EmpresaNombre,
                    FhCreacion = x.FechaCreacion,
                    FhExpiracion = x.FechaExpiracion,
                    Estado = x.Estado
                }).ToList();

            return Json(Descuentos);
        }

        public JsonResult CargarDescuento(int CatDescuentoId)
        {
            RH_CatalogoDescuento rH_CatalogoDescuento = db.RH_CatalogoDescuento.Find(CatDescuentoId);

            return Json(new
            {
                rH_CatalogoDescuento.CatDescuentoId,
                rH_CatalogoDescuento.CatUsuarioId,
                rH_CatalogoDescuento.EmpresaCatalogoId,
                rH_CatalogoDescuento.Estado,
                FechaCreacion = rH_CatalogoDescuento.FechaCreacion.ToString("dd/MM/yyyy"),
                FechaExpiracion = rH_CatalogoDescuento.FechaExpiracion.ToString("dd/MM/yyyy"),
                rH_CatalogoDescuento.CatDescripcion,
                rH_CatalogoDescuento.CatInformacionSucursal,
                EmpresaNombre = rH_CatalogoDescuento.RH_EmpresaCatalogo.EmpresaNombre.ToUpper(),
                EmpresaLogo = rH_CatalogoDescuento.RH_EmpresaCatalogo.EmpresaLogo,
                CatDescuentoDescripcion = rH_CatalogoDescuento.CatDescripcion
            });
        }

        private void AgregarTipoDescuento(string[] Seleccionados, RH_EmpresaCatalogo RH_EmpresaCatalogo)
        {
            if (Seleccionados == null)
                Seleccionados = new string[] { };

            if (RH_EmpresaCatalogo.RH_CatalogoDescuentoTipo == null) RH_EmpresaCatalogo.RH_CatalogoDescuentoTipo = new List<RH_CatalogoDescuentoTipo>();

            var tiposelectionadosHS = new HashSet<string>(Seleccionados);
            var marcaTipos = new HashSet<int>(RH_EmpresaCatalogo.RH_CatalogoDescuentoTipo.Select(t => t.CatDescuentoTipoId));

            foreach (var item in db.RH_CatalogoDescuentoTipo)
            {
                if (tiposelectionadosHS.Contains(item.CatDescuentoTipoId.ToString()))
                {
                    if (!marcaTipos.Contains(item.CatDescuentoTipoId))
                    {
                        RH_EmpresaCatalogo.RH_CatalogoDescuentoTipo.Add(item);
                    }
                }
                else
                {
                    if (marcaTipos.Contains(item.CatDescuentoTipoId))
                    {
                        RH_EmpresaCatalogo.RH_CatalogoDescuentoTipo.Remove(item);
                    }
                }
            }

            db.SaveChanges();
        }

        private void ActualizarDescuentos() {

            DateTime fecha = DateTime.Now.Date.AddDays(1);

            List<RH_CatalogoDescuento> DescuentosExpirados = db.RH_CatalogoDescuento.Where(x => x.FechaExpiracion < fecha && x.Estado).ToList();

            if (DescuentosExpirados.Count > 0) {

                foreach (RH_CatalogoDescuento descuento in DescuentosExpirados) {

                    descuento.Estado = false;

                }
                db.SaveChanges();
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
