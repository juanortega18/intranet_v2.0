﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.ModuloVisitas;
using intra.Code;
using System.IO;

namespace intra.Controllers.ModuloVisitas
{
    public class VisitasController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: Visitas
        public ActionResult Index()
        {
            return View(db.Visitas.ToList());
        }

        // GET: Visitas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Visitas visitas = db.Visitas.Find(id);
            if (visitas == null)
            {
                return HttpNotFound();
            }
            return View(visitas);
        }

        //Obtener Personas

        [HttpPost]
        public JsonResult GetPersona(string cedula, int tipoIdentificacion)
        {
            var person = Utilities2.ObtenerPersonaSic(cedula.Replace("-", ""), tipoIdentificacion);

            return Json(person, JsonRequestBehavior.AllowGet);
        }

        // GET: Visitas/Create
        public ActionResult CrearVisita()
        {
            ViewBag.DepartamentoId = new SelectList(db.vw_Departamentos.ToList(), "DeptoID", "Descripcion");

            return View();
        }

        //public void UploadImage(string imageData)
        //{
           
        //}


        // POST: Visitas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult CrearVisitantes(FormCollection fc, Visitas visitas, HttpPostedFileBase file)
        {
           
            string NombreCompleto = fc["Nombres"] + " " + fc["PrimerApellido"] + " " + fc["SegundoApellido"];

            string cedula = fc["NumeroIdentificacion"];
            string route = fc["ImagenVisita"];

            /* string base64 = route.Substring(route.IndexOf(',') + 1);
             base64 = base64.Trim('\0');
             byte[] chartData = Convert.FromBase64String(base64);
             var str = chartData.ToString(); //System.Text.Encoding.Default.GetString(chartData);*/


            string ruta = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/ImagenesVisitas/";
            string foto = Server.MapPath(ruta);
            visitas.VisitaImagen = foto + route;

            /* string path = Path.Combine(Server.MapPath(ruta));
             string fileNameWitPath = path + cedula.Replace("-", "") + ".png";
             using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
             {
                 using (BinaryWriter bw = new BinaryWriter(fs))
                 {
                     byte[] data = Convert.FromBase64String(route);
                     visitas.VisitaImagen = data.ToString();
                     //bw.Write(data);
                     //bw.Close();
                 }
             }*/

            var visitante = db.Visitas.Where(x=> x.VisitaCedula == cedula).FirstOrDefault();



           /* string ruta = HttpRuntime.AppDomainAppVirtualPath + "Content/ImagenesVisitas/";
            
            if (file != null && file.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath(ruta), Path.GetFileName(cedula));
                file.SaveAs(path);
                visitas.VisitaImagen = ruta + file.FileName;
            }*/

            try
            {
                

                if(visitante != null)
                {
                    visitas.VisitaCedula = visitante.VisitaCedula;
                    visitas.VisitaDepartamentoId = int.Parse(fc["DepartamentoId"]);
                    visitas.VisitaFecha = DateTime.Now;
                    visitas.VisitaNombre = NombreCompleto;
                    visitas.VisitaId = visitante.VisitaId;
                    visitas.VisitaImagen = visitante.VisitaImagen;
                    db.SaveChanges();
                    return Json(new { success = true, Mensaje = "Registro Ingresado Correctamente" });
                }
                else
                {

                    visitas.VisitaCedula = cedula;
                    visitas.VisitaDepartamentoId = int.Parse(fc["DepartamentoId"]);
                    visitas.VisitaFecha = DateTime.Now;
                    visitas.VisitaNombre = NombreCompleto;
                   // visitas.VisitaImagen = "No tiene"; //fc["VisitaImagen"];
                    db.Visitas.Add(visitas);
                    db.SaveChanges();

                    return Json(new { success = true, Mensaje = "Registro Ingresado Correctamente" });
                }

            }
            catch
            {
                return Json(new { success = false, Mensaje = "Ha ocurrido un error al registrar esta persona" });

            }

          //  return Json(new { success = true, Mensaje = "Registro Ingresado Correctamente" });


        }

        // GET: Visitas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Visitas visitas = db.Visitas.Find(id);
            if (visitas == null)
            {
                return HttpNotFound();
            }
            return View(visitas);
        }

        // POST: Visitas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VisitaId,VisitaCedula,VisitaNombre,VisitaImagen,VisitaFecha")] Visitas visitas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(visitas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(visitas);
        }

        // GET: Visitas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Visitas visitas = db.Visitas.Find(id);
            if (visitas == null)
            {
                return HttpNotFound();
            }
            return View(visitas);
        }

        // POST: Visitas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Visitas visitas = db.Visitas.Find(id);
            db.Visitas.Remove(visitas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
