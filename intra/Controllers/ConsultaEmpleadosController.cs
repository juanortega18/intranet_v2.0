﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.ViewModel;
using System.Text.RegularExpressions;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
        
    public class ConsultaEmpleadosController : Controller
    {
        dbIntranet db = new dbIntranet();
        // GET: ConsultaEmpleados

        public ActionResult ConsultaEmpleado()
        {
         
            return View();
        }

        //CONSULTA DE EMPLEADOS EN RRHH Y DOMINIO
        [HttpPost]
        public JsonResult BuscarCedula(string cedula, string tipoIdentificacion)
        {
            // cedula = "228-0000982-5";

            //Comentado

            //object ulpad = null;
            //  var ulpad = object(null);

         if (tipoIdentificacion == "")
            {
                return Json(new { success = false, Mensaje = "Debe seleccionar la el tipo de busqueda" }, JsonRequestBehavior.AllowGet);


            }

            var ulpad = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldap '{0}'", cedula.Trim())).FirstOrDefault();


            if (tipoIdentificacion == "2")
            {
                ulpad = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", cedula.Trim())).FirstOrDefault();


            }

            if (ulpad == null)
            {
                return Json(new { success = false, Mensaje = "Esta cedula no se encuentra en el Dominio" }, JsonRequestBehavior.AllowGet);
            }

            if (ulpad.employeeid == "")
            {
                return Json(new { success = false, Mensaje = "Esta persona no tiene cedula en el Dominio" }, JsonRequestBehavior.AllowGet);


            }

            if (ulpad.samAccountName == null)
            {
                return Json(new { success = false, Mensaje = "Esta persona no tiene usuario en el dominio" }, JsonRequestBehavior.AllowGet);


            }

            var empl = db.VISTA_EMPLEADOS.Where(x => x.cedula.Trim() == ulpad.employeeid.Trim()).FirstOrDefault();
            if (empl == null)
            {
                return Json(new { success = false, Mensaje = "Esta persona no se encuentra en RRHH" }, JsonRequestBehavior.AllowGet);
            }


            var empleado = (from ve in db.VISTA_EMPLEADOS
                            select new vEmpleados
                            {
                                CedulaAD = ulpad.employeeid,
                                DepartamentoAD = ulpad.department.ToUpper(),
                                NombreAD = ulpad.displayName.ToUpper(),
                                UsuarioAD = ulpad.samAccountName,
                                CedulaVE = empl.cedula,
                                NombreVE = empl.nombre.ToUpper(),
                                DepartamentoVE = empl.departamento.ToUpper(),
                                CodigoVE = empl.empleadoid.ToUpper(),
                                DependenciaVE = empl.dependencia.ToUpper(),
                                CargoVE = empl.cargo.ToUpper(),

                            }).Where(x => x.CedulaAD == ulpad.employeeid).FirstOrDefault();

            if (empleado == null)
            {
                return Json(new { success = false, Mensaje = "No se encontraron datos para esta persona" }, JsonRequestBehavior.AllowGet);
            }


            return Json( empleado);
        }

    }
}