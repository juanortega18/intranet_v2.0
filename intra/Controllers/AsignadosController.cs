﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;
using System.Globalization;
using intra.Code;
using intra.Controllers;
using intra;
using System.Configuration;
using System.IO;
using intra.Models.GestionHumana;



namespace intra.Controllers
{
    public class AsignadosController : Controller
    {
        SolicitudModel smodel = new SolicitudModel();

        private dbIntranet obj = new dbIntranet();
        RegistroSolicitudController reg = new RegistroSolicitudController();
        Correo correo = new Correo();
        intra.Code.Utilities2 domain = new intra.Code.Utilities2();
        public static string Direccion = ConfigurationManager.AppSettings["URLINTRA"];
        HelpDeskController Help = new HelpDeskController();

        #region ObtenerEtiquetaHTML
        public string DescripcionCorreo(string Tema, string Departamento, string NombreCompleto, string Problema, string Link, string descripcion)
        {
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/Solicitud-nuevoServicio.html");

            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {
                Archivo = sr.ReadToEnd();
            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", Tema);
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", descripcion);

            return Archivo;
        }

        #endregion

        #region EnviarCorreo
        public void EnviarCorreo(string Supervisor, string Responsable, string Sujeto, string Descripcion)
        {
            string PrimerCorreo = "";

            string SegundoCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            if (Supervisor != "")
            {
                PrimerCorreo = Supervisor + "@PGR.GOB.DO";
            }
            else
            {
                PrimerCorreo = "";
            }

            SegundoCorreo = Responsable + "@PGR.GOB.DO";

            //ObjCorreo.EnviarCorreo(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion);

            correo.EnviarCorreo_(new[] { PrimerCorreo.Trim(), SegundoCorreo.Trim() }, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
        }
        #endregion


        //intra.Code code = new Code

        #region MostrarTipoConsulta
        private void MostrarTipoConsulta()
        {
            int Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

            smodel.TipoConsulta = obj.Sol_Tipo_Consulta_Solicitud.Select(cd => new TipoConsultaModel
            {
                ConsultaId = cd.ConsultaId,
                Descripcion = cd.Descripcion
            }).ToList();

            if (Supervisor(Cdg) == 2 && ValidarDirector(Cdg) == false) { smodel.TipoConsulta.RemoveAt(1); }

        }
        #endregion 

        #region MostrarEstadoConsulta
        private void MostrarEstadoConsulta()
        {
            //if (Enlace.State == ConnectionState.Closed)
            //{
            //    Enlace.Open();
            //}

            //SqlCommand cmd = new SqlCommand("Sp_Mostrar_Estado_Busqueda", Enlace);
            //cmd.CommandType = CommandType.StoredProcedure;
            //SqlDataAdapter Sda = new SqlDataAdapter(cmd);
            //DataTable Dta = new DataTable();
            //Sda.Fill(Dta);


            //List<SelectListItem> list = new List<SelectListItem>();

            //smodel.EstadoConsulta = Dta.AsEnumerable().Select(cd => new EstadoConsultaModel
            //{
            //    EstadoId = cd.Field<int>("EstadoId"),
            //    Descripcion = cd.Field<string>("Descripcion"),
            //}).ToList();

            smodel.EstadoConsulta = obj.Sol_Estado_Solicitud.Select(cd => new EstadoConsultaModel
            {
                EstadoId = cd.EstadoId,
                Descripcion = cd.Descripcion
            }).ToList();



        }
        #endregion

        #region Mostrar_Informacion_Departamentos
        private void Mostrar_Informacion_Departamentos()
        {
            //if (Enlace.State == ConnectionState.Closed)
            //{
            //    Enlace.Open();
            //}

            //SqlCommand cmd = new SqlCommand("SP_Mostrar_Dep", Enlace);
            //cmd.CommandType = CommandType.StoredProcedure;
            //SqlDataAdapter Sda = new SqlDataAdapter(cmd);
            //DataTable Dta = new DataTable();
            //Sda.Fill(Dta);


            //List<SelectListItem> list = new List<SelectListItem>();


            //smodel.Departamentos = Dta.AsEnumerable().Select(cd => new DepartamentoModel()
            //{
            //    DepartamentoId = cd.Field<int>("DepartamentoId"),
            //    Descripcion = cd.Field<string>("Descripcion"),
            //}).ToList();
            smodel.Departamentos = obj.Sol_Departamento.ToList().Where(cd => cd.Estado == true).Select(cd => new DepartamentoModel()
            {
                DepartamentoId = cd.DepartamentoId,
                Descripcion = cd.Descripcion
            }).ToList();
        }

        #endregion

        #region ValidarActividad
        public bool ValidarActividad(int CodigoSupervisor)
        {
            bool CdgActividad;


            var Valor = obj.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSupervisor).FirstOrDefault();

            var Valor2 = obj.Sol_DependenciasRegionales.Where(cd => cd.DependenciaSupervisorId == CodigoSupervisor).FirstOrDefault();

            if (Valor != null || Valor2 != null)
            {
                CdgActividad = true;
            }
            else
            {
                CdgActividad = false;
            }


            return CdgActividad;

        }

        #endregion

        #region MostrarSINOSupervisor
        private int Supervisor(int CodigoSupervisor)
        {
            int Resultado = 0;

            var cantidad = obj.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSupervisor).ToList();

            if (cantidad.Count > 0)
            {
                Resultado = 1;
            }
            else
            {
                Resultado = 2;
            }
            return Resultado;
        }
        #endregion

        #region ValidarDirector
        private bool ValidarDirector(int CodigoCdg)
        {
            bool A;

            var Valor = obj.Sol_Departamento.Where(cd => cd.DirectorId == CodigoCdg).ToList();

            if (Valor.Count > 0)
            {

                A = true;
            }
            else
            {
                A = false;
            }

            return A;


        }
        #endregion

        #region Buscar Director
        private int BuscarDirector(int CodigoCdg)
        {
            int CodigoDepartamento = 0;

            var Valor = obj.Sol_Departamento.Where(cd => cd.DirectorId == CodigoCdg).FirstOrDefault();

            if (Valor != null)
            {
                CodigoDepartamento = Valor.DepartamentoId;
            }

            return CodigoDepartamento;
        }

        public List<ActividadDependencia> CodigoActividad2(int CodigoSupervisor)
        {
            List<ActividadDependencia> CdgActividades = new List<ActividadDependencia>();

            var Valor = obj.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSupervisor).ToList();

            if (Valor != null)
            {
                foreach (var item in Valor)
                {
                    CdgActividades.Add(new ActividadDependencia { Key = "Actividad", Value = item.CodigoActividad });
                }
            }

            var Valor2 = obj.Sol_DependenciasRegionales.Where(x => x.DependenciaSupervisorId == CodigoSupervisor).ToList();

            if (Valor2 != null)
            {
                foreach (var item in Valor2)
                {
                    CdgActividades.Add(new ActividadDependencia { Key = "Dependencia", Value = item.DependenciaId });
                }
            }

            return CdgActividades;
        }
        #endregion

        #region MostrarPorActividad
        public int BuscarActividad(int CodigoSupervisor)
        {
            int CdgActividad = 0;

            var Valor = obj.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSupervisor).FirstOrDefault();

            if (Valor != null)
            {
                CdgActividad = Valor.CodigoActividad;
            }

            return CdgActividad;
        }
        #endregion




        [CustomAuthorize]
        // GET: Asignados
        [HttpGet]
        public ActionResult Asignados()
        {
            //Variable 

            int Cdg = 0;

            MostrarTipoConsulta();
            MostrarEstadoConsulta();



            if ((HttpContext.Session["empleadoId"] != null) && (HttpContext.Session["nombre"] != null))
            {


                Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());
                smodel.Usurio = HttpContext.Session["nombre"].ToString();
                //Cdg = 18735;
                string dependenciaId = Session["dependenciaId"].ToString();
                string departamentoId = Session["departamentoId"].ToString();

                var Asignado = obj.Sol_Registro_Solicitud.Where(x => x.TecnicoId == Cdg && x.EstadoId == 1).ToList();
                var solicitado = obj.Sol_Registro_Solicitud.Where(x => x.SolicitanteId == Cdg && x.EstadoId == 1).ToList();

                HttpContext.Session["CantidadAsignados"] = Asignado.Count();
                HttpContext.Session["CantidadSolicitados"] = solicitado.Count();

                var Otrostecnicos = obj.Sol_Otros_Tecnicos.Where(x => x.TecnicoId == Cdg && x.Estado).FirstOrDefault();
                if (Otrostecnicos != null)
                {
                    ViewBag.Otrostecnicos = "Otrostecnico";
                }

                var director = obj.Sol_Departamento.Where(x => x.DirectorId == Cdg).FirstOrDefault();
                if (director != null)
                {
                    ViewBag.Director = "Director";
                }
                var Supervisor = obj.Sol_Actividades.Where(x => x.SupervisorId == Cdg && x.Estado).FirstOrDefault();
                if (Supervisor != null)
                {
                    ViewBag.Supervisor = "Supervisor";
                }
                var supervisorDependencia = obj.Sol_DependenciasRegionales.Where(x => x.DependenciaSupervisorId == Cdg && x.DependenciaEstado).FirstOrDefault();
                if(supervisorDependencia != null)
                {
                    ViewBag.SupervisorDependencia = "SupervisorDependencia";
                }

                var TecnicoAlternativo = obj.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == Cdg && x.Estado && x.AgregarServicio == true).FirstOrDefault();
                if (TecnicoAlternativo != null)
                {
                    ViewBag.TecnicoAlternativo = "tecnicoAlternativo";
                }

                var asignadoEmpleado = (from a in obj.VISTA_EMPLEADOS
                                        where a.dependenciaid == dependenciaId && a.departamentoid == departamentoId
                                        orderby a.nombre ascending
                                        select new PersonalPgrModel
                                        {
                                            EmpleadoId = a.empleadoid,
                                            Descripcion = a.nombre.ToUpper(),
                                            Cedula = a.cedula,

                                        }).ToList();
                ViewBag.tecnico = asignadoEmpleado;


                if (ValidarActividad(Cdg) == true)
                {
                    List<ActividadDependencia> CdgTipoSolicitud = new List<ActividadDependencia>();

                    CdgTipoSolicitud = CodigoActividad2(Cdg);

                    smodel.TB_Detalle_Lista = new List<Vw_Mostrar_Usuario_Con_Su_Descripcion>();


                    foreach (var item in CdgTipoSolicitud)
                    {
                        if (item.Key == "Actividad")
                        {
                            var lista_solicitudes_por_actividad = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoEstadoSolicitud == 1 && cd.CodigoTipoSolicitud == item.Value && cd.Estado == true && cd.DependenciaId == 1).OrderByDescending(cd => cd.FechaCreacion).ToList();


                            foreach (var item2 in lista_solicitudes_por_actividad)
                            {
                                smodel.TB_Detalle_Lista.Add(item2);
                            }
                        }
                        else if (item.Key == "Dependencia")
                        {
                            var lista_solicitudes_por_dependencia = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoEstadoSolicitud == 1 && (cd.DependenciaId == item.Value && cd.DependenciaId != 1) && cd.Estado == true).OrderByDescending(cd => cd.FechaCreacion).ToList();

                            foreach (var item2 in lista_solicitudes_por_dependencia)
                            {
                                smodel.TB_Detalle_Lista.Add(item2);


                            }

                        }
                    }

                    #region Cargar Empleados En Las Depedencias

                    if (!CdgTipoSolicitud.Any(l => l.Key == "Actividad")) {

                        var dependencias = CdgTipoSolicitud.Where(l=>l.Key== "Dependencia").Select(l=>l.Value);

                        ViewBag.tecnico = (
                            from x in obj.Sol_Tecnicos_Dependencias
                            where dependencias.Contains(x.DependenciaId) && x.TecnicoDependenciaEstado
                            select new PersonalPgrModel
                            {
                                EmpleadoId = x.TecnicoDependenciaEmpleadoId.ToString(),
                                Descripcion = x.TecnicoDependenciaNombre,
                                Cedula = x.TecnicoDependenciaCedula
                            }
                        ).ToList();

                    }

                    #endregion

                    var solicitudes_individuales = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoEstadoSolicitud == 1 && cd.Estado == true && cd.CodigoTecnico == Cdg).OrderByDescending(cd => cd.FechaCreacion).ToList();

                    foreach (var item2 in solicitudes_individuales)
                    {
                        if (!smodel.TB_Detalle_Lista.Contains(item2))
                        {
                            smodel.TB_Detalle_Lista.Add(item2);
                        }

                    }
                }
                else
                {
                    smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoEstadoSolicitud == 1 && cd.CodigoTecnico == Cdg && cd.Estado == true).OrderByDescending(cd => cd.FechaCreacion).ToList();
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


            HttpContext.Session["RetornoSolicitud"] = "Asignados";
            return View(smodel);
        }

        [CustomAuthorize]
        [HttpPost]
        public ActionResult Consultas_de_Servicios(SolicitudModel model, FormCollection fc)
        {
            ///Remover Session 



            Session["CodigoSolicitud"] = null;

            Session["CdgDeptos"] = null;

            Session["CodigoSolicitudHistorico"] = null;

            //Variable 
            int Cdg = int.Parse(Session["empleadoId"].ToString());
            string dependenciaId = Session["dependenciaId"].ToString();
            string departamentoId = Session["departamentoId"].ToString();
            var asignadoEmpleado = (from a in obj.Sol_Mostrar_Personal_PGR
                                    where a.CodigoDependencia == dependenciaId && a.CodigoDepartamento == departamentoId
                                    orderby a.NombreCompleto ascending
                                    select new PersonalPgrModel
                                    {
                                        EmpleadoId = a.Codigo,
                                        Descripcion = a.NombreCompleto,
                                        Cedula = a.Cedula,

                                    }).ToList();
            ViewBag.tecnico = asignadoEmpleado;


            var Otrostecnicos = obj.Sol_Otros_Tecnicos.Where(x => x.TecnicoId == Cdg).FirstOrDefault();
            if (Otrostecnicos != null)
            {
                ViewBag.Otrostecnicos = "Otrostecnico";
            }

            var director = obj.Sol_Departamento.Where(x => x.DirectorId == Cdg).FirstOrDefault();
            if (director != null)
            {
                ViewBag.Director = "Director";
            }
            var Supervisor = obj.Sol_Actividades.Where(x => x.SupervisorId == Cdg).FirstOrDefault();
            if (Supervisor != null)
            {
                ViewBag.Supervisor = "Supervisor";
            }
            var TecnicoAlternartivo = obj.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == Cdg && x.Estado == true && x.AgregarServicio == true).FirstOrDefault();

            if (TecnicoAlternartivo != null)
            {
                ViewBag.TecnicoAlternartivo = "TecnicoAlternartivo";

            }

            //var TecnicoAlternativo = obj.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == Cdg && x.Estado).FirstOrDefault();
            //if (TecnicoAlternativo != null)
            //{
            //    ViewBag.TecnicoAlternativo = "tecnicoAlternativo";
            //}


            int codigotipo = model.CodigoTipoConsulta;



            var Usuario = model.FiltrarPorUsuario;

            var Responsable = model.FiltrarPorResponsable;

            var Lista = new List<Vw_Mostrar_Usuario_Con_Su_Descripcion>();

            int CodigoActividad = BuscarActividad(Cdg);

            int CodigoDepartamento = BuscarDirector(Cdg);

            #region Convertir los valores a tipo fecha
            var fechaFin = Request.Form["FechaFinal"].ToString();
            var fechaInicio = Request.Form["FechaInicial"].ToString();
            //Conversion formato fecha Inicial
            DateTime FechaInicial = DateTime.ParseExact(fechaInicio, "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture);
            //Para que la fecha inicial sea YYYY-MM-DD 11:59:59 PM
            DateTime A = codigotipo == 3 ? FechaInicial.AddDays(1).AddSeconds(-1) : DateTime.Now;
            //DateTime A = codigotipo == 3 ? model.FechaInicial : DateTime.Now;

            //Conversion formato fecha Final
            DateTime fechafinal = DateTime.ParseExact(fechaFin, "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture);

            //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            DateTime B = codigotipo == 3 ? fechafinal.AddDays(1).AddSeconds(-1) : DateTime.Now;
            #endregion



            #region buscar codigos de las actividades y dependencias

            var actividades = obj.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == Cdg).Select(x => x.CodigoActividad).ToList();

            List<int?> dependencias = obj.Sol_DependenciasRegionales.Where(x => x.DependenciaSupervisorId == Cdg).Select(x => (int?)x.DependenciaId).ToList();

            #endregion


            Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true
            && (cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta || !(model.CodigoEstadoConsulta > 0))
            && (cd.CodigoDepartamento == CodigoDepartamento || CodigoDepartamento == 0)
            && (cd.CodigoTecnico == Cdg || CodigoDepartamento != 0)
            && (cd.NombreSolicitante.Contains(Usuario) || codigotipo != 1)
            && (cd.NombreTecnico.Contains(Responsable) || codigotipo != 2)
            && (cd.FechaCreacion >= A && cd.FechaCreacion <= B || codigotipo != 3)
                ).OrderByDescending(cd => cd.FechaCreacion).ToList();



            if (ValidarActividad(Cdg) == true)
            {
                //List<ActividadDependencia> CdgTipoSolicitud = new List<ActividadDependencia>();

                //CdgTipoSolicitud = CodigoActividad2(Cdg);

                //foreach (var item in CdgTipoSolicitud)
                //{

                //    var lista_solicitudes_por_actividad = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true
                //    && (cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta || !(model.CodigoEstadoConsulta > 0))
                //    && (cd.CodigoTipoSolicitud == item.Value || item.Key != "Actividad")
                //    && (cd.DependenciaId == item.Value || item.Key != "Dependencia")
                //    && (cd.NombreSolicitante.Contains(Usuario) || codigotipo != 1)
                //    && (cd.NombreTecnico.Contains(Responsable) || codigotipo != 2)
                //    && (cd.FechaCreacion >= A && cd.FechaCreacion <= B || codigotipo != 3)
                //        ).OrderByDescending(cd => cd.FechaCreacion).ToList();


                //    foreach (var item2 in lista_solicitudes_por_actividad)
                //    {
                //        if (!Lista.Contains(item2))
                //        {
                //            Lista.Add(item2);

                //        }
                //    }
                //}

                var lista_id_servicios = obj.Sol_Registro_Solicitud.Where(cd => cd.Estado
                && (actividades.Contains(cd.Tipo_SolicitudId) || dependencias.Contains(cd.DependenciaRegionalId))
                && (cd.EstadoId == model.CodigoEstadoConsulta || !(model.CodigoEstadoConsulta > 0))
                && (cd.FhCreacion >= A && cd.FhCreacion <= B || codigotipo != 3)
                //&& (cd.FhCreacion >= mesanterior && cd.FhCreacion <= DateTime.Now || codigotipo == 3)
                ).OrderByDescending(cd => cd.FhCreacion).Select(x => x.SolicitudId).ToList();

                lista_id_servicios = lista_id_servicios.Distinct().ToList();

                var lista_servicios = obj.Sol_Detalle_Lista.Where(cd => lista_id_servicios.Contains(cd.CodigoSolicitud)
                && (cd.NombreSolicitante.Contains(Usuario) || codigotipo != 1)
                && (cd.NombreTecnico.Contains(Responsable) || codigotipo != 2)
                ).ToList();

                Lista.AddRange(lista_servicios);
            }


            //obj.VISTA_EMPLEADOS


            //var resp = obj.VISTA_EMPLEADOS.Where(x => x.nombre.Contains("maria") ).Select(x => x.empleadoid).Distinct().ToList();


            //var nList = obj.Sol_Registro_Solicitud.Where(x => x.Estado &&
            //((model.CodigoEstadoConsulta == 0) || x.EstadoId == model.CodigoEstadoConsulta) &&
            //(dependencias.Contains((int) x.DependenciaRegionalId) || actividades.Contains(x.Tipo_SolicitudId) || x.TecnicoId == Cdg)
            //&& (codigotipo != 3 || (x.FhCreacion >= A && x.FhCreacion <= B)))
            //.OrderByDescending(x => x.FhCreacion).ToList();


           // var nlistA = obj.Sol_Registro_Solicitud.Join(obj.VISTA_EMPLEADOS, warm => warm, european => european, (warm, european) => warm);



            #region Mostrar informacion

            if (Lista.Count == 0)
            {
                smodel.MensajeError = "NO EXISTEN REGISTROS.";
            }
            smodel.TB_Detalle_Lista = Lista;


            MostrarTipoConsulta();
            MostrarEstadoConsulta();

            #endregion

            #region Comentado

            //if ((codigotipo == 1 || codigotipo == 2 || codigotipo == 3) && (model.CodigoEstadoConsulta > 0))
            //{
            //    if (CodigoDepartamento != 0)
            //    {
            //        switch (codigotipo)
            //        {
            //            case 1:
            //                Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoDepartamento == CodigoDepartamento && cd.Estado == true && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreSolicitante.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                break;
            //            case 2:
            //                Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoDepartamento == CodigoDepartamento && cd.Estado == true && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreTecnico.Contains(Responsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                break;
            //            case 3:
            //                #region BuscarSolicitudesPorFecha

            //                DateTime A = model.FechaInicial;

            //                //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            //                DateTime B = model.FechaFinal.AddDays(1).AddSeconds(-1);

            //                Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoDepartamento == CodigoDepartamento && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.FechaCreacion >= A && cd.FechaCreacion <= B).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                #endregion
            //                break;
            //        }
            //    }
            //    else
            //    {
            //        if (Supervisor(Cdg) == 1)
            //        {
            //            switch (codigotipo)
            //            {
            //                case 1:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTipoSolicitud == CodigoActividad && cd.Estado == true && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreSolicitante.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 2:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTipoSolicitud == CodigoActividad && cd.Estado == true && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreTecnico.Contains(Responsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 3:
            //                    #region BuscarSolicitudesPorFecha

            //                    DateTime A = model.FechaInicial;

            //                    //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            //                    DateTime B = model.FechaFinal.AddDays(1).AddSeconds(-1);

            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTipoSolicitud == CodigoActividad && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.FechaCreacion >= A && cd.FechaCreacion <= B).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    #endregion
            //                    break;
            //            }
            //        }
            //        else if (Supervisor(Cdg) == 2)
            //        {
            //            switch (codigotipo)
            //            {
            //                case 1:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTecnico == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreSolicitante.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 2:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTecnico == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreSolicitante.Contains(Responsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 3:
            //                    #region BuscarSolicitudesPorFecha

            //                    DateTime A = model.FechaInicial;

            //                    //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            //                    DateTime B = model.FechaFinal.AddDays(1).AddSeconds(-1);

            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTecnico == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.FechaCreacion >= A && cd.FechaCreacion <= B && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    #endregion
            //                    break;
            //            }
            //        }
            //    }

            //}
            //else if (model.CodigoEstadoConsulta > 0)
            //{
            //    if (CodigoDepartamento != 0)
            //    {
            //        Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoDepartamento == CodigoDepartamento && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //    }
            //    else
            //    {
            //        if (Supervisor(Cdg) == 1)
            //        {
            //            Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTipoSolicitud == CodigoActividad && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //        }
            //        else
            //        {
            //            Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTecnico == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //        }
            //    }
            //}
            //else if ((codigotipo == 1 || codigotipo == 2 || codigotipo == 3) && model.CodigoEstadoConsulta == 0)
            //{
            //    if (CodigoDepartamento != 0)
            //    {
            //        switch (codigotipo)
            //        {
            //            case 1:
            //                Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoDepartamento == CodigoDepartamento && cd.Estado == true && cd.NombreSolicitante.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                break;
            //            case 2:
            //                Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoDepartamento == CodigoDepartamento && cd.Estado == true && cd.NombreTecnico.Contains(Responsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                break;
            //            case 3:
            //                #region BuscarSolicitudesPorFecha

            //                DateTime A = model.FechaInicial;

            //                //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            //                DateTime B = model.FechaFinal.AddDays(1).AddSeconds(-1);

            //                Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoDepartamento == CodigoDepartamento && cd.FechaCreacion >= A && cd.FechaCreacion <= B).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                #endregion
            //                break;
            //        }
            //    }
            //    else
            //    {
            //        if (Supervisor(Cdg) == 1)
            //        {
            //            switch (codigotipo)
            //            {
            //                case 1:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTipoSolicitud == CodigoActividad && cd.Estado == true && cd.NombreSolicitante.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 2:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTipoSolicitud == CodigoActividad && cd.Estado == true && cd.NombreTecnico.Contains(Responsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 3:
            //                    #region BuscarSolicitudesPorFecha

            //                    DateTime A = model.FechaInicial;

            //                    //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            //                    DateTime B = model.FechaFinal.AddDays(1).AddSeconds(-1);

            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTipoSolicitud == CodigoActividad && cd.FechaCreacion >= A && cd.FechaCreacion <= B).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    #endregion
            //                    break;
            //            }
            //        }
            //        else
            //        {
            //            switch (codigotipo)
            //            {
            //                case 1:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTecnico == Cdg && cd.NombreSolicitante.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 2:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTecnico == Cdg && cd.NombreSolicitante.Contains(Responsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 3:
            //                    #region BuscarSolicitudesPorFecha

            //                    DateTime A = model.FechaInicial;

            //                    //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            //                    DateTime B = model.FechaFinal.AddDays(1).AddSeconds(-1);

            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTecnico == Cdg && cd.FechaCreacion >= A && cd.FechaCreacion <= B).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    #endregion
            //                    break;
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    if (CodigoDepartamento != 0)
            //    {
            //        Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoDepartamento == CodigoDepartamento).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //    }
            //    else
            //    {
            //        if (Supervisor(Cdg) == 1)
            //        {
            //            Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTipoSolicitud == CodigoActividad).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //        }
            //        else
            //        {
            //            Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTecnico == Cdg).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //        }
            //    }
            //}

            #endregion

            smodel.Usurio = HttpContext.Session["nombre"].ToString();


            return View("Asignados", smodel);
        }

        [HttpPost]
        public JsonResult Asignar(int[] solicitudId, string[] tecnico1)
        {
            if (tecnico1 == null)
            {
                return Json(new { success = false, Mensaje = "Seleccione la persona que se le va a asignar el servicio" });
            }
            string empleadoId = Session["empleadoId"].ToString();

            for (int i = 0; i < solicitudId.Length; i++)
            {

                int sol = solicitudId[i];
                var asignado = obj.Sol_Registro_Solicitud.Where(x => x.SolicitudId == sol).FirstOrDefault();

                //for (int a = 0; a <= tecnico1.Length; a++)
                //{

                asignado.TecnicoId = int.Parse(tecnico1[i]);

                var tecnico = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == asignado.TecnicoId.ToString()).FirstOrDefault();

                asignado.DomainUserTecnico = obj.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", tecnico.Cedula)).FirstOrDefault().samAccountName;

                asignado.TecnicoId = asignado.TecnicoId;
                asignado.DomainUserTecnico = asignado.DomainUserTecnico;
                asignado.FhFinalSolicitud = DateTime.Now;
                asignado.FhModificacion = DateTime.Now;
                obj.SaveChanges();

                var empleado = obj.VISTA_EMPLEADOS.Where(x => x.empleadoid == asignado.TecnicoId.ToString()).FirstOrDefault();
                var supervisor = obj.VISTA_EMPLEADOS.Where(x => x.empleadoid == empleadoId).FirstOrDefault();
                var actividad = obj.Sol_Actividades.Where(x => x.ActividadId == asignado.Tipo_SolicitudId).FirstOrDefault();


                string descripcion = "";

                descripcion = "SE REASIGNO " + empleado.nombre.ToUpper() + " POR " + supervisor.nombre.ToUpper();


                reg.InsertarLog(asignado.SolicitudId, descripcion);

                if (asignado.Tipo_SolicitudId != -1)
                {
                    int CdgTipoSolicitud = asignado.SolicitudId;

                    var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado).FirstOrDefault();

                    string correo_supervisor = "";

                    if (Supervisor != null)
                    {
                        if (Supervisor.CodigoDepartamento == 15)
                        {
                            correo_supervisor = Code.Utilities2.obtenerCorreoUsuario(Supervisor.Cedula);
                        }
                        else
                        {
                            correo_supervisor = Code.Utilities2.obtenerCorreoUsuario(Supervisor.Cedula);
                        }
                    }

                    String CorreoUsuario = "";

                    var empleado_Cedula = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == empleado.cedula).FirstOrDefault();

                    if (empleado_Cedula != null)
                    {
                        if (empleado_Cedula.CodigoDependencia == "10001" && empleado_Cedula.CodigoDepartamento == "15")
                        {
                            CorreoUsuario = Code.Utilities2.obtenerCorreoUsuario(empleado_Cedula.Cedula);
                        }
                        else
                        {
                            CorreoUsuario = Code.Utilities2.obtenerCorreoUsuario(empleado_Cedula.Cedula);
                        }
                    }

                    var solicitante_obj = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == asignado.SolicitanteId.ToString()).FirstOrDefault();

                    string solicitante_correo = "";

                    if (solicitante_obj != null)
                    {
                        if (solicitante_obj.CodigoDependencia == "10001" && solicitante_obj.CodigoDepartamento == "15")
                        {
                            solicitante_correo = Code.Utilities2.obtenerCorreoUsuario(solicitante_obj.Cedula);
                        }
                        else
                        {
                            solicitante_correo = Code.Utilities2.obtenerCorreoUsuario(solicitante_obj.Cedula);
                        }
                    }

                    string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/RegistroSolicitud?id={asignado.SolicitudId}";

                    EnviarCorreo(asignado.DomainUserSolicitante, asignado.DomainUserTecnico, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + supervisor.dependencia.ToUpper(), DescripcionCorreo("REASIGNACIÓN DE SOLICITUD", solicitante_obj.Departamento.ToUpper(), solicitante_obj.NombreCompleto.ToUpper(), asignado.DescripcionSolicitud, url, actividad.Descripcion));

                }


                else
                {
                    var Supervisor = obj.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == asignado.DependenciaRegionalId && cd.DependenciaEstado == true).FirstOrDefault();

                    string correo_supervisor = "";

                    if (Supervisor != null)
                    {
                        var Supervisor_DatosPersonales = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == Supervisor.DependenciaSupervisorId.ToString()).FirstOrDefault();

                        correo_supervisor = Code.Utilities2.obtenerCorreoUsuario(Supervisor_DatosPersonales.Cedula);
                    }

                    String CorreoUsuario = "";

                    var empleado_Cedula = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == empleado.cedula).FirstOrDefault();

                    if (empleado_Cedula != null)
                    {
                        if (empleado_Cedula.CodigoDependencia == "10001" && empleado_Cedula.CodigoDepartamento == "15")
                        {
                            CorreoUsuario = Code.Utilities2.obtenerCorreoUsuario(empleado_Cedula.Cedula);
                        }
                        else
                        {
                            CorreoUsuario = Code.Utilities2.obtenerCorreoUsuario(empleado_Cedula.Cedula);
                        }
                    }

                    var solicitante_obj = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == asignado.SolicitanteId.ToString()).FirstOrDefault();

                    string solicitante_correo = "";

                    if (solicitante_obj != null)
                    {
                        if (solicitante_obj.CodigoDependencia == "10001" && solicitante_obj.CodigoDepartamento == "15")
                        {
                            solicitante_correo = Code.Utilities2.obtenerCorreoUsuario(solicitante_obj.Cedula);
                        }
                        else
                        {
                            solicitante_correo = Code.Utilities2.obtenerCorreoUsuario(solicitante_obj.Cedula);
                        }
                    }
                    string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/RegistroSolicitud?id={asignado.SolicitudId}";


                    EnviarCorreo(solicitante_correo, asignado.DomainUserTecnico, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + supervisor.dependencia, DescripcionCorreo("REASIGNACIÓN DE SOLICITUD", supervisor.departamento.ToUpper(), supervisor.nombre.ToUpper(), asignado.DescripcionSolicitud, url, Supervisor.DependenciaNombre ));

                }
            }


            // reg.Mostrar_Registro_Solicitud(asignado.SolicitudId);
            return Json(new { success = true, Mensaje = "Solicitud Asignada Correctamente" });
        }

    }



}