﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;
//using ExcelDataReader;

using Excel;
using System.IO;
using System.Data;
using intra.Code;
using System.IO.Compression;
using System.Data.Sql;
using intra.Models.Nomina;


namespace intra.Controllers
{
    public class ArchivoNominaIntranetController : Controller
    {
        private Nomina db = new Nomina();
        private const string salt = "HbXjj7804IkPMw";

        [CustomAuthorize]
        // GET: ArchivoNominaIntranet
        public ActionResult Index()
        {
            var nomina_files = db.ArchivoNominaIntranet.ToList();
            
            foreach (var item in nomina_files)
            {
                if (item.ArchNomMes!= null)
                {
                    item.ArchNomMesNombre = Utilities.GetMonthName(item.ArchNomMes.Value);
                }   
            }


            return View(nomina_files.OrderBy(x => x.ArchNomId));
        }

        [CustomAuthorize]
        // GET: ArchivoNominaIntranet/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        [CustomAuthorize]
        // GET: ArchivoNominaIntranet/Create
        public ActionResult Create()
        {
            return View();
        }
        
        // POST: ArchivoNominaIntranet/Create
        [HttpPost]
        public ActionResult Create(ArchivoNominaIntranet view_object, FormCollection fc)
        {
            int año = int.Parse(fc["ArchNomAnio"]);
            int mes = int.Parse(fc["ArchNomMes"]);
            int AñoActual = Convert.ToInt32(DateTime.Now.Year);
            try
            {
                var nominaDuplicada = db.ArchivoNominaIntranet.Where(x => x.ArchNomAnio == año && x.ArchNomMes == mes).FirstOrDefault();
                if(nominaDuplicada != null)
                {
                    ViewBag.Nomina = "Ok";

                    return View(view_object);

                   // return View(view_object);
                }

                if (!ModelState.IsValid)
                    return View(view_object);
                
                if(view_object==null)
                    return View(view_object);

                if (view_object.ArchNomAnio < 2010 || view_object.ArchNomAnio > AñoActual)
                {
                    ModelState.AddModelError("ArchNomAnio", "Ha ingresado datos fuera del rango establecido");

                    return View(view_object);

                }
                if (view_object.ArchNomMes < 1 || view_object.ArchNomMes > 12)
                {
                    ModelState.AddModelError("ArchNomMes", "Ha ingresado datos fuera del rango establecido");
                    return View(view_object);
                }
                if (view_object.ArchivoSubido == null || view_object.ArchivoSubido.ContentLength <= 0)
                {
                    ModelState.AddModelError("ArchivoSubido", "Debe subir un archivo.");
                    return View(view_object);
                }

                var is_in = db.ArchivoNominaIntranet.Where(x => x.ArchNomAnio == view_object.ArchNomAnio && x.ArchNomMes == view_object.ArchNomMes).FirstOrDefault();

                if(is_in != null)
                {
                    ModelState.AddModelError("ArchNomMes", "Ya ha subido un archivo correspondiente a esta fecha.");
                    return View(view_object);
                }

                IExcelDataReader reader = null;

                using (var ms = view_object.ArchivoSubido.InputStream)

                {
                    byte[] array = new byte[ms.Length];

                    ms.Read(array, 0, array.Length);

                    view_object.ArchNomArchivo = array;


                    if (view_object.ArchivoSubido.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(ms);
                    }
                    else if (view_object.ArchivoSubido.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(ms);

                    }
                    else
                    {
                        ModelState.AddModelError("ArchivoSubido", "El formato de archivo que ha subido no es soportado.");
                        return View(view_object);
                    }

                   

                }

                
                reader.IsFirstRowAsColumnNames = true;
                DataSet result = reader.AsDataSet();
                reader.Close();

                for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                {
                    if(i!=0)
                    {
                        if (result.Tables[0].Rows[i][2].ToString().StartsWith("00-"))
                        {
                            NominaIntranet nmi = new NominaIntranet();

                            nmi.NominaNombre = result.Tables[0].Rows[i][0].ToString();
                            nmi.NominaCargo = result.Tables[0].Rows[i][1].ToString();
                            nmi.NominaCedula = result.Tables[0].Rows[i][2].ToString().Substring(3, result.Tables[0].Rows[i][2].ToString().Length-3);
                            nmi.NominaIngresoBruto = Convert.ToDecimal(result.Tables[0].Rows[i][4]);
                            nmi.NominaOtrosIngresos = Convert.ToDecimal(result.Tables[0].Rows[i][5]);
                            nmi.NominaTotalIngresos = Convert.ToDecimal(result.Tables[0].Rows[i][6]);
                            nmi.NominaAFP = Convert.ToDecimal(result.Tables[0].Rows[i][7]);
                            nmi.NominaISR = Convert.ToDecimal(result.Tables[0].Rows[i][8]);
                            nmi.NominaSFS = Convert.ToDecimal(result.Tables[0].Rows[i][9]);
                            nmi.NominaOtrosDescuentos = Convert.ToDecimal(result.Tables[0].Rows[i][10]);
                            nmi.NominaTotalDescuentos = Convert.ToDecimal(result.Tables[0].Rows[i][11]);
                            nmi.NominaIngresoNeto = Convert.ToDecimal(result.Tables[0].Rows[i][12]);
                            nmi.NominaMes = view_object.ArchNomMes;
                            nmi.NominaAnio = view_object.ArchNomAnio;

                            db.NominaIntranet.Add(nmi);
                            db.SaveChanges();

                            #region commented_code
                            //var nmi2 = db.NominaIntranet.Where(x => x.NominaCedula == nmi.NominaCedula).FirstOrDefault();

                            //if(nmi2!=null)
                            //{
                            //    nmi2.NominaNombre = nmi.NominaNombre;
                            //    nmi2.NominaCargo = nmi.NominaCargo;
                            //    nmi2.NominaIngresoBruto = nmi.NominaIngresoBruto;
                            //    nmi2.NominaOtrosIngresos = nmi.NominaOtrosIngresos;
                            //    nmi2.NominaTotalIngresos = nmi.NominaTotalIngresos;
                            //    nmi2.NominaAFP = nmi.NominaAFP;
                            //    nmi2.NominaISR = nmi.NominaISR;
                            //    nmi2.NominaSFS = nmi.NominaSFS;
                            //    nmi2.NominaOtrosDescuentos = nmi.NominaOtrosDescuentos;
                            //    nmi2.NominaTotalDescuentos = nmi.NominaTotalDescuentos;
                            //    nmi2.NominaIngresoNeto = nmi.NominaIngresoNeto;

                            //    db.Entry(nmi2).State = System.Data.Entity.EntityState.Modified;

                            //    db.SaveChanges();
                            //}else
                            //{
                            //    db.NominaIntranet.Add(nmi);
                            //    db.SaveChanges();
                            //}
                            #endregion 
                        }
                    }
                }

                #region more_commented_code
                //using (var ms = view_object.ArchivoSubido.InputStream)
                //{
                //    byte[] array = new byte[ms.Length];

                //    ms.Read(array, 0, array.Length);

                //    view_object.ArchNomArchivo = array;

                //    //view_object.ArchivoSubido.InputStream.CopyTo(ms);

                //    //byte[] array = ms.GetBuffer();
                //    //view_object.ArchNomArchivo = array;

                //}
                #endregion

                db.ArchivoNominaIntranet.Add(view_object);
                db.SaveChanges();

                return RedirectToAction("Index");
        }
            catch(Exception error)
            {
                error.ToString();
                ModelState.AddModelError("ArchivoSubido", "El archivo no pudo ser leído correctamente.");
                var error_message = error.Message;
                return View(view_object);
    }
}

        [CustomAuthorize]
        // GET: ArchivoNominaIntranet/Edit/5
        public ActionResult Edit(int? id)
        {
            if(id!=null)
            {
                
                ArchivoNominaIntranet arch_nmi = db.ArchivoNominaIntranet.Find(id);

                if (arch_nmi.ArchNomArchivo.Length == 0)
                {
                    ViewBag.EmptyFile = "Empty";
                }
                else
                {
                    ViewBag.EmptyFile = "Full";
                }

                string imageBase64 = Convert.ToBase64String(arch_nmi.ArchNomArchivo);

                ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,{0}", imageBase64);
                ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";

                arch_nmi.Jmnx = UtilityMethods.SetSHA1(arch_nmi.ArchNomId.ToString() + arch_nmi.ArchNomMes.ToString() + "/" + arch_nmi.ArchNomAnio.ToString());
                
                return View(arch_nmi);
            }
            else
            {
                return RedirectToAction("Index");
            }
            
        }

        // POST: ArchivoNominaIntranet/Edit/5
        [HttpPost]
        public ActionResult Edit(ArchivoNominaIntranet view_object)
        {
            try
            {
                string view_code = UtilityMethods.SetSHA1(view_object.ArchNomId.ToString() + view_object.ArchNomMes.ToString() + "/" + view_object.ArchNomAnio.ToString());

                if (view_code != view_object.Jmnx)
                    return RedirectToAction("Index");

                if (!ModelState.IsValid)
                {
                    ArchivoNominaIntranet arch_nmi = db.ArchivoNominaIntranet.Find(view_object.ArchNomId);

                    if (arch_nmi.ArchNomArchivo.Length == 0)
                    {
                        ViewBag.EmptyFile = "Empty";
                    }
                    else
                    {
                        ViewBag.EmptyFile = "Full";
                    }

                    string imageBase64 = Convert.ToBase64String(arch_nmi.ArchNomArchivo);

                    ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";

                    arch_nmi.Jmnx = UtilityMethods.SetSHA1(arch_nmi.ArchNomId.ToString() + arch_nmi.ArchNomMes.ToString() + "/" + arch_nmi.ArchNomAnio.ToString());

                    return View(arch_nmi);
                }
                
                if (view_object == null)
                {
                    ArchivoNominaIntranet arch_nmi = db.ArchivoNominaIntranet.Find(view_object.ArchNomId);

                    if (arch_nmi.ArchNomArchivo.Length == 0)
                    {
                        ViewBag.EmptyFile = "Empty";
                    }
                    else
                    {
                        ViewBag.EmptyFile = "Full";
                    }

                    string imageBase64 = Convert.ToBase64String(arch_nmi.ArchNomArchivo);

                    ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";

                    arch_nmi.Jmnx = UtilityMethods.SetSHA1(arch_nmi.ArchNomId.ToString() + arch_nmi.ArchNomMes.ToString() + "/" + arch_nmi.ArchNomAnio.ToString());

                    return View(arch_nmi);
                }
                if (view_object.ArchivoSubido == null || view_object.ArchivoSubido.ContentLength <= 0)
                {
                    ArchivoNominaIntranet arch_nmi = db.ArchivoNominaIntranet.Find(view_object.ArchNomId);

                    if (arch_nmi.ArchNomArchivo.Length == 0)
                    {
                        ViewBag.EmptyFile = "Empty";
                    }
                    else
                    {
                        ViewBag.EmptyFile = "Full";
                    }

                    string imageBase64 = Convert.ToBase64String(arch_nmi.ArchNomArchivo);

                    ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";

                    arch_nmi.Jmnx = UtilityMethods.SetSHA1(arch_nmi.ArchNomId.ToString() + arch_nmi.ArchNomMes.ToString() + "/" + arch_nmi.ArchNomAnio.ToString());
                    
                    ModelState.AddModelError("ArchivoSubido", "Debe subir un archivo.");

                    return View(arch_nmi);
                }

                IExcelDataReader reader = null;

                using (var ms = view_object.ArchivoSubido.InputStream)
                {
                    byte[] array = new byte[ms.Length];

                    ms.Read(array, 0, array.Length);

                    view_object.ArchNomArchivo = array;

                    if (view_object.ArchivoSubido.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(ms);
                    }
                    else if (view_object.ArchivoSubido.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(ms);
                    }
                    else
                    {
                        ArchivoNominaIntranet arch_nmi = db.ArchivoNominaIntranet.Find(view_object.ArchNomId);

                        if (arch_nmi.ArchNomArchivo.Length == 0)
                        {
                            ViewBag.EmptyFile = "Empty";
                        }
                        else
                        {
                            ViewBag.EmptyFile = "Full";
                        }

                        string imageBase64 = Convert.ToBase64String(arch_nmi.ArchNomArchivo);

                        ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,{0}", imageBase64);
                        ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";

                        arch_nmi.Jmnx = UtilityMethods.SetSHA1(arch_nmi.ArchNomId.ToString() + arch_nmi.ArchNomMes.ToString() + "/" + arch_nmi.ArchNomAnio.ToString());

                        ModelState.AddModelError("ArchivoSubido", "El formato de archivo que ha subido no es soportado.");

                        return View(arch_nmi);
                    }
                }


                //COMENTADO POR MARIA
                reader.IsFirstRowAsColumnNames = true;

                DataSet result = reader.AsDataSet();



                reader.Close();

                int row_counter = result.Tables.Count;

                if (row_counter<=0)
                {
                    ArchivoNominaIntranet arch_nmi = db.ArchivoNominaIntranet.Find(view_object.ArchNomId);

                    if (arch_nmi.ArchNomArchivo.Length == 0)
                    {
                        ViewBag.EmptyFile = "Empty";
                    }
                    else
                    {
                        ViewBag.EmptyFile = "Full";
                    }

                    string imageBase64 = Convert.ToBase64String(arch_nmi.ArchNomArchivo);

                    ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";

                    arch_nmi.Jmnx = UtilityMethods.SetSHA1(arch_nmi.ArchNomId.ToString() + arch_nmi.ArchNomMes.ToString() + "/" + arch_nmi.ArchNomAnio.ToString());

                    ModelState.AddModelError("ArchivoSubido", "El archivo está vacío.");

                    return View(arch_nmi);
                }

                //List<NominaIntranet> nomina_records_from_excel = new List<NominaIntranet>();

                for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                {
                    if (i != 0)
                    {
                        if (result.Tables[0].Rows[i][2].ToString().StartsWith("00-"))
                        {
                            NominaIntranet nmi = new NominaIntranet();

                            nmi.NominaNombre = result.Tables[0].Rows[i][0].ToString();
                            nmi.NominaCargo = result.Tables[0].Rows[i][1].ToString();
                            nmi.NominaCedula = result.Tables[0].Rows[i][2].ToString().Substring(3, result.Tables[0].Rows[i][2].ToString().Length - 3);
                            nmi.NominaIngresoBruto = Convert.ToDecimal(result.Tables[0].Rows[i][4]);
                            nmi.NominaOtrosIngresos = Convert.ToDecimal(result.Tables[0].Rows[i][5]);
                            nmi.NominaTotalIngresos = Convert.ToDecimal(result.Tables[0].Rows[i][6]);
                            nmi.NominaAFP = Convert.ToDecimal(result.Tables[0].Rows[i][7]);
                            nmi.NominaISR = Convert.ToDecimal(result.Tables[0].Rows[i][8]);
                            nmi.NominaSFS = Convert.ToDecimal(result.Tables[0].Rows[i][9]);
                            nmi.NominaOtrosDescuentos = Convert.ToDecimal(result.Tables[0].Rows[i][10]);
                            nmi.NominaTotalDescuentos = Convert.ToDecimal(result.Tables[0].Rows[i][11]);
                            nmi.NominaIngresoNeto = Convert.ToDecimal(result.Tables[0].Rows[i][12]);
                            nmi.NominaMes = view_object.ArchNomMes;
                            nmi.NominaAnio = view_object.ArchNomAnio;

                            //nomina_records_from_excel.Add(nmi);

                            db.NominaIntranet.Add(nmi);
                            db.SaveChanges();

                            #region commented_code
                            //var nmi2 = db.NominaIntranet.Where(x => x.NominaCedula == nmi.NominaCedula).FirstOrDefault();

                            //if(nmi2!=null)
                            //{
                            //    nmi2.NominaNombre = nmi.NominaNombre;
                            //    nmi2.NominaCargo = nmi.NominaCargo;
                            //    nmi2.NominaIngresoBruto = nmi.NominaIngresoBruto;
                            //    nmi2.NominaOtrosIngresos = nmi.NominaOtrosIngresos;
                            //    nmi2.NominaTotalIngresos = nmi.NominaTotalIngresos;
                            //    nmi2.NominaAFP = nmi.NominaAFP;
                            //    nmi2.NominaISR = nmi.NominaISR;
                            //    nmi2.NominaSFS = nmi.NominaSFS;
                            //    nmi2.NominaOtrosDescuentos = nmi.NominaOtrosDescuentos;
                            //    nmi2.NominaTotalDescuentos = nmi.NominaTotalDescuentos;
                            //    nmi2.NominaIngresoNeto = nmi.NominaIngresoNeto;

                            //    db.Entry(nmi2).State = System.Data.Entity.EntityState.Modified;

                            //    db.SaveChanges();
                            //}else
                            //{
                            //    db.NominaIntranet.Add(nmi);
                            //    db.SaveChanges();
                            //}
                            #endregion 
                        }
                    }
                }

                #region commentecode2
                //List<NominaIntranet> nomina_record_from_table = db.NominaIntranet.Where(x => x.NominaMes == view_object.ArchNomMes && x.NominaAnio == view_object.ArchNomAnio).ToList();

                //foreach (var item in nomina_records_from_excel)
                //{
                //    var row_from_table = nomina_record_from_table.Where(x => x.NominaCedula == item.NominaCedula).FirstOrDefault();

                //    if(row_from_table!=null)
                //    {
                //        row_from_table.NominaAFP = item.NominaAFP;
                //        row_from_table.NominaAnio = item.NominaAnio;
                //        row_from_table.NominaCargo = item.NominaCargo;
                //        row_from_table.NominaCedula = item.NominaCedula;
                //        row_from_table.NominaIngresoBruto = item.NominaIngresoBruto;
                //        row_from_table.NominaIngresoNeto = item.NominaIngresoNeto;
                //        row_from_table.NominaISR = item.NominaISR;
                //        row_from_table.NominaMes = item.NominaMes;
                //        row_from_table.NominaNombre = item.NominaNombre;
                //        row_from_table.NominaOtrosDescuentos = item.NominaOtrosDescuentos;
                //        row_from_table.NominaOtrosIngresos = item.NominaOtrosIngresos;
                //        row_from_table.NominaSFS = item.NominaSFS;
                //        row_from_table.NominaTotalDescuentos = item.NominaTotalDescuentos;
                //        row_from_table.NominaTotalIngresos = item.NominaTotalIngresos;

                //        db.Entry(row_from_table).State = System.Data.Entity.EntityState.Modified; 
                //    }
                //    else
                //    {
                //        db.NominaIntranet.Add(item);
                //        db.SaveChanges();
                //    }

                //}

                //foreach (var item in nomina_record_from_table)
                //{
                //    var row_from_excel = nomina_records_from_excel.Where(x => x.NominaCedula == item.NominaCedula).FirstOrDefault();

                //    if (row_from_excel == null)
                //    {
                //        db.NominaIntranet.Remove(item);
                //        db.SaveChanges();
                //    }
                //}
                #endregion

                #region more_commented_code
                //using (var ms = view_object.ArchivoSubido.InputStream)
                //{
                //    byte[] array = new byte[ms.Length];

                //    ms.Read(array, 0, array.Length);

                //    view_object.ArchNomArchivo = array;

                //    //view_object.ArchivoSubido.InputStream.CopyTo(ms);

                //    //byte[] array = ms.GetBuffer();
                //    //view_object.ArchNomArchivo = array;

                //}
                #endregion

                db.Entry(view_object).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch(Exception error)
            {
                var message = error.Message;

                ArchivoNominaIntranet arch_nmi = db.ArchivoNominaIntranet.Find(view_object.ArchNomId);

                if (arch_nmi.ArchNomArchivo.Length == 0)
                {
                    ViewBag.EmptyFile = "Empty";
                }
                else
                {
                    ViewBag.EmptyFile = "Full";
                }

                string imageBase64 = Convert.ToBase64String(arch_nmi.ArchNomArchivo);

                ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,{0}", imageBase64);
                ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";

                arch_nmi.Jmnx = UtilityMethods.SetSHA1(arch_nmi.ArchNomId.ToString() + arch_nmi.ArchNomMes.ToString() + "/" + arch_nmi.ArchNomAnio.ToString());

                ModelState.AddModelError("ArchivoSubido", "El archivo no pudo ser leído correctamente.");

                return View(arch_nmi);
            }
        }

        [CustomAuthorize]
        // GET: ArchivoNominaIntranet/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ArchivoNominaIntranet/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult DeleteFile(int id)
        {
            try
            {
                var arh_nom = db.ArchivoNominaIntranet.Find(id);

                arh_nom.ArchNomArchivo = new byte[0];

                db.Entry(arh_nom).State = System.Data.Entity.EntityState.Modified;
                
                db.NominaIntranet.RemoveRange(db.NominaIntranet.Where(x => x.NominaMes == arh_nom.ArchNomMes && x.NominaAnio == arh_nom.ArchNomAnio));
                db.SaveChanges();

                return Json("success");
            }catch(Exception)
            {
                return Json("error");
            }
        }
    }
}
