﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Models.TallerPgr;

namespace intra.Controllers
{
    public class TallerPgrController : Controller
    {
        private TallerPgrDM db = new TallerPgrDM();

        // GET: TallerPgr
        public ActionResult Index()
        {
            var tallerPgr = db.TallerPgr.Include(t => t.EntradaTallerPgr);
            return View(tallerPgr.ToList());
        }

        // GET: TallerPgr/Create
        public ActionResult Create()
        {
            ViewBag.EntradaId = new SelectList(db.EntradaTallerPgr, "EntradaId", "EmpleadoTallerId");
            return View();
        }

        // POST: TallerPgr/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TallerPgrId,EntradaId,TallerPgrAvance,TallerPgrFecha")] TallerPgr tallerPgr)
        {

            tallerPgr.TallerPgrFecha = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.TallerPgr.Add(tallerPgr);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EntradaId = new SelectList(db.EntradaTallerPgr, "EntradaId", "EmpleadoTallerId", tallerPgr.EntradaId);
            return View(tallerPgr);
        }

        // GET: TallerPgr/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TallerPgr tallerPgr = db.TallerPgr.Find(id);
            if (tallerPgr == null)
            {
                return HttpNotFound();
            }
            ViewBag.EntradaId = new SelectList(db.EntradaTallerPgr, "EntradaId", "EmpleadoTallerId", tallerPgr.EntradaId);
            return View(tallerPgr);
        }

        // POST: TallerPgr/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TallerPgrId,EntradaId,TallerPgrAvance,TallerPgrFecha")] TallerPgr tallerPgr)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tallerPgr).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EntradaId = new SelectList(db.EntradaTallerPgr, "EntradaId", "EmpleadoTallerId", tallerPgr.EntradaId);
            return View(tallerPgr);
        }

        // GET: TallerPgr/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TallerPgr tallerPgr = db.TallerPgr.Find(id);
            if (tallerPgr == null)
            {
                return HttpNotFound();
            }
            return View(tallerPgr);
        }

        // POST: TallerPgr/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TallerPgr tallerPgr = db.TallerPgr.Find(id);
            db.TallerPgr.Remove(tallerPgr);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
