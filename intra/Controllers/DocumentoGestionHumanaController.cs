﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.GestionHumana;
using System.IO;
using intra.Models.ViewModel;
using System.Text.RegularExpressions;

namespace intra.Controllers
{
    public class DocumentoGestionHumanaController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        public ActionResult Index(int? id)
        {
            var DocumentosGHViewModel = new DocumentosGHViewModel();

            if (id == null) {
                DocumentosGHViewModel.CarpetasDocumentosGHs = db.CarpetasDocumentosGH.Where(x => x.CarpetaRaizId == null && x.Estado).ToList();
                DocumentosGHViewModel.DocumentoGestionHumana = db.DocumentoGestionHumana.Where(x => x.CarpetaId == null && x.Estado).ToList();
                return View(DocumentosGHViewModel);
            }

            DocumentosGHViewModel.CarpetasDocumentosGHs = db.CarpetasDocumentosGH.Where(x => x.CarpetaRaizId == id && x.Estado).ToList();
            DocumentosGHViewModel.DocumentoGestionHumana = db.DocumentoGestionHumana.Where(x => x.CarpetaId == id && x.Estado).ToList();
            DocumentosGHViewModel.CarpetasDocumentosGH = db.CarpetasDocumentosGH.Find(id);

            return View(DocumentosGHViewModel);
        }
        public ActionResult Documentos()
        {
            int id = 6;
            var DocumentosGHViewModel = new DocumentosGHViewModel();

            if (id == 0)
            {
                DocumentosGHViewModel.CarpetasDocumentosGHs = db.CarpetasDocumentosGH.Where(x => x.CarpetaRaizId == null && x.Estado).ToList();
                DocumentosGHViewModel.DocumentoGestionHumana = db.DocumentoGestionHumana.Where(x => x.CarpetaId == null && x.Estado).ToList();
                return View(DocumentosGHViewModel);
            }

            DocumentosGHViewModel.CarpetasDocumentosGHs = db.CarpetasDocumentosGH.Where(x => x.CarpetaRaizId == id && x.Estado).ToList();
            DocumentosGHViewModel.DocumentoGestionHumana = db.DocumentoGestionHumana.Where(x => x.CarpetaId == id && x.Estado).ToList();
            DocumentosGHViewModel.CarpetasDocumentosGH = db.CarpetasDocumentosGH.Find(id);

            return View(DocumentosGHViewModel);
        }


        [CustomAuthorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            DocumentoGestionHumana documentoGestionHumana = db.DocumentoGestionHumana.Find(id);

            if (documentoGestionHumana == null)
            {
                return HttpNotFound();
            }

            ViewBag.IconImage = IconoImagen(documentoGestionHumana);

            return View(documentoGestionHumana);
        }

        [Authorize(Roles = "DOCINS")]
        public ActionResult CrearDocumentos(int? id)
        {
            if (VerificarCarpeta(id))
            {
                ViewBag.carpetas = new SelectList(db.CarpetasDocumentosGH.Where(x => x.CarpetaId == id && x.Estado).ToList(), "CarpetaId", "CarpetaNombre");
                ViewBag.carpetaid = id;
                return View();
            }
            ViewBag.carpetas = new SelectList(db.CarpetasDocumentosGH.Where(x => x.Estado).ToList(), "CarpetaId", "CarpetaNombre");
            ViewBag.carpetaid = null;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "DOCINS")]
        [ValidateAntiForgeryToken]
        public ActionResult CrearDocumentos(DocumentoGestionHumana documentoGestionHumana)
        {
            if (ModelState.IsValid && documentoGestionHumana.DocGHArchivoVista!=null && (VerificarCarpeta(documentoGestionHumana.CarpetaId) || documentoGestionHumana.CarpetaId == null))
            {
                documentoGestionHumana.DocGHFhCreacion = DateTime.Now;
                documentoGestionHumana.DocGHUsuarioCreador = Convert.ToInt32(HttpContext.Session["empleadoId"].ToString());
                documentoGestionHumana.DocGHNombreUsuarioCreador = HttpContext.Session["nombre"].ToString();
                documentoGestionHumana.Estado = true;

                documentoGestionHumana.DocGHArchivoNombre = Path.GetFileName(documentoGestionHumana.DocGHArchivoVista.FileName);
                documentoGestionHumana.DocGHExtension = Path.GetExtension(documentoGestionHumana.DocGHArchivoVista.FileName);

                using (MemoryStream ms = new MemoryStream())
                {
                    documentoGestionHumana.DocGHArchivoVista.InputStream.CopyTo(ms);
                    documentoGestionHumana.DocGHArchivo = ms.GetBuffer();
                }

                db.DocumentoGestionHumana.Add(documentoGestionHumana);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = documentoGestionHumana.CarpetaId });
            }
            if (documentoGestionHumana.DocGHArchivoVista == null)
                ViewBag.errorDocumento = "Debe Subir el Documento";
            ViewBag.carpetas = new SelectList(db.CarpetasDocumentosGH.Where(x => x.Estado).ToList(), "CarpetaId", "CarpetaNombre",documentoGestionHumana.CarpetaId);
            ViewBag.carpetaid = documentoGestionHumana.CarpetaId;
            return View(documentoGestionHumana);
        }

        [Authorize(Roles = "DOCINS")]
        public ActionResult AgregarCarpeta(int? id) {

            if (VerificarCarpeta(id)) {
                ViewBag.carpetas = new SelectList(db.CarpetasDocumentosGH.Where(x => x.CarpetaId == id && x.Estado).ToList(), "CarpetaId", "CarpetaNombre");
                ViewBag.carpetaid = id;
                return View();
            }

            ViewBag.carpetas = new SelectList(db.CarpetasDocumentosGH.Where(x => x.Estado).ToList(), "CarpetaId", "CarpetaNombre");
            ViewBag.carpetaid = null;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "DOCINS")]
        public ActionResult AgregarCarpeta(CarpetasDocumentosGH carpeta)
        {
            if (ModelState.IsValid && (VerificarCarpeta(carpeta.CarpetaRaizId) || carpeta.CarpetaRaizId == null)) {

                carpeta.Estado = true;
                db.CarpetasDocumentosGH.Add(carpeta);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = carpeta.CarpetaRaizId });
            }
            ViewBag.carpetas = new SelectList(db.CarpetasDocumentosGH.Where(x => x.CarpetaId == carpeta.CarpetaRaizId && x.Estado).ToList(), "CarpetaId", "CarpetaNombre");
            ViewBag.carpetaid = carpeta.CarpetaRaizId;
            return View();
        }

        // GET: DocumentoGestionHumana/Edit/5
        [Authorize(Roles = "DOCINS")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            DocumentoGestionHumana documentoGestionHumana = db.DocumentoGestionHumana.Find(id);

            if (documentoGestionHumana == null)
            {
                return HttpNotFound();
            }

            ViewBag.IconImage = IconoImagen(documentoGestionHumana);

            ViewBag.carpetas = new SelectList(db.CarpetasDocumentosGH.Where(x => x.Estado).ToList(), "CarpetaId", "CarpetaNombre", documentoGestionHumana.CarpetaId);

            return View(documentoGestionHumana);
        }

        [HttpPost]
        [Authorize(Roles = "DOCINS")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DocumentoGestionHumana documentoGHnuevo)
        {
            var documento = db.DocumentoGestionHumana.Find(documentoGHnuevo.DocGHId);

            if (documento == null) { return HttpNotFound(); }

            if (ModelState.IsValid && (VerificarCarpeta(documentoGHnuevo.CarpetaId) || documentoGHnuevo.CarpetaId == null))
            {
                documento.DocGHFhModificacion = DateTime.Now;
                documento.DocGHUsuarioModificador = Convert.ToInt32(HttpContext.Session["empleadoId"].ToString());
                documento.DocGHNombreUsuarioModificador = HttpContext.Session["nombre"].ToString();
                documento.DocGHDescripcion = documentoGHnuevo.DocGHDescripcion;
                documento.CarpetaId = documentoGHnuevo.CarpetaId;

                if (documentoGHnuevo.DocGHArchivoVista != null)
                {
                    documento.DocGHArchivoNombre = Path.GetFileName(documentoGHnuevo.DocGHArchivoVista.FileName);
                    documento.DocGHExtension = Path.GetExtension(documentoGHnuevo.DocGHArchivoVista.FileName);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        documentoGHnuevo.DocGHArchivoVista.InputStream.CopyTo(ms);
                        documento.DocGHArchivo = ms.GetBuffer();
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index", new { id = documentoGHnuevo.CarpetaId});
            }
            ViewBag.IconImage = IconoImagen(documento);
            ViewBag.carpetas = new SelectList(db.CarpetasDocumentosGH.Where(x => x.Estado == true).ToList(), "CarpetaId", "CarpetaNombre", documentoGHnuevo.CarpetaId);
            return View(documentoGHnuevo);
        }

        [Authorize(Roles = "DOCINS")]
        public ActionResult EditarCarpeta(int? id) {

            var carpeta = db.CarpetasDocumentosGH.Find(id);
            if (carpeta == null ) { return HttpNotFound(); }
            ViewBag.carpetas = new SelectList(db.CarpetasDocumentosGH.Where(x=>x.CarpetaId != id && x.Estado).ToList(), "CarpetaId", "CarpetaNombre" , carpeta.CarpetaRaizId);
            return View(carpeta);
        }

        [HttpPost]
        [Authorize(Roles = "DOCINS")]
        public ActionResult EditarCarpeta(CarpetasDocumentosGH carpetaNueva) {

            var carpeta = db.CarpetasDocumentosGH.Find(carpetaNueva.CarpetaId);
            if (carpeta == null) { return HttpNotFound(); }

            if(ModelState.IsValid && (VerificarCarpeta(carpetaNueva.CarpetaRaizId) || carpetaNueva.CarpetaRaizId == null))
            {
                carpeta.CarpetaNombre = carpetaNueva.CarpetaNombre;
                carpeta.CarpetaRaizId = carpetaNueva.CarpetaRaizId;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = carpetaNueva.CarpetaRaizId});
            }
            ViewBag.carpetas = new SelectList(db.CarpetasDocumentosGH.Where(x => x.CarpetaId != carpeta.CarpetaId && x.Estado).ToList(), "CarpetaId", "CarpetaNombre", carpetaNueva.CarpetaRaizId);
            return View(carpetaNueva);
        }

        [HttpPost]
        [Authorize(Roles = "DOCINS")]
        public JsonResult EliminarDocumento(int id)
        {
            //if (id == null) { return RedirectToAction("Index"); }
            DocumentoGestionHumana documentoGestionHumana = db.DocumentoGestionHumana.Find(id);

            if (documentoGestionHumana == null) { return Json("Error"); }

            documentoGestionHumana.Estado = false;
            db.SaveChanges();

            return Json("Eliminado");
        }

        [HttpPost]
        [Authorize(Roles = "DOCINS")]
        public ActionResult EliminarCarpeta(int id)
        {
            CarpetasDocumentosGH carpeta = db.CarpetasDocumentosGH.Find(id);

            if (carpeta == null) { return Json("Error"); }

            var nuevaCarpetaRaiz = carpeta.CarpetaRaizId;

            carpeta.Estado = false;

            foreach (CarpetasDocumentosGH carpetaEncarpeta in db.CarpetasDocumentosGH.Where(x=>x.Estado && x.CarpetaRaizId == id).ToList()) {
                carpetaEncarpeta.CarpetaRaizId = nuevaCarpetaRaiz;
            }

            foreach (DocumentoGestionHumana documentoEncarpeta in db.DocumentoGestionHumana.Where(x => x.Estado && x.CarpetaId == id).ToList()) {
                documentoEncarpeta.CarpetaId = nuevaCarpetaRaiz;
            }

            db.SaveChanges();

            return Json("Eliminado");

        }


     
        public ActionResult Pdf(int documentoId)
        {
            var documento = db.DocumentoGestionHumana.FirstOrDefault(d => d.DocGHId == documentoId);

            var bytes = documento.DocGHArchivo;
            FileResult fileResult = new FileContentResult(bytes, "application/pdf");
            return fileResult;
        }

        [HttpGet]
        [AllowAnonymous]
        public FileResult DownloadFile(int id)
        {
            var file = db.DocumentoGestionHumana.Find(id);

            return File(file.DocGHArchivo, file.DocGHExtension, file.DocGHArchivoNombre);
        }

        private string IconoImagen(DocumentoGestionHumana documentoGestionHumana) {

            string image64 = Convert.ToBase64String(documentoGestionHumana.DocGHArchivo);

            if (documentoGestionHumana.DocGHExtension.ToLower() == ".jpg" || documentoGestionHumana.DocGHExtension.ToLower() == ".jpeg")
            {
                return string.Format("data:image/jpg;base64,{0}", image64);
            }
            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".png")
            {
                return string.Format("data:image/png;base64,{0}", image64);
            }
            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".gif")
            {
                return string.Format("data:image/gif;base64,{0}", image64);
            }
            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".pdf")
            {
                return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/pdf.png";
            }
            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".doc" || documentoGestionHumana.DocGHExtension.ToLower() == ".docx")
            {
                return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/doc.png";
            }
            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".xls" || documentoGestionHumana.DocGHExtension.ToLower() == ".xlsx")
            {
                return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";
            }
            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".ppt" || documentoGestionHumana.DocGHExtension.ToLower() == ".pptx")
            {
                return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/ppt.png";
            }
            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".txt")
            {
                return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/txt.png";
            }
            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".zip")
            {
                return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/zip.png";
            }
            else
            {
                return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/file.png";
            }
        }

        private bool VerificarCarpeta(int? id) {
            if (db.CarpetasDocumentosGH.Find(id) != null) { return true; }
            return false;
        }

        public ActionResult Excel(int documentoId)
        {
            var documento = db.DocumentoGestionHumana.FirstOrDefault(d => d.DocGHId == documentoId);
            Response.Clear();
            MemoryStream ms = new MemoryStream(documento.DocGHArchivo);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", "attachment;filename=" + documento.DocGHArchivoNombre);
            Response.Buffer = true;
            ms.WriteTo(Response.OutputStream);
            Response.End();
            return new FileStreamResult(ms, "application/vnd.ms-excel");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Commented Code

//        string image64 = Convert.ToBase64String(documentoGestionHumana.DocGHArchivo);

//            if (documentoGestionHumana.DocGHExtension.ToLower() == ".jpg" || documentoGestionHumana.DocGHExtension.ToLower() == ".jpeg")
//            {
//                ViewBag.IconImage = string.Format("data:image/jpg;base64,{0}", image64);
//    }
//            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".png")
//            {
//                ViewBag.IconImage = string.Format("data:image/png;base64,{0}", image64);
//}
//            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".gif")
//            {
//                ViewBag.IconImage = string.Format("data:image/gif;base64,{0}", image64);
//            }
//            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".pdf")
//            {
//                ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/pdf.png";
//            }
//            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".doc" || documentoGestionHumana.DocGHExtension.ToLower() == ".docx")
//            {
//                ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/doc.png";
//            }
//            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".xls" || documentoGestionHumana.DocGHExtension.ToLower() == ".xlsx")
//            {
//                ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";
//            }
//            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".ppt" || documentoGestionHumana.DocGHExtension.ToLower() == ".pptx")
//            {
//                ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/ppt.png";
//            }
//            else if (documentoGestionHumana.DocGHExtension.ToLower() == ".txt")
//            {
//                ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/txt.png";
//            }
//            else
//            {
//                ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/file.png";
//            }

        #endregion

    }
}
