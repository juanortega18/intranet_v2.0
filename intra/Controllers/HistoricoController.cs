﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;

namespace intra.Controllers
{
    public class HistoricoController : Controller
    {

        private SolicitudModel smodel = new SolicitudModel();

        private dbIntranet obj = new dbIntranet();

        private Correo ObjCorreo = new Correo();


        public void MostrarHistorial(int codigosolicitud)
        {

            smodel.TB_Mostrar_Logs = obj.Sol_Mostrar_Log.ToList().Where(cd => cd.CodigoSolicitud == codigosolicitud).ToList();

        }


        // GET: Historico
        [CustomAuthorize]
        [HttpGet]
        public ActionResult Historico()
        {

            if (Session["CodigoSolicitudHistorico"] != null)
            {

                int Cdg = 0;

                Cdg = int.Parse(Session["CodigoSolicitudHistorico"].ToString());


                MostrarHistorial(Cdg);
            }
            else
            {
                return RedirectToAction("RegistroSolicitud", "RegistroSolicitud");
            }

            return View(smodel);
        }

        [HttpPost]
        public ActionResult Volver(FormCollection Fc)
        {
            if (Fc["Volver"] != null)
            {


                return RedirectToAction("RegistroSolicitud", "RegistroSolicitud", new {Id = int.Parse(Session["CodigoSolicitudHistorico"].ToString()) });


            }


            return View(smodel);
        }
    }
}