﻿using intra.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models.FlotasExtensiones;
namespace intra.Controllers
{
    public class FlotaExtensionEmpleadoController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: FlotaExtensionEmpleado
        public ActionResult Index()
        {
            return View();
        }

        // GET: FlotaExtensionEmpleado/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: FlotaExtensionEmpleado/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FlotaExtensionEmpleado/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: FlotaExtensionEmpleado/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var flotaextensionempleado = db.FlotaExtensionEmpleado.Find(id);

                var employee_info = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == flotaextensionempleado.FlotExtEmpEmpleadoId.ToString()).FirstOrDefault();

                ViewBag.EmployeeName = employee_info.nombre;
                ViewBag.EmployeeCharge = employee_info.cargo;
                ViewBag.EmployeeDepartment = employee_info.departamento;
                ViewBag.FlotaConfirmed = flotaextensionempleado.FlotExtEmpDatosConfirmados.Value.ToString();

                return View(flotaextensionempleado);
            }catch(Exception)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // POST: FlotaExtensionEmpleado/Edit/5
        [HttpPost]
        public ActionResult Edit(FlotaExtensionEmpleado view_object)
        {
            try
            {
                // TODO: Add update logic here
                if(!ModelState.IsValid)
                {
                    return View(view_object);
                }

                view_object.FlotExtEmpDatosConfirmados = true;

                db.Entry(view_object).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index","Home");
            }
            catch
            {
                return View(view_object);
            }
        }

        // GET: FlotaExtensionEmpleado/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: FlotaExtensionEmpleado/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
