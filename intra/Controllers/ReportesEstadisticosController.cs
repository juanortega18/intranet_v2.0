﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Rotativa;
using Rotativa.Options;
using System.Web.Mvc;
using intra.Utilidades;

namespace intra.Controllers
{
    public class ReportesEstadisticosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: ReportesEstadisticos
        public ActionResult Index()
        {
           
            return View();
        }

        [CustomAuthorize]
        public ActionResult reportServUlt7Dias(int departamentoId = 0) {

            //Gráfica de los Servicios de los Ultimos 7 dias//
            var datosCantGrafica1 = UtilidadesEstadistica.servUlt7Dias(db, departamentoId);
            ViewBag.datosCantGrafica1 = datosCantGrafica1;

            return new Rotativa.ViewAsPdf("reportServUlt7Dias") { FileName = "reporteServiciosUltimo7dias.pdf",
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }

            };
        }

        [CustomAuthorize]
        public ActionResult reportTiempoRespuesta(int departamentoId = 0)
        {
            //Gráfica Tiempo Respuesta de los Servicios de los Ultimos 7 dias //
            var datosCantGrafica2 = UtilidadesEstadistica.tiempoRespuesta(db, departamentoId);
            ViewBag.datosCantGrafica2 = datosCantGrafica2;


            return new Rotativa.ViewAsPdf("reportTiempoRespuesta"){FileName = "reporteTiempoRespuesta.pdf",
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = { Left = 1, Right = 1 ,Top = 1, Bottom = 1}

            };
        }

        [CustomAuthorize]
        public ActionResult reportSolicitudesXmes(int departamentoId = 0, int mes=0, int anno = 0)
        {
            //Gráfica de los Servicios mensuales //
            var datosCantGrafica3 = UtilidadesEstadistica.solicitudesXmes(db, departamentoId, mes, anno);
            ViewBag.datosCantGrafica3 = datosCantGrafica3;

            return new Rotativa.ViewAsPdf("reportSolicitudesXmes"){FileName = "reporteSolicititudesPorMes.pdf",
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }

            };

        }

        [CustomAuthorize]
        public ActionResult reportTiempoRespuestaXmes(int departamentoId = 0)
        {
            //Gráfica del Tiempo excedido de los Tecnicos(Mensual)  //
            var datosCantGrafica4 = UtilidadesEstadistica.tiempoRespuestaXmes(db, departamentoId);
            ViewBag.datosCantGrafica4 = datosCantGrafica4;

            return new Rotativa.ViewAsPdf("reportTiempoRespuestaXmes"){FileName = "reporteTiempoRespuestaPorMes.pdf",
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }

            };
        }

        [CustomAuthorize]
        public ActionResult reportAsignaUltimaSemanaAbiertas(int departamentoId = 0)
        {
            //Gráfica de las asignaciones abiertas de los tecnicos de los ultimo 7 dias//
            var datosAsignadGrafica1 = UtilidadesEstadistica.asignaUltimaSemanaAbiertas(db, departamentoId);
            ViewBag.datosAsignadGrafica1 = datosAsignadGrafica1;


            return new Rotativa.ViewAsPdf("reportAsignaUltimaSemanaAbiertas"){FileName = "reporteAsignacionesUltimaSemanaAbiertas.pdf",
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }

            };
        }

        [CustomAuthorize]
        public ActionResult reportAsignaUltimaSemanaCerradas(int departamentoId = 0)
        {

            //Gráfica de las asignaciones cerradas de los tecnicos de los ultimo 7 dias//
            var datosAsignadGrafica2 = UtilidadesEstadistica.asignaUltimaSemanaCerradas(db, departamentoId);
            ViewBag.datosAsignadGrafica2 = datosAsignadGrafica2;


            return new Rotativa.ViewAsPdf("reportAsignaUltimaSemanaCerradas"){FileName = "reporteAsignacionesUltimaSemanaCerradas.pdf",
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }

            };

        }

        [CustomAuthorize]

        public ActionResult reportSolicitudRealizadasXGerencias(int departamentoId = 0, int mes = 0, int anno = 0)
        {

            //Gráfica de las solicitudes realizadas por las gerencias//
            var datosGerenciasGrafica1 = UtilidadesEstadistica.solicitudRealizadasXGerencias(db, departamentoId, anno, mes);
            ViewBag.datosGerenciasGrafica1 = datosGerenciasGrafica1;

            return new Rotativa.ViewAsPdf("reportSolicitudRealizadasXGerencias"){ FileName = "reporteSolicitudesRealizadasPorGerencias.pdf",
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }

            };
        }

        [CustomAuthorize]
        public ActionResult reportSolicitudesRealizadasXUsuarios(int departamentoId = 0, int mes = 0, int anno = 0)
        {
            //Gráfica de las solicitudes realizadas por las usuarios//
            var datosGerenciasGrafica2 = UtilidadesEstadistica.solicitudesRealizadasXUsuarios(db, departamentoId, anno, mes);
            ViewBag.datosGerenciasGrafica2 = datosGerenciasGrafica2;


            return new Rotativa.ViewAsPdf("reportSolicitudesRealizadasXUsuarios"){FileName = "reporteSolicitudesRealizadasPorUsuarios.pdf",
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }

            };

        }

        [CustomAuthorize]
        public ActionResult reportServCantidadHoraUltimaSemana(int departamentoId = 0)
        {
            //Gráfica  de la Cantidad de horas de los ultimos 7 dias //
            var datosValorHGrafica1 = UtilidadesEstadistica.servCantidadHoraUltimaSemana(db, departamentoId);
            ViewBag.datosValorHGrafica1 = datosValorHGrafica1;

            return new Rotativa.ViewAsPdf("reportServCantidadHoraUltimaSemana"){FileName = "reporteServiciosCantidadHoraUltimaSemana.pdf",
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }
            };
        }

        [CustomAuthorize]
        public ActionResult reportTiempoExcedRespuesta(int departamentoId = 0)
        {
            //Gráfica del tiempo excedido de Respuesta por los tecnicos//
            var datosValorHGrafica2 = UtilidadesEstadistica.tiempoExcedRespuesta(db, departamentoId);
            ViewBag.datosValorHGrafica2 = datosValorHGrafica2;

            return new Rotativa.ViewAsPdf("reportTiempoExcedRespuesta")
            {
                FileName = "reporteTiempoExcedidoRespuesta.pdf",
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }
            };

        }

        [CustomAuthorize]
        public ActionResult reportServCantidadHoraMensual(int departamentoId = 0, int mes=0, int anno = 0)
        {
            //Gráfica  de la Cantidad de horas mensuales //

            var datosValorHGrafica3 = UtilidadesEstadistica.servCantidadHoraMensual(db, departamentoId, anno, mes);
            ViewBag.datosValorHGrafica3 = datosValorHGrafica3;

            return new Rotativa.ViewAsPdf("reportServCantidadHoraMensual") {FileName = "reporteServiciosCantidadHoraMensual.pdf",
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }
            };

        }

        [CustomAuthorize]
        public ActionResult reportTiempoExcedMensual(int departamentoId = 0)
        {

            //Gráfica  del tiempo excedido mensual por los tecnicos //
            var datosValorHGrafica4 = UtilidadesEstadistica.tiempoExcedMensual(db, departamentoId);
            ViewBag.datosValorHGrafica4 = datosValorHGrafica4;

            return new Rotativa.ViewAsPdf("reportTiempoExcedMensual"){ FileName = "reporteTiempoExcedidoMensual.pdf",
                PageOrientation = Orientation.Portrait,
                PageSize =  Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }
            };
        
        }

    }

}