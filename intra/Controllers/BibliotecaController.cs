﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
    public class BibliotecaController : Controller
    {
        dbIntranet db = new dbIntranet();
        public ActionResult Index()
        {
            return View(db.SeccionBiblioteca.ToList());
        }

        public ActionResult List(int? id)
        {
            if (id == null)
            {
                ViewBag.Seccion = "Libros Disponibles";
                ViewBag.id = null;
                return View(db.LibroBiblioteca.ToList());
            }
            try
            {
                ViewBag.Seccion = db.SeccionBiblioteca.Find(id).SeccionBibliotecaDescripcion;
                ViewBag.id = id;
                return View(db.LibroBiblioteca.Where(x => x.SeccionBibliotecaId == id).ToList());
            } catch
            {
                return HttpNotFound();
            }
        }

        public ActionResult VerLibro(int? id)
        {
            if (id==null) { return RedirectToAction("Index"); }
            var libro = db.LibroBiblioteca.Find(id);
            if (libro == null) { return RedirectToAction("Index"); }
            ViewBag.pesodellibro = libro.LibroBibliotecaPeso == null ? "Libro Externo" : ToFileSize(Convert.ToInt64(libro.LibroBibliotecaPeso));
            return View(libro);
        }

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public ActionResult AgregarLibro(int? id)
        {
            ViewBag.id = id;
            ViewBag.secciones = new SelectList (db.SeccionBiblioteca.ToList(), "SeccionBibliotecaId", "SeccionBibliotecaDescripcion", id);
            ViewBag.autores = new SelectList(db.AutorLibroBiblioteca.ToList(), "AutorLBId", "AutorLBNombre");
            return View();
        }

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarLibro(LibroBiblioteca libro)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    libro.LibroBibliotecaExterno = Convert.ToBoolean(libro.LibroBibliotecaExterno);
                    libro.LibroBibliotecaFhCreacion = DateTime.Now;

                    if (libro.LibroBibliotecaExterno == true)
                    {
                        libro.LibroBibliotecaPeso = libro.LibroArchivo.ContentLength;
                        var nombre_libro = libro.LibroBibliotecaRuta = Path.GetFileName(libro.LibroArchivo.FileName);
                        var ruta = Path.Combine(HttpRuntime.AppDomainAppPath + "\\Libros\\", nombre_libro);
                        libro.LibroArchivo.SaveAs(ruta);
                    }

                    if (libro.LibroVista != null)
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            libro.LibroVista.InputStream.CopyTo(ms);
                            libro.LibroBibliotecaImagen = ms.GetBuffer();
                        }
                    }
                    db.LibroBiblioteca.Add(libro);
                    db.SaveChanges();
                    return RedirectToAction("List", new { id = libro.SeccionBibliotecaId });
                }
                ViewBag.secciones = new SelectList(db.SeccionBiblioteca.ToList(), "SeccionBibliotecaId", "SeccionBibliotecaDescripcion");
                ViewBag.autores = new SelectList(db.AutorLibroBiblioteca.ToList(), "AutorLBId", "AutorLBNombre");
                return View();
            }
            catch
            {
                TempData["MsjError"] = "Error al Crear Libro";
                ViewBag.secciones = new SelectList(db.SeccionBiblioteca.ToList(), "SeccionBibliotecaId", "SeccionBibliotecaDescripcion");
                ViewBag.autores = new SelectList(db.AutorLibroBiblioteca.ToList(), "AutorLBId", "AutorLBNombre");
                return View();
            }

        }

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public ActionResult EditarLibro(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            LibroBiblioteca libro = db.LibroBiblioteca.Find(id);
            if (libro == null)
            {
                return HttpNotFound();
            }

            ViewBag.pesodellibro = libro.LibroBibliotecaPeso == null ? "Libro Externo" : ToFileSize(Convert.ToInt64(libro.LibroBibliotecaPeso));
            ViewBag.id = libro.SeccionBibliotecaId;
            ViewBag.secciones = new SelectList(db.SeccionBiblioteca.ToList(), "SeccionBibliotecaId", "SeccionBibliotecaDescripcion");
            ViewBag.autores = new SelectList(db.AutorLibroBiblioteca.ToList(), "AutorLBId", "AutorLBNombre");
            ViewBag.ruta = libro.LibroBibliotecaRuta;
            
            libro.LibroBibliotecaRuta = libro.LibroBibliotecaExterno == false ? null : libro.LibroBibliotecaRuta;
            return View(libro);
        }

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarLibro(LibroBiblioteca libro)
        {
            var libroresultado = db.LibroBiblioteca.Find(libro.LibroBibliotecaId);
            if (libroresultado != null)
            {
                if (ModelState.IsValid)
                {
                    if (libro.LibroVista != null)
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            libro.LibroVista.InputStream.CopyTo(ms);
                            libroresultado.LibroBibliotecaImagen = ms.GetBuffer();
                        }
                    }

                    if (libro.LibroBibliotecaExterno == false && libro.LibroArchivo != null)
                    {
                        EliminarArchivoLibro(libroresultado.LibroBibliotecaRuta);
                        libroresultado.LibroBibliotecaPeso = libro.LibroArchivo.ContentLength;
                        var nombre_libro = libroresultado.LibroBibliotecaRuta = Path.GetFileName(libro.LibroArchivo.FileName);
                        var ruta = Path.Combine(HttpRuntime.AppDomainAppPath + "\\Libros\\", nombre_libro);
                        libro.LibroArchivo.SaveAs(ruta);
                        libroresultado.LibroBibliotecaExterno = libro.LibroBibliotecaExterno;
                    }

                    if (libro.LibroBibliotecaExterno == true && libro.LibroBibliotecaRuta != null)
                    {
                        EliminarArchivoLibro(libroresultado.LibroBibliotecaRuta);
                        libroresultado.LibroBibliotecaPeso = null;
                        libroresultado.LibroBibliotecaRuta = libro.LibroBibliotecaRuta;
                        libroresultado.LibroBibliotecaExterno = libro.LibroBibliotecaExterno;
                    }

                    libroresultado.LibroBibliotecaNombre = libro.LibroBibliotecaNombre;
                    libroresultado.SeccionBibliotecaId = libro.SeccionBibliotecaId;
                    libroresultado.AutorLBId = libro.AutorLBId;
                    db.SaveChanges();
                    return RedirectToAction("List", new { id = libroresultado.SeccionBibliotecaId } );
                }
                ViewBag.id = libro.SeccionBibliotecaId;
                ViewBag.secciones = new SelectList(db.SeccionBiblioteca.ToList(), "SeccionBibliotecaId", "SeccionBibliotecaDescripcion");
                ViewBag.autores = new SelectList(db.AutorLibroBiblioteca.ToList(), "AutorLBId", "AutorLBNombre");
                return View(libro);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public JsonResult EliminarLibro(int? id)
        {
            if (id == null)
            {
                return Json("Error");
            }
            LibroBiblioteca libro = db.LibroBiblioteca.Find(id);
            if (libro == null)
            {
                return Json("Error");
            }
            var seccionId = libro.SeccionBibliotecaId;
            EliminarArchivoLibro(libro.LibroBibliotecaRuta);
            db.LibroBiblioteca.Remove(libro);
            db.SaveChanges();
            return Json(seccionId);
        }

        public JsonResult CargarAutores()
        {
            var autores = db.AutorLibroBiblioteca.Select(x => new {
                Value = x.AutorLBId,
                Text = x.AutorLBNombre
            }).ToList();
            return Json(autores);
        }

        private static string ToFileSize(long peso)
        {
            const int byteConversion = 1024;
            double bytes = Convert.ToDouble(peso);

            if (bytes >= Math.Pow(byteConversion, 3)) //GB Rango
            {
                return string.Concat(Math.Round(bytes / Math.Pow(byteConversion, 3), 2), " GB");
            }
            else if (bytes >= Math.Pow(byteConversion, 2)) //MB Rango
            {
                return string.Concat(Math.Round(bytes / Math.Pow(byteConversion, 2), 2), " MB");
            }
            else if (bytes >= byteConversion) //KB Rango
            {
                return string.Concat(Math.Round(bytes / byteConversion, 2), " KB");
            }
            else //Bytes
            {
                return string.Concat(bytes, " Bytes");
            }
        }

        public FileResult DesgarcarImagen(int id)
        {
            var imagen = db.LibroBiblioteca.Find(id);
            return File(imagen.LibroBibliotecaImagen, "image/jpg", imagen.LibroBibliotecaNombre + ".jpg");
            //return File(@"~\\Content\\images\\img_noDisponible.jpg", "image/jpg");
        }

        public FileResult DescargarLibro(int id)
        {
            var libro = db.LibroBiblioteca.Find(id);
            return File(@"~\\Libros\\" + libro.LibroBibliotecaRuta, ".pdf");
            // href='@Url.Action("PhotoDownload")/@item.FotoGHId'
        }

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        private void EliminarArchivoLibro(string ruta) {
            var libro_viejo = Path.Combine(HttpRuntime.AppDomainAppPath + "\\Libros\\", ruta.ToString());
            if (System.IO.File.Exists(libro_viejo)) { System.IO.File.Delete(libro_viejo); }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}