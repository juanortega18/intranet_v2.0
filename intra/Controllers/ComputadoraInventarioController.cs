﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.Inventario;

namespace intra.Controllers
{
    public class ComputadoraInventarioController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: ComputadoraInventario
        [CustomAuthorize]
        [Authorize(Roles = "IEA,IEWR")]
        public ActionResult Index()
        {
            return View(db.ComputadoraInventario.ToList());
        }

        [CustomAuthorize]
        public PartialViewResult vInventario()
        {
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            return PartialView(db.VistaComputadoraInventario.ToList());
        }
       


        [HttpGet]

        public JsonResult GetProvincia(int id)
        {


            List<SelectListItem> listProvincia = new List<SelectListItem>();
            if (id < 1)
            {

                RedirectToAction("Create");

            }

            var Provinces = (from r in db.Provinces
                          from i in db.Region
                          from rp in db.RegionProvincia
                          where i.RegionId == rp.RegionId
                          where r.Province_ID == rp.Province_ID
                          where i.RegionId == id
                          orderby r.Province_Name ascending
                          select new
                          {
                              r.Province_Name,
                              r.Province_ID

                          }).ToList();

            foreach (var Province in Provinces)
            {
                listProvincia.Add(new SelectListItem { Text = Province.Province_Name.ToString(), Value = Province.Province_ID.ToString() });
                
            }

            return Json(new SelectList(listProvincia, "value", "Text"), JsonRequestBehavior.AllowGet);
        }

        //Get Municipio
        public JsonResult Municipio(string Provincia)
        {
            List<SelectListItem> Municipios;

            int cl = int.Parse(Provincia);
            Municipios = db.Municipalities.Where(x => x.Province_ID == cl).Select(x => new SelectListItem() { Text = x.Municipality_Name, Value = x.Municipality_ID.ToString()}).ToList();
            

            return Json(new SelectList(Municipios, "Value", "Text"));
        }

        [CustomAuthorize]
        public ActionResult Localidad(string Municipio)
        {
            var muni = int.Parse(Municipio);
            var Localidad = new List<SelectListItem>();
            var localidad = db.Localidad.Where(x => x.Municipality_ID == muni).Select(x => new SelectListItem() { Text = x.LocalidadNombre, Value = x.LocalidadId.ToString() }).ToList();
            return Json(new SelectList(localidad, "Value", "Text"),JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        // GET: ComputadoraInventario/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VistaComputadoraInventario computadoraInventario = db.VistaComputadoraInventario.Find(id);
            var calc = DiferenciaFechas(computadoraInventario.ComputadoraGarantia, computadoraInventario.ComputadoraFechaCompra);
            ViewBag.calc = calc;
            Console.WriteLine(calc);     
            if (computadoraInventario == null)
            {
                return HttpNotFound();
            }
            return View(computadoraInventario);
        }

        // GET: ComputadoraInventario/Create
        [CustomAuthorize]
        [Authorize(Roles = "IEA,IEWR")]
        public ActionResult Create()
        {
            ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            ViewBag.Modelo = new SelectList(db.ModeloInventario.ToList(), "ModeloId", "ModeloDescripcion");
            ViewBag.Proveedor = new SelectList(db.ProveedorInventario.ToList(), "ProveedorId", "ProveedorNombre");
            ViewBag.Estatus = new SelectList(db.EstatusInventario.ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");

            return View();
        }

        // POST: ComputadoraInventario/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "IEA,IEWR")]
        public ActionResult Create([Bind(Include = "ComputadoraId,RegionId,Province_ID,Municipality_ID,ComputadoraSerie,ComputadoraServiceTag,ComputadoraGarantia,ComputadoraFechaCompra,ComputadoraFechaCreación,ComputadoraUsuario,MarcaId,ModeloId,EstatusId,CaracteristicaId,LocalidadId,ProveedorId,ComputadoraNombreRed,LocalidadId")] FormCollection gr,  ComputadoraInventario computadoraInventario)
        {
            //string fecha = DateTime.Now.Date.ToString();

            ////string compra = computadoraInventario.ComputadoraFechaCompra.ToString("dd/MM/yyyy");
            var compra = gr["ComputadoraFechaCompra"];
            var garantia = gr["ComputadoraGarantia"];
            computadoraInventario.ComputadoraFechaCompra = DateTime.ParseExact(compra,"dd/MM/yyyy",null);
            computadoraInventario.ComputadoraGarantia = DateTime.ParseExact(garantia, "dd/MM/yyyy", null);
            //TimeSpan difference = DateTime.Parse(fecha.ToString()) - DateTime.Parse(compra.ToString());
            ////difference.TotalDays
            //var days = difference.Days;

            //computadoraInventario.ComputadoraGarantia = days.ToString();
            computadoraInventario.ComputadoraFechaCreación = DateTime.Now;
            computadoraInventario.ComputadoraUsuario = Session["usuario"].ToString();
           
            if (!ModelState.IsValid)
            {
                db.ComputadoraInventario.Add(computadoraInventario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var error = ModelState.Values.SelectMany(v => v.Errors);
            ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            ViewBag.Modelo = new SelectList(db.ModeloInventario.ToList(), "ModeloId", "ModeloDescripcion");
            ViewBag.Proveedor = new SelectList(db.ProveedorInventario.ToList(), "ProveedorId", "ProveedorNombre");
            ViewBag.Estatus = new SelectList(db.EstatusInventario.ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            return View(computadoraInventario);
        }
        public JsonResult Modelo(string marca)
        {
            List<SelectListItem> modelos;

            int cl = int.Parse(marca);
            modelos = db.ModeloInventario.Where(x => x.MarcaId == cl).Select(x => new SelectListItem() { Text = x.ModeloDescripcion, Value = x.ModeloId.ToString() }).ToList();

            return Json(new SelectList(modelos, "Value", "Text"));
        }

        // GET: ComputadoraInventario/Edit/5
        [CustomAuthorize]
        [Authorize(Roles = "IEA,IEWR")]
        public ActionResult Edit(int? id)
        {
            ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            ViewBag.Modelo = new SelectList(db.ModeloInventario.ToList(), "ModeloId", "ModeloDescripcion");
            ViewBag.Proveedor = new SelectList(db.ProveedorInventario.ToList(), "ProveedorId", "ProveedorNombre");
            ViewBag.Estatus = new SelectList(db.EstatusInventario.ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ComputadoraInventario computadoraInventario = db.ComputadoraInventario.Find(id);
            //var a = computadoraInventario.ComputadoraFechaCompra.ToString("dd/MM/yy");
            //computadoraInventario.ComputadoraFechaCompra = DateTime.Parse(a);
            //computadoraInventario.ComputadoraGarantia = computadoraInventario.ComputadoraGarantia.Date;
            if (computadoraInventario == null)
            {
                return HttpNotFound();
            }
            return View(computadoraInventario);
        }

        // POST: ComputadoraInventario/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "IEA,IEWR")]
        public ActionResult Edit([Bind(Include = "ComputadoraId,RegionId,Province_ID,ComputadoraSerie,ComputadoraServiceTag,ComputadoraGarantia,ComputadoraFechaCompra,ComputadoraFechaCreación,ComputadoraUsuario,MarcaId,ModeloId,CaracteristicaComputadora,EstatusId,CaracteristicaId,LocalidadId,ProveedorId,ComputadoraNombreRed,Municipality_ID")] ComputadoraInventario computadoraInventario)
        {

            ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            ViewBag.Modelo = new SelectList(db.ModeloInventario.ToList(), "ModeloId", "ModeloDescripcion");
            ViewBag.Proveedor = new SelectList(db.ProveedorInventario.ToList(), "ProveedorId", "ProveedorNombre");
            ViewBag.Estatus = new SelectList(db.EstatusInventario.ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            DateTime da = DateTime.Now;
           // DateTime dn = new DateTime(1977, 10, 10);
            DateTime dn = computadoraInventario.ComputadoraFechaCompra;

            computadoraInventario.ComputadoraFechaCreación = DateTime.Now;
            computadoraInventario.ComputadoraUsuario = Session["usuario"].ToString();
            if (ModelState.IsValid)
            {
                db.Entry(computadoraInventario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }



            return View(computadoraInventario);
        }

        [CustomAuthorize]
        // GET: ComputadoraInventario/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VistaComputadoraInventario vistaComputadoraInventario = db.VistaComputadoraInventario.Find(id);
            if (vistaComputadoraInventario == null)
            {
                return HttpNotFound();
            }
            return View(vistaComputadoraInventario);
        }

        // POST: ComputadoraInventario/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            ComputadoraInventario computadoraInventario = db.ComputadoraInventario.Find(id);
            db.ComputadoraInventario.Remove(computadoraInventario);
            db.SaveChanges();
            return RedirectToAction("Index","ComputadoraInventario");
        }
        private String DiferenciaFechas(DateTime newdt, DateTime olddt)
        {
            Int32 anios;
            Int32 meses;
            Int32 dias;
            String str = "";

            anios = (newdt.Year - olddt.Year);
            meses = (newdt.Month - olddt.Month);
            dias = (newdt.Day - olddt.Day);

            if (meses < 0)
            {
                anios -= 1;
                meses += 12;
            }
            if (dias < 0)
            {
                meses -= 1;
                dias += DateTime.DaysInMonth(newdt.Year, newdt.Month);
            }

            if (anios < 0)
            {
                return "Fecha Invalida";
            }
            if (anios > 0)
                str = str + anios.ToString() + " años ";
            if (meses > 0)
                str = str + meses.ToString() + " meses ";
            if (dias > 0)
                str = str + dias.ToString() + " dias ";

            return str;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
