﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;

namespace intra.Controllers
{
    public class ScanCheckController : Controller
    {

        const string STR_BREAK = "_8r34K_";
        const string SEPARATOR = "_s3p_";

        [CustomAuthorize]
        // Page to add a new document
        public ActionResult Index()
        {
            if (Session["departamento"] == null)
                return Redirect("~/login");

            //ViewBag.url = Request.Url;

            return View();
        }

        // Page post method to upload the document
        [HttpPost]
        public ActionResult Index(string reason, string comment, string Lugar, string Emisor, params HttpPostedFileBase[] getFiles)
        {
            if (Session["departamento"] == null)
                return Redirect("~/login");

            if (getFiles == null || getFiles.Length == 0 || comment == null)
                return View();

            //string ruta = System.IO.Path.Combine(HttpRuntime.AppDomainAppPath, "/Content/Scans/");
            string ruta = HttpRuntime.AppDomainAppPath + "\\Content\\Scans\\";
            //var folderPath = Server.MapPath("~/Scans/");

            List<string> fileNames = new List<string>();

            //if (!System.IO.Directory.Exists(folderPath))
            //    System.IO.Directory.CreateDirectory(folderPath);

            // Set files' names
            foreach (var a in getFiles)
            {
                var baseName = a.FileName.Substring(0, a.FileName.LastIndexOf("."));
                var fileExt = a.FileName.Substring(baseName.Length);
                var nameHolder = "{0}{1}({2}){3}";
                string tempFileName = null;

                if (System.IO.File.Exists(tempFileName = ruta + baseName + fileExt))
                    for (int x = 0; ; x++)
                    {
                        if (!System.IO.File.Exists(tempFileName = string.Format(nameHolder, ruta, baseName, x, fileExt)))
                        {
                            fileNames.Add(tempFileName);
                            break;
                        }
                    }
                else
                    fileNames.Add(tempFileName);
            }

            // Save files
            for(int x = 0; x < getFiles.Length; x++)
            {
                // Save files in the server
                try { getFiles[x].SaveAs(fileNames.ElementAt(x)); }
                catch (Exception ex)
                {
                    // Delete all the saved files (RollBack)
                    for(int y = x; y >= 0; y--)
                        if (System.IO.File.Exists(fileNames.ElementAt(y)))
                            System.IO.File.Delete(fileNames.ElementAt(y));

                    ViewBag.Error = "Ha ocurrido un problema al intentar guardar los datos en el servidor";
                    return View();
                }
            }

            // Save the data to the database
            dbIntranet db = new dbIntranet();

            var idUsuario = Convert.ToInt32(Session["empleadoId"]);

            var idDpto = Convert.ToInt32((from t in db.VISTA_EMPLEADOS
                         where t.empleadoid == idUsuario.ToString()
                         select t.departamentoid).First());

            var newRegister = new EscanerRegistro() {
                EmpleadoId = idUsuario,
                EmpleadoNombre = Session["nombre"].ToString(),
                EscanerRegistroFechaPublicacion = DateTime.Now,
                DepartamentoId = idDpto,
                EscanerRegistroDetalle = comment,
                EscanerRegistroRazon = reason,
                EscanerLugarProcedencia = Lugar,
                EscanerEmisor = Emisor
            };

            db.EscanerRegistro.Add(newRegister);
            db.SaveChanges();

            var lastId = (from t in db.EscanerRegistro
                          where t.EmpleadoId == idUsuario
                          orderby t.EscanerRegistroId descending
                          select t.EscanerRegistroId).First();

            foreach(var a in fileNames)
            {
                var registerPaths = new EscanerArchivos()
                {
                    EscanerRegistroId = lastId,
                    EscanerArchivosRuta = a
                };

                db.EscanerArchivos.Add(registerPaths);
            }

            db.SaveChanges();

            //TempData["Success"] = "El registro de documentos se ha realizado correctamente";
            //return RedirectToAction("index");

            return RedirectToAction("viewscan", new { document = lastId });
        }

        [CustomAuthorize]
        // Page to see all your linked documents
        public ActionResult LinkedDocs()
        {
            if (Session["departamento"] == null)
                return Redirect("~/login");

            dbIntranet db = new dbIntranet();

            var idEmpleado = Convert.ToInt32(Session["empleadoId"]);

            var result = from t in db.EscanerRegistro
                         where t.EmpleadoId == idEmpleado
                         select new { t.EscanerRegistroId, t.EscanerRegistroRazon, t.EscanerRegistroFechaPublicacion, t.EmpleadoNombre };

            List<Models.ScanLinkedDocs> linkedDocs = new List<Models.ScanLinkedDocs>();

            foreach (var a in result)
                linkedDocs.Add(new Models.ScanLinkedDocs
                {
                    ScanRegisterId = a.EscanerRegistroId,
                    NombreUsuario = a.EmpleadoNombre,
                    FechaPublicacion = string.Format(
                        "{0:00}/{1:00}/{2:0000}",
                        a.EscanerRegistroFechaPublicacion.Day,
                        a.EscanerRegistroFechaPublicacion.Month,
                        a.EscanerRegistroFechaPublicacion.Year),
                    Razon = a.EscanerRegistroRazon
                });

            result = from t in db.EscanerDocumentoVinculado
                     join t2 in db.EscanerRegistro
                     on t.EscanerRegistroId equals t2.EscanerRegistroId
                     where t.EmpleadoId == idEmpleado
                     select new { t.EscanerRegistroId, t2.EscanerRegistroRazon, t2.EscanerRegistroFechaPublicacion, t2.EmpleadoNombre };

            foreach(var a in result)
                linkedDocs.Add(new Models.ScanLinkedDocs
                {
                    ScanRegisterId = a.EscanerRegistroId,
                    NombreUsuario = a.EmpleadoNombre,
                    FechaPublicacion = string.Format(
                        "{0:00}/{1:00}/{2:0000}",
                        a.EscanerRegistroFechaPublicacion.Day,
                        a.EscanerRegistroFechaPublicacion.Month,
                        a.EscanerRegistroFechaPublicacion.Year),
                    Razon = a.EscanerRegistroRazon
                });

            return View(linkedDocs);
        }

        [CustomAuthorize]
        public ActionResult ListadoEscaner()
        {
            if (Session["departamento"] == null)
                return Redirect("~/login");

            dbIntranet db = new dbIntranet();

            var idEmpleado = Convert.ToInt32(Session["empleadoId"]);

            var result = from t in db.EscanerRegistro
                         where t.EmpleadoId == idEmpleado
                         select new { t.EscanerRegistroId, t.EscanerRegistroRazon, t.EscanerRegistroFechaPublicacion, t.EmpleadoNombre };

            List<Models.ScanLinkedDocs> linkedDocs = new List<Models.ScanLinkedDocs>();

            foreach (var a in result)
                linkedDocs.Add(new Models.ScanLinkedDocs
                {
                    ScanRegisterId = a.EscanerRegistroId,
                    NombreUsuario = a.EmpleadoNombre,
                    FechaPublicacion = string.Format(
                        "{0:00}/{1:00}/{2:0000}",
                        a.EscanerRegistroFechaPublicacion.Day,
                        a.EscanerRegistroFechaPublicacion.Month,
                        a.EscanerRegistroFechaPublicacion.Year),
                    Razon = a.EscanerRegistroRazon
                });

            result = from t in db.EscanerDocumentoVinculado
                     join t2 in db.EscanerRegistro
                     on t.EscanerRegistroId equals t2.EscanerRegistroId
                     where t.EmpleadoId == idEmpleado
                     select new { t.EscanerRegistroId, t2.EscanerRegistroRazon, t2.EscanerRegistroFechaPublicacion, t2.EmpleadoNombre };

            foreach (var a in result)
                linkedDocs.Add(new Models.ScanLinkedDocs
                {
                    ScanRegisterId = a.EscanerRegistroId,
                    NombreUsuario = a.EmpleadoNombre,
                    FechaPublicacion = string.Format(
                        "{0:00}/{1:00}/{2:0000}",
                        a.EscanerRegistroFechaPublicacion.Day,
                        a.EscanerRegistroFechaPublicacion.Month,
                        a.EscanerRegistroFechaPublicacion.Year),
                    Razon = a.EscanerRegistroRazon
                });

            return View(db.EscanerRegistro.ToList());
        }

        [CustomAuthorize]
        // Page to see an specific document
        public ActionResult ViewScan(string document)
        {
            if (Session["departamento"] == null)
                return Redirect("~/login");

            if (document == null)
                return RedirectToAction("linkeddocs");

            var idEmpleado = Convert.ToInt32(Session["empleadoId"]);
            int idScan;

            if(!int.TryParse(document, out idScan))
                return RedirectToAction("linkeddocs");

            string ownerAvatar = "temp_val";
            dbIntranet db = new dbIntranet();

            // If the logged user is the "author" of the document:
            var result = (from t in db.EscanerRegistro
                          join t2 in db.EscanerArchivos
                          on t.EscanerRegistroId equals t2.EscanerRegistroId
                          where t.EscanerRegistroId == idScan && t.EmpleadoId == idEmpleado
                          select new
                          {
                              t.EscanerRegistroId,
                              t.DepartamentoId,
                              t.EmpleadoId,
                              t.EscanerRegistroRazon,
                              t.EmpleadoNombre,
                              t.EscanerRegistroFechaPublicacion,
                              t.EscanerRegistroDetalle,
                              t.EscanerLugarProcedencia,
                              t.EscanerEmisor
                          }).FirstOrDefault();

            if(result == null)
            {
                // If the logged user is a member of the discussion forum for the document:
                result = (from t in db.EscanerDocumentoVinculado
                          join t2 in db.EscanerRegistro
                          on t.EscanerRegistroId equals t2.EscanerRegistroId
                          join t3 in db.EscanerArchivos
                          on t.EscanerRegistroId equals t3.EscanerRegistroId
                          where t.EmpleadoId == idEmpleado && t.EscanerRegistroId == idScan
                          select new
                          {
                              t.EscanerRegistroId,
                              t2.DepartamentoId,
                              t2.EmpleadoId,
                              t2.EscanerRegistroRazon,
                              t2.EmpleadoNombre,
                              t2.EscanerRegistroFechaPublicacion,
                              t2.EscanerRegistroDetalle,
                              t2.EscanerLugarProcedencia,
                              t2.EscanerEmisor
                          }).FirstOrDefault();

                // If neither of the above: return back to:
                if (result == null)
                    return RedirectToAction("linkeddocs");
                // The owner of the document is not the current user
                else
                    ownerAvatar = (from t in db.MiPerfilAvatars
                                   where t.codigoEmpleado == result.EmpleadoId.ToString()
                                   select t.avatar).FirstOrDefault();
            }

            //var viewDocs = new List<Models.ScanViewDocs>();
            var linkedUsers = new List<Models.ScanEmpleados>();
            var scanChat = new List<Models.ScanChatModel>();
            var filesPaths = new List<string[]>();


            var users = from t in db.EscanerDocumentoVinculado
                        where t.EscanerRegistroId == idScan && t.EscanerDocumentoVinculadoEstado == 1
                        select new { t.EmpleadoId, t.EmpleadoNombre };

            var userAvatar = (from t in db.MiPerfilAvatars
                              where t.codigoEmpleado == idEmpleado.ToString()
                              select t.avatar).FirstOrDefault();

            // Consider that the owner of the document is the current user
            if (ownerAvatar == "temp_val")
                ownerAvatar = userAvatar;

            var dpto = (from t in db.VISTA_EMPLEADOS
                        where t.departamentoid == result.DepartamentoId.ToString() && t.empleadoid == idEmpleado.ToString()
                        select t.departamento).First();

            //var chat = from t in db.ScanChat
            //           join t2 in db.MiPerfilAvatars
            //           on t.EmpleadoId.ToString() equals t2.codigoEmpleado
            //           where t.ScanRegisterId == idScan
            //           select new { t.EmpleadoId, t.Usuario, t.Descripcion, t.Archivo, t.FechaCreacion, t2.avatar };

            //var chat = from t in db.ScanChat
            //           from t2 in db.MiPerfilAvatars
            //           where t.ScanRegisterId == idScan
            //           select new { t.Archivo, t.Descripcion, t.FechaCreacion, t.EmpleadoId, t.Usuario, t2.avatar };

            var chat = db.EscanerChat
                .Where((obj) => obj.EscanerRegistroId == idScan && obj.EscanerChatEstado == 1)
                .Select((obj) => new { obj.EscanerChatArchivo, obj.EscanerChatDescripcion, obj.EscanerChatFechaCreacion, obj.EmpleadoId, obj.EmpleadoNombre,
                    avatar = (
                                db.MiPerfilAvatars
                                .Where((obj2) => obj2.codigoEmpleado == obj.EmpleadoId.ToString())
                                .Select((obj2) => obj2.avatar).FirstOrDefault()
                             )
                });

            var files = from t in db.EscanerArchivos
                        where t.EscanerRegistroId == idScan
                        select t.EscanerArchivosRuta;

            foreach (var a in users)
                linkedUsers.Add(new Models.ScanEmpleados { IdEmpleado = a.EmpleadoId, NombreEmpleado = a.EmpleadoNombre });

            foreach (var a in chat)
                scanChat.Add(new Models.ScanChatModel
                {
                    Archivo = (a.EscanerChatArchivo == null ? null: a.EscanerChatArchivo.Replace(Server.MapPath("~/"), "").Replace("\\", "/")),
                    Descripcion = a.EscanerChatDescripcion,
                    FechaCreacion = a.EscanerChatFechaCreacion.ToLocalTime(),
                    IdUsuario = a.EmpleadoId,
                    Usuario = a.EmpleadoNombre,
                    Avatar = a.avatar
                });

            foreach (var a in files)
            {
                var tempStr = Server.MapPath("~/");

                filesPaths.Add(new string[]
                {
                    // 0 -> File path
                    a.Replace(tempStr, "").Replace("\\", "/"),
                    // 1 -> File name
                    a.Substring(a.LastIndexOf("\\") + 1),
                    // 2 -> File extension
                    (a.Contains(".") ? a.Substring(a.LastIndexOf(".")): "")
                });
            }

            return View(new Models.ScanViewModel
            {
                //linkedUsers = linkedUsers,
                viewDoc = new Models.ScanViewDocs
                {
                    IdUsuario = result.EmpleadoId,
                    Detalle = result.EscanerRegistroDetalle,
                    FechaPublicacion = string.Format(
                        "{0:00}/{1:00}/{2:0000}",
                        result.EscanerRegistroFechaPublicacion.Day,
                        result.EscanerRegistroFechaPublicacion.Month,
                        result.EscanerRegistroFechaPublicacion.Year),
                    IdDepartamento = result.DepartamentoId,
                    NombreDepartamento = dpto,
                    NombreUsuario = result.EmpleadoNombre,
                    Razon = result.EscanerRegistroRazon,
                    ScanRegisterId = result.EscanerRegistroId,
                    Avatar = userAvatar,
                    OwnerAvatar = ownerAvatar,
                    LugarProcedencia = result.EscanerLugarProcedencia,
                    EscanEmisor = result.EscanerEmisor
                },
                scanChat = scanChat,
                RutasArchivos = filesPaths,
                LinkedUsers = linkedUsers
            });
        }

        // Web method (for ajax) to send a message through the chat
        [HttpPost]
        [System.Web.Services.WebMethod]
        public bool ViewScan(string cp, string msg, object file)
        {
            //return true;
            try
            {
                var documentId = Convert.ToInt32(cp);
                // Clean any html special character
                msg = Server.HtmlEncode(msg);

                // Variable to know wether "file" returns a string
                bool isResultString = false;
                // Variable to use in case of "file" returning an actual file
                HttpPostedFileBase resultFile = null;

                var idUsuario = Convert.ToInt32(Session["empleadoId"]);
                var userName = Session["nombre"].ToString();

                string ruta = HttpRuntime.AppDomainAppPath + "\\Content\\Scans\\";

                //string folderPath = Server.MapPath("~/ScanChatFiles/");
                string filePath = null;

                //if (!System.IO.Directory.Exists(folderPath))
                //    System.IO.Directory.CreateDirectory(folderPath);

                // Get wether there is a file attached or not //
                // If "file" is not of type "string":
                if (!(isResultString = file.GetType().ToString().Contains("String")))
                {
                    // Cast value to "HttpPostedFileBase" when there is a file attached
                    resultFile = ((HttpPostedFileBase[])file)[0];
                    
                    string fileName =
                        // Set a specific notation to avoid name repetitions
                        idUsuario + "_" + DateTime.Now.ToUniversalTime().ToString().Replace(" ", "_").Replace(":", SEPARATOR).Replace("/", "-") +
                        // Set a special group of characters as a break for the string
                        STR_BREAK +
                        resultFile.FileName;

                    filePath = ruta + fileName;
                    
                    resultFile.SaveAs(filePath);
                }

                dbIntranet db = new dbIntranet();

                db.EscanerChat.Add(new EscanerChat {
                    EscanerChatArchivo = filePath,
                    EscanerChatDescripcion = msg,
                    EmpleadoId = idUsuario,
                    EscanerChatEstado = 1,
                    EscanerChatFechaCreacion = DateTime.Now.ToUniversalTime(),
                    EscanerRegistroId = documentId,
                    EmpleadoNombre = userName
                });

                db.SaveChanges();
                
                return true;
            }
            catch// (Exception ex)
            {
                return false;
            }
        }

        // Mehod to create a report (Eddy)
        [HttpPost]
        public ActionResult CreateReport(string values)
        {
            if (Session["departamento"] == null)
                return Redirect("~/login");

            // Temporal return
            return RedirectToAction("index");
        }

        // Web method (for ajax) to get all the employees working on this building (dependenciaid = 10001)
        [System.Web.Services.WebMethod]
        [HttpPost]
        public string GetInfo()
        {
            if (Session["departamento"] == null)
                return Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { });

            try
            {
                var db = new dbIntranet();

                var result = from t in db.VISTA_EMPLEADOS
                             // 10001 -> PGR (edificio nuevo)
                             where t.dependenciaid == "10001"
                             select t;

                var lista = new List<Models.ScanListaEmpleados>();

                foreach (var a in result)
                    lista.Add(new Models.ScanListaEmpleados
                    {
                        label = a.nombre,
                        value = a.departamento,
                        code = a.empleadoid
                    });

                return Newtonsoft.Json.JsonConvert.SerializeObject(lista);
            }
            catch {
                return Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { });
            }
        }

        // Web method (for ajax) to link a person with a document
        [System.Web.Services.WebMethod]
        [HttpPost]
        public bool AddPerson(string info, string cp)
        {
            if (Session["departamento"] == null)
                return false;

            try
            {
                var idUsuario = Convert.ToInt32(info);
                var idScan = Convert.ToInt32(cp);

                var db = new dbIntranet();

                var nombreUsuario = (from t in db.VISTA_EMPLEADOS
                                     // 10001 -> PGR (edificio nuevo)
                                     where t.empleadoid == idUsuario.ToString() && t.dependenciaid == "10001"
                                     select t.nombre).FirstOrDefault();

                // This means the specified ID doesn't match the requirements to be added
                if (nombreUsuario == null)
                    return false;

                var result = (from t in db.EscanerDocumentoVinculado
                              where t.EmpleadoId == idUsuario && t.EscanerRegistroId == idScan && t.EscanerDocumentoVinculadoEstado == 1
                              select "").FirstOrDefault();

                // This means the specified ID is linked to this document already
                if (result == "")
                    return false;

                result = (from t in db.EscanerRegistro
                          where t.EmpleadoId == idUsuario && t.EscanerRegistroId == idScan
                          select "").FirstOrDefault();

                // This means the specified ID is the owner of the document
                if (result == "")
                    return false;

                // Check if there is a "disabled link" (Estado == 0)
                var savedRegistry = from t in db.EscanerDocumentoVinculado
                                    where t.EscanerRegistroId == idScan && t.EmpleadoId == idUsuario && t.EscanerDocumentoVinculadoEstado == 0
                                    select t;

                if(savedRegistry != null && savedRegistry.Count() > 0)
                {
                    // This is supposed to run only once
                    foreach (var a in savedRegistry)
                        a.EscanerDocumentoVinculadoEstado = 1;

                    db.SaveChanges();

                    return true;
                }

                db.EscanerDocumentoVinculado.Add(new EscanerDocumentoVinculado
                {
                    EmpleadoId = idUsuario,
                    EscanerRegistroId = idScan,
                    EmpleadoNombre = nombreUsuario
                });

                db.SaveChanges();


                //var myHolders = new Code.UtilityMethods.MailPlaceHolders
                //{
                //    Departamento = Session["departamento"].ToString(), LogoImagenUrl
                //};

                //Code.UtilityMethods.SendMail("Se te ha vinculado a un nuevo documento", )

                return true;
            }
            catch {
                return false;
            }
        }

        // Web method (for ajax) to "remove" a person from a a linked document
        [HttpPost]
        [System.Web.Services.WebMethod]
        public bool EliminarRegistro(string info, string cp)
        {
            if (Session["departamento"] == null)
                return false;

            try
            {
                var idUsuario = Convert.ToInt32(info);
                var idScan = Convert.ToInt32(cp);

                var db = new dbIntranet();

                var result = (from t in db.VISTA_EMPLEADOS
                              // 10001 -> PGR (edificio nuevo)
                              where t.empleadoid == idUsuario.ToString() && t.dependenciaid == "10001"
                              select "").FirstOrDefault();

                // This means the specified ID doesn't match the requirements to be added
                if (result == null)
                    return false;

                var disableRegistry = from t in db.EscanerDocumentoVinculado
                              where t.EmpleadoId == idUsuario && t.EscanerRegistroId == idScan
                              select t;

                // This is supposed to only run once
                foreach (var a in disableRegistry)
                    a.EscanerDocumentoVinculadoEstado = 0;

                db.SaveChanges();
                
                return true;
            }
            catch {
                return false;
            }
        }

        [CustomAuthorize]
        // Download a file to the client's machine
        public ActionResult Download(string file)
        {
            var filepath = System.IO.Path.Combine(Server.MapPath("~/"), file);

            return File(filepath, MimeMapping.GetMimeMapping(filepath), file);
        }
    }
}