﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models;
using System.IO;
using System.Xml.Linq;
using System.Net;
using Newtonsoft.Json;
using intra.Code;
using intra.Models.GestionHumana;
using intra.Models.Cumpleanos;
using intra.Models.ViewModel;

namespace intra.Controllers
{
    public class HomeController : Controller
    {
        private dbIntranet db = new dbIntranet();


        [AllowAnonymous]
        [CustomAuthorize]
        #region ValidarActividad
        public bool ValidarActividad(int CodigoSupervisor)
        {
            //string idempleado = Session["empleadoid"].ToString();
            //var emp = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == idempleado).FirstOrDefault();
            //string emp = HttpContext.Session["nombre"].ToString();
            //var name = emp.Split(' ');
            //ViewBag.Nombre = name[0];


            bool CdgActividad;


            var Valor = db.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSupervisor).FirstOrDefault();

            if (Valor != null)
            {
                CdgActividad = true;
            }
            else
            {
                CdgActividad = false;
            }


            return CdgActividad;

        }
        #endregion

        #region MostrarUsuarioPorActividad
        public int CodigoActividad(int CodigoSupervisor)
        {
            int CdgActividad = 0;

            var Valor = db.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSupervisor).FirstOrDefault();

            CdgActividad = Valor.CodigoActividad;

            return CdgActividad;

        }

        #endregion

        //[HttpGet]
        //public ActionResult Public()
        //{
        //    return View(db.Noticias.OrderByDescending(c => c.NoticiaId).Take(3).ToList());
        //}

        [CustomAuthorize]
        [HttpGet]
        public ActionResult Index()
        {
            int Cdg = 0;

            if (HttpContext.Session["empleadoId"] != null)
            {


                Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());
                string codigo = HttpContext.Session["empleadoId"].ToString();

                var solicitados = db.Sol_Registro_Solicitud.Where(x => x.SolicitanteId == Cdg && x.EstadoId == 1).ToList();
                var asignados = db.Sol_Registro_Solicitud.Where(x => x.TecnicoId == Cdg && x.EstadoId == 1).ToList();
                Session["CantidadSolicitados"] = solicitados.Count;
                Session["CantidadAsignados"] = asignados.Count;


                //var CantidadSolicitados = db.SSR_Servicios.Where(x => x.CodigoCreador == codigo && x.ServicioEstadoId == 1).ToList().Count();
                //var cantidadAsignados = db.SSR_Servicios.Where(x => x.CodigoResponsable == codigo && x.ServicioEstadoId == 1).ToList().Count();
                //Session["CantidadServicioSolicitadosRepatriados"] = CantidadSolicitados;
                //Session["CantidadServicioAsignadosRepatriados"] = cantidadAsignados;



                MostrarCantidaddeSolicitud(Cdg);

                MostrarCantidadAsignados(Cdg);

                //MostrarCantidadChat(Cdg);

            }
            else
                return Redirect("login");


            //Persona que cumple anios
            Vw_Mostrar_Empleados_Cumpleanos cumple = new Vw_Mostrar_Empleados_Cumpleanos();
            HttpContext.Session["cumple"] = cumple.Dia;

            string EmpCargo = HttpContext.Session["empleadoId"].ToString();
            string emp = HttpContext.Session["nombre"].ToString();

            var name = emp.Split(' ');
            HttpContext.Session["PrimerNombre"] = name[0].ToUpper();

            var empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == EmpCargo).FirstOrDefault();

            HttpContext.Session["Sexo"] = empleados.Sexo;
            TempData["Sexo"] = Session["Sexo"];

            //Para Mostrar Foto de Perfil                             
            var FotoPerfil = db.MiPerfilAvatars.Where(x => x.codigoEmpleado == EmpCargo).OrderByDescending(x => x.id).Take(1).Select(f => f.avatar).SingleOrDefault();

            if (string.IsNullOrEmpty(FotoPerfil))
            {
                /*HttpContext.Session["avatar"] = "~/Content/images/user.png";*/ ///Imagen Por Defecto Cuando No tiene Cargada
                HttpContext.Session["avatar"] = string.Concat(System.Configuration.ConfigurationManager.AppSettings["intranet"], "Content/images/user.png");
                HttpContext.Session["avatarEstado"] = 1;
            }
            else
            {
                HttpContext.Session["avatar"] = FotoPerfil; //Ruta de Imagen Guardada en la BD
                HttpContext.Session["avatarEstado"] = 0;
            }

            //Convert.ToInt32("Walala");

            return View(db.Noticias.OrderByDescending(c => c.NoticiaId).Take(10).ToList());
        }

        public PartialViewResult vLecturaDelDia(string fecha)
        {
            try
            {
                DateTime Fecha_Expiracion = DateTime.Now;

                if(fecha!=null) Code.Utilities2.convertirStringAfecha(fecha, out Fecha_Expiracion);

                int day = (int)(Fecha_Expiracion - new DateTime(2000, 1, 1)).TotalDays;
                Random rnd = new Random(day);
                int id = rnd.Next(1, 31101);

                var lectura = db.bible_fts_full.Find(id);

                return PartialView(lectura);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return PartialView(new bible_fts_full());
            }

            #region Codigo Viejo

                //try
                //{
                //    WebRequest webRequest = WebRequest.Create("http://evangeliodeldia.org/rss/v2/evangelizo_rss-sp.xml");
                //    NetworkCredential nc = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["Usuario"], System.Configuration.ConfigurationManager.AppSettings["Contrasena"]);
                //    webRequest.Proxy = new WebProxy("http://172.18.0.1:8080/", true, null, nc);
                //    WebResponse response = webRequest.GetResponse();
                //    Stream dataStream = response.GetResponseStream();
                //    StreamReader reader = new StreamReader(dataStream);

                //    //   string lecturaSatanas = reader.ReadToEnd();

                //    XDocument doc = XDocument.Load(reader);

                //    string nowFecha = DateTime.Now.ToShortDateString();
                //    string linkAbsoluto = "main.php?language=SP&module=readings&localdate=" + nowFecha;

                //    var query = (from feed in doc.Descendants("item")
                //                 select new Feed
                //                 {
                //                     titulo = feed.Element("title").Value,
                //                     link = feed.Element("link").Value + linkAbsoluto,
                //                     descripcion = feed.Element("description").Value,

                //                 }).FirstOrDefault();

                //    return PartialView(query);
                //}
                //catch (Exception ex)
                //{
                //    ex.ToString();
                //    return PartialView(new Feed() { titulo = "", link = "", descripcion = "" });
                //}

            #endregion

        }

        private Boolean ValidarCertificado(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        public PartialViewResult PalabraIngles(string fecha)
        {
            try
            {
                DateTime Fecha_Expiracion = DateTime.Now;

                if (fecha != null) Code.Utilities2.convertirStringAfecha(fecha, out Fecha_Expiracion);

                int day = (int)(Fecha_Expiracion - new DateTime(2000, 1, 1)).TotalDays;
                Random rnd = new Random(day);
                int id = rnd.Next(1, 1000);

                var cita = db.citas.Find(id);

                ViewBag.cita = cita.cita;
                ViewBag.autor = cita.autor;

                return PartialView();
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.cita = "Ha ocurrido un error al cargar la cita.";
                return PartialView();
            }

            #region Codigo Viejo

            //    System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(ValidarCertificado);

            //    //https://www.merriam-webster.com/wotd/feed/rss2
            //    //https://wordsmith.org/awad/rss1.xml
            //    WebRequest webRequest = WebRequest.Create("http://feeds.feedblitz.com/english-word-of-the-day-for-spanish&x=1.xml");
            //    NetworkCredential nc = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["Usuario"], System.Configuration.ConfigurationManager.AppSettings["Contrasena"]);
            //    webRequest.Proxy = new WebProxy("http://172.18.1.9:8080/", true, null, nc);
            //    WebResponse response = webRequest.GetResponse();
            //    Stream dataStream = response.GetResponseStream();
            //    StreamReader reader = new StreamReader(dataStream);

            //    XDocument doc = XDocument.Load(reader);

            //    string nowFecha = DateTime.Now.ToShortDateString();

            //    var query = (from feed in doc.Descendants("item")
            //                 select new Feed
            //                 {
            //                     titulo = feed.Element("title").Value,
            //                     link = feed.Element("link").Value,
            //                     descripcion = feed.Element("description").Value,

            //                 }).FirstOrDefault();

            #endregion
        }

        public JsonResult palabra()
        {
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(ValidarCertificado);

                //http://www.spanishdict.com/wordoftheday/feed

                WebRequest webRequest = WebRequest.Create("http://www.spanishdict.com/wordoftheday/feed");
                NetworkCredential nc = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["Usuario"], System.Configuration.ConfigurationManager.AppSettings["Contrasena"]);
                webRequest.Proxy = new WebProxy("http://172.18.0.1:8080/", true, null, nc);
                WebResponse response = webRequest.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);

                XDocument doc = XDocument.Load(reader);

                string nowFecha = DateTime.Now.ToShortDateString();

                var query = (from feed in doc.Descendants("item")
                             select new Feed
                             {
                                 titulo = feed.Element("title").Value,
                                 link = feed.Element("link").Value,
                                 descripcion = feed.Element("description").Value,

                             }).FirstOrDefault();
                var json = JsonConvert.SerializeObject(query);

                return Json(json, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        #region MostrarCantidaddeSolicitud
        public void MostrarCantidaddeSolicitud(int CodigoEmpleado)
        {

            var solicitado = db.Sol_Registro_Solicitud.Where(x => x.SolicitanteId == CodigoEmpleado && x.EstadoId == 1).ToList();

            if (solicitado != null)
            {
                HttpContext.Session["CantidadSolicitados"] = solicitado.Count();

            }
            else
            {
                HttpContext.Session["CantidadSolicitados"] = 0;
            }
          
        }
        #endregion

        //private void MostrarCantidadAsignados(int CodigoEmpleado)
        //{

        //    var Asignados = db.Sol_Registro_Solicitud.Where(x => x.TecnicoId == CodigoEmpleado && x.EstadoId == 1).ToList();

        //    if (Asignados != null)
        //    {
        //        HttpContext.Session["CantidadAsignados"] = Asignados.Count();

        //    }
        //    else
        //    {
        //        HttpContext.Session["CantidadAsignados"] = 0;
        //    }
        //}


        #region MostrarCantidadAsignados
        public void MostrarCantidadAsignados(int CodigoEmpleado)
        {
            int Cdg = 0;

            Cdg = CodigoEmpleado;

            //if (ValidarActividad(Cdg) == true)
            //{
            //    List<ActividadDependencia> CdgTipoSolicitud = new List<ActividadDependencia>();

            //    CdgTipoSolicitud = new AsignadosController().CodigoActividad2(Cdg);

            //    var CantidadAsignado = new List<Vw_Mostrar_Usuario_Con_Su_Descripcion>();

            //    foreach (var item in CdgTipoSolicitud)
            //    {
            //        if (item.Key == "Actividad")
            //        {
            //            var lista_solicitudes_por_actividad = db.Sol_Detalle_Lista.Where(cd => cd.CodigoEstadoSolicitud == 1 && cd.CodigoTipoSolicitud == item.Value && cd.CodigoTecnico == Cdg && cd.DependenciaId == 1).OrderByDescending(cd => cd.FechaCreacion).ToList();

            //            foreach (var item2 in lista_solicitudes_por_actividad)
            //            {
            //                CantidadAsignado.Add(item2);
            //            }
            //        }
            //        else
            //        {
            //            var lista_solicitudes_por_dependencia = db.Sol_Detalle_Lista.Where(cd => cd.CodigoEstadoSolicitud == 1 && cd.CodigoTecnico == Cdg && cd.DependenciaId == item.Value).OrderByDescending(cd => cd.FechaCreacion).ToList();

            //            foreach (var item2 in lista_solicitudes_por_dependencia)
            //            {
            //                CantidadAsignado.Add(item2);
            //            }
            //        }
            //    }

            var Asignados = db.Sol_Registro_Solicitud.Where(x => x.TecnicoId == CodigoEmpleado && x.EstadoId == 1).ToList();

            if (Asignados != null)
            {
                HttpContext.Session["CantidadAsignados"] = Asignados.Count();

            }
            else
            {
                HttpContext.Session["CantidadAsignados"] = 0;
            }


            //    if (CantidadAsignado.Count > 0)
            //    {
            //        HttpContext.Session["CantidadAsignado"] = CantidadAsignado.Count();
            //    }
            //    else
            //    {
            //        HttpContext.Session["CantidadAsignado"] = 0;
            //    }
            //}
            //else
            //{
            //    var CantidadAsignado = db.Sol_Detalle_Lista.Where(cd => cd.CodigoTecnico == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

            //    if (CantidadAsignado.Count > 0)
            //    {
            //        HttpContext.Session["CantidadAsignado"] = CantidadAsignado.Count();
            //    }
            //    else
            //    {
            //        HttpContext.Session["CantidadAsignado"] = 0;
            //    }

        }

    }
    #endregion

    //public FileResult DownloadWinners()
    //{
    //    var ruta_archivo = HttpContext.Server.MapPath("~/Content/Archivos/Rifas Numeros no Reclamados.xlsx");

    //    byte[] archivo = System.IO.File.ReadAllBytes(ruta_archivo);

    //    return File(archivo, "xlsx", "Rifas Numeros no Reclamados.xlsx");
    //}


        #region MostrarCantidadChat
        //private void MostrarCantidadChat(int CodigoEmpleado)
        //{
        //    int Cdg = 0;

        //    Cdg = CodigoEmpleado;

        //    var CantidadChat = db.Sol_Mostrar_Lista_de_Asignacion_Chat.Where(cd => cd.Estado == true && cd.CodigoUsuario == Cdg).ToList();


        //    if (CantidadChat.Count > 0)
        //    {

        //        HttpContext.Session["CantidadChat"] = CantidadChat.Count();
        //    }
        //    else
        //    {
        //        HttpContext.Session["CantidadChat"] = 0;
        //    }



        //}
        #endregion

        //#region MostrarCantidaddeSolicitud
        //private void MostrarCantidaddeSolicitud()
        //{
        //    int Cdg = 0;

        //    Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

        //    var Cantidad = db.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

        //    if (Cantidad.Count > 0)
        //    {

        //        HttpContext.Session["Cantidad"] = Cantidad.Count();
        //    }
        //    else
        //    {
        //        HttpContext.Session["Cantidad"] = 0;
        //    }



        //}
        //#endregion

        //#region MostrarCantidadAsignados
        //private void MostrarCantidadAsignados()
        //{
        //    int Cdg = 0;

        //    Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

        //    var CantidadAsignado = db.Sol_Detalle_Lista.Where(cd => cd.CodigoTecnico == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

        //    if (CantidadAsignado.Count > 0)
        //    {

        //        HttpContext.Session["CantidadAsignado"] = CantidadAsignado.Count();
        //    }
        //    else
        //    {
        //        HttpContext.Session["CantidadAsignado"] = 0;
        //    }



        //}
        //#endregion

        //#region MostrarCantidadChat
        //private void MostrarCantidadChat()
        //{
        //    int Cdg = 0;

        //    Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

        //    var CantidadChat = db.Sol_Mostrar_Lista_de_Asignacion_Chat.Where(cd => cd.Estado == true && cd.CodigoUsuario == Cdg).ToList();


        //    if (CantidadChat.Count > 0)
        //    {

        //        HttpContext.Session["CantidadChat"] = CantidadChat.Count();
        //    }
        //    else
        //    {
        //        HttpContext.Session["CantidadChat"] = 0;
        //    }



        //}
        //#endregion
    }





