﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.RolesUsuarios;

namespace intra.Controllers
{
    public class SistemasController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: Sistemas
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Mantenimiento de Sistemas");
            return View(db.Sistemas.ToList());
        }

        // GET: Sistemas/Details/5
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sistemas sistemas = db.Sistemas.Find(id);
            if (sistemas == null)
            {
                return HttpNotFound();
            }
            return View(sistemas);
        }

        // GET: Sistemas/Create
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sistemas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Create([Bind(Include = "SistemaId,SistemaNombre,SistemaDescripcion")] Sistemas sistemas)
        {
            if (ModelState.IsValid)
            {
                db.Sistemas.Add(sistemas);
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo el Sistema llamado (" + sistemas.SistemaNombre + ")");
                return RedirectToAction("Index");
            }

            return View(sistemas);
        }

        // GET: Sistemas/Edit/5
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sistemas sistemas = db.Sistemas.Find(id);
            if (sistemas == null)
            {
                return HttpNotFound();
            }
            return View(sistemas);
        }

        // POST: Sistemas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Edit([Bind(Include = "SistemaId,SistemaNombre,SistemaDescripcion")] Sistemas sistemas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sistemas).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito el Sistema id (" + sistemas.SistemaId + ")");
                return RedirectToAction("Index");
            }
            return View(sistemas);
        }

        // GET: Sistemas/Delete/5
        [CustomAuthorize]
        [Authorize(Roles = "RUA")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sistemas sistemas = db.Sistemas.Find(id);
            if (sistemas == null)
            {
                return HttpNotFound();
            }
            return View(sistemas);
        }

        // POST: Sistemas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA")]
        public ActionResult DeleteConfirmed(int id)
        {
            Sistemas sistemas = db.Sistemas.Find(id);
            db.Sistemas.Remove(sistemas);
            db.SaveChanges();
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Elimino el Sistema llamado (" + sistemas.SistemaNombre + ")");
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
