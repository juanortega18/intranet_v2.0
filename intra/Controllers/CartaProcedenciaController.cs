﻿using intra.Models.CartaProcedencia;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    [Authorize(Roles = "RRHHA")]
    public class CartaProcedenciaController : Controller
    {
        dbIntranet dbIntranet = new dbIntranet();


        [HttpGet]
        [CustomAuthorize]
        public ActionResult Index()
        {
            var cartas = (from x in dbIntranet.CartaProcedencia
                          from e in dbIntranet.VISTA_EMPLEADOS
                          where x.CartaProcCodigoEmpleado.ToString() == e.empleadoid
                          where x.CartaProcEstado
                          select new ProcedenciaDTO {
                              Codigo = x.CartaProcId,
                              CodigoEmpleado = x.CartaProcCodigoEmpleado,
                              FechaIngreso = x.CartaProcFechaIngreso,
                              FechaSalida = x.CartaProcFechaSalida,
                              Tiempo = x.CartaProcTiempo,
                              DocumentoNombre = x.CartaProcDocumentoNombre,
                              Verificado = x.CartaProcVerificado,
                              NombreEmpleado = e.nombre
                          }).OrderBy(x=>x.NombreEmpleado).ToList();

            return View(cartas);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult Crear()
        {
            return View();
        }

        #region BUSCAR EMPLEADO

        [HttpGet]
        [CustomAuthorize]
        public JsonResult BuscarEmpleado(string codigo)
        {
            int codigoEmpleado;

            try
            {
                codigoEmpleado = Convert.ToInt32(codigo);
            }
            catch (Exception)
            {
                return Json(new EmpleadoProcedenciaDTO(), JsonRequestBehavior.AllowGet);
            }

            var empleado = dbIntranet.VISTA_EMPLEADOS.Where(emp => emp.empleadoid == codigoEmpleado.ToString()).FirstOrDefault();

            if (empleado == null) return Json(new EmpleadoProcedenciaDTO(), JsonRequestBehavior.AllowGet);

            var cartasProcedencia = dbIntranet.CartaProcedencia.Where(x => x.CartaProcCodigoEmpleado == codigoEmpleado && x.CartaProcEstado == true)
                .Select(x=> new ProcedenciaDTO
                {
                    Codigo = x.CartaProcId,
                    FechaIngreso = x.CartaProcFechaIngreso,
                    FechaSalida = x.CartaProcFechaSalida,
                    DocumentoNombre = x.CartaProcDocumentoNombre,
                    Tiempo = x.CartaProcTiempo
                }).OrderBy(x=>x.FechaIngreso).ToList();

            EmpleadoProcedenciaDTO empleadoProcedencia = new EmpleadoProcedenciaDTO();
            empleadoProcedencia.CodigoEmpleado = Convert.ToInt32(empleado.empleadoid);
            empleadoProcedencia.Nombre = empleado.nombre;
            empleadoProcedencia.Departamento = empleado.departamento;
            empleadoProcedencia.CartasProcedencia = cartasProcedencia;

            return Json(empleadoProcedencia, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region GUARDAR CARTA (Comentado)

        //[HttpPost]
        //[Authorize(Roles = "RRHHA")]
        //[CustomAuthorize]
        //public JsonResult GuardarProcedencia(ProcedenciaDTO procedencia)
        //{
        //    string[] listaErrores = new string[] {
        //        "Ingrese una fecha de ingreso",
        //        "Ingrese una fecha de salida",
        //        "La fecha de entrada no puede ser mayor que la fecha de salida"
        //    };

        //    List<string> errores = new List<string>();

        //    if (procedencia.FechaIngreso.Date.ToString("dd/MM/yyyy") == "01/01/0001")
        //    {
        //        errores.Add(listaErrores[0]);
        //    }

        //    if (procedencia.FechaSalida.Date.ToString("dd/MM/yyyy") == "01/01/0001")
        //    {
        //        errores.Add(listaErrores[1]);
        //    }

        //    if (procedencia.FechaIngreso > procedencia.FechaSalida)
        //    {
        //        errores.Add(listaErrores[2]);
        //    }

        //    if (errores.Count > 0)
        //    {
        //        return Json(new { Errores = errores });
        //    }

        //    DateTime fechaIngreso = procedencia.FechaIngreso;
        //    DateTime fechaSalida = procedencia.FechaSalida;

        //    double dias = (fechaSalida - fechaIngreso).TotalDays;
        //    int meses = dias % 31 > 27 ? (int)Math.Ceiling(dias / 31) : (int)(dias / 31);

        //    CartaProcedencia cartaProcedencia = new CartaProcedencia();
        //    cartaProcedencia.CartaProcCodigoEmpleado = procedencia.CodigoEmpleado;
        //    cartaProcedencia.CartaProcFechaIngreso = procedencia.FechaIngreso;
        //    cartaProcedencia.CartaProcFechaSalida = procedencia.FechaSalida;
        //    cartaProcedencia.CartaProcTiempo = meses;
        //    cartaProcedencia.CartaProcEstado = true;

        //    #region Agregar Archivo y Nombre

        //    if (Request.Files.Count > 0)
        //    {
        //        //get extension file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
        //        //file name String.Join(".", file.FileName.Split('.').Take(file.FileName.Split('.').Length - 1));
        //        HttpPostedFileBase file = Request.Files[0];
        //        cartaProcedencia.CartaProcDocumentoNombre = file.FileName;

        //        using (MemoryStream ms = new MemoryStream())
        //        {
        //            file.InputStream.CopyTo(ms);
        //            cartaProcedencia.CartaProcDocumento = ms.GetBuffer();
        //        }

        //    }

        //    #endregion

        //    dbIntranet.CartaProcedencia.Add(cartaProcedencia);
        //    dbIntranet.SaveChanges();

        //    return Json(new { Errores = errores });
        //}

        #endregion

        #region GUARDAR CARTA

        [HttpPost]
        [Authorize(Roles = "RRHHA")]
        [CustomAuthorize] 
        public JsonResult GuardarProcedencia(ProcedenciaDTO procedencia)
        {
            string[] listaErrores = new string[] {
                "Ingrese una fecha de ingreso",
                "Ingrese una fecha de salida",
                "La fecha de entrada no puede ser mayor que la fecha de salida",
                "No se pudo enviar el correo"
            };

            List<string> errores = new List<string>();

            if (procedencia.FechaIngreso.Date.ToString("dd/MM/yyyy") == "01/01/0001")
            {
                errores.Add(listaErrores[0]);
            }

            if (procedencia.FechaSalida.Date.ToString("dd/MM/yyyy") == "01/01/0001")
            {
                errores.Add(listaErrores[1]);
            }

            if (procedencia.FechaIngreso > procedencia.FechaSalida)
            {
                errores.Add(listaErrores[2]);
            }

            if (errores.Count > 0)
            {
                return Json(new { Errores = errores });
            }

            DateTime fechaIngreso = procedencia.FechaIngreso;
            DateTime fechaSalida = procedencia.FechaSalida;

            double dias = (fechaSalida - fechaIngreso).TotalDays;
            int meses = dias % 31 > 27 ? (int)Math.Ceiling(dias / 31) : (int)(dias / 31);

            CartaProcedencia cartaProcedencia = new CartaProcedencia();
            cartaProcedencia.CartaProcFechaIngreso = procedencia.FechaIngreso;
            cartaProcedencia.CartaProcFechaSalida = procedencia.FechaSalida;
            cartaProcedencia.CartaProcTiempo = meses;
            cartaProcedencia.CartaProcEstado = true;

            cartaProcedencia.CartaProcVerificado = User.IsInRole("RRHHA") == true ? true : false;
            //cartaProcedencia.CartaProcVerificado = false;


            cartaProcedencia.CartaProcCodigoEmpleado = procedencia.CodigoEmpleado == 0 ? int.Parse(Session["empleadoId"].ToString()) : procedencia.CodigoEmpleado;


            #region Agregar Archivo y Nombre

            if (Request.Files.Count > 0)
            {
                //get extension file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                //file name String.Join(".", file.FileName.Split('.').Take(file.FileName.Split('.').Length - 1));
                HttpPostedFileBase file = Request.Files[0];
                cartaProcedencia.CartaProcDocumentoNombre = file.FileName;

                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    cartaProcedencia.CartaProcDocumento = ms.GetBuffer();
                }

            }

            #endregion

            dbIntranet.CartaProcedencia.Add(cartaProcedencia);
            dbIntranet.SaveChanges();

            if(!cartaProcedencia.CartaProcVerificado)

            try {
                EnviarCorreo(Session["usuario"].ToString(), "CARTA DE PROCEDENCIA", DescripcionCorreo(cartaProcedencia.CartaProcId.ToString()), 
                    cartaProcedencia.CartaProcDocumento, cartaProcedencia.CartaProcDocumentoNombre);
            }
            catch {
                errores.Add(listaErrores[3]);
            }

            return Json(new { Errores = errores });
        }

        #endregion

        #region VERIFICAR PROCEDENCIA

        [HttpPost]
        [Authorize(Roles = "RRHHA")]
        [CustomAuthorize]
        public JsonResult VerificarProcedencia(int cartaId)
        {
            CartaProcedencia cartaProcedencia = dbIntranet.CartaProcedencia.Where(cp => cp.CartaProcId == cartaId).FirstOrDefault();
            cartaProcedencia.CartaProcVerificado = true;


            dbIntranet.SaveChanges();

            return Json(new { });
        }

        #endregion

        #region ELIMINAR PROCEDENCIA

        [HttpPost]
        [Authorize(Roles = "RRHHA")]
        [CustomAuthorize]
        public JsonResult EliminarProcedencia(int cartaId)
        {
            CartaProcedencia cartaProcedencia = dbIntranet.CartaProcedencia.Where(cp => cp.CartaProcId == cartaId).FirstOrDefault();
            cartaProcedencia.CartaProcEstado = false;

            dbIntranet.SaveChanges();

            return Json(new { });
        }

        #endregion

        [HttpGet]
        [AllowAnonymous]
        public FileResult DescargarDocumento(int id)
        {
            var archivo = dbIntranet.CartaProcedencia.Find(id);

            return File(archivo.CartaProcDocumento,archivo.CartaProcDocumentoNombre);
        }

        #region EnviarCorreo
        public void EnviarCorreo(string Usuario, string Sujeto, string Descripcion, byte[] documento,string documentoNombre)
        {
            Correo correo = new Correo();

            string Correo = Usuario + "@PGR.GOB.DO";

            string Logo = "";

            string LogoDTI = "";

            //string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            //Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            //ObjCorreo.EnviarCorreo(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion);

            correo.EnviarCorreoCartaProcedencia_(Correo, Sujeto, Descripcion, Logo, LogoDTI, documento, documentoNombre);
        }
        #endregion

        #region ObtenerEtiquetaHTML
        public string DescripcionCorreo(/*string Departamento,*/string Id/*, string Link*/)
        {

            //string NombreArchivo = Server.MapPath (@"~/EtiquetasHtml/Solicitud_nuevoServicio_PGR.html");
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/Correo-CartaProcedencia.html");

            string Departamento = Session["departamento"].ToString();

            string NombreCompleto = Session["nombre"].ToString();

            string Link = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/CartaProcedencia/DescargarDocumento?id={Id}";

            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", "CARTA DE PROCEDENCIA REMITIDA");
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$pagina", Link);

            return Archivo;
        }

        #endregion
    }
}