﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;

namespace intra.Controllers
{
    public class SolicitadosController : Controller
    {

        //private static SqlConnection Enlace = new SqlConnection(ConfigurationManager.ConnectionStrings["inv"].ConnectionString);

        SolicitudModel smodel = new SolicitudModel();

        private dbIntranet obj = new dbIntranet();

        #region MostrarTipoConsulta
        private void MostrarTipoConsulta()
        {
            int Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

            smodel.TipoConsulta = obj.Sol_Tipo_Consulta_Solicitud.Select(cd => new TipoConsultaModel
            {
                ConsultaId = cd.ConsultaId,
                Descripcion = cd.Descripcion 
            }).ToList();

            if (Supervisor(Cdg) == 2 && ValidarDirector(Cdg) == false) { smodel.TipoConsulta.RemoveAt(0); }

        }
        #endregion 

        #region MostrarEstadoConsulta
        private void MostrarEstadoConsulta()
        {
            #region Commented Code
            //if (Enlace.State == ConnectionState.Closed)
            //{
            //    Enlace.Open();
            //}

            //SqlCommand cmd = new SqlCommand("Sp_Mostrar_Estado_Busqueda", Enlace);
            //cmd.CommandType = CommandType.StoredProcedure;
            //SqlDataAdapter Sda = new SqlDataAdapter(cmd);
            //DataTable Dta = new DataTable();
            //Sda.Fill(Dta);


            //List<SelectListItem> list = new List<SelectListItem>();

            //smodel.EstadoConsulta = Dta.AsEnumerable().Select(cd => new EstadoConsultaModel
            //{
            //    EstadoId = cd.Field<int>("EstadoId"),
            //    Descripcion = cd.Field<string>("Descripcion"),
            //}).ToList();
            #endregion

            smodel.EstadoConsulta = obj.Sol_Estado_Solicitud.Select(cd => new EstadoConsultaModel
            {
                EstadoId = cd.EstadoId,
                Descripcion = cd.Descripcion
            }).ToList();
        }
        #endregion

        #region Mostrar_Informacion_Departamentos
        private void Mostrar_Informacion_Departamentos()
        {
            //if (Enlace.State == ConnectionState.Closed)
            //{
            //    Enlace.Open();
            //}

            //SqlCommand cmd = new SqlCommand("SP_Mostrar_Dep", Enlace);
            //cmd.CommandType = CommandType.StoredProcedure;
            //SqlDataAdapter Sda = new SqlDataAdapter(cmd);
            //DataTable Dta = new DataTable();
            //Sda.Fill(Dta);


            //List<SelectListItem> list = new List<SelectListItem>();


            //smodel.Departamentos = Dta.AsEnumerable().Select(cd => new DepartamentoModel()
            //{
            //    DepartamentoId = cd.Field<int>("DepartamentoId"),
            //    Descripcion = cd.Field<string>("Descripcion"),
            //}).ToList();
            smodel.Departamentos = obj.Sol_Departamento.ToList().Where(cd => cd.Estado == true).Select(cd => new DepartamentoModel()
            {
                DepartamentoId = cd.DepartamentoId,
                Descripcion = cd.Descripcion
            }).ToList();
        }

        #endregion
        
        #region ValidarSupervisor
        private int  ValidarSupervisor(int CodigoSpervisor)
        {
            int A = 0;
            
            var B = obj.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSpervisor && cd.Estado == true).ToList();

            if (B.Count > 0)
            {
                A = 1;
            }
            else
            {
                A = 2;
            }
            return A;

        }
        #endregion
        
        #region MostrarSINOSupervisor
        private int Supervisor(int CodigoSupervisor)
        {
            int Resultado = 0;

            var cantidad = obj.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSupervisor).ToList();

            if (cantidad.Count > 0)
            {
                Resultado = 1;
            }
            else
            {
                Resultado = 2;
            }
            return Resultado;
        }
        #endregion

        #region OtrosTecnicos
        public List<Sol_Tecnicos_Alternativos> ConsultarOtrosTecnicos(int Cdg)
        {
            var is_otro_tecnico = obj.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == Cdg && x.Estado).ToList();

            if (is_otro_tecnico.Count > 0)
            {
                return is_otro_tecnico;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region ValidarDirector
        private bool ValidarDirector(int CodigoCdg)
        {
            bool A;

            var Valor = obj.Sol_Departamento.Where(cd => cd.DirectorId == CodigoCdg).ToList();

            if (Valor.Count > 0)
            {

                A = true;
            }
            else
            {
                A = false;
            }

            return A;


        }
        #endregion

        #region BuscarDirector
        private int BuscarDirector(int CodigoCdg)
        {
            int CodigoDepartamento = 0;

            var Valor = obj.Sol_Departamento.Where(cd => cd.DirectorId == CodigoCdg).FirstOrDefault();

            if (Valor != null) {
                CodigoDepartamento = Valor.DepartamentoId;
            }

            return CodigoDepartamento;
        }
        #endregion

        #region MostrarPorActividad
        public int BuscarActividad(int CodigoSupervisor)
        {
            int CdgActividad = 0;

            var Valor = obj.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSupervisor).FirstOrDefault();

            if (Valor!=null) {
                CdgActividad = Valor.CodigoActividad;
            }

            return CdgActividad;
        }
        #endregion

        // GET: Solicitados
        //
        [CustomAuthorize]
        //lisbettetravieso3@gmail.com
        [HttpGet]
        public ActionResult Solicitados(SolicitudModel model)
        {
            //Variable 
            int Cdg = 0;
            int CodigoDepartamentos = 0;
            
            MostrarTipoConsulta();
            MostrarEstadoConsulta();

            Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

            var Asignado = obj.Sol_Registro_Solicitud.Where(x => x.TecnicoId == Cdg && x.EstadoId == 1).ToList();
            var solicitado = obj.Sol_Registro_Solicitud.Where(x => x.SolicitanteId == Cdg && x.EstadoId == 1).ToList();

            HttpContext.Session["CantidadAsignados"] = Asignado.Count();
            HttpContext.Session["CantidadSolicitados"] = solicitado.Count();


            if (ValidarDirector(Cdg) == true)
            {
                CodigoDepartamentos = BuscarDirector(Cdg);

                smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoDepartamento == CodigoDepartamentos && cd.CodigoEstadoSolicitud == 1).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();


                //smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoDepartamento == CodigoDepartamentos && cd.CodigoEstadoSolicitud == 1).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
                MostrarTipoConsulta();
                MostrarEstadoConsulta();
            }
            else
            {
                var list_alternativo = ConsultarOtrosTecnicos(Cdg);

                if(list_alternativo != null && list_alternativo.Count > 0)
                {
                    smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == 1).OrderByDescending(cd => cd.FechaCreacion).ToList();

                    foreach (var item in list_alternativo)
                    {
                        var lista_individual = obj.Sol_Detalle_Lista.Where(cd => (cd.CodigoTipoSolicitud == item.ActividadId || cd.DependenciaId == item.DependenciaRegionalId.Value) && cd.CodigoEstadoSolicitud == 1).OrderByDescending(cd => cd.FechaCreacion).ToList();

                        if(lista_individual!=null && lista_individual.Count > 0)
                        {
                            foreach (var item2 in lista_individual)
                            {
                                if (!smodel.TB_Detalle_Lista.Contains(item2)) { smodel.TB_Detalle_Lista.Add(item2); }
                            }
                        }
                    }

                    MostrarTipoConsulta();
                    MostrarEstadoConsulta();
                }
                else
                {
                    smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == 1).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
                    MostrarTipoConsulta();
                    MostrarEstadoConsulta();
                }
            }

            #region Commented Code
            //Revisar

            //int C = 0;

            //C = ValidarSupervisor(Cdg);


            //if (Supervisor(Cdg) == 1)
            //{

            //    var Supervisor= obj.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == Cdg && cd.Estado == true).FirstOrDefault();

            //    smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd=>cd.CodigoEstadoSolicitud == 1 && cd.CodigoTipoSolicitud == Supervisor.CodigoActividad).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();

            //}
            //else if (Supervisor(Cdg) == 2)
            //{
            //    smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoEstadoSolicitud == 1 && cd.CodigoSolicitante == Cdg).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //}
            #endregion

            smodel.Usurio = HttpContext.Session["nombre"].ToString();
            HttpContext.Session["RetornoSolicitud"] = "Solicitados";
            return View(smodel);
        }

        [HttpPost]
        public ActionResult Consultas_de_Servicios(SolicitudModel model)
        {
            ///Remover Session 

            Session["CodigoSolicitud"] = null;

            Session["CdgDeptos"] = null;

            Session["CodigoSolicitudHistorico"] = null;

            //Variable 

            int Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

            int codigotipo = model.CodigoTipoConsulta;

            var Usuario = model.FiltrarPorUsuario;

            var Responsable  = model.FiltrarPorResponsable;

            int CodigoActividad = BuscarActividad(Cdg);

            int CodigoDepartamento = BuscarDirector(Cdg);

            var Lista = new List<Vw_Mostrar_Usuario_Con_Su_Descripcion>();

            #region Convertir los valores a tipo fecha
            var fechaFin = Request.Form["FechaFinal"].ToString();
            var fechaInicio = Request.Form["FechaInicial"].ToString();
            //Conversion formato fecha Inicial
            DateTime FechaInicial = DateTime.ParseExact(fechaInicio, "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture);
            //Para que la fecha inicial sea YYYY-MM-DD 11:59:59 PM
            DateTime A = codigotipo == 3 ? FechaInicial.AddDays(1).AddSeconds(-1) : DateTime.Now;
            //DateTime A = codigotipo == 3 ? model.FechaInicial : DateTime.Now;
       
            //Conversion formato fecha Final
            DateTime fechafinal = DateTime.ParseExact(fechaFin, "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture);

            //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            DateTime B = codigotipo == 3 ? fechafinal.AddDays(1).AddSeconds(-1) : DateTime.Now;
            #endregion

            //if (model.CodigoEstadoConsulta > 0) { ... }

            Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true
            && (cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta || !(model.CodigoEstadoConsulta>0))
            && (cd.CodigoDepartamento == CodigoDepartamento || CodigoDepartamento == 0)
            && (cd.CodigoSolicitante == Cdg || CodigoDepartamento != 0)
            && (cd.NombreSolicitante.Contains(Usuario) || codigotipo != 1)
            && (cd.NombreTecnico.Contains(Responsable) || codigotipo != 2)
            && (cd.FechaCreacion >= A && cd.FechaCreacion <= B || codigotipo != 3)
                ).OrderByDescending(cd => cd.FechaCreacion).ToList();

            #region Agregar Usuario Alternativos

            var list_alternativo = ConsultarOtrosTecnicos(Cdg);

            var lista_id_servicios = new List<int>();

            if (list_alternativo != null && list_alternativo.Count > 0)
            {
                //foreach (var item in list_alternativo)
                //{
                    //var lista_individual = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true
                    //    && (cd.CodigoTipoSolicitud == item.ActividadId || cd.DependenciaId == item.DependenciaRegionalId.Value)
                    //    && (cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta || !(model.CodigoEstadoConsulta > 0))
                    //    && (cd.NombreSolicitante.Contains(Usuario) || codigotipo != 1)
                    //    && (cd.NombreTecnico.Contains(Responsable) || codigotipo != 2)
                    //    && (cd.FechaCreacion >= A && cd.FechaCreacion <= B || codigotipo != 3)
                    //    && (cd.FechaCreacion >= mesanterior && cd.FechaCreacion <= DateTime.Now || codigotipo == 3)
                    //        ).OrderByDescending(cd => cd.FechaCreacion).ToList();

                    //if (lista_individual != null && lista_individual.Count > 0)
                    //{
                    //    foreach (var item2 in lista_individual)
                    //    {
                    //        if (!Lista.Contains(item2))
                    //        {
                    //            Lista.Add(item2);
                    //        }
                    //    }
                    //}
                //}

                var dependencias = list_alternativo.Where(x => x.ActividadId == 34).Select(x=>x.DependenciaRegionalId).ToList();

                var actividades = list_alternativo.Select(x => x.ActividadId).ToList();

                DateTime mesanterior = DateTime.Now;
                mesanterior = mesanterior.AddMonths(-2);

                lista_id_servicios = obj.Sol_Registro_Solicitud.Where(cd => cd.Estado
                && (actividades.Contains(cd.Tipo_SolicitudId) || dependencias.Contains(cd.DependenciaRegionalId))
                && (cd.EstadoId == model.CodigoEstadoConsulta || !(model.CodigoEstadoConsulta > 0))
                && (cd.FhCreacion >= A && cd.FhCreacion <= B || codigotipo != 3)
                //&& (cd.FhCreacion >= mesanterior && cd.FhCreacion <= DateTime.Now || codigotipo == 3)
                ).OrderByDescending(cd => cd.FhCreacion).Select(x => x.SolicitudId).ToList();

                lista_id_servicios = lista_id_servicios.Distinct().ToList();

                var lista_servicios = obj.Sol_Detalle_Lista.Where(cd => lista_id_servicios.Contains(cd.CodigoSolicitud)
                && (cd.NombreSolicitante.Contains(Usuario) || codigotipo != 1)
                && (cd.NombreTecnico.Contains(Responsable) || codigotipo != 2)
                ).ToList();

                Lista.AddRange(lista_servicios);

            }

            #endregion

            #region Mostrar informacion

            if (Lista.Count > 0)
            {
                smodel.TB_Detalle_Lista = Lista;
                MostrarTipoConsulta();
                MostrarEstadoConsulta();

            }
            else
            {
                smodel.TB_Detalle_Lista = Lista;
                smodel.MensajeError = "NO EXISTEN REGISTRO.";
                MostrarTipoConsulta();
                MostrarEstadoConsulta();
            }

            #endregion

            #region Commented Code

            //else if ((codigotipo == 2) && (model.CodigoEstadoConsulta > 0))
            //{
            //    if (Supervisor(Cdg) == 1)
            //    {
            //        var Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreTecnico.Contains(model.FiltrarPorResponsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();

            //        if (Lista.Count > 0)
            //        {
            //            smodel.TB_Detalle_Lista = Lista;
            //            MostrarTipoConsulta();
            //            MostrarEstadoConsulta();
            //        }
            //        else
            //        {

            //            smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //            smodel.MensajeError = "NO EXISTEN REGISTRO.";
            //            MostrarTipoConsulta();
            //            MostrarEstadoConsulta();

            //        }
            //    }
            //    else if (Supervisor(Cdg) == 2)
            //    {
            //        var Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreTecnico.Contains(model.FiltrarPorResponsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();

            //        if (Lista.Count > 0)
            //        {
            //            smodel.TB_Detalle_Lista = Lista;
            //            MostrarTipoConsulta();
            //            MostrarEstadoConsulta();
            //        }
            //        else
            //        {

            //            smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //            smodel.MensajeError = "NO EXISTEN REGISTRO.";
            //            MostrarTipoConsulta();
            //            MostrarEstadoConsulta();

            //        }
            //    }
            //}
            //else if ((codigotipo == 3) && (model.CodigoEstadoConsulta > 0))
            //{

            //    DateTime A = model.FechaInicial;

            //    string createddateA = Convert.ToDateTime(A).ToString("yyyy-MM-dd h:mm tt");

            //    DateTime FechaIncial = DateTime.ParseExact(createddateA, "yyyy-MM-dd h:mm tt", CultureInfo.InvariantCulture);

            //    DateTime B = model.FechaFinal;

            //    string createddateB = Convert.ToDateTime(B).ToString("yyyy-MM-dd h:mm tt");

            //    DateTime FechaFinal = DateTime.ParseExact(createddateB, "yyyy-MM-dd h:mm tt", CultureInfo.InvariantCulture);

            //    //var valores = obj.Sol_Detalle_Lista.Select(cd => cd.FechaCreacion).FirstOrDefault();

            //    //DateTime Prueba = valores;

            //    if (Supervisor(Cdg) == 1)
            //    {

            //        var Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.FechaCreacion >= FechaIncial && cd.FechaCreacion <= FechaFinal).OrderByDescending(cd => cd.FechaCreacion).ToList();

            //        if (Lista.Count > 0)
            //        {
            //            smodel.TB_Detalle_Lista = Lista;
            //            MostrarTipoConsulta();
            //            MostrarEstadoConsulta();
            //        }
            //        else
            //        {

            //            smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //            smodel.MensajeError = "NO EXISTE INFORMACION CON ESTE RANGO DE FECHA.";
            //            MostrarTipoConsulta();
            //            MostrarEstadoConsulta();
            //        }
            //    }
            //    else if (Supervisor(Cdg) == 2)
            //    {


            //        var Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.FechaCreacion >= FechaIncial && cd.FechaCreacion <= FechaFinal).OrderByDescending(cd => cd.FechaCreacion).ToList();

            //        if (Lista.Count > 0)
            //        {
            //            smodel.TB_Detalle_Lista = Lista;
            //            MostrarTipoConsulta();
            //            MostrarEstadoConsulta();
            //        }
            //        else
            //        {

            //            smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //            smodel.MensajeError = "NO EXISTE INFORMACION CON ESTE RANGO DE FECHA.";
            //            MostrarTipoConsulta();
            //            MostrarEstadoConsulta();
            //        }

            //    }
            //}

            #endregion

            #region Comentado

            //if ((codigotipo == 1 || codigotipo == 2 || codigotipo == 3) && (model.CodigoEstadoConsulta > 0))
            //{
            //    if (CodigoDepartamento != 0)
            //    {
            //        switch (codigotipo)
            //        {
            //            case 1:
            //                Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoDepartamento == CodigoDepartamento && cd.Estado == true && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreSolicitante.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                break;
            //            case 2:
            //                Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoDepartamento == CodigoDepartamento && cd.Estado == true && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreTecnico.Contains(Responsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                break;
            //            case 3:
            //                #region BuscarSolicitudesPorFecha

            //                DateTime A = model.FechaInicial;

            //                //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            //                DateTime B = model.FechaFinal.AddDays(1).AddSeconds(-1);

            //                Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoDepartamento == CodigoDepartamento && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.FechaCreacion >= A && cd.FechaCreacion <= B).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                #endregion
            //                break;
            //        }
            //    }
            //    else
            //    {
            //        if (Supervisor(Cdg) == 1)
            //        {
            //            switch (codigotipo)
            //            {
            //                case 1:

            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTipoSolicitud == CodigoActividad && cd.Estado == true && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreSolicitante.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();

            //                    #region solicitudes individuales

            //                    //var solicitudes_individuales = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.Estado == true && cd.CodigoTecnico == Cdg && cd.CodigoTipoSolicitud == CodigoActividad && cd.NombreSolicitante.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();

            //                    //foreach (var item2 in solicitudes_individuales)
            //                    //{
            //                    //    if (!Lista.Contains(item2))
            //                    //    {
            //                    //        smodel.TB_Detalle_Lista.Add(item2);
            //                    //    }
            //                    //}

            //                    #endregion

            //                    break;
            //                case 2:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTipoSolicitud == CodigoActividad && cd.Estado == true && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreTecnico.Contains(Responsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 3:
            //                    #region BuscarSolicitudesPorFecha

            //                    DateTime A = model.FechaInicial;

            //                    //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            //                    DateTime B = model.FechaFinal.AddDays(1).AddSeconds(-1);

            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTipoSolicitud == CodigoActividad && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.FechaCreacion >= A && cd.FechaCreacion <= B).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    #endregion
            //                    break;
            //            }
            //        }
            //        else if (Supervisor(Cdg) == 2)
            //        {
            //            switch (codigotipo)
            //            {
            //                case 1:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreTecnico.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 2:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreTecnico.Contains(Responsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 3:
            //                    #region BuscarSolicitudesPorFecha

            //                    DateTime A = model.FechaInicial;

            //                    //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            //                    DateTime B = model.FechaFinal.AddDays(1).AddSeconds(-1);

            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.FechaCreacion >= A && cd.FechaCreacion <= B).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    #endregion
            //                    break;
            //            }
            //        }
            //    }

            //    #region Commeted Code

            //    //if (Supervisor(Cdg) == 1)
            //    //{
            //    //    var Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreTecnico.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();

            //    //    if (Lista.Count > 0)
            //    //    {
            //    //        smodel.TB_Detalle_Lista = Lista;
            //    //        MostrarTipoConsulta();
            //    //        MostrarEstadoConsulta();

            //    //    }
            //    //    else
            //    //    {
            //    //        smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //    //        smodel.MensajeError = "NO EXISTEN REGISTRO.";
            //    //        MostrarTipoConsulta();
            //    //        MostrarEstadoConsulta();
            //    //    }
            //    //}
            //    //else if (Supervisor(Cdg) == 2)
            //    //{
            //    //    var Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta && cd.NombreTecnico.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();

            //    //    if (Lista.Count > 0)
            //    //    {
            //    //        smodel.TB_Detalle_Lista = Lista;
            //    //        MostrarTipoConsulta();
            //    //        MostrarEstadoConsulta();

            //    //    }
            //    //    else
            //    //    {
            //    //        smodel.TB_Detalle_Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //    //        smodel.MensajeError = "NO EXISTEN REGISTRO.";
            //    //        MostrarTipoConsulta();
            //    //        MostrarEstadoConsulta();
            //    //    }
            //    //}

            //    #endregion
            //}
            //else if (model.CodigoEstadoConsulta > 0)
            //{
            //    if (CodigoDepartamento != 0)
            //    {
            //        Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoDepartamento == CodigoDepartamento && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //    }
            //    else
            //    {
            //        if (Supervisor(Cdg) == 1)
            //        {
            //            CodigoActividad = BuscarActividad(Cdg);

            //            Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTipoSolicitud == CodigoActividad && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //        }
            //        else
            //        {
            //            Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == model.CodigoEstadoConsulta).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //        }
            //    }
            //}
            //else if ((codigotipo == 1 || codigotipo == 2 || codigotipo == 3) && model.CodigoEstadoConsulta == 0)
            //{
            //    if (CodigoDepartamento != 0)
            //    {
            //        switch (codigotipo)
            //        {
            //            case 1:
            //                Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoDepartamento == CodigoDepartamento && cd.Estado == true && cd.NombreSolicitante.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                break;
            //            case 2:
            //                Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoDepartamento == CodigoDepartamento && cd.Estado == true && cd.NombreTecnico.Contains(Responsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                break;
            //            case 3:
            //                #region BuscarSolicitudesPorFecha

            //                DateTime A = model.FechaInicial;

            //                //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            //                DateTime B = model.FechaFinal.AddDays(1).AddSeconds(-1);

            //                Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoDepartamento == CodigoDepartamento && cd.FechaCreacion >= A && cd.FechaCreacion <= B).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                #endregion
            //                break;
            //        }
            //    }
            //    else
            //    {
            //        if (Supervisor(Cdg) == 1)
            //        {
            //            switch (codigotipo)
            //            {
            //                case 1:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTipoSolicitud == CodigoActividad && cd.Estado == true && cd.NombreSolicitante.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 2:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTipoSolicitud == CodigoActividad && cd.Estado == true && cd.NombreTecnico.Contains(Responsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 3:
            //                    #region BuscarSolicitudesPorFecha

            //                    DateTime A = model.FechaInicial;

            //                    //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            //                    DateTime B = model.FechaFinal.AddDays(1).AddSeconds(-1);

            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTipoSolicitud == CodigoActividad && cd.FechaCreacion >= A && cd.FechaCreacion <= B).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    #endregion
            //                    break;
            //            }
            //        }
            //        else if (Supervisor(Cdg) == 2)
            //        {
            //            switch (codigotipo)
            //            {
            //                case 1:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg && cd.NombreTecnico.Contains(Usuario)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 2:
            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg && cd.NombreTecnico.Contains(Responsable)).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    break;
            //                case 3:
            //                    #region BuscarSolicitudesPorFecha

            //                    DateTime A = model.FechaInicial;

            //                    //Para que la fecha final sea YYYY-MM-DD 11:59:59 PM
            //                    DateTime B = model.FechaFinal.AddDays(1).AddSeconds(-1);

            //                    Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg && cd.FechaCreacion >= A && cd.FechaCreacion <= B).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //                    #endregion
            //                    break;
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    if (CodigoDepartamento != 0)
            //    {
            //        Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoDepartamento == CodigoDepartamento).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //    }
            //    else
            //    {
            //        if (Supervisor(Cdg) == 1)
            //        {
            //            Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoTipoSolicitud == CodigoActividad).OrderByDescending(cd => cd.FechaCreacion).ToList();
            //        }
            //        else
            //        {
            //            Lista = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitante == Cdg).OrderByDescending(cd => cd.FechaCreacion).ToList().ToList();
            //        }
            //    }
            //}

            #endregion

            smodel.Usurio = HttpContext.Session["nombre"].ToString();

            return View("Solicitados",smodel);
        }
    }
}