﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Models.TallerPgr;
using Rotativa.Options;
using System.Threading;
using System.Globalization;
using intra.Models.GestionHumana;
using Rotativa;
using intra.Models;

namespace intra.Controllers
{
    [Authorize(Roles = "TA")]
    public class EntradaTallerPgrController : Controller
    {
        private dbIntranet dbIntra = new dbIntranet();
        private TallerPgrDM db = new TallerPgrDM();

        // GET: EntradaTallerPgr
        public ActionResult Index()
        {
            var entradaTallerPgr = db.EntradaTallerPgr.Where(x => x.EntradaEstatus != null).Include(e => e.Departamento)
                .Include(e => e.Equipo).Include(e => e.localizacion).OrderBy(x=>x.EntradaEstatus).ThenByDescending(x => x.EntradaFecha).ToList();

            var TecnicosTaller = db.TecnicoTaller.ToList();

            entradaTallerPgr.ForEach(ent => ent.Tecnico = TecnicosTaller.Where(tec => tec.TecnicoUsuario == ent.EntradaTecnicoResponsable).FirstOrDefault());

            return View(entradaTallerPgr);
        }

        // GET: EntradaTallerPgr/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EntradaTallerPgr entradaTallerPgr = db.EntradaTallerPgr.Find(id);
            if (entradaTallerPgr == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartamentoId = new SelectList(db.Departamento, "DepartamentoId", "DepartamentoDescripcion");
            return View(entradaTallerPgr);
        }
        public ActionResult ReporteTaller(int? entradaId)
        {
            if (entradaId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EntradaTallerPgr entrada = db.EntradaTallerPgr.Where(x => x.EntradaId == entradaId).FirstOrDefault();
            var empleados = dbIntra.VISTA_EMPLEADOS.Where(x => x.empleadoid == entrada.EmpleadoTallerId).FirstOrDefault();
            ViewBag.EntregadoPor = empleados.nombre.ToUpper();

            if (entrada.EntradaPersonaResponsable != "")
            {
                var empleado1 = dbIntra.VISTA_EMPLEADOS.Where(x=> x.nombre.ToUpper().Contains(entrada.EntradaPersonaResponsable)).FirstOrDefault();
                if (empleado1 != null)
                {
                    ViewBag.PersonaEntrega = empleado1.empleadoid;

                }
                else
                {
                    ViewBag.PersonaEntrega = "";
                }
            }

                    if (entrada == null)
                    {
                        return HttpNotFound();
                    }
                    return new Rotativa.ViewAsPdf(entrada)
                    {
                        PageOrientation = Orientation.Portrait,
                        PageSize = Rotativa.Options.Size.A4,
                        PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }
                    };
                }



        [HttpGet]
        // GET: EntradaTallerPgr/Create
        public ActionResult Create(EntradaTallerPgr entrada = null, int? id = null)
        {
            if (id == null)
            {
                ModelState.Clear();
            }
            var DatosEmpleado = (from A in dbIntra.VISTA_EMPLEADOS
                                     orderby A.nombre ascending
                                     select new PersonalPgrModel
                                     {
                                         EmpleadoId = A.empleadoid,
                                         Descripcion = A.nombre.ToUpper(),
                                     }).ToList();

            ViewBag.nombre = new SelectList(DatosEmpleado, "EmpleadoId", "Descripcion");
            ViewBag.EntradaTecnicoResponsable = new SelectList(db.TecnicoTaller.Where(x => x.Estado).ToList(), "TecnicoUsuario", "TecnicoNombre", Session["usuario"].ToString());
            ViewBag.TipoEquipoId = new SelectList(db.TipoEquipo, "TipoEquipoId", "TipoEquipoDescripcion");
            ViewBag.Localizacion = new SelectList(db.localizacion.OrderBy(l=>l.LocalizacionDescripcion), "LocalizacionId", "LocalizacionDescripcion");
            ViewBag.Region = new SelectList(db.Region.OrderBy(r => r.RegionDescripcion), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provincia.OrderBy(p => p.ProvinciaDescripcion), "ProvinciaId", "ProvinciaDescripcion");
            ViewBag.Municipio = new SelectList(db.Municipio.OrderBy(m=>m.MunicipioDescripcion), "MunicipioId", "MunicipioDescripcion");
            ViewBag.DepartamentoId = new SelectList(db.Departamento.OrderBy(d=>d.DepartamentoDescripcion), "DepartamentoId", "DepartamentoDescripcion");
            return View();
        }

        // POST: EntradaTallerPgr/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(EntradaTallerPgr entradaTallerPgr)
        {
            //entradaTallerPgr.EmpleadoTallerId = Request.Form["codigoEmpleado"].ToString();
            //entradaTallerPgr.EquipoId = int.Parse(Request.Form["equipoID"]);
            #region Validar Lugar o Localizacíon

            ViewBag.lugar = Request.Form["DeptoLocal"];
            Boolean lugarvalido = true;

            if (entradaTallerPgr.LocalizacionId != null && ViewBag.lugar == "1")
            {
                entradaTallerPgr.DepartamentoId = null;
            }
            if (entradaTallerPgr.DepartamentoId != null && ViewBag.lugar == "2")
            {
                entradaTallerPgr.LocalizacionId = null;
            }
            if (entradaTallerPgr.LocalizacionId == null && entradaTallerPgr.DepartamentoId == null)
            {
                lugarvalido = false;
            }

            #endregion

            //entradaTallerPgr.EntradaTecnicoResponsable = Session["usuario"].ToString();
            entradaTallerPgr.EntradaEstatus = false;
            entradaTallerPgr.EntradaFecha = DateTime.Now;
            if (entradaTallerPgr.EntradaPersonaResponsable != null) { entradaTallerPgr.EntradaPersonaResponsable.ToLower(); }

            var usuarioValido = GetEmpleado(entradaTallerPgr.EmpleadoTallerId).Data == null ? false : true;
            //var activofijovalido = !db.EntradaTallerPgr.Any(x => x.ActivoFijo == entradaTallerPgr.ActivoFijo);
            bool tecnicovalido = ValidarTecnico(entradaTallerPgr.EntradaTecnicoResponsable);

            if (ModelState.IsValid && usuarioValido && lugarvalido && /*activofijovalido &&*/ tecnicovalido)
            {
                entradaTallerPgr.EntradaPersonaResponsable?.ToUpper();
                entradaTallerPgr.EntradaRazon?.ToUpper();
                db.EntradaTallerPgr.Add(entradaTallerPgr);
                db.SaveChanges();

                //TallerPgr tallerpgr = new TallerPgr
                //{
                //    EntradaId = entradaTallerPgr.EntradaId,
                //    TallerPgrFecha = DateTime.Now
                //};

                //db.TallerPgr.Add(tallerpgr);
                //db.SaveChanges();

                return RedirectToAction("Index");
            }

            if (!lugarvalido)
            {
                ViewBag.lugarvalido = "Favor de Seleccionar la procedencia del Equipo";
            }

            //if (!activofijovalido)
            //{
            //    ViewBag.activofijovalido = "Este Activo Fijo ya fue Registrado";
            //}

            if (!tecnicovalido)
            {
                ViewBag.tecnicovalido = "Este Técnico es inválido";
            }
            return Create(entradaTallerPgr, 0);
        }

        [HttpGet]
        public ActionResult SalidaTaller(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EntradaTallerPgr entradaTallerPgr = db.EntradaTallerPgr.Where(x => x.EntradaId == id && x.EntradaEstatus == true).FirstOrDefault();

            ViewBag.DepartamentoId = new SelectList(db.Departamento, "DepartamentoId", "DepartamentoDescripcion", entradaTallerPgr.DepartamentoId);
            ViewBag.LocalizacionId = new SelectList(db.localizacion, "LocalizacionId", "LocalizacionDescripcion", entradaTallerPgr.LocalizacionId);
            return View(entradaTallerPgr);
        }

        [HttpPost]
        public ActionResult SalidaTaller(EntradaTallerPgr entradaTallerPgr)
        {
            SalidaTallerPgr salida = new SalidaTallerPgr();
            salida.EmpleadoTallerId = entradaTallerPgr.EmpleadoTallerId;

            salida.EquipoId = entradaTallerPgr.EquipoId;
            salida.SalidaTecnicoResponsable = entradaTallerPgr.EntradaTecnicoResponsable;
            salida.SalidaRazon = entradaTallerPgr.EntradaRazon;
            salida.SalidaPersonaResponsable = entradaTallerPgr.EntradaPersonaResponsable;
            salida.SalidaPersonaResponsableDocumento = entradaTallerPgr.EntradaPersonaResponsableDocumento;
            salida.SalidaPersonaResponsableTelefono = entradaTallerPgr.EntradaPersonaResponsableTelefono;
            salida.ActivoFijo = entradaTallerPgr.ActivoFijo.ToString();
            salida.DepartamentoId = entradaTallerPgr.DepartamentoId;
            salida.LocalidadId = entradaTallerPgr.LocalizacionId;
            salida.SalidaFecha = DateTime.Now;
            salida.EntradaEstatus = entradaTallerPgr.EntradaEstatus;
            db.SalidaTallerPgr.Add(salida);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        #region Obtener Provincias
        [HttpGet]
        public JsonResult GetProvincias(int regId)
        {
            var provincias = db.Provincia.Where(x => x.RegionId == regId).Select(x => new SelectListItem() { Text = x.ProvinciaDescripcion, Value = x.ProvinciaId.ToString() }).ToList();

            return Json(provincias, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Obtener Municipios
        [HttpGet]
        public JsonResult GetMunicipios(int provinId)
        {
            var municipios = db.Municipio.Where(x => x.ProvinciaId == provinId).Select(x => new SelectListItem() { Text = x.MunicipioDescripcion, Value = x.MunicipioId.ToString() }).ToList();
            return Json(municipios, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Obtener Localidades
        [HttpGet]
        public JsonResult GetLocalizaciones(int municipId)
        {
            var localizaciones = db.localizacion.Where(x => x.MunicipioId == municipId).Select(x => new SelectListItem() { Text = x.LocalizacionDescripcion, Value = x.LocalizacionId.ToString() }).ToList();
            return Json(localizaciones, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Obtener Empleado

        public JsonResult GetEmpleado(string codigoEmpleado)
        {
            var query = dbIntra.VISTA_EMPLEADOS.Where(x => x.empleadoid == codigoEmpleado).FirstOrDefault();
            return Json(query);
        }

        #endregion

        #region Obtener Equipo 
        public JsonResult GetEquipo(string serie)
        {
            if (String.IsNullOrEmpty(serie))
            {
                return Json(new { Message = "Debe ingresar una serie de equipo" });
            }

            List<string> equipo = new List<string>();

            equipo = db.Equipo.Where(x => x.EquipoSerie.StartsWith(serie.ToUpper())).Select(x => x.EquipoSerie.ToUpper()).ToList();

            //var equipo = db.Equipo.Where(x => x.EquipoSerie.Contains(serie)).Select(x => new { TipoEquipo = x.TipoEquipo.TipoEquipoDescripcion, MarcaEquipo = x.MarcaEquipo.MarcaEquipoDescripcion, Equipo = x.EquipoId }).ToList();
            return Json(equipo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEntrada(int activofijo)
        {
            //var entradas = db.EntradaTallerPgr.Where(x => x.ActivoFijo.StartsWith(activofijo.ToUpper()))
            //    .Select(x => new { ActivoFijo = x.ActivoFijo.ToUpper(), Id = x.EntradaId }).ToList();

            var entradas = db.EntradaTallerPgr.Where(x => x.EntradaEstatus != null && x.ActivoFijo.ToString().StartsWith(activofijo.ToString()))
                .Select(x => x.ActivoFijo.ToString()).ToArray();

            return Json(entradas, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDatosEquipo(string serialEquipo, int? EquipoId)
        {
            var datosEquipo = db.Equipo.Where(x => (serialEquipo == null || x.EquipoSerie == serialEquipo) && (EquipoId == null || x.EquipoId == EquipoId))
                .Select(x => new { x.TipoEquipo.TipoEquipoDescripcion, x.MarcaEquipo.MarcaEquipoDescripcion, x.EquipoId, x.EquipoSerie, x.ModeloEquipo.ModeloEquipoDescripcion }).ToList();

            //var equipo = db.Equipo.Where(x => x.EquipoSerie.Contains(serie)).Select(x => new { TipoEquipo = x.TipoEquipo.TipoEquipoDescripcion, MarcaEquipo = x.MarcaEquipo.MarcaEquipoDescripcion, Equipo = x.EquipoId }).ToList();
            return Json(datosEquipo, JsonRequestBehavior.AllowGet);
        }

        #endregion

        // GET: EntradaTallerPgr/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EntradaTallerPgr entradaTallerPgr = db.EntradaTallerPgr.Find(id);
            if (entradaTallerPgr == null)
            {
                return HttpNotFound();
            }
            entradaTallerPgr.Empleado = dbIntra.VISTA_EMPLEADOS.Where(x => x.empleadoid == entradaTallerPgr.EmpleadoTallerId).FirstOrDefault();
            var DatosEmpleado = (from A in dbIntra.VISTA_EMPLEADOS
                                 orderby A.nombre ascending
                                 select new PersonalPgrModel
                                 {
                                     EmpleadoId = A.empleadoid,
                                     Descripcion = A.nombre.ToUpper(),
                                 }).ToList();

            ViewBag.nombre = new SelectList(DatosEmpleado, "EmpleadoId", "Descripcion", entradaTallerPgr.Empleado?.empleadoid);
            ViewBag.EntradaTecnicoResponsable = new SelectList(db.TecnicoTaller.Where(x => x.Estado).ToList(), "TecnicoUsuario", "TecnicoNombre", entradaTallerPgr.EntradaTecnicoResponsable);
            ViewBag.DepartamentoId = new SelectList(db.Departamento, "DepartamentoId", "DepartamentoDescripcion", entradaTallerPgr.DepartamentoId);
            ViewBag.LocalizacionId = new SelectList(db.localizacion, "LocalizacionId", "LocalizacionDescripcion", entradaTallerPgr.LocalizacionId);
            ViewBag.EntrataStatus = new SelectList(db.EntradaTallerPgr, "EntradaEstatus", "DepartamentoDescripcion");
            return View(entradaTallerPgr);
        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "EntradaId,EmpleadoTallerId,EquipoId,EntradaTecnicoResponsable,EntradaRazon,EntradaFecha,EntradaAppUsuario," +
            "EntradaPersonaResponsable,EntradaPersonaResponsableDocumento,EntradaPersonaResponsableTelefono,LocalizacionId,DepartamentoId,ActivoFijo")] EntradaTallerPgr entradaTallerPgr)
        {
            var entradaresultado = db.EntradaTallerPgr.Find(entradaTallerPgr.EntradaId);
            var tecnicovalido = true;
            if (entradaresultado == null) { return HttpNotFound(); }

            //var test = default(ModelState);

            //if (ModelState.TryGetValue("ActivoFijo", out test))
            //{
            //    ModelState.Remove("ActivoFijo");
            //    ModelState.AddModelError("ActivoFijo", "Valor Inválido");

            //}

            if (entradaresultado.EntradaTecnicoResponsable != entradaTallerPgr.EntradaTecnicoResponsable)
            {
                tecnicovalido = ValidarTecnico(entradaTallerPgr.EntradaTecnicoResponsable);
            }

            bool? estatus = entradaresultado.EntradaEstatus = string.IsNullOrWhiteSpace(Request.Form["EntradaEstatus"]) ? default(bool?) : Request.Form["EntradaEstatus"] == "1";
            var usuarioValido = GetEmpleado(entradaTallerPgr.EmpleadoTallerId).Data == null ? false : true;

            if (ModelState.IsValid && tecnicovalido && estatus != null && (usuarioValido || entradaTallerPgr.EmpleadoTallerId == entradaresultado.EmpleadoTallerId) )
            {
                entradaresultado.EntradaEstatus = estatus;
                entradaresultado.EntradaTecnicoResponsable = Session["usuario"].ToString();
                entradaresultado.EmpleadoTallerId = entradaTallerPgr.EmpleadoTallerId;
                entradaresultado.ActivoFijo = entradaTallerPgr.ActivoFijo;
                entradaresultado.EntradaRazon = entradaTallerPgr.EntradaRazon;
                entradaresultado.EntradaTecnicoResponsable = entradaTallerPgr.EntradaTecnicoResponsable;
                entradaresultado.EntradaPersonaResponsable = entradaTallerPgr.EntradaPersonaResponsable;
                entradaresultado.EntradaPersonaResponsableDocumento = entradaTallerPgr.EntradaPersonaResponsableDocumento;
                entradaresultado.EntradaPersonaResponsableTelefono = entradaTallerPgr.EntradaPersonaResponsableTelefono;

                //int entradaId = entradaresultado.EntradaId;
                //DateTime? entradaFecha = db.EntradaTallerPgr.Where(x => x.EntradaId == entradaId).FirstOrDefault().EntradaFecha;

                //entradaTallerPgr.EntradaFecha = entradaFecha;

                //db.Entry(entradaTallerPgr).State = EntityState.Modified;

                //TallerPgr tallerPgr = db.TallerPgr.Find(entradaresultado.EntradaId);

                if (!String.IsNullOrWhiteSpace(Request.Form["TallerPgrAvance"]))
                {
                    TallerPgr tallerPgrNew = new TallerPgr();
                    tallerPgrNew.EntradaId = entradaTallerPgr.EntradaId;
                    tallerPgrNew.TallerPgrAvance = Request.Form["TallerPgrAvance"];
                    tallerPgrNew.TallerPgrFecha = DateTime.Now;

                    db.TallerPgr.Add(tallerPgrNew);
                };

                #region Validar Lugar o Localizacíon

                string lugar = Request.Form["DeptoLocal"];

                if (entradaTallerPgr.LocalizacionId != null && lugar == "1")
                {
                    entradaresultado.LocalizacionId = entradaTallerPgr.LocalizacionId;
                    entradaresultado.DepartamentoId = null;
                }

                if (entradaTallerPgr.DepartamentoId != null && lugar == "2")
                {
                    entradaresultado.DepartamentoId = entradaTallerPgr.DepartamentoId;
                    entradaresultado.LocalizacionId = null;
                }

                #endregion

                db.SaveChanges();
                if (estatus == true) { SalidaTaller(entradaresultado); }
                return RedirectToAction("Index");
            }

            if (!tecnicovalido)
            {
                ViewBag.tecnicovalido = "Este Técnico Es Inválido";
            }

            if (estatus == null)
            {
                ViewBag.estatus = "Favor de Seleccionar Un Estado";
            }

            return Edit(entradaTallerPgr.EntradaId);
        }

        public ActionResult ReporteSalida()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ReporteSalidaPdf(string activosfijosval, string dirigido)
        {
            if(activosfijosval == "")
            {
                return View("ReporteSalida");
            }
            List<int> activosfijos = activosfijosval.Split(',').Select(Int32.Parse).ToList();
            List<EntradaTallerPgr> activosfijosList = db.EntradaTallerPgr.Where(x => activosfijos.Contains(x.ActivoFijo)).ToList();


            ViewBag.dirigido = dirigido;
            ViewBag.user = Session["nombre"].ToString();
            ViewBag.count = activosfijos.Count;

            DateTime fecha = DateTime.Now;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-ES");
            ViewBag.fecha = String.Format("{0:dd} de " + "{0:MMMM} del " + "{0:yyyy}", fecha);
            return new Rotativa.ViewAsPdf(activosfijosList)
            {
                PageSize = Rotativa.Options.Size.Letter,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }
            };
            
        }

        //public ActionResult ReporteSalidaPdf(string[] entradaTallerPgr,string destino)
        //{



        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(entradaTallerPgr).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.DepartamentoId = new SelectList(db.Departamento, "DepartamentoId", "DepartamentoDescripcion", entradaTallerPgr.DepartamentoId);
        //    ViewBag.EquipoId = new SelectList(db.Equipo, "EquipoId", "EquipoSerie", entradaTallerPgr.EquipoId);
        //    ViewBag.LocalizacionId = new SelectList(db.localizacion, "LocalizacionId", "LocalizacionDescripcion", entradaTallerPgr.LocalizacionId);
        //    return View(entradaTallerPgr);
        //}


        private bool ValidarTecnico(string EntradaTecnicoResponsable)
        {
            return db.TecnicoTaller.Any(x => x.TecnicoUsuario.ToLower() == EntradaTecnicoResponsable.ToLower() && x.Estado);
        }

        // GET: EntradaTallerPgr/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EntradaTallerPgr entradaTallerPgr = db.EntradaTallerPgr.Find(id);
            if (entradaTallerPgr == null)
            {
                return HttpNotFound();
            }
            return View(entradaTallerPgr);
        }

        // POST: EntradaTallerPgr/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            EntradaTallerPgr entradaTallerPgr = db.EntradaTallerPgr.Find(id);
            db.EntradaTallerPgr.Remove(entradaTallerPgr);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
