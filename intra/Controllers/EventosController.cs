﻿using intra.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using System.IO;
using intra.Models.Servicios;

namespace intra.Controllers
{
    public class EventosController : Controller
    {
        public Correo objCorreo = new Correo();
        private Eventos eventos = new Eventos();
        private dbIntranet db = new dbIntranet();
        public static string Direccion = ConfigurationManager.AppSettings["URLINTRA"];

        // GET: Eventos
        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SolicitudEspacios([Bind(Include = "EventoId,SubActividadId,EventoFhInicio, EventoFhFinal,EventoHoraInicio,EventoHoraFinal,EventoHoraInicio,EventoCantidadPersonas,TipoEventoId,EventoNombreActividad,EventoResponsable,EventoTelefonoIP ,EventoEmail,EventoDetalles")]Eventos evento, FormCollection fc)
        {
            Sol_Registro_Solicitud Registro_Solicitud = new Sol_Registro_Solicitud();
            Registro_Solicitud.DomainUserTecnico = "";
            Registro_Solicitud.SolicitanteId = int.Parse(HttpContext.Session["empleadoId"].ToString());
            Registro_Solicitud.DomainUserSolicitante = HttpContext.Session["usuario"].ToString();
            Registro_Solicitud.Tipo_SolicitudId = int.Parse(fc["ActividadesId"]);
            var Supervisor = db.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == Registro_Solicitud.Tipo_SolicitudId && cd.Estado == true).FirstOrDefault();
            Registro_Solicitud.TecnicoId = Supervisor.SupervisorId;
            Registro_Solicitud.DepartamentoId = int.Parse(fc["DepartamentoId"]);
            Registro_Solicitud.Tipo_Sub_SolicitudId = int.Parse(fc["SubActividadesId"]);
            Registro_Solicitud.DescripcionSolicitud = fc["DescripcionSolicitud"];
            Registro_Solicitud.EstadoId = 1;
            Registro_Solicitud.Estado = true;
            Registro_Solicitud.Horas = 24;
            Registro_Solicitud.TipodeAsistencia = 4;
            Registro_Solicitud.FhFinalSolicitud = DateTime.Now;
            Registro_Solicitud.FhInicioSolicitud = DateTime.Now;
            Registro_Solicitud.FhCreacion = DateTime.Now;
            Registro_Solicitud.FhModificacion = DateTime.Now;

            HelpDeskController HelpDesk = new HelpDeskController();
            db.Sol_Registro_Solicitud.Add(Registro_Solicitud);
            db.SaveChanges();
            //EnviarCorreo(Supervisor.Correo, Usuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, Direccion + Valor, Descripcion));

            var A = db.Sol_Registro_Solicitud.OrderByDescending(cd => cd.SolicitudId).Take(1).FirstOrDefault();
            var departamento = db.Sol_Departamento.Where(d => d.DepartamentoId == Registro_Solicitud.DepartamentoId).Select(x => x.Descripcion).FirstOrDefault();
            int Valor = A.SolicitudId;
            //string Descripcion = Mth_Mostrar_Actividad(A.Tipo_SolicitudId);

            EnviarCorreo(Supervisor.Correo, Registro_Solicitud.DomainUserSolicitante, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("REASIGNACIÓN DE SOLICITUD", departamento, HttpContext.Session["nombre"].ToString(), Registro_Solicitud.DescripcionSolicitud, Direccion + Valor, Registro_Solicitud.DescripcionSolicitud));


            if (ModelState.IsValid)
            {
                evento.SolicitudId = Registro_Solicitud.SolicitudId;
                db.Eventos.Add(evento);
                db.SaveChanges();
                return RedirectToAction("Solicitud", "HelpDesk");


            }
            var error = ModelState.Values.SelectMany(e => e.Errors);

            ViewBag.TipoEvento = db.TipoEventos.ToList();
            ViewBag.Evento = evento;
            return RedirectToAction("Solicitud", "HelpDesk");

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CoberturaPrensa([Bind(Include = "EventoId,EventoTipoCobertura,SubActividadId,EventoFhInicio,EventoFhFinal,EventoHoraInicio ,EventoHoraFinal,EventoCantidadPersonas,TipoEventoId,EventoNombreActividad,EventoResponsable,EventoTelefonoIP ,EventoEmail,TipoCoberturaId,EventoDetalles")]Eventos evento, string[] TipoCoberturaId, FormCollection fc)
        {
            Sol_Registro_Solicitud Registro_Solicitud = new Sol_Registro_Solicitud();
            Registro_Solicitud.DomainUserTecnico = "";
            Registro_Solicitud.SolicitanteId = int.Parse(HttpContext.Session["empleadoId"].ToString());
            Registro_Solicitud.DomainUserSolicitante = HttpContext.Session["usuario"].ToString();
            Registro_Solicitud.Tipo_SolicitudId = int.Parse(fc["ActividadesId"]);
            var Supervisor = db.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == Registro_Solicitud.Tipo_SolicitudId && cd.Estado == true).FirstOrDefault();
            Registro_Solicitud.TecnicoId = Supervisor.SupervisorId;
            Registro_Solicitud.DepartamentoId = int.Parse(fc["DepartamentoId"]);
            Registro_Solicitud.Tipo_Sub_SolicitudId = int.Parse(fc["SubActividadesId"]);
            Registro_Solicitud.DescripcionSolicitud = fc["DescripcionSolicitud"];
            Registro_Solicitud.EstadoId = 1;
            Registro_Solicitud.Estado = true;
            Registro_Solicitud.Horas = 24;
            Registro_Solicitud.TipodeAsistencia = 4;
            Registro_Solicitud.FhFinalSolicitud = DateTime.Now;
            Registro_Solicitud.FhInicioSolicitud = DateTime.Now;
            Registro_Solicitud.FhCreacion = DateTime.Now;
            Registro_Solicitud.FhModificacion = DateTime.Now;

            HelpDeskController HelpDesk = new HelpDeskController();
            db.Sol_Registro_Solicitud.Add(Registro_Solicitud);
            db.SaveChanges();

            //EnviarCorreo(Supervisor.Correo, Usuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, Direccion + Valor, Descripcion));

            var A = db.Sol_Registro_Solicitud.OrderByDescending(cd => cd.SolicitudId).Take(1).FirstOrDefault();
            var departamento = db.Sol_Departamento.Where(d => d.DepartamentoId == Registro_Solicitud.DepartamentoId).Select(x => x.Descripcion).FirstOrDefault();
            int Valor = A.SolicitudId;
            //string Descripcion = Mth_Mostrar_Actividad(A.Tipo_SolicitudId);

            EnviarCorreo(Supervisor.Correo, Registro_Solicitud.DomainUserSolicitante, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("REASIGNACIÓN DE SOLICITUD", departamento, HttpContext.Session["nombre"].ToString(), A.DescripcionSolicitud, Direccion + Valor, Registro_Solicitud.DescripcionSolicitud));

            evento.EventoTipoCobertura = new List<EventoTipoCobertura>();
            for (int i = 0; i <= TipoCoberturaId.Length - 1; i++)
            {

                if (TipoCoberturaId != null)
                {
                    EventoTipoCobertura EventoTipoCobertura = new EventoTipoCobertura();
                    EventoTipoCobertura.TipoCoberturaId = Convert.ToInt32(TipoCoberturaId[i]);

                    evento.EventoTipoCobertura.Add(EventoTipoCobertura);

                }
            }

            if (ModelState.IsValid)
            {
                evento.SolicitudId = Registro_Solicitud.SolicitudId;
                db.Eventos.Add(evento);
                db.SaveChanges();

                return RedirectToAction("Solicitud", "HelpDesk");

            }

            var error = ModelState.Values.SelectMany(e => e.Errors);
            ViewBag.TipoEvento = db.TipoEventos.ToList();
            ViewBag.tipoCoberturas = db.TipoCobertura.ToList();
            ViewBag.Evento = evento;

            return RedirectToAction("Solicitud", "HelpDesk");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MontajeEventos([Bind(Include = "EventoId,EventoTipoCobertura,SubActividadId,EventoFhInicio,EventoFhFinal,EventoHoraInicio ,EventoHoraFinal,EventoCantidadPersonas,TipoEventoId,EventoNombreActividad,EventoResponsable,EventoTelefonoIP,EventoEmail,TipoCoberturaId,EventoDetalles")]Eventos evento, string[] TipoCoberturaId, FormCollection fc)
        {
            Sol_Registro_Solicitud Registro_Solicitud = new Sol_Registro_Solicitud();
            Registro_Solicitud.DomainUserTecnico = "";
            Registro_Solicitud.SolicitanteId = int.Parse(HttpContext.Session["empleadoId"].ToString());
            Registro_Solicitud.DomainUserSolicitante = HttpContext.Session["usuario"].ToString();
            Registro_Solicitud.Tipo_SolicitudId = int.Parse(fc["ActividadesId"]);
            var Supervisor = db.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == Registro_Solicitud.Tipo_SolicitudId && cd.Estado == true).FirstOrDefault();
            Registro_Solicitud.TecnicoId = Supervisor.SupervisorId;
            Registro_Solicitud.DepartamentoId = int.Parse(fc["DepartamentoId"]);
            Registro_Solicitud.Tipo_Sub_SolicitudId = int.Parse(fc["SubActividadesId"]);
            Registro_Solicitud.DescripcionSolicitud = fc["DescripcionSolicitud"];
            Registro_Solicitud.EstadoId = 1;
            Registro_Solicitud.Estado = true;
            Registro_Solicitud.Horas = 24;
            Registro_Solicitud.TipodeAsistencia = 4;
            Registro_Solicitud.FhFinalSolicitud = DateTime.Now;
            Registro_Solicitud.FhInicioSolicitud = DateTime.Now;
            Registro_Solicitud.FhCreacion = DateTime.Now;
            Registro_Solicitud.FhModificacion = DateTime.Now;

            HelpDeskController HelpDesk = new HelpDeskController();
            db.Sol_Registro_Solicitud.Add(Registro_Solicitud);
            db.SaveChanges();
            //EnviarCorreo(Supervisor.Correo, Usuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, Direccion + Valor, Descripcion));

            var A = db.Sol_Registro_Solicitud.OrderByDescending(cd => cd.SolicitudId).Take(1).FirstOrDefault();
            var departamento = db.Sol_Departamento.Where(d => d.DepartamentoId == Registro_Solicitud.DepartamentoId).Select(x => x.Descripcion).FirstOrDefault();

            int Valor = A.SolicitudId;
            //string Descripcion = Mth_Mostrar_Actividad(A.Tipo_SolicitudId);

            EnviarCorreo(Supervisor.Correo, Registro_Solicitud.DomainUserSolicitante, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("REASIGNACIÓN DE SOLICITUD", departamento, HttpContext.Session["nombre"].ToString(), Registro_Solicitud.DescripcionSolicitud, Direccion + Valor, Registro_Solicitud.DescripcionSolicitud));

            evento.EventoTipoCobertura = new List<EventoTipoCobertura>();
            for (int i = 0; i <= TipoCoberturaId.Length - 1; i++)
            {

                if (TipoCoberturaId != null)
                {
                    EventoTipoCobertura EventoTipoCobertura = new EventoTipoCobertura();
                    EventoTipoCobertura.TipoCoberturaId = Convert.ToInt32(TipoCoberturaId[i]);

                    evento.EventoTipoCobertura.Add(EventoTipoCobertura);

                }
            }
            if (ModelState.IsValid)
            {
                evento.SolicitudId = Registro_Solicitud.SolicitudId;
                db.Eventos.Add(evento);
                db.SaveChanges();

                return RedirectToAction("Solicitud", "HelpDesk");
            }
            var error = ModelState.Values.SelectMany(e => e.Errors);
            ViewBag.TipoEvento = db.TipoEventos.ToList();
            ViewBag.tipoCoberturas = db.TipoCobertura.ToList();
            ViewBag.Evento = evento;

            return RedirectToAction("Solicitud", "HelpDesk");

        }

        public string DescripcionCorreo(string Tema, string Departamento, string NombreCompleto, string Problema, string Link, string Descripcion)
        {


            //string NombreArchivo = Server.MapPath (@"~/EtiquetasHtml/Solicitud_nuevoServicio_PGR.html");
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/Solicitud-nuevoServicio.html");

            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {
                Archivo = sr.ReadToEnd();
            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", Tema);
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", Descripcion);

            return Archivo;
        }

    

    public void EnviarCorreo(string Supervisor, string Responsable, string Sujeto, string Descripcion)
        {

            string PrimerCorreo = "";

            string SegundoCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            if (Supervisor != "")
            {
                PrimerCorreo = Supervisor + "@PGR.GOB.DO";
            }
            else
            {
                PrimerCorreo = "";
            }

            SegundoCorreo = Responsable + "@PGR.GOB.DO";

            //ObjCorreo.EnviarCorreo(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion);

            objCorreo.EnviarCorreo_(new string[] { PrimerCorreo.Trim(), SegundoCorreo.Trim() }, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
        }

    }
}