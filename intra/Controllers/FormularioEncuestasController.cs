﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.Encuestas;
using Rotativa;
using intra.Models.ViewModel;

namespace intra.Controllers
{
    public class FormularioEncuestasController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: FormularioEncuestas
        [HttpGet]
        public ActionResult ListadoEncuestadeSatisfacion()
        {
            int año = 2018;
            var formularioEncuesta = (from r in db.Sol_Registro_Solicitud
                                     join em in db.VISTA_EMPLEADOS on  r.SolicitanteId.ToString() equals em.empleadoid
                                     join emp in db.VISTA_EMPLEADOS on r.TecnicoId.ToString() equals emp.empleadoid
                                     join act in db.Sol_Actividades on r.Tipo_SolicitudId equals act.ActividadId
                                     join subact in db.Sol_Sub_Actividades on r.Tipo_Sub_SolicitudId equals subact.SubActividadId
                                     where  r.DepartamentoId == 15 && r.EstadoId == 2 && r.FhCreacion.Year >= año
                                      select new vServiciosIntranet
                                      {
                                          SolicitudId = r.SolicitudId,
                                          Solicitante = em.nombre.ToUpper(),
                                          Responsable = emp.nombre.ToUpper(),
                                          SubActividad = subact.Descripcion,
                                          TipoSolicitud = act.Descripcion,
                                          DescripcionSolicitud = r.DescripcionSolicitud,
                                          Solucion = r.DescripcionSolucion

                                      }).ToList().Take(10);
            foreach(var item in formularioEncuesta)
            {
                var encuesta = db.FormularioEncuesta.Where(x=> x.SolicitudId == item.SolicitudId).FirstOrDefault();
                if(encuesta != null)
                {
                    TempData["EncuestaReporte"] = "ok";
                }
            }

            return View(formularioEncuesta.OrderByDescending(x=> x.SolicitudId));
        }

        [HttpPost]
        public ActionResult ListadoEncuestadeSatisfacion(string id)
        {
            int año = 2018;
            var formularioEncuesta = (from r in db.Sol_Registro_Solicitud
                                      join em in db.VISTA_EMPLEADOS on r.SolicitanteId.ToString() equals em.empleadoid
                                      join emp in db.VISTA_EMPLEADOS on r.TecnicoId.ToString() equals emp.empleadoid
                                      join act in db.Sol_Actividades on r.Tipo_SolicitudId equals act.ActividadId
                                      join subact in db.Sol_Sub_Actividades on r.Tipo_Sub_SolicitudId equals subact.SubActividadId
                                      where r.DepartamentoId == 15 && r.EstadoId == 2 && r.FhCreacion.Year >= año
                                      select new vServiciosIntranet
                                      {
                                          SolicitudId = r.SolicitudId,
                                          Solicitante = em.nombre.ToUpper(),
                                          Responsable = em.nombre.ToUpper(),
                                          SubActividad = subact.Descripcion,
                                          TipoSolicitud = act.Descripcion,
                                          DescripcionSolicitud = r.DescripcionSolicitud,
                                          Solucion = r.DescripcionSolucion



                                      }).ToList();
            return View(formularioEncuesta.OrderByDescending(x => x.SolicitudId));
        }

        // GET: FormularioEncuestas/Details/5
        // [HttpPost]
        public ActionResult ReporteEncuestaServicioos(int? id)
        {
            //id = 59511;
            var solicitante = db.Sol_Registro_Solicitud.Find(id);

            var empleados = db.VISTA_EMPLEADOS.ToList();
            var solicitanteServicio = empleados.Where(x => x.empleadoid == solicitante.SolicitanteId.ToString()).FirstOrDefault();
            ViewBag.Solicitante = solicitanteServicio.nombre.ToUpper();
            ViewBag.Responsable = empleados.Where(x => x.empleadoid == solicitante.TecnicoId.ToString()).FirstOrDefault().nombre.ToUpper();
            ViewBag.Servicio = solicitante.DescripcionSolicitud;
            ViewBag.Solucion = solicitante.DescripcionSolucion;
            ViewBag.fechaSolicitud = solicitante.FhCreacion;
            var reporte = db.FormularioEncuesta.Where(x => x.SolicitudId == id).ToList();

            if(reporte.Count() == 0)
            {
                TempData["EncuestadeSatisfacion"] = "ok";
                return RedirectToAction("ListadoEncuestadeSatisfacion");
               // return View("ListadoEncuestadeSatisfacion");
            }

            return View(reporte);
         
        }
        public ActionResult ReporteEncuestaServicios(int? id)
        {
            id = 59511;
            var solicitante = db.Sol_Registro_Solicitud.Find(id);

            var empleados = db.VISTA_EMPLEADOS.ToList();
            var solicitanteServicio = empleados.Where(x => x.empleadoid == solicitante.SolicitanteId.ToString()).FirstOrDefault();
            ViewBag.Solicitante = solicitanteServicio.nombre.ToUpper();
            ViewBag.Responsable = empleados.Where(x => x.empleadoid == solicitante.TecnicoId.ToString()).FirstOrDefault().nombre.ToUpper();
            ViewBag.Servicio = solicitante.DescripcionSolicitud;
            ViewBag.Solucion = solicitante.DescripcionSolucion;
            ViewBag.fechaSolicitud = solicitante.FhCreacion;
            var reporte = db.FormularioEncuesta.Where(x => x.SolicitudId == id).ToList();

            if (reporte.Count() == 0)
            {
                TempData["EncuestadeSatisfacion"] = "ok";
                return RedirectToAction("ListadoEncuestadeSatisfacion");
                // return View("ListadoEncuestadeSatisfacion");
            }

            return new ViewAsPdf("ReporteEncuestaServicios", reporte)
            {
                PageSize = Rotativa.Options.Size.Letter
            };

        }

        // GET: FormularioEncuestas/Create
        [AllowAnonymous]
        public ActionResult FormularioEncuesta( int id, bool EsEncuestaRespondida = false)
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }

            ViewBag.solicitud = id;
            ViewBag.EstadoId1 = 1;
            ViewBag.EstadoId2 = 2;
            ViewBag.EstadoId3 = 3;
            ViewBag.EstadoId4 = 4;

            ViewBag.PreguntasEncuestaId1 = 1;
            ViewBag.PreguntasEncuestaId2 = 2;
            ViewBag.PreguntasEncuestaId3 = 3;
            ViewBag.PreguntasEncuestaId4 = 4;

          var solicitados  = db.Sol_Registro_Solicitud.Where(x => x.SolicitudId == id).FirstOrDefault();
            ViewBag.Descripcion = solicitados.DescripcionSolicitud;
            ViewBag.Solucion = solicitados.DescripcionSolucion;

            if (!EsEncuestaRespondida)
            {
                var solicitadoss = db.FormularioEncuesta.Where(x => x.SolicitudId == id).FirstOrDefault();



                if (solicitadoss != null)
                {
                    TempData["Message"] = "Ya se ha realizado la encuesta";

                    return View();

                    //    return Json(new { success = true, message = "Gracias Por Participar de Nuestra Encuesta!", JsonRequestBehavior.AllowGet });

                }
            }  
         

            //ViewBag.PreguntasEncuestaId = new SelectList(db.PreguntasEncuesta, "PreguntasEncuestaId", "PreguntasDescripcion");
            //ViewBag.SolicitudId = new SelectList(db.Sol_Registro_Solicitud, "SolicitudId", "DomainUserSolicitante");
            return View();
        }

        // POST: FormularioEncuestas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [AllowAnonymous]
        [HttpPost]
        
        public ActionResult FormularioEncuesta(FormularioEncuesta formularioEncuesta, FormCollection fc)
        {

            int Estado1 = Convert.ToInt32(fc["Estado1"]);
            int Estado2 = Convert.ToInt32(fc["Estado2"]);
            int Estado3 = Convert.ToInt32(fc["Estado3"]);
            int Estado4 = Convert.ToInt32(fc["Estado4"]);

            int Pregunta1 = Convert.ToInt32(fc["PreguntasEncuestaId1"]);
            int Pregunta2 = Convert.ToInt32(fc["PreguntasEncuestaId2"]);
            int Pregunta3 = Convert.ToInt32(fc["PreguntasEncuestaId3"]);
            int Pregunta4 = Convert.ToInt32(fc["PreguntasEncuestaId4"]);
            int solicitar = Convert.ToInt32(fc["solicitud"]);


            ViewBag.Solicitados = db.FormularioEncuesta.Where(x => x.SolicitudId == solicitar).FirstOrDefault();

            if (ViewBag.Solicitados != null)
            {
                TempData["Encuestadoa"] = "OK";

                return RedirectToAction("index", "Home");
            }
          

                if (ModelState.IsValid)
                {

                    formularioEncuesta.PreguntasEncuestaId = Pregunta1;
                    formularioEncuesta.EstadoEncuestaId = Estado1;
                    formularioEncuesta.FechaCreacion = DateTime.Now;
                    formularioEncuesta.SolicitudId = solicitar;
                    db.FormularioEncuesta.Add(formularioEncuesta);
                    // db.SaveChanges();

                    var formEnc = new FormularioEncuesta();
                    formEnc.PreguntasEncuestaId = Pregunta2;
                    formEnc.EstadoEncuestaId = Estado2;
                    formEnc.FechaCreacion = DateTime.Now;
                    formEnc.SolicitudId = solicitar;
                    db.FormularioEncuesta.Add(formEnc);

                    var encuestas = new FormularioEncuesta();
                    encuestas.PreguntasEncuestaId = Pregunta3;
                    encuestas.EstadoEncuestaId = Estado3;
                    encuestas.FechaCreacion = DateTime.Now;
                    encuestas.SolicitudId = solicitar;
                    db.FormularioEncuesta.Add(encuestas);

                    var encuesta3 = new FormularioEncuesta();
                    encuesta3.PreguntasEncuestaId = Pregunta4;
                    encuesta3.EstadoEncuestaId = Estado4;
                    encuesta3.FechaCreacion = DateTime.Now;
                    encuesta3.SolicitudId = solicitar;
                    db.FormularioEncuesta.Add(encuesta3);

                    db.SaveChanges();

                    TempData["Solicitar"] = "OK";
                return RedirectToAction("FormularioEncuesta", new { id = formularioEncuesta.SolicitudId, EsEncuestaRespondida = true });

            }

            //return RedirectToAction("Index", "Home");


          return RedirectToAction("FormularioEncuesta", new { id = formularioEncuesta.SolicitudId, EsEncuestaRespondida = true });
           //return Json(new { success = true, message = "Gracias Por Participar de Nuestra Encuesta!", JsonRequestBehavior.AllowGet });
            //return View(formularioEncuesta);
        }


    
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
