﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models;
using System.IO;

namespace intra.Controllers
{
    public class MiPerfilAvatarsController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: MiPerfilAvatars
        [CustomAuthorize]
        public ActionResult Index()
        {

            return View();
        }

        // GET: MiPerfilAvatars/Details/5
        [CustomAuthorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            MiPerfilAvatars miPerfilAvatars = db.MiPerfilAvatars.Find(id);
            if (miPerfilAvatars == null)
            {
                return HttpNotFound();
            }
            return View(miPerfilAvatars);
        }

        // GET: MiPerfilAvatars/Create
        [CustomAuthorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: MiPerfilAvatars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Create(HttpPostedFileBase file)
        {
            MiPerfilAvatars avatar = new MiPerfilAvatars();

            //ruta de para guardar Imagenes
            string ruta = HttpRuntime.AppDomainAppPath + "Content\\images\\userImages";
            string EmpId = @Session["empleadoId"].ToString();

           

            if (file != null && file.ContentLength > 0)
            {
                string Ext = Path.GetExtension(file.FileName);
                string Archivo = file.FileName.Replace(file.FileName, EmpId + Ext);              
                string path = Path.Combine(ruta, Archivo);

                if (EmpId != "" && Ext == ".jpeg" || Ext == ".jpg" || Ext == ".png" || Ext == ".JPEG" ||
                    Ext == ".JPG" || Ext == ".PNG")
                {

                    file.SaveAs(path);
                    avatar.avatar = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/userImages/" + Archivo;
                    avatar.codigoEmpleado = EmpId;
                }
                else
                {
                    ViewBag.MsjError = "Solo Se Admiten Imagenes en Formato JPEG & PNG";
                    return View("Create", avatar);
                }
            }
            else
            {
                //Imagen por Defecto en caso de no subir ninguna
                //avatar.avatar = string.Concat(ruta, "user.png");
                ViewBag.MsjError = "Debe Cargar Una Imagen";
                return View("Create", avatar);
            }

            if (ModelState.IsValid)
            {
                db.MiPerfilAvatars.Add(avatar);
                db.SaveChanges();

                HttpContext.Session["avatar"] = avatar.avatar;

                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Subio una imagen a su perfil");
                return RedirectToAction("Index","Home");
            }

            
            return View("Create", avatar);                
            
        }

        // GET: MiPerfilAvatars/Edit/5
        [CustomAuthorize]
        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MiPerfilAvatars miPerfilAvatars = db.MiPerfilAvatars.Where(x=>x.codigoEmpleado==id).FirstOrDefault();
            if (miPerfilAvatars == null)
            {
                return HttpNotFound();
            }
            return View(miPerfilAvatars);
        }

        // POST: MiPerfilAvatars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Edit(HttpPostedFileBase file, FormCollection vd)
        {
            //ruta de para guardar Imagenes

            string ruta = HttpRuntime.AppDomainAppPath + "Content\\images\\userImages";
            string EmpId = @Session["empleadoId"].ToString();

            MiPerfilAvatars avatar = db.MiPerfilAvatars.Where(x => x.codigoEmpleado == EmpId).FirstOrDefault();
            
            if (file != null && file.ContentLength > 0)
            {
                string Ext = Path.GetExtension(file.FileName);
                string Archivo = file.FileName.Replace(file.FileName, EmpId + Ext);
                string path = Path.Combine(ruta, Archivo);

                if (EmpId != "" && Ext == ".jpeg" || Ext == ".jpg" || Ext == ".png")
                {
                    FileInfo fi = new FileInfo(avatar.avatar);

                    //var server_path = Server.MapPath(ruta);
                    var server_path = ruta;

                    try
                    {
                        System.IO.File.Delete(Path.Combine(server_path, fi.Name));

                        file.SaveAs(path);
                        avatar.avatar = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/userImages/" + Archivo;
                        avatar.codigoEmpleado = EmpId;
                    }catch(Exception)
                    {
                        return RedirectToAction("Create");
                    }
                }
                else
                {
                    ViewBag.MsjError = "Solo Se Admiten Imagenes en Formato JPEG & PNG";
                    return View("Edit", avatar);
                }
            }
            else
            {
                //Imagen por Defecto en caso de no subir ninguna
                //avatar.avatar = string.Concat(ruta, "user.png");
                ViewBag.MsjError = "Debe Cargar Una Imagen";
                return View("Edit", avatar);
            }

            if (ModelState.IsValid)
            {              
                db.Entry(avatar).State = EntityState.Modified;
                db.SaveChanges();

                HttpContext.Session["avatar"] = avatar.avatar;

                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Cambio su imagen de perfil");

                return RedirectToAction("Index", "MiPerfil");
            }


            return View("Edit", avatar);
        }

        // GET: MiPerfilAvatars/Delete/5
        [CustomAuthorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MiPerfilAvatars miPerfilAvatars = db.MiPerfilAvatars.Find(id);
            if (miPerfilAvatars == null)
            {
                return HttpNotFound();
            }
            return View(miPerfilAvatars);
        }

        // POST: MiPerfilAvatars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            MiPerfilAvatars miPerfilAvatars = db.MiPerfilAvatars.Find(id);
            db.MiPerfilAvatars.Remove(miPerfilAvatars);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
