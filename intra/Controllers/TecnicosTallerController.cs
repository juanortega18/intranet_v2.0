﻿using intra.Models.TallerPgr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    public class TecnicosTallerController : Controller
    {
        private dbIntranet dbIntra = new dbIntranet();
        private TallerPgrDM db = new TallerPgrDM();

        // GET: Tecnicos
        public ActionResult Index()
        {
            var tecnicos = db.TecnicoTaller.ToList();

            return View(tecnicos);
        }

        [HttpPost]
        public JsonResult AgregarTecnico(int tecnicoId) {

            var persona = dbIntra.VISTA_EMPLEADOS.FirstOrDefault(x => x.empleadoid == tecnicoId.ToString());

            if (persona == null) { return Json("Un error a ocurrido, la persona no fue encontrada"); }

            if (db.TecnicoTaller.FirstOrDefault(x => x.TecnicoCodigo == tecnicoId.ToString()) != null) { return Json("Esta persona ya es un Tecnico del Taller"); }

            var tecnicotaller = new TecnicoTaller();

            TipoSolicitudesController tiposolicitudes = new TipoSolicitudesController();

            tecnicotaller.TecnicoCodigo = tecnicoId.ToString();
            tecnicotaller.TecnicoNombre = persona.nombre;
            tecnicotaller.Estado = true;
            tecnicotaller.TecnicoUsuario = tiposolicitudes.BuscarUsuario(persona.cedula).samAccountName;

            db.TecnicoTaller.Add(tecnicotaller);

            db.SaveChanges();

            return Json("Guardado");
        }

        [HttpPost]
        public JsonResult CambiarEstado(int id)
        {
            TecnicoTaller tecnicoTaller = db.TecnicoTaller.Find(id);

            if (tecnicoTaller == null) { return Json("Error"); }

            if (tecnicoTaller.Estado == true && db.TecnicoTaller.Where(x => x.Estado).ToList().Count <= 1)
            { return Json("Incambiable"); }

            tecnicoTaller.Estado = !tecnicoTaller.Estado;
            db.SaveChanges();

            if (tecnicoTaller.Estado) { return Json("Activado"); }

            return Json("Desactivado");
        }

    }
}