﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;
using System.IO;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
    public class DocIdVisualController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: DocIdVisual
        public ActionResult Index()
        {
            string EmpId = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                {
                    EmpId = HttpContext.Session["empleadoId"].ToString();
                }
                else
                {
                    EmpId = "public_user";
                }
            }
            catch (Exception)
            {
                EmpId = "public_user";
            }

            ViewBag.access = EmpId;

            var documents = db.DocIdVisual.ToList();

            return View(documents);
        }

        // GET: DocIdVisual/Details/5
        public ActionResult Details(int? id)
        {
            if (id != null)
            {
                string EmpId = string.Empty;

                try
                {
                    if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                    {
                        EmpId = HttpContext.Session["empleadoId"].ToString();
                    }
                    else
                    {
                        EmpId = "public_user";
                    }
                }
                catch (Exception)
                {
                    EmpId = "public_user";
                }

                ViewBag.access = EmpId;

                DocIdVisual document = db.DocIdVisual.Find(id);

                if (document.DocIdVisualArchivo.Length == 0)
                {
                    ViewBag.EmptyFile = "Empty";
                }
                else
                {
                    ViewBag.EmptyFile = "Full";
                }

                string imageBase64 = Convert.ToBase64String(document.DocIdVisualArchivo);

                if (document.DocIdVisualExtension.ToLower() == ".pdf")
                {
                    ViewBag.FileSource = string.Format("data:application/pdf;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/pdf.png";
                }
                else if (document.DocIdVisualExtension.ToLower() == ".doc")
                {
                    ViewBag.FileSource = string.Format("data:application/msword;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/doc.png";
                }
                else if (document.DocIdVisualExtension.ToLower() == ".docx")
                {
                    ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/doc.png";
                }
                else if (document.DocIdVisualExtension.ToLower() == ".xls")
                {
                    ViewBag.FileSource = string.Format("data:application/vnd.ms-excel;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";
                }
                else if (document.DocIdVisualExtension.ToLower() == ".xlsx")
                {
                    ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";
                }
                else if (document.DocIdVisualExtension.ToLower() == ".ppt")
                {
                    ViewBag.FileSource = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/ppt.png";
                }
                else if (document.DocIdVisualExtension.ToLower() == ".pptx")
                {
                    ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.presentationml.presentation;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/ppt.png";
                }
                else if (document.DocIdVisualExtension.ToLower() == ".txt")
                {
                    ViewBag.FileSource = string.Format("data:text/plain;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/txt.png";
                }
                else
                {
                    ViewBag.FileSource = string.Format("data:text/plain;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/file.png";
                    ViewBag.FileSource = "#";
                }

                return View(document);
            }

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "DocIdVisual (Admin)")]
        // GET: DocIdVisual/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DocIdVisual/Create
        [HttpPost]
        public ActionResult Create(DocIdVisual view_object)
        {
            try
            {
                if(!ModelState.IsValid)
                {
                    return View(view_object);
                }

                if(view_object==null)
                {
                    return View(view_object);
                }

                if(view_object.ArchivoSubido == null || view_object.ArchivoSubido.ContentLength <= 0)
                {
                    ModelState.AddModelError("ArchivoSubido", "Debe subir un archivo.");
                    return View(view_object);
                }

                var is_in = db.DocIdVisual.Where(x => x.DocIdVisualNombre == view_object.DocIdVisualNombre).FirstOrDefault();

                if(is_in!=null)
                {
                    ModelState.AddModelError("DocIdVisualNombre", "Ya ha subido este archivo.");
                }

                view_object.DocIdVisualExtension = Path.GetExtension(view_object.ArchivoSubido.FileName);

                using (var ms = view_object.ArchivoSubido.InputStream)
                {
                    byte[] array = new byte[ms.Length];

                    ms.Read(array, 0, array.Length);

                    view_object.DocIdVisualArchivo = array;
                }

                view_object.DocIdVisualFhCreacion = DateTime.Now;
                view_object.DocIdVisualUsuarioCreador = Convert.ToInt32(HttpContext.Session["empleadoId"]);
                view_object.DocIdVisualUsuarioCreadorNombre = Session["usuario"].ToString();

                db.DocIdVisual.Add(view_object);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(view_object);
            }
        }

        [Authorize(Roles = "DocIdVisual (Admin)")]
        // GET: DocIdVisual/Edit/5
        public ActionResult Edit(int? id)
        {
            if(id!=null)
            {
                DocIdVisual document = db.DocIdVisual.Find(id);

                if(document.DocIdVisualArchivo.Length == 0)
                {
                    ViewBag.EmptyFile = "Empty";
                }
                else
                {
                    ViewBag.EmptyFile = "Full";
                }

                string imageBase64 = Convert.ToBase64String(document.DocIdVisualArchivo);

                if (document.DocIdVisualExtension.ToLower() == ".pdf")
                {
                    ViewBag.FileSource = string.Format("data:application/pdf;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/pdf.png";
                }
                else if (document.DocIdVisualExtension.ToLower() == ".doc")
                {
                    ViewBag.FileSource = string.Format("data:application/msword;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/doc.png";
                }
                else if(document.DocIdVisualExtension.ToLower() == ".docx")
                {
                    ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/doc.png";
                }
                else if (document.DocIdVisualExtension.ToLower() == ".xls" )
                {
                    ViewBag.FileSource = string.Format("data:application/vnd.ms-excel;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";
                }
                else if(document.DocIdVisualExtension.ToLower() == ".xlsx")
                {
                    ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";
                }
                else if (document.DocIdVisualExtension.ToLower() == ".ppt")
                {
                    ViewBag.FileSource = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/ppt.png";
                }
                else if (document.DocIdVisualExtension.ToLower() == ".pptx")
                {
                    ViewBag.FileSource = string.Format("data:application/vnd.openxmlformats-officedocument.presentationml.presentation;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/ppt.png";
                }
                else if (document.DocIdVisualExtension.ToLower() == ".txt")
                {
                    ViewBag.FileSource = string.Format("data:text/plain;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/txt.png";
                }
                else
                {
                    ViewBag.FileSource = string.Format("data:text/plain;base64,{0}", imageBase64);
                    ViewBag.IconImage = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/file.png";
                    ViewBag.FileSource = "#";
                }

                return View(document);
            }

            return RedirectToAction("Index");
        }

        // POST: DocIdVisual/Edit/5
        [HttpPost]
        public ActionResult Edit(DocIdVisual view_object)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(view_object);
                }

                if (view_object == null)
                {
                    return View(view_object);
                }

                if (view_object.ArchivoSubido == null || view_object.ArchivoSubido.ContentLength <= 0)
                {
                    ModelState.AddModelError("ArchivoSubido", "Debe subir un archivo.");
                    return View(view_object);
                }
                
                view_object.DocIdVisualExtension = Path.GetExtension(view_object.ArchivoSubido.FileName);

                using (var ms = view_object.ArchivoSubido.InputStream)
                {
                    byte[] array = new byte[ms.Length];

                    ms.Read(array, 0, array.Length);

                    view_object.DocIdVisualArchivo = array;
                }

                view_object.DocIdVisualFhModificacion = DateTime.Now;
                view_object.DocIdVisualUsuarioModificador = Convert.ToInt32(HttpContext.Session["empleadoId"]);
                view_object.DocIdVisualUsuarioModificadorNombre = Session["usuario"].ToString();

                db.Entry(view_object).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }
            catch
            {
                return View(view_object);
            }
        }

        // GET: DocIdVisual/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DocIdVisual/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult DeleteFile(int id)
        {
            try
            {
                var document = db.DocIdVisual.Find(id);

                document.DocIdVisualArchivo = new byte[0];
                document.DocIdVisualExtension = "";

                db.Entry(document).State = System.Data.Entity.EntityState.Modified;
                
                db.SaveChanges();

                return Json("success");
            }
            catch (Exception)
            {
                return Json("error");
            }
        }
    }
}
