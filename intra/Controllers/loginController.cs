﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.DirectoryServices;
using System.Configuration;
using System.Data.SqlClient;
using intra.Models.Cumpleanos;
using System.IO;
using PagedList;
using System.Xml.Linq;
using intra.Models.AvisosActividades;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
    [AllowAnonymous]
    public class loginController : Controller
    {
        dbIntranet db = new dbIntranet();

        public ActionResult Index(string url)
        {
            return View();
        }
        [CustomAuthorize]
        public ActionResult vNovedadesGestionHumana()
        {
            List<Avisos> avisos = new List<Avisos>();

            try
            {
                string EmpId = string.Empty;

                ViewBag.access = EmpId;

                avisos = db.Avisos.Where(x => x.AvisosEstatus != 0).OrderByDescending(c => c.AvisosId).Take(5).ToList();

                RegisterLogs rl = new RegisterLogs(EmpId, "Ingreso al Modulo de Avisos");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(avisos);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]       
        public ActionResult Index(FormCollection form)
        {
            string ReturnUrl = Convert.ToString(Request.QueryString["url"]);
            if (ModelState.IsValid)
            {
                bool entra = true;
                DirectoryEntry de = new DirectoryEntry(ConfigurationManager.AppSettings["ldapserver"], form["usuario"], form["clave"]);
                try
                {
                    DirectorySearcher searcher = new DirectorySearcher(de, "(sAMAccountName=" + form["usuario"] + ")" );                                 
                        searcher.FindOne();
                    }
                catch(Exception ex)
                {
                    entra = false;

                    if (ex.Message == "El nombre de usuario o la contraseña son correctos.\r\n")
                        ViewBag.MensajeError = "Crendenciales incorrectas";
                    else
                      TempData["MensajeError"] = "El nombre de usuario o la contraseña son incorrectos";
                    return RedirectToAction("Index", "Login");

                }

                try {

                    if (entra)
                    {
                        using (dbIntranet rh = new dbIntranet())
                        {
                            if (ConfigurationManager.AppSettings["DebugSetting"] == "true") {

                              //  form["usuario"] = "jonathan.bobea";

                            }

                            SqlParameter param1 = new SqlParameter("@ulpad", form["usuario"]);
                            var ulpad = rh.SP_USERS_LDAP.SqlQuery("sp_users_ldap @ulpad", param1).FirstOrDefault();

                            if (ulpad.employeeid == null || ulpad.employeeid.Trim() == "") {
                                ViewBag.MensajeError = "No existe esta cedula en el dominio, comuniquese con DTI";
                                return View();
                            }

                            if (ulpad.mail == null)
                            {
                                ViewBag.MensajeError = "Usted no tiene correo asigando en el dominio, comuniquese con DTI";
                                return View();
                            }

                            if (ulpad != null)
                            {
                                var empl = rh.VISTA_EMPLEADOS.FirstOrDefault(x => x.cedula == ulpad.employeeid.Trim());

                                if (empl == null)
                                {
                                    ViewBag.MensajeError = "No se pudo encontrar su código de empleado por su cédula, comuniquese con DTI";
                                    return View();
                                }

                                HttpContext.Session["nombre"] = ulpad.displayName;

                                HttpContext.Session["cedula"] = ulpad.employeeid.Trim();
                                HttpContext.Session["usuario"] = form["usuario"].ToLower();

                                HttpContext.Session["correo"] = ulpad.mail.Trim();

                                if (string.IsNullOrEmpty(ulpad.department))
                                    HttpContext.Session["departamento"] = "No Tiene Asignado en AD";
                                else
                                    HttpContext.Session["departamento"] = ulpad.department;

                                setAuthIntra au = new setAuthIntra(HttpContext, form["usuario"]);

                                if (empl != null)
                                {

                                    HttpContext.Session["empleadoId"] = empl.empleadoid.ToString();
                                    HttpContext.Session["departamento"] = empl.departamento;
                                    HttpContext.Session["departamentoId"] = empl.departamentoid;
                                    HttpContext.Session["dependencia"] = empl.dependencia;
                                    HttpContext.Session["dependenciaId"] = empl.dependenciaid;
                                    HttpContext.Session["cargo"] = empl.cargo;
                                    HttpContext.Session["Sexo"] = empl.Sexo;

                                    //Persona que cumple anios
                                    Vw_Mostrar_Empleados_Cumpleanos cumple = new Vw_Mostrar_Empleados_Cumpleanos();
                                    HttpContext.Session["cumple"] = cumple.Dia;

                                    string EmpCargo = HttpContext.Session["empleadoId"].ToString();
                                    string emp = HttpContext.Session["nombre"].ToString();

                                    var name = emp.Split(' ');
                                    HttpContext.Session["PrimerNombre"] = name[0].ToUpper();

                                    //Para Mostrar Foto de Perfil                             
                                    var FotoPerfil = db.MiPerfilAvatars.Where(x => x.codigoEmpleado == EmpCargo).OrderByDescending(x => x.id).Take(1).Select(f => f.avatar).SingleOrDefault();

                                    if (string.IsNullOrEmpty(FotoPerfil))
                                    {
                                        /*HttpContext.Session["avatar"] = "~/Content/images/user.png";*/ ///Imagen Por Defecto Cuando No tiene Cargada
                                        HttpContext.Session["avatar"] = string.Concat(System.Configuration.ConfigurationManager.AppSettings["intranet"], "Content/images/user.png");
                                        HttpContext.Session["avatarEstado"] = 1;
                                    }
                                    else
                                    {
                                        HttpContext.Session["avatar"] = FotoPerfil; //Ruta de Imagen Guardada en la BD
                                        HttpContext.Session["avatarEstado"] = 0;
                                    }

                                    //HttpContext.Session["sexo"] = empl.sexo;

                                }


                                RegisterLogs rl = new RegisterLogs(empl.empleadoid.ToString(), "Inicio de Sesión");

                                if (string.IsNullOrEmpty(ReturnUrl))
                                {
                                    return RedirectToAction("Index", "Home");
                                }
                                else { return Redirect(ReturnUrl); }
                            }
                        }
                    }

                }
                catch (Exception e)
                {
                    e.ToString();
                    ViewBag.MensajeError = "Ha ocurrido un error, comuniquese con DTI";
                }

            }
            return View();
        }
    }
}