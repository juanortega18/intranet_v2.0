﻿using intra.Models;
using intra.Models.GestionHumana;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace intra.Controllers
{
    public class CustomController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Session["empleadoId"] == null) {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "login",
                    action = "Index",
                    url = new { url = Request.Url.AbsoluteUri }
                }));
                //if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }

            }
        }
    }

    [Authorize(Roles = "EC")]
    public class EmpleadosContratadosController : CustomController
    {
        dbIntranet db = new dbIntranet();

        // GET: EmpleadosContratados
        [OutputCache(Duration = 0)]
        public ActionResult Index()
        {
            //var lista = new List<EmpleadosContratados>();
            //lista = db.empleadoscontratados.OrderBy(x=>x.Empleadoid).ToList();
            return View(db.empleadoscontratados.OrderBy(x => x.Empleadoid).ToList());
        }

        // GET: DatosPersonas/Create
        public ActionResult Create()
        {
            getSelectLists("10001");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmpleadosContratados EmpleadosContratado)
        {
            EmpleadosContratado.Estatus = 0;

            DateTime Fecha_Ingreso , Fecha_Nacimiento;

            bool fecha_ingreso_valida = DateTime.TryParseExact(Request.Form["Fecha_Ingreso"], "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out Fecha_Ingreso);
            bool fecha_nacimiento_valida  = DateTime.TryParseExact(Request.Form["Fecha_Nacimiento"], "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out Fecha_Nacimiento);

            if (fecha_ingreso_valida) EmpleadosContratado.Fechaingreso = Fecha_Ingreso; 
            if (fecha_nacimiento_valida) EmpleadosContratado.Cumpleano = Fecha_Nacimiento; 

            EmpleadosContratado.Cargo = db.vw_Cargos.Find(EmpleadosContratado.Cargoid).Descripcion;
            EmpleadosContratado.Departamento = db.vw_Departamentos.FirstOrDefault(x => x.DeptoID.ToString() == EmpleadosContratado.departamentoid && x.DependenciasID == EmpleadosContratado.DependenciaId).Descripcion;
            EmpleadosContratado.Dependencia = db.vw_Dependencias.FirstOrDefault(x => x.DependenciaID.ToString() == EmpleadosContratado.DependenciaId).Descripcion;
            var exitsCedula = db.VISTA_EMPLEADOS.Any(x => x.cedula == EmpleadosContratado.Cedula);

            if (ModelState.IsValid && !exitsCedula && fecha_ingreso_valida && fecha_nacimiento_valida /* && !exitsCodigoEmp  && Idvalido*/)
            {
                try
                {
                    EmpleadosContratado.Nombre = EmpleadosContratado.Nombre.ToUpper();

                    db.empleadoscontratados.Add(EmpleadosContratado);
                    db.SaveChanges();

                    RegisterLogs log = new RegisterLogs(HttpContext.Session["empleadoId"].ToString(), "Creo el empleado contratado con la cédula " + EmpleadosContratado.Cedula);

                    EmpleadosContratado.Empleadoid = EmpleadosContratado.id_persona.ToString();

                    db.SaveChanges();

                    TempData["toastrMessage"] = "Guardado Correctamente!";
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    TempData["toastrMessage"] = "Ha ocurrido un Error, favor de intentarlo mas tarde.";
                }
            }

            ViewBag.nombre = Request.Form["DisplayNombre"];
            ViewBag.validada = Request.Form["CedulaValidada"];

            if (!fecha_ingreso_valida)
                ModelState.AddModelError("Fechaingreso", "Fecha de ingreso inválida");

            if (!fecha_nacimiento_valida)
                ModelState.AddModelError("Cumpleano", "Fecha de nacimiento inválida");

            if (exitsCedula)
                ModelState.AddModelError("Cedula", "Esta cédula ya existe");

            getSelectLists();
            return View(EmpleadosContratado);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmpleadosContratados EmpleadosContratados = db.empleadoscontratados.Find(id);
            if (EmpleadosContratados == null)
            {
                return HttpNotFound();
            }
            ViewBag.nombre = EmpleadosContratados.Nombre;
            ViewBag.validada = EmpleadosContratados.Cedula;
            getSelectLists("10001");
            return View(EmpleadosContratados);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmpleadosContratados EmpleadosContratado)
        {
            DateTime Fecha_Ingreso, Fecha_Nacimiento;

            bool fecha_ingreso_valida = DateTime.TryParseExact(Request.Form["Fecha_Ingreso"], "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out Fecha_Ingreso);
            bool fecha_nacimiento_valida = DateTime.TryParseExact(Request.Form["Fecha_Nacimiento"], "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out Fecha_Nacimiento);

            if (fecha_ingreso_valida) EmpleadosContratado.Fechaingreso = Fecha_Ingreso;
            if (fecha_nacimiento_valida) EmpleadosContratado.Cumpleano = Fecha_Nacimiento;

            EmpleadosContratado.Cargo = db.vw_Cargos.Find(EmpleadosContratado.Cargoid).Descripcion;
            EmpleadosContratado.Departamento = db.vw_Departamentos.FirstOrDefault(x => x.DeptoID.ToString() == EmpleadosContratado.departamentoid && x.DependenciasID == EmpleadosContratado.DependenciaId).Descripcion;
            EmpleadosContratado.Dependencia = db.vw_Dependencias.FirstOrDefault(x => x.DependenciaID.ToString() == EmpleadosContratado.DependenciaId).Descripcion;

            var exitsCedula = db.VISTA_EMPLEADOS.Any(x => x.empleadoid != EmpleadosContratado.Empleadoid && x.cedula == EmpleadosContratado.Cedula);

            if (ModelState.IsValid && !exitsCedula && fecha_ingreso_valida && fecha_nacimiento_valida)
            {
                try
                {
                    EmpleadosContratado.Nombre = EmpleadosContratado.Nombre.ToUpper();
                    db.Entry(EmpleadosContratado).State = EntityState.Modified;
                    db.SaveChanges();

                    RegisterLogs log = new RegisterLogs(HttpContext.Session["empleadoId"].ToString(), "Edito el empleado contratado con la cédula " + EmpleadosContratado.Cedula);

                    TempData["toastrMessage"] = "Editado Correctamente!";
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    TempData["toastrMessage"] = "Ha ocurrido un Error, favor de intentarlo mas tarde.";
                }

            }

            ViewBag.validada = Request.Form["CedulaValidada"];
            ViewBag.nombre = Request.Form["DisplayNombre"];

            if (!fecha_ingreso_valida)
                ModelState.AddModelError("Fechaingreso", "Fecha de ingreso inválida");

            if (!fecha_nacimiento_valida)
                ModelState.AddModelError("Cumpleano", "Fecha de nacimiento inválida");

            if (exitsCedula) ModelState.AddModelError("Cedula", "Esta cédula ya existe");

            getSelectLists();
            return View(EmpleadosContratado);
        }

        [HttpPost]
        public JsonResult CambiarEstado(int id)
        {
            EmpleadosContratados EmpleadoContratado = db.empleadoscontratados.Find(id);

            if (EmpleadoContratado == null) return Json("Error");

            bool estado = EmpleadoContratado.Estatus == 0;

            EmpleadoContratado.Estatus = estado ? 1 : 0;
            db.SaveChanges();

            RegisterLogs log = new RegisterLogs(HttpContext.Session["empleadoId"].ToString(),
                (!estado ? "Activo el empleado contratado con la cedula " : "Desactivado el empleado contratado con la cédula ")
                + EmpleadoContratado.Cedula);

            return Json(!estado ? "Activado" : "Desactivado");
        }

        [HttpPost]
        public JsonResult ValidarCedula(string cedula)
        {
            if (db.VISTA_EMPLEADOS.Any(x => x.cedula == cedula))
                return Json("Esta persona ya esta registrada.");

            using (var WebClient = new WebClient())
            {
                try
                {
                    var url = Utilities.UrlWebService(cedula);
                    var result = WebClient.DownloadString(url);
                    var objeto = JsonConvert.DeserializeObject(result, typeof(Persona)) as Persona;

                    if (objeto.Respuesta == "OK")
                    {
                        return Json(objeto);
                    }
                    else if (objeto.Respuesta == "Error")
                    {
                        return Json("Error en la validación");
                    }
                    else if (objeto.Respuesta == "TimeOut")
                    {
                        return Json("Tiempo de espera agotado. Intente de nuevo más tarde.");
                    }
                    else if (objeto.Respuesta == "NotFound")
                    {
                        return Json("No se ha encontrado la cédula digitada. Digite una existente.");
                    }
                    else if (objeto.Respuesta == "InvalidRequest")
                    {
                        return Json("Formato de cédula invalido. Por favor, digite uno correcto.");
                    }
                    else
                    {
                        return Json("Se ha producido un error. Intente más tarde.");
                    }
                }
                catch (Exception)
                {
                    return Json("Se ha producido un error. Intente más tarde.");
                }
            }
        }

        public void getSelectLists(string dependencia=null)
        {
            ViewBag.departamentos = new SelectList(db.Sol_Departamento.OrderBy(x=>x.Descripcion), "DepartamentoId", "Descripcion");

            var dependecias = db.vw_Dependencias.GroupBy(d => d.DependenciaID).Select(d => d.FirstOrDefault()).OrderBy(d=>d.Descripcion);

            ViewBag.dependencia = new SelectList(dependecias, "DependenciaId", "Descripcion", dependencia);
            ViewBag.cargos = new SelectList(db.vw_Cargos.OrderBy(x => x.Descripcion), "CargoId", "Descripcion");
        }
    }
}