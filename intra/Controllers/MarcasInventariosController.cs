﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.Inventario;

namespace intra.Controllers
{
    public class MarcasInventariosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: MarcasInventarios
        [CustomAuthorize]
        [Authorize(Roles="IEA,IEWR")]
        public ActionResult Index()
        {
            return View(db.MarcasInventario.ToList());
        }

        // GET: MarcasInventarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarcasInventario marcasInventario = db.MarcasInventario.Find(id);
            if (marcasInventario == null)
            {
                return HttpNotFound();
            }
            return View(marcasInventario);
        }

        // GET: MarcasInventarios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MarcasInventarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles="IEA,IEWR")]
        public ActionResult Create([Bind(Include = "MarcaId,MarcaDescripción,MarcaFecha,MarcaUsuario,MarcaTipo")] MarcasInventario marcasInventario)
        {

            marcasInventario.MarcaFecha = DateTime.Now;
            marcasInventario.MarcaUsuario = Session["usuario"].ToString();

            if (ModelState.IsValid)
            {
                db.MarcasInventario.Add(marcasInventario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(marcasInventario);
        }

        // GET: MarcasInventarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarcasInventario marcasInventario = db.MarcasInventario.Find(id);
            if (marcasInventario == null)
            {
                return HttpNotFound();
            }
            return View(marcasInventario);
        }

        // POST: MarcasInventarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles="IEA,IEWR")]
        public ActionResult Edit([Bind(Include = "MarcaId,MarcaDescripción,MarcaFecha,MarcaUsuario,MarcaTipo")] MarcasInventario marcasInventario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(marcasInventario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(marcasInventario);
        }

        // GET: MarcasInventarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarcasInventario marcasInventario = db.MarcasInventario.Find(id);
            if (marcasInventario == null)
            {
                return HttpNotFound();
            }
            return View(marcasInventario);
        }

        // POST: MarcasInventarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MarcasInventario marcasInventario = db.MarcasInventario.Find(id);
            db.MarcasInventario.Remove(marcasInventario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
