﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Xml.Linq;
using System.Web.Services;
using System.Xml;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using intra.Models.Denuncias;

namespace intra.Controllers
{
    public class FormularioDenunciasController : Controller
    {
        static HttpClient client = new HttpClient();

        [CustomAuthorize]
        // GET: FormularioDenuncias
        public ActionResult Index()
        {
            return View("");
        }
        [HttpPost]     
        public ActionResult Registrar(HttpPostedFileBase file, string NombreDenunciante, string Cedula,
                                        string TelefonoDenunciante, string EmailDenunciante, 
                                        string DireccionDenunciante, int TipoDenuncia, string NombreDenunciado,
                                        string Apellido1Denunciado, string Apellido2Denunciado, string CedulaDenunciado, 
                                        string AliasDenunciado, string DireccionDenunciado, string Denuncia)
        {

            ViewBag.Denuncias = "Error";
            using (DenunciaContext _contex = new DenunciaContext())
            {
            try
            {
                    var denunciante = new Denunciante();
                    var denunciado = new Denunciado();
                    var denuncia = new Denuncia();

                    denunciante.TipoIdentificacionId = 1;
                    denunciante.NumeroDocumento = Cedula;
                    denunciante.DenuncianteNombres = NombreDenunciante;
                    denunciante.DenuncianteApellido1 = TelefonoDenunciante;
                    denunciante.DenuncianteCorreo = EmailDenunciante;
                    denunciante.DenuncianteCalle = DireccionDenunciante;
                    denunciante.Denuncias = null;

                    _contex.Denunciantes.Add(denunciante);
                    _contex.SaveChanges();

                    denunciado.TipoIdentificacionId = 1;
                    denunciado.NumeroDocumento = CedulaDenunciado;
                    denunciado.DenunciadoNombres = NombreDenunciado;
                    denunciado.DenunciadoApellido1 = Apellido1Denunciado;
                    denunciado.DenunciadoApellido2 = Apellido2Denunciado;
                    denunciado.DenunciadoAlias = AliasDenunciado;
                    denunciado.DenunciadoCalle = DireccionDenunciado;

                    _contex.Denunciados.Add(denunciado);
                    _contex.SaveChanges();

                    denuncia.DenunciadoId = denunciado.DenunciadoId;
                    denuncia.DenuncianteId = denunciante.DenuncianteId;
                    denuncia.DenunciaDescripcion = Denuncia;
                    denuncia.FechaCreacion = DateTime.Now;
                    // denuncia. = TipoDenuncia;
                    denuncia.TipoDenunciaId = TipoDenuncia;

                    _contex.Denuncias.Add(denuncia);
                    _contex.SaveChanges();

                    if (file != null && file.ContentLength > 0)
                    {
                        var directory = Server.MapPath("~/Expedientes/");

                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        for (var i = 0; i < Request.Files.Count; i++)
                        {
                            var path = Path.Combine(Server.MapPath("~/Expedientes/"), Path.GetFileName(Request.Files[i].FileName));

                            if (!System.IO.File.Exists(path))
                                Request.Files[i].SaveAs(path);
                        }

                    }

                    //var comportamiento = new Comportamiento(_context);
                    //comportamiento.Update(model);
                    //transaccion.Commit();
           
                    
                    Message.sendMessage(EmailDenunciante, new string[] { "", "", "" });
                    return Json(new { success = true, messages = "Success" }, JsonRequestBehavior.AllowGet);
                    
                }
                catch (Exception e)
                {
                    //transaccion.Rollback();
                    return Json(new { success = false, messages = e.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }

            //return View();
        }



    }
}