﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    public class CulturaOrganizacionalTipoController : Controller
    {
        private dbIntranet db = new dbIntranet();
        // GET: CulturaOrganizacionalTipo
        public ActionResult Index()
        {
            var culorgtipo = db.CulturaOrganizacional.ToList();
            return View(culorgtipo);
        }
    }
}