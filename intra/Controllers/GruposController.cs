﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.RolesUsuarios;

namespace intra.Controllers
{
    public class GruposController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: Grupos
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Index()
        {

            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Mantenimiento de Grupos");
            return View(db.Grupos.ToList());
        }

        // GET: Grupos/Details/5
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grupos grupos = db.Grupos.Find(id);
            if (grupos == null)
            {
                return HttpNotFound();
            }
            return View(grupos);
        }

        // GET: Grupos/Create
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Create()
        {
            ViewBag.Sistemas = new SelectList(db.Sistemas.ToList(), "SistemaId", "SistemaNombre");
            ViewBag.Categorias = new SelectList(db.Categorias.ToList(), "CategoriaId", "CategoriaNombre");
            return View();
        }

        // POST: Grupos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Create([Bind(Include = "GrupoId,GrupoNombre,GrupoDescripcion,SistemaId,CategoriaId")] Grupos grupos)
        {
            if (ModelState.IsValid)
            {
                db.Grupos.Add(grupos);
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo un Grupo llamado ("+grupos.GrupoNombre +")");
                return RedirectToAction("Index");
            }

            ViewBag.Sistemas = new SelectList(db.Sistemas.ToList(), "SistemaId", "SistemaNombre");
            ViewBag.Categorias = new SelectList(db.Categorias.ToList(), "CategoriaId", "CategoriaNombre");
            return View(grupos);
        }

        // GET: Grupos/Edit/5
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grupos grupos = db.Grupos.Find(id);

            if (grupos == null)
            {
                return HttpNotFound();
            }

            ViewBag.Sistemas = new SelectList(db.Sistemas.ToList(), "SistemaId", "SistemaNombre");
            ViewBag.Categorias = new SelectList(db.Categorias.ToList(), "CategoriaId", "CategoriaNombre");
            return View(grupos);
        }

        // POST: Grupos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Edit([Bind(Include = "GrupoId,GrupoNombre,GrupoDescripcion,SistemaId,CategoriaId")] Grupos grupos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(grupos).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito el Grupo id(" + grupos.GrupoId + ")");
                return RedirectToAction("Index");
            }

            ViewBag.Sistemas = new SelectList(db.Sistemas.ToList(), "SistemaId", "SistemaNombre");
            ViewBag.Categorias = new SelectList(db.Categorias.ToList(), "CategoriaId", "CategoriaNombre");
            return View(grupos);
        }

        // GET: Grupos/Delete/5
        [CustomAuthorize]
        [Authorize(Roles = "RUA")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grupos grupos = db.Grupos.Find(id);
            if (grupos == null)
            {
                return HttpNotFound();
            }
            return View(grupos);
        }

        // POST: Grupos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA")]
        public ActionResult DeleteConfirmed(int id)
        {
            Grupos grupos = db.Grupos.Find(id);
            db.Grupos.Remove(grupos);
            db.SaveChanges();
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Elimino el Grupo (" + grupos.GrupoNombre + ")");
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
