﻿using intra.Models.GestionHumana;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Code;
using intra.Models;

namespace intra.Controllers
{
    public class FotoGestionHumanaController : Controller
    {
        private dbIntranet db = new dbIntranet();

        //public ActionResult FotosAlbum(int id)
        //{
        //    return RedirectToAction("Index", new { AlbumId = id });
        //}

        // GET: FotoGestionHumana
        public ActionResult Index(int AlbumId)
        {
            var photos = db.FotoGestionHumana.Where(x => x.AlbumGHId == AlbumId && x.FotoEstado == true).ToList();
            var album = db.AlbumGestionHumana.Find(AlbumId);

            ViewBag.NombreAlbum = album.AlbumGHNombre;
            ViewBag.IdAlbum = album.AlbumGHId;

            return View(photos);
        }

        // GET: FotoGestionHumana/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: FotoGestionHumana/Create
        [Authorize(Roles = "RRHHA")]
        public ActionResult Create(int AlbumId)
        {
            ViewBag.IdAlbum = AlbumId;

            return View();
        }

        // POST: FotoGestionHumana/Create
        [HttpPost]
        [Authorize(Roles = "RRHHA")]
        public ActionResult Create(FotoGestionHumana view_object, FormCollection fc, HttpPostedFileBase file)
        {
            try
            {
                List<FotoGestionHumana> fotos_commit = Utilities.UploadPhotoGH(view_object.FotoVista);
                if (fc["FotoGHDescripcion"] != null)
                {
                    string descripcion = fc["FotoGHDescripcion"].ToString();
                    string[] arrayDescripcion = descripcion.Split(',');
                    int i = 0;

                    foreach (var item in fotos_commit)
                    {

                        view_object.FotoGHFhCreacion = DateTime.Now;
                        view_object.FotoGHUsuarioCreador = Convert.ToInt32(Session["empleadoId"]);
                        view_object.FotoGHNombre = Path.GetFileName(item.FotoGHNombre);
                        view_object.FotoGHExtension = Path.GetExtension(item.FotoGHExtension);
                        view_object.FotoGHFhSubida = DateTime.Now;
                        view_object.FotoEstado = true;
                       

                        var pic_name = Path.GetFileName(item.FotoGHNombre);
                        var path = Path.Combine(HttpRuntime.AppDomainAppPath + "\\Content\\images\\GH_IMAGES\\AlbumFotos", pic_name);
                        view_object.FotoVista[i].SaveAs(path);
                        view_object.FotoGHArchivo = item.FotoGHArchivo;
                            view_object.FotoGHAlto =item.FotoGHAlto;
                            view_object.FotoGHAncho = item.FotoGHAncho;
                      
                        i++;
                        db.FotoGestionHumana.Add(view_object);
                        db.SaveChanges();
                       
                    }
                }
                return RedirectToAction("Index", new { AlbumId = view_object.AlbumGHId });
            }
            catch
            {
                TempData["MsjError"] = "Error al subir la foto";

                return View();
            }
        }

        [Authorize(Roles = "RRHHA")]
        // GET: FotoGestionHumana/Edit/5
        public ActionResult Edit(int id, int AlbumId)
        {
            var foto = db.FotoGestionHumana.Find(id);

            if(foto != null)
            {
                string image64 = Convert.ToBase64String(foto.FotoGHArchivo);

                if (foto.FotoGHExtension.ToLower() == ".jpg" || foto.FotoGHExtension.ToLower() == ".jpeg")
                {
                    ViewBag.IconImage = string.Format("data:image/jpg;base64,{0}", image64);
                }
                else if (foto.FotoGHExtension.ToLower() == ".png")
                {
                    ViewBag.IconImage = string.Format("data:image/png;base64,{0}", image64);
                }
                else if (foto.FotoGHExtension.ToLower() == ".gif")
                {
                    ViewBag.IconImage = string.Format("data:image/gif;base64,{0}", image64);
                }
            }
            
            ViewBag.IdAlbum = AlbumId;

            var usuario_creador = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == foto.FotoGHUsuarioCreador.ToString()).FirstOrDefault();

            if (usuario_creador != null)
                ViewBag.UsuarioCreador = usuario_creador.NombreCompleto;

            return View(foto);
        }

        // POST: FotoGestionHumana/Edit/5
        [HttpPost]
        [Authorize(Roles = "RRHHA")]
        public ActionResult Edit(FotoGestionHumana view_object)
        {
            try
            {
                // TODO: Add update logic here
                
                if(view_object.FotoGHDescripcion != null)
                {
                    view_object.AlbumGestionHumana = null;

                    db.Entry(view_object).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }

                return RedirectToAction("Index", new { AlbumId = view_object.AlbumGHId });
            }
            catch(Exception e)
            {
                e.ToString();
                return RedirectToAction("Edit", new { id = view_object.AlbumGHId, AlbumId = view_object.AlbumGHId });
            }
        }

        // GET: FotoGestionHumana/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        // POST: FotoGestionHumana/Delete/5
        [HttpPost]
        public JsonResult Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                var foto = db.FotoGestionHumana.Find(id);

                if(foto != null)
                {
                    foto.FotoEstado = false;
                    db.SaveChanges();

                    return Json("Success");
                }
                else
                {
                    return Json("Error");
                }
            }
            catch
            {
                return Json("Error");
            }
        }

        public FileResult PhotoDownload(int id)
        {
            var foto = db.FotoGestionHumana.Find(id);

            return File(foto.FotoGHArchivo, foto.FotoGHExtension, foto.FotoGHNombre);
        }
    }
}
