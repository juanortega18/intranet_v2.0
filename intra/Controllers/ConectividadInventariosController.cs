﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Models.Inventario;



namespace intra.Controllers
{
    public class ConectividadInventariosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: ConectividadInventarios
        public ActionResult Index()
        {
            return View(db.ConectividadInventarios.ToList());
        }

        [CustomAuthorize]
        public PartialViewResult vConectividad()
        {
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            return PartialView(db.VistaConectividads.ToList());
        }
        [HttpGet]

        public JsonResult GetProvincia(int id)
        {


            List<SelectListItem> listProvincia = new List<SelectListItem>();
            if (id < 1)
            {

                RedirectToAction("Create");

            }

            var Provinces = (from r in db.Provinces
                             from i in db.Region
                             from rp in db.RegionProvincia
                             where i.RegionId == rp.RegionId
                             where r.Province_ID == rp.Province_ID
                             where i.RegionId == id
                             orderby r.Province_Name ascending
                             select new
                             {
                                 r.Province_Name,
                                 r.Province_ID

                             }).ToList();

            foreach (var Province in Provinces)
            {
                listProvincia.Add(new SelectListItem { Text = Province.Province_Name.ToString(), Value = Province.Province_ID.ToString() });

            }

            return Json(new SelectList(listProvincia, "value", "Text"), JsonRequestBehavior.AllowGet);
        }

        //Get Municipio
        [HttpPost]
        public JsonResult Municipio(string Provincia)
        {
            List<SelectListItem> Municipios;

            int cl = int.Parse(Provincia);
            Municipios = db.Municipalities.Where(x => x.Province_ID == cl).Select(x => new SelectListItem() { Text = x.Municipality_Name, Value = x.Municipality_ID.ToString() }).ToList();


            return Json(new SelectList(Municipios, "Value", "Text"));
        }

        [CustomAuthorize]
        public ActionResult Localidad(string Municipio)
        {
            var muni = int.Parse(Municipio);
            var Localidad = new List<SelectListItem>();
            var localidad = db.Localidad.Where(x => x.Municipality_ID == muni).Select(x => new SelectListItem() { Text = x.LocalidadNombre, Value = x.LocalidadId.ToString() }).ToList();
            return Json(new SelectList(localidad, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        // GET: ConectividadInventarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VistaConectividads conectividadInventario = db.VistaConectividads.Find(id);
            if (conectividadInventario == null)
            {
                return HttpNotFound();
            }
            return View(conectividadInventario);
        }

        // GET: ConectividadInventarios/Create
        [CustomAuthorize]
        public ActionResult Create()
        {
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Proveedor = new SelectList(db.ProveedorInventario.ToList(), "ProveedorId", "ProveedorNombre");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            ViewBag.Medios = new SelectList(db.MediosInventario.ToList(), "MedioId", "MedioDescripcion");
            ViewBag.Velocidad = new SelectList(db.VelocidadInventario.ToList(), "VelocidadId", "VelocidadDescripcion");
            return View();
        }

        // POST: ConectividadInventarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ConectividadId,RegionId,Province_ID,Municipality_ID,ConectividadServicio,ConectividadNoContrato,ConectividadTecnicoAsignado,ConectividadUsuario,ConectividadFecha,ProveedorId,LocalidadId,MedioId,VelocidadId")] ConectividadInventario conectividadInventario)
        {
            conectividadInventario.ConectividadUsuario = Session["usuario"].ToString();
            conectividadInventario.ConectividadFecha = DateTime.Now;

            if (ModelState.IsValid)
            {
                db.ConectividadInventarios.Add(conectividadInventario);
                db.SaveChanges();
                return RedirectToAction("Index","ComputadoraInventario");
            }

            var errors = ModelState.Values.SelectMany(v => v.Errors);

            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Proveedor = new SelectList(db.ProveedorInventario.ToList(), "ProveedorId", "ProveedorNombre");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            ViewBag.Medios = new SelectList(db.MediosInventario.ToList(), "MedioId", "MedioDescripcion");
            ViewBag.Velocidad = new SelectList(db.VelocidadInventario.ToList(), "VelocidadId", "VelocidadDescripcion");
            return View(conectividadInventario);
        }

        [CustomAuthorize]
        // GET: ConectividadInventarios/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            ViewBag.Proveedor = new SelectList(db.ProveedorInventario.ToList(), "ProveedorId", "ProveedorNombre");
            ViewBag.Medios = new SelectList(db.MediosInventario.ToList(), "MedioId", "MedioDescripcion");
            ViewBag.Velocidad = new SelectList(db.VelocidadInventario.ToList(), "VelocidadId", "VelocidadDescripcion");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConectividadInventario conectividadInventario = db.ConectividadInventarios.Find(id);
            if (conectividadInventario == null)
            {
                return HttpNotFound();
            }

            return View(conectividadInventario);
        }

        // POST: ConectividadInventarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ConectividadId,RegionId,Province_ID,Municipality_ID,ConectividadServicio,ConectividadNoContrato,ConectividadTecnicoAsignado,ConectividadUsuario,ConectividadFecha,ProveedorId,LocalidadId,MedioId,VelocidadId")] ConectividadInventario conectividadInventario)
        {
            conectividadInventario.ConectividadUsuario = Session["usuario"].ToString();
            conectividadInventario.ConectividadFecha = DateTime.Now;
            if (!ModelState.IsValid)
            {
                db.Entry(conectividadInventario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "ComputadoraInventario");
            }
            ViewBag.Proveedor = new SelectList(db.ProveedorInventario.ToList(), "ProveedorId", "ProveedorNombre");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            ViewBag.Medios = new SelectList(db.MediosInventario.ToList(), "MedioId", "MedioDescripcion");
            ViewBag.Velocidad = new SelectList(db.VelocidadInventario.ToList(), "VelocidadId", "VelocidadDescripcion");
            return View(conectividadInventario);
        }

        [CustomAuthorize]
        // GET: ConectividadInventarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VistaConectividads vistaConectividads = db.VistaConectividads.Find(id);
            if (vistaConectividads == null)
            {
                return HttpNotFound();
            }
            return View(vistaConectividads);
        }

        // POST: ConectividadInventarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ConectividadInventario conectividadInventario = db.ConectividadInventarios.Find(id);
            db.ConectividadInventarios.Remove(conectividadInventario);
            db.SaveChanges();
            return RedirectToAction("Index","ComputadoraInventario");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
