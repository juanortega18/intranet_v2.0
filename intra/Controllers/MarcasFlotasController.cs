﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.FlotasExtensiones;

namespace intra.Controllers
{
    public class MarcasFlotasController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: MarcasFlotas
        [CustomAuthorize]
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso Modulo de Marcas Flotas");
            return View(db.MarcasFlotas.ToList());
        }

        [CustomAuthorize]
        // GET: MarcasFlotas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarcasFlotas marcasFlotas = db.MarcasFlotas.Find(id);
            if (marcasFlotas == null)
            {
                return HttpNotFound();
            }
            return View(marcasFlotas);
        }

        [CustomAuthorize]
        // GET: MarcasFlotas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MarcasFlotas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MarcaId,MarcaDescripcion,MarcaFhCreacion,MarcaUsuario")] MarcasFlotas marcasFlotas)
        {
            marcasFlotas.MarcaFhCreacion = DateTime.Now;
            marcasFlotas.MarcaUsuario = Session["usuario"].ToString();

            if (ModelState.IsValid)
            {
                db.MarcasFlotas.Add(marcasFlotas);
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo un registro en el Modulo de Marcas Flotas id (" + marcasFlotas.MarcaId + ")");
                return RedirectToAction("Index");
            }

            return View(marcasFlotas);
        }

        [CustomAuthorize]
        // GET: MarcasFlotas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarcasFlotas marcasFlotas = db.MarcasFlotas.Find(id);
            if (marcasFlotas == null)
            {
                return HttpNotFound();
            }
            return View(marcasFlotas);
        }

        // POST: MarcasFlotas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MarcaId,MarcaDescripcion,MarcaFhCreacion,MarcaUsuario")] MarcasFlotas marcasFlotas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(marcasFlotas).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito un registro en el Modulo de Marcas Flotas id (" + marcasFlotas.MarcaId + ")");
                return RedirectToAction("Index");
            }
            return View(marcasFlotas);
        }

        [CustomAuthorize]
        // GET: MarcasFlotas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarcasFlotas marcasFlotas = db.MarcasFlotas.Find(id);
            if (marcasFlotas == null)
            {
                return HttpNotFound();
            }
            return View(marcasFlotas);
        }

        // POST: MarcasFlotas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MarcasFlotas marcasFlotas = db.MarcasFlotas.Find(id);
            db.MarcasFlotas.Remove(marcasFlotas);
            db.SaveChanges();
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Elimino un registro en el Modulo de Marcas Flotas id (" + marcasFlotas.MarcaId + ")");
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
