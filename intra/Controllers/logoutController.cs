﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace intra.Controllers
{
    public class logoutController : Controller
    {
        // GET: logout
        public ActionResult Index()
        {
            if (Session["empleadoId"] != null)
            {
                string emp = HttpContext.Session["empleadoId"].ToString();
                FormsAuthentication.SignOut();
                Session.Abandon();

                RegisterLogs rl = new RegisterLogs(emp, "Cerro Sesión");
            }

            return RedirectToAction("Index", "Login");
          
        }
    }
}