﻿using intra.Models.TallerPgr;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    [Authorize(Roles = "TA")]
    public class TipoEquipoController : Controller
    {
        private TallerPgrDM db = new TallerPgrDM();
        // GET: TipoEquipo
        [CustomAuthorize]
        public ActionResult Index()
        {
            var tipoequipo = db.TipoEquipo.ToList();
            return View(tipoequipo);
        }

        //GET: /TipoEquipo/
        [CustomAuthorize]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult Create(TipoEquipo tipoequipo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tipoequipo.TipoEquipoDescripcion = tipoequipo.TipoEquipoDescripcion.ToUpper();
                    db.TipoEquipo.Add(tipoequipo);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                //return View(tipoequipo);
            }
            catch(DataException /* dex*/)
            {
                ModelState.AddModelError("", "Unable to save changes. try again.");
            }
            return View(tipoequipo);
           
        }

        [CustomAuthorize]
        public ActionResult Edit(int? id)
        {
           if(id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEquipo tipoequipo = db.TipoEquipo.Find(id);
            if(tipoequipo==null)
            {
                return HttpNotFound();
            }
            return View(tipoequipo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TipoEquipo tipoequipo)
        {

            if(ModelState.IsValid)
            {
                tipoequipo.TipoEquipoDescripcion = tipoequipo.TipoEquipoDescripcion.ToUpper();
                db.Entry(tipoequipo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        [CustomAuthorize]
        public ActionResult Delete(int? id)
        {
            if(id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEquipo tipoEquipo = db.TipoEquipo.Find(id);
            if(tipoEquipo==null)
            {
                return HttpNotFound();
            }
            return View(tipoEquipo);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoEquipo tipoequipo = db.TipoEquipo.Find(id);
            try
            {
                db.TipoEquipo.Add(tipoequipo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                ModelState.AddModelError("", "Dont Delete. try again.");
            }
            return View(tipoequipo);

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}