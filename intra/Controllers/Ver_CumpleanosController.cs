﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.Cumpleanos;

namespace intra.Controllers
{
    public class Ver_CumpleanosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: Ver_Cumpleanos
        public ActionResult Index(FormCollection vc)
        {

            int mes = DateTime.Now.Month;

            string EmpId = string.Empty;


            EmpId = HttpContext.Session["empleadoId"].ToString();

            //int dia = DateTime.Now.Day;


            var Dependencia = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == EmpId).Select(x => x.dependencia).FirstOrDefault();


            var CumpleNow = (from c in db.Vw_Mostrar_Empleados_Cumpleanos
                                 //where c.Dia == dia
                             //where c.Mes == Convert.ToInt32( vc["Meses"])
                             where c.Dependencia == Dependencia
                             select new
                             {
                                 nombre = c.NombreCompleto,
                                 departamento = c.Departamento,
                                 dependencia = c.Dependencia,
                                 codigo = c.Codigo

                             }).ToList().Select(x => new Vw_Mostrar_Empleados_Cumpleanos
                             {
                                 NombreCompleto = x.nombre,
                                 Dependencia = x.dependencia,
                                 Departamento = x.departamento,
                                 Codigo = x.codigo

                             }).ToList();


            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso a Modulo de Cumpleaños");
            return View(CumpleNow);
        }

        [CustomAuthorize]
        // GET: Ver_Cumpleanos/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vw_Mostrar_Empleados_Cumpleanos vw_Mostrar_Empleados_Cumpleanos = db.Vw_Mostrar_Empleados_Cumpleanos.Find(id);
            if (vw_Mostrar_Empleados_Cumpleanos == null)
            {
                return HttpNotFound();
            }
            return View(vw_Mostrar_Empleados_Cumpleanos);
        }

        [CustomAuthorize]
        // GET: Ver_Cumpleanos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Ver_Cumpleanos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Codigo,NombreCompleto,CodigoDependencia,Dependencia,CodigoDepartamento,Departamento,FechaNacimiento,Dia,Mes,Estado,IdEstado,CodigoEmpleado")] Vw_Mostrar_Empleados_Cumpleanos vw_Mostrar_Empleados_Cumpleanos)
        {
            if (ModelState.IsValid)
            {
                db.Vw_Mostrar_Empleados_Cumpleanos.Add(vw_Mostrar_Empleados_Cumpleanos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vw_Mostrar_Empleados_Cumpleanos);
        }

        [CustomAuthorize]
        // GET: Ver_Cumpleanos/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vw_Mostrar_Empleados_Cumpleanos vw_Mostrar_Empleados_Cumpleanos = db.Vw_Mostrar_Empleados_Cumpleanos.Find(id);
            if (vw_Mostrar_Empleados_Cumpleanos == null)
            {
                return HttpNotFound();
            }
            return View(vw_Mostrar_Empleados_Cumpleanos);
        }

        // POST: Ver_Cumpleanos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Codigo,NombreCompleto,CodigoDependencia,Dependencia,CodigoDepartamento,Departamento,FechaNacimiento,Dia,Mes,Estado,IdEstado,CodigoEmpleado")] Vw_Mostrar_Empleados_Cumpleanos vw_Mostrar_Empleados_Cumpleanos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vw_Mostrar_Empleados_Cumpleanos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vw_Mostrar_Empleados_Cumpleanos);
        }

        [CustomAuthorize]
        // GET: Ver_Cumpleanos/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vw_Mostrar_Empleados_Cumpleanos vw_Mostrar_Empleados_Cumpleanos = db.Vw_Mostrar_Empleados_Cumpleanos.Find(id);
            if (vw_Mostrar_Empleados_Cumpleanos == null)
            {
                return HttpNotFound();
            }
            return View(vw_Mostrar_Empleados_Cumpleanos);
        }

        // POST: Ver_Cumpleanos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Vw_Mostrar_Empleados_Cumpleanos vw_Mostrar_Empleados_Cumpleanos = db.Vw_Mostrar_Empleados_Cumpleanos.Find(id);
            db.Vw_Mostrar_Empleados_Cumpleanos.Remove(vw_Mostrar_Empleados_Cumpleanos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
