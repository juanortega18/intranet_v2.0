﻿using intra.Models;
using intra.Models.ViewModel;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using intra.Models.BienesIncautados;
using intra.Models.GestionHumana;
namespace intra.Controllers
{
    [Authorize(Roles = "BI,BIA,BIWR")]
    public class IncautacionesController : Controller
    {
        private inv db = new inv();

        // GET: Incautaciones
        //[Authorize(Roles = "BI,BIA,BIWR")]
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();

            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo BI");

            ViewBag.provincias = new SelectList(db.INV_PROVINCIAS, "ID_Provincia", "Descripcion");
            //var incautaciones = db.VISTA_INCAUTACIONES.ToList();
            
            try
            {
                var incautaciones = db.VISTA_INCAUTACIONES.Where(inc=>inc.INCA_ESTATUS!=12).ToList();

                return View(incautaciones);
            }
            catch (ArgumentNullException error)
            {
                ViewBag.error = error;
                return View();
            }
        }

        // GET: incautaciones/Details/5
        //[Authorize(Roles = "BI,BIA,BIWR")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INV_INCAUTACIONES iNV_INCAUTACIONES = db.INV_INCAUTACIONES.Find(id);
            if (iNV_INCAUTACIONES == null)
            {
                return HttpNotFound();
            }
            return View(iNV_INCAUTACIONES);
        }

        // GET: incautaciones/Create
        [Authorize(Roles = "BIA,BIWR")]
        [CustomAuthorize]
        public ActionResult Create()
        {
            using (inv dbs = new inv())
            {
                var casos = dbs.INV_CASOS.Where(x => x.ESTA_ID != 6 && x.ESTA_ID != 12).ToList();

                if(casos.Count < 1)
                {
                    TempData["msjError"] = "Lo sentimos, aún no existen casos para poder crear un nuevo bien incautado.";

                    return RedirectToAction("Index", "Incautaciones");
                }

                ViewBag.Clases = new SelectList(dbs.INV_CLASES.ToList(), "CLAS_ID", "CLAS_NOMBRE");
                ViewBag.Tipos = new SelectList(dbs.INV_TIPOS.ToList(), "TIPO_ID", "TIPO_NOMBRE");
                ViewBag.Estatus = new SelectList(dbs.INV_ESTATUS.Where(x => x.ESTA_TIPO == 1).OrderBy(x=>x.ESTA_NOMBRE).ToList(), "ESTA_ID", "ESTA_NOMBRE");
                ViewBag.ProvInc = ViewBag.ProvInv = new SelectList(dbs.INV_PROVINCIAS.ToList(), "ID_Provincia", "Descripcion");
                ViewBag.Colores = new SelectList(db.INV_COLORES, "COLOR_ID", "COLOR_NOMBRE");
                ViewBag.Casos = new SelectList(casos, "CASO_ID", "CASO_NOMBRE");
                ViewBag.FechaEntrada = "";

                List<INV_CUSTODIO> all_custodios = db.INV_CUSTODIO.Where(x=>x.CUST_ESTADO == true).ToList();

                foreach (var item in all_custodios)
                {
                    if(item.CUST_NOMBRE.Equals("Ninguno"))
                    {
                        item.CUST_NOMBRE = item.CUST_DEPARTAMENTO;
                    }
                }

                ViewBag.Custodios = new SelectList(all_custodios, "CUST_ID", "CUST_NOMBRE");
                
                return View();
            }
        }

        // POST: incautaciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult Create(INV_INCAUTACIONES view_object, FormCollection fc)
        {
            view_object.INCA_DIRECCION = fc["INCA_DIRECCION"];
            view_object.INCA_LONGITUD = fc["INCA_LONGITUD"];
            view_object.INCA_LATITUD = fc["INCA_LATITUD"];
            view_object.INCA_DESCRIPCION = fc["INCA_DESCRIPCION"];
            //view_object.INCA_FHENTRADA = Convert.ToDateTime(fc["INCA_FHENTRADA"]);

            string inca_fhdevolucion_s = fc["PRE_INCA_FHDEVOLUCION"].ToString();
            string inca_fhvencseguro_s = fc["PRE_INCA_FHVENCSEGURO"].ToString();
            string inca_fhentrada_s = fc["PRE_INCA_FHENTRADA"].ToString();


            
            #region Date Converter
            CultureInfo ci = CultureInfo.InstalledUICulture;

            var languaje_name = ci.Name;

            if (languaje_name == "en-US")
            {
                if (inca_fhdevolucion_s == "")
                {
                    inca_fhdevolucion_s = "";
                }
                else
                {
                    string[] splitted_date = inca_fhdevolucion_s.ToString().Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    inca_fhdevolucion_s = english_date.ToString("MM/dd/yyyy");
                }

                if (inca_fhvencseguro_s.ToString() == "")
                {
                    inca_fhvencseguro_s = "";
                }
                else
                {
                    string[] splitted_date = inca_fhvencseguro_s.ToString().Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    inca_fhvencseguro_s = english_date.ToString("MM/dd/yyyy");
                }

                if (inca_fhentrada_s == "")
                {
                    inca_fhentrada_s = "";
                }
                else
                {
                    string[] splitted_date = inca_fhentrada_s.Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    inca_fhentrada_s = english_date.ToString("MM/dd/yyyy");
                }
            }
            else if (languaje_name == "es-ES")
            {
                inca_fhdevolucion_s = inca_fhdevolucion_s == "" ? "" : inca_fhdevolucion_s;
                inca_fhvencseguro_s = inca_fhvencseguro_s == "" ? "" : inca_fhvencseguro_s;
                inca_fhentrada_s = inca_fhentrada_s == "" ? "" : inca_fhentrada_s;
            }

            DateTime INCA_FHDEVOLUCION = inca_fhdevolucion_s != "" ? Convert.ToDateTime(inca_fhdevolucion_s) : new DateTime();
            DateTime INCA_FHVENCSEGURO = inca_fhvencseguro_s != "" ? Convert.ToDateTime(inca_fhvencseguro_s) : new DateTime();
            DateTime INCA_FHENTRADA = inca_fhentrada_s != "" ? Convert.ToDateTime(inca_fhentrada_s) : new DateTime();
            #endregion

            int i = 0;

            view_object.INCA_NO = Convert.ToChar(64 + view_object.TIPO_ID).ToString() + view_object.INCA_FHENTRADA.ToString("yyyyMMdd") + DateTime.Now.ToString("fffmmsss");
            view_object.INCA_FHCREACION = DateTime.Now;
            view_object.INCA_CASO = "0";
            view_object.USUA_ID = Session["usuario"].ToString();

            if(INCA_FHDEVOLUCION!=new DateTime())
            {
                view_object.INCA_FHDEVOLUCION = INCA_FHDEVOLUCION;
            }

            if(INCA_FHVENCSEGURO!=new DateTime())
            {
                view_object.INCA_FHVENCSEGURO = INCA_FHVENCSEGURO;
            }

            if(INCA_FHENTRADA!=new DateTime())
            {
                view_object.INCA_FHENTRADA = INCA_FHENTRADA;
            }
            
            if (ModelState.IsValid)
            {
                var custodio = GetCustodio(fc["CUST_ID"].ToString());

                view_object.CUST_ID = custodio.CUST_ID;

                HashSet<INV_FOTOS> fotos_commit = Utilities.UploadPhoto(view_object.ARCHIVO_VISTA, view_object.INCA_NO);

                db.INV_INCAUTACIONES.Add(view_object);

                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    ModelState.AddModelError(string.Empty, dbEx.Message);
                }

                fc["fotoDescripcion"] = "Archivo";

                if (fc["fotoDescripcion"] != null)
                {
                    string descripcion =   fc["fotoDescripcion"].ToString();

                    //string[] arrayDescripcion = descripcion.Split(',');

                    foreach (var item in fotos_commit)
                    {
                        item.FOTO_DESCRIPCION = descripcion + i;

                        db.INV_FOTOS.Add(item);
                        db.SaveChanges();

                        i++;
                    }
                }

                //string empId = HttpContext.Session["empleadoId"].ToString();

                //RegisterLogs rl = new RegisterLogs(empId, "Creo un registro en el modulo BI con el inca_no (" + view_object.INCA_NO + ")");

                //Logs tosave_log = rl.returned_log;
                //INV_INCALOGS inca_log = new INV_INCALOGS() { INCA_ID = view_object.INCA_ID /*tosave_inca.INCA_ID*/, LogId = tosave_log.LogId, CUST_ID = custodio.CUST_ID, CUST_CAMBIO = true };
                //db.INV_INCALOGS.Add(inca_log);
                //db.SaveChanges();

                RegistarLog("Creo un registro en el modulo BI con el inca_no (" + view_object.INCA_NO + ")", view_object.INCA_ID, true, custodio.CUST_ID);

                return RedirectToAction("Index");
            }

            var errors = ModelState.Values.SelectMany(v => v.Errors);
            
            ViewBag.Clases = new SelectList(db.INV_CLASES.ToList(), "CLAS_ID", "CLAS_NOMBRE");
            ViewBag.Tipos = new SelectList(db.INV_TIPOS.ToList(), "TIPO_ID", "TIPO_NOMBRE");
            ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 1).ToList(), "ESTA_ID", "ESTA_NOMBRE");
            ViewBag.ProvInc = ViewBag.ProvInv = new SelectList(db.INV_PROVINCIAS.ToList(), "ID_Provincia", "Descripcion");
            ViewBag.Colores = new SelectList(db.INV_COLORES.ToList(), "COLOR_ID", "COLOR_NOMBRE");
            ViewBag.Casos = new SelectList(db.INV_CASOS.Where(x => x.ESTA_ID != 6 && x.ESTA_ID != 12).ToList(), "CASO_ID", "CASO_NOMBRE");
            ViewBag.Custodios = new SelectList(db.INV_CUSTODIO.Where(x => x.CUST_ESTADO == true).ToList(), "CUST_ID", "CUST_NOMBRE");

            ViewBag.FechaEntrada = fc["PRE_INCA_FHENTRADA"].ToString();
            return View(view_object);
        }

        public JsonResult dbTipos(string clase)
        {
            List<SelectListItem> tipos;

            int cl = int.Parse(clase);
            tipos = db.INV_TIPOS.Where(x => x.CLAS_ID == cl).OrderBy(x => x.TIPO_NOMBRE).Select(x => new SelectListItem() { Text = x.TIPO_NOMBRE, Value = x.TIPO_ID.ToString() }).ToList();

            return Json(new SelectList(tipos, "Value", "Text"));
        }

        [HttpPost]
        public JsonResult CambiarEstadoArchivo(int? id, string estado)
        {
            INV_FOTOS iNV_FOTOS = db.INV_FOTOS.Find(id);

            try
            {
                if (estado.Equals("Activo"))
                {
                    if (iNV_FOTOS.FOTO_ESTATUS==false)
                    {
                        iNV_FOTOS.FOTO_ESTATUS = true;
                        db.Entry(iNV_FOTOS).State = EntityState.Modified;
                        db.SaveChanges();

                    }
                }
                else if (estado.Equals("Inactivo"))
                {
                    if (iNV_FOTOS.FOTO_ESTATUS==true)
                    {
                        iNV_FOTOS.FOTO_ESTATUS = false;
                        db.Entry(iNV_FOTOS).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                else if (estado.Equals("Eliminado"))
                {

                    iNV_FOTOS.FOTO_ESTATUS = null;
                    db.Entry(iNV_FOTOS).State = EntityState.Modified;
                    db.SaveChanges();

                }
                return Json("Cambios guardados");
            }
            catch (Exception error)
            {
                error.ToString();
                return Json("Ha ocurrido un error");
            }
        }
        
        // GET: incautaciones/Edit/5
        //[Authorize(Roles = "BIA,BIWR")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            INV_INCAUTACIONES iNV_INCAUTACIONES = db.INV_INCAUTACIONES.FirstOrDefault(inc=> inc.INCA_ID==id && inc.INCA_ESTATUS != 12);

            var data = db.INV_INCAUTACIONES.ToList();

            if (iNV_INCAUTACIONES==null)
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.Clases = new SelectList(db.INV_CLASES.ToList(), "CLAS_ID", "CLAS_NOMBRE", iNV_INCAUTACIONES.CLAS_ID);
            ViewBag.TIPO_ID = new SelectList(db.INV_TIPOS.Where(x => x.CLAS_ID == iNV_INCAUTACIONES.CLAS_ID).ToList(), "TIPO_ID", "TIPO_NOMBRE", iNV_INCAUTACIONES.TIPO_ID);
            ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 1).ToList(), "ESTA_ID", "ESTA_NOMBRE", iNV_INCAUTACIONES.INCA_ESTATUS);
            ViewBag.ProvInc = ViewBag.ProvInv = new SelectList(db.INV_PROVINCIAS.ToList(), "ID_Provincia", "Descripcion", iNV_INCAUTACIONES.INCA_LOCALIDADINC);
            ViewBag.Colores = new SelectList(db.INV_COLORES.ToList(), "COLOR_ID", "COLOR_NOMBRE", iNV_INCAUTACIONES.COLOR_ID);
            ViewBag.Casos = new SelectList(db.INV_CASOS.Where(x => x.ESTA_ID != 6 && x.ESTA_ID != 12).ToList(), "CASO_ID", "CASO_NOMBRE", iNV_INCAUTACIONES.CASO_ID);
            ViewBag.INCA_FHDEVOLUCION_String = iNV_INCAUTACIONES.INCA_FHDEVOLUCION != null ? iNV_INCAUTACIONES.INCA_FHDEVOLUCION.Value.ToString("dd/MM/yyyy") : "";
            ViewBag.INCA_FHVENCSEGURO_String = iNV_INCAUTACIONES.INCA_FHVENCSEGURO != null ? iNV_INCAUTACIONES.INCA_FHVENCSEGURO.Value.ToString("dd/MM/yyyy") : "";
            ViewBag.INCA_FHENTRADA_String = iNV_INCAUTACIONES.INCA_FHENTRADA != null ? iNV_INCAUTACIONES.INCA_FHENTRADA.ToString("dd/MM/yyyy") : "";
            ViewBag.INCA_FHCREACION_String = iNV_INCAUTACIONES.INCA_FHCREACION.ToString("dd/MM/yyyy");

            List<INV_CUSTODIO> all_custodios = db.INV_CUSTODIO.Where(x => x.CUST_ESTADO == true).ToList();

            foreach (var item in all_custodios)
            {
                if (item.CUST_NOMBRE.Equals("Ninguno"))
                {
                    item.CUST_NOMBRE = item.CUST_DEPARTAMENTO;
                }
            }

            ViewBag.Custodios = new SelectList(all_custodios, "CUST_ID", "CUST_NOMBRE");
            
            ViewBag.Valor = Convert.ToInt32(iNV_INCAUTACIONES.INCA_VALOR);
            ViewBag.Acuerdo = Convert.ToInt32(iNV_INCAUTACIONES.INCA_ACUERDO);

            var numero = iNV_INCAUTACIONES.INCA_VALOR;
            iNV_INCAUTACIONES.INV_FOTOS = new List<INV_FOTOS>();
            iNV_INCAUTACIONES.INV_FOTOS = db.INV_FOTOS.Where(x => x.INCA_NO == iNV_INCAUTACIONES.INCA_NO && x.FOTO_ESTATUS!=null ).ToList();

            if(iNV_INCAUTACIONES.INV_FOTOS!=null)
            {
                foreach (var item in iNV_INCAUTACIONES.INV_FOTOS)
                {
                    string imageBase64 = Convert.ToBase64String(item.FOTO_ARCHIVO);

                    if (item.FOTO_TIPO.ToLower() == ".jpg" || item.FOTO_TIPO.ToLower() == ".jpeg")
                    {
                        item.IMAGE_SRC = string.Format("data:image/jpg;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = item.IMAGE_SRC;
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".png")
                    {
                        item.IMAGE_SRC = string.Format("data:image/png;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = item.IMAGE_SRC;
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".gif")
                    {
                        item.IMAGE_SRC = string.Format("data:image/gif;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = item.IMAGE_SRC;
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".mp4" || item.FOTO_TIPO.ToLower() == ".3gp" || item.FOTO_TIPO.ToLower() == ".avi" ||
                        item.FOTO_TIPO.ToLower() == ".flv" || item.FOTO_TIPO.ToLower() == ".mpg" || item.FOTO_TIPO.ToLower() == ".wmv")
                    {
                        //item.IMAGE_SRC = string.Format("data:application/pdf;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/iconos/icono_video_descarga.svg";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".pdf")
                    {
                        //item.IMAGE_SRC = string.Format("data:application/pdf;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/pdf.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".doc" || item.FOTO_TIPO.ToLower() == ".docx")
                    {
                        //item.IMAGE_SRC = string.Format("data:application/msword;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/doc.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".xls" || item.FOTO_TIPO.ToLower() == ".xlsx")
                    {
                        //item.IMAGE_SRC = string.Format("data:application/vnd.ms-excel;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".ppt" || item.FOTO_TIPO.ToLower() == ".pptx")
                    {
                        //item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/ppt.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".txt")
                    {
                        //item.IMAGE_SRC = string.Format("data:text/plain;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/txt.png";
                    }
                    else
                    {
                        //item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/file.png";
                        item.IMAGE_SRC = "#";
                    }
                }
            }

            if (iNV_INCAUTACIONES == null)
            {
                return HttpNotFound();
            }

            ViewBag.Direccion = iNV_INCAUTACIONES.INCA_DIRECCION;

            return View(iNV_INCAUTACIONES);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult Edit(INV_INCAUTACIONES view_object, FormCollection fc)
        { 
            int i = 0;
            view_object.INCA_CASO = "0";

            string inca_fhdevolucion_s = fc["PRE_INCA_FHDEVOLUCION"].ToString();
            string inca_fhvencseguro_s = fc["PRE_INCA_FHVENCSEGURO"].ToString();
            string inca_fhentrada_s = fc["PRE_INCA_FHENTRADA"].ToString();
            string inca_fhcreacion_s = fc["PRE_INCA_FHCREACION"].ToString();

            #region Date Converter
            CultureInfo ci = CultureInfo.InstalledUICulture;

            var languaje_name = ci.Name;

            if (languaje_name == "en-US")
            {
                if (inca_fhdevolucion_s != "")
                {
                    string[] splitted_date = inca_fhdevolucion_s.ToString().Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    inca_fhdevolucion_s = english_date.ToString("MM/dd/yyyy");    
                }
                
                if (inca_fhvencseguro_s.ToString() != "")
                {
                    string[] splitted_date = inca_fhvencseguro_s.ToString().Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    inca_fhvencseguro_s = english_date.ToString("MM/dd/yyyy");
                }
                
                if (inca_fhentrada_s != "")
                {
                    string[] splitted_date = inca_fhentrada_s.Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    inca_fhentrada_s = english_date.ToString("MM/dd/yyyy");    
                }

                if(inca_fhcreacion_s != "")
                {
                    string[] splitted_date = inca_fhcreacion_s.Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    inca_fhcreacion_s = english_date.ToString("MM/dd/yyyy");
                }
                
            }
            else if (languaje_name == "es-ES")
            {
                inca_fhdevolucion_s = inca_fhdevolucion_s == "" ? "" : inca_fhdevolucion_s;
                inca_fhvencseguro_s = inca_fhvencseguro_s == "" ? "" : inca_fhvencseguro_s;
                inca_fhentrada_s = inca_fhentrada_s == "" ? "" : inca_fhentrada_s;
                inca_fhcreacion_s= inca_fhcreacion_s == "" ? "" : inca_fhcreacion_s;
            }

            DateTime? INCA_FHDEVOLUCION = null; 
            DateTime? INCA_FHVENCSEGURO = null;
            DateTime? INCA_FHENTRADA = null;
            DateTime? INCA_FHCREACION = null;

            if (!string.IsNullOrEmpty(inca_fhdevolucion_s))
                INCA_FHVENCSEGURO = INCA_FHDEVOLUCION = Convert.ToDateTime(inca_fhdevolucion_s);

            if (!string.IsNullOrEmpty(inca_fhvencseguro_s))
                Convert.ToDateTime(inca_fhvencseguro_s);

            if (!string.IsNullOrEmpty(inca_fhentrada_s))
                INCA_FHENTRADA = Convert.ToDateTime(inca_fhentrada_s);
            else
            {
                TempData["msjError"] = "La Fecha de Entrada es obligatoria";
                
                return RedirectToAction("Edit", "Incautaciones", view_object.INCA_ID);
            }

            if (!string.IsNullOrEmpty(inca_fhcreacion_s))
                INCA_FHCREACION = Convert.ToDateTime(inca_fhcreacion_s);
            else
            {
                TempData["msjError"] = "La Fecha de Entrada es obligatoria";

                return RedirectToAction("Edit", "Incautaciones", view_object.INCA_ID);
            }
            #endregion

            if (view_object.CASO_ID==2)
            {
                //if (INCA_FHDEVOLUCION != null)
                //{
                //    view_object.INCA_FHDEVOLUCION = INCA_FHDEVOLUCION;
                //}

                //if (INCA_FHVENCSEGURO != null)
                //{
                //    view_object.INCA_FHVENCSEGURO = INCA_FHVENCSEGURO;
                //}

                view_object.INCA_FHVENCSEGURO = INCA_FHVENCSEGURO;
                view_object.INCA_FHDEVOLUCION = INCA_FHDEVOLUCION;
            }
            
            if (ModelState.IsValid)
            {

                if (INCA_FHDEVOLUCION != null)
                    view_object.INCA_FHDEVOLUCION = INCA_FHDEVOLUCION;
                
                if (INCA_FHVENCSEGURO != null)
                    view_object.INCA_FHVENCSEGURO = INCA_FHVENCSEGURO;
                
                if (INCA_FHENTRADA != null)
                    view_object.INCA_FHENTRADA = INCA_FHENTRADA.Value;
                
                if(INCA_FHCREACION != null)
                    view_object.INCA_FHCREACION = INCA_FHCREACION.Value;
                
                HashSet<INV_FOTOS> fotos_commit = Utilities.UploadPhoto(view_object.ARCHIVO_VISTA, view_object.INCA_NO);

                db.Entry(view_object).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    ModelState.AddModelError(string.Empty, dbEx.Message);
                }

                view_object.INV_FOTOS = new List<INV_FOTOS>();
                view_object.INV_FOTOS = db.INV_FOTOS.Where(x => x.INCA_NO == view_object.INCA_NO && x.FOTO_ESTATUS != null).ToList();

                if (fc["FOTO_DESCRIPCION"] != null)
                {
                    string[] a_modificar = fc["FOTO_DESCRIPCION"].Split(',');

                    foreach (var item in view_object.INV_FOTOS)
                    {
                        item.FOTO_DESCRIPCION = a_modificar[i];

                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();

                        i++;
                    }
                }

                i = 0;

                if (fc["fotoDescripcion"] != null)
                {
                    string descripcion = fc["fotoDescripcion"].ToString();

                    string[] arrayDescripcion = descripcion.Split(',');

                    foreach (var item in fotos_commit)
                    {
                        item.FOTO_DESCRIPCION = arrayDescripcion[i];

                        db.INV_FOTOS.Add(item);
                        db.SaveChanges();

                        i++;
                    }
                }

                var CUST_ID_ANTERIOR = fc["CUST_ID_ANTERIOR"].ToString();
                var CUST_ID = view_object.CUST_ID;

                var CustodioCambiado = CUST_ID_ANTERIOR != CUST_ID.ToString();

                string empId = HttpContext.Session["empleadoId"].ToString();

                var custodio = GetCustodio(CUST_ID.ToString());

                //string LogAccion = custodio.CUST_TIPO == "Persona"
                //? "Creo un registro en el modulo BI con el inca_no (" + view_object.INCA_NO + "), custodiado por : " + custodio.CUST_NOMBRE + " Portador de la Cedula: " + custodio.CUST_CEDULA
                //: "Creo un registro en el modulo BI con el inca_no (" + view_object.INCA_NO + "), custodiado por : " + custodio.CUST_NOMBRE;

                //RegisterLogs rl = new RegisterLogs(empId, "Actualizo un registro en el modulo BI con el inca_no (" + view_object.INCA_NO + ")");

                //Logs tosave_log = rl.returned_log;
                ////INV_INCAUTACIONES tosave_inca = db.INV_INCAUTACIONES.Where(x => x.INCA_NO == view_object.INCA_NO).FirstOrDefault();
                //INV_INCALOGS inca_log = new INV_INCALOGS() { INCA_ID = view_object.INCA_ID /*tosave_inca.INCA_ID*/, LogId = tosave_log.LogId, CUST_ID = custodio.CUST_ID, CUST_CAMBIO = CustodioCambiado ? true : false };
                //db.INV_INCALOGS.Add(inca_log);
                //db.SaveChanges();

                RegistarLog("Actualizo un registro en el modulo BI con el inca_no (" + view_object.INCA_NO + ")", view_object.INCA_ID, CustodioCambiado ? true : false, custodio.CUST_ID);

                return RedirectToAction("Index");
            }

            var errors = ModelState.Values.SelectMany(v => v.Errors);

            ViewBag.Clases = new SelectList(db.INV_CLASES.ToList(), "CLAS_ID", "CLAS_NOMBRE");
            ViewBag.TIPO_ID = new SelectList(db.INV_TIPOS.Where(x => x.CLAS_ID == view_object.CLAS_ID).ToList(), "TIPO_ID", "TIPO_NOMBRE", view_object.TIPO_ID);
            ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 1).ToList(), "ESTA_ID", "ESTA_NOMBRE");
            ViewBag.ProvInc = ViewBag.ProvInv = new SelectList(db.INV_PROVINCIAS.ToList(), "ID_Provincia", "Descripcion");
            ViewBag.Colores = new SelectList(db.INV_COLORES.ToList(), "COLOR_ID", "COLOR_NOMBRE");
            ViewBag.Casos = new SelectList(db.INV_CASOS.Where(x => x.ESTA_ID != 6 && x.ESTA_ID != 12).ToList(), "CASO_ID", "CASO_NOMBRE");
            ViewBag.Custodios = new SelectList(db.INV_CUSTODIO.Where(x => x.CUST_ESTADO == true).ToList(), "CUST_ID", "CUST_NOMBRE");
            ViewBag.INCA_FHDEVOLUCION_String = view_object.INCA_FHDEVOLUCION != null ? view_object.INCA_FHDEVOLUCION.Value.ToString("dd/MM/yyyy") : "";
            ViewBag.INCA_FHVENCSEGURO_String = view_object.INCA_FHVENCSEGURO != null ? view_object.INCA_FHVENCSEGURO.Value.ToString("dd/MM/yyyy") : "";
            ViewBag.INCA_FHCREACION_String = view_object.INCA_FHCREACION.ToString("dd/MM/yyyy");

            return View(view_object);
        }

        public JsonResult INC_Filtrar(int? origen,int? destino, string chasis, string matricula, string placa,string modelo,string marca) {

            var bienes = db.VISTA_INCAUTACIONES.Where(x =>
            (x.INCA_LOCALIDADINC == origen || origen == null) &&
            (x.INCA_LOCALIDADINV == destino || destino == null) &&
            (x.INCA_CHASIS.Contains(chasis) || chasis == null) &&
            (x.INCA_PLACA.Contains(placa) || placa == null) &&
            (x.INCA_MATRICULA.Contains(matricula) || matricula == null) &&
            (x.INCA_MODELO.Contains(modelo) || modelo == null) &&
            (x.INCA_MARCA.Contains(marca) || marca == null) && x.INCA_ESTATUS != 12
            ).ToList();

            return Json(bienes);
        }

        [HttpPost]
        [Authorize(Roles="BIA")]
        public JsonResult Delete(int? id)
        {
            try
            {
                INV_INCAUTACIONES iNV_INCAUTACIONES = db.INV_INCAUTACIONES.Find(id);

                iNV_INCAUTACIONES.INCA_ESTATUS = 12;

                var fotos = db.INV_FOTOS.Where(x => x.INCA_NO == iNV_INCAUTACIONES.INCA_NO && x.FOTO_ESTATUS != null).ToList();

                foreach (var item in fotos) {
                    item.FOTO_ESTATUS = null;
                }

                db.SaveChanges();

                RegistarLog("Elimino el registro con el inca_no (" + iNV_INCAUTACIONES.INCA_NO + ")", iNV_INCAUTACIONES.INCA_ID, null, null);

                return Json("Eliminado");
            }
            catch (Exception e) {
                e.ToString();
                return Json("Error");
            }

        }

        //[Authorize(Roles= "BIA,BIWR")]
        public ActionResult ReporteEditar(int? id)
        {
            VISTA_INCAUTACIONES a_reporte = db.VISTA_INCAUTACIONES.Find(id);
            ViewBag.a_reporte = a_reporte;
            //a_reporte.INV_CUSTODIO = db.INV_CUSTODIO.Where(x => x.CUST_ID == a_reporte.CUST_ID).FirstOrDefault();

            ViewBag.Clase = db.INV_CLASES.Where(x => x.CLAS_ID == a_reporte.CLAS_ID).Select(x => x.CLAS_NOMBRE).FirstOrDefault();

            //ViewBag.Tipo = db.INV_TIPOS.Where(x => x.TIPO_ID == a_reporte.TIPO_ID).Select(x => x.TIPO_NOMBRE).FirstOrDefault();
            //ViewBag.Estatus = db.INV_ESTATUS.Where(x => x.ESTA_ID == a_reporte.INCA_ESTATUS).Select(x => x.ESTA_NOMBRE).FirstOrDefault();
            //ViewBag.ProvInc = db.INV_PROVINCIAS.Where(x => x.ID_Provincia == a_reporte.INCA_LOCALIDADINC).Select(x => x.Descripcion).FirstOrDefault();
            //ViewBag.ProvInv = db.INV_PROVINCIAS.Where(x => x.ID_Provincia == a_reporte.INCA_LOCALIDADINV).Select(x => x.Descripcion).FirstOrDefault();
            //ViewBag.Caso = db.INV_CASOS.Where(x => x.CASO_ID == a_reporte.CASO_ID).Select(x => x.CASO_NOMBRE).FirstOrDefault();
            //ViewBag.Color = a_reporte.COLOR_ID != null ? db.INV_COLORES.FirstOrDefault(x => x.COLOR_ID == a_reporte.COLOR_ID).COLOR_NOMBRE : "INDEFINIDO";

            //ViewBag.direccion = a_reporte.INCA_DIRECCION;
            //ViewBag.matricula = a_reporte.INCA_MATRICULA;
            //ViewBag.descripcion = a_reporte.INCA_DESCRIPCION;

            a_reporte.INV_FOTOS = db.INV_FOTOS.Where(x => x.INCA_NO == a_reporte.INCA_NO && x.FOTO_ESTATUS==true && 
            (x.FOTO_TIPO.ToLower() == ".jpg" || x.FOTO_TIPO.ToLower() == ".jpeg"
            || x.FOTO_TIPO.ToLower() == ".png" || x.FOTO_TIPO.ToLower() == ".gif")).ToList();

            List<INV_FOTOS> SOLO_FOTOS = new List<INV_FOTOS>();

            #region codigo comentado

            //if (a_reporte.INV_FOTOS != null)
            //{
            //    foreach (var item in a_reporte.INV_FOTOS)
            //    {
            //        string imageBase64 = Convert.ToBase64String(item.FOTO_ARCHIVO);

            //        if (item.FOTO_TIPO.ToLower() == ".jpg" || item.FOTO_TIPO.ToLower() == ".jpeg")
            //        {
            //            item.IMAGE_SRC = string.Format("data:image/jpg;base64,{0}", imageBase64);
            //            item.IMAGE_IMAGE = item.IMAGE_SRC;
            //            SOLO_FOTOS.Add(item);
            //        }
            //        else if (item.FOTO_TIPO.ToLower() == ".png")
            //        {
            //            item.IMAGE_SRC = string.Format("data:image/png;base64,{0}", imageBase64);
            //            item.IMAGE_IMAGE = item.IMAGE_SRC;
            //            SOLO_FOTOS.Add(item);
            //        }
            //        else if (item.FOTO_TIPO.ToLower() == ".gif")
            //        {
            //            item.IMAGE_SRC = string.Format("data:image/gif;base64,{0}", imageBase64);
            //            item.IMAGE_IMAGE = item.IMAGE_SRC;
            //            SOLO_FOTOS.Add(item);
            //        }
            //        else if (item.FOTO_TIPO.ToLower() == ".pdf")
            //        {
            //            item.IMAGE_SRC = string.Format("data:application/pdf;base64,{0}", imageBase64);
            //            item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/pdf.png";
            //        }
            //        else if (item.FOTO_TIPO.ToLower() == ".doc" || item.FOTO_TIPO.ToLower() == ".docx")
            //        {
            //            item.IMAGE_SRC = string.Format("data:application/msword;base64,{0}", imageBase64);
            //            item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/doc.png";
            //        }
            //        else if (item.FOTO_TIPO.ToLower() == ".xls" || item.FOTO_TIPO.ToLower() == ".xlsx")
            //        {
            //            item.IMAGE_SRC = string.Format("data:application/vnd.ms-excel;base64,{0}", imageBase64);
            //            item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";
            //        }
            //        else if (item.FOTO_TIPO.ToLower() == ".ppt" || item.FOTO_TIPO.ToLower() == ".pptx")
            //        {
            //            item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
            //            item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/ppt.png";
            //        }
            //        else if (item.FOTO_TIPO.ToLower() == ".txt")
            //        {
            //            item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
            //            item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/txt.png";
            //        }
            //        else
            //        {
            //            item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
            //            item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/file.png";
            //            item.IMAGE_SRC = "#";
            //        }
            //    }

            //    a_reporte.INV_SOLO_FOTOS = SOLO_FOTOS;
            //}

            #endregion

            if (a_reporte.INV_FOTOS != null)
            {
                foreach (var imagen in a_reporte.INV_FOTOS)
                {
                    //imagen.FOTO_ARCHIVO = GetCompressedBitmap(ByteToImage(imagen.FOTO_ARCHIVO), 60L);
                    imagen.IMAGE_IMAGE = string.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(imagen.FOTO_ARCHIVO));
                }
            }

            ViewBag.Anexos = a_reporte.INV_FOTOS.Count;

            //List<INV_FOTOS> ffilt = new List<INV_FOTOS>();

            //foreach (var item in a_reporte.INV_SOLO_FOTOS)
            //{
            //    if(item.FOTO_ESTATUS==true)
            //    {
            //        ffilt.Add(item);
            //    }
            //}

            ViewBag.FotosActivas = a_reporte.INV_FOTOS;
            ViewBag.UsuarioImpresor = Session["usuario"].ToString();
            //ViewBag.ModeloFinal = a_reporte;

            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Descargó PDF de impresion del BI con el inca_no (" + a_reporte.INCA_NO + ")");

            try
            {
                Logs tosave_log = rl.returned_log;
                INV_INCAUTACIONES tosave_inca = db.INV_INCAUTACIONES.Where(x => x.INCA_NO == a_reporte.INCA_NO).FirstOrDefault();
                INV_INCALOGS inca_log = new INV_INCALOGS() { INCA_ID = tosave_inca.INCA_ID, LogId = tosave_log.LogId };
                db.INV_INCALOGS.Add(inca_log);
                db.SaveChanges();
            }
            catch (Exception error)
            {
                ModelState.AddModelError("",error.Message);
            }
            
            return new ViewAsPdf("ReporteEditar")
            {
                PageSize = Size.Letter,
            };
            //return new ViewAsPdf("ReporteEditar-copia")
            //{
            //    PageOrientation = Orientation.Portrait,
            //    PageSize = Size.A4,
            //    PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 },
            //};
        }

        [Authorize(Roles = "BIA,BIWR")]
        //diseño de reporte viejo
        public ActionResult ReporteV(int? id)
        {
            INV_INCAUTACIONES a_reporte = db.INV_INCAUTACIONES.Find(id);
            a_reporte.INV_CUSTODIO = db.INV_CUSTODIO.Where(x => x.CUST_ID == a_reporte.CUST_ID).FirstOrDefault();

            ViewBag.Clase = db.INV_CLASES.Where(x => x.CLAS_ID == a_reporte.CLAS_ID).Select(x => x.CLAS_NOMBRE).FirstOrDefault();
            ViewBag.Tipo = db.INV_TIPOS.Where(x => x.TIPO_ID == a_reporte.TIPO_ID).Select(x => x.TIPO_NOMBRE).FirstOrDefault();
            ViewBag.Estatus = db.INV_ESTATUS.Where(x => x.ESTA_ID == a_reporte.INCA_ESTATUS).Select(x => x.ESTA_NOMBRE).FirstOrDefault();
            ViewBag.ProvInc = db.INV_PROVINCIAS.Where(x => x.ID_Provincia == a_reporte.INCA_LOCALIDADINC).Select(x => x.Descripcion).FirstOrDefault();
            ViewBag.ProvInv = db.INV_PROVINCIAS.Where(x => x.ID_Provincia == a_reporte.INCA_LOCALIDADINV).Select(x => x.Descripcion).FirstOrDefault();
            ViewBag.Caso = db.INV_CASOS.Where(x => x.CASO_ID == a_reporte.CASO_ID).Select(x => x.CASO_NOMBRE).FirstOrDefault();
            ViewBag.direccion = a_reporte.INCA_DIRECCION;
            ViewBag.matricula = a_reporte.INCA_MATRICULA;
            ViewBag.descripcion = a_reporte.INCA_DESCRIPCION;

            a_reporte.INV_FOTOS = new List<INV_FOTOS>();
            a_reporte.INV_FOTOS = db.INV_FOTOS.Where(x => x.INCA_NO == a_reporte.INCA_NO).ToList();

            List<INV_FOTOS> SOLO_FOTOS = new List<INV_FOTOS>();

            if (a_reporte.INV_FOTOS != null)
            {
                foreach (var item in a_reporte.INV_FOTOS)
                {
                    string imageBase64 = Convert.ToBase64String(item.FOTO_ARCHIVO);

                    if (item.FOTO_TIPO.ToLower() == ".jpg" || item.FOTO_TIPO.ToLower() == ".jpeg")
                    {
                        item.IMAGE_SRC = string.Format("data:image/jpg;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = item.IMAGE_SRC;
                        SOLO_FOTOS.Add(item);
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".png")
                    {
                        item.IMAGE_SRC = string.Format("data:image/png;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = item.IMAGE_SRC;
                        SOLO_FOTOS.Add(item);
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".gif")
                    {
                        item.IMAGE_SRC = string.Format("data:image/gif;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = item.IMAGE_SRC;
                        SOLO_FOTOS.Add(item);
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".pdf")
                    {
                        item.IMAGE_SRC = string.Format("data:application/pdf;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/pdf.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".doc" || item.FOTO_TIPO.ToLower() == ".docx")
                    {
                        item.IMAGE_SRC = string.Format("data:application/msword;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/doc.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".xls" || item.FOTO_TIPO.ToLower() == ".xlsx")
                    {
                        item.IMAGE_SRC = string.Format("data:application/vnd.ms-excel;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".ppt" || item.FOTO_TIPO.ToLower() == ".pptx")
                    {
                        item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/ppt.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".txt")
                    {
                        item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/txt.png";
                    }
                    else
                    {
                        item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/file.png";
                        item.IMAGE_SRC = "#";
                    }
                }

                a_reporte.INV_SOLO_FOTOS = SOLO_FOTOS;
            }

            ViewBag.Anexos = a_reporte.INV_FOTOS.Count;

            List<INV_FOTOS> ffilt = new List<INV_FOTOS>();

            foreach (var item in a_reporte.INV_SOLO_FOTOS)
            {
                if (item.FOTO_ESTATUS==true)
                {
                    ffilt.Add(item);
                }
            }

            ViewBag.FotosActivas = ffilt;
            ViewBag.UsuarioImpresor = Session["usuario"].ToString();
            ViewBag.ModeloFinal = a_reporte;

            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Descargó PDF de impresion del BI con el inca_no (" + a_reporte.INCA_NO + ")");

            try
            {
                Logs tosave_log = rl.returned_log;
                INV_INCAUTACIONES tosave_inca = db.INV_INCAUTACIONES.Where(x => x.INCA_NO == a_reporte.INCA_NO).FirstOrDefault();
                INV_INCALOGS inca_log = new INV_INCALOGS() { INCA_ID = tosave_inca.INCA_ID, LogId = tosave_log.LogId };
                db.INV_INCALOGS.Add(inca_log);
                db.SaveChanges();
            }
            catch (Exception error)
            {
                ModelState.AddModelError("", error.Message);
            }

            //return new ViewAsPdf("ReporteEditar")
            //{
            //    PageSize = Size.Letter,
            //};
            return new ViewAsPdf("ReporteEditar-copia")
            {
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 },
            };
        }

        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult ReporteEditars(int? id)
        {
            INV_INCAUTACIONES a_reporte = db.INV_INCAUTACIONES.Find(id);
            ViewBag.a_reporte = a_reporte;
            a_reporte.INV_CUSTODIO = db.INV_CUSTODIO.Where(x => x.CUST_ID == a_reporte.CUST_ID).FirstOrDefault();

            ViewBag.Clase = db.INV_CLASES.Where(x => x.CLAS_ID == a_reporte.CLAS_ID).Select(x => x.CLAS_NOMBRE).FirstOrDefault();
            ViewBag.Tipo = db.INV_TIPOS.Where(x => x.TIPO_ID == a_reporte.TIPO_ID).Select(x => x.TIPO_NOMBRE).FirstOrDefault();
            ViewBag.Estatus = db.INV_ESTATUS.Where(x => x.ESTA_ID == a_reporte.INCA_ESTATUS).Select(x => x.ESTA_NOMBRE).FirstOrDefault();
            ViewBag.ProvInc = db.INV_PROVINCIAS.Where(x => x.ID_Provincia == a_reporte.INCA_LOCALIDADINC).Select(x => x.Descripcion).FirstOrDefault();
            ViewBag.ProvInv = db.INV_PROVINCIAS.Where(x => x.ID_Provincia == a_reporte.INCA_LOCALIDADINV).Select(x => x.Descripcion).FirstOrDefault();
            ViewBag.Caso = db.INV_CASOS.Where(x => x.CASO_ID == a_reporte.CASO_ID).Select(x => x.CASO_NOMBRE).FirstOrDefault();
            //ViewBag.direccion = a_reporte.INCA_DIRECCION;
            //ViewBag.matricula = a_reporte.INCA_MATRICULA;
            //ViewBag.descripcion = a_reporte.INCA_DESCRIPCION;

            a_reporte.INV_FOTOS = db.INV_FOTOS.Where(x => x.INCA_NO == a_reporte.INCA_NO && x.FOTO_ESTATUS == true).ToList();

            List<INV_FOTOS> SOLO_FOTOS = new List<INV_FOTOS>();

            if (a_reporte.INV_FOTOS != null)
            {
                foreach (var item in a_reporte.INV_FOTOS)
                {
                    string imageBase64 = Convert.ToBase64String(item.FOTO_ARCHIVO);

                    if (item.FOTO_TIPO.ToLower() == ".jpg" || item.FOTO_TIPO.ToLower() == ".jpeg")
                    {
                        item.IMAGE_SRC = string.Format("data:image/jpg;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = item.IMAGE_SRC;
                        SOLO_FOTOS.Add(item);
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".png")
                    {
                        item.IMAGE_SRC = string.Format("data:image/png;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = item.IMAGE_SRC;
                        SOLO_FOTOS.Add(item);
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".gif")
                    {
                        item.IMAGE_SRC = string.Format("data:image/gif;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = item.IMAGE_SRC;
                        SOLO_FOTOS.Add(item);
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".pdf")
                    {
                        item.IMAGE_SRC = string.Format("data:application/pdf;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/pdf.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".doc" || item.FOTO_TIPO.ToLower() == ".docx")
                    {
                        item.IMAGE_SRC = string.Format("data:application/msword;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/doc.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".xls" || item.FOTO_TIPO.ToLower() == ".xlsx")
                    {
                        item.IMAGE_SRC = string.Format("data:application/vnd.ms-excel;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".ppt" || item.FOTO_TIPO.ToLower() == ".pptx")
                    {
                        item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/ppt.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".txt")
                    {
                        item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/txt.png";
                    }
                    else
                    {
                        item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/file.png";
                        item.IMAGE_SRC = "#";
                    }
                }

                a_reporte.INV_SOLO_FOTOS = SOLO_FOTOS;
            }

            ViewBag.Anexos = a_reporte.INV_FOTOS.Count;

            //List<INV_FOTOS> ffilt = new List<INV_FOTOS>();

            //foreach (var item in a_reporte.INV_SOLO_FOTOS)
            //{
            //    if(item.FOTO_ESTATUS==true)
            //    {
            //        ffilt.Add(item);
            //    }
            //}

            ViewBag.FotosActivas = a_reporte.INV_SOLO_FOTOS;
            ViewBag.UsuarioImpresor = Session["usuario"].ToString();
            ViewBag.ModeloFinal = a_reporte;

            return View("ReporteEditar");
        }

        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult ReporteVs(int? id)
        {
            INV_INCAUTACIONES a_reporte = db.INV_INCAUTACIONES.Find(id);
            a_reporte.INV_CUSTODIO = db.INV_CUSTODIO.Where(x => x.CUST_ID == a_reporte.CUST_ID).FirstOrDefault();

            ViewBag.Clase = db.INV_CLASES.Where(x => x.CLAS_ID == a_reporte.CLAS_ID).Select(x => x.CLAS_NOMBRE).FirstOrDefault();
            ViewBag.Tipo = db.INV_TIPOS.Where(x => x.TIPO_ID == a_reporte.TIPO_ID).Select(x => x.TIPO_NOMBRE).FirstOrDefault();
            ViewBag.Estatus = db.INV_ESTATUS.Where(x => x.ESTA_ID == a_reporte.INCA_ESTATUS).Select(x => x.ESTA_NOMBRE).FirstOrDefault();
            ViewBag.ProvInc = db.INV_PROVINCIAS.Where(x => x.ID_Provincia == a_reporte.INCA_LOCALIDADINC).Select(x => x.Descripcion).FirstOrDefault();
            ViewBag.ProvInv = db.INV_PROVINCIAS.Where(x => x.ID_Provincia == a_reporte.INCA_LOCALIDADINV).Select(x => x.Descripcion).FirstOrDefault();
            ViewBag.Caso = db.INV_CASOS.Where(x => x.CASO_ID == a_reporte.CASO_ID).Select(x => x.CASO_NOMBRE).FirstOrDefault();
            ViewBag.direccion = a_reporte.INCA_DIRECCION;
            ViewBag.matricula = a_reporte.INCA_MATRICULA;
            ViewBag.descripcion = a_reporte.INCA_DESCRIPCION;

            a_reporte.INV_FOTOS = new List<INV_FOTOS>();
            a_reporte.INV_FOTOS = db.INV_FOTOS.Where(x => x.INCA_NO == a_reporte.INCA_NO).ToList();

            List<INV_FOTOS> SOLO_FOTOS = new List<INV_FOTOS>();

            if (a_reporte.INV_FOTOS != null)
            {
                foreach (var item in a_reporte.INV_FOTOS)
                {
                    string imageBase64 = Convert.ToBase64String(item.FOTO_ARCHIVO);

                    if (item.FOTO_TIPO.ToLower() == ".jpg" || item.FOTO_TIPO.ToLower() == ".jpeg")
                    {
                        item.IMAGE_SRC = string.Format("data:image/jpg;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = item.IMAGE_SRC;
                        SOLO_FOTOS.Add(item);
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".png")
                    {
                        item.IMAGE_SRC = string.Format("data:image/png;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = item.IMAGE_SRC;
                        SOLO_FOTOS.Add(item);
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".gif")
                    {
                        item.IMAGE_SRC = string.Format("data:image/gif;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = item.IMAGE_SRC;
                        SOLO_FOTOS.Add(item);
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".pdf")
                    {
                        item.IMAGE_SRC = string.Format("data:application/pdf;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/pdf.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".doc" || item.FOTO_TIPO.ToLower() == ".docx")
                    {
                        item.IMAGE_SRC = string.Format("data:application/msword;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/doc.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".xls" || item.FOTO_TIPO.ToLower() == ".xlsx")
                    {
                        item.IMAGE_SRC = string.Format("data:application/vnd.ms-excel;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".ppt" || item.FOTO_TIPO.ToLower() == ".pptx")
                    {
                        item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/ppt.png";
                    }
                    else if (item.FOTO_TIPO.ToLower() == ".txt")
                    {
                        item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/txt.png";
                    }
                    else
                    {
                        item.IMAGE_SRC = string.Format("data:application/vnd.ms-powerpoint;base64,{0}", imageBase64);
                        item.IMAGE_IMAGE = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/file.png";
                        item.IMAGE_SRC = "#";
                    }
                }

                a_reporte.INV_SOLO_FOTOS = SOLO_FOTOS;
            }

            ViewBag.Anexos = a_reporte.INV_FOTOS.Count;

            List<INV_FOTOS> ffilt = new List<INV_FOTOS>();

            foreach (var item in a_reporte.INV_SOLO_FOTOS)
            {
                if (item.FOTO_ESTATUS==true)
                {
                    ffilt.Add(item);
                }
            }

            ViewBag.FotosActivas = ffilt;
            ViewBag.UsuarioImpresor = Session["usuario"].ToString();
            ViewBag.ModeloFinal = a_reporte;

            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Descargó PDF de impresion del BI con el inca_no (" + a_reporte.INCA_NO + ")");

            try
            {
                Logs tosave_log = rl.returned_log;
                INV_INCAUTACIONES tosave_inca = db.INV_INCAUTACIONES.Where(x => x.INCA_NO == a_reporte.INCA_NO).FirstOrDefault();
                INV_INCALOGS inca_log = new INV_INCALOGS() { INCA_ID = tosave_inca.INCA_ID, LogId = tosave_log.LogId };
                db.INV_INCALOGS.Add(inca_log);
                db.SaveChanges();
            }
            catch (Exception error)
            {
                ModelState.AddModelError("", error.Message);
            }

            //return new ViewAsPdf("ReporteEditar")
            //{
            //    PageSize = Size.Letter,
            //};
            return View("ReporteEditar-copia");
        }

        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult RegistroActividad(int? id)
        {
            INV_INCALOGS inca_log = new INV_INCALOGS();

            inca_log.INV_INCASLOGS = db.INV_INCALOGS.Where(x => x.INCA_ID == id).ToList();
            
            using (dbIntranet intra = new dbIntranet())
            {
                foreach (var item in inca_log.INV_INCASLOGS)
                {
                    Logs log = intra.Logs.Find(item.LogId);
                    //Vw_Mostrar_Personal_Permisos_PGR us = new Vw_Mostrar_Personal_Permisos_PGR();
                    Vw_Mostrar_Personal_Permisos_PGR us = intra.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == log.LogUsuario).FirstOrDefault();
                    log.LogUsuarioNombre = us.NombreCompleto;
                    inca_log.Logs.Add(log);
                }
            }

            return View(inca_log.Logs);
        }

        [HttpPost]
        public JsonResult LoadCustodios()
        {
            try
            {
                var custodios = db.INV_CUSTODIO.Where(x => x.CUST_ID != 1 && x.CUST_ESTADO == true).Select(a => new {
                    Value = a.CUST_ID,
                    Text = !string.IsNullOrEmpty(a.CUST_NOMBRE) ? a.CUST_NOMBRE : a.CUST_DEPARTAMENTO
                }).ToList();

                if(custodios.Count<1)
                {
                    return Json("Error");
                }
                else
                {
                    return Json(custodios);
                }
            }catch(Exception)
            {
                return Json("Error");
            }

            
        }

        [HttpGet]
        public FileResult DownloadFile(int id)
        {
            var file = db.INV_FOTOS.Where(x => x.FOTO_ID == id && x.FOTO_ESTATUS != null).FirstOrDefault();

            return File(file.FOTO_ARCHIVO, file.FOTO_TIPO, file.FOTO_NOMBRE);
        }

        public ActionResult ReporteExcel(object algo = null) {

            ViewBag.Casos = new SelectList(db.INV_CASOS.Where(x => x.ESTA_ID != 6 && x.ESTA_ID != 12).ToList(), "CASO_ID", "CASO_NOMBRE");

            ViewBag.Clases = new SelectList(db.INV_CLASES.ToList(), "CLAS_ID", "CLAS_NOMBRE");

            //ViewBag.Tipos = new SelectList(db.INV_TIPOS.Where(x => x.CLAS_ID == view_object.CLAS_ID).ToList(), "TIPO_ID", "TIPO_NOMBRE", view_object.TIPO_ID);

            ViewBag.Colores = new SelectList(db.INV_COLORES.ToList(), "COLOR_ID", "COLOR_NOMBRE");

            ViewBag.ProvInc = ViewBag.ProvInv = new SelectList(db.INV_PROVINCIAS.ToList(), "ID_Provincia", "Descripcion");

            ViewBag.Custodios = new SelectList(db.INV_CUSTODIO.Where(x => x.CUST_ESTADO == true).ToList(), "CUST_ID", "CUST_NOMBRE");

            ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 1).ToList(), "ESTA_ID", "ESTA_NOMBRE");

            return View();
        }

        [HttpGet]
        public ActionResult GenerarExcel(FiltroVM filtro)
        {
            try
            {
                DateTime FechaInicial = filtro.FechaTipo != 0 ? DateTime.ParseExact(filtro.INCA_FHINICIAL, "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture) : new DateTime();
                DateTime FechaFinal = filtro.FechaTipo != 0 ? DateTime.ParseExact(filtro.INCA_FHFINAL, "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture) : new DateTime();

                var Clases = db.INV_CLASES.Where(cl=> (filtro.Clases.Contains(cl.CLAS_ID) || filtro.Clases.Contains(0)) ).ToList();

                filtro.INCA_DT_FHINICIAL = FechaInicial;
                filtro.INCA_DT_FHFINAL = FechaFinal;

                var reporte = Consultar(filtro);

                string TablaExcel = "<head><meta charset=\"utf-8\"></head><style>table{font-size:15px;}</style>"; //"<style>table{font-size:20px;}</style>";

                for (int i = 0; i < (filtro.Clases.Contains(0) ? Clases.Count : filtro.Clases.Length) ; i++)
                {
                    var grid = new System.Web.UI.WebControls.GridView();

                    var Id_clase = filtro.Clases.Contains(0) ? Clases.Select(x=>x.CLAS_ID).ToArray()[i] : filtro.Clases[i];

                    var modelo = ConvertirModel(reporte.Where(x => x.CLAS_ID == Id_clase).ToList(), Clases.FirstOrDefault(cl => cl.CLAS_ID == Id_clase).CLAS_ID);

                    int columnas = modelo.GetType().GetProperties()[0].PropertyType.GenericTypeArguments.Length;

                    //caccc2.GenericTypeArguments;

                    //QUITAR EL TAKE AL SUBIR!!!!!!!!!!!!!!!!
                    grid.DataSource = modelo;// ConvertirModel(reporte.Where(x => x.CLAS_ID == filtro.Clases[i]).Take(10).ToList(), Clases.FirstOrDefault(cl=>cl.CLAS_ID == filtro.Clases[i]).CLAS_ID);

                    grid.DataBind();

                    StringWriter sw = new StringWriter();

                    System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);

                    grid.RenderControl(htw);

                    //string Tabla = sw.ToString() == "<div>\r\n\r\n</div>" ? 
                    //sw.ToString().Replace("<div>\r\n\r\n</div>",
                    //                "<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t<th colspan=\"" + columnas + "\" >" +
                    //                " SISTEMA CENTRAL DE BIENES INCAUTADOS, REPORTE DE " + Clases.FirstOrDefault(cl => cl.CLAS_ID == Id_clase).CLAS_NOMBRE.ToUpper() + "</th></tr>") + "</table></div><br>" :

                    //sw.ToString().Replace("<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t",
                    //                    "<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t<th colspan=\""+columnas+"\" >" +
                    //                    " SISTEMA CENTRAL DE BIENES INCAUTADOS, REPORTE DE "+ Clases.FirstOrDefault(cl => cl.CLAS_ID == Id_clase).CLAS_NOMBRE.ToUpper() + "</th></tr>")+"<br>";

                    string Tabla = sw.ToString().Replace("<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t",
                                        "<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t<th colspan=\"" + columnas + "\" >" +
                                        " SISTEMA CENTRAL DE BIENES INCAUTADOS, REPORTE DE " + Clases.FirstOrDefault(cl => cl.CLAS_ID == Id_clase).CLAS_NOMBRE.ToUpper() + "</th></tr>") + "<br>";

                    TablaExcel = TablaExcel + Tabla;
                }

                //var grid = new System.Web.UI.WebControls.GridView();
                //var grid2 = new System.Web.UI.WebControls.GridView();
                //var grid3 = new System.Web.UI.WebControls.GridView();

                //grid.DataSource = ConvertirModel(reporte.Where(x=>x.CLAS_ID == 1).ToList(), 1);
                //grid2.DataSource = ConvertirModel(reporte.Where(x => x.CLAS_ID == 3).ToList(), 3);
                //grid3.DataSource = ConvertirModel(reporte.Where(x => x.CLAS_ID == 2).ToList(), 2);

                //grid.DataBind();
                //grid2.DataBind();
                //grid3.DataBind();

                //StringWriter sw = new StringWriter();
                //StringWriter sw2 = new StringWriter();
                //StringWriter sw3 = new StringWriter();

                //System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                //System.Web.UI.HtmlTextWriter htw2 = new System.Web.UI.HtmlTextWriter(sw2);
                //System.Web.UI.HtmlTextWriter htw3 = new System.Web.UI.HtmlTextWriter(sw3);

                //grid.RenderControl(htw);
                //grid2.RenderControl(htw2);
                //grid3.RenderControl(htw3);

                ////grid.HeaderRow.Cells.Add(header);// .a .HeaderRow = "Hola";

                ////string valor = sw.ToString();

                //string String = sw2.ToString().Replace("<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t",
                //    "<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t<th colspan=\"14\" > SISTEMA CENTRAL DE BIENES INCAUTADOS, REPORTE DE OBJETOS</th></tr>");

                //string String2 = sw.ToString().Replace("<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t",
                //                    "<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t<th colspan=\"19 \" > SISTEMA CENTRAL DE BIENES INCAUTADOS, REPORTE DE INMUEBLES</th></tr>");

                //string String3 = sw3.ToString().Replace("<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t",
                //                    "<div>\r\n\t<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t<tr>\r\n\t\t\t<th colspan=\"21\" > SISTEMA CENTRAL DE BIENES INCAUTADOS, REPORTE DE VEHICULOS DE MOTOR</th></tr>");

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachement; filename=Bienes Incautados.xls");
                Response.ContentType = "application/excel";

                Response.Output.Write(TablaExcel);
                Response.Flush();
                Response.End();

                string empId = HttpContext.Session["empleadoId"].ToString();

                //RegisterLogs rl = new RegisterLogs(empId, "Genero un reporte excel del Modulo BI");

                return ReporteExcel();
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "error";
                ex.ToString();
            }
            return RedirectToAction("ReporteExcel");
        }

        [HttpPost]
        public JsonResult ValidarConsultaExcel(FiltroVM filtro) {

            DateTime FechaInicial = new DateTime();
            DateTime FechaFinal = new DateTime();

            if (filtro.FechaTipo != 0 && !(DateTime.TryParseExact(filtro.INCA_FHINICIAL, "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out FechaInicial)
                && DateTime.TryParseExact(filtro.INCA_FHFINAL, "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out FechaFinal)))
            {
                return Json("fechaError");
            }

            filtro.INCA_DT_FHINICIAL = FechaInicial;
            filtro.INCA_DT_FHFINAL = FechaFinal;

            var cantidad = Consultar(filtro).Count;

            return Json(new { cantidad = cantidad });
        }

        private object ConvertirModel(List<VISTA_INCAUTACIONES> reporte, int clase)
        {
            switch (clase) {

                case 0:
                    return reporte.Select(x => new {

                        //INFORMACIÓN GENERAL DE UNA INCAUTACIÓN

                        CÓDIGO_DEL_BIEN = x.INCA_NO.ToUpper(),
                        CASO = x.CASO_NOMBRE,
                        FECHA_DE_ENTRADA = x.INCA_FHENTRADA.ToString("dd/MM/yyyy"),
                        //CLASE = x.CLAS_NOMBRE,
                        TIPO = x.TIPO_NOMBRE,
                        COLOR = x.COLOR_NOMBRE,
                        LUGAR_DE_INCAUTACIÓN = x.ProvinciaOrigen,
                        LOCALIZACIÓN_ACTUAL = x.ProvinciaDestino,
                        //PAGO_DE_MANTENIMIENTO = x.INCA_PAGOMANT,
                        ESTATUS = x.ESTA_NOMBRE,
                        VALOR_TASACIÓN = x.INCA_VALOR,
                        VALOR_ACUERDO = x.INCA_ACUERDO,
                        USUARIO_CREADOR = x.USUA_ID,
                        CREACION_REGISTRO = x.INCA_FHCREACION.ToString("dd/MM/yyyy"),
                        DESCRIPCIÓN = x.INCA_DESCRIPCION,
                        NOTA = x.INCA_NOTA,

                        //INFORMACIÓN INCAUTACIÓN INMUEBLE

                        DIRECCIÓN = x.INCA_DIRECCION,

                        //INFORMACIÓN INCAUTACIÓN VEHÍCULO

                        MODELO = x.INCA_MODELO,
                        MARCA = x.INCA_MARCA,
                        AÑO = x.INCA_ANO,
                        CHASIS = x.INCA_CHASIS,
                        MATRÍCULA = x.INCA_MATRICULA,
                        PLACA = x.INCA_PLACA,
                        //REMITIDO_POR = x.INCA_REMITIDO,
                        //FECHA_DE_VENCIMIENTO_DEL_SEGURO = x.INCA_FHVENCSEGURO,
                        //DEVUELTO_A = x.INCA_DEVOLUCION,
                        //FECHA_DE_DEVOLUCION = x.INCA_FHDEVOLUCION,
                        CONTACTO = x.INCA_TELEDEVCONTACTO,

                        //INFORMACIÓN INCAUTACIÓN CUSTODIO

                        CUSTODIADO_POR = x.CUST_NOMBRE,
                        CEDULA_CUSTODIO = x.CUST_CEDULA,
                        TELEFONO_RESIDENCIAL = x.CUST_TELRES,
                        TELEFONO_CELULAR = x.CUST_TELCEL,
                        TELEFONO_OFICINA = x.CUST_TELOFI

                    }).ToArray();

                case 1:
                    return reporte.Select(x => new {

                        //INFORMACIÓN INCAUTACIÓN INMUEBLE

                        CÓDIGO_DEL_BIEN = x.INCA_NO.ToUpper(),
                        CASO = x.CASO_NOMBRE,
                        FECHA_DE_ENTRADA = x.INCA_FHENTRADA.ToString("dd/MM/yyyy"),
                        TIPO = x.TIPO_NOMBRE,
                        DIRECCIÓN = x.INCA_DIRECCION,
                        DESCRIPCIÓN = x.INCA_DESCRIPCION,
                        LUGAR_DE_INCAUTACIÓN = x.ProvinciaOrigen,
                        LOCALIZACIÓN_ACTUAL = x.ProvinciaDestino,
                        ESTATUS = x.ESTA_NOMBRE,
                        VALOR_ACUERDO = x.INCA_ACUERDO,

                        CUSTODIADO_POR = x.CUST_NOMBRE,
                        CUSTODIO_DOCUMENTO = x.CUST_CEDULA,
                        TELEFONO_RESIDENCIAL = x.CUST_TELRES,
                        TELEFONO_CELULAR = x.CUST_TELCEL,
                        TELEFONO_OFICINA = x.CUST_TELOFI,
                        CONTACTO_RELACIONADO = x.CUST_RELACIONADO,
                        TELEFONO_DE_CONTACTO = x.CUST_RELACIONADO_CONTACTO,

                        NOTA = x.INCA_NOTA,
                        VALOR_TASACIÓN = x.INCA_VALOR
                    });
                     
                case 3:

                    return reporte.Select(x => new {

                        //INFORMACIÓN DE UNA INCAUTACIÓN OBJETO

                        CÓDIGO_DEL_BIEN = x.INCA_NO.ToUpper(),
                        CASO = x.CASO_NOMBRE,
                        FECHA_DE_ENTRADA = x.INCA_FHENTRADA.ToString("dd/MM/yyyy"),
                        TIPO = x.TIPO_NOMBRE,
                        DESCRIPCIÓN = x.INCA_DESCRIPCION,
                        LUGAR_DE_INCAUTACIÓN = x.ProvinciaOrigen,
                        LOCALIZACIÓN_ACTUAL = x.ProvinciaDestino,
                        ESTATUS = x.ESTA_NOMBRE,

                        CUSTODIADO_POR = x.CUST_NOMBRE,
                        CUSTODIO_DOCUMENTO = x.CUST_CEDULA,
                        TELEFONO_RESIDENCIAL = x.CUST_TELRES,
                        TELEFONO_CELULAR = x.CUST_TELCEL,
                        TELEFONO_OFICINA = x.CUST_TELOFI,
                        //x.CONTACTO_RELACIONADO,
                        //x.TELEFONO_DE_CONTACTO,

                        NOTA = x.INCA_NOTA

                    });

                default:
                    return reporte.Select(x => new {

                        //INFORMACIÓN INCAUTACIÓN VEHÍCULO

                        CÓDIGO_DEL_BIEN = x.INCA_NO.ToUpper(),
                        CASO = x.CASO_NOMBRE,
                        FECHA_DE_ENTRADA = x.INCA_FHENTRADA.ToString("dd/MM/yyyy"),
                        TIPO = x.TIPO_NOMBRE,
                        CHASIS = x.INCA_CHASIS,
                        MATRÍCULA = x.INCA_MATRICULA,
                        AÑO = x.INCA_ANO,
                        MODELO = x.INCA_MODELO,
                        MARCA = x.INCA_MARCA,
                        COLOR = x.COLOR_NOMBRE,
                        PLACA = x.INCA_PLACA,
                        LUGAR_DE_INCAUTACIÓN = x.ProvinciaOrigen,
                        LOCALIZACIÓN_ACTUAL = x.ProvinciaDestino,
                        ESTATUS = x.ESTA_NOMBRE,

                        CUSTODIADO_POR = x.CUST_NOMBRE,
                        CUSTODIO_DOCUMENTO = x.CUST_CEDULA,
                        TELEFONO_RESIDENCIAL = x.CUST_TELRES,
                        TELEFONO_CELULAR = x.CUST_TELCEL,
                        TELEFONO_OFICINA = x.CUST_TELOFI,
                        //x.CONTACTO_RELACIONADO,
                        //x.TELEFONO_DE_CONTACTO,

                        DESCRIPCIÓN = x.INCA_DESCRIPCION,
                        NOTA = x.INCA_NOTA

                    });
            }
        }

        private List<VISTA_INCAUTACIONES> Consultar(FiltroVM filtro) {

            return db.VISTA_INCAUTACIONES.Where(inc => inc.INCA_ESTATUS != 12 && inc.INCA_ESTATUS != 15
                && (inc.CASO_ID == filtro.Casos || filtro.Casos == 0)
                && (filtro.Clases.Contains(inc.CLAS_ID) || filtro.Clases.Contains(0))
                && (inc.TIPO_ID == filtro.Tipos || filtro.Tipos == 0)
                && (inc.COLOR_ID == filtro.Colores || filtro.Colores == 0)
                && (inc.INCA_LOCALIDADINC == filtro.ProvInc || filtro.ProvInc == 0)
                && (inc.INCA_LOCALIDADINV == filtro.ProvInv || filtro.ProvInv == 0)
                && (inc.CUST_ID == filtro.Custodios || filtro.Custodios == 0)
                && (inc.INCA_ESTATUS == filtro.Estatus || filtro.Estatus == 0)
                && (inc.INCA_FHENTRADA >= filtro.INCA_DT_FHINICIAL && inc.INCA_FHENTRADA <= filtro.INCA_DT_FHFINAL || filtro.FechaTipo != 1)
                && (inc.INCA_FHCREACION >= filtro.INCA_DT_FHINICIAL && inc.INCA_FHCREACION <= filtro.INCA_DT_FHFINAL || filtro.FechaTipo != 2)).ToList();
        }

        private INV_CUSTODIO GetCustodio(string StringCustodio)
        {
            var custodioID = 0;
            var ValidarcustodioId = int.TryParse(StringCustodio, out custodioID);

            return ValidarcustodioId ? db.INV_CUSTODIO.Find(custodioID) : db.INV_CUSTODIO.Find(1);
        }

        private List<INV_COLORES> getColors()
        {

            var lista = new List<INV_COLORES>();
            lista.Add(new INV_COLORES { COLOR_ID = 1, COLOR_NOMBRE = "INDEFINIDO" });
            lista.AddRange(db.INV_COLORES.Where(x => x.COLOR_ID != 1).OrderBy(x => x.COLOR_NOMBRE));
            return lista;
        }

        private void RegistarLog( string descripcion, int INCA_ID , bool? CambioCustodio, int? CustodioID) {

            string empId = HttpContext.Session["empleadoId"].ToString();

            RegisterLogs rl = new RegisterLogs(empId, descripcion);

            try
            {
                Logs tosave_log = rl.returned_log;

                INV_INCALOGS inca_log = CambioCustodio != null ? new INV_INCALOGS() { INCA_ID = INCA_ID, LogId = tosave_log.LogId, CUST_ID = CustodioID, CUST_CAMBIO = true } :
                    new INV_INCALOGS() { INCA_ID = INCA_ID, LogId = tosave_log.LogId };

                db.INV_INCALOGS.Add(inca_log);
                db.SaveChanges();
            }
            catch (Exception error)
            {
                ModelState.AddModelError("", error.Message);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Optimizacion de Imagen

        //public static System.Drawing.Image ByteToImage(byte[] archivo)
        //{
        //    using (MemoryStream mStream = new MemoryStream())
        //    {
        //        mStream.Write(archivo, 0, archivo.Length);
        //        mStream.Seek(0, SeekOrigin.Begin);

        //        System.Drawing.Bitmap bm = new System.Drawing.Bitmap(mStream);
        //        return bm;
        //    }
        //}

        //public byte[] GetCompressedBitmap(System.Drawing.Image bmp, long quality)
        //{
        //    using (var mss = new MemoryStream())
        //    {
        //        System.Drawing.Bitmap bit = new System.Drawing.Bitmap(bmp);

        //        System.Drawing.Imaging.EncoderParameter qualityParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
        //        System.Drawing.Imaging.ImageCodecInfo jgpEncoder = GetEncoder(System.Drawing.Imaging.ImageFormat.Jpeg);
        //        System.Drawing.Imaging.EncoderParameters parameters = new System.Drawing.Imaging.EncoderParameters(1);
        //        parameters.Param[0] = qualityParam;
        //        bit.Save(mss, jgpEncoder, parameters);
        //        return mss.ToArray();
        //    }
        //}

        //private ImageCodecInfo GetEncoder(ImageFormat format)
        //{
        //    ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

        //    foreach (ImageCodecInfo codec in codecs)
        //    {
        //        if (codec.FormatID == format.Guid)
        //        {
        //            return codec;
        //        }
        //    }
        //    return null;
        //}

        #endregion
    }
}