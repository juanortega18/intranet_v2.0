﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.ActualizacionDatos;
using intra.Models.ViewModel;
using intra.Models;
using Rotativa;
using System.IO;
using System.Web.UI.WebControls;
using intra.Models.Servicios;
using System.Globalization;

namespace intra.Controllers
{
    public class VISTA_EMPLEADOSController : Controller
    {
        private dbIntranet db = new dbIntranet();
        private DATOSEMPLEADOS empleado = new DATOSEMPLEADOS();

        public ActionResult Index()
        {

            ViewBag.cargos = new SelectList(db.vw_Cargos.ToList(), "CargoId", "Descripcion");
            ViewBag.dependencias = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");

            return View();
        }

        //Consulta de Empleados por Cumpleaños, Sexo
        public ActionResult ConsultaEmpleado()
        {
            ViewBag.dependencias = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");


            return View();
        }

        
        [HttpGet]
        public ActionResult ReporteSoporteUsuarios()
        {

                             
            return View();
        }


        [HttpPost]
        public ActionResult ReporteSoporteUsuarios(FormCollection fc)
        {
            
            if(!string.IsNullOrEmpty(Request.Form["FechaFinal"]) || !string.IsNullOrEmpty(Request.Form["FechaInicial"])) { 
            DateTime fechaFin = DateTime.ParseExact(Request.Form["FechaFinal"], "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime fechaInicio = DateTime.ParseExact(Request.Form["FechaInicial"], "dd/MM/yyyy", CultureInfo.InvariantCulture);

             var reporte = (from m in db.Sol_Detalle_Lista
                            from e in db.FlotaExtensionEmpleado
                           where m.CodigoTecnico == 2252
                           || m.CodigoTecnico == 2922
                           || m.CodigoTecnico == 19766
                           || m.CodigoTecnico == 15004 && m.CodigoEstadoSolicitud == 2
                           where e.FlotExtEmpEmpleadoId == m.CodigoSolicitante
                           select new ReporteSoporteUsuarios
                          
                           {
                               TecnicoId = m.CodigoTecnico.ToString(),
                               Solicitante = m.NombreSolicitante,
                               Tecnico = m.NombreTecnico,
                               Fecha = m.FechaInicioSolicitud,
                               Extension = e.FlotExtEmpExtesion,
                               Departamento = m.DepartamentoSolicitante,
                               DescripcionSolicitud = m.DescripcionSolicitud
                           }).Where(x => x.Fecha >= fechaInicio && x.Fecha <= fechaFin 
                           ).ToList();

                var grid = new GridView();
                grid.DataSource = reporte;
                grid.DataBind();

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachement; filename=ReporteSoporteUsuarios.xls");
                Response.ContentType = "application/excel";
                StringWriter sw = new StringWriter();
                System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                grid.RenderControl(htw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
                return View(reporte);

                //return new ViewAsPdf("ReporteSoporteUsuarios", reporte)
                //{
                //    PageSize = Rotativa.Options.Size.Letter


                //};
            }

         
                
                return View("ReporteSoporteUsuarios");

        }

        [HttpPost]
        public JsonResult ConsultaEmpleados(int? filtro, string Sexo, int? cumpleanos, string departamento, string dependencia, FormCollection fc)
        {


            string dependenciaId = dependencia;
            string departamentoId = departamento;

            var empleados = db.VISTA_EMPLEADOS.ToList();



            var dependencias = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");

            if (filtro == 1)
            {
                try
                {
                    empleados = empleados.Where(x => x.dependenciaid == dependenciaId).ToList();
                    ViewBag.dependencias = dependencias;
                    return Json(empleados);
                }
                catch (Exception e)
                {
                    e.ToString();
                    return Json(new { success = false, Mensaje = "Ocurrio un error mientras se procesaba su solicitud, Intente mas tarde" });

                }

            }
            else if (filtro == 2)
            {
                try
                {
                    empleados = empleados.Where(x => x.dependenciaid == dependenciaId && x.departamentoid == departamentoId).ToList();
                    ViewBag.dependencias = dependencias;
                    return Json(empleados);
                }
                catch (Exception e)
                {
                    e.ToString();
                    return Json(new { success = false, Mensaje = "Ocurrio un error mientras se procesaba su solicitud, Intente mas tarde" });

                }
            }
            else if (filtro == 3)
            {
                try
                {
                    empleados = empleados.Where(x => x.dependenciaid == dependenciaId && x.Sexo == Sexo).ToList();
                    ViewBag.dependencias = dependencias;
                    return Json(empleados);
                }
                catch (Exception e)
                {
                    e.ToString();
                    return Json(new { success = false, Mensaje = "Ocurrio un error mientras se procesaba su solicitud, Intente mas tarde" });
                }
            }
            else if (filtro == 4)
            {
                try
                {
                    empleados = empleados.Where(x => x.dependenciaid == dependenciaId && x.Cumpleano.Month == cumpleanos).ToList();
                    ViewBag.dependencias = dependencias;
                    return Json(empleados);
                }
                catch (Exception e)
                {
                    e.ToString();
                    return Json(new { success = false, Mensaje = "Ocurrio un error mientras se procesaba su solicitud, Intente mas tarde" });


                }
            }

            return Json(empleados);
        }


        //Seleccionar Departamento
        [HttpPost]
        public JsonResult SeleccionarPersona(string departamento, string dependencia)
        {

            var empleado = db.VISTA_EMPLEADOS.Where(x => x.departamentoid == departamento && x.dependenciaid == dependencia).OrderBy(x => x.nombre).Select(x => new Models.PersonalPgrModel
            {
                EmpleadoId = x.empleadoid,
                Descripcion = x.nombre,
                Cedula = x.cedula,

            }).ToList();
            return Json(empleado);
        }




        //Seleccionar Departamento
        [HttpPost]
        public JsonResult SeleccionarDepartamento(int DependenciaId)
        {
            var Departamento = db.vw_Departamentos.Where(x => x.DependenciasID == DependenciaId.ToString()).ToList();
            return Json(Departamento);
        }

                                                                                                                                                                                                                                                                             


        public ActionResult Actualizados()
        {

            ViewBag.cargos = new SelectList(db.vw_Cargos.ToList(), "CargoId", "Descripcion");
            ViewBag.dependencias = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");

            return View();
        }



        // GET: VISTA_EMPLEADOS/Details/5
        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == 0 || id == null)
                return RedirectToAction("Index");

            var vISTA_EMPLEADOS = db.VISTA_EMPLEADOS.FirstOrDefault(x => x.empleadoid == id.ToString());

            if (vISTA_EMPLEADOS == null)
                return RedirectToAction("Index");

            var FotoPerfil = db.MiPerfilAvatars.FirstOrDefault(x => x.codigoEmpleado == id.ToString())?.avatar;

            if (!string.IsNullOrEmpty(FotoPerfil))

                ViewBag.avatar = FotoPerfil;

            ViewBag.AFP = (vISTA_EMPLEADOS.AfpId != null && vISTA_EMPLEADOS.AfpId != 0) ? db.AFP.FirstOrDefault(afp => afp.AfpId == vISTA_EMPLEADOS.AfpId).Descripcion : "N/A";
            ViewBag.Provincia = (vISTA_EMPLEADOS.ProvinciaId != null && vISTA_EMPLEADOS.ProvinciaId != 0) ? db.vw_Provinces.FirstOrDefault(p => p.ProvinciaId == vISTA_EMPLEADOS.ProvinciaId).ProvinciaDescripcion : "N/A";
            ViewBag.Municipio = (vISTA_EMPLEADOS.MunicipioId != null && vISTA_EMPLEADOS.MunicipioId != 0) ? db.vw_Municipios.FirstOrDefault(p => p.MunicipioId == vISTA_EMPLEADOS.MunicipioId).MunicipioDescripcion : "N/A";
            ViewBag.Ciudad = (vISTA_EMPLEADOS.CiudadId != null && vISTA_EMPLEADOS.CiudadId != 0) ? db.vw_Ciudad.FirstOrDefault(p => p.CiudadId == vISTA_EMPLEADOS.CiudadId).CiudadDescripcion : "N/A";
            ViewBag.Sectores = (vISTA_EMPLEADOS.SectorId != null && vISTA_EMPLEADOS.SectorId != 0) ? db.vw_Sectores.FirstOrDefault(p => p.SectorId == vISTA_EMPLEADOS.SectorId).SectorDescripcion : "N/A";
            ViewBag.Distrito = (vISTA_EMPLEADOS.DistritoId != null && vISTA_EMPLEADOS.DistritoId != 0) ? db.vw_Distritos.FirstOrDefault(p => p.DistritoId == vISTA_EMPLEADOS.DistritoId).DistritoDescripcion : "N/A";

            ViewBag.Edad = Code.Utilities2.CalcularEdad(vISTA_EMPLEADOS.Cumpleano);

            ViewBag.ARS = (vISTA_EMPLEADOS.ArsId != null && vISTA_EMPLEADOS.ArsId != 0) ? db.ARS.FirstOrDefault(ars => ars.ArsId == vISTA_EMPLEADOS.ArsId).Descripcion : "N/A";

            return View(vISTA_EMPLEADOS);

            // return View(vISTA_EMPLEADOS + "/"+ id );
        }

        //ACTUALIZAR DATOS DE EMPLEADOS
        [HttpPost]
        [CustomAuthorize]
        public JsonResult Details(NMEMPLEADOS empleo)
        {
            if (empleo == null)
            {
                return Json(new { success = false, Mensaje = "Su codigo de empleado es invalido" }, JsonRequestBehavior.AllowGet);
            }

            var datos = empleado.NMEMPLEADOS.Where(x => x.EmpleadoID == empleo.EmpleadoID).FirstOrDefault();
            datos.Direccion = empleo.Direccion;
            datos.Telefono1 = empleo.Telefono1;
            datos.Celular = empleo.Celular;
            datos.Sexo = empleo.Sexo;
            datos.Sector = empleo.Sector;
            empleado.SaveChanges();

            return Json(new { success = true, Mensaje = "Datos Actualizados Correctamente" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ReporteActualizados()
        {

            ViewBag.cargos = new SelectList(db.vw_Cargos.ToList(), "CargoId", "Descripcion");
            ViewBag.dependencias = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");

            return View();
        }

        //Reporte Empleados Actualizados
        [HttpPost]
        public ActionResult ReporteEmpleadosActualizados(FormCollection fc)
        {
            string filtro = fc["filtros"];
            string departamento = fc["DeptoID"];
            string dependencia = fc["dependencias"];
            string formato = fc["formato"];
            bool actualizados = true;
           // bool dependientes = true;
            var vISTA_EMPLEADOS = db.VISTA_EMPLEADOS.FirstOrDefault();

            var empleados = (from x in db.VISTA_EMPLEADOS
                             
                             select new vmEmpleadosActualizados
                             {
                                 Nombre = x.nombre.ToUpper(),
                                 Codigo = x.empleadoid,
                                 Cedula = x.cedula,
                                 DependenciaId = x.dependenciaid,
                                 DepartamentoId = x.departamentoid,
                                 Dependencia = x.dependencia.ToUpper(),
                                 Departamento = x.departamento.ToUpper(),
                                 Cargo = x.cargo.ToUpper(),
                                 Celular = x.Celular,
                                 Telefono = x.telefono1,
                                 FechaIngreso = x.fechaingreso.ToString(),
                                 FechaNacimiento = x.Cumpleano.ToString(),
                                 Sexo = x.Sexo,
                                 Email = x.Email,
                                 TipoSangre = x.TiposSangre,
                                 EstadoCivil = x.EstadoCivil,
                                 Provincia = x.ProvinciaId != null  ? x.vw_Provinces.ProvinciaDescripcion : "SIN DATOS",
                                 Municipio = x.MunicipioId != null ? x.vw_Municipios.MunicipioDescripcion : "SIN DATOS",
                                 // Ciudad = x.DistritoId != null  ? dis.DistritoDescripcion : "NO TIENE",
                                 Localidad = x.CiudadId != null ? x.vw_Ciudad.CiudadDescripcion : "SIN DATOS",
                                 Sector = x.SectorId != null  ? x.vw_Sectores.SectorDescripcion : "SIN DATOS",
                               //  Profesion = x.Profesion != null ? x.Profesiones.Descripcion : "SIN DATOS",
                                 NivelAcademico = x.NivelAcademicoId != null ? x.Nivel_Academico.Descripcion : "SIN DATOS",
                                 Direccion = x.Direccion,
                                 AFP = (!string.IsNullOrEmpty(x.AfpId.ToString())) ? x.AFP.Descripcion : "SIN DATOS",
                                 Ars = (!string.IsNullOrEmpty(x.ArsId.ToString())) ? x.ARS.Descripcion : "SIN DATOS",
                                 Facebook = x.Facebook,
                                 Instagram = x.Instagram,
                                 Twitter = x.Twitter,
                                 Observaciones = x.Observaciones,
                                 CantidadDependientes = x.Dependiente.ToString()

                             }).ToList();


            if (filtro == "1" && string.IsNullOrEmpty(departamento))
            {
                empleados = empleados.Where(x => x.DependenciaId == dependencia).ToList();
              
            }
            else if (!string.IsNullOrEmpty(dependencia) && !string.IsNullOrEmpty(departamento))
            {

                empleados = empleados.Where(x => x.DependenciaId == dependencia && x.DepartamentoId == departamento).ToList();
            }
            else if (string.IsNullOrEmpty(dependencia) && string.IsNullOrEmpty(departamento))
            {
                empleados.OrderBy(x => x.Dependencia).ToList();
            }

            else
            {
                empleados = empleados.OrderBy(x => x.Departamento).ToList();
            }


            if (actualizados)
            {
                var empleadosActualizados = db.EmpleadoComplemento.Select(x => x.EmpleadoId.ToString()).ToArray();

                empleados = empleados.Where(x => empleadosActualizados.Contains(x.Codigo)).OrderBy(x => x.Departamento).ToList();
                ViewBag.CantidadActualizados = empleados.Count();
            }

            if (filtro == "4")
            {
             
                    var empleadosDependiente = db.ActualizacionDependientes.Select(x => x.EmpleadoId.ToString()).ToArray();

                   empleados = empleados.Where(x => empleadosDependiente.Contains(x.Codigo)).OrderBy(x => x.Departamento)
                    .ToList();

                ViewBag.CantidadActualizados = empleados.Count();
            }


            if (formato == "1")
            {
                return new ViewAsPdf("ReporteEmpleadosActualizados", empleados.OrderBy(x => x.Nombre))
                {

                    PageSize = Rotativa.Options.Size.Letter,
                    PageOrientation = Rotativa.Options.Orientation.Landscape,
                };
            }
            else
            {
                var grid = new GridView();
                grid.DataSource = empleados;
                grid.DataBind();

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachement; filename=EmpleadosActualizados.xls");
                Response.ContentType = "application/excel";
                StringWriter sw = new StringWriter();
                System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                grid.RenderControl(htw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();

                return View(empleados);

            }



            //return Json(empleados);
        }

        public ActionResult ReporteEmpleadosActualizados2()
        {
            bool actualizados = true;
            var empleados = db.VISTA_EMPLEADOS.Where(x => x.dependenciaid == "10003" && x.departamentoid == "1")
                //.Where(x => (x.dependenciaid == dependencia || dependencia == "")
                //&& (x.departamentoid == departamento || departamento == "")
                //&& (x.cargoid == cargo || cargo == ""))
                .Select(x => new vmEmpleadosActualizados
                {
                    Nombre = x.nombre.ToUpper(),
                    Codigo = x.empleadoid,
                    Cedula = x.cedula,
                    Dependencia = x.dependencia.ToUpper(),
                    Departamento = x.departamento.ToUpper(),
                    Cargo = x.cargo.ToUpper()
                })
                .ToList();

            if (actualizados)
            {
                var empleadosActualizados = db.EmpleadoComplemento.Select(x => x.EmpleadoId.ToString()).ToArray();

                empleados = empleados.Where(x => empleadosActualizados.Contains(x.Codigo)).ToList();
            }


            return View("ReporteEmpleadosActualizados", empleados);
        }

        public JsonResult FiltrarEmpleados(bool actualizados, string dependencia, string departamento, string cargo)
        {

            var empleados = db.VISTA_EMPLEADOS
                .Where(x => (x.dependenciaid == dependencia || dependencia == "")
                && (x.departamentoid == departamento || departamento == "")
                && (x.cargoid == cargo || cargo == ""))
                .Select(x => new { Nombre = x.nombre, Codigo = x.empleadoid, Cedula = x.cedula, Dependencia = x.dependencia, Departamento = x.departamento, Cargo = x.cargo })
                .ToList();

            if (actualizados)
            {
                var empleadosActualizados = db.EmpleadoComplemento.Select(x => x.EmpleadoId.ToString()).ToArray();

                empleados = empleados.Where(x => empleadosActualizados.Contains(x.Codigo)).ToList();
            }

            return Json(empleados);
        }

        public JsonResult GetDepartamentos(string dependencia)
        {
            var departamentos = db.vw_Departamentos.Where(x => x.DependenciasID == dependencia)
                .GroupBy(x => x.Descripcion).Select(x => x.FirstOrDefault()).ToList();

            foreach (var item in departamentos)
            {
                if (item.Descripcion.StartsWith(" ")) item.Descripcion = item.Descripcion.Substring(1);
            }

            var departamentos2 = departamentos.OrderBy(x => x.Descripcion)
                .Select(x => new
                {
                    Value = x.DeptoID,
                    Text = x.Descripcion
                }).OrderBy(x => x.Text);

            return Json(departamentos2);
        }

        [AllowAnonymous]
        public JsonResult GetCargos(int dependencia, int departamento, bool todos)
        {
            var CargoId = db.vw_CargosDepto.Where(x => x.DeptoID == departamento
            && x.DependenciaID == dependencia
            && ((!x.Cargos.Contains("ENC.") && !x.Cargos.Contains("ENCARGADO") && !x.Cargos.StartsWith("PROCURADOR GENERAL") && !x.Cargos.Contains("PROCURADOR ADJUNTO")
            && !x.Cargos.Contains("DIRECTOR") && !x.Cargos.Contains("SUPERVISOR") && !x.Cargos.Contains("PROCURADOR FISCAL") && !x.Cargos.Contains("FISCALIZADOR")
            ) || todos == true)
            ).Select(x => new
            {
                Value = x.CargoId,
                Text = x.Cargos
            }).OrderBy(x => x.Text).Distinct().ToList();

            return Json(CargoId);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
