﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models.GestionHumana;
using System.Data.Entity;

namespace intra.Controllers
{
    public class MensajePonchesController : Controller
    {
        private DFMPModel db = new DFMPModel();

        [CustomAuthorize]
        // GET: MensajePonches
        public ActionResult Index()
        {
            var ponches_messages = db.MensajePonches.ToList();
            return View(ponches_messages);
        }

        [CustomAuthorize]
        // GET: MensajePonches/Edit/5
        public ActionResult Edit(int id)
        {
            MensajePonches ponches_messages = db.MensajePonches.Find(id);
            return View(ponches_messages);
        }

        // POST: MensajePonches/Edit/5
        [HttpPost]
        public ActionResult Edit(MensajePonches view_object)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(view_object);

                var result = (from t in db.MensajePonches
                              where t.MensajePonchesId == view_object.MensajePonchesId
                              select t);

                foreach (var a in result)
                {
                    a.MensajePonchesDescripcion = view_object.MensajePonchesDescripcion;
                    break;
                }
                
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch(Exception error)
            {
                return View(view_object);
            }
        }

        [CustomAuthorize]
        // GET: MensajePonches/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        [CustomAuthorize]
        // GET: MensajePonches/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MensajePonches/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [CustomAuthorize]
        // GET: MensajePonches/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: MensajePonches/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
