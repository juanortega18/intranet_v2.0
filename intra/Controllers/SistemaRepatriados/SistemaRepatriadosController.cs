﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Models.SistemaRepatriados;
using intra;
using intra.Code;
using System.Threading;
using intra.Models.SistemaRepatriados.ViewModel;

namespace intra.Controllers.SistemaRepatriados
{
    public class SistemaRepatriadosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        public PartialViewResult RepatriadosList()
        {
            //if (HttpContext.Session["empleadoId"] != null)
            //{


                string codigo = HttpContext.Session["empleadoId"].ToString();
                var CantidadSolicitados = db.SSR_Servicios.Where(x => x.CodigoCreador == codigo && x.ServicioEstadoId == 1).ToList().Count();
                var cantidadAsignados = db.SSR_Servicios.Where(x => x.CodigoResponsable == codigo && x.ServicioEstadoId == 1).ToList().Count();
                Session["CantidadServicioSolicitadosRepatriados"] = CantidadSolicitados;
                Session["CantidadServicioAsignadosRepatriados"] = cantidadAsignados;

           // }

                return PartialView();
        }

        public ActionResult Index()
        {
            string codigo = HttpContext.Session["empleadoId"].ToString();
            var CantidadSolicitados = db.SSR_Servicios.Where(x => x.CodigoCreador == codigo && x.ServicioEstadoId == 1).ToList().Count();
            var cantidadAsignados = db.SSR_Servicios.Where(x => x.CodigoResponsable == codigo && x.ServicioEstadoId == 1).ToList().Count();
            Session["CantidadServicioSolicitadosRepatriados"] = CantidadSolicitados;
            Session["CantidadServicioAsignadosRepatriados"] = cantidadAsignados;
            return View();
        }

        public ActionResult ListadoRepatriados()
        {
            string codigo = HttpContext.Session["empleadoId"].ToString();
            var CantidadSolicitados = db.SSR_Servicios.Where(x => x.CodigoCreador == codigo && x.ServicioEstadoId == 1).ToList().Count();
            var cantidadAsignados = db.SSR_Servicios.Where(x => x.CodigoResponsable == codigo && x.ServicioEstadoId == 1).ToList().Count();
            Session["CantidadServicioSolicitadosRepatriados"] = CantidadSolicitados;
            Session["CantidadServicioAsignadosRepatriados"] = cantidadAsignados;
            ViewBag.PaisId = new SelectList(db.Pais.ToList(), "Paisid", "PaisDescripcion");
            ViewBag.TelefonoTipoId = new SelectList(db.SSR_TelefonosTipos.ToList(), "TelefonoTipoId", "Descripcion");
            var repatriados = db.SSR_Repatriados.ToList();
            ViewBag.ParentescoId = new SelectList(db.Parentesco, "ParentescoId", "Descripcion");
            ViewBag.ProvinciaId = new SelectList(db.vw_Provinces.ToList(), "ProvinciaId", "ProvinciaDescripcion");
            ViewBag.Provincias = ViewBag.ProvinciaId;
            ViewBag.MunicipioId = new SelectList(db.vw_Municipios.ToList(), "MunicipioId", "MunicipioDescripcion");
            ViewBag.Municipios = ViewBag.MunicipioId;
            ViewBag.DistritoId = new SelectList(db.vw_Distritos.ToList(), "DistritoId", "DistritoDescripcion");
            ViewBag.Distritos = ViewBag.DistritoId;
            ViewBag.CiudadId = new SelectList(db.vw_Ciudad.ToList(), "CiudadId", "CiudadDescripcion");
            ViewBag.Ciudades = ViewBag.CiudadId;
            ViewBag.SectorId = new SelectList(db.vw_Sectores.ToList(), "SectorId", "SectorDescripcion");
            ViewBag.Sectores = ViewBag.SectorId;
            ViewBag.NacionalidadId = new SelectList(db.Vw_Mostrar_Nacionalidades, "ID_Nacionalidad", "Descripcion");


            return View(repatriados);
        }

        

        [HttpPost]
        public JsonResult ListaContactoRepatriados(int? id)
        {
            var contacto = (from Tel in db.SSR_Telefonos
                            join tr in db.SSR_TelefonosRepatriados
                            on Tel.TelefonoId equals tr.TelefonoId
                            join re in db.SSR_Repatriados on tr.RepatriadoId
                            equals re.RepatriadoId
                            join tt in db.SSR_TelefonosTipos
                             on Tel.TelefonoTipoId equals tt.TelefonoTipoId
                            where re.RepatriadoId == id
                            select new TelefonoRepatriados
                            {
                               RepatriadoId = re.RepatriadoId,
                                tipoTelefono = tt.Descripcion,
                                NumeroTelefono = Tel.Numero
                            }).ToList().Take(200);
        


            return Json(contacto);
        }


        [HttpPost]
        public JsonResult GetPersona(string cedula, int tipoIdentificacion)
        {
            var person = Utilities2.ObtenerPersonaSic(cedula.Replace("-", ""), tipoIdentificacion);

            return Json(person, JsonRequestBehavior.AllowGet);
        }

        // GET: SistemaRepatriados/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SSR_Repatriados sSR_Repatriados = db.SSR_Repatriados.Find(id);
            if (sSR_Repatriados == null)
            {
                return HttpNotFound();
            }
            return View(sSR_Repatriados);
        }



        // GET: SistemaRepatriados/Create
        public ActionResult CrearRepatriados()
        {

            var repatriado = db.SSR_Repatriados.OrderByDescending(x => x.RepatriadoId).FirstOrDefault();
            if(repatriado!= null)
            { 
            ViewBag.RepatriadoId = repatriado.RepatriadoId;
            }
            ViewBag.ProvinciaId = new SelectList(db.vw_Provinces.ToList(), "ProvinciaId", "ProvinciaDescripcion");
            ViewBag.Provincias = ViewBag.ProvinciaId;
            ViewBag.MunicipioId = new SelectList(db.vw_Municipios.ToList(), "MunicipioId", "MunicipioDescripcion");
            ViewBag.Municipios = ViewBag.MunicipioId;
            ViewBag.DistritoId = new SelectList(db.vw_Distritos.ToList(), "DistritoId", "DistritoDescripcion");
            ViewBag.Distritos = ViewBag.DistritoId;
            ViewBag.CiudadId = new SelectList(db.vw_Ciudad.ToList(), "CiudadId", "CiudadDescripcion");
            ViewBag.Ciudades = ViewBag.CiudadId;
            ViewBag.SectorId = new SelectList(db.vw_Sectores.ToList(), "SectorId", "SectorDescripcion");
            ViewBag.Sectores = ViewBag.SectorId;
            ViewBag.TelefonoTipoId = new SelectList(db.SSR_TelefonosTipos, "TelefonoTipoId", "Descripcion");
            //ViewBag.TiposTelefono = new SelectList(db.SSR_TelefonosTipos, "TelefonoTipoId", "Descripcion");

            ViewBag.PaisId = new SelectList(db.Pais.ToList(), "Paisid", "PaisDescripcion");
          

            return View();
        }

        // POST: SistemaRepatriados/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult CrearRepatriados(FormCollection fc, SSR_Repatriados Repatriado)
        {
            string Cedula = fc["NumeroIdentificacion"];
            string Retorno = fc["FechaRetorno"];
            string Nacimiento = fc["FechaNacimiento"];
            string numeroTelefono = fc["NumeroTelefono"];

            if(!string.IsNullOrEmpty(numeroTelefono))
            { 
           
                var Telefono = db.SSR_Telefonos.Where(x => x.Numero == numeroTelefono).FirstOrDefault();
                if (Telefono != null)
                {
                    return Json(new { success = false, Mensaje = "Este numero de telefono ya existe" }, JsonRequestBehavior.AllowGet);

                }
            }


            if (string.IsNullOrEmpty(fc["Nombres1"]))
            {
                return Json(new { success = false, Mensaje = "El Campo Nombre es Obligatorio"}, JsonRequestBehavior.AllowGet);
            }
            if(string.IsNullOrEmpty(fc["Apellidos1"]))
            {
                return Json(new { success = false, Mensaje = "El Campo Primer Apellido es Obligatorio" }, JsonRequestBehavior.AllowGet);
            }
           
            try {
                DateTime FechaRetorno = DateTime.ParseExact(Retorno, "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture);
                DateTime FechaNacimiento = DateTime.ParseExact(Nacimiento, "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture);

                if (!string.IsNullOrEmpty(Cedula)) { 

                var ConsultaRepatriado = db.SSR_Repatriados.Where(x => x.NumeroIdentificacion == Cedula).FirstOrDefault();
                    if (ConsultaRepatriado != null)
                    {
                        return Json(new { success = false, Mensaje = "Esta cedula ya fue digitada" }, JsonRequestBehavior.AllowGet);
                    }
                }

                if (fc["Nombres"] == "")
                { Repatriado.Nombres = fc["Nombres1"];}else
                { Repatriado.Nombres = fc["Nombres"]; }
                    if(fc["Apellido1"] == "")
                { Repatriado.Apellido1 = fc["Apellidos1"]; }else
                { Repatriado.Apellido1 = fc["Apellido1"]; }
                    if(fc["Apellido2"] == "")
                { Repatriado.Apellido2 = fc["Apellidos2"]; }
                else { Repatriado.Apellido2 = fc["Apellido2"]; }

                    Repatriado.TipoIdentificacionId = 1;
                    Repatriado.NumeroIdentificacion = fc["NumeroIdentificacion"];
                    Repatriado.NumeroIdentificador = fc["NumeroIdentificador"];
                    Repatriado.FechaRetorno = FechaRetorno;
                   
                    Repatriado.FechaNacimiento = FechaNacimiento;
                    Repatriado.Sexo = bool.Parse(fc["Sexo"]);
                    Repatriado.UsuarioCreador = Session["usuario"].ToString();
                    Repatriado.FechaCreacion = DateTime.Now;
                    Repatriado.PaisId = int.Parse(fc["PaisId"]);
                    Repatriado.Estado = true;
                    Repatriado.MotivoRepatriacion = fc["MotivoRepatriacion"];
                    db.SSR_Repatriados.Add(Repatriado);
                    db.SaveChanges();

                
                var Repatriados = db.SSR_Repatriados.OrderByDescending(x => x.RepatriadoId).FirstOrDefault();

                // Direccion de Repatriados
                SSR_Direcciones direccion = new SSR_Direcciones();
                direccion.ProvinciaId = int.Parse(fc["ProvinciaId"]);
                direccion.MunicipioId = int.Parse(fc["MunicipioId"]);
                direccion.DistritoId = int.Parse(fc["DistritoId"]);
                direccion.CiudadId = int.Parse(fc["CiudadId"]);
                direccion.SectorId = int.Parse(fc["SectorId"]);
                direccion.Calle = fc["Calle"];
                direccion.Numero = fc["Numero"];
                direccion.FechaCreacion = DateTime.Now;
                db.SSR_Direcciones.Add(direccion);
                db.SaveChanges();
                //Direccion Repatriados
                var direccionRepatriado = db.SSR_Direcciones.OrderByDescending(x => x.DireccionId).FirstOrDefault();
                SSR_DireccionesRepatriados DirRepatriados = new SSR_DireccionesRepatriados();
                DirRepatriados.FechaCreacion = DateTime.Now;
                DirRepatriados.RepatriadoId = Repatriados.RepatriadoId;  //int.Parse(fc["RepatriadoId"]); // repatriados.RepatriadoId;
                DirRepatriados.DireccionId = direccionRepatriado.DireccionId;
                DirRepatriados.Estado = true;
                db.SSR_DireccionesRepatriados.Add(DirRepatriados);
                db.SaveChanges();

                if(!string.IsNullOrEmpty(numeroTelefono))
                {
                //Telefono Repatriados
                SSR_Telefonos Telefonos = new SSR_Telefonos();
                Telefonos.Numero = fc["NumeroTelefono"];
                Telefonos.TelefonoTipoId = int.Parse(fc["TelefonoTipoId"]);
                db.SSR_Telefonos.Add(Telefonos);
                db.SaveChanges();
                var Telefono1 = db.SSR_Telefonos.OrderByDescending(x => x.TelefonoId).FirstOrDefault();
                //Telefono Repatriados
                SSR_TelefonosRepatriados TelRepatriados = new SSR_TelefonosRepatriados();
                TelRepatriados.RepatriadoId = Repatriados.RepatriadoId;
                TelRepatriados.TelefonoId = Telefono1.TelefonoId;
                TelRepatriados.FechaCreacion = DateTime.Now;
                TelRepatriados.Estado = true;
                db.SSR_TelefonosRepatriados.Add(TelRepatriados);
                db.SaveChanges();
                }

               return Json(new { success = true, Mensaje = "Datos Guardado Correctamente" }, JsonRequestBehavior.AllowGet);

 
            }catch(Exception e)
            {
                e.ToString();
                return Json(new { success = false, Mensaje = "Ha ocurrido un error, Favor Verificar" }, JsonRequestBehavior.AllowGet);

            }

           // return Json(new { success = false, Mensaje= "Ha ocurrido un error, Favor Verificar" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListaFamiliaresRepatriados(int? id)
        {
            var familiares = (from r in db.SSR_Repatriados
                              join fr in db.SSR_Familiares
                              on r.RepatriadoId equals fr.RepatriadoId
                              join p in db.Parentesco
                              on fr.ParentescoId equals p.ParentescoId
                              join nacionalidad in db.Vw_Mostrar_Nacionalidades on fr.NacionalidadId equals nacionalidad.ID_Nacionalidad
                              join dirfam in db.SSR_DireccionesFamiliares
                              on fr.FamiliarId equals dirfam.FamiliarId
                              join dir in db.SSR_Direcciones on dirfam.DireccionId equals dir.DireccionId
                              join provin in db.vw_Provinces on dir.ProvinciaId equals provin.ProvinciaId
                              join mun in db.vw_Municipios on dir.MunicipioId equals mun.MunicipioId
                              join ciudad in db.vw_Ciudad on dir.CiudadId equals ciudad.CiudadId
                              join sector in db.vw_Sectores on dir.SectorId equals sector.SectorId
                              join distrito in db.vw_Distritos on dir.DistritoId equals distrito.DistritoId
                              //join tr in db.SSR_TelefonosFamiliares on fr.FamiliarId equals tr.FamiliarId
                              //join tel in db.SSR_Telefonos on tr.TelefonoId equals tel.TelefonoId
                              //join tipoTel in db.SSR_TelefonosTipos on tel.TelefonoTipoId equals tipoTel.TelefonoTipoId


                              select new FamiliaresRepatriadosVM
                              {
                                  Repatriado = fr.RepatriadoId,
                                  Parentesco = p.Descripcion,
                                  NombreCompleto = fr.Nombres + " " + fr.Apellido1 + " " + fr.Apellido2,
                                  Nacionalidad = nacionalidad.Descripcion,
                                //  TipoTelefono = tel.SSR_TelefonosTipos.Descripcion,
                                  //Telefono = tel.Numero,
                                  Provincia = provin.ProvinciaDescripcion,
                                  Municipio = mun.MunicipioDescripcion,
                                  Distrito = distrito.DistritoDescripcion,
                                  Ciudad = ciudad.CiudadDescripcion,
                                  Sectores = sector.SectorDescripcion,
                                  Calle = dir.Calle,
                                  NumeroCasa = dir.Numero

                              }).Where(x=> x.Repatriado == id).ToList();

            return Json(familiares);
        }

        [HttpGet]
        public ActionResult FamiliaresRepatriados()
        {
            var repatriado = db.SSR_Repatriados.OrderByDescending(x => x.RepatriadoId).FirstOrDefault();
            ViewBag.Repatriado = repatriado.RepatriadoId;
            ViewBag.TelefonoTipoId = new SelectList(db.SSR_TelefonosTipos, "TelefonoTipoId", "Descripcion");
            ViewBag.Provincias = new SelectList(db.vw_Provinces.ToList(), "ProvinciaId", "ProvinciaDescripcion");
            ViewBag.Municipios = new SelectList(db.vw_Municipios.ToList(), "MunicipioId", "MunicipioDescripcion");
            ViewBag.Distritos = new SelectList(db.vw_Distritos.ToList(), "DistritoId", "DistritoDescripcion");
            ViewBag.Ciudades = new SelectList(db.vw_Ciudad.ToList(), "CiudadId", "CiudadDescripcion");
            ViewBag.Sectores = new SelectList(db.vw_Sectores.ToList(), "SectorId", "SectorDescripcion");
            ViewBag.ParentescoId = new SelectList(db.Parentesco, "ParentescoId", "Descripcion");
            ViewBag.RepatriadoId = new SelectList(db.SSR_Repatriados, "RepatriadoId", "Nombres");
            ViewBag.NacionalidadId = new SelectList(db.Vw_Mostrar_Nacionalidades, "ID_Nacionalidad", "Descripcion");
            return View();
        }

        [HttpPost]
        public JsonResult CrearFamiliarRepatriados(SSR_Familiares familiar, FormCollection fc)
        {
            int RepatriadoId = int.Parse(fc["RepatriadoFamiliarId"]);

            if(fc["TelefonoTipoId"] != "")
            { 

            int TelefonoTipo = int.Parse(fc["TelefonoTipoId"]);
            }

            string parentesco = fc["ParentescoId"].ToString();


            try { 
            if(RepatriadoId != 0)
            {
                if(fc["Nombres1Familiar"] == "")
                    { familiar.Nombres = fc["NombreFamiliar"];}
                    else {familiar.Nombres = fc["Nombres1Familiar"]; }
                 if(fc["PrimerApellido"] == "") { familiar.Apellido1 = fc["Apellido1Familiar"]; }
                    else {familiar.Apellido1 = fc["PrimerApellido"]; }
                    if (fc["SegundoApellido"] == "") { familiar.Apellido2 = fc["Apellido2Familiar"]; }
                    else { familiar.Apellido2 = fc["SegundoApellido"]; }

                    if (string.IsNullOrEmpty(parentesco))
                    {
                        return Json(new { success = false, Mensaje = "Debe indicar el parentesco de esta persona" }, JsonRequestBehavior.AllowGet);
                    }

                    if (string.IsNullOrEmpty(familiar.Nombres))
                    {
                        return Json(new { success = false, Mensaje = "El Campo Nombre es Obligatorio" }, JsonRequestBehavior.AllowGet);
                    }
                    if (string.IsNullOrEmpty(familiar.Apellido1))
                    {
                        return Json(new { success = false, Mensaje = "El Campo Primer Apellido es Obligatorio" }, JsonRequestBehavior.AllowGet);
                    }

                    // familiar.Apellido2 = fc["SegundoApellido"];
                    familiar.FechaVinculacion = DateTime.Now;
                familiar.NacionalidadId = int.Parse(fc["NacionalidadId"]);
                familiar.Notas = "";
                familiar.ParentescoId = int.Parse(fc["ParentescoId"]);
                familiar.RepatriadoId = RepatriadoId;
                familiar.NumeroIdentificacion = fc["Cedula"];
                familiar.UsuarioCreador = Session["usuario"].ToString();
                familiar.Vive = bool.Parse(fc["Vive"]);

                db.SSR_Familiares.Add(familiar);

                db.SaveChanges();

                var familiares = db.SSR_Familiares.OrderByDescending(x => x.FamiliarId).FirstOrDefault();

                //Direccion de Repatriados
                SSR_Direcciones direccion = new SSR_Direcciones();
                direccion.ProvinciaId = int.Parse(fc["Provincias"]);
                direccion.MunicipioId = int.Parse(fc["Municipios"]);
                direccion.DistritoId = int.Parse(fc["Distritos"]);
                direccion.CiudadId = int.Parse(fc["Ciudades"]);
                direccion.SectorId = int.Parse(fc["Sectores"]);
                direccion.Calle = fc["Calle"];
                direccion.Numero = fc["Numero"];
                direccion.FechaCreacion = DateTime.Now;
                db.SSR_Direcciones.Add(direccion);
                db.SaveChanges();
                //Direccion Repatriados
                var direccionRepatriado = db.SSR_Direcciones.OrderByDescending(x => x.DireccionId).FirstOrDefault();
                SSR_DireccionesFamiliares DirFamiliares = new SSR_DireccionesFamiliares();
                DirFamiliares.FechaCreacion = DateTime.Now;
                DirFamiliares.FamiliarId = familiares.FamiliarId;
                DirFamiliares.DireccionId = direccionRepatriado.DireccionId;
                DirFamiliares.Estado = true;
                db.SSR_DireccionesFamiliares.Add(DirFamiliares);
                db.SaveChanges();
                    if(fc["NumeroTelefono1"] != "")
                    { 
                //Telefono Repatriados
                SSR_Telefonos Telefonos = new SSR_Telefonos();
                Telefonos.Numero = fc["NumeroTelefono1"];
                    Telefonos.TelefonoTipoId = int.Parse(fc["TelefonoTipoId"]); 
                db.SSR_Telefonos.Add(Telefonos);
                db.SaveChanges();
                var Telefono1 = db.SSR_Telefonos.OrderByDescending(x => x.TelefonoId).FirstOrDefault();
                
                    //Telefonos Familiares
                    SSR_TelefonosFamiliares TelFamiliares = new SSR_TelefonosFamiliares();
                    TelFamiliares.TelefonoId = Telefono1.TelefonoId;
                    TelFamiliares.FamiliarId = familiares.FamiliarId;
                    TelFamiliares.FechaCreacion = DateTime.Now;
                    TelFamiliares.Estado = true;
                db.SSR_TelefonosFamiliares.Add(TelFamiliares);
                db.SaveChanges();
                    }

                }
                return Json(new { success = true, Mensaje = "Datos Guardado Correctamente" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e) {

                e.ToString();
            return Json(new { success = false, Mensaje = "Ha ocurrido un error, Favor Verificar" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult ContactosRepatriados()
        {

            ViewBag.TelefonoTipoId = new SelectList(db.SSR_TelefonosTipos, "TelefonoTipoId", "Descripcion");
            return View();
        }

        [HttpPost]
        public JsonResult CrearContactosRepatriados(SSR_Telefonos telefonos, FormCollection fc)
        {
     
            string numeroTelefono = fc["NumeroTelefono"];
            string RepatriadoId = fc["RepatriadoContacto"];

            if (numeroTelefono != "")
            {
                var Telefono = db.SSR_Telefonos.Where(x => x.Numero == numeroTelefono).FirstOrDefault();
                if (Telefono != null)
                {
                    return Json(new { success = false, Mensaje = "Este numero de telefono ya existe" }, JsonRequestBehavior.AllowGet);

                }
                    //Telefono Repatriados
                    SSR_Telefonos Telefonos = new SSR_Telefonos();
                    Telefonos.Numero = fc["NumeroTelefono"];
                    Telefonos.TelefonoTipoId = int.Parse(fc["TelefonoTipoId"]);
                    db.SSR_Telefonos.Add(Telefonos);
                    db.SaveChanges();
                    var Telefono1 = db.SSR_Telefonos.OrderByDescending(x => x.TelefonoId).FirstOrDefault();
                
                   //Telefono Repatriados
                    SSR_TelefonosRepatriados TelRepatriados = new SSR_TelefonosRepatriados();
                    TelRepatriados.RepatriadoId = int.Parse(RepatriadoId);
                    TelRepatriados.TelefonoId = Telefono1.TelefonoId;
                    TelRepatriados.FechaCreacion = DateTime.Now;
                    TelRepatriados.Estado = true;
                    db.SSR_TelefonosRepatriados.Add(TelRepatriados);
                    db.SaveChanges();

                    return Json(new { success = true, Mensaje = "Datos Guardado Correctamente" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, Mensaje = "Ha ocurrido un error, Favor Verificar" }, JsonRequestBehavior.AllowGet);

        }

        // GET: SistemaRepatriados/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SSR_Repatriados sSR_Repatriados = db.SSR_Repatriados.Find(id);
            if (sSR_Repatriados == null)
            {
                return HttpNotFound();
            }
            return View(sSR_Repatriados);
        }

        // POST: SistemaRepatriados/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RepatriadoId,NumeroIdentificador,FechaRetorno,Nombres,Apellido1,Apellido2,TipoIdentificacionId,NumeroIdentificacion,FechaNacimiento,Sexo,PaisId,Conclusion,Estado,UsuarioCreador,FechaCreacion")] SSR_Repatriados sSR_Repatriados)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sSR_Repatriados).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sSR_Repatriados);
        }

        [HttpPost]
        public JsonResult SeleccionMunicipio(int provinciaId)
        {
            var Municipios = db.vw_Municipios.Where(x => x.ProvinciaId == provinciaId).ToList();
            return Json(Municipios);
        }
        //Seleccionar distrito
        [HttpPost]
        public JsonResult SeleccionarDistricto(int MunicipioId)
        {
            var Distrito = db.vw_Distritos.Where(x => x.MunicipioId == MunicipioId).ToList();
            return Json(Distrito);
        }

        //Seleccionar Ciudad
        [HttpPost]
        public JsonResult SeleccionarCiudad(int DistritoId)
        {
            var ciudad = db.vw_Ciudad.Where(x => x.DistritoId == DistritoId).ToList();
            return Json(ciudad);
        }
        //Sector
        [HttpGet]
        public JsonResult SeleccionarSector(int CiudaId)
        {
            var Sectores = db.vw_Sectores.Where(x => x.CiudadId == CiudaId).ToList();
            return Json(Sectores, JsonRequestBehavior.AllowGet);
        }



        // GET: SistemaRepatriados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SSR_Repatriados sSR_Repatriados = db.SSR_Repatriados.Find(id);
            if (sSR_Repatriados == null)
            {
                return HttpNotFound();
            }
            return View(sSR_Repatriados);
        }

        // POST: SistemaRepatriados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SSR_Repatriados sSR_Repatriados = db.SSR_Repatriados.Find(id);
            db.SSR_Repatriados.Remove(sSR_Repatriados);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
