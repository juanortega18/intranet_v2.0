﻿using intra.Models.GestionHumana;
using intra.Models.SistemaRepatriados;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers.SistemaRepatriados
{
    public class ServiciosRepatriadosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        public ActionResult Lista()
        {
            ViewBag.layout = "~/Views/intraMaster.cshtml";
            ViewBag.crear = "si";

            return View(db.SSR_Servicios.Where(x => x.ServicioEstadoId == 1).ToList());
        }

        public ActionResult Solicitados()
        {
            string codigo = HttpContext.Session["empleadoId"].ToString();

            ViewBag.crear = "si";
            ViewBag.layout = "~/Views/intraMaster.cshtml";

            return View("Index", db.SSR_Servicios.Where(x => x.ServicioEstadoId == 1 && x.CodigoCreador == codigo).ToList());
        }

        public ActionResult SolicitadosPartial()
        {
            string codigo = HttpContext.Session["empleadoId"].ToString();
            var CantidadSolicitados = db.SSR_Servicios.Where(x => x.CodigoCreador == codigo && x.ServicioEstadoId == 1).ToList().Count();
            var cantidadAsignados = db.SSR_Servicios.Where(x => x.CodigoResponsable == codigo && x.ServicioEstadoId == 1).ToList().Count();
            Session["CantidadServicioSolicitadosRepatriados"] = CantidadSolicitados;
            Session["CantidadServicioAsignadosRepatriados"] = cantidadAsignados;
            PartialViewBag();
            return View("Solicitados");
        }

        public ActionResult Asignados()
        {
            string codigo = HttpContext.Session["empleadoId"].ToString();

            ViewBag.crear = "si";
            ViewBag.layout = "~/Views/intraMaster.cshtml";

            return View("Index", db.SSR_Servicios.Where(x => x.ServicioEstadoId == 1 && x.CodigoResponsable == codigo).ToList());
        }

        public ActionResult AsignadosPartial()
        {
            string codigo = HttpContext.Session["empleadoId"].ToString();
            var CantidadSolicitados = db.SSR_Servicios.Where(x => x.CodigoCreador == codigo && x.ServicioEstadoId == 1).ToList().Count();
            var cantidadAsignados = db.SSR_Servicios.Where(x => x.CodigoResponsable == codigo && x.ServicioEstadoId == 1).ToList().Count();
            Session["CantidadServicioSolicitadosRepatriados"] = CantidadSolicitados;
            Session["CantidadServicioAsignadosRepatriados"] = cantidadAsignados;
            PartialViewBag();
            return View("Asignados");
        }

        public ActionResult Crear()
        {
            ViewBag.layout = "~/Views/intraMaster.cshtml";
            getSelectLists();
            return View();
        }

        public ActionResult CrearPartial()
        {
            ViewBag.ServicioTipoId = new SelectList(db.SSR_ServiciosTipos.Where(s => s.Estado == true), "ServicioTipoId", "Descripcion");
            ViewBag.IdRepatriados = new SelectList(new List<SSR_Servicios>(), "RepatriadoId", "NombreCompleto");
            ViewBag.InstitucionId = new SelectList(db.SSR_Instituciones.Where(i => i.Estado == true), "InstitucionId", "Descripcion");
            return View("Crear");
        }

        [HttpPost]
        public JsonResult CrearModal(SSR_Servicios ssr_servicios)
        {
            List<string> Errores = new List<string>();

            ssr_servicios.RepatriadoId = int.Parse(Request.Form["IdRepatriados"].ToString());
            ssr_servicios.ServicioTipoId = int.Parse(Request.Form["ServicioTipoId"].ToString());
            ssr_servicios.InstitucionId = int.Parse(Request.Form["InstitucionId"].ToString());
            ssr_servicios.Notas = Request.Form["Notas"].ToString();

            if (ssr_servicios.ServicioTipoId == null || ssr_servicios.ServicioTipoId == 0) Errores.Add("Tipo de servicio inválido");
            if (ssr_servicios.RepatriadoId == 0) Errores.Add("Repatriado inválido");
            if (ssr_servicios.InstitucionId == 0) Errores.Add("Institución inválido");
            if (string.IsNullOrWhiteSpace(ssr_servicios.Notas)) Errores.Add("Debe ingresar una nota del servicio que desea realizar");

            if (Errores.Count == 0)
            {

                ssr_servicios.FechaCreacion = DateTime.Now;
                ssr_servicios.FechaFin = DateTime.Now.AddDays(1);
                ssr_servicios.ServicioEstadoId = 1;
                ssr_servicios.UsuarioCreador = HttpContext.Session["usuario"].ToString();
                ssr_servicios.CodigoCreador = HttpContext.Session["empleadoId"].ToString();

                var resposable = db.SSR_ResponsableRepatriados.FirstOrDefault();

                ssr_servicios.UsuarioResponsable = resposable.UsuarioResponsable;
                ssr_servicios.CodigoResponsable = resposable.CodigoResponsable;

                try
                {
                    db.SSR_Servicios.Add(ssr_servicios);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    return Json("Error");
                }

                try
                {
                    EnviarCorreoServicioRepatriado(new string[] { resposable.CorreoResponsable, HttpContext.Session["correo"].ToString() }, "USTED HA RECIBIDO UN MENSAJE DE SERVICIOS REPATRIADOS",
                        Descripcion("SERVICIO REPATRIADOS", " " + HttpContext.Session["departamento"].ToString().ToUpper(), HttpContext.Session["nombre"].ToString().ToUpper(),
                        ssr_servicios.Notas, ssr_servicios.ServicioId, " nombre del servicio repatriados"));
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    return Json("ErrorDeCorreo");
                }

                return Json("Guardado");

            }

            return Json(Errores);
        }

        [HttpPost]
        public JsonResult Crear(SSR_Servicios ssr_servicios)
        {
            List<string> Errores = new List<string>();

            ssr_servicios.RepatriadoId = int.Parse(Request.Form["IdRepatriados"].ToString());

            if (ssr_servicios.ServicioTipoId == null || ssr_servicios.ServicioTipoId == 0) Errores.Add("Tipo de servicio inválido");
            if (ssr_servicios.RepatriadoId == 0) Errores.Add("Repatriado inválido");
            if (ssr_servicios.InstitucionId == 0) Errores.Add("Institución inválido");
            if (string.IsNullOrWhiteSpace(ssr_servicios.Notas)) Errores.Add("Debe ingresar una nota del servicio que desea realizar");

            if (Errores.Count == 0)
            {

                ssr_servicios.FechaCreacion = DateTime.Now;
                ssr_servicios.FechaFin = DateTime.Now.AddDays(1);
                ssr_servicios.ServicioEstadoId = 1;
                ssr_servicios.UsuarioCreador = HttpContext.Session["usuario"].ToString();
                ssr_servicios.CodigoCreador = HttpContext.Session["empleadoId"].ToString();

                var resposable = db.SSR_ResponsableRepatriados.FirstOrDefault();

                ssr_servicios.UsuarioResponsable = resposable.UsuarioResponsable;
                ssr_servicios.CodigoResponsable = resposable.CodigoResponsable;

                try
                {
                    db.SSR_Servicios.Add(ssr_servicios);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    return Json("Error");
                }

                try
                {
                    EnviarCorreoServicioRepatriado(new string[] { resposable.CorreoResponsable, HttpContext.Session["correo"].ToString() }, "USTED HA RECIBIDO UN MENSAJE DE SERVICIOS REPATRIADOS",
                        Descripcion("SERVICIO REPATRIADOS", " " + HttpContext.Session["departamento"].ToString().ToUpper(), HttpContext.Session["nombre"].ToString().ToUpper(),
                        ssr_servicios.Notas, ssr_servicios.ServicioId, " nombre del servicio repatriados"));
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    return Json("ErrorDeCorreo");
                }

                return Json("Guardado");

            }

            return Json(Errores);
        }

        public ActionResult RegistroServicio(int id)
        {
            SSR_Servicios servicio = db.SSR_Servicios.Find(id);

            if (servicio == null) return HttpNotFound();

            if (User.IsInRole("SRA"))
            {

                getSelectLists(servicio.ServicioTipoId.GetValueOrDefault(), servicio.RepatriadoId, servicio.InstitucionId);

                ViewBag.CodigoResponsable = new SelectList(db.VISTA_EMPLEADOS.Where(x => x.cargoid == "517"), "Empleadoid", "Nombre", servicio.CodigoResponsable);
            }
            //else if (User.IsInRole("SRAC")) { }

            ViewBag.ServicioEstadoId = new SelectList(db.SSR_ServiciosEstados.Where(x=>x.ServicioEstadoId != 4), "ServicioEstadoId", "Descripcion", servicio.ServicioEstadoId);

            return View(servicio);
        }

        public JsonResult CargarServicioRepatriado(int id)
        {
            SSR_Servicios servicio = db.SSR_Servicios.Find(id);

            return Json(servicio);
        }

        [HttpPost]
        public JsonResult RegistroServicio(SSR_Servicios servicio)
        {
            //getSelectLists();
            SSR_Servicios ssr_servicios = db.SSR_Servicios.Find(servicio.ServicioId);

            if (User.IsInRole("SRA"))
            {

                if (ssr_servicios.CodigoResponsable != servicio.CodigoResponsable) ActualizarResponsable(ssr_servicios, servicio);

                if (!TryUpdateModel(ssr_servicios, new string[] { "ServicioEstadoId" , "ServicioTipoId" , "Conclusion", "RepatriadoId", "InstitucionId", "UsuarioResponsable", "CodigoResponsable" })) return Json("Error");

            }
            else if (User.IsInRole("SRAC"))
            {

                if (!TryUpdateModel(ssr_servicios, new string[] { "ServicioEstadoId", "Conclusion" })) return Json("Error");
            }

            db.SaveChanges();

            return Json("Guardado");
        }

        [HttpPost]
        public JsonResult ActualizarServicioModal(SSR_Servicios servicio, FormCollection fc)
        {
            SSR_Servicios ssr_servicios = db.SSR_Servicios.Find(int.Parse(Request.Form["ServicioIdEditModal"].ToString()));

            ssr_servicios.ServicioEstadoId = int.Parse(Request.Form["ServicioEstadoIdModal"].ToString());
            ssr_servicios.Conclusion = Request.Form["conclusion"].ToString();

            if (User.IsInRole("SRA"))
            {
                DateTime FechaFinal = DateTime.ParseExact(fc["FechaFin"], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                ssr_servicios.FechaFin = FechaFinal;
                ssr_servicios.InstitucionId = int.Parse(Request.Form["InstitucionIdModal"].ToString());
                ssr_servicios.ServicioTipoId = int.Parse(Request.Form["ServicioTipoIdModal"].ToString());

                servicio.CodigoResponsable = Request.Form["CodigoResponsableModal"].ToString();
                servicio.CodigoCreador = Request.Form["CodigoCreadorEditModal"].ToString(); 

                if (ssr_servicios.CodigoResponsable != servicio.CodigoResponsable) ActualizarResponsable(ssr_servicios, servicio);

                //if (!TryUpdateModel(ssr_servicios, new string[] { "ServicioEstadoId", "ServicioTipoId", "Conclusion", "FechaFin", "RepatriadoId",
                //    "InstitucionId", "UsuarioResponsable", "CodigoResponsable" })) return Json("Error");

            }

            db.SaveChanges();

            return Json("Actualizado");
        }

        
        public ActionResult RegistroServicioModal()
        {
           
          
                getSelectLists();
        

            ViewBag.CodigoResponsable = new SelectList(db.VISTA_EMPLEADOS.Where(x => x.cargoid == "517"), "Empleadoid", "Nombre");

            ViewBag.ServicioEstadoId = new SelectList(db.SSR_ServiciosEstados.Where(x => x.ServicioEstadoId != 4), "ServicioEstadoId", "Descripcion");

            return View();
        }

        [HttpPost]
        public JsonResult RegistroServicioModal(int? id)
        {
            var servicios = db.SSR_Servicios.Find(id);

            getSelectLists();


            ViewBag.CodigoResponsable = new SelectList(db.VISTA_EMPLEADOS.Where(x => x.cargoid == "517"), "Empleadoid", "Nombre");

            ViewBag.ServicioEstadoId = new SelectList(db.SSR_ServiciosEstados.Where(x => x.ServicioEstadoId != 4), "ServicioEstadoId", "Descripcion");

            return Json(servicios);
        }


        [HttpPost]
        public JsonResult CargarRepatriados()
        {

            var repatriados = db.SSR_Repatriados.Select(x => new { Value = x.RepatriadoId, Text = x.Nombres + " " + x.Apellido1 + " " + x.Apellido2 }).ToList();

            return Json(repatriados);
        }

        [HttpPost]
        public JsonResult CargarServiciosRepatriados(string opcion, string repatriado, string codigoUsuario, string codigoResponsable,
            int? servicioEstadoId, int? tipoServicioId, int? institucionId, string fechainicio_string, string fechafinal_string)
        {
            string codigo = HttpContext.Session["empleadoId"].ToString();

            //var resultado = Opcion != "asignados";

            #region Convertir los valores a tipo fecha

            bool fecha_filtro_valido = true;

            DateTime Fecha_Inicio, Fecha_Final; Fecha_Inicio = Fecha_Final = DateTime.Now;

            if (!string.IsNullOrEmpty(fechainicio_string) && !string.IsNullOrEmpty(fechafinal_string))
            {
                if (Code.Utilities2.convertirStringAfecha(fechainicio_string, out Fecha_Inicio) && 
                    Code.Utilities2.convertirStringAfecha(fechafinal_string, out Fecha_Final))
                    fecha_filtro_valido = false;
                else
                    return Json("fecha invalidas");
                Fecha_Final = Fecha_Final.AddDays(1).AddSeconds(-1);
            }

            #endregion

            var servicios = db.SSR_Servicios.Where(x => x.ServicioEstadoId != 4 &&

                   (x.CodigoResponsable == codigo || opcion != "asignados")
                && (x.CodigoCreador == codigo || opcion != "solicitados")
                && (x.ServicioEstadoId == servicioEstadoId || servicioEstadoId == null)
                && (x.InstitucionId == institucionId || institucionId == null)
                && (x.ServicioTipoId == tipoServicioId || tipoServicioId == null)
                && ((x.SSR_Repatriados.Nombres.Contains(repatriado) || x.SSR_Repatriados.Apellido1.Contains(repatriado) ||
                x.SSR_Repatriados.Apellido2.Contains(repatriado)) || repatriado == null)
                && ((x.CodigoResponsable == codigoResponsable) || codigoResponsable == null)
                && ((x.CodigoCreador == codigoUsuario) || codigoUsuario == null)
                && (x.FechaCreacion >= Fecha_Inicio && x.FechaCreacion <= Fecha_Final || (fecha_filtro_valido))

            ).Select(x => new
            {
                ServicioId = x.ServicioId,
                Repatriado = x.SSR_Repatriados.Nombres + " " + x.SSR_Repatriados.Apellido1 + " " + x.SSR_Repatriados.Apellido2,
                Servicios = x.SSR_ServiciosTipos.Descripcion,
                ServiciosEstado = x.SSR_ServiciosEstados.Descripcion,
                FechaCreacion = x.FechaCreacion,
                Notas = x.Notas
            }).ToList();

            return Json(servicios);
        }

        public ActionResult ReporteFiltro() {
            PartialViewBag();
            return View();
        }

        public ActionResult GenerarReporte(int? tipo, int? institucion, int? tipoServicio, string FHINICIAL, string FHFINAL)
        {

            ViewBag.institucion = institucion != null ? db.SSR_Instituciones.Find((int)institucion)?.Descripcion : null;
            ViewBag.tipoServicio = tipoServicio!= null ? db.SSR_ServiciosTipos.Find((int)tipoServicio)?.Descripcion :  null;

            ViewBag.tipo = tipo == 1 ? "Por Responsables" : "Por Creadores";

            ViewBag.fecha_inicio = FHINICIAL;
            ViewBag.fecha_final = FHFINAL;

            #region Convertir los valores a tipo fecha

            bool fecha_filtro_valido = true;

            DateTime Fecha_Inicio, Fecha_Final; Fecha_Inicio = Fecha_Final = DateTime.Now;

            if (!string.IsNullOrEmpty(FHINICIAL) && !string.IsNullOrEmpty(FHFINAL))
            {
                if (Code.Utilities2.convertirStringAfecha(FHINICIAL, out Fecha_Inicio) &&
                    Code.Utilities2.convertirStringAfecha(FHFINAL, out Fecha_Final))
                    fecha_filtro_valido = false;
                else
                    return Json("fecha invalidas");
                Fecha_Final = Fecha_Final.AddDays(1).AddSeconds(-1);
            }

            #endregion

            var prequery = (tipo == 1) ? db.SSR_Servicios.ToArray().Join(db.Vw_Mostrar_Personal_Permisos_PGR, SSR_Servicios => SSR_Servicios.CodigoResponsable,
                Vw_Mostrar_Personal_Permisos_PGR => Vw_Mostrar_Personal_Permisos_PGR.Codigo, (SSR_Servicios, Vw_Mostrar_Personal_Permisos_PGR) =>
                new { full_name = Vw_Mostrar_Personal_Permisos_PGR.NombreCompleto, SSR_Servicios = SSR_Servicios }) : 
                db.SSR_Servicios.ToArray().Join(db.Vw_Mostrar_Personal_Permisos_PGR, SSR_Servicios => SSR_Servicios.CodigoCreador,
                Vw_Mostrar_Personal_Permisos_PGR => Vw_Mostrar_Personal_Permisos_PGR.Codigo, (SSR_Servicios, Vw_Mostrar_Personal_Permisos_PGR) =>
                new { full_name = Vw_Mostrar_Personal_Permisos_PGR.NombreCompleto, SSR_Servicios = SSR_Servicios });

            var query = prequery.Where

            (x => x.SSR_Servicios.ServicioEstadoId != 4

                && (x.SSR_Servicios.InstitucionId == institucion || institucion == null) 

                && (x.SSR_Servicios.ServicioTipoId == tipoServicio || tipoServicio == null)

                && ((x.SSR_Servicios.FechaCreacion >= Fecha_Inicio && x.SSR_Servicios.FechaCreacion <= Fecha_Final || fecha_filtro_valido))

                //&& (x.SSR_Servicios.FechaCreacion > DateTime.Now.AddDays(-30) && (string.IsNullOrEmpty(FHINICIAL) && string.IsNullOrEmpty(FHFINAL)) || 
                //(x.SSR_Servicios.FechaCreacion >= Fecha_Inicio && x.SSR_Servicios.FechaCreacion <= Fecha_Final))

                //&& (x.Sol_Registro_Solicitud.EstadoId.ToString() == state || string.IsNullOrEmpty(state)) 

                //&& (x.Sol_Registro_Solicitud.Tipo_SolicitudId.ToString() == service_type || string.IsNullOrEmpty(service_type))

                //&& (x.Sol_Registro_Solicitud.Tipo_Sub_SolicitudId.ToString() == service_area || string.IsNullOrEmpty(service_area))

                //&& (x.Sol_Registro_Solicitud.DependenciaRegionalId.ToString() == dependency || string.IsNullOrEmpty(dependency)))

                ).GroupBy(x => new { x.full_name, x.SSR_Servicios.ServicioEstadoId }).Select(x => new { full_name = x.Key.full_name, quantity = x.Count(), state_id = x.Key.ServicioEstadoId }).OrderBy(x => x.full_name).ToList();

            var first_chart_data = new List<Code.ChartData>();
            var names = new List<string>();

            for (int i = 0; i < query.Count; i++)
            {
                string item_name = query[i].full_name;

                if (i > 0)
                {
                    int repeated = 0;

                    for (int j = i - 1; j >= 0; j--)
                    {
                        if (query[j].full_name == query[i].full_name)
                        {
                            repeated++;
                        }
                    }

                    if (repeated == 0)
                    {
                        names.Add(query[i].full_name);
                    }
                }
                else if (i == 0)
                {
                    names.Add(query[i].full_name);
                }
            }

            foreach (var item in names)
            {
                var rows = query.Where(x => x.full_name == item);

                var first_chart_item = new Code.ChartData();

                foreach (var item2 in rows)
                {
                    if (item2.state_id == 1)
                    {
                        first_chart_item.pending = item2.quantity;
                    }
                    else if (item2.state_id == 2)
                    {
                        first_chart_item.closed = item2.quantity;
                    }
                    else if (item2.state_id == 3)
                    {
                        first_chart_item.canceled = item2.quantity;
                    }
                    else if (item2.state_id == 4)
                    {
                        first_chart_item.pending_evaluation = item2.quantity;
                    }
                }

                first_chart_item.name = item;
                first_chart_item.total = first_chart_item.pending + first_chart_item.closed + first_chart_item.canceled + first_chart_item.pending_evaluation;

                first_chart_data.Add(first_chart_item);
            }

            ViewBag.lista = first_chart_data;

            return new ViewAsPdf("SSR_Reportes")
            {
                PageOrientation = Orientation.Portrait,
                PageSize = Rotativa.Options.Size.Letter,
                PageMargins = { Left = 0, Right = 0, Bottom = 12, Top = 10 }
            };
        }

        private void ActualizarResponsable(SSR_Servicios ssr_servicios, SSR_Servicios servicio)
        {
            ssr_servicios.CodigoResponsable = servicio.CodigoResponsable;

            var ResponsableUser = Code.Utilities2.obtenerSp_UserPorCodigo(servicio.CodigoResponsable);
            var CreadorUser = Code.Utilities2.obtenerSp_UserPorCodigo(servicio.CodigoCreador);

            ssr_servicios.UsuarioResponsable = ResponsableUser.samAccountName;

            EnviarCorreoServicioRepatriado(new string[] { CreadorUser.mail, HttpContext.Session["correo"].ToString(), ResponsableUser.mail }, "USTED HA RECIBIDO UN MENSAJE DE SERVICIOS REPATRIADOS",
            Descripcion("REASIGNACIÓN SOLICITUD DE REPATRIADOS", " " + HttpContext.Session["departamento"].ToString().ToUpper(), CreadorUser.displayName.ToUpper(),
            ssr_servicios.Notas, ssr_servicios.ServicioId, " nombre del servicio repatriados"));
        }

        [HttpPost]
        public JsonResult GetUsuarioCreadores()
        {
            var Ids_UsuariosCreadores = db.SSR_Servicios.Select(x => x.CodigoCreador).Distinct();

            var UsuariosCreadores = db.VISTA_EMPLEADOS.Where(x => Ids_UsuariosCreadores.Contains(x.empleadoid)).Select(x=> new {

                Value = x.empleadoid,
                Text = x.nombre
            }).ToList();

            return Json(UsuariosCreadores);
        }

        private string Descripcion(string Titulo, string Departamento, string NombreCompleto, string Problema, int ID, string Descripcion)
        {
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/Repatriado-nuevoServicio.html");

            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", Titulo);
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/ServiciosRepatriados/RegistroServicio/{ID}");
            Archivo = Archivo.Replace("$Descripcion", Descripcion);

            return Archivo;
        }

        private void EnviarCorreoServicioRepatriado(string[] Correos, string Sujeto, string Descripcion)
        {
            Correo correo = new Correo();

            for (int i = 0; i < Correos.Length; i++)
            {
                Correos[i] = (Correos[i] != "") ? Correos[i] : "";
            }

            //string PrimerCorreo = (Responsable != "") ? Responsable + "@PGR.GOB.DO" : "";

            string Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            string LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            string Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            correo.EnviarCorreoServicioRepatriado_(Correos, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
        }

        public void getSelectLists(int servicioTipo = 0, int id = 0, int repatriado =0, int institucion = 0)
        {
            ViewBag.ServicioTipoId = new SelectList(db.SSR_ServiciosTipos.Where(s => s.Estado == true), "ServicioTipoId", "Descripcion", servicioTipo);
            ViewBag.RepatriadoId = new SelectList(db.SSR_Repatriados, "RepatriadoId", "NombreCompleto", repatriado);
            ViewBag.IdRepatriados = new SelectList(db.SSR_Repatriados, "RepatriadoId", "NombreCompleto", repatriado);
            ViewBag.InstitucionId = new SelectList(db.SSR_Instituciones.Where(i => i.Estado == true), "InstitucionId", "Descripcion", institucion);
        }

        private void PartialViewBag()
        {
            ViewBag.institucion = new SelectList(db.SSR_Instituciones, "InstitucionId", "Descripcion");
            ViewBag.tipoServicio = new SelectList(db.SSR_ServiciosTipos, "ServicioTipoId", "Descripcion");
            ViewBag.estados = new SelectList(db.SSR_ServiciosEstados.Where(x => x.ServicioEstadoId != 4), "ServicioEstadoId", "Descripcion");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}