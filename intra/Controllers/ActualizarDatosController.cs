﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.ActualizacionDatos;
using System.Configuration;
using System.IO;
using Rotativa;
using intra.Code;
using intra.Models.GestionHumana;
using intra.Models.vw_Models;

namespace intra.Controllers
{
    public class ActualizarDatosController : Controller
    {
        private dbIntranet db = new dbIntranet();
        private DATOSEMPLEADOS db3 = new DATOSEMPLEADOS();
        private Correo correo = new Correo();


        [AllowAnonymous]
        #region CorreoActualizacionDatos
        public void CorreoActualizacionDatos(string Solicitante, string Sujeto, string Descripcion)
        {
            Solicitante = "catherine.ventura@pgr.gob.do";

            string correoUsuario = Session["usuario"].ToString() + "@pgr.gob.do";

           string SegundoCorreo = "";
           string TercerCorreo = "gestionhumana@pgr.gob.do";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");


            SegundoCorreo = Solicitante;
            correo.CorreoGestionHumana(SegundoCorreo.Trim(), Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            correo.CorreoGestionHumana(correoUsuario.Trim(), Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            correo.CorreoGestionHumana(TercerCorreo.Trim(), Sujeto, Descripcion, Logo, Flecha, LogoDTI);
        }
        #endregion
       [AllowAnonymous]
        [CustomAuthorize]
        public PartialViewResult vDatosPersonales()
        {
            string id = Session["empleadoId"].ToString();
            var Empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id).FirstOrDefault();
            int Codigo = int.Parse(Session["empleadoId"].ToString());
            //try { 
            ViewBag.EmpleadoId = id;
            ViewBag.parentesco = new SelectList(db.Parentesco, "ParentescoId", "Descripcion").ToList();

            ViewBag.ActividadPreferencial = db.ActualizacionActividad.ToList();

            ViewBag.ActividadesSeleccionada =
                db.EmpleadoComplemento.FirstOrDefault(x => x.EmpleadoId == Codigo).ActualizacionActividad.Select(x => x.ActividadId).ToList();

            var dependientes = db.ActualizacionDependientes.Where(x => x.EmpleadoId == Codigo && x.Estado).ToList();

            ViewBag.dependientes = dependientes?.Count;

            var empleadosContratados = db.empleadoscontratados.Where(x => x.Empleadoid == id).FirstOrDefault();
            var EmpleadoComplemento1 = db.EmpleadoComplemento.Where(x => x.EmpleadoId == Codigo).FirstOrDefault();
            var empleadoIntermedio = db.EmpleadoIntermedio.Where(x => x.EmpleadoCodigo == Codigo).FirstOrDefault();
            if(empleadoIntermedio == null &&  empleadosContratados == null)
            {
                EmpleadoIntermedio empleadointer = new EmpleadoIntermedio();
                empleadointer.EmpleadoCodigo = Codigo;
                empleadointer.EstadoCivil = 3;
                empleadointer.FhCreacion = DateTime.Now;
                db.EmpleadoIntermedio.Add(empleadointer);
                db.SaveChanges();

               
            } else if(empleadoIntermedio != null)
                
             {

                var empleadoIntermedio1 = db.EmpleadoIntermedio.OrderByDescending(x => x.EmpleadoIntId).FirstOrDefault();
                empleadoIntermedio = empleadoIntermedio1;
            }
            else
            {

                empleadosContratados.Empleadoid = id;
                empleadosContratados.EstadoCivil = 3;
                db.SaveChanges();

                var empleadosContratados1 = db.empleadoscontratados.Where(x => x.Empleadoid == id).FirstOrDefault();
                ViewBag.empleadosContratados = empleadosContratados1;


            }



            if (EmpleadoComplemento1 == null)
            {
               
                EmpleadoComplemento empleado = new EmpleadoComplemento();
                empleado.EmpleadoId = Codigo;
                empleado.FhCreacion = DateTime.Now;
                empleado.FhModificacion = DateTime.Now;
                //empleado.NivelAcademicoId = 3;
                db.EmpleadoComplemento.Add(empleado);
                db.SaveChanges();

                ViewBag.AfpId = new SelectList(db.AFP.ToList(), "AfpId", "Descripcion");
                ViewBag.ArsId = new SelectList(db.ARS.ToList(), "ArsId", "Descripcion");
                ViewBag.ID_EstadoCivil = new SelectList(db.EstadosCiviles.ToList(), "ID_EstadoCivil", "Descripcion");
                ViewBag.EmpleadoComplemento1 = empleado;


            }
            else
            {
                ViewBag.ProvinciaId = new SelectList(db.vw_Provinces.ToList(), "ProvinciaId", "ProvinciaDescripcion", EmpleadoComplemento1.ProvinciaId);
                ViewBag.MunicipioId = new SelectList(db.vw_Municipios.Where(x=> x.ProvinciaId == EmpleadoComplemento1.ProvinciaId).ToList(), "MunicipioId", "MunicipioDescripcion", EmpleadoComplemento1.MunicipioId);
                ViewBag.DistritoId = new SelectList(db.vw_Distritos.Where(x=> x.MunicipioId == EmpleadoComplemento1.MunicipioId).ToList(), "DistritoId", "DistritoDescripcion", EmpleadoComplemento1.DistritoId);
                ViewBag.CiudadId = new SelectList(db.vw_Ciudad.Where(x=> x.DistritoId == EmpleadoComplemento1.DistritoId).ToList(), "CiudadId", "CiudadDescripcion", EmpleadoComplemento1.CiudadId);
                ViewBag.SectorId = new SelectList(db.vw_Sectores.Where(x=> x.CiudadId == EmpleadoComplemento1.CiudadId).ToList(), "SectorId", "SectorDescripcion", EmpleadoComplemento1.SectorId);
                ViewBag.AfpId = new SelectList(db.AFP.ToList(), "AfpId", "Descripcion", EmpleadoComplemento1.AfpId);
                ViewBag.ArsId = new SelectList(db.ARS.ToList(), "ArsId", "Descripcion", EmpleadoComplemento1.ArsId);

                if(empleadosContratados != null)
                {
                ViewBag.ID_EstadoCivil = new SelectList(db.EstadosCiviles.ToList(), "ID_EstadoCivil", "Descripcion", empleadosContratados?.EstadoCivil);
                }
                else
                {
                    ViewBag.ID_EstadoCivil = new SelectList(db.EstadosCiviles.ToList(), "ID_EstadoCivil", "Descripcion", empleadoIntermedio?.EstadoCivil);


                }
                ViewBag.EmpleadoComplemento1 = EmpleadoComplemento1;
            }

            return PartialView(Empleados);
 

            

        }

        public ActionResult PruebaAcademico()
        {
            return View();

        }


        //Tab Datos Personales
        [CustomAuthorize]

        [HttpPost]
        public JsonResult DatosPersonales(EmpleadoComplemento empleadoComplemento, EmpleadoIntermedio empleadoIntermedio, FormCollection fc)
        {
            string id = Session["empleadoId"].ToString();
            int Codigo = int.Parse(Session["empleadoId"].ToString());

            string[] actividades = fc["actividades[]"].Split(',');

            try {


                ViewBag.EmpleadoId = id;

            var Empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id).FirstOrDefault();
            var EmpleadoComplemento1 = db.EmpleadoComplemento.Where(x => x.EmpleadoId == Codigo).FirstOrDefault();
            var empleadosContratados = db.empleadoscontratados.Where(x => x.Empleadoid == id).FirstOrDefault();
           
            if(empleadosContratados != null)
              {
                    empleadosContratados.EstadoCivil = int.Parse(fc["ID_EstadoCivil"]);
                    empleadosContratados.Celular = fc["Celular"];
                    empleadosContratados.Telefono1 = fc["telefono1"];
                    empleadosContratados.TelefonoResidencial = fc["Telefono2"];
                    empleadosContratados.Email = fc["Email"];
                    db.SaveChanges();

                    var empleadosContratados1 = db.empleadoscontratados.Where(x => x.Empleadoid == id).FirstOrDefault();

                    ViewBag.empleadosContratados = empleadosContratados1;

                }
            else
                {
                    EmpleadoIntermedio empleado1 = new EmpleadoIntermedio();
                    empleado1.EmpleadoCodigo = Codigo;
                    empleado1.FhCreacion = DateTime.Now;
                    var email = fc["Email"].ToString();
                    empleado1.Email = fc["Email"];
                    var EstadoCivil = fc["ID_EstadoCivil"].ToString();
                    empleado1.EstadoCivil = int.Parse(fc["ID_EstadoCivil"]);
                    var celular = fc["Celular"].ToString();
                    empleado1.Celular = fc["Celular"];
                    var Telefono1 = fc["telefono1"].ToString();
                    empleado1.Telefono1 = fc["telefono1"];
                    var Telefono2 = fc["telefono2"].ToString();
                    empleado1.Telefono2 = fc["Telefono2"];
                    db.EmpleadoIntermedio.Add(empleado1);
                    db.SaveChanges();
                }

                if (EmpleadoComplemento1 == null)
                {
                    EmpleadoComplemento empleadoComple = new EmpleadoComplemento();
                    empleadoComple.AfpId = int.Parse(fc["AfpId"]);
                    empleadoComple.ArsId = int.Parse(fc["ArsId"]);
                    empleadoComple.ProvinciaId = int.Parse(fc["ProvinciaId"]);
                    empleadoComple.MunicipioId = int.Parse(fc["MunicipioId"]);
                    empleadoComple.DistritoId = int.Parse(fc["DistritoId"]);
                    empleadoComple.CiudadId = int.Parse(fc["CiudadId"]);
                    empleadoComple.SectorId = int.Parse(fc["SectorId"]);
                    empleadoComple.Dependiente = int.Parse(fc["CantDependiente"]);
                    empleadoComple.Calle = fc["Calle"];
                    empleadoComple.Numero = fc["Numero"];
                    db.EmpleadoComplemento.Add(empleadoComple);
                    db.SaveChanges();

                    AgregarActividades(actividades, empleadoComple);


                    ViewBag.EmpleadoComplemento1 = empleadoComple;
                }
                else
                {
                    EmpleadoComplemento1.AfpId = int.Parse(fc["AfpId"]);
                    EmpleadoComplemento1.ArsId = int.Parse(fc["ArsId"]);
                    if (fc["ProvinciaId"] == "" || fc["ProvinciaId"] == null)
                    {
                        return Json(new { success = false, Mensaje = "Favor Elegir una Provincia" }, JsonRequestBehavior.AllowGet);
                    }

                    if (fc["MunicipioId"] == "" || fc["MunicipioId"] == null)
                    {
                        return Json(new { success = false, Mensaje = "Favor Elegir un municipio" }, JsonRequestBehavior.AllowGet);

                    }
                    if (fc["DistritoId"] == "" || fc["DistritoId"] == null)
                    {
                        return Json(new { success = false, Mensaje = "Favor Elegir una ciudad" }, JsonRequestBehavior.AllowGet);
                    }
                    if (fc["CiudadId"] == "" || fc["CiudadId"] == null)
                    {
                        return Json(new { success = false, Mensaje = "Favor Elegir una localidad" }, JsonRequestBehavior.AllowGet);
                    }
                    if (fc["SectorId"] == "" || fc["SectorId"] == null)
                    {
                        return Json(new { success = false, Mensaje = "Favor Elegir un sector" }, JsonRequestBehavior.AllowGet);
                    }

                EmpleadoComplemento1.ProvinciaId = int.Parse(fc["ProvinciaId"]);
                EmpleadoComplemento1.MunicipioId = int.Parse(fc["MunicipioId"]);
                EmpleadoComplemento1.DistritoId = int.Parse(fc["DistritoId"]);
                EmpleadoComplemento1.CiudadId = int.Parse(fc["CiudadId"]);
                EmpleadoComplemento1.SectorId = int.Parse(fc["SectorId"]);
                EmpleadoComplemento1.Calle = fc["Calle"];
                //EmpleadoComplemento1.Dependiente = int.Parse(fc["Dependiente"]);
                EmpleadoComplemento1.Numero = fc["Numero"];
                db.SaveChanges();

                    AgregarActividades(actividades, EmpleadoComplemento1);


                    ViewBag.EmpleadoComplemento1 = EmpleadoComplemento1;
            }


            return Json(new { success = true, Mensaje = "Datos Actualizados Correctamente" }, JsonRequestBehavior.AllowGet);
        } catch(Exception ex)

            {
                ex.ToString();

                //TempData["Index"] = "Ha ocurrido un error, al procesar su solicitud";

                return Json(new { success = false, Mensaje = "Ha ocurrido un error, al procesar su solicitud" }, JsonRequestBehavior.AllowGet);

            }
}


        //Tab Nivel Academico
        [CustomAuthorize]
        public PartialViewResult vNivelAcademico()
        {
            string id = Session["empleadoId"].ToString();
            int Codigo = int.Parse(Session["empleadoId"].ToString());
            try { 
            ViewBag.EmpleadoId = id;
            ViewBag.ProfesionId = new SelectList(db.Profesiones.ToList().OrderBy(x=> x.Descripcion), "ProfesionId", "Descripcion");
            ViewBag.InstitucionId = new SelectList(db.Instituciones.ToList().OrderBy(x=> x.Descripcion), "InstitucionId", "DescripcionConSiglas");

            var EmpleadoComplemento1 = db.EmpleadoComplemento.Where(x => x.EmpleadoId == Codigo).FirstOrDefault();

            if (EmpleadoComplemento1 == null)
            {
                EmpleadoComplemento empleado = new EmpleadoComplemento();
                empleado.EmpleadoId = Codigo;
                empleado.FhCreacion = DateTime.Now;
                empleado.FhModificacion = DateTime.Now;
                empleado.NivelAcademicoId = 3;
                db.EmpleadoComplemento.Add(empleado);
                db.SaveChanges();


                ViewBag.EmpleadoComplemento1 = empleado;

                ViewBag.NivelAcademicoId = new SelectList(db.NivelAcademico.ToList().OrderBy(x=>x.Descripcion), "NivelAcademicoId", "Descripcion");

            }
            else
            {

                ViewBag.NivelAcademicoId = new SelectList(db.NivelAcademico.ToList().OrderBy(x => x.Descripcion), "NivelAcademicoId", "Descripcion");

                ViewBag.EmpleadoComplemento1 = EmpleadoComplemento1;
            }

            ViewBag.carreras = db.Empleados_Carreras.Where(x=>x.Empleadoid == Codigo && x.Estado).ToList();

            var Empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id).FirstOrDefault();

            return PartialView(Empleados);
        } catch(Exception ex)

            {
                ex.ToString();

                TempData["Index"] = "Ha ocurrido un error, al procesar su solicitud";
               return PartialView("vNivelAcademico", "ActualizarDatos");

    }


}

        [HttpPost]
        public JsonResult NivelAcademico(Empleados_Carreras carrera) {

            if((carrera.ProfesionId == 0 && string.IsNullOrEmpty(carrera.OtraProfesion)) 
                || (carrera.InstitucionId == 0 && string.IsNullOrEmpty(carrera.OtraInstitucion)) 
                || carrera.NivelAcademicoId == 0)

                return Json(new { success = false, Mensaje = "Faltan Campos" });

            try {

                carrera.FhCreacion = carrera.FhModificacion = DateTime.Now;

                carrera.Estado = true;

                db.Empleados_Carreras.Add(carrera);

                db.SaveChanges();
            }
            catch (Exception ex) {

                ex.ToString();
                return Json(new { success = false, Mensaje = "Un Error ha Ocurrido" });
            }

            return Json(new { success = true, Mensaje = "Datos Actualizados Correctamente" });
        }

        [HttpPost]
        public JsonResult GetCarrera(int id) {

            var carrera = db.Empleados_Carreras.Where(x=>x.CarreraId == id).Select(x => new { ID=x.CarreraId, InstitucionID = x.InstitucionId , ProfesionID = x.ProfesionId,
                NivelID = x.NivelAcademicoId, OtraInstitucion = x.OtraInstitucion, OtraProfesion = x.OtraProfesion , Profesion = x.Profesiones.Descripcion,
                Institucion = x.Institucion.Descripcion, NivelAcademico = x.NivelAcademico.Descripcion });

            return Json(carrera.First());
        }

        [HttpPost]
        public JsonResult CargarCarreras()
        {
            int codigo = int.Parse(Session["empleadoid"].ToString());

            var carreras = db.Empleados_Carreras.Where(x => x.Empleadoid == codigo && x.Estado == true).Select(x => new
            {
                CarreraId = x.CarreraId,
                Descripcion = x.NivelAcademico.Descripcion,
                OtraInstitucion = x.OtraInstitucion,
                DescripcionConSiglas = x.Institucion.Descripcion + "(" + x.Institucion.Siglas.Trim() + ")",
                OtraProfesion = x.OtraProfesion,
                ProfesionesDescripcion = x.Profesiones.Descripcion
            });

            return Json(carreras);
        }

        [HttpPost]
        public JsonResult EditarCarrera(int CarreraId,int InstitucionId,int ProfesionId,int NivelAcademicoId,string OtraProfesion,string OtraInstitucion)
        {
            if ((ProfesionId == 0 && string.IsNullOrEmpty(OtraProfesion))
                || (InstitucionId == 0 && string.IsNullOrEmpty(OtraInstitucion))
                || NivelAcademicoId == 0 || CarreraId == 0)

                return Json(new { success = false, Mensaje = "Faltan Campos" });

            try
            {
                var carrera = db.Empleados_Carreras.Find(CarreraId);

                carrera.InstitucionId = InstitucionId;
                carrera.ProfesionId = ProfesionId;
                carrera.NivelAcademicoId = NivelAcademicoId;
                carrera.OtraProfesion = OtraProfesion;
                carrera.OtraInstitucion = OtraInstitucion;

                carrera.FhModificacion = DateTime.Now;

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json(new { success = false, Mensaje = "Un Error ha Ocurrido" });
            }

            return Json(new { success = true, Mensaje = "Datos Actualizados Correctamente" });
        }

        [HttpPost]
        public JsonResult CambiarEstadoCarrera(int id)
        {
            var carrera = db.Empleados_Carreras.Find(id);
            carrera.Estado = false;
            db.SaveChanges();
            return Json("Eliminado");
        }

        //Tab Observaciones
        [CustomAuthorize]
        [HttpGet]
        public PartialViewResult vObservaciones()
        {
            string id = Session["empleadoId"].ToString();
            int Codigo = int.Parse(Session["empleadoId"].ToString());
            ViewBag.EmpleadoId = id;
            try
            { 
            var Empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id).FirstOrDefault();
            var EmpleadoComplemento1 = db.EmpleadoComplemento.Where(x => x.EmpleadoId == Codigo).FirstOrDefault();

            if (EmpleadoComplemento1 == null)
            {
                EmpleadoComplemento empleado = new EmpleadoComplemento();
                empleado.EmpleadoId = Codigo;
                empleado.FhCreacion = DateTime.Now;
                empleado.FhModificacion = DateTime.Now;
                empleado.Observaciones = "";
                db.EmpleadoComplemento.Add(empleado);
                db.SaveChanges();


                ViewBag.EmpleadoComplemento1 = empleado;

            }
            else
            {

                ViewBag.EmpleadoComplemento1 = EmpleadoComplemento1;
            }

            return PartialView(Empleados);
        } catch(Exception ex)

            {
                ex.ToString();

                TempData["Index"] = "Ha ocurrido un error, al procesar su solicitud";
               return PartialView("vObservaciones", "ActualizarDatos");

    }
}

        //Tab Observaciones
        [CustomAuthorize]

        [HttpPost]
        public JsonResult Observaciones(EmpleadoComplemento empleadoComplemento, FormCollection fc)
        {
            string id = Session["empleadoId"].ToString();
            int Codigo = int.Parse(Session["empleadoId"].ToString());
            ViewBag.EmpleadoId = id;

            try { 

            var Empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id).FirstOrDefault();
            var EmpleadoComplemento1 = db.EmpleadoComplemento.Where(x => x.EmpleadoId == Codigo).FirstOrDefault();

            if (EmpleadoComplemento1 == null)
            {
                EmpleadoComplemento empleado = new EmpleadoComplemento();
                empleado.EmpleadoId = Codigo;
                empleado.FhCreacion = DateTime.Now;
                empleado.FhModificacion = DateTime.Now;
                empleado.Observaciones = fc["Observaciones"];
                db.EmpleadoComplemento.Add(empleado);
                db.SaveChanges();
                ViewBag.EmpleadoComplemento1 = empleado;



            }
            else
            {
                EmpleadoComplemento1.Observaciones = empleadoComplemento.Observaciones;
                db.SaveChanges();

                ViewBag.EmpleadoComplemento1 = EmpleadoComplemento1;
            }


            return Json(new { success = true, Mensaje = "Datos Actualizados Correctamente" }, JsonRequestBehavior.AllowGet);
        }
             catch (Exception ex)
            {
                ex.ToString();
                return Json(new { success = false, Mensaje = "Ha ocurrido un error con los datos proporcinado, verifique que todo esta correcto" }, JsonRequestBehavior.AllowGet);
            }
        }



        //Tab Redes Sociales
        [CustomAuthorize]
        public PartialViewResult vRedesSociales()
        {
            string id = Session["empleadoId"].ToString();
            int Codigo = int.Parse(Session["empleadoId"].ToString());
            ViewBag.EmpleadoId = id;
            TempData["Facebook"] = "";
            TempData["Twitter"] = "";
            TempData["Instagram"] = "";

            try { 

            var Empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id).FirstOrDefault();
            var EmpleadoComplemento1 = db.EmpleadoComplemento.Where(x => x.EmpleadoId == Codigo).FirstOrDefault();

            if (EmpleadoComplemento1 == null)
            {
                EmpleadoComplemento empleado = new EmpleadoComplemento();
                empleado.EmpleadoId = Codigo;
                empleado.FhCreacion = DateTime.Now;
                empleado.FhModificacion = DateTime.Now;
                empleado.Facebook = "";
                empleado.Instagram = "";
                empleado.Twitter = "";
                db.EmpleadoComplemento.Add(empleado);
                db.SaveChanges();
                ViewBag.EmpleadoComplemento1 = empleado;

                TempData["Facebook"] = "";
                TempData["Twitter"] = "";
                TempData["Instagram"] = "";
            }
            else
            {

                ViewBag.NivelAcademicoID = new SelectList(db.NivelAcademico.ToList(), "NivelAcademicoId", "Descripcion", EmpleadoComplemento1.NivelAcademicoId);

                
                if(EmpleadoComplemento1.Facebook != null)
                {
                    TempData["Facebook"] = "Ok";

                }
                if (EmpleadoComplemento1.Twitter != null)
                {
                    TempData["Twitter"] = "Ok";

                }

                if (EmpleadoComplemento1.Instagram != null)
                {
                    TempData["Instagram"] = "Ok";

                }


                ViewBag.EmpleadoComplemento1 = EmpleadoComplemento1;
            }

            return PartialView(Empleados);
        } catch(Exception ex)

            {
                ex.ToString();

                TempData["Index"] = "Ha ocurrido un error, al procesar su solicitud";
               return PartialView("vRedesSociales", "ActualizarDatos");

    }
}
        //Tab Redes Sociales
        [CustomAuthorize]

        [HttpPost]
        public JsonResult RedesSociales(EmpleadoComplemento empleadoComplemento, FormCollection fc)
        {
            string id = Session["empleadoId"].ToString();
            int Codigo = int.Parse(Session["empleadoId"].ToString());
            ViewBag.EmpleadoId = id;

            try { 

            var Empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id).FirstOrDefault();
            var EmpleadoComplemento1 = db.EmpleadoComplemento.Where(x => x.EmpleadoId == Codigo).FirstOrDefault();

            if (EmpleadoComplemento1 == null)
            {
                EmpleadoComplemento empleado = new EmpleadoComplemento();
                empleado.EmpleadoId = Codigo;
                empleado.FhCreacion = DateTime.Now;
                empleado.FhModificacion = DateTime.Now;
                empleado.Facebook = fc["Facebook1"];
                empleado.Instagram = fc["instagram1"];
                empleado.Twitter = fc["twitter1"];
                db.EmpleadoComplemento.Add(empleado);
                db.SaveChanges();
                ViewBag.EmpleadoComplemento1 = empleado;

            }
            else
            {
                EmpleadoComplemento1.Facebook = fc["Facebook1"];
                EmpleadoComplemento1.Instagram = fc["instagram1"];
                EmpleadoComplemento1.Twitter = fc["twitter1"];
                db.SaveChanges();

                ViewBag.EmpleadoComplemento1 = EmpleadoComplemento1;
               
            }


            return Json(new { success = true, Mensaje = "Datos Actualizados Correctamente" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json(new { success = false, Mensaje = "Ha ocurrido un error con los datos proporcinado, verifique que todo esta correcto" }, JsonRequestBehavior.AllowGet);
            }
        }



        //Tab Cargos 

        [CustomAuthorize]
        public PartialViewResult vCargo()
        {
            string id = Session["empleadoId"].ToString();
            int Codigo = int.Parse(Session["empleadoId"].ToString());
            ViewBag.EmpleadoId = id;

            try
            { 
            var Empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id).FirstOrDefault();

            //Calcular Tiempo Laborando
            DateCalculations dateCalculations = new DateCalculations(Empleados.fechaingreso, intra.Controllers.PermisosController.fechasVacaciones);
            string tiempoLaborando = dateCalculations.ObtenerTextoTiempoLaborando();

            ViewBag.Tiempo = tiempoLaborando;

            int departamentoId = int.Parse(Empleados.departamentoid);
            int dependenciaId = int.Parse(Empleados.dependenciaid);
            ViewBag.DependenciaID = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");
            ViewBag.DependenciaID1 = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");
                ViewBag.DeptoID = new SelectList(db.vw_Departamentos.Where(x=> x.DependenciasID == Empleados.dependenciaid).ToList(), "DeptoID", "Descripcion");
            ViewBag.CargoId = new SelectList(db.vw_CargosDepto.Where(x=> x.DeptoID == departamentoId && x.DependenciaID == dependenciaId).ToList(), "CargoId", "Cargos");
            return PartialView(Empleados);

            } catch(Exception ex)

            {
                ex.ToString();

                TempData["Index"] = "Ha ocurrido un error, al procesar su solicitud";
               return PartialView("vCargo", "ActualizarDatos");

    }
}

        //Post Cargos
        [CustomAuthorize]

        [HttpPost]
        public JsonResult Cargos(EmpleadoComplemento empleadoComplemento, FormCollection fc)
        {
          
                string id = Session["empleadoId"].ToString();
            int Codigo = int.Parse(Session["empleadoId"].ToString());
            string usuarioSesion = Session["usuario"].ToString();
            ViewBag.EmpleadoId = id;
            try
            {
           var empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id).FirstOrDefault();
            var EmpleadoComplemento1 = db.EmpleadoComplemento.Where(x => x.EmpleadoId == Codigo).FirstOrDefault();

        
         
            if (fc["DependenciaID"] != "" || fc["DeptoID"] != "" || fc["CargoId"] != "")
            {

                var ACTUAL = db.EmpleadoActualizarDatos.Where(x => x.EmpleadoCodigo == Codigo).FirstOrDefault();
                if (ACTUAL != null)
                {
                    ACTUAL.EmpleadoCodigo = Codigo;


                    if (fc["CargoId"] == "" || fc["CargoId"] == null)
                    {
                        ACTUAL.CargoActual = Convert.ToInt32(empleados.cargoid);

                    }
                    else
                    {
                        ACTUAL.CargoActual = int.Parse(fc["CargoId"].ToString());
                    }

                    if (fc["DependenciaID"] == "" || fc["DependenciaID"] == null)
                    {
                        ACTUAL.DepenActual = Convert.ToInt32(empleados.dependenciaid);
                    }
                    else
                    {

                        ACTUAL.DepenActual = int.Parse(fc["DependenciaID"].ToString());

                    }

                    if (fc["DeptoID"] == "" || fc["DeptoID"] == null)
                    {
                        ACTUAL.DeptoActual = Convert.ToInt32(empleados.departamentoid);
                    }else
                    {
                        ACTUAL.DeptoActual = int.Parse(fc["DeptoID"].ToString());
                    }

                   

                    db.SaveChanges();
                }
                else
                {
                    EmpleadoActualizarDatos Actualizar = new EmpleadoActualizarDatos();
                    Actualizar.EmpleadoCodigo = Codigo;
                  

                   //Insertar el cargo
                    
                    if (fc["CargoId"] == "")
                    {
                        Actualizar.CargoActual = Convert.ToInt32(empleados.cargoid);

                    }
                    else
                    {
                        Actualizar.CargoActual = int.Parse(fc["CargoId"].ToString());
                    }

                    //Insertar el departamento
                    if (fc["DeptoID"] == "")
                    {
                        Actualizar.DeptoActual = Convert.ToInt32( empleados.departamentoid);
                    }
                    else
                    {
                        Actualizar.DeptoActual = int.Parse(fc["DeptoID"].ToString());
                    }
                    //Insertar la dependencia
                    if (fc["DependenciaID"] == "")
                    {
                        Actualizar.DepenActual = Convert.ToInt32(empleados.dependenciaid);
                    }
                    else
                    {
                     Actualizar.DepenActual = int.Parse(fc["DependenciaID"].ToString());

                    }



                    db.EmpleadoActualizarDatos.Add(Actualizar);
                    db.SaveChanges();
                }
                var update = db.EmpleadoActualizarDatos.OrderByDescending(x => x.EmpleadoCodigo).FirstOrDefault();

                // //ENVIO CORREO A TODOS
                string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/ACTUALIZARDATOS/ReporteActualizacion?id={Codigo}";
                CorreoActualizacionDatos(usuarioSesion + "@pgr.gob.do", "ACTUALIZACIÓN DATOS EMPLEADOS", EnviarCorreoRRHH(url));

            }


                return Json(new { success = true, Mensaje = "Datos Actualizados Correctamente" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json(new { success = false, Mensaje = "ha ocurrido un error, verifique que toda la información este correcta!" }, JsonRequestBehavior.AllowGet);

            }
        }



        //Reporte Actualizacion Datos empleados
        [AllowAnonymous]
        public ActionResult ReporteActualizacion(string id)
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }
            int Code = int.Parse(id);

            //try
            //{
                var actualizar = db.EmpleadoActualizarDatos.Where(x => x.EmpleadoCodigo == Code).FirstOrDefault();

            var Cargo4 = db.vw_Cargos.Where(x => x.CargoId == actualizar.CargoActual.ToString()).FirstOrDefault();
            ViewBag.Cargo4 = Cargo4?.Descripcion;
            var dependencia = db.vw_Dependencias.Where(x => x.DependenciaID == actualizar.DepenActual).FirstOrDefault();
            ViewBag.Dependencia = dependencia.Descripcion;
            var Departamento = db.vw_Departamentos.Where(x => x.DeptoID == actualizar.DeptoActual && x.DependenciasID == dependencia.DependenciaID.ToString()).FirstOrDefault();
            ViewBag.Departamento = Departamento.Descripcion;


            var ActualizacionDatos = (from v in db.VISTA_EMPLEADOS
                                      from e in db.EmpleadoActualizarDatos
                                      from c in db.vw_Cargos
                                      where e.EmpleadoCodigo == Code

                                      select new vDatosEmpleados
                                      {
                                          Nombre = v.nombre.ToUpper(),
                                          DepartamentoAnterior = v.departamento.ToUpper(),
                                          DependenciaAnterior = v.dependencia.ToUpper(),
                                          CargoAnterior = v.cargo.ToUpper(),
                                          CargoActual = c.Descripcion.ToUpper(),
                                          Cedula = v.cedula,
                                        //  Supervisor = e.Supervisor,
                                          DepartamentoActual = e.DepenActual,
                                          DependenciaActual = e.DepenActual,
                                          EmpleadoId = v.empleadoid

                                      }).Where(x => x.EmpleadoId == id).FirstOrDefault();


            return new ViewAsPdf("ReporteActualizarDatos", ActualizacionDatos)
            {
                PageSize = Rotativa.Options.Size.Letter
            };
            //}
            //catch (Exception ex)
            //{
            //    ex.ToString();

            //    TempData["Index"] = "Ha ocurrido un error, al procesar su solicitud";
            //    return RedirectToAction("Index", "Home");

            //}

        }


        //DescripcionCorreoActualizacion
        #region ObtenerEtiquetaHTML
        public string EnviarCorreoRRHH(string Link)
        {

            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/ActualizacionDatos.html");


            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", "ACTUALIZACIÓN DE DATOS EMPLEADOS");
            //Archivo = Archivo.Replace("$departamento", Departamento);
            //Archivo = Archivo.Replace("$nombres", NombreCompleto);
            // Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            return Archivo;
        }
        #endregion



        // GET: VISTA_EMPLEADOS/Details/5
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ActualizarDatos()
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }

            string id = Session["empleadoId"].ToString();
            int Codigo = int.Parse(Session["empleadoId"].ToString());
            ViewBag.EmpleadoId = id;

            var Empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id).FirstOrDefault();

            var EmpleadoComplemento1 = db.EmpleadoComplemento.Where(x => x.EmpleadoId == Codigo).FirstOrDefault();

            if (EmpleadoComplemento1 == null)
            {
                EmpleadoComplemento empleado = new EmpleadoComplemento();
                empleado.EmpleadoId = Codigo;
                empleado.FhCreacion = DateTime.Now;
                empleado.FhModificacion = DateTime.Now;
                empleado.NivelAcademicoId = 3;
                db.EmpleadoComplemento.Add(empleado);
                db.SaveChanges();


                ViewBag.EmpleadoComplemento1 = empleado;

                ViewBag.NivelAcademicoID = new SelectList(db.NivelAcademico.ToList(), "NivelAcademicoId", "Descripcion", empleado.NivelAcademicoId);


            }
            else
            {

                ViewBag.NivelAcademicoID = new SelectList(db.NivelAcademico.ToList(), "NivelAcademicoId", "Descripcion", EmpleadoComplemento1.NivelAcademicoId);

                ViewBag.EmpleadoComplemento1 = EmpleadoComplemento1;
            }


            //Calcular Tiempo Laborando
            //DateCalculations dateCalculations = new DateCalculations(Empleados.fechaingreso, intra.Controllers.PermisosController.fechasVacaciones);
            //string tiempoLaborando = dateCalculations.ObtenerTextoTiempoLaborando();

            //ViewBag.Tiempo = tiempoLaborando;


            return View(Empleados);


        }

        //Seleccionar Cargos
   
        [HttpPost]
        public JsonResult SeleccionarCargos(int DepartamentoId, string dependencia2, int dependencia3)
        {
            int dependencia = 0;
            string dependenciaCargo = dependencia2;

            if (dependencia3 != 0 )
            {
                dependencia = dependencia3;

            }
            else
            {
                
                var dependencia1 = db.vw_Dependencias.Where(x => x.Descripcion == dependenciaCargo).FirstOrDefault();
                dependencia = dependencia1.DependenciaID;

            }

            var Cargos = db.vw_CargosDepto.Where(x => x.DeptoID == DepartamentoId && x.DependenciaID == dependencia).ToList();
            return Json(Cargos);
        }


        [HttpPost]
        public JsonResult SeleccionMunicipio (int provinciaId)
        {
            var Municipios = db.vw_Municipios.Where(x => x.ProvinciaId == provinciaId).ToList();
            return Json(Municipios);
        }
        //Seleccionar distrito
        [HttpPost]
        public JsonResult SeleccionarDistricto(int MunicipioId)
        {
            var Distrito = db.vw_Distritos.Where(x => x.MunicipioId == MunicipioId).ToList();
            return Json(Distrito);
        }

        //Seleccionar Ciudad
        [HttpPost]
        public JsonResult SeleccionarCiudad(int DistritoId)
        {
            var ciudad = db.vw_Ciudad.Where(x => x.DistritoId == DistritoId).ToList();
            return Json(ciudad);
        }
        //Sector
        [HttpGet]
        public JsonResult SeleccionarSector(int CiudaId)
        {
            var Sectores = db.vw_Sectores.Where(x => x.CiudadId == CiudaId).ToList();
            return Json(Sectores,JsonRequestBehavior.AllowGet);
        }
        //Seleccionar Departamento
        [HttpPost]
        public JsonResult SeleccionarDepartamento(int DependenciaId)
        {
            var Departamento = db.vw_Departamentos.Where(x => x.DependenciasID == DependenciaId.ToString()).ToList();
            return Json(Departamento);
        }

        //ACTUALIZAR DATOS DE EMPLEADOS

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarDatos(EmpleadoIntermedio empleo, FormCollection fc)
        {


            if (empleo == null)
            {
                return Json(new { success = false, Mensaje = "Su codigo de empleado es invalido" }, JsonRequestBehavior.AllowGet);
            }

            int codigoEmpleado = int.Parse(Session["empleadoId"].ToString());
            string codigo = codigoEmpleado.ToString();
            var empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == codigo).FirstOrDefault();


            var datos = new EmpleadoIntermedio();
            datos.EmpleadoCodigo = codigoEmpleado;
            datos.Direccion = empleo.Direccion;
            datos.Telefono1 = empleo.Telefono1;
            datos.Celular = empleo.Celular;
            datos.Sector = fc["Sector"];
            datos.Email = empleo.Email;
            datos.Telefono2 = fc["Telefono2"];
            datos.EstadoCivil = empleo.EstadoCivil;
            datos.FhCreacion = DateTime.Now;
            db.EmpleadoIntermedio.Add(datos);
            db.SaveChanges();
            if (fc["Dependiente"] == "")
            {
                fc["Dependiente"] = "0";
            }

            var DatosEmpleados = db.EmpleadoComplemento.Where(x => x.EmpleadoId == codigoEmpleado).FirstOrDefault();
            ViewBag.NivelAcademicoID = new SelectList(db.NivelAcademico.ToList(), "NivelAcademicoId", "Descripcion", DatosEmpleados.NivelAcademicoId);
            int Grado = int.Parse(fc["NivelAcademicoID"].ToString());
            if (DatosEmpleados == null)
            {
                var EmpleadoComplemento = new EmpleadoComplemento();
                EmpleadoComplemento.EmpleadoId = codigoEmpleado;
                EmpleadoComplemento.AfpId = int.Parse( fc["NombreAfp"]);
                EmpleadoComplemento.ArsId = int.Parse( fc["NombreArs"]);
                EmpleadoComplemento.Dependiente = int.Parse(fc["Dependiente"]);
                EmpleadoComplemento.Facebook = fc["Facebook"];
                EmpleadoComplemento.Instagram = fc["Instagram"];
                EmpleadoComplemento.Twitter = fc["Twitter"];
                EmpleadoComplemento.Profesion = fc["Profesion"];
                EmpleadoComplemento.Observaciones = fc["Observaciones"];
                EmpleadoComplemento.FhCreacion = DateTime.Now;
                EmpleadoComplemento.FhModificacion = DateTime.Now;
                EmpleadoComplemento.NivelAcademicoId = Grado;
                db.EmpleadoComplemento.Add(EmpleadoComplemento);
                db.SaveChanges();
            }
            else
            {
                DatosEmpleados.EmpleadoId = codigoEmpleado;
                DatosEmpleados.AfpId = int.Parse(fc["NombreAfp"]);
                DatosEmpleados.ArsId = int.Parse(fc["NombreArs"]);
                DatosEmpleados.Dependiente = int.Parse(fc["Dependiente"]);
                DatosEmpleados.Facebook = fc["Facebook"];
                DatosEmpleados.Instagram = fc["Instagram"];
                DatosEmpleados.Twitter = fc["Twitter"];
                DatosEmpleados.Profesion = fc["Profesion"];
                DatosEmpleados.Observaciones = fc["Observaciones"];
                DatosEmpleados.FhCreacion = DateTime.Now;
                DatosEmpleados.FhModificacion = DateTime.Now;
                DatosEmpleados.NivelAcademicoId = Grado;
                db.SaveChanges();
            }

            //if (fc["Departamento"] == "1" || fc["Cargo1"] == "1" || fc["Supervisor"] == "1")
            //{

            //    int empleadoId = datos.EmpleadoCodigo;

            //    var ACTUAL = db.EmpleadoActualizarDatos.Where(x => x.EmpleadoCodigo == empleadoId).FirstOrDefault();
            //    if (ACTUAL != null)
            //    {
            //        ACTUAL.EmpleadoCodigo = empleadoId;
            //        ACTUAL.CargoActual = fc["Cargo2"];
            //        ACTUAL.DeptoActual = fc["Departamento2"];
            //        ACTUAL.DepenActual = fc["Dependencia2"];

            //        if (fc["Cargo2"] == "")
            //        {
            //            ACTUAL.CargoActual = empleados.cargo.ToUpper();

            //        }

            //        if (fc["Departamento2"] == "")
            //        {
            //            ACTUAL.DeptoActual = empleados.departamento.ToUpper();
            //        }
            //        if (fc["Dependencia2"] == "")
            //        {
            //            ACTUAL.DepenActual = empleados.dependencia.ToUpper();
            //        }

            //        db.SaveChanges();
            //    }
            //    else
            //    {
            //        EmpleadoActualizarDatos Actualizar = new EmpleadoActualizarDatos();
            //        Actualizar.EmpleadoCodigo = empleadoId;
            //        Actualizar.CargoActual = fc["Cargo2"];
            //        Actualizar.DeptoActual = fc["Departamento2"];
            //        Actualizar.DepenActual = fc["Dependencia2"];
            //        if (fc["Cargo2"] == "")
            //        {
            //            Actualizar.CargoActual = empleados.cargo.ToUpper();

            //        }

            //        if (fc["Departamento2"] == "")
            //        {
            //            Actualizar.DeptoActual = empleados.departamento.ToUpper();
            //        }
            //        if (fc["Dependencia2"] == "")
            //        {
            //            Actualizar.DepenActual = empleados.dependencia.ToUpper();
            //        }


            //        db.EmpleadoActualizarDatos.Add(Actualizar);
            //        db.SaveChanges();
            //    }
            //    var update = db.EmpleadoActualizarDatos.OrderByDescending(x => x.EmpleadoCodigo).FirstOrDefault();

            //    // //ENVIO CORREO A TODOS
            //    string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/MiPerfil/ReporteActualizacion?id={codigoEmpleado}";
            //    CorreoActualizacionDatos("maria.rodriguez@pgr.gob.do", "ACTUALIZACIÓN DATOS EMPLEADOS", EnviarCorreoRRHH(url));


            //}



            return Json(new { success = true, Mensaje = "Datos Actualizados Correctamente" }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CrearDependientes(string[] DataEdad, string[] DataEstadoCivil/*, bool[] DataGenero*/, string[] DataParentesco, ActualizacionDependientes Dependientes)
        {
            int CodigoEmpleado = int.Parse(Session["empleadoId"].ToString());
            if (DataEdad == null || DataEstadoCivil == null)
            {
                return Json(new { success = false, Mensaje = "Debe seleccionar al menos un dependiente para Guardar" });
            }

            Dependientes.Estado = true;

            for (int i = 0; i < DataParentesco.Length; i++)
            {

                Dependientes.EmpleadoId = CodigoEmpleado;
                Dependientes.EstadoCivil = int.Parse(DataEstadoCivil[i]);
                //Dependientes.Genero = DataGenero[i];
                Dependientes.Edad = Convert.ToByte(DataEdad[i]);
                Dependientes.ParentescoId = int.Parse(DataParentesco[i]);
                db.ActualizacionDependientes.Add(Dependientes);
                db.SaveChanges();

            }

            var dependientes = db.ActualizacionDependientes.Where(x => x.EmpleadoId == CodigoEmpleado && x.Estado).Count();

            var empleadoComplemento = db.EmpleadoComplemento.Where(x => x.EmpleadoId == CodigoEmpleado).FirstOrDefault();
            empleadoComplemento.Dependiente = dependientes;
            db.SaveChanges();

            return Json(new { success = true, Mensaje = "Datos Guardados Correctamente", Cantidad = dependientes });
        }

        [CustomAuthorize]
        public JsonResult ListaDependientes(int? id)
        {
            id = int.Parse(Session["empleadoId"].ToString());

            var dependientes = (from a in db.ActualizacionDependientes
                                join p in db.Parentesco on a.ParentescoId equals p.ParentescoId
                                where a.EmpleadoId == id && a.Estado

                                select new vActualizacionDependiente
                                {
                                    DependienteId = a.DependienteId,
                                    //Genero = a.Genero == false ? "Masculino" : "Femenino",
                                    EstadoCivil = a.EstadoCivil == 1 ? "Casado(a)" : a.EstadoCivil == 2 ? "Divorciado(a)" : a.EstadoCivil == 3 ? "Soltero(a)" : "Viudo(a)",
                                    ParentescoId = p.Descripcion,
                                    Edad = a.Edad.ToString()

                                }).ToList();

            return Json(dependientes);
        }

        public JsonResult EliminarDependientes(int IdDependiente)
        {
            var dependiente = db.ActualizacionDependientes.Find(IdDependiente);

            dependiente.Estado = false;

            db.SaveChanges();

            var dependientes = db.ActualizacionDependientes.Where(x => x.EmpleadoId == dependiente.EmpleadoId && x.Estado).Count();

            return Json(new { success = true, Mensaje = "Se elimino el dependiente exitosamente", Cantidad = dependientes });
        }


        // GET: ActualizarDatos
        [AllowAnonymous]
        public ActionResult Index()
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }

            string id = Session["empleadoId"].ToString();
            int Codigo = int.Parse(Session["empleadoId"].ToString());
            ViewBag.EmpleadoId = id;

            try { 
            var Empleados = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id).FirstOrDefault();
            var EmpleadoComplemento1 = db.EmpleadoComplemento.Where(x => x.EmpleadoId == Codigo).FirstOrDefault();

            ViewBag.DependenciaID = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");
            ViewBag.DeptoID = new SelectList(db.vw_Departamentos.ToList(), "DeptoID", "Descripcion");
            ViewBag.CargoId = new SelectList(db.vw_CargosDepto.ToList(), "CargoId", "Cargos");
            ViewBag.AfpId = new SelectList(db.AFP.ToList(), "AfpId", "Descripcion");
            ViewBag.ArsId = new SelectList(db.ARS.ToList(), "ArsId", "Descripcion");


            if (EmpleadoComplemento1 == null)
            {
                EmpleadoComplemento empleado = new EmpleadoComplemento();
                empleado.EmpleadoId = Codigo;
                empleado.FhCreacion = DateTime.Now;
                empleado.FhModificacion = DateTime.Now;
                empleado.NivelAcademicoId = 3;
                db.EmpleadoComplemento.Add(empleado);
                db.SaveChanges();


                ViewBag.EmpleadoComplemento1 = empleado;

                ViewBag.NivelAcademicoID = new SelectList(db.NivelAcademico.ToList(), "NivelAcademicoId", "Descripcion", empleado.NivelAcademicoId);
                //ViewBag.DistritoId = 1;
                //ViewBag.ID_Ciudad = 1;
                //ViewBag.ID_Municipio = 1;
                //ViewBag.ProvinciaId = 1;
                //ViewBag.id_sector = 1;
                //ViewBag.AfpId = 1;
                //ViewBag.ArsId = 1;
             
            }
            else
            {

                ViewBag.NivelAcademicoID = new SelectList(db.NivelAcademico.ToList(), "NivelAcademicoId", "Descripcion", EmpleadoComplemento1.NivelAcademicoId);

                ViewBag.EmpleadoComplemento1 = EmpleadoComplemento1;

                //ViewBag.CiudadId = new SelectList(db.vw_Ciudad.ToList(), "CiudadId", "CiudadDescripcion", EmpleadoComplemento1.CiudadId);
                //ViewBag.DistritoId = new SelectList(db.vw_Distritos.ToList(), "DistritoId", "DistritoDescripcion",EmpleadoComplemento1.DistritoId);
                //ViewBag.MunicipioId = new SelectList(db.vw_Municipios.ToList(), "MunicipioId", "MunicipioDescripcion", EmpleadoComplemento1.MunicipioId);
                //ViewBag.ProvinciaId = new SelectList(db.vw_Provinces.ToList(), "ProvinciaId", "ProvinciaDescripcion", EmpleadoComplemento1.ProvinciaId);
                //ViewBag.SectorId = new SelectList(db.vw_Sectores.ToList(), "SectorId", "SectorDescripcion", EmpleadoComplemento1.SectorId);     
                ViewBag.AfpId = new SelectList(db.AFP.ToList(), "AfpId", "Descripcion", EmpleadoComplemento1.AfpId);
                ViewBag.ArsId = new SelectList(db.ARS.ToList(), "ArsId", "Descripcion", EmpleadoComplemento1.ArsId);
                ViewBag.ID_EstadoCivil = new SelectList(db.EstadosCiviles.ToList(), "ID_EstadoCivil", "Descripcion", Empleados.EstadoCivil);
            }

                return View(Empleados);

            } catch(Exception ex)
            {
                ex.ToString();

                TempData["Index"] = "Ha ocurrido un error, al procesar su solicitud";
                return RedirectToAction("Index", "Home");

            }


           
        }

        // GET: ActualizarDatos/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VISTA_EMPLEADOS vISTA_EMPLEADOS = db.VISTA_EMPLEADOS.Find(id);
            if (vISTA_EMPLEADOS == null)
            {
                return HttpNotFound();
            }
            return View(vISTA_EMPLEADOS);
        }

        // GET: ActualizarDatos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ActualizarDatos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "empleadoid,nombre,telefono1,cedula,cargoid,departamentoid,fechaingreso,id_persona,cargo,dependencia,dependenciaid,departamento,salario,estatus,Sexo,Direccion,Cumpleano,EstadoCivil,Sector,Email,TiposSangre,Telefono2,Celular,Profesion,Dependiente,ARS,AFP,Facebook,Instagram,Twitter,Observaciones,NivelAcademicoId")] VISTA_EMPLEADOS vISTA_EMPLEADOS)
        {
            if (ModelState.IsValid)
            {
                db.VISTA_EMPLEADOS.Add(vISTA_EMPLEADOS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vISTA_EMPLEADOS);
        }

        // GET: ActualizarDatos/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VISTA_EMPLEADOS vISTA_EMPLEADOS = db.VISTA_EMPLEADOS.Find(id);
            if (vISTA_EMPLEADOS == null)
            {
                return HttpNotFound();
            }
            return View(vISTA_EMPLEADOS);
        }

        // POST: ActualizarDatos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "empleadoid,nombre,telefono1,cedula,cargoid,departamentoid,fechaingreso,id_persona,cargo,dependencia,dependenciaid,departamento,salario,estatus,Sexo,Direccion,Cumpleano,EstadoCivil,Sector,Email,TiposSangre,Telefono2,Celular,Profesion,Dependiente,ARS,AFP,Facebook,Instagram,Twitter,Observaciones,NivelAcademicoId")] VISTA_EMPLEADOS vISTA_EMPLEADOS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vISTA_EMPLEADOS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vISTA_EMPLEADOS);
        }

        // GET: ActualizarDatos/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VISTA_EMPLEADOS vISTA_EMPLEADOS = db.VISTA_EMPLEADOS.Find(id);
            if (vISTA_EMPLEADOS == null)
            {
                return HttpNotFound();
            }
            return View(vISTA_EMPLEADOS);
        }

        // POST: ActualizarDatos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            VISTA_EMPLEADOS vISTA_EMPLEADOS = db.VISTA_EMPLEADOS.Find(id);
            db.VISTA_EMPLEADOS.Remove(vISTA_EMPLEADOS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private void AgregarActividades(string[] Seleccionados, EmpleadoComplemento EmpleadoComplemento)
        {
            if (Seleccionados == null)
                Seleccionados = new string[] { };

            var tiposelectionadosHS = new HashSet<string>(Seleccionados);
            var marcaTipos = new HashSet<int>(EmpleadoComplemento.ActualizacionActividad.Select(t => t.ActividadId));

            foreach (var item in db.ActualizacionActividad)
            {
                if (tiposelectionadosHS.Contains(item.ActividadId.ToString()))
                {
                    if (!marcaTipos.Contains(item.ActividadId))
                    {
                        EmpleadoComplemento.ActualizacionActividad.Add(item);
                    }
                }
                else
                {
                    if (marcaTipos.Contains(item.ActividadId))
                    {
                        EmpleadoComplemento.ActualizacionActividad.Remove(item);
                    }
                }
            }
            db.SaveChanges();
        }

        public JsonResult ValidarActualizacion() {

            try
            {
                int Codigo = int.Parse(Session["empleadoId"].ToString());

                return Json(db.EmpleadoComplemento.Where(x => x.EmpleadoId == Codigo).Any() ? true : false);
            }
            catch (Exception)
            {
                return Json("error");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
