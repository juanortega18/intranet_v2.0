﻿using intra.Helpers;
using intra.Models.Repositorio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    public class RepositorioController : Controller
    {
        RepositorioDB _database = new RepositorioDB();

        private static int _idSubseccion;
        private static int _idSeccion;
        private static string _nombreSubSeccion;

        // GET: Repositorio
        public ActionResult Index(int idSubSeccion, int idSeccion)
        {
            _idSubseccion = idSubSeccion;
            _idSeccion = idSeccion;

            return View();
        }

        #region INICIAR SISTEMA

        public JsonResult IniciarSistema()
        {
            _verificarExisteCarpetaPrincipal();

            var carpetaPrincipal = (
                from elementoRepositorio
                in _database.ElementoRepositorio
                where 
                    elementoRepositorio.SubSeccionRepositorioId == _idSubseccion
                    &&
                    elementoRepositorio.SeccionRepositorioId == _idSeccion
                select elementoRepositorio
            ).FirstOrDefault();

            _nombreSubSeccion = carpetaPrincipal.ElementoRepositorioNombre;

            return Json(new {
                CarpetaActualID = carpetaPrincipal.ElementoRepositorioId,
                CarpetaActualNombre = carpetaPrincipal.ElementoRepositorioNombre,
                CarpetaActualRuta = carpetaPrincipal.ElementoRepositorioRuta
            });
        }

        #endregion

        #region VERIFICAR EXISTENCIA DE CARPETA PRINCIPAL

        private void _verificarExisteCarpetaPrincipal()
        {
            bool existeCarpetaPrincipal = (
                from elementoRepositorio
                in _database.ElementoRepositorio
                where 
                    elementoRepositorio.SubSeccionRepositorioId == _idSubseccion
                    &&
                    elementoRepositorio.SeccionRepositorioId == _idSeccion
                select elementoRepositorio
            ).Any();

            if (!existeCarpetaPrincipal)
            {
                string carpetaPrincipalNombre = (
                    from subSeccionRepositorio
                    in _database.SubSeccionRepositorio
                    where
                        subSeccionRepositorio.SubSeccionRepositorioId == _idSubseccion
                        &&
                        subSeccionRepositorio.SeccionRepositorioId == _idSeccion
                    select new { subSeccionRepositorio.SubSeccionRepositorioDescripcion }
                ).Single().SubSeccionRepositorioDescripcion;

                ElementoRepositorio carpetaPrincipal = new ElementoRepositorio();
                carpetaPrincipal.SubSeccionRepositorioId = _idSubseccion;
                carpetaPrincipal.ElementoRepositorioRuta = $"{carpetaPrincipalNombre}/";
                carpetaPrincipal.ElementoRepositorioBinario = null;
                carpetaPrincipal.ElementoRepositorioMadreId = null;
                carpetaPrincipal.ElementoRepositorioNombre = carpetaPrincipalNombre;
                carpetaPrincipal.ElementoRepositorioExtension = null;
                carpetaPrincipal.ElementoRepositorioTipo = false;
                carpetaPrincipal.TipoContenido = null;
                carpetaPrincipal.SeccionRepositorioId = _idSeccion;

                _database.ElementoRepositorio.Add(carpetaPrincipal);
                _database.SaveChanges();
            }
        }

        #endregion

        #region CREAR CARPETA

        public JsonResult CrearCarpeta(int carpetaMadreID, string carpetaNombre, string carpetaMadreRuta)
        {
            string mensajeValidacion = ValidarCampo(carpetaNombre, carpetaMadreID);

            if (mensajeValidacion != "OK")
            {
                return Json(new
                {
                    Estatus = false,
                    MensajeValidacion = mensajeValidacion
                });
            }

            carpetaNombre = _verificarExistenciaNombre(carpetaNombre, carpetaMadreID, false);

            ElementoRepositorio elementoRepositorio = new ElementoRepositorio();
            elementoRepositorio.ElementoRepositorioMadreId = carpetaMadreID;
            elementoRepositorio.ElementoRepositorioNombre = carpetaNombre;
            elementoRepositorio.ElementoRepositorioRuta = carpetaMadreRuta.ToUpper();
            elementoRepositorio.ElementoRepositorioTipo = false;

            _database.ElementoRepositorio.Add(elementoRepositorio);
            _database.SaveChanges();

            return Json(new { Estatus = true });
        }

        #endregion

        #region OBTENER ELEMENTOS HIJOS

        public JsonResult ObtenerElementosHijos(int carpetaMadreID)
        {
            var elementosHijos = (
                from elementoRepositorio
                in _database.ElementoRepositorio
                where elementoRepositorio.ElementoRepositorioMadreId == carpetaMadreID
                orderby elementoRepositorio.ElementoRepositorioTipo ascending, elementoRepositorio.ElementoRepositorioNombre
                select elementoRepositorio
            ).ToList();

            List<ElementoHijo> carpetaHijos = new List<ElementoHijo>();

            foreach (var elementoHijo in elementosHijos)
            {
                carpetaHijos.Add(
                    new ElementoHijo
                    {
                        ElementoHijoID = elementoHijo.ElementoRepositorioId,
                        ElementoHijoMadreID = elementoHijo.ElementoRepositorioMadreId,
                        ElementoHijoNombre = elementoHijo.ElementoRepositorioNombre,
                        ElementoHijoExtension = elementoHijo.ElementoRepositorioExtension,
                        ElementoHijoRuta = elementoHijo.ElementoRepositorioRuta,
                        ElementoHijoTipo = elementoHijo.ElementoRepositorioTipo
                    }
                );
            }

            return Json(carpetaHijos);
        }

        #endregion

        #region GUARDAR ARCHIVO

        public JsonResult GuardarArchivo(int carpetaMadreID, string carpetaMadreRuta)
        {
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];

                int archivoTamanoB = file.ContentLength; // Tamaño en bytes
                string archivoNombre = String.Join(".", file.FileName.Split('.').Take(file.FileName.Split('.').Length - 1));
                string archivoExtension = file.FileName.Split('.')[file.FileName.Split('.').Length - 1];

                System.IO.Stream archivoContenido = file.InputStream;
                BinaryReader binaryReader = new BinaryReader(archivoContenido);
                byte[] archivoBinario = binaryReader.ReadBytes(archivoTamanoB);

                TipoContenido tipoContenido = new TipoContenido();
                tipoContenido.TipoContenidoDescripcion = file.ContentType;

                ElementoRepositorio elementoRepositorio = new ElementoRepositorio();
                elementoRepositorio.ElementoRepositorioMadreId = carpetaMadreID;
                elementoRepositorio.ElementoRepositorioNombre = _verificarExistenciaNombre(archivoNombre, carpetaMadreID, true);
                elementoRepositorio.ElementoRepositorioRuta = carpetaMadreRuta;
                elementoRepositorio.ElementoRepositorioTipo = true;
                elementoRepositorio.ElementoRepositorioExtension = archivoExtension;
                elementoRepositorio.ElementoRepositorioBinario = archivoBinario;
                elementoRepositorio.TipoContenidoId = tipoContenido.TipoContenidoId;

                _database.TipoContenido.Add(tipoContenido);
                _database.ElementoRepositorio.Add(elementoRepositorio);
                _database.SaveChanges();
            }

            return Json(new { Estatus = true });
        }

        #endregion

        #region RENOMBRAR ELEMENTO

        public JsonResult RenombrarElemento(int elementoID, string elementoNombre, string elementoNuevoNombre, string elementoRuta, int elementoMadreID)
        {
            string mensajeValidacion = ValidarCampo(elementoNuevoNombre, elementoMadreID, elementoID);

            if (mensajeValidacion != "OK")
            {
                return Json(new
                {
                    Estatus = false,
                    MensajeValidacion = mensajeValidacion
                });
            }

            bool elementoTipo = (
                from elementoRepositorio
                in _database.ElementoRepositorio
                where elementoRepositorio.ElementoRepositorioId == elementoID
                select elementoRepositorio.ElementoRepositorioTipo
            ).FirstOrDefault().Value;

            elementoNuevoNombre = _verificarExistenciaNombre(elementoNuevoNombre, elementoMadreID, elementoTipo);

            var elementosHijos = (
                from elementoRepositorio
                in _database.ElementoRepositorio
                where
                    elementoRepositorio.ElementoRepositorioRuta.Contains(elementoRuta)
                select elementoRepositorio
            ).ToList();

            foreach (var elementoHijo in elementosHijos) {
                elementoHijo.ElementoRepositorioRuta = elementoHijo.ElementoRepositorioRuta.Replace(elementoNombre, elementoNuevoNombre.ToUpper());
            }

            var elementoPadre = (
                 from elementoRepositorio
                 in _database.ElementoRepositorio
                 where
                     elementoRepositorio.ElementoRepositorioId == elementoID
                 select elementoRepositorio
             ).FirstOrDefault();

            elementoPadre.ElementoRepositorioNombre = elementoNuevoNombre;

            _database.SaveChanges();

            return Json(new {
                Estatus = true,
                ElementoNombre = elementoPadre.ElementoRepositorioNombre
            });
        }

        #endregion

        #region ELIMINAR ELEMENTO

        public JsonResult EliminarElemento(int elementoID, string elementoNombre, string elementoRuta)
        {
            elementoRuta = elementoRuta + $"{elementoNombre}/";

            var elementosHijos = (
                from elementoRepositorio
                in _database.ElementoRepositorio
                where
                    elementoRepositorio.ElementoRepositorioRuta.Contains(elementoRuta)
                select elementoRepositorio
            ).ToList();

            var elementoPadre = (
                from elementoRepositorio
                in _database.ElementoRepositorio
                where
                    elementoRepositorio.ElementoRepositorioId == elementoID
                select elementoRepositorio
            ).FirstOrDefault();

            foreach (var elementoHijo in elementosHijos)
            {
                var tipoContenido = (
                    from tipo
                    in _database.TipoContenido
                    where tipo.TipoContenidoId == elementoHijo.TipoContenidoId
                    select tipo
                ).FirstOrDefault();

                _database.TipoContenido.Remove(tipoContenido);
                _database.ElementoRepositorio.Remove(elementoHijo);
            }

            if (elementoPadre != null)
            {
                _database.ElementoRepositorio.Remove(elementoPadre);
            }

            _database.SaveChanges();

            return Json(new { Estatus = true });
        }

        #endregion

        #region DESCARGAR ARCHIVO
        
        public FileResult DescargarArchivo(int archivoID)
        {
            var archivo = (
                from elementoRepositorio
                in _database.ElementoRepositorio
                where 
                    elementoRepositorio.ElementoRepositorioId == archivoID
                select elementoRepositorio
            ).FirstOrDefault();

            return File(archivo.ElementoRepositorioBinario, archivo.TipoContenido.TipoContenidoDescripcion, $"{archivo.ElementoRepositorioNombre}.{archivo.ElementoRepositorioExtension}");
        }

        #endregion

        #region VALIDAR CAMPO

        public string ValidarCampo(string elementoNombre, int elementoMadreID, int elementoID = 0)
        {
            // VALORES DEL ESTATUS
            // 0 : vacío.
            // 1 : carácter slash (/) no se permite.
            // 2 : no se permite nombrar elemento igual que la división.
            // 3 : nombre demasiado largo.
            // 4 : no se permite nombrar elementos con el mismo nombre de departamento.
            // 5 : todo correcto.

            elementoNombre = elementoNombre.Trim();

            int estatus = 5;

            string[] mensajes = new string[] {
                "!El campo está vacío!",
                "!No se permite poner \"/\" en el nombre!",
                "!El nombre no puede ser igual que el nombre de su división!",
                "¡El nombre es demasiado largo!",
                "¡No se permite nombrar un elemento con el mismo nombre del departamento!",
                "OK"
            };

            string patron = @"[\/]";

            Match tieneSlash = Regex.Match(elementoNombre, patron);

            string nombreDivision = "DESARROLLO DE SISTEMAS";

            if (String.IsNullOrEmpty(elementoNombre))
            {
                estatus = 0;
            }
            else if (tieneSlash.Success)
            {
                estatus = 1;
            }
            else if (elementoNombre.ToUpper() == nombreDivision)
            {
                estatus = 2;
            }
            else if (elementoNombre.Length > 100)
            {
                estatus = 3;
            }
            else if (elementoNombre.ToLower() == _nombreSubSeccion.ToLower())
            {
                estatus = 4;
            }

            return mensajes[estatus];
        }

        #endregion

        #region VERIFICAR SI EXISTE NOMBRE DE ELEMENTO

        private string _verificarExistenciaNombre(string elementoNombre, int carpetaMadreID, bool elementoTipo)
        {
            bool existeNombre = (
                from elementoRepositorio
                in _database.ElementoRepositorio
                where
                    elementoRepositorio.ElementoRepositorioMadreId == carpetaMadreID
                    &&
                    elementoRepositorio.ElementoRepositorioTipo == elementoTipo
                select elementoRepositorio
            ).Any(x => x.ElementoRepositorioNombre == elementoNombre);

            if (!existeNombre)
            {
                return elementoNombre;
            }

            List<string> elementosNombres = (
                from elementoRepositorio
                in _database.ElementoRepositorio
                where
                    elementoRepositorio.ElementoRepositorioMadreId == carpetaMadreID
                    &&
                    elementoRepositorio.ElementoRepositorioTipo == elementoTipo
                select elementoRepositorio.ElementoRepositorioNombre
            ).ToList();

            int contador = 1;

            string patron = @"^[a-z ]+[0-9]+$";
            Match patronValido = Regex.Match(elementoNombre.ToLower(), patron);

            if (patronValido.Success)
            {
                elementoNombre = elementoNombre.Split(' ')[0];
            }

            foreach (string nombre in elementosNombres)
            {
                if(elementoNombre.ToLower() == nombre.ToLower() || $"{elementoNombre.ToLower()} {contador}" == nombre.ToLower())
                {
                    contador++;
                }
            }

            elementoNombre = $"{elementoNombre} {contador}";

            return elementoNombre;
        }

        #endregion
    }
}