﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using System.Data.SqlClient;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using Microsoft.Ajax.Utilities;
using intra.Models.RolesUsuarios;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
    public class UsuariosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        [Authorize(Roles="RU,RUA,RUWR")]
        public ActionResult Index()
        {
            var Usuarios = (from u in db.Usuarios
                            from v in db.VISTA_EMPLEADOS
                            where u.UsuarioCreadoPor == v.empleadoid
                            select new
                            {
                                UsuarioId = u.UsuarioId,
                                Usuario = u.UsuarioLogin,
                                CreadoPor = v.nombre,
                                Fecha = u.UsuarioFechaCreacion

                            }).ToList().Select(x => new Usuarios
                            {
                                UsuarioId = x.UsuarioId,
                                UsuarioLogin = x.Usuario,
                                UsuarioCreadoPor = x.CreadoPor,
                                UsuarioFechaCreacion = x.Fecha

                            }).ToList();

            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Index de Usuarios )");


            return View(Usuarios);
        }

        //llamada Ajax Para Cargar Dinamicamente Usuarios
        public JsonResult GetUsuarios(string term)
        {
            List<string> ulpad;

            //ulpad = db.VistaLdap.Where(x => x.Usuario.StartsWith(term)).Select(y => y.Usuario).Take(5).ToList();
            ulpad = db.VISTA_EMPLEADOS.Where(x => x.nombre.StartsWith(term)||x.empleadoid.StartsWith(term)).Select(y => y.nombre).Take(15).ToList();
            if (ulpad.Count <= 0)
            {
                ulpad.Add("No Existe Este Usuario");
            }

            return Json(ulpad, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Create()
        {
            ViewBag.Categorias = db.Categorias.ToList();
            ViewBag.Grupos = db.Grupos.ToList();
            ViewBag.Roles = (from Roles in db.Roles
                             select new
                             {
                                 RolId = Roles.RolId,
                                 Rol = Roles.RolNombre,
                                 Descripcion = Roles.RolDescripcion,
                                 Grupo = Roles.GrupoId

                             }).ToList().Select(x => new RolesSet
                             {
                                 RolId = x.RolId,
                                 RolDescripcion = x.Descripcion,
                                 RolNombre = x.Rol,
                                 GrupoId = x.Grupo
                             }).ToList();



            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Create(FormCollection fC, string[] checkbox)
        {
            Usuarios us = new Usuarios();
            #region Carga de Checkbox
            ViewBag.Categorias = db.Categorias.ToList();
            ViewBag.Grupos = db.Grupos.ToList();
            ViewBag.Roles = (from Roles in db.Roles
                             select new
                             {
                                 RolId = Roles.RolId,
                                 Rol = Roles.RolNombre,
                                 Descripcion = Roles.RolDescripcion,
                                 Grupo = Roles.GrupoId

                             }).ToList().Select(x => new RolesSet
                             {
                                 RolId = x.RolId,
                                 RolDescripcion = x.Descripcion,
                                 RolNombre = x.Rol,
                                 GrupoId = x.Grupo
                             }).ToList();
            #endregion

            bool Save = false;
            string usuario = string.Empty;
            string msjNotFound = string.Empty;

            usuario = fC["UsuarioLogin"];

            msjNotFound = "No Existe Este Usuario";//Esto Captura el mensaje de notfound que envia JSON

            if (string.IsNullOrEmpty(usuario)) {
                ViewBag.Error = "El Campo Usuario Es Requerido";
                ModelState.Clear();
                return View();
            }

            if (string.Compare(usuario, msjNotFound, true) == 0)
            {
                ViewBag.ErrorNotFound = "No se encontro registro de este usuario, Consultar al Administrador de Sistemas";
                ModelState.Clear();
                return View();
            }

            var empl = db.VISTA_EMPLEADOS.Where(x => x.nombre == usuario).FirstOrDefault();

            if (string.IsNullOrEmpty(empl.cedula) || empl.cedula.Length < 13)
            {
                ViewBag.Error = "Este Usuario No Contiene una Cedula Valida Registrada, Consulte al Administrador de Sistemas";
                ModelState.Clear();
                return View();
            }

            var ifExit = db.Usuarios.Where(x => x.UsuarioEmpleadoId == empl.empleadoid).ToList();

            if (ifExit.Count > 0)
            {
                ViewBag.Error = "Este Usuario ya tiene Grupos Asignados, por favor diríjase a (Usuarios) y de Click en Informacion " ;
                ModelState.Clear();
                return View();
            }

            SqlParameter parametro = new SqlParameter("ulpad", empl.cedula);
            var ulpad = db.sp_users_ldapID.SqlQuery(" sp_users_ldapID @ulpad", parametro).FirstOrDefault();

            if (ulpad != null)
            {
                us.UsuarioLogin = ulpad.samAccountName;
                us.UsuarioFechaCreacion = DateTime.Now;
                us.UsuarioCreadoPor = HttpContext.Session["empleadoId"].ToString();
                us.UsuarioEmpleadoId = empl.empleadoid;

                if (ModelState.IsValid)
                {
                    db.Usuarios.Add(us);
                    db.SaveChanges();                   
                    Save = true;
                }
                else
                {
                    ViewBag.Error = "Error, No pudo ser Agregado el Usuario";
                    ModelState.Clear();
                    return View();
                }
            }
            else
            {
                ViewBag.ErrorNotFound = "Usuario No Existe en el Active Directory, Consultar al Administrador de Sistemas";
                ModelState.Clear();
                return View();
            }

            if (Save) {

                GruposUsuarios gpu = new GruposUsuarios();
                var idUsuario = db.Usuarios.Where(x => x.UsuarioLogin == us.UsuarioLogin).Select(x => x.UsuarioId).FirstOrDefault();

                for (int i = 0; i <= checkbox.Length - 1; i++)
                {
                    if (checkbox[i] != "false")
                    {
                        gpu.UsuarioId = idUsuario;
                        gpu.GrupoId = Convert.ToInt16(checkbox[i]);
                        gpu.SistemaId = 1; 

                        if (ModelState.IsValid)
                        {
                            db.GruposUsuarios.Add(gpu);
                            db.SaveChanges();
                            Save = true;
                        }
                    }
                }
            }

            if (Save)
            {

                ViewBag.Succes = "Usuario Cargado Con Exito";
                ModelState.Clear();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Agrego Roles al Usuario (" + us.UsuarioEmpleadoId + ")");
                return View();
            }

            ViewBag.Error = "Error, No pudo ser Agregado el Usuario";
            ModelState.Clear();
            return View();
        }

        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Details(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "El Identificador(ID) Es una referencia Nula, Verifique el parametro enviado");
            }

            #region Informacion del Usuario
            var Usuarios = (from u in db.Usuarios
                            from v in db.VISTA_EMPLEADOS
                            where u.UsuarioEmpleadoId == v.empleadoid
                            where u.UsuarioId == id
                            select new
                            {
                                UsuarioId = u.UsuarioId,
                                Usuario = u.UsuarioLogin,
                                Nombre = v.nombre,
                                empId = v.empleadoid,
                                Departamento = v.departamento,
                                Cargo = v.cargo,

                            }).ToList().Select(x => new InfoUsuario
                            {
                                UsuarioId = x.UsuarioId,
                                UsuarioLogin = x.Usuario,
                                Nombre = x.Nombre,
                                EmpleadoId = x.empId,
                                Departamento = x.Departamento,
                                Cargo = x.Cargo

                            }).FirstOrDefault();
            #endregion

            #region Informacion detalle de Los Grupos x Categorias y Roles Asociados del Usuario

            var Ct = (from c in db.Categorias
                      join g in db.Grupos on c.CategoriaId equals g.CategoriaId
                      join gu in db.GruposUsuarios on g.GrupoId equals gu.GrupoId
                      where gu.UsuarioId == id
                      select new
                      {
                          Categoria = c.CategoriaNombre,
                          Descripcion = c.CategoriaDescripcion,
                          Id = c.CategoriaId

                      }).Distinct().ToList().Select(x => new Categorias
                      {
                          CategoriaNombre = x.Categoria,
                          CategoriaDescripcion = x.Descripcion,
                          CategoriaId = x.Id
                      }).ToList();

            ViewBag.Categorias = Ct;

            var Gp = (from gp in db.GruposUsuarios
                      join g in db.Grupos on gp.GrupoId equals g.GrupoId
                      where gp.UsuarioId == id
                      select new
                      {
                          Grupo = g.GrupoNombre,
                          Descripcion = g.GrupoDescripcion,
                          Id = g.GrupoId,
                          CatId = g.CategoriaId
                      }).ToList().Select(x => new Grupos
                      {

                          GrupoNombre = x.Grupo,
                          GrupoDescripcion = x.Descripcion,
                          GrupoId = x.Id,
                          CategoriaId = x.CatId
                      }).ToList();

            ViewBag.Grupos = Gp;

            var Rl = (from gp in db.GruposUsuarios
                      join r in db.Roles on gp.GrupoId equals r.GrupoId
                      where gp.UsuarioId == id
                      select new
                      {
                          Roles = r.RolNombre,
                          Descripcion = r.RolDescripcion,
                          Id = r.GrupoId
                      }).ToList().Select(x => new RolesSet
                      {
                          RolNombre = x.Roles,
                          RolDescripcion = x.Descripcion,
                          GrupoId = x.Id

                      }).ToList();

            ViewBag.Roles = Rl;

            #endregion

            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Consulto Informacion del Usuario id (" + id + ")");

            if (Usuarios == null)
            {
                return HttpNotFound("Este Usuario no Fue encontrado en la Base de Datos, Verifique la informacion y Vuelva a Consultar");
            }

            return View(Usuarios);
        }

        [CustomAuthorize]
        [Authorize(Roles = "RUA")]
        public ActionResult Asociar(int id) {

            Usuarios usuario = db.Usuarios.Find(id);

            #region Carga de Checkbox
            ViewBag.Categorias = db.Categorias.ToList();
            ViewBag.Grupos = db.Grupos.ToList();
            ViewBag.Roles = (from Roles in db.Roles
                             select new
                             {
                                 RolId = Roles.RolId,
                                 Rol = Roles.RolNombre,
                                 Descripcion = Roles.RolDescripcion,
                                 Grupo = Roles.GrupoId

                             }).ToList().Select(x => new RolesSet
                             {
                                 RolId = x.RolId,
                                 RolDescripcion = x.Descripcion,
                                 RolNombre = x.Rol,
                                 GrupoId = x.Grupo
                             }).ToList();
            #endregion
            ViewBag.GruposUsuarios = (from gu in db.GruposUsuarios
                                      where gu.UsuarioId == id
                                      select new { Grupoid = gu.GrupoId }).ToList().Select(x => new GruposUsuarios { GrupoId = x.Grupoid }).ToArray();
            ViewBag.Succes = "Se han asoiado con Exito los Grupos";
            return View(usuario);
        }


        [HttpPost]
        [CustomAuthorize]
        [Authorize(Roles = "RUA")]
        public ActionResult Asociar(FormCollection fC, string[] checkbox)
        {
            bool save = false;
            GruposUsuarios gpu = new GruposUsuarios();
            int idUsuario = Convert.ToInt16(fC["idUsuario"]);

            for (int i = 0; i <= checkbox.Length - 1; i++)
            {
                if (checkbox[i] != "false")
                {
                    gpu.UsuarioId = idUsuario;
                    gpu.GrupoId = Convert.ToInt16(checkbox[i]);
                    gpu.SistemaId = 1;

                    if (ModelState.IsValid)
                    {
                        db.GruposUsuarios.Add(gpu);
                        db.SaveChanges();
                        save = true;
                    }
                }
            }
            if (save)
            {
                ViewBag.Succes = "Se han asoiado con Exito los Grupos";
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Asocio otros roles al usuario id (" + idUsuario + ")");
                return RedirectToRoute(idUsuario);
            }

            ViewBag.Error = "Error, No se Pudo Asociar Grupos";

            return RedirectToRoute(idUsuario);
        }


        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuarios usuarios = db.Usuarios.Find(id);
            if (usuarios == null)
            {
                return HttpNotFound();
            }
            return View(usuarios);
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Edit([Bind(Include = "UsuarioId,UsuarioLogin,UsuarioFechaCreacion,UsuarioCreadoPor")] Usuarios usuarios)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuarios).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito Informacion del Usuario (" + usuarios.UsuarioEmpleadoId + ")");
                return RedirectToAction("Index");
            }
            return View(usuarios);
        }

        [CustomAuthorize]
        [Authorize(Roles = "RUA")]
        public ActionResult Delete(int usId,int gpId)
        {
            if (usId==0 && gpId==0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var Busqueda = db.GruposUsuarios.FirstOrDefault(x => x.UsuarioId == usId && x.GrupoId == gpId);

            if (Busqueda == null)
            {
                return HttpNotFound();
            }
            return View(Busqueda);
        }

       
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA")]
        public ActionResult DeleteConfirmed(int usId, int gpId)
        {
           
            if (ModelState.IsValid)
            {

                db.Database.ExecuteSqlCommand("Delete from GruposUsuarios where UsuarioId=@usid and GrupoId=@gpid",
                                                 new SqlParameter("@usid", usId),
                                                 new SqlParameter("gpid", gpId));
                string empId = HttpContext.Session["empleadoId"].ToString();

                RegisterLogs rl = new RegisterLogs(empId, "Elimino el Grupo id ("+gpId+") al Usuario id ("+usId+")");
                db.SaveChanges();

            }

            return Redirect("Details/" + usId);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
