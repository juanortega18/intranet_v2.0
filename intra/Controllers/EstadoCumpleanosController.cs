﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.Cumpleanos;

namespace intra.Controllers
{
    public class EstadoCumpleanosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: EstadoCumpleanos
        public ActionResult Index()
        {
            int cumple = 1;
            var excluir = db.EstadoCumpleanos.Where(x => x.Estado == cumple.ToString());
                           

            return View(excluir);
        }

        [CustomAuthorize]
        // GET: EstadoCumpleanos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoCumpleanos estadoCumpleanos = db.EstadoCumpleanos.Find(id);
            if (estadoCumpleanos == null)
            {
                return HttpNotFound();
            }
            return View(estadoCumpleanos);
        }

        [CustomAuthorize]
        // GET: EstadoCumpleanos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EstadoCumpleanos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdEstado,Estado")] EstadoCumpleanos estadoCumpleanos)
        {
            //if(estadoCumpleanos.Estado == 1)
            //{
            //    estadoCumpleanos.Estado = 0
            //}
            //else
            //{
            //    estadoCumpleanos.Estado = "1";
            //}
            //if (ModelState.IsValid)
            //{
            //    db.EstadoCumpleanos.Add(estadoCumpleanos);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}

            return View();
        }

        [CustomAuthorize]
        // GET: EstadoCumpleanos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoCumpleanos estadoCumpleanos = db.EstadoCumpleanos.Find(id);
            if (estadoCumpleanos == null)
            {
                return HttpNotFound();
            }
            return View(estadoCumpleanos);
        }

        // POST: EstadoCumpleanos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection fc)
        {
            int id = 0;
            id = Convert.ToInt16(fc["IdEstado"]);
            EstadoCumpleanos estadoCumpleanos = db.EstadoCumpleanos.Find(id);
          

            string estado = fc["Estado"];

            if (estado == "1")
            {

                estadoCumpleanos.Estado = "0";
            }
            else
            {
                estadoCumpleanos.Estado = "1";
            }


            if (ModelState.IsValid)
            {
                db.Entry(estadoCumpleanos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estadoCumpleanos);
        }

        [CustomAuthorize]
        // GET: EstadoCumpleanos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoCumpleanos estadoCumpleanos = db.EstadoCumpleanos.Find(id);
            if (estadoCumpleanos == null)
            {
                return HttpNotFound();
            }
            return View(estadoCumpleanos);
        }

        // POST: EstadoCumpleanos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EstadoCumpleanos estadoCumpleanos = db.EstadoCumpleanos.Find(id);
            db.EstadoCumpleanos.Remove(estadoCumpleanos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
