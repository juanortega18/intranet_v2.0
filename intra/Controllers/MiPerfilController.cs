﻿using intra.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models.ActualizacionDatos;
using System.Configuration;
using System.IO;
using Rotativa;
using intra.Models.FlotasExtensiones;
using intra.Models.Nomina;
using intra.Models.GestionHumana.Permisos;
using intra.Models.GestionHumana;
using intra.Models.vw_Models;

namespace intra.Controllers
{
    public class MiPerfilController : Controller
    {
        private dbIntranet db = new dbIntranet();
        private Nomina db2 = new Nomina();
        private DATOSEMPLEADOS db3 = new DATOSEMPLEADOS();
        private Correo correo = new Correo();

        // GET: MiPerfil
        [CustomAuthorize]
        public ActionResult Index()
        {
            string EmpCargo = HttpContext.Session["empleadoId"].ToString();
            int codigoEmpleado = Convert.ToInt32(EmpCargo);
            var DatosEmpleados = db.EmpleadoComplemento.Where(x => x.EmpleadoId == codigoEmpleado).FirstOrDefault();

            ViewBag.NivelAcademicoID = new SelectList(db.NivelAcademico.ToList(), "NivelAcademicoID", "Descripcion");
            // ==========
            // PERMISOS Y VACACIONES
            // ==========COMMENTADO POR MARIA======
            // ==========DESCOMMENTADO POR JUAN====


            ViewBag.FechaActual = DateTime.Now.Date;



            var empleado = db.Vw_Mostrar_Personal_Permisos_PGR.Where(e => e.Codigo == EmpCargo).FirstOrDefault();
            //COMENTADO POR MARIA
            //if (empleado != null)
            //{
            //    var encargadoId = Code.UtilityMethods.buscarEncargado(empleado)?.ToString();
            //    ViewBag.NombreEncargado = db.VISTA_EMPLEADOS.Find(encargadoId)?.nombre;
            //}

            //Maria
            var permisoUsuario = (
                db
                .Permisos
                .Where(x => x.EmpleadoId == EmpCargo && x.TipoLicenciaId == 6)
                .OrderByDescending(x => x.PermisoFechaRegistro)
                .Take(1)
                .FirstOrDefault()
            );

            //ViewBag.UltimoPermiso = permisoUsuario;

            // ==========
            // ==========


            var cargo = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == EmpCargo).FirstOrDefault();
            ViewBag.FechaIngreso = cargo.fechaingreso;
            ViewBag.Cargo = cargo.cargo;




            //Para Mostrar Foto de Perfil                             
            var FotoPerfil = db.MiPerfilAvatars.Where(x => x.codigoEmpleado == EmpCargo).OrderByDescending(x => x.id).Take(1).Select(f => f.avatar).SingleOrDefault();


            //var FotoPerfil = db.MiPerfilAvatars.Where(x => x.codigoEmpleado == EmpCargo).Select(f => f.avatar).SingleOrDefault();

            //ViewBag.image.Replace("~/", "");

            if (!string.IsNullOrEmpty(FotoPerfil))
            {
                ViewBag.FotoPerfil = FotoPerfil;
            }


            //COMENTADO DE PONCHE
            //List<sp_report_ponches_Result> ponches = new List<sp_report_ponches_Result>();

            //using (RHLogsEntities1 db2 = new RHLogsEntities1())
            //{
            //    var fechaInicio = Utilities.EmbeddedDate();

            //    ponches = db2.Database.SqlQuery<sp_report_ponches_Result>(string.Format("exec sp_report_ponches '{0}','{1}','{2}','{3}','{4}','{5}','{6}'",
            //        fechaInicio.Date.ToString("yyyy-MM-dd"), DateTime.Now.Date.ToString("yyyy-MM-dd"), 0, 0, 0, 0, Convert.ToInt32(EmpCargo))).OrderBy(o => o.Fecha_Ponche).ToList();
            //}
            //using (RHLogsEntities1 db2 = new RHLogsEntities1())
            //{
            //    var fechaInicio = Utilities.EmbeddedDate();

            //    ponches = db2.Database.SqlQuery<sp_report_ponches_Result>(string.Format("exec sp_report_ponches '{0}','{1}','{2}','{3}','{4}','{5}','{6}'",
            //        fechaInicio.Date.ToString("yyyy-MM-dd"), DateTime.Now.Date.ToString("yyyy-MM-dd"), 0, 0, 0, 0, Convert.ToInt32(EmpCargo))).OrderBy(o => o.Fecha_Ponche).ToList();
            //}

            MiPerfil mpf = new MiPerfil();

            //mpf.ponches = ponches;

            Salario(mpf);

            //Comentado hoy

            //int cod_empleado = Convert.ToInt32(EmpCargo);

            //var is_incharge = db.Database.SqlQuery<sp_verif_Encargados_Result>("sp_verif_Encargados {0}", cod_empleado).FirstOrDefault();

            //if (is_incharge.Nombre != "N/A" && is_incharge.Procedencia != "N/A")
            //{
            //    ViewBag.AuthorizeCharge = "YES";
            //}
            //else
            //{
            //    ViewBag.AuthorizeCharge = "NO";   
            //}

            RegisterLogs rl = new RegisterLogs(EmpCargo, "Ingreso a MiPerfil");

            return View(mpf);
        }

        public void Salario(MiPerfil mi_perfil)
        {
            string EmpCargo = HttpContext.Session["empleadoId"].ToString();
            var empleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == EmpCargo).FirstOrDefault();

            var nomina_value = db2.NominaIntranet.Where(x => x.NominaCedula == empleado.cedula).OrderByDescending(x=> x.NominaId).ToList().
                Select(a => new
                {
                    value = a.NominaId,
                    text = Utilities.GetMonthName(a.NominaMes.Value) + " (" + a.NominaMes.Value + ") - " + a.NominaAnio.Value
                }).ToList();


            mi_perfil.NominaDropDownValues = new SelectList(nomina_value, "value", "text");

            var current_nomina = db2.NominaIntranet.Where(x => x.NominaMes == DateTime.Now.Month && x.NominaAnio == DateTime.Now.Year && x.NominaCedula == empleado.cedula).ToList().LastOrDefault();

            if (current_nomina != null)
            {
                current_nomina.NominaMesNombre = Utilities.GetMonthName(Convert.ToInt32(current_nomina.NominaMes));

                mi_perfil.CurrentNomina = current_nomina;
                mi_perfil.ShowTable = "true";
            }
            else
            {
                mi_perfil.CurrentNomina = new NominaIntranet();
                mi_perfil.ShowTable = "false";
            }
        }
        [CustomAuthorize]
        public PartialViewResult viewFlotasExt()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();

            var FlotaExt = (from f in db.AsignacionFlots
                            where f.AsignacionEmpleadoId == empId
                            select new
                            {
                                Flota = f.AsignacionNoFlota,
                                Ext = f.AsignacionExt

                            }).ToList().Select(x => new AsignacionFlots
                            {
                                AsignacionNoFlota = x.Flota,
                                AsignacionExt = x.Ext

                            }).FirstOrDefault();

            if (FlotaExt != null)
            {
                ViewBag.Flota = FlotaExt.AsignacionNoFlota;
                ViewBag.Extencion = FlotaExt.AsignacionExt;

                return PartialView();
            }

            return PartialView();
        }

        [HttpPost]
        public JsonResult GetNomina(int id)
        {
            try
            {
                var nomina = db2.NominaIntranet.Find(id);
                nomina.NominaMesNombre = Utilities.GetMonthName(nomina.NominaMes.Value);

                return Json(nomina);
            }
            catch (Exception)
            {
                return Json("Error");
            }
        }

        [HttpPost]
        public ActionResult LoadSalarioView()
        {

            return PartialView("Salario");
        }

        public ActionResult _Prioridades()
        {
            string employee = Session["empleadoId"].ToString();

            CargarListas();

            Nmdependencias_Permisos return_object = new Nmdependencias_Permisos();

            var is_incharge = db.Database.SqlQuery<sp_verif_Encargados_Result>("sp_verif_Encargados {0}", employee).FirstOrDefault();

            var status = db.EncargadoStatus.ToList();

            List<int> to_remove = new List<int>();

            if (is_incharge.Nombre != "N/A" && is_incharge.Procedencia != "N/A")
            {
                switch (is_incharge.Procedencia)
                {
                    case "DEPENDENCIA":

                        var dependency = db.Nmdependencias_Permisos.Where(x => x.DirectorID.ToString() == employee).FirstOrDefault();

                        foreach (var item in status)
                        {
                            if (item.EncargadoStatusId == 2)
                            {
                                var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == dependency.DirectorID.Value.ToString()).FirstOrDefault();

                                string name = "";

                                if (employee_object != null)
                                {
                                    name = employee_object.NombreCompleto;
                                }
                                else
                                {
                                    to_remove.Add(item.EncargadoStatusId);
                                }

                                item.EncargadoStatusDescripcion += ": " + name;
                            }
                            else if (item.EncargadoStatusId == 3)
                            {
                                var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == dependency.AuxiliarID.Value.ToString()).FirstOrDefault();

                                string name = "";

                                if (employee_object != null)
                                {
                                    name = employee_object.NombreCompleto;
                                }
                                else
                                {
                                    to_remove.Add(item.EncargadoStatusId);
                                }

                                item.EncargadoStatusDescripcion += ": " + name;
                            }
                        }

                        foreach (var item in to_remove)
                        {
                            var remove_object = status.Where(x => x.EncargadoStatusId == item).FirstOrDefault();
                            status.Remove(remove_object);
                        }

                        ViewBag.CurrentPonchesResponsibles = new SelectList(status.Select(a => new { value = a.EncargadoStatusId, text = a.EncargadoStatusDescripcion }), "value", "text");

                        return_object = dependency;

                        var dependency_activities = db.DependenciaActividad.Where(x => x.DependenciaID == return_object.DependenciaID).ToList();

                        return_object.PoncheStatus = dependency_activities.Where(x => x.ActividadEncargadoID == 1).FirstOrDefault().EncargadoStatusID;

                        return_object.PermisosVacacionesStatus = dependency_activities.Where(x => x.ActividadEncargadoID == 2).FirstOrDefault().EncargadoStatusID;

                        break;

                    case "DEPARTAMENTO":

                        var department = db.Nmdeptos_Permisos.Where(x => x.EncargadoID.Value.ToString() == employee).FirstOrDefault();

                        foreach (var item in status)
                        {
                            if (item.EncargadoStatusId == 2)
                            {
                                var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == department.EncargadoID.Value.ToString()).FirstOrDefault();

                                string name = "";

                                if (employee_object != null)
                                {
                                    name = employee_object.NombreCompleto;
                                }
                                else
                                {
                                    to_remove.Add(item.EncargadoStatusId);
                                }

                                item.EncargadoStatusDescripcion += ": " + name;
                            }
                            else if (item.EncargadoStatusId == 3)
                            {
                                var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == department.AuxiliarID.Value.ToString()).FirstOrDefault();

                                string name = "";

                                if (employee_object != null)
                                {
                                    name = employee_object.NombreCompleto;
                                }
                                else
                                {
                                    to_remove.Add(item.EncargadoStatusId);
                                }

                                item.EncargadoStatusDescripcion += ": " + name;
                            }
                        }

                        foreach (var item in to_remove)
                        {
                            var remove_object = status.Where(x => x.EncargadoStatusId == item).FirstOrDefault();
                            status.Remove(remove_object);
                        }

                        ViewBag.CurrentPonchesResponsibles = new SelectList(status.Select(a => new { value = a.EncargadoStatusId, text = a.EncargadoStatusDescripcion }), "value", "text");

                        return_object.DependenciaID = department.DependenciaID;
                        return_object.DeptoID = department.DeptoID;

                        var department_activities = db.DepartamentoActividad.Where(x => x.DependenciaID == return_object.DependenciaID && x.DepartamentoID == return_object.DeptoID).ToList();

                        return_object.PoncheStatus = department_activities.Where(x => x.ActividadEncargadoID == 1).FirstOrDefault().EncargadoStatusID;

                        return_object.PermisosVacacionesStatus = department_activities.Where(x => x.ActividadEncargadoID == 2).FirstOrDefault().EncargadoStatusID;

                        break;

                    case "DIVISION":

                        var division = db.Nmdivision_Permisos.Where(x => x.EncargadoID.Value.ToString() == employee).FirstOrDefault();

                        foreach (var item in status)
                        {
                            if (item.EncargadoStatusId == 2)
                            {
                                var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == division.EncargadoID.Value.ToString()).FirstOrDefault();

                                string name = "";

                                if (employee_object != null)
                                {
                                    name = employee_object.NombreCompleto;
                                }
                                else
                                {
                                    to_remove.Add(item.EncargadoStatusId);
                                }

                                item.EncargadoStatusDescripcion += ": " + name;
                            }
                            else if (item.EncargadoStatusId == 3)
                            {
                                var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == division.AuxiliarID.Value.ToString()).FirstOrDefault();

                                string name = "";

                                if (employee_object != null)
                                {
                                    name = employee_object.NombreCompleto;
                                }
                                else
                                {
                                    to_remove.Add(item.EncargadoStatusId);
                                }

                                item.EncargadoStatusDescripcion += ": " + name;
                            }
                        }

                        foreach (var item in to_remove)
                        {
                            var remove_object = status.Where(x => x.EncargadoStatusId == item).FirstOrDefault();
                            status.Remove(remove_object);
                        }

                        ViewBag.CurrentPonchesResponsibles = new SelectList(status.Select(a => new { value = a.EncargadoStatusId, text = a.EncargadoStatusDescripcion }), "value", "text");

                        return_object.DependenciaID = division.DependenciaID;
                        return_object.DeptoID = division.DeptoID;
                        return_object.DivisionID = division.DivisionID;

                        var division_activities = db.DivisionActividad.Where(x => x.DependenciaID == return_object.DependenciaID && x.DepartamentoID == return_object.DeptoID && x.DivisionID == return_object.DivisionID).ToList();

                        return_object.PoncheStatus = division_activities.Where(x => x.ActividadEncargadoID == 1).FirstOrDefault().EncargadoStatusID;

                        return_object.PermisosVacacionesStatus = division_activities.Where(x => x.ActividadEncargadoID == 2).FirstOrDefault().EncargadoStatusID;

                        break;

                    case "SECCION":

                        var section = db.NmSecciones_Permisos.Where(x => x.EncargadoID.Value.ToString() == employee).FirstOrDefault();

                        foreach (var item in status)
                        {
                            if (item.EncargadoStatusId == 2)
                            {
                                var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == section.EncargadoID.Value.ToString()).FirstOrDefault();

                                string name = "";

                                if (employee_object != null)
                                {
                                    name = employee_object.NombreCompleto;
                                }
                                else
                                {
                                    to_remove.Add(item.EncargadoStatusId);
                                }

                                item.EncargadoStatusDescripcion += ": " + name;
                            }
                            else if (item.EncargadoStatusId == 3)
                            {
                                var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == section.AuxiliarID.Value.ToString()).FirstOrDefault();

                                string name = "";

                                if (employee_object != null)
                                {
                                    name = employee_object.NombreCompleto;
                                }
                                else
                                {
                                    to_remove.Add(item.EncargadoStatusId);
                                }

                                item.EncargadoStatusDescripcion += ": " + name;
                            }
                        }

                        foreach (var item in to_remove)
                        {
                            var remove_object = status.Where(x => x.EncargadoStatusId == item).FirstOrDefault();
                            status.Remove(remove_object);
                        }

                        ViewBag.CurrentPonchesResponsibles = new SelectList(status.Select(a => new { value = a.EncargadoStatusId, text = a.EncargadoStatusDescripcion }), "value", "text");

                        return_object.DependenciaID = section.DependenciaID;
                        return_object.DeptoID = section.DeptoID;
                        return_object.DivisionID = section.DivisionID;
                        return_object.SeccionID = section.SeccionID;

                        var section_activities = db.SeccionActividad.Where(x => x.DependenciaID == return_object.DependenciaID && x.DepartamentoID == return_object.DeptoID && x.DivisionID == return_object.DivisionID && x.SeccionID == return_object.SeccionID).ToList();

                        return_object.PoncheStatus = section_activities.Where(x => x.ActividadEncargadoID == 1).FirstOrDefault().EncargadoStatusID;

                        return_object.PermisosVacacionesStatus = section_activities.Where(x => x.ActividadEncargadoID == 2).FirstOrDefault().EncargadoStatusID;

                        break;

                    default:
                        break;
                }

                ViewBag.Hierarchy = is_incharge.Procedencia;
            }
            else
            {
                #region Commented Code
                //var is_auxiliar = db.Database.SqlQuery<sp_verif_Auxiliar_Result>("sp_verif_Auxiliar {0}", HttpContext.Session["empleadoId"].ToString()).FirstOrDefault();

                //if (is_auxiliar.Nombre != "N/A" && is_auxiliar.Procedencia != "N/A")
                //{
                //    switch (is_auxiliar.Procedencia)
                //    {
                //        case "DEPENDENCIA":

                //            var dependency = db.Nmdependencias_Permisos.Where(x => x.AuxiliarID.ToString() == employee).FirstOrDefault();

                //            foreach (var item in status)
                //            {
                //                if (item.EncargadoStatusId == 2)
                //                {
                //                    var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == dependency.DirectorID.Value.ToString()).FirstOrDefault();

                //                    string name = "";

                //                    if (employee_object != null)
                //                    {
                //                        name = employee_object.NombreCompleto;
                //                    }
                //                    else
                //                    {
                //                        to_remove.Add(item.EncargadoStatusId);
                //                    }

                //                    item.EncargadoStatusDescripcion += ": " + name;
                //                }
                //                else if (item.EncargadoStatusId == 3)
                //                {
                //                    var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == dependency.AuxiliarID.Value.ToString()).FirstOrDefault();

                //                    string name = "";

                //                    if (employee_object != null)
                //                    {
                //                        name = employee_object.NombreCompleto;
                //                    }
                //                    else
                //                    {
                //                        to_remove.Add(item.EncargadoStatusId);
                //                    }

                //                    item.EncargadoStatusDescripcion += ": " + name;
                //                }
                //            }

                //            foreach (var item in to_remove)
                //            {
                //                var remove_object = status.Where(x => x.EncargadoStatusId == item).FirstOrDefault();
                //                status.Remove(remove_object);
                //            }

                //            ViewBag.CurrentPonchesResponsibles = new SelectList(status.Select(a => new { value = a.EncargadoStatusId, text = a.EncargadoStatusDescripcion }), "value", "text");

                //            return_object = dependency;

                //            break;

                //        case "DEPARTAMENTO":

                //            var department = db.Nmdeptos_Permisos.Where(x => x.EncargadoID.Value.ToString() == employee).FirstOrDefault();

                //            foreach (var item in status)
                //            {
                //                if (item.EncargadoStatusId == 2)
                //                {
                //                    var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == department.EncargadoID.Value.ToString()).FirstOrDefault();

                //                    string name = "";

                //                    if (employee_object != null)
                //                    {
                //                        name = employee_object.NombreCompleto;
                //                    }
                //                    else
                //                    {
                //                        to_remove.Add(item.EncargadoStatusId);
                //                    }

                //                    item.EncargadoStatusDescripcion += ": " + name;
                //                }
                //                else if (item.EncargadoStatusId == 3)
                //                {
                //                    var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == department.AuxiliarID.Value.ToString()).FirstOrDefault();

                //                    string name = "";

                //                    if (employee_object != null)
                //                    {
                //                        name = employee_object.NombreCompleto;
                //                    }
                //                    else
                //                    {
                //                        to_remove.Add(item.EncargadoStatusId);
                //                    }

                //                    item.EncargadoStatusDescripcion += ": " + name;
                //                }
                //            }

                //            foreach (var item in to_remove)
                //            {
                //                var remove_object = status.Where(x => x.EncargadoStatusId == item).FirstOrDefault();
                //                status.Remove(remove_object);
                //            }

                //            ViewBag.CurrentPonchesResponsibles = new SelectList(status.Select(a => new { value = a.EncargadoStatusId, text = a.EncargadoStatusDescripcion }), "value", "text");

                //            return_object.DependenciaID = department.DependenciaID;
                //            return_object.DeptoID = department.DeptoID;

                //            break;

                //        case "DIVISION":

                //            var division = db.Nmdivision_Permisos.Where(x => x.EncargadoID.Value.ToString() == employee).FirstOrDefault();

                //            foreach (var item in status)
                //            {
                //                if (item.EncargadoStatusId == 2)
                //                {
                //                    var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == division.EncargadoID.Value.ToString()).FirstOrDefault();

                //                    string name = "";

                //                    if (employee_object != null)
                //                    {
                //                        name = employee_object.NombreCompleto;
                //                    }
                //                    else
                //                    {
                //                        to_remove.Add(item.EncargadoStatusId);
                //                    }

                //                    item.EncargadoStatusDescripcion += ": " + name;
                //                }
                //                else if (item.EncargadoStatusId == 3)
                //                {
                //                    var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == division.AuxiliarID.Value.ToString()).FirstOrDefault();

                //                    string name = "";

                //                    if (employee_object != null)
                //                    {
                //                        name = employee_object.NombreCompleto;
                //                    }
                //                    else
                //                    {
                //                        to_remove.Add(item.EncargadoStatusId);
                //                    }

                //                    item.EncargadoStatusDescripcion += ": " + name;
                //                }
                //            }

                //            foreach (var item in to_remove)
                //            {
                //                var remove_object = status.Where(x => x.EncargadoStatusId == item).FirstOrDefault();
                //                status.Remove(remove_object);
                //            }

                //            ViewBag.CurrentPonchesResponsibles = new SelectList(status.Select(a => new { value = a.EncargadoStatusId, text = a.EncargadoStatusDescripcion }), "value", "text");

                //            return_object.DependenciaID = division.DependenciaID;
                //            return_object.DeptoID = division.DeptoID;
                //            return_object.DivisionID = division.DivisionID;

                //            break;

                //        case "SECCION":

                //            var section = db.NmSecciones_Permisos.Where(x => x.EncargadoID.Value.ToString() == employee).FirstOrDefault();

                //            foreach (var item in status)
                //            {
                //                if (item.EncargadoStatusId == 2)
                //                {
                //                    var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == section.EncargadoID.Value.ToString()).FirstOrDefault();

                //                    string name = "";

                //                    if (employee_object != null)
                //                    {
                //                        name = employee_object.NombreCompleto;
                //                    }
                //                    else
                //                    {
                //                        to_remove.Add(item.EncargadoStatusId);
                //                    }

                //                    item.EncargadoStatusDescripcion += ": " + name;
                //                }
                //                else if (item.EncargadoStatusId == 3)
                //                {
                //                    var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == section.AuxiliarID.Value.ToString()).FirstOrDefault();

                //                    string name = "";

                //                    if (employee_object != null)
                //                    {
                //                        name = employee_object.NombreCompleto;
                //                    }
                //                    else
                //                    {
                //                        to_remove.Add(item.EncargadoStatusId);
                //                    }

                //                    item.EncargadoStatusDescripcion += ": " + name;
                //                }
                //            }

                //            foreach (var item in to_remove)
                //            {
                //                var remove_object = status.Where(x => x.EncargadoStatusId == item).FirstOrDefault();
                //                status.Remove(remove_object);
                //            }

                //            ViewBag.CurrentPonchesResponsibles = new SelectList(status.Select(a => new { value = a.EncargadoStatusId, text = a.EncargadoStatusDescripcion }), "value", "text");

                //            return_object.DependenciaID = section.DependenciaID;
                //            return_object.DeptoID = section.DeptoID;
                //            return_object.DivisionID = section.DivisionID;
                //            return_object.SeccionID = section.SeccionID;

                //            break;

                //        default:
                //            break;
                //    }


                //    ViewBag.Hierarchy = is_incharge.Procedencia;
                //}
                //else
                //{
                //return RedirectToAction("NoAccess", "Nmdependencias_Permisos");
                #endregion
                ViewBag.Hierarchy = "N/A";
                //}
            }

            return PartialView(return_object);
        }

        public ActionResult NoAccess()
        {
            return View();
        }

        [HttpPost]
        public JsonResult VerifyBelow(string dependency, string department, string division)
        {
            try
            {
                if (string.IsNullOrEmpty(department) && string.IsNullOrEmpty(division))
                {
                    var below_departments = db.Nmdeptos_Permisos.Where(x => x.DependenciaID.ToString() == dependency).ToList();

                    if (below_departments != null && below_departments.Count > 0)
                    {
                        return Json("Yes");
                    }
                    else
                    {
                        return Json("No");
                    }

                }
                else if (string.IsNullOrEmpty(division))
                {
                    var below_division = db.Nmdivision_Permisos.Where(x => x.DependenciaID.ToString() == dependency && x.DeptoID.ToString() == department).ToList();

                    if (below_division != null && below_division.Count > 0)
                    {
                        return Json("Yes");
                    }
                    else
                    {
                        return Json("No");
                    }
                }
                else if (!string.IsNullOrEmpty(dependency) && !string.IsNullOrEmpty(department) && !string.IsNullOrEmpty(division))
                {
                    var below_section = db.NmSecciones_Permisos.Where(x => x.DependenciaID.ToString() == dependency && x.DeptoID.ToString() == department && x.DivisionID.ToString() == division).ToList();

                    if (below_section != null && below_section.Count > 0)
                    {
                        return Json("Yes");
                    }
                    else
                    {
                        return Json("No");
                    }
                }
                else
                {
                    return Json("Error");
                }
            }
            catch (Exception)
            {
                return Json("Error");
            }
        }

        public string VerifyBelow2(string dependency, string department, string division)
        {
            try
            {
                if (string.IsNullOrEmpty(department) && string.IsNullOrEmpty(division))
                {
                    var below_departments = db.Nmdeptos_Permisos.Where(x => x.DependenciaID.ToString() == dependency).ToList();

                    if (below_departments != null && below_departments.Count > 0)
                    {
                        return "Yes";
                    }
                    else
                    {
                        return "No";
                    }

                }
                else if (string.IsNullOrEmpty(division))
                {
                    var below_division = db.Nmdivision_Permisos.Where(x => x.DependenciaID.ToString() == dependency && x.DeptoID.ToString() == department).ToList();

                    if (below_division != null && below_division.Count > 0)
                    {
                        return "Yes";
                    }
                    else
                    {
                        return "No";
                    }
                }
                else if (!string.IsNullOrEmpty(dependency) && !string.IsNullOrEmpty(department) && !string.IsNullOrEmpty(division))
                {
                    var below_section = db.NmSecciones_Permisos.Where(x => x.DependenciaID.ToString() == dependency && x.DeptoID.ToString() == department && x.DivisionID.ToString() == division).ToList();

                    if (below_section != null && below_section.Count > 0)
                    {
                        return "Yes";
                    }
                    else
                    {
                        return "No";
                    }
                }
                else
                {
                    return "Error";
                }
            }
            catch (Exception)
            {
                return "Error";
            }
        }

        [HttpPost]
        public JsonResult BringAuthorizedPersonal(string dependency, string department, string division, string section)
        {
            /*Query the catalogue table*/
            var status = db.EncargadoStatus.ToList();

            try
            {
                if (string.IsNullOrEmpty(department) && string.IsNullOrEmpty(division) && string.IsNullOrEmpty(section))
                {
                    var dependency_area = db.Nmdependencias_Permisos.Where(x => x.DependenciaID.ToString() == dependency).FirstOrDefault();
                    List<int> to_remove = new List<int>();

                    foreach (var item in status)
                    {
                        if (item.EncargadoStatusId == 2)
                        {
                            var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == dependency_area.DirectorID.Value.ToString()).FirstOrDefault();

                            string name = "";

                            if (employee_object != null)
                            {
                                name = employee_object.NombreCompleto;
                            }
                            else
                            {
                                to_remove.Add(item.EncargadoStatusId);
                            }

                            item.EncargadoStatusDescripcion += ": " + name;
                        }
                        else if (item.EncargadoStatusId == 3)
                        {
                            var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == dependency_area.AuxiliarID.Value.ToString()).FirstOrDefault();

                            string name = "";

                            if (employee_object != null)
                            {
                                name = employee_object.NombreCompleto;
                            }
                            else
                            {
                                to_remove.Add(item.EncargadoStatusId);
                            }

                            item.EncargadoStatusDescripcion += ": " + name;
                        }
                    }

                    foreach (var item in to_remove)
                    {
                        var remove_object = status.Where(x => x.EncargadoStatusId == item).FirstOrDefault();
                        status.Remove(remove_object);
                    }
                }
                else if (string.IsNullOrEmpty(division) && string.IsNullOrEmpty(section))
                {
                    var department_area = db.Nmdeptos_Permisos.Where(x => x.DependenciaID.ToString() == dependency && x.DeptoID.ToString() == department).FirstOrDefault();
                    List<int> to_remove = new List<int>();

                    foreach (var item in status)
                    {
                        if (item.EncargadoStatusId == 2)
                        {
                            var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == department_area.EncargadoID.Value.ToString()).FirstOrDefault();

                            string name = "";

                            if (employee_object != null)
                            {
                                name = employee_object.NombreCompleto;
                            }
                            else
                            {
                                to_remove.Add(item.EncargadoStatusId);
                            }

                            item.EncargadoStatusDescripcion += ": " + name;
                        }
                        else if (item.EncargadoStatusId == 3)
                        {
                            var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == department_area.AuxiliarID.Value.ToString()).FirstOrDefault();

                            string name = "";

                            if (employee_object != null)
                            {
                                name = employee_object.NombreCompleto;
                            }
                            else
                            {
                                to_remove.Add(item.EncargadoStatusId);
                            }

                            item.EncargadoStatusDescripcion += ": " + name;
                        }
                    }

                    foreach (var item in to_remove)
                    {
                        var remove_object = status.Where(x => x.EncargadoStatusId == item).FirstOrDefault();
                        status.Remove(remove_object);
                    }
                }
                else if (string.IsNullOrEmpty(section))
                {
                    var division_area = db.Nmdivision_Permisos.Where(x => x.DependenciaID.ToString() == dependency && x.DeptoID.ToString() == department && x.DivisionID.ToString() == division).FirstOrDefault();
                    List<int> to_remove = new List<int>();

                    foreach (var item in status)
                    {
                        if (item.EncargadoStatusId == 2)
                        {
                            var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == division_area.EncargadoID.Value.ToString()).FirstOrDefault();

                            string name = "";

                            if (employee_object != null)
                            {
                                name = employee_object.NombreCompleto;
                            }
                            else
                            {
                                to_remove.Add(item.EncargadoStatusId);
                            }

                            item.EncargadoStatusDescripcion += ": " + name;
                        }
                        else if (item.EncargadoStatusId == 3)
                        {
                            var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == division_area.AuxiliarID.Value.ToString()).FirstOrDefault();

                            string name = "";

                            if (employee_object != null)
                            {
                                name = employee_object.NombreCompleto;
                            }
                            else
                            {
                                to_remove.Add(item.EncargadoStatusId);
                            }

                            item.EncargadoStatusDescripcion += ": " + name;
                        }
                    }

                    foreach (var item in to_remove)
                    {
                        var remove_object = status.Where(x => x.EncargadoStatusId == item).FirstOrDefault();
                        status.Remove(remove_object);
                    }
                }
                else if (!string.IsNullOrEmpty(dependency) && !string.IsNullOrEmpty(department) && !string.IsNullOrEmpty(division) && !string.IsNullOrEmpty(section))
                {
                    var section_area = db.NmSecciones_Permisos.Where(x => x.DependenciaID.ToString() == dependency && x.DeptoID.ToString() == department && x.DivisionID.ToString() == division && x.SeccionID.ToString() == section).FirstOrDefault();
                    List<int> to_remove = new List<int>();

                    foreach (var item in status)
                    {
                        if (item.EncargadoStatusId == 2)
                        {
                            var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == section_area.EncargadoID.Value.ToString()).FirstOrDefault();

                            string name = "";

                            if (employee_object != null)
                            {
                                name = employee_object.NombreCompleto;
                            }
                            else
                            {
                                to_remove.Add(item.EncargadoStatusId); ;
                            }

                            item.EncargadoStatusDescripcion += ": " + name;
                        }
                        else if (item.EncargadoStatusId == 3)
                        {
                            var employee_object = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == section_area.AuxiliarID.Value.ToString()).FirstOrDefault();

                            string name = "";

                            if (employee_object != null)
                            {
                                name = employee_object.NombreCompleto;
                            }
                            else
                            {
                                to_remove.Add(item.EncargadoStatusId);
                            }

                            item.EncargadoStatusDescripcion += ": " + name;
                        }
                    }

                    foreach (var item in to_remove)
                    {
                        var remove_object = status.Where(x => x.EncargadoStatusId == item).FirstOrDefault();
                        status.Remove(remove_object);
                    }
                }
                else
                {
                    return Json("Error");
                }
            }
            catch (Exception ex)
            {
                var m = ex.Message;

                return Json("Error");
            }

            return Json(status);
        }

        public void CargarListas()
        {
            ViewBag.Dependencia = new SelectList(db.vw_Dependencias.ToList().Select(obj => new
            {
                DependenciaID = obj.DependenciaID,
                Descripcion = obj.Descripcion,
                DirectorID = (obj.DirectorID == null ? "" : obj.DirectorID.ToString())
            }), "DependenciaID", "Descripcion");

            ViewBag.Depto = new SelectList(db.vw_Departamentos.ToList().Select(obj => new
            {
                DeptoID = obj.DeptoID,
                Descripcion = obj.Descripcion,
                EncargadoID = (obj.EncargadoID == null ? "" : obj.EncargadoID.ToString())
            }), "DeptoID", "Descripcion");

            ViewBag.Division = new SelectList(db.vw_Divisiones.ToList().Select(obj => new
            {
                DivisionID = obj.DivisionID,
                DeptoID = obj.DeptoID,
                Descripcion = obj.Descripcion,
                EncargadoID = (obj.EncargadoID == null ? "" : obj.EncargadoID.ToString())
            }), "DeptoID", "Descripcion");

            ViewBag.Seccion = new SelectList(db.vw_Secciones.ToList().Select(obj => new
            {
                SeccionID = obj.SeccionID,
                DivisionID = obj.DivisionID,
                DeptoID = obj.DeptoID,
                Descripcion = obj.Descripcion,
                EncargadoID = (obj.EncargadoID == null ? "" : obj.EncargadoID.ToString())
            }), "DeptoID", "Descripcion");
        }

        [HttpPost]
        public JsonResult Area(string dependency, string department, string division)
        {
            try
            {
                if (string.IsNullOrEmpty(department) && string.IsNullOrEmpty(division))
                {
                    var department_list = db.vw_Departamentos.Where(x => x.DependenciasID == dependency).Select(a => new
                    {
                        value = a.DeptoID,
                        text = a.Descripcion
                    }).ToList();

                    return Json(department_list);
                }
                else if (string.IsNullOrEmpty(division))
                {
                    var division_list = db.vw_Divisiones.Where(x => x.DependenciaID.ToString() == dependency && x.DeptoID.ToString() == department).Select(a => new
                    {
                        value = a.DivisionID,
                        text = a.Descripcion
                    }).ToList();

                    return Json(division_list);
                }
                else if (!string.IsNullOrEmpty(dependency) && !string.IsNullOrEmpty(department) && !string.IsNullOrEmpty(division))
                {
                    var section_list = db.vw_Secciones.Where(x => x.DependenciaID.ToString() == dependency && x.DeptoID == department && x.DivisionID.ToString() == division).Select(a => new
                    {
                        value = a.SeccionID,
                        text = a.Descripcion
                    }).ToList();

                    return Json(section_list);
                }
                else
                {
                    return Json("Error");
                }
            }
            catch (Exception)
            {
                return Json("Error");
            }
        }

        public JsonResult SaveMyArea(string ponches_values, string permisos_vacaciones_values)
        {
            int employee = Convert.ToInt32(Session["empleadoId"].ToString());

            try
            {
                var in_charge = db.Database.SqlQuery<sp_verif_Encargados_Result>("sp_verif_Encargados {0}", employee).FirstOrDefault();

                if (in_charge.Nombre != "N/A" && in_charge.Procedencia != "N/A")
                {
                    switch (in_charge.Procedencia)
                    {
                        case "DEPENDENCIA":

                            var dependency = db.Nmdependencias_Permisos.Where(x => x.DirectorID == employee).FirstOrDefault();

                            if (!string.IsNullOrEmpty(ponches_values) && !string.IsNullOrEmpty(permisos_vacaciones_values))
                            {
                                dependency.PoncheStatus = Convert.ToInt32(ponches_values);
                                dependency.PermisosVacacionesStatus = Convert.ToInt32(permisos_vacaciones_values);

                                db.Entry(dependency).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();

                                return Json("Success");
                            }
                            else
                            {
                                return Json("Error");
                            }

                        case "DEPARTAMENTO":

                            var department = db.Nmdeptos_Permisos.Where(x => x.EncargadoID == employee).FirstOrDefault();

                            if (!string.IsNullOrEmpty(ponches_values) && !string.IsNullOrEmpty(permisos_vacaciones_values))
                            {
                                department.PoncheStatus = Convert.ToInt32(ponches_values);
                                department.PermisosVacacionesStatus = Convert.ToInt32(permisos_vacaciones_values);

                                db.Entry(department).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();

                                return Json("Success");
                            }
                            else
                            {
                                return Json("Error");
                            }

                        case "DIVISION":

                            var division = db.Nmdivision_Permisos.Where(x => x.EncargadoID == employee).FirstOrDefault();

                            if (!string.IsNullOrEmpty(ponches_values) && !string.IsNullOrEmpty(permisos_vacaciones_values))
                            {
                                division.PoncheStatus = Convert.ToInt32(ponches_values);
                                division.PermisosVacacionesStatus = Convert.ToInt32(permisos_vacaciones_values);

                                db.Entry(division).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();

                                return Json("Success");
                            }
                            else
                            {
                                return Json("Error");
                            }

                        case "SECCION":

                            var section = db.NmSecciones_Permisos.Where(x => x.EncargadoID == employee).FirstOrDefault();

                            if (!string.IsNullOrEmpty(ponches_values) && !string.IsNullOrEmpty(permisos_vacaciones_values))
                            {
                                section.PoncheStatus = Convert.ToInt32(ponches_values);
                                section.PoncheStatus = Convert.ToInt32(permisos_vacaciones_values);

                                db.Entry(section).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();

                                return Json("Success");
                            }
                            else
                            {
                                return Json("Error");
                            }

                        default:
                            return Json("Error");
                    }

                }
                else
                {
                    return Json("Error");
                }
            }
            catch (Exception)
            {
                return Json("Error");
            }
        }

        #region NotWorking
        //----NOT WORKING----------------------------------------------------------------------------------

        //[CustomAuthorize]
        public ActionResult Index2()
        {
            return View();
        }

        public ActionResult Salario_copy()
        {
            return View();
        }
        #endregion
    }
}