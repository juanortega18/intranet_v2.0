﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Models;
using System.Data.SqlClient;
using System.Configuration;
using intra.Code;
using intra.Models.GestionHumana;
using System.Data.Entity.SqlServer;
using Newtonsoft.Json;
using System.Globalization;
using Rotativa;
using Rotativa.Options;
using intra.Models.Almacen;
using intra.Models.GestionHumana.Permisos;



namespace intra.Controllers
{
    public class PermisosController : Controller
    {
        private dbIntranet db = new dbIntranet();
        private AlmacenEntities dbAlmacen = new AlmacenEntities();
        
        //Variable WebConfig
        public static string Direccion = ConfigurationManager.AppSettings["URLINTRA"];
        internal static List<DateTime[]> fechasVacaciones;

        [CustomAuthorize]
        // GET: Permisos
        public ActionResult Index()
        {
            string IdEmpleado = HttpContext.Session["empleadoId"].ToString();
            int empleado = Convert.ToInt32(IdEmpleado);
            var EsEncargadoInfo = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format( "sp_verif_Encargados {0}" , 0, empleado)).FirstOrDefault();
            bool EsEncargado = EsEncargadoInfo.Nombre == "N/A" ? false : true;

            if (EsEncargado)
            {
                try
                {
                    var result = db.Permisos.Where(x=>x.EncargadoId.ToString() == IdEmpleado).ToList();
                    
                    foreach (var item in result)
                    {
                        item.TipoLicencia = (from a in db.TipoLicencia
                                             where a.TipoLicenciaId == item.TipoLicenciaId
                                             select a).FirstOrDefault();

                        item.PermisosEstados = (from b in db.PermisosEstados
                                                where b.PermisoEstadoId == item.PermisoEstado
                                                select b).FirstOrDefault();

                        var empleadoNombre = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == item.EmpleadoId).FirstOrDefault();

                        if (empleadoNombre != null)
                            item.EmpleadoNombre = empleadoNombre.NombreCompleto;
                        else
                            item.EmpleadoNombre = item.PermisoUsuario;
                    }
                    return View(result);
                }
                catch (Exception ex)
                {
                    var ListaVacia = new List<Permisos>();
                    ViewBag.Message = "No hay solicitudes a evaluar";
                    return View(ListaVacia);
                }
            }
            else
            {
                return RedirectToAction("Create");
            }
        }
        
        [CustomAuthorize]
        // GET: Permisos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Permisos permisos = db.Permisos.Find(id);
            DiaEstudio diaEstudio;

            if (permisos.TipoLicenciaId == 4)
            {
                diaEstudio = db.DiaEstudio.Where(d => d.DiaEstudioPermisoId == permisos.PermisoId).FirstOrDefault();

                ViewBag.DiaEstudio = diaEstudio;
            }

            if (permisos == null)
            {
                return HttpNotFound();
            }

            #region CODIGO VIEJO QUE BUSCA POR AREA
            //var area_solicitante = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == permisos.EmpleadoId).FirstOrDefault();

            ////Dependencia
            //if(!string.IsNullOrEmpty(area_solicitante.CodigoDependencia) && string.IsNullOrEmpty(area_solicitante.CodigoDepartamento) && 
            //    string.IsNullOrEmpty(area_solicitante.CodigoDivision) && string.IsNullOrEmpty(area_solicitante.CodigoSeccion))
            //{
            //    var es_encargado = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", area_solicitante.Codigo)).FirstOrDefault();

            //    if(es_encargado.Nombre == "N/A" && es_encargado.Procedencia == "N/A")
            //    {
            //        var soy_encargado = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", Session["empleadoId"].ToString())).FirstOrDefault();

            //        if(soy_encargado.Nombre == area_solicitante.Dependencia)
            //        {
            //            return View(permisos);
            //        }
            //    }
            //}
            ////Departamento
            //else if((!string.IsNullOrEmpty(area_solicitante.CodigoDependencia) && !string.IsNullOrEmpty(area_solicitante.CodigoDepartamento) &&
            //    string.IsNullOrEmpty(area_solicitante.CodigoDivision) && string.IsNullOrEmpty(area_solicitante.CodigoSeccion)))
            //{
            //    var es_encargado = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", area_solicitante.Codigo)).FirstOrDefault();

            //    if (es_encargado.Nombre == "N/A" && es_encargado.Procedencia == "N/A")
            //    {
            //        var soy_encargado = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", Session["empleadoId"].ToString())).FirstOrDefault();

            //        if (soy_encargado.Nombre == area_solicitante.Departamento)
            //        {
            //            return View(permisos);
            //        }
            //    }
            //}
            ////Division
            //else if ((!string.IsNullOrEmpty(area_solicitante.CodigoDependencia) && !string.IsNullOrEmpty(area_solicitante.CodigoDepartamento) &&
            //    !string.IsNullOrEmpty(area_solicitante.CodigoDivision) && string.IsNullOrEmpty(area_solicitante.CodigoSeccion)))
            //{
            //    var es_encargado = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", area_solicitante.Codigo)).FirstOrDefault();

            //    if (es_encargado.Nombre == "N/A" && es_encargado.Procedencia == "N/A")
            //    {
            //        var soy_encargado = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", Session["empleadoId"].ToString())).FirstOrDefault();

            //        if (soy_encargado.Nombre == area_solicitante.Division)
            //        {
            //            return View(permisos);
            //        }
            //    }
            //}
            ////Sección
            //else if ((!string.IsNullOrEmpty(area_solicitante.CodigoDependencia) && !string.IsNullOrEmpty(area_solicitante.CodigoDepartamento) &&
            //    !string.IsNullOrEmpty(area_solicitante.CodigoDivision) && !string.IsNullOrEmpty(area_solicitante.CodigoSeccion)))
            //{
            //    var es_encargado = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", area_solicitante.Codigo)).FirstOrDefault();

            //    if (es_encargado.Nombre == "N/A" && es_encargado.Procedencia == "N/A")
            //    {
            //        var soy_encargado = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", Session["empleadoId"].ToString())).FirstOrDefault();

            //        if (soy_encargado.Nombre == area_solicitante.Seccion)
            //        {
            //            return View(permisos);
            //        }
            //    }
            //}
            #endregion

            if (Convert.ToInt32(Session["empleadoId"]) == permisos.EncargadoId)
                return View(permisos);
            else
                return RedirectToAction("Index");
        }

        [CustomAuthorize]
        // GET: Permisos/Create
        public ActionResult Create()
        {
            ViewBag.TipoLicencia = new SelectList(db.TipoLicencia.ToList(), "TipoLicenciaId", "TipoDescripcion");

            //Para información del usuario
            string IdEmpleado = HttpContext.Session["empleadoId"].ToString();

            var Empleado = (from a in db.VISTA_EMPLEADOS
                            where a.empleadoid == IdEmpleado
                            select a).FirstOrDefault();

            ViewBag.FechaIngreso = Empleado.fechaingreso;
            ViewBag.Cargo = Empleado.cargo;

            if (Session["dependenciaId"].ToString() == "10001" && Session["departamentoId"].ToString() == "15")
            {
                ViewBag.Correo = intra.Code.Utilities2.obtenerCorreoUsuario(Session["cedula"].ToString());
            }
            else
            {
                ViewBag.Correo = intra.Code.Utilities2.GetUserName(Session["cedula"].ToString()) + "@pgr.gob.do";
            }

            //Para Mostrar Foto de Perfil                             
            var FotoPerfil = db.MiPerfilAvatars.Where(x => x.codigoEmpleado == IdEmpleado).OrderByDescending(x => x.id).Take(1).Select(f => f.avatar).SingleOrDefault();


            if (!string.IsNullOrEmpty(FotoPerfil))
            {
                ViewBag.FotoPerfil = FotoPerfil;
            }


            RegisterLogs rl = new RegisterLogs(IdEmpleado, "Ingreso a Permisos");

            var empleado = db.Vw_Mostrar_Personal_Permisos_PGR.Where(e => e.Codigo == IdEmpleado).FirstOrDefault();
            var encargadoId = Code.UtilityMethods.buscarEncargado(empleado).ToString();
            string nombreEncargado = db.VISTA_EMPLEADOS.Find(encargadoId).nombre;

            ViewBag.NombreEncargado = nombreEncargado;

            //IdEmpleado = "18096"; // Channa
            //IdEmpleado = "10584"; // Richard

            //ViewBag.Permisos = (
            //    db.Permisos
            //    .Where(x => (x.EmpleadoId == IdEmpleado && x.TipoLicenciaId != 1 && x.TipoLicenciaId != 2) || (x.EmpleadoId == IdEmpleado && (x.TipoLicenciaId == 1 || x.TipoLicenciaId == 2) && x.PermisoEstado == 1))
            //    .OrderBy(x => x.PermisoFechaRegistro)
            //    .ToList()
            //);

            //ViewBag.Vacaciones = (
            //     dbAlmacen
            //     .RHVACACION
            //     .Where(x => x.EmpleadoID == IdEmpleado && x.TipoLicenciaID == 1 && x.TipoLicenciaID == 2)
            //     .OrderBy(x => x.FechaRegistro)
            //     .ToList()
            // );

            var permisoUsuario = (
                db
                .Permisos
                .Where(x => x.EmpleadoId == IdEmpleado && x.TipoLicenciaId == 6)
                .OrderByDescending(x => x.PermisoFechaRegistro)
                .Take(1)
                .FirstOrDefault()
            );

            ViewBag.UltimoPermiso = permisoUsuario;

            return View();
        }
        
        // POST: Permisos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult Create([Bind(Include = "PermisoId,EmpleadoId,PermisoUsuario,TipoLicenciaId,PermisoObservacion,PermisoEstado,PermisoAprobacion,PermisoFechaInicio,PermisoFechaFin,PermisoFechaRegistro,PermisoFechaAprobado")] Permisos permisos)
        {
            ViewBag.TipoLicencia = new SelectList(db.TipoLicencia.ToList(), "TipoLicenciaId", "TipoDescripcion");
            permisos.PermisoFechaRegistro = DateTime.Now;
            string empleadId = HttpContext.Session["empleadoId"].ToString();
            permisos.EmpleadoId = empleadId;
            permisos.PermisoEstado = 1;
            permisos.PermisoUsuario = Session["usuario"].ToString();
            try
            {
                permisos = db.Permisos.Add(permisos);
                db.SaveChanges();
                var Empleado = db.Vw_Mostrar_Personal_Permisos_PGR.Where(e => e.Codigo == empleadId).FirstOrDefault();
                var EncargadoId = Code.UtilityMethods.buscarEncargado(Empleado);
                var cedulaEncargado = (from b in db.VISTA_EMPLEADOS
                              where b.empleadoid == EncargadoId.ToString()
                              select b.cedula).FirstOrDefault();

                //var correoEncargado = db.Database.SqlQuery()

                var EnvioMensaje = Code.UtilityMethods.SendMail
                                   (
                                       HttpRuntime.AppDomainAppPath + "\\EtiquetasHtml\\NotificacionPermisoEncargado.html",
                                       "Solicitud de permisos / vacaciones",
                                       new string[]
                                       {
                                // $tituloMensajeCorreo
                                "Solicitud de permisos / vacaciones",

                                // $nombres
                                HttpContext.Session["nombre"].ToString(),

                                // $tema,
                                "Se ha solicitado un nuevo permiso / vacaciones, para ver más detalles ir al siguiente link: ",
                                 // $pagina
                                HttpRuntime.AppDomainAppVirtualPath  + Url.Action("details", "Permisos", new { id = permisos.PermisoId }),
                                     },  "maria.rodriguez@pgr.gob.do" 
                                   );

                return Json(new { Estatus = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = "ERROR", Message = ex.Message });
            }
        }

        [CustomAuthorize]
        public PartialViewResult vInformacionEmpleado(string id)
        {
            #region Informacion del Usuario -- OLD CODE
            //var Empleado = (from u in db.Permisos
            //                from v in db.VISTA_EMPLEADOS
            //                where u.EmpleadoId == v.empleadoid
            //                where u.PermisoId == id
            //                select new
            //                {
            //                    PermisoId = u.PermisoId,
            //                    Usuario = u.PermisoUsuario,
            //                    Nombre = v.nombre,
            //                    empId = v.empleadoid,
            //                    Departamento = v.departamento,
            //                    Cargo = v.cargo,
            //                    FechaIngreso = v.fechaingreso

            //                }).ToList().Select(x => new InfoUsuario
            //                {
            //                    PermisoId = x.PermisoId,
            //                    Nombre = x.Nombre,
            //                    EmpleadoId = x.empId,
            //                    Departamento = x.Departamento,
            //                    Cargo = x.Cargo,
            //                    FechaIngreso = x.FechaIngreso


            //                }).FirstOrDefault();
            #endregion 

            var Empleado = (from a in db.VISTA_EMPLEADOS
                            where a.empleadoid == id
                            select a).FirstOrDefault();

            if (Empleado.dependenciaid == "10001" && Empleado.departamentoid == "15")
            {
                ViewBag.Correo = intra.Code.Utilities2.obtenerCorreoUsuario(Empleado.cedula);
            }
            else
            {
                ViewBag.Correo = intra.Code.Utilities2.GetUserName(Empleado.cedula) + "@pgr.gob.do";
            }
            
            return PartialView(Empleado);
        }

        [CustomAuthorize]
        // GET: Permisos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.TipoLicencia = new SelectList(db.TipoLicencia.ToList(), "TipoLicenciaId", "TipoDescripcion");

            Permisos permisos = db.Permisos.Find(id);
            if (permisos == null)
            {
                return HttpNotFound();
            }
            return View(permisos);
        }

        // POST: Permisos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PermisoId,EmpleadoId,PermisoUsuario,TipoLicenciaId,PermisoObservacion,PermisoEstado,PermisoAprobacion,PermisoFechaInicio,PermisoFechaFin,PermisoFechaRegistro,PermisoFechaAprobado")] Permisos permisos)
        {
            permisos.PermisoFechaAprobado = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Entry(permisos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(permisos);
        }

        public JsonResult VerificarEstado(int permisoID)
        {
            int estado = db.Permisos.Find(permisoID).PermisoEstado;

            return Json(new { PermisoEstado = estado });
        }

        [CustomAuthorize]
        public PartialViewResult _Permisos(string EmpleadoId)
        {
            var Emp = db.VistaPermisos.Where(c => c.EmpleadoId == EmpleadoId).ToList();

            ViewBag.TiposLicencias = db.TipoLicencia.Where(x => x.TipoLicenciaId != 6 && x.TipoLicenciaId != 8).ToList();

            ViewBag.Permisos = (
                db.Permisos
                .Where(x => x.EmpleadoId == EmpleadoId && x.TipoLicenciaId != 1 && x.TipoLicenciaId != 2)
                .OrderBy(x => x.PermisoFechaRegistro)
                .ToList()
            );

            return PartialView(Emp);
        }

        public JsonResult GuardarPermiso(int tipoPermiso, string _fechaInicio, string _fechaFin, int diaEstudioDiaId, string diaEstudioHoraInicio, string diaEstudioHoraFin, string observacion)
        {
            _fechaInicio = _fechaInicio == "01-01-0001" ? "01-01-0001 00:00 AM" : _fechaInicio;
            _fechaFin = _fechaFin == "01-01-0001" ? "01-01-0001 00:00 AM" : _fechaFin;

            try
            {
                DateTime fechaInicio = UtilityMethods.ConvertirFechaStringADateTime(_fechaInicio);
                DateTime fechaFin = UtilityMethods.ConvertirFechaStringADateTime(_fechaFin);

                #region VALIDACION Y GUARDADO
                var textosErrores = new DateCalculations(Session["empleadoId"].ToString()).ValidadPermisoSolicitud(tipoPermiso, fechaInicio, fechaFin, diaEstudioDiaId, diaEstudioHoraInicio, diaEstudioHoraFin, observacion);

                if (textosErrores.Count() == 0)
                {
                    var empleadoId = Session["empleadoId"].ToString();
                    var empleado = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == empleadoId).FirstOrDefault();

                    var empleadoUsername = "";

                    if (Session["dependenciaId"].ToString() == "10001" && Session["departamentoId"].ToString() == "15")
                        empleadoUsername = Code.Utilities2.obtenerCorreoUsuario(Session["cedula"].ToString());
                    else
                        empleadoUsername = Code.Utilities2.GetUserName(empleado.Cedula);

                    var encargadoId = Code.UtilityMethods.buscarEncargado(empleado).ToString();
                    var encargado = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == encargadoId).FirstOrDefault();

                    var encargadoUsername = "";

                    if (encargado.CodigoDependencia == "10001" && encargado.CodigoDepartamento == "15")
                        encargadoUsername = Code.Utilities2.obtenerCorreoUsuario(encargado.Cedula);
                    else
                        encargadoUsername = Code.Utilities2.GetUserName(encargado.Cedula);

                    var permiso = new Permisos();
                    permiso.EmpleadoId = empleado.Codigo;
                    permiso.PermisoUsuario = empleadoUsername;
                    permiso.TipoLicenciaId = tipoPermiso;
                    permiso.PermisoObservacion = observacion;
                    permiso.PermisoEstado = 1;
                    permiso.PermisoFechaInicio = fechaInicio;
                    permiso.PermisoFechaFin = fechaFin;
                    permiso.PermisoFechaRegistro = DateTime.Now;
                    permiso.PermisoFechaAprobado = null;
                    permiso.EncargadoId = Convert.ToInt32(encargado.Codigo);
                    permiso.UsuarioEncargado = encargadoUsername;

                    db.Permisos.Add(permiso);
                    db.SaveChanges();

                    if (tipoPermiso == 9)
                    {
                        string diaEstudioNombre = "";

                        switch (diaEstudioDiaId)
                        {
                            case 1:
                                diaEstudioNombre = "Lunes";
                                break;
                            case 2:
                                diaEstudioNombre = "Martes";
                                break;
                            case 3:
                                diaEstudioNombre = "Miércoles";
                                break;
                            case 4:
                                diaEstudioNombre = "Jueves";
                                break;
                            case 5:
                                diaEstudioNombre = "Viernes";
                                break;
                        }

                        DiaEstudio diaEstudio = new DiaEstudio();

                        diaEstudio.DiaEstudioPermisoId = permiso.PermisoId;
                        diaEstudio.DiaEstudioNombre = diaEstudioNombre;
                        diaEstudio.DiaEstudioHoraInicio = diaEstudioHoraInicio;
                        diaEstudio.DiaEstudioHoraFin = diaEstudioHoraFin;
                        diaEstudio.DiaEstudioEstado = true;

                        db.DiaEstudio.Add(diaEstudio);
                        db.SaveChanges();
                    }

                    #region ENVIAR CORREO A ENCARGADO

                    var queryObtenerIdUltimoPermiso = (
                        from
                            permisos
                        in
                            db.Permisos
                        where
                            permisos.EmpleadoId == empleado.Codigo
                        orderby
                            permisos.PermisoFechaRegistro descending
                        select
                            permisos.PermisoId
                    );

                    var queryObtenerNombreCompleto = (
                        from
                            vistaEmpleados
                        in
                            db.VISTA_EMPLEADOS
                        where
                            vistaEmpleados.empleadoid == empleado.Codigo
                        select
                            vistaEmpleados.nombre
                    );

                    var archivoHTML = Server.MapPath(@"~/EtiquetasHtml/SolicitudPermisoVacaciones.html");
                    var logoIntranet = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

                    string Logo = "";

                    string LogoDTI = "";

                    string Flecha = "";

                    Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

                    LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

                    Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

                    var permisoID = queryObtenerIdUltimoPermiso.FirstOrDefault();
                    var nombreCompleto = queryObtenerNombreCompleto.FirstOrDefault();

                    // Borrar
                    //empleadoID = "18150"; //Erick
                    //empleadoID = "10259"; //Ana, Soporte Técnico
                    //empleadoID = "18074"; //Javier 
                    //empleadoID = "18917"; //Gabriel 
                    //empleadoID = "2248"; //Deris, Redes

                    var encargadoCorreo = $"{encargadoUsername}@pgr.gob.do";
                    //var encargadoCorreo = $"wilkin.vasquez@pgr.gob.do";

                    var mail = new Mail();

                    var linkSolicitudPermiso = string.Empty;

                    if (HttpRuntime.AppDomainAppVirtualPath.Contains("intra"))
                    {
                        //Prueba
                        linkSolicitudPermiso = $"localhost{HttpRuntime.AppDomainAppVirtualPath}/Permisos/Details?id={permisoID}";
                    }
                    else if(HttpRuntime.AppDomainAppVirtualPath.Contains("PGR"))
                    {
                        //Producción
                        linkSolicitudPermiso = $"http://intranet.pgr.gob.do{HttpRuntime.AppDomainAppVirtualPath}/Permisos/Details?id={permisoID}";
                    }
                    else if (HttpRuntime.AppDomainAppVirtualPath.Contains("CP") || HttpRuntime.AppDomainAppVirtualPath.Contains("cp"))
                    {
                        //Producción
                        linkSolicitudPermiso = $"http://intranet.pgr.gob.do{HttpRuntime.AppDomainAppVirtualPath}/Permisos/Details?id={permisoID}";
                    }

                    var contenido = mail.ObtenerFormatoHTML(Session["departamento"].ToString(), nombreCompleto, "Solicitud permiso", archivoHTML, linkSolicitudPermiso, observacion);
                    
                    mail.EnviarCorreoPermisoVacacion(encargadoCorreo, "Solicitud permiso".ToUpper(), contenido, DateTime.Now.ToString(), Logo, LogoDTI, Flecha);
                    #endregion
                }

                #endregion

                return Json(new { TextosErrores = textosErrores });
            }
            catch(Exception e)
            {
                return Json(new { TextosErroress = e.Message });
            }
        }

        public JsonResult VerificarUltimoPermisoPendiente()
        {
            bool UltimoPermisoPendiente = (
                db.Permisos
                .OrderByDescending(x => x.PermisoFechaRegistro)
                .Any(x => x.PermisoEstado == 1 && x.TipoLicenciaId != 6)
            );

            return Json(new { UltimoPermisoPendiente = UltimoPermisoPendiente });
        }

        public JsonResult CambiarEstado(int value, int permisoID)
        {
            #region CAMBIAR ESTADO DEL PERMISO

            var permiso = db.Permisos.Find(permisoID);

            string nombreEncargado = db.VISTA_EMPLEADOS.Find(Session["empleadoId"]).nombre;
            //string nombreEncargado = db.VISTA_EMPLEADOS.Find("19145").nombre;

            // VALORES DEL VALUE
            // 0 : pendiente o rechazado
            // 1 : aprobado
            if (value == 0)
            {
                permiso.PermisoEstado = 3;
                permiso.PermisoFechaAprobado = DateTime.Now;

                //if (permiso.TipoLicenciaId == 6)
                //{
                //    permiso.PermisoObservacion = $"APROBACION DE { permiso.PermisoObservacion } APROBADO POR {nombreEncargado}.";

                //    db.SaveChanges();

                //    int response = dbAlmacen.Database.ExecuteSqlCommand(string.Format("SP_InsertarPermisos '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}'", permiso.PermisoId, permiso.EmpleadoId, permiso.PermisoUsuario, permiso.TipoLicenciaId, permiso.PermisoObservacion, permiso.PermisoEstado, permiso.PermisoFechaInicio.Value.ToString("yyyy-MM-dd HH:mm:ss"), permiso.PermisoFechaFin.Value.ToString("yyyy-MM-dd HH:mm:ss"), permiso.PermisoFechaRegistro.Value.ToString("yyyy-MM-dd HH:mm:ss"), permiso.PermisoFechaAprobado.Value.ToString("yyyy-MM-dd HH:mm:ss")));
                //}

                // ==========
                // ==========
                // AQUI VA LA LOGICA QUE VA A AGREGAR LA OBSERVASION DEPENDIENDO DEL TIPO DE PERMISO.

                DateCalculations dateCalculations = new DateCalculations();

                string observacion = "";

                if(permiso.TipoLicenciaId == 1)
                {
                    observacion = $"APROBACIÓN DE { permiso.PermisoObservacion } APROBADO POR {nombreEncargado}.";
                }
                else
                {
                    observacion = dateCalculations.ObtenerObservacionPermiso(permisoID);
                }

                // ==========
                // ==========

                db.SaveChanges();

                int response = dbAlmacen.Database.ExecuteSqlCommand(string.Format("SP_InsertarPermisos '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}'", permiso.PermisoId, permiso.EmpleadoId, permiso.PermisoUsuario, permiso.TipoLicenciaId, observacion, permiso.PermisoEstado, permiso.PermisoFechaInicio.Value.ToString("yyyy-MM-dd HH:mm:ss"), permiso.PermisoFechaFin.Value.ToString("yyyy-MM-dd HH:mm:ss"), permiso.PermisoFechaRegistro.Value.ToString("yyyy-MM-dd HH:mm:ss"), permiso.PermisoFechaAprobado.Value.ToString("yyyy-MM-dd HH:mm:ss"), permiso.UsuarioEncargado));

                dbAlmacen.SaveChanges();
            }
            else if (value == 1)
            {
                permiso.PermisoEstado = 2;

                db.SaveChanges();
            }

            #endregion

            #region ENVIAR CORREO

            string archivoHTML = Server.MapPath(@"~/EtiquetasHtml/RespuestaSolicitudPermisoVacaciones.html");
            string logoIntranet = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            string empleadoID = db.Permisos.Find(permisoID).EmpleadoId;
            string departamento = db.VISTA_EMPLEADOS.Find(empleadoID).departamento;
            string empleadoCedula = db.VISTA_EMPLEADOS.Find(empleadoID).cedula;
            //string empleadoCorreo = Code.Utilities2.obtenerCorreoUsuario(empleadoCedula);
            //string empleadoUsername = Code.Utilities2.GetUserName(empleadoCedula);
            var empleadoCorreo = $"{permiso.PermisoUsuario}@pgr.gob.do";
            //var empleadoCorreo = $"wilkin.vasquez@pgr.gob.do";

            Mail mail = new Mail();

            string linkSolicitudPermiso = $"{Request.Url.Host}/intra/Permisos/Details?id={permisoID}";
            string permisoUsuarioEstado = db.Permisos.Find(permisoID).PermisosEstados.PermisoEstadoNombre;

            string titulo = "Solicitud de permiso";
            string asunto = "Solicitud de  Permiso";

            if(permiso.TipoLicenciaId == 1)
            {
                titulo = "Solicitud de vacaciones";
                asunto = "Solicitud de vacaciones";
            }

            var contenido = mail.ObtenerFormatoHTMLRespuesta(departamento, nombreEncargado, titulo, permisoUsuarioEstado, archivoHTML);

            mail.EnviarCorreoPermisoVacacion(empleadoCorreo, asunto.ToUpper(), contenido, DateTime.Now.ToString(), Logo, LogoDTI, Flecha);

            #endregion

            return Json(new { Estatus = "OK", PermisoEstado = permiso.PermisoEstado });
        }

        [CustomAuthorize]
        public PartialViewResult _Vacaciones(string empleadoID)
        {
            //empleadoID = "18310"; // Reinaldo
            //empleadoID = "17830"; // Junior
            //empleadoID = "9693"; // Regalado
            //empleadoID = "14763"; // Atisol
            //empleadoID = "11984"; // Manus
            //empleadoID = "2250"; // Fabio
            //empleadoID = "10584"; // Richard 
            //empleadoID = "18096"; // Channa 
            //empleadoID = "14754"; // Tió 
            //empleadoID = "16098"; // Egnis
            //empleadoID = "19070"; // Lissette

            #region QUERIES

            var queryVistaEmpleados = (
               from
                   vistaEmpleados
               in
                   db.VISTA_EMPLEADOS
               where
                   vistaEmpleados.empleadoid == empleadoID
               select
                   vistaEmpleados.fechaingreso
            );

            //var queryPermisos = (
            //    from
            //        permisos
            //    in
            //        db.Permisos
            //    where
            //        permisos.EmpleadoId == empleadoID &&
            //        permisos.TipoLicenciaId == 6
            //    select
            //        permisos
            //);

            var queryRHVacacion = (
                 from
                     RHVacacion
                 in
                     dbAlmacen.RHVACACION
                 where
                     RHVacacion.EmpleadoID == empleadoID &&
                     RHVacacion.TipoLicenciaID == 2 &&
                     RHVacacion.LicenciaID == 9 &&
                     RHVacacion.Anulado == false &&
                     RHVacacion.Aprobado == true
                 select
                     new
                     {
                         RHVacacion.FechaInicio,
                         RHVacacion.FechaFin
                     }
             );

            #endregion
           
            #region PREPARACION DE LA DATA

            var vacaciones = queryRHVacacion.ToList();
            List<DateTime[]> fechasVacaciones = new List<DateTime[]>();

            foreach (var row in vacaciones)
            {
                DateTime[] vacacionInicioFin = new DateTime[] { row.FechaInicio.Value.Date, row.FechaFin.Value.Date };

                fechasVacaciones.Add(vacacionInicioFin);
            }

            //DateTime fechaIngreso = new DateTime(2016, 2, 1);

            //List<DateTime[]> fechasVacaciones = new List<DateTime[]> {
            //    new DateTime[]{ new DateTime(2017, 8, 7), new DateTime(2017, 8, 29) }, // 15 dias
            //    new DateTime[]{ new DateTime(2016, 1, 7), new DateTime(2016, 1, 13) }, // 5 dias
            //    new DateTime[]{ new DateTime(2015, 7, 29), new DateTime(2015, 8, 11) } // 10 dias
            //};

            DateTime fechaIngreso = queryVistaEmpleados.FirstOrDefault().Date;
            //DateTime fechaIngreso = new DateTime(2007, 5, 1);
            //DateTime fechaIngreso = new DateTime(2007, 12, 1);

            DateCalculations dateCalculations = new DateCalculations(fechaIngreso, fechasVacaciones);
            string tiempoLaborando = dateCalculations.ObtenerTextoTiempoLaborando();

            int tiempoLaborandoNumerico = dateCalculations.ObtenerTiempoLaborando(DateTime.Now.Date)[0];

            List<Intervalo> intervalosDiasAcumulados;
            int diasAcumulados;

            if (tiempoLaborandoNumerico == 0)
            {
                intervalosDiasAcumulados = null;
                diasAcumulados = 0;
            }
            else
            {
                intervalosDiasAcumulados = dateCalculations.ObtenerDiasAcumulados();
                diasAcumulados = intervalosDiasAcumulados.Count() < 2 ? intervalosDiasAcumulados[0].DiasAcumulados : intervalosDiasAcumulados[0].DiasAcumulados + intervalosDiasAcumulados[1].DiasAcumulados;
            }

            ViewBag.IntervalosObservacion = JsonConvert.SerializeObject(intervalosDiasAcumulados);
            ViewBag.FechaIngreso = fechaIngreso.ToString("dd/MM/yyyy");
            ViewBag.TiempoLaborando = tiempoLaborando;
            ViewBag.DiasAcumulados = diasAcumulados;

            //ViewBag.Permisos = (
            //    db
            //    .Permisos
            //    .Where(x => x.EmpleadoId == empleadoID)
            //    .OrderBy(x => x.PermisoFechaRegistro)
            //    .ToList()
            //);

            var permisoUsuario = (
                db
                .Permisos
                .Where(x => x.EmpleadoId == empleadoID && x.TipoLicenciaId == 1)
                .OrderByDescending(x => x.PermisoFechaRegistro)
                .Take(1)
                .FirstOrDefault()
            );

            ViewBag.UltimoPermiso = permisoUsuario;

            ViewBag.Vacaciones = (
                 dbAlmacen
                 .RHVACACION
                 .Where(x => x.EmpleadoID == empleadoID && x.TipoLicenciaID == 2 && x.LicenciaID == 9)
                 .OrderBy(x => x.FechaRegistro)
                 .ToList()
             );

            if (permisoUsuario == null)
            {
                ViewBag.EstatusVacacion = "2";
            }
            else
            {
                ViewBag.EstatusVacacion = dateCalculations.ValidarVacacionPendienteVacacionEnProceso(permisoUsuario.PermisoEstado, permisoUsuario.PermisoFechaInicio.Value.Date, permisoUsuario.PermisoFechaFin.Value.Date);
            }

            #endregion

            return PartialView();
        }

        public JsonResult VacacionSolicitud(Vacacion vacacion)
        {
            string empleadoID = Session["empleadoId"].ToString();

            //empleadoID = "18310"; // Reinaldo
            //empleadoID = "17830"; // Junior
            //empleadoID = "2250"; // Fabio
            //empleadoID = "11984"; // Manus
            //empleadoID = "18096"; // Channa 
            //empleadoID = "9693"; // Regalado
            //empleadoID = "10584"; // Richard 
            //empleadoID = "19070"; // Lissette
            //empleadoID = "14754"; // Tió 

            #region VALIDAR VACACION FECHA SALIDA Y DIAS

            DateCalculations dateCalculations = new DateCalculations();
           
            int estatus = dateCalculations.ValidarVacacionSolicitud(vacacion.FechaInicio, vacacion.Dias);

            if (estatus != 5)
            {
                return Json(new { Estatus = estatus });
            }

            #endregion

            #region QUERIES

            var queryObtenerIdUltimoPermiso = (
                from
                    permisos
                in
                    db.Permisos
                where
                    permisos.EmpleadoId == empleadoID
                orderby
                    permisos.PermisoFechaRegistro descending
                select
                    permisos.PermisoId
            );

            var queryObtenerNombreCompleto = (
                from
                    vistaEmpleados
                in
                    db.VISTA_EMPLEADOS
                where
                    vistaEmpleados.empleadoid == empleadoID
                select
                    vistaEmpleados.nombre
            );

            var queryRHVacacion = (
                from
                    RHVacacion
                in
                    dbAlmacen.RHVACACION
                where
                    RHVacacion.EmpleadoID == empleadoID &&
                    (RHVacacion.TipoLicenciaID == 1 || RHVacacion.TipoLicenciaID == 2) &&
                    RHVacacion.Aprobado == true
                select
                    new
                    {
                        RHVacacion.FechaInicio,
                        RHVacacion.FechaFin
                    }
            );

            #endregion

            #region GUARDAR PERMISO
            var vacaciones = queryRHVacacion.ToList();
            List<DateTime[]> fechasVacaciones = new List<DateTime[]>();

            foreach (var row in vacaciones)
            {
                DateTime[] vacacionInicioFin = new DateTime[] { row.FechaInicio.Value.Date, row.FechaFin.Value.Date };

                fechasVacaciones.Add(vacacionInicioFin);
            }

            var empleado = db.VISTA_EMPLEADOS.Find(Session["empleadoId"]);
            var empleado2 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(e => e.Codigo == empleadoID).FirstOrDefault();

            DateTime fechaIngreso = empleado.fechaingreso;
            
            DateCalculations dateCalc = new DateCalculations(fechaIngreso, fechasVacaciones);
            
            var empleadoUsername = "";

            if (Session["dependenciaId"].ToString() == "10001" && Session["departamentoId"].ToString() == "15")
                empleadoUsername = Code.Utilities2.obtenerCorreoUsuario(empleado2.Cedula);
            else
                empleadoUsername = Code.Utilities2.GetUserName(empleado2.Cedula);
            
            var encargadoId = Code.UtilityMethods.buscarEncargado(empleado2).Value;
            var encargado = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == encargadoId.ToString()).FirstOrDefault();
            var observacion = dateCalc.ObtenerObservacionVacacion(vacacion);

            var encargadoUsername = "";

            if (encargado.CodigoDependencia == "10001" && encargado.CodigoDepartamento == "15")
                encargadoUsername = Code.Utilities2.obtenerCorreoUsuario(encargado.Cedula);
            else
                encargadoUsername = Code.Utilities2.GetUserName(encargado.Cedula);

            var permiso = new Permisos();
            permiso.EmpleadoId = empleadoID;
            permiso.PermisoUsuario = empleadoUsername;
            permiso.TipoLicenciaId = 1;
            permiso.PermisoObservacion = observacion;
            permiso.PermisoEstado = 1;
            permiso.PermisoFechaInicio = vacacion.FechaInicio;
            permiso.PermisoFechaFin = dateCalc.ObtenerFechaFin(vacacion.FechaInicio, vacacion.Dias);
            permiso.PermisoFechaRegistro = DateTime.Now;
            permiso.PermisoFechaAprobado = null;
            permiso.EncargadoId = encargadoId;
            permiso.UsuarioEncargado = encargadoUsername;

            db.Permisos.Add(permiso);
            db.SaveChanges();

            #endregion

            #region ENVIAR CORREO

            var archivoHTML = Server.MapPath(@"~/EtiquetasHtml/SolicitudPermisoVacaciones.html");
            var logoIntranet = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            var permisoID = queryObtenerIdUltimoPermiso.FirstOrDefault();
            var nombreCompleto = queryObtenerNombreCompleto.FirstOrDefault();
            var departamento = db.VISTA_EMPLEADOS.Find(empleadoID).departamento;
            
            var encargadoCorreo = $"{encargadoUsername}@pgr.gob.do";
           
            Mail mail = new Mail();

            var linkSolicitudPermiso = string.Empty;

            if (HttpRuntime.AppDomainAppVirtualPath.Contains("intra"))
            {
                //Prueba
                linkSolicitudPermiso = $"localhost{HttpRuntime.AppDomainAppVirtualPath}/Permisos/Details?id={permisoID}";
            }
            else if (HttpRuntime.AppDomainAppVirtualPath.Contains("PGR"))
            {
                //Producción
                linkSolicitudPermiso = $"http://intranet.pgr.gob.do{HttpRuntime.AppDomainAppVirtualPath}/Permisos/Details?id={permisoID}";
            }
            else if (HttpRuntime.AppDomainAppVirtualPath.Contains("CP") || HttpRuntime.AppDomainAppVirtualPath.Contains("cp"))
            {
                //Producción
                linkSolicitudPermiso = $"http://intranet.pgr.gob.do{HttpRuntime.AppDomainAppVirtualPath}/Permisos/Details?id={permisoID}";
            }

            var contenido = mail.ObtenerFormatoHTML(departamento, nombreCompleto, "Solicitud vacaciones", archivoHTML, linkSolicitudPermiso, observacion);

            mail.EnviarCorreoPermisoVacacion(encargadoCorreo, "Solicitud vacaciones".ToUpper(), contenido, DateTime.Now.ToString(), Logo, LogoDTI, Flecha);
            #endregion

            return Json(new { Estatus = estatus });
        }

        public ActionResult PermisoReporte(int id) {

            var permiso = db.Permisos.Find(id);

            if (permiso == null) { return View("Index", "MiPerfil"); }

            ViewBag.empleado = db.VISTA_EMPLEADOS.FirstOrDefault(x => x.empleadoid == permiso.EmpleadoId);
            ViewBag.supervisor = db.VISTA_EMPLEADOS.FirstOrDefault(x => x.empleadoid == permiso.EncargadoId.ToString()).nombre;

            return new ViewAsPdf("PermisoReporte", permiso)
            {
                PageSize = Size.Letter,
            };
        }

        public ActionResult PermisoReportes(int id)
        {
            var permiso = db.Permisos.Find(id);

            if (permiso == null) { return View("Index", "MiPerfil"); }

            ViewBag.empleado = db.VISTA_EMPLEADOS.FirstOrDefault(x => x.empleadoid == permiso.EmpleadoId);
            ViewBag.supervisor = db.VISTA_EMPLEADOS.FirstOrDefault(x => x.empleadoid == permiso.EncargadoId.ToString()).nombre;

            return View("PermisoReporte", permiso);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
