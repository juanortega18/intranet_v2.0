﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Models.TallerPgr;

namespace intra.Controllers
{
    [Authorize(Roles = "TA")]
    public class EquipoController : Controller
    {
        private TallerPgrDM db = new TallerPgrDM();

        // GET: Equipo
        public ActionResult Index()
        {
            var equipos = db.Equipo.ToList();
            foreach (Equipo equipo in equipos) {
                equipo.ModeloEquipo = (equipo.ModeloEquipo == null) ? new ModeloEquipo() { ModeloEquipoDescripcion = "NINGUNO" } : equipo.ModeloEquipo;
            }
            return View(equipos);
        }

        // GET: Equipo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Equipo equipo = db.Equipo.Find(id);
            if (equipo == null)
            {
                return HttpNotFound();
            }
            return View(equipo);
        }

        // GET: Equipo/Create
        public ActionResult Create()
        {
            ViewBag.TipoEquipoId = new SelectList(db.TipoEquipo, "TipoEquipoId", "TipoEquipoDescripcion");
            return View();
        }

        [HttpPost]
        public JsonResult Create(Equipo equipo)
        {
            equipo.EquipoSerie.ToUpper();

            var resultado = ValidarEquipo(equipo);

            if (resultado == "Valido")
            {
                db.Equipo.Add(equipo);
                db.SaveChanges();
                return Json("Guardado");
            }
            return Json(resultado);
        }

        // GET: Equipo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Equipo equipo = db.Equipo.Find(id);
            if (equipo == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoEquipoId = new SelectList(db.TipoEquipo, "TipoEquipoId", "TipoEquipoDescripcion", equipo.TipoEquipoId);
            return View(equipo);
        }

        [HttpPost]
        public JsonResult Edit(int id, int tipoid, int marcaid, int? modeloid, string serie)
        {
            var equipo = db.Equipo.Find(id);
            if (equipo == null) { return Json("A ocurrido un error, Trate de actualizar la pagina e intente otra vez."); }
            equipo.TipoEquipoId = tipoid;
            equipo.MarcaEquipoId = marcaid;
            equipo.ModeloEquipoId = modeloid;
            equipo.EquipoSerie = serie.ToUpper();

            var resultado = ValidarEquipo(equipo);

            if (resultado == "Valido") {
                db.SaveChanges();
                return Json("Actualizado");
            }
            return Json(resultado);
        }

        private string ValidarEquipo(Equipo equipo)
        {
            if (db.TipoEquipo.Find(equipo.TipoEquipoId) == null) { return "Ingrese un tipo de equipo valido."; }

            if (db.MarcaEquipo.Find(equipo.MarcaEquipoId) == null) { return "Ingrese una marca de equipo valida."; }

            if (equipo.EquipoSerie == null) { return "Ingrese la serie del modelo"; }

            if (db.TipoEquipo.Find(equipo.TipoEquipoId).MarcaEquipo.Where(x => x.MarcaEquipoId == equipo.MarcaEquipoId).Count() == 0)
            { return "El tipo y la marca de equipo que selecciono no estan relacionados."; }

            if (db.Equipo.Where(x => x.EquipoSerie == equipo.EquipoSerie && x.TipoEquipoId == equipo.TipoEquipoId 
            && x.MarcaEquipoId == equipo.MarcaEquipoId && x.ModeloEquipoId == equipo.ModeloEquipoId).Count() != 0)
            { return "Este equipo ya a sido ingresado."; }

            if (equipo.ModeloEquipoId != null) { if (db.ModeloEquipo.Find(equipo.ModeloEquipoId) == null) { return "El modelo ingresado es inválido"; } }
            return "Valido";
        }

        // GET: Equipo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Equipo equipo = db.Equipo.Find(id);
            if (equipo == null)
            {
                return HttpNotFound();
            }
            return View(equipo);
        }

        // POST: Equipo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Equipo equipo = db.Equipo.Find(id);
            db.Equipo.Remove(equipo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
