﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models.Votaciones;
namespace intra.Controllers
{
    public class AsambleaElectoralController : Controller
    {
        private HojaVotaciones db = new HojaVotaciones();

        // GET: Hoja
        public ActionResult Index()
        {
            return View();
        }

        // GET: Hoja/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Hoja/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Hoja/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Hoja/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Hoja/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Hoja/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Hoja/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult GetHojaInfo(string cedula)
        {
            try
            {
                var empleado = db.Hoja.Where(c => c.CEDULA == cedula).FirstOrDefault();

                if(empleado!=null)
                {
                    return Json(empleado);
                }
                else
                {
                    return Json("Empty");
                }
            }catch(Exception)
            {
                return Json("Error");
            }
            
        }
    }
}
