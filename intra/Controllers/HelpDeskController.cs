﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Sockets;
using System.IO;
using intra.Code;
using intra.Models.ViewModel;
using intra.Models.Servicios;
using intra.Models.PaseProduccion;
using intra.Models.FlotasExtensiones;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
    public class HelpDeskController : Controller
    {
        dbIntranet db = new dbIntranet();
        //Variable WebConfig
        public static string Direccion = ConfigurationManager.AppSettings["URLINTRA"];

        private static Correo objCorreo = new Correo();

        public static SqlConnection Enlace = new SqlConnection(ConfigurationManager.ConnectionStrings["intra"].ConnectionString);

        // GET: HelpDesk

        SolicitudModel smodel = new SolicitudModel();

        private dbIntranet obj = new dbIntranet();

        #region correoUsuario
        //private string correoUsuario(string cedula)
        //{

        //    string Resultado = "";

        //    if (Enlace.State == ConnectionState.Closed)
        //    {
        //        Enlace.Open();
        //    }

        //    SqlCommand cmd = new SqlCommand("sp_users_ldapID", Enlace);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.Clear();
        //    cmd.Parameters.AddWithValue("@id", cedula);
        //    SqlDataAdapter Sda = new SqlDataAdapter(cmd);
        //    DataTable Dta = new DataTable();
        //    Sda.Fill(Dta);

        //    String Datosusuario = Dta.Rows[0]["samAccountName"].ToString();

        //    Resultado = Datosusuario;

        //    return Resultado;

        //}
        #endregion

        #region correoUsuario
        private string correoUsuario(string cedula)
        {
            string Resultado = "";

            if (Enlace.State == ConnectionState.Closed)
            {
                Enlace.Open();
            }

            SqlCommand cmd = new SqlCommand("sp_users_ldapID", Enlace);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@id", cedula);
            SqlDataAdapter Sda = new SqlDataAdapter(cmd);
            DataTable Dta = new DataTable();
            Sda.Fill(Dta);

            if ((Dta != null) && (Dta.Rows.Count > 0))
            {

                Resultado = Dta.Rows[0]["samAccountName"].ToString();

            }
            else
            {
                Resultado = "SINREGISTRODOMAIN";
            }

            return Resultado;


        }
        #endregion

        #region EnviarCorreo
        private void EnviarCorreo(string Supervisor, string Responsable, string Sujeto, string Descripcion)
        {

            string CorreoSupervisor = "";

            string UsuarioResponsable = "";

            string PrimerCorreo = "";

            string SegundoCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            CorreoSupervisor = Supervisor;

            PrimerCorreo = CorreoSupervisor + "@PGR.GOB.DO";

            UsuarioResponsable = Responsable;

            SegundoCorreo = UsuarioResponsable + "@PGR.GOB.DO";

            objCorreo.EnviarCorreo_(new string[] { PrimerCorreo.Trim(), SegundoCorreo.Trim() }, Sujeto, Descripcion, Logo, Flecha, LogoDTI);

        }


        public string obtenerCorreoUsuario(string codigoEmpleado)
        {
            var cedulaEmpleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == codigoEmpleado).FirstOrDefault();
            var user = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", cedulaEmpleado.cedula)).FirstOrDefault();
            string correo = $"{user.samAccountName}@pgr.gob.do";

            return correo;
        }

        #region SendCorreo
        private void SendCorreo(string Sujeto, string Descripcion)
        {

            string codigoEmpleado = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoDTI" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoDTI = obtenerCorreoUsuario(codigoEmpleado);

            string codigoEmpleadoEncargadoDesarrollo = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoDesarrollo" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoDesarrollo = obtenerCorreoUsuario(codigoEmpleadoEncargadoDesarrollo);

            //Correo del Encargado de Proyecto - Removido para del proceso de Pase a Produccion
            //string codigoEmpleadoEncargadoProyecto = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoProyecto" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            //string correoEncargadoProyecto = obtenerCorreoUsuario(codigoEmpleadoEncargadoProyecto);

            //string lider = Session["usuario"].ToString();
            //string correoLider = lider + "@pgr.gob.do";
            //Lider
            //string codigoEmpleadoLiderEquipo = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "Lider" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            //string correoLiderEquipo = obtenerCorreoUsuario(codigoEmpleadoLiderEquipo);

            ////string UsuarioResponsable = "";

         //   string PrimerCorreo = "";

            //string SegundoCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            //CorreoSupervisor = Supervisor;

           // PrimerCorreo = Supervisor + "@PGR.GOB.DO";

           // UsuarioResponsable = Responsable + "@PGR.GOB.DO";

            //SegundoCorreo = UsuarioResponsable;// + "@PGR.GOB.DO";

           // objCorreo.CorreoPaseProduccion(SegundoCorreo, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            objCorreo.CorreoPaseProduccion( correoEncargadoDesarrollo, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            objCorreo.CorreoPaseProduccion( correoEncargadoDTI, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            //objCorreo.CorreoPaseProduccion( correoEncargadoProyecto, Sujeto, Descripcion, Logo, Flecha, LogoDTI);

        }
        #endregion


        #region ObtenerEtiquetaHTML
        public string SolicitudAprobacionPaseProduccion(string Departamento, string NombreCompleto, string Problema, string Link, string Descripcion)
        {


            //string NombreArchivo = Server.MapPath (@"~/EtiquetasHtml/Solicitud_nuevoServicio_PGR.html");
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/SolicitudPaseProduccion.html");


            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", "SOLICITUD PASE PRODUCCION");
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", Descripcion);



            return Archivo;
        }

        #endregion









        #region ObtenerEtiquetaHTML
        public string DescripcionCorreo(string Departamento, string NombreCompleto, string Problema, string Link, string Descripcion)
        {


            //string NombreArchivo = Server.MapPath (@"~/EtiquetasHtml/Solicitud_nuevoServicio_PGR.html");
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/Solicitud-nuevoServicio.html");


            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", "SERVICIO ASIGNADO");
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", Descripcion);



            return Archivo;
        }

        #endregion





        #endregion

        #region Insertar_en_Logs
        public void InsertarLog(int CodigoSolicitud, string Descripcion)
        {
            string IP4Address = String.Empty;

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily == AddressFamily.InterNetwork)
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            try
            {

                if (HttpContext.Session["empleadoId"] != null)
                {
                    Sol_Logs Tb = new Sol_Logs();
                    Tb.SistemaId = 1;
                    Tb.SolicitudId = CodigoSolicitud;
                    Tb.logIP = IP4Address;
                    Tb.logUsuario = HttpContext.Session["empleadoId"].ToString();
                    Tb.logFecha = DateTime.Now;
                    Tb.logAccion = Descripcion;
                    obj.Sol_Logs.Add(Tb);
                    obj.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

        }
        #endregion

        #region Mostrar_Nombre_Empleados
        private string Mth_Mostrar_Nombre_Empleados(string CodigoEmpleados)
        {
            string NombreCompleto = "";

            var A = obj.Sol_Mostrar_Personal_PGR.Where(cd => cd.Codigo == CodigoEmpleados).FirstOrDefault();

            NombreCompleto = A.NombreCompleto;

            return NombreCompleto;

        }
        #endregion

        #region Mostrar_Deptos_Empleados
        private string Mth_Mostrar_Deptos_Empleados(string CodigoEmpleados)
        {
            string Departamento = "";

            var A = obj.Sol_Mostrar_Personal_PGR.Where(cd => cd.Codigo == CodigoEmpleados).FirstOrDefault();

            Departamento = A.Departamento;

            return Departamento;

        }
        #endregion

        #region MostrarCantidaddeSolicitud
        private void MostrarCantidaddeSolicitud()
        {
            int Cdg = 0;

            Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

            var Cantidad = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

            if (Cantidad.Count > 0)
            {

                HttpContext.Session["Cantidad"] = Cantidad.Count();
            }
            else
            {
                HttpContext.Session["Cantidad"] = 0;
            }
        }
        #endregion

        #region MostrarCantidadAsignados
        private void MostrarCantidadAsignados()
        {
            int Cdg = 0;

            Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

            if (new HomeController().ValidarActividad(Cdg) == true)
            {
                List<ActividadDependencia> CdgTipoSolicitud = new List<ActividadDependencia>();

                CdgTipoSolicitud = new AsignadosController().CodigoActividad2(Cdg);

                var CantidadAsignado = new List<Vw_Mostrar_Usuario_Con_Su_Descripcion>();

                foreach (var item in CdgTipoSolicitud)
                {
                    if (item.Key == "Actividad")
                    {
                        var lista_solicitudes_por_actividad = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoEstadoSolicitud == 1 && cd.CodigoTipoSolicitud == item.Value && cd.CodigoTecnico == Cdg && cd.DependenciaId == 1).OrderByDescending(cd => cd.FechaCreacion).ToList();

                        foreach (var item2 in lista_solicitudes_por_actividad)
                        {
                            CantidadAsignado.Add(item2);
                        }
                    }
                    else
                    {
                        var lista_solicitudes_por_dependencia = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoEstadoSolicitud == 1 && cd.CodigoTecnico == Cdg && cd.DependenciaId == item.Value).OrderByDescending(cd => cd.FechaCreacion).ToList();

                        foreach (var item2 in lista_solicitudes_por_dependencia)
                        {
                            CantidadAsignado.Add(item2);
                        }
                    }
                }

                if (CantidadAsignado.Count > 0)
                {
                    HttpContext.Session["CantidadAsignado"] = CantidadAsignado.Count();
                }
                else
                {
                    HttpContext.Session["CantidadAsignado"] = 0;
                }
            }
            else
            {
                var CantidadAsignado = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTecnico == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

                if (CantidadAsignado.Count > 0)
                {
                    HttpContext.Session["CantidadAsignado"] = CantidadAsignado.Count();
                }
                else
                {
                    HttpContext.Session["CantidadAsignado"] = 0;
                }
            }
        }
        #endregion

        #region Mostrar_Listado_Informacion_Departamentos
        private void Mostrar_Informacion_Listado_Personal_PGR_Por_Deptos()
        {
            try
            {
                var Detalles = (from A in obj.VISTA_EMPLEADOS
                                orderby A.nombre.ToUpper() ascending
                                select new ListadoPersonalPgrModel
                                {
                                    EmpleadoId = A.empleadoid,
                                    Descripcion = A.nombre.ToUpper(),
                                    Cedula = A.cedula,
                                }).ToList();

                smodel.Listado_EmpleadoPGR = Detalles;

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }
        #endregion

        #region MostrarSINODirectores
        private int CdgDirectores(int CodigoDirectores)
        {
            int A;

            var Valor = obj.Sol_Departamento.Where(cd => cd.DirectorId == CodigoDirectores).ToList();

            if (Valor.Count > 0)
            {

                A = 1;
            }
            else
            {
                A = 2;
            }

            return A;


        }
        #endregion

        #region MostrarSINOSupervisor
        private int CdgSupervisor(int CodigoSupervisor)
        {
            int Resultado = 0;

            var cantidad = obj.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSupervisor).ToList();

            if (cantidad.Count > 0)
            {
                Resultado = 1;
            }
            else
            {
                Resultado = 2;
            }


            return Resultado;


        }
        #endregion

        #region MostrarSINOOtrosTecnicos
        private int CdgOtrosTecnicos(int CodigoOtrosTecnicos)
        {
            int Resultado = 0;

            var cantidad = obj.Sol_Tecnicos_Alternativos.Where(cd => cd.TecnicoId == CodigoOtrosTecnicos && cd.Estado).ToList();

            if (cantidad.Count > 0)
            {
                Resultado = 1;
            }
            else
            {
                Resultado = 2;
            }


            return Resultado;


        }
        #endregion

        #region Mostrar_Cedula_Empleados
        private string Mth_Mostrar_Cedula_Empleados(string CodigoEmpleados)
        {
            string Cedula = "";

            var A = obj.Sol_Mostrar_Personal_PGR.Where(cd => cd.Codigo == CodigoEmpleados).FirstOrDefault();

            Cedula = A.Cedula;

            return Cedula;

        }
        #endregion

        [CustomAuthorize]
        [HttpGet]
        public ActionResult Solicitud()
        {
            Mostrar_Informacion_Usuario();
            Mostrar_Informacion_Departamentos();
            Mostrar_Informacion_Listado_Personal_PGR_Por_Deptos();

            int Cdg = 0;

            if (HttpContext.Session["empleadoId"] != null)
            {
                Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

                if (CdgSupervisor(Cdg) == 1)
                {
                    smodel.CodigoSupervisor = CdgSupervisor(Cdg);
                }
                else if (CdgDirectores(Cdg) == 1)
                {
                    smodel.CodigoDirector = CdgDirectores(Cdg);
                }
                else if (CdgOtrosTecnicos(Cdg) == 1)
                {
                    smodel.CodigoOtrosTecnicos = CdgOtrosTecnicos(Cdg);
                }
            }

            if (TempData["Msj"] != null)
            {
                ViewBag.MSJ = TempData["Msj"].ToString();
            }
            //Estas consultas son para los datePicker de las solicitudes , para que los dias reservados aparezcan desabilitados
            using (dbIntranet db = new dbIntranet())
            {
                ViewBag.TipoEvento = db.TipoEventos.ToList();
                ViewBag.tipoCoberturas = db.TipoCobertura.ToList();

                var solicitudEspacios = db.Sol_Registro_Solicitud.SelectMany(x => x.Eventos.Where(y => y.EventoFhFinal > DateTime.Now && x.Tipo_SolicitudId == 1016 || x.Tipo_SolicitudId == 17).ToList()).OrderBy(x => x.EventoFhInicio).ToList();
                ViewBag.fechasSolicEsp = solicitudEspacios;

                var coberturaPrensa = db.Sol_Registro_Solicitud.SelectMany(x => x.Eventos.Where(y => y.EventoFhFinal > DateTime.Now && x.Tipo_SolicitudId == 1017 || x.Tipo_SolicitudId == 18).ToList()).OrderBy(x => x.EventoFhInicio).ToList();
                ViewBag.fechasCoberturaPrensa = coberturaPrensa;

                var MontajeEventos = db.Sol_Registro_Solicitud.SelectMany(x => x.Eventos.Where(y => y.EventoFhFinal > DateTime.Now && x.Tipo_SolicitudId == 1018 || x.Tipo_SolicitudId == 19).ToList()).OrderBy(x => x.EventoFhInicio).ToList();
                ViewBag.fechasMontajeEventos = MontajeEventos;

                ViewBag.AreasImplicadas = (from a in db.AreasImplicadasPaseProduccion
                                           select new AreasProduccionVM
                                           {
                                               AreaId = a.AreasImplicadasId,
                                               AreasDescripcion = a.AreasImplicadasDescripcion
                                           }).ToList();
                                          
                ViewBag.CategoriasProduccion = (from c in db.CategoriasProduccion
                                               select new CategoriaProduccioVM
                                               {
                                                   CategoriaId = c.CategoriasProduccionId,
                                                   CategoriaDescripcion = c.CategoriasProduccionDescripcion
                                               }).ToList();

                string flota = "";
                string extension = "";

                if (HttpContext.Session["empleadoId"] != null)
                {
                    string emp_codigo = HttpContext.Session["empleadoId"].ToString();

                    var empleado_tel_datos = db.FlotaExtensionEmpleado.Where(x => (x.FlotExtEmpEmpleadoId.Value.ToString() == emp_codigo)).FirstOrDefault();

                    if (empleado_tel_datos != null)
                    {
                        ViewBag.Confirmed = empleado_tel_datos.FlotExtEmpDatosConfirmados;

                        flota = empleado_tel_datos.FlotExtEmpFlota;
                        extension = empleado_tel_datos.FlotExtEmpExtesion;
                    }

                    smodel.Flota = flota;
                    smodel.Extension = extension;
                }

                List<DependenciaRegionalModel> dependenciasRegionales = (
                    from TablaDependenciasRegionales in db.Sol_DependenciasRegionales /*where
                    TablaDependenciasRegionales.DependenciaId == 1*/
                    select new DependenciaRegionalModel
                    {
                        DependenciaId = TablaDependenciasRegionales.DependenciaId,
                        DependenciaNombre = TablaDependenciasRegionales.DependenciaNombre
                    }
                ).ToList();

                smodel.ListadoDependenciasRegionales = dependenciasRegionales;
            }

            ViewBag.Evento = new intra.Models.Eventos();

            return View(smodel);
        }

        #region Mostrar_Actividad
        private string Mth_Mostrar_Actividad(int CodigoActividad)
        {
            string Descripcion = "";

            var A = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CodigoActividad).FirstOrDefault();

            Descripcion = A.Descripcion;

            return Descripcion;

        }
        #endregion

        #region Mostrar_Informacion_Departamentos
        private void Mostrar_Informacion_Departamentos()
        {
            //if (Enlace.State == ConnectionState.Closed)
            //{
            //    Enlace.Open();
            //}

            //SqlCommand cmd = new SqlCommand("SP_Mostrar_Dep", Enlace);
            //cmd.CommandType = CommandType.StoredProcedure;
            //SqlDataAdapter Sda = new SqlDataAdapter(cmd);
            //DataTable Dta = new DataTable();
            //Sda.Fill(Dta);


            //List<SelectListItem> list = new List<SelectListItem>();


            //smodel.Departamentos = Dta.AsEnumerable().Select(cd => new DepartamentoModel()
            //{
            //    DepartamentoId = cd.Field<int>("DepartamentoId"),
            //    Descripcion = cd.Field<string>("Descripcion"),
            //}).ToList();
            smodel.Departamentos = obj.Sol_Departamento.ToList().Where(cd => cd.Estado == true).Select(cd => new DepartamentoModel()
            {
                DepartamentoId = cd.DepartamentoId,
                Descripcion = cd.Descripcion
            }).ToList();
        }
        #endregion

        #region Mostrar_Tipo_Servicios
        public JsonResult GetActividades(int id)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            if (id < 1)
            {
                RedirectToAction("Solicitud");
            }
            else
            {
               

                var Tipo_Actividades = obj.Sol_Actividades.Where(cd => cd.DepartamentoId == id && (cd.ActividadId != 34 || cd.ActividadId == 1019)).OrderBy(x => x.Descripcion).ToList();

                foreach (var Dr in Tipo_Actividades)
                {
                    list.Add(new SelectListItem { Text = Dr.Descripcion.ToString(), Value = Dr.ActividadId.ToString() });
                }
            }

            return Json(new SelectList(list, "value", "Text"));
        }

        #endregion

        #region Mostrar_Tipos_Actividades_Servicios
        public JsonResult GetSubActividades(int id)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            if (id < 1)
            {
                RedirectToAction("Solicitud");
            }
            else
            {
                //if (Enlace.State == ConnectionState.Closed)
                //{
                //    Enlace.Open();
                //}

                //SqlCommand cmd = new SqlCommand("SP_Buscar_Actividades", Enlace);
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@CdgDepartamento", id);
                //SqlDataAdapter Sda = new SqlDataAdapter(cmd);
                //DataTable Dta = new DataTable();
                //Sda.Fill(Dta);

                //foreach (DataRow Dr in Dta.Rows)
                //{
                //    list.Add(new SelectListItem { Text = Dr["Descripcion"].ToString(), Value = Dr["ActividadId"].ToString() });
                //}

                var Sub_Tipo_Actividades = obj.Sol_Sub_Actividades.Where(cd => cd.ActividadId == id && cd.Estado).OrderBy(x=>x.Descripcion).ToList();

                foreach (var Dr in Sub_Tipo_Actividades)
                {
                    list.Add(new SelectListItem { Text = Dr.Descripcion.ToString(), Value = Dr.SubActividadId.ToString() });
                }
            }



            return Json(new SelectList(list, "value", "Text"));



        }

        #endregion

        #region Mostrar_Informacion_Usuario
        private void Mostrar_Informacion_Usuario()
        {
            if ((HttpContext.Session["nombre"] != null) && (HttpContext.Session["departamento"] != null))
            {
                smodel.Usurio = HttpContext.Session["nombre"].ToString();
                smodel.Deptos = HttpContext.Session["departamento"].ToString();
            }
            else
            {
                RedirectToAction("Index", "Home");
            }
        }
        #endregion

        [HttpPost]
        public ActionResult Insertrar_Informacion_Servicios(SolicitudModel solicitudModel, FormCollection fc, int[] AreasImplicadas, int[] CategoriasProduccion)
        {
            if (string.IsNullOrEmpty(solicitudModel.Extension))
            {
                ModelState.AddModelError("Extension", "Este campo es requerido");
                return RedirectToAction("Solicitud");
            }



            int Cdg = 0;

            #region Para guardar flota y extension
            string codigo_empleado = HttpContext.Session["empleadoId"].ToString();

            Cdg = Convert.ToInt32(codigo_empleado);

            if ((CdgSupervisor(Cdg) == 1) || (CdgDirectores(Cdg) == 1) || (CdgOtrosTecnicos(Cdg) == 1))
            {
                if (solicitudModel.CodigoOtrosEmpleados > 0)
                {
                    if (string.IsNullOrEmpty(solicitudModel.Flota))
                    {
                        solicitudModel.Flota = null;
                    }

                    using (dbIntranet db = new dbIntranet())
                    {
                        var tiene_flota = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpEmpleadoId == solicitudModel.CodigoOtrosEmpleados).FirstOrDefault();

                        string archivo_html = Server.MapPath(@"~/EtiquetasHtml/mensaje_flotas.html");

                        if (tiene_flota == null)
                        {
                            FlotaExtensionEmpleado nueva_flota = new FlotaExtensionEmpleado();
                            Vw_Mostrar_Personal_Permisos_PGR empleado = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == solicitudModel.CodigoOtrosEmpleados.ToString()).FirstOrDefault();
                            VISTA_EMPLEADOS empleado2 = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == solicitudModel.CodigoOtrosEmpleados.ToString()).FirstOrDefault();

                            if (!string.IsNullOrEmpty(solicitudModel.Flota) && !string.IsNullOrEmpty(solicitudModel.Extension))
                            {
                                if (empleado != null && empleado2 != null)
                                {
                                    nueva_flota.FlotExtEmpFlota = solicitudModel.Flota;
                                    nueva_flota.FlotExtEmpDatosConfirmados = false;
                                    nueva_flota.FlotExtEmpEmpleadoId = solicitudModel.CodigoOtrosEmpleados;
                                    nueva_flota.FlotExtEmpExtesion = solicitudModel.Extension;

                                    db.FlotaExtensionEmpleado.Add(nueva_flota);

                                    db.SaveChanges();

                                    if (!string.IsNullOrEmpty(nueva_flota.FlotExtEmpFlota))
                                    {
                                        FlotaExtensionEmpleado flota_correo = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpFlota == nueva_flota.FlotExtEmpFlota).FirstOrDefault();

                                        Code.Mail mail = new Code.Mail();
                                        mail.SendFlotaMail(flota_correo, archivo_html, "Creación de nueva flota".ToUpper());
                                    }
                                }
                            }
                            else if (string.IsNullOrEmpty(solicitudModel.Flota) && !string.IsNullOrEmpty(solicitudModel.Extension))
                            {
                                nueva_flota.FlotExtEmpEmpleadoId = Convert.ToInt32(solicitudModel.CodigoOtrosEmpleados);
                                nueva_flota.FlotExtEmpExtesion = solicitudModel.Extension;

                                db.FlotaExtensionEmpleado.Add(nueva_flota);

                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            if (tiene_flota.FlotExtEmpFlota != solicitudModel.Flota || tiene_flota.FlotExtEmpExtesion != solicitudModel.Extension)
                            {
                                if (tiene_flota.FlotExtEmpFlota == solicitudModel.Flota)
                                {
                                    tiene_flota.FlotExtEmpFlota = solicitudModel.Flota;
                                    tiene_flota.FlotExtEmpExtesion = solicitudModel.Extension;

                                    db.Entry(tiene_flota).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    tiene_flota.NumeroFlotaViejo = tiene_flota.FlotExtEmpFlota;
                                    tiene_flota.FlotExtEmpFlota = solicitudModel.Flota;
                                    tiene_flota.FlotExtEmpExtesion = solicitudModel.Extension;
                                    tiene_flota.FlotExtEmpDatosConfirmados = false;

                                    db.Entry(tiene_flota).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();

                                    Code.Mail mail = new Code.Mail();
                                    mail.SendFlotaMail(tiene_flota, archivo_html, "Atualización de número de flota".ToUpper());
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(solicitudModel.Flota))
                    {
                        solicitudModel.Flota = null;
                    }

                    using (dbIntranet db = new dbIntranet())
                    {
                        var tiene_flota = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpEmpleadoId.Value.ToString() == codigo_empleado).FirstOrDefault();

                        string archivo_html = Server.MapPath(@"~/EtiquetasHtml/mensaje_flotas.html");

                        if (tiene_flota == null)
                        {
                            FlotaExtensionEmpleado nueva_flota = new FlotaExtensionEmpleado();
                            Vw_Mostrar_Personal_Permisos_PGR empleado = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == codigo_empleado).FirstOrDefault();
                            VISTA_EMPLEADOS empleado2 = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == codigo_empleado).FirstOrDefault();

                            if (!string.IsNullOrEmpty(solicitudModel.Flota) && !string.IsNullOrEmpty(solicitudModel.Extension))
                            {
                                if (empleado != null && empleado2 != null)
                                {
                                    nueva_flota.FlotExtEmpFlota = solicitudModel.Flota;
                                    nueva_flota.FlotExtEmpDatosConfirmados = false;
                                    nueva_flota.FlotExtEmpEmpleadoId = Convert.ToInt32(codigo_empleado);
                                    nueva_flota.FlotExtEmpExtesion = solicitudModel.Extension;

                                    db.FlotaExtensionEmpleado.Add(nueva_flota);

                                    db.SaveChanges();

                                    if (!string.IsNullOrEmpty(nueva_flota.FlotExtEmpFlota))
                                    {
                                        FlotaExtensionEmpleado flota_correo = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpFlota == nueva_flota.FlotExtEmpFlota).FirstOrDefault();

                                        Code.Mail mail = new Code.Mail();
                                        mail.SendFlotaMail(flota_correo, archivo_html, "Creación de nueva flota".ToUpper());
                                    }
                                }
                            }
                            else if (string.IsNullOrEmpty(solicitudModel.Flota) && !string.IsNullOrEmpty(solicitudModel.Extension))
                            {
                                nueva_flota.FlotExtEmpEmpleadoId = Convert.ToInt32(codigo_empleado);
                                nueva_flota.FlotExtEmpExtesion = solicitudModel.Extension;

                                db.FlotaExtensionEmpleado.Add(nueva_flota);

                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            if (tiene_flota.FlotExtEmpFlota != solicitudModel.Flota || tiene_flota.FlotExtEmpExtesion != solicitudModel.Extension)
                            {
                                if (tiene_flota.FlotExtEmpFlota == solicitudModel.Flota)
                                {
                                    tiene_flota.FlotExtEmpFlota = solicitudModel.Flota;
                                    tiene_flota.FlotExtEmpExtesion = solicitudModel.Extension;

                                    db.Entry(tiene_flota).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    tiene_flota.NumeroFlotaViejo = tiene_flota.FlotExtEmpFlota;
                                    tiene_flota.FlotExtEmpFlota = solicitudModel.Flota;
                                    tiene_flota.FlotExtEmpExtesion = solicitudModel.Extension;
                                    tiene_flota.FlotExtEmpDatosConfirmados = false;

                                    db.Entry(tiene_flota).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();

                                    Code.Mail mail = new Code.Mail();
                                    mail.SendFlotaMail(tiene_flota, archivo_html, "Atualización de número de flota".ToUpper());
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(solicitudModel.Flota))
                {
                    solicitudModel.Flota = null;
                }

                using (dbIntranet db = new dbIntranet())
                {
                    var tiene_flota = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpEmpleadoId.Value.ToString() == codigo_empleado).FirstOrDefault();

                    string archivo_html = Server.MapPath(@"~/EtiquetasHtml/mensaje_flotas.html");

                    if (tiene_flota == null)
                    {
                        FlotaExtensionEmpleado nueva_flota = new FlotaExtensionEmpleado();
                        Vw_Mostrar_Personal_Permisos_PGR empleado = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == codigo_empleado).FirstOrDefault();
                        VISTA_EMPLEADOS empleado2 = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == codigo_empleado).FirstOrDefault();

                        if (!string.IsNullOrEmpty(solicitudModel.Flota) && !string.IsNullOrEmpty(solicitudModel.Extension))
                        {
                            if (empleado != null && empleado2 != null)
                            {
                                nueva_flota.FlotExtEmpFlota = solicitudModel.Flota;
                                nueva_flota.FlotExtEmpDatosConfirmados = false;
                                nueva_flota.FlotExtEmpEmpleadoId = Convert.ToInt32(codigo_empleado);
                                nueva_flota.FlotExtEmpExtesion = solicitudModel.Extension;

                                db.FlotaExtensionEmpleado.Add(nueva_flota);

                                db.SaveChanges();

                                if (!string.IsNullOrEmpty(nueva_flota.FlotExtEmpFlota))
                                {
                                    FlotaExtensionEmpleado flota_correo = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpFlota == nueva_flota.FlotExtEmpFlota).FirstOrDefault();

                                    Code.Mail mail = new Code.Mail();
                                    mail.SendFlotaMail(flota_correo, archivo_html, "Creación de nueva flota".ToUpper());
                                }
                            }
                        }
                        else if (string.IsNullOrEmpty(solicitudModel.Flota) && !string.IsNullOrEmpty(solicitudModel.Extension))
                        {
                            nueva_flota.FlotExtEmpEmpleadoId = Convert.ToInt32(codigo_empleado);
                            nueva_flota.FlotExtEmpExtesion = solicitudModel.Extension;

                            db.FlotaExtensionEmpleado.Add(nueva_flota);

                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        if (tiene_flota.FlotExtEmpFlota != solicitudModel.Flota || tiene_flota.FlotExtEmpExtesion != solicitudModel.Extension)
                        {
                            if (tiene_flota.FlotExtEmpFlota == solicitudModel.Flota)
                            {
                                tiene_flota.FlotExtEmpFlota = solicitudModel.Flota;
                                tiene_flota.FlotExtEmpExtesion = solicitudModel.Extension;

                                db.Entry(tiene_flota).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                tiene_flota.NumeroFlotaViejo = tiene_flota.FlotExtEmpFlota;
                                tiene_flota.FlotExtEmpFlota = solicitudModel.Flota;
                                tiene_flota.FlotExtEmpExtesion = solicitudModel.Extension;

                                db.Entry(tiene_flota).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();

                                Code.Mail mail = new Code.Mail();
                                mail.SendFlotaMail(tiene_flota, archivo_html, "Atualización de número de flota".ToUpper());
                            }
                        }
                    }
                }
            }
            #endregion
            //int xb = Convert.ToInt32("abc");

            //Metodos Proceso Consulta

            Mostrar_Informacion_Usuario();
            Mostrar_Informacion_Departamentos();

            //Variables Usuario

            string Usuario = "";

            string DomainUserSolicitante = "";

            string NombreCompleto = "";

            string Cedula = "";

            int CodigoDepartamento = 0;

            int CodigoTipoSolicitud = 0;

            int CodigoSubTipoSolicitud = 0;

            string DescripcionSolicitud = "";

            int Horas = 24;

            DomainUserSolicitante = HttpContext.Session["usuario"].ToString();

            CodigoDepartamento = solicitudModel.DepartamentoId;

            CodigoTipoSolicitud = solicitudModel.Actividades;

            CodigoSubTipoSolicitud = solicitudModel.SubActividades;

            DescripcionSolicitud = solicitudModel.Descripcion;

            //Variable de Secciones
            Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());
            NombreCompleto = HttpContext.Session["nombre"].ToString();
            Cedula = HttpContext.Session["cedula"].ToString();
            Usuario = HttpContext.Session["usuario"].ToString();


            //Condicion 

            if ((NombreCompleto != null) && (Cedula != null) && (Usuario != null) && (Cdg >= 0) && (DescripcionSolicitud != "") && (DomainUserSolicitante != null))
            {
                if (CodigoDepartamento == 0 && solicitudModel.CodigoDependencia == 1)
                {
                    smodel.MensajeError = "SELECCIONE EL DEPARTAMENTO.";
                }
                else if (CodigoTipoSolicitud == 0 && solicitudModel.CodigoDependencia == 1)
                {
                    smodel.MensajeError = "SELECCIONE EL TIPO DE SOLICITUD.";
                }
                else if (CodigoSubTipoSolicitud == 0 && solicitudModel.CodigoDependencia == 1)
                {
                    smodel.MensajeError = "SELECCIONE EL TIPO DE SERVICIO.";
                }
                else if ((DescripcionSolicitud == "") || (DescripcionSolicitud == null))
                {
                    smodel.MensajeError = "FAVOR DETALLAR EL PROBLEMA.";
                }
                else if ((CodigoDepartamento == 0) && (CodigoTipoSolicitud == 0) && solicitudModel.CodigoDependencia == 1)
                {
                    smodel.MensajeError = "FAVOR SELECCIONAR EL DEPARTAMENTO Y EL TIPO DE SOLICITUD.";
                }
                else if ((CodigoDepartamento > 0) && (CodigoTipoSolicitud > 0) && (CodigoSubTipoSolicitud > 0) && solicitudModel.CodigoDependencia == 1)
                {
                    try
                    {
                        #region Commented Code
                        //SqlCommand cmd = new SqlCommand("Sp_Insertar_Registro_Solicitud", Enlace);
                        //cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.Clear();
                        //cmd.Parameters.AddWithValue("@CodigoSolicitante", Cdg);
                        //cmd.Parameters.AddWithValue("@CodigoDepartamento", CodigoDepartamento);
                        //cmd.Parameters.AddWithValue("@CodigoTipoSolicitud", CodigoTipoSolicitud);
                        //cmd.Parameters.AddWithValue("@DescripcionSolicitud", DescripcionSolicitud);
                        //cmd.Parameters.AddWithValue("@CodigoEstado ", 1);
                        //cmd.Parameters.AddWithValue("@Estado", true);
                        //cmd.Parameters.AddWithValue("@Horas", Horas);
                        //cmd.Parameters.AddWithValue("@FhInicioSolicitud", Convert.ToDateTime(DateTime.Now.ToString()));
                        //cmd.Parameters.AddWithValue("@FhModificacion ", Convert.ToDateTime(DateTime.Now.ToString()));
                        //cmd.ExecuteNonQuery();
                        #endregion

                        var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CodigoTipoSolicitud && cd.Estado == true).FirstOrDefault();

                        var Supervisor2 = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == Supervisor.SupervisorId.ToString()).FirstOrDefault();

                        string correo_encargado = "";

                        if (Supervisor2.CodigoDependencia == "10001" && Supervisor2.CodigoDepartamento == "15")
                            correo_encargado = Code.Utilities2.obtenerCorreoUsuario(Supervisor2.Cedula);
                        else
                            correo_encargado = correoUsuario(Supervisor2.Cedula);

                        if (string.IsNullOrEmpty(correo_encargado))
                        {
                            correo_encargado = Supervisor.Correo;
                        }

                        var UsuarioEnSesion = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == Cdg.ToString()).FirstOrDefault();

                        string correo_usuario_en_sesion = "";

                        if (UsuarioEnSesion.CodigoDependencia == "10001" && UsuarioEnSesion.CodigoDepartamento == "15")
                        {
                            correo_usuario_en_sesion = Code.Utilities2.obtenerCorreoUsuario(UsuarioEnSesion.Cedula);
                        }
                        else
                        {
                            try
                            {
                                correo_usuario_en_sesion = correoUsuario(UsuarioEnSesion.Cedula);
                            } catch (Exception e)
                            {
                                try
                                {
                                    correo_usuario_en_sesion = Code.Utilities2.obtenerCorreoUsuario(UsuarioEnSesion.Cedula);
                                } catch (Exception e1)
                                {
                                    correo_usuario_en_sesion = "";
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(correo_usuario_en_sesion))
                        {
                            correo_usuario_en_sesion = "SINREGISTRODOMAIN";
                        }

                      Sol_Registro_Solicitud Tb = new Sol_Registro_Solicitud();

                        //int Codigo = 0;

                        //string Cedulas = "";

                        if ((CdgSupervisor(Cdg) == 1) || (CdgDirectores(Cdg) == 1) || (CdgOtrosTecnicos(Cdg) == 1))
                        {
                            if (solicitudModel.CodigoOtrosEmpleados == 0)
                            {
                                solicitudModel.CodigoOtrosEmpleados = Convert.ToInt32(HttpContext.Session["empleadoId"].ToString());
                            }

                            if (solicitudModel.CodigoOtrosEmpleados > 0)
                            {
                                string Codigos = "";

                                Codigos = solicitudModel.CodigoOtrosEmpleados.ToString();

                                var DatosPersona = obj.Sol_Mostrar_Personal_PGR.Where(cd => cd.Codigo == Codigos).FirstOrDefault();

                                Tb.TecnicoId = int.Parse(DatosPersona.Codigo);

                                ///Cedulas = Mth_Mostrar_Cedula_Empleados(Convert.ToString(solicitudModel.CodigoOtrosEmpleados));
                                string DomainUser = "";

                                if (DatosPersona.CodigoDependencia == "10001" && DatosPersona.CodigoDepartamento == "15")
                                {
                                    DomainUser = Code.Utilities2.obtenerCorreoUsuario(DatosPersona.Cedula);
                                    
                                }
                                else
                                {
                                    try
                                    {
                                        DomainUser = correoUsuario(DatosPersona.Cedula);
                                    }
                                    catch (Exception e)
                                    {
                                        try
                                        {
                                            DomainUser = Code.Utilities2.obtenerCorreoUsuario(DatosPersona.Cedula);
                                        }
                                        catch (Exception e1)
                                        {
                                            DomainUser = "SINREGISTRODOMAIN";
                                        }
                                    }

                                }

                                if (DomainUser == "SINREGISTRODOMAIN")
                                {
                                    TempData["Msj"] = "EL USUARIO DEL DOMINIO TIENE INCONVENIENTES, FAVOR CONTACTARSE CON LA EXTENSION 174.";
                                }
                             

                                else
                                {
                                    string NombreCompletoCo = "";

                                    string DepartamentoCo = "";

                                    if (fc["CodigoOtrosEmpleados"] == "Seleccione"  || fc["EmpleadoOtrosId"] == "Seleccione" ||
                                        fc["EmpleadoOtrosId"] == "" || fc["CodigoOtrosEmpleados"] == "" || fc["ActividadesId"] == "2" || fc["Actividades"] == "2"
                                        || fc["Actividades"] == "32" || fc["ActividadesId"] == "32")
                                    {

                                        Tb.DomainUserSolicitante = Session["usuario"].ToString();
                                        Tb.SolicitanteId = int.Parse(Session["empleadoid"].ToString());
                                        Tb.TecnicoId = Supervisor.SupervisorId;
                                        Tb.DomainUserTecnico = Supervisor.Correo;
                                        DomainUser = Tb.DomainUserSolicitante;

                                        var employe = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == Tb.SolicitanteId.ToString()).FirstOrDefault();
                                        NombreCompletoCo = employe.nombre.ToUpper();
                                        DepartamentoCo = employe.departamento.ToUpper();


                                    }
                                    else
                                    {
                                        Tb.DomainUserSolicitante = DomainUser;
                                        Tb.SolicitanteId = Tb.TecnicoId;  
                                        Tb.TecnicoId = Supervisor.SupervisorId; ;
                                        Tb.DomainUserTecnico = Supervisor.Correo;
                                        NombreCompletoCo = DatosPersona.NombreCompleto;
                                        DepartamentoCo = DatosPersona.Departamento;

                                    }

                                    Tb.DepartamentoId = CodigoDepartamento;
                                    Tb.Tipo_SolicitudId = CodigoTipoSolicitud;
                                    Tb.Tipo_Sub_SolicitudId = CodigoSubTipoSolicitud;
                                    Tb.DescripcionSolicitud = DescripcionSolicitud;
                                    Tb.EstadoId = 1;
                                    Tb.Estado = true;
                                    Tb.Horas = Horas;
                                    Tb.TipodeAsistencia = 4;
                                    Tb.FhInicioSolicitud = DateTime.Now;
                                    Tb.FhFinalSolicitud = DateTime.Now;
                                    Tb.FhModificacion = DateTime.Now;
                                    Tb.FhCreacion = DateTime.Now;
                                    Tb.DependenciaRegionalId = solicitudModel.CodigoDependencia;
                                    obj.Sol_Registro_Solicitud.Add(Tb);
                                    obj.SaveChanges();

                                    //string NombreCompletoCo = "";

                                    //string DepartamentoCo = "";

                                    var A = obj.Sol_Registro_Solicitud.OrderByDescending(cd => cd.SolicitudId).Take(1).FirstOrDefault();
                                   

                                   

                                    string Valor = A.SolicitudId.ToString() + "#Ancla";

                                    string Descripcion = Mth_Mostrar_Actividad(A.Tipo_SolicitudId);

                                    InsertarLog(A.SolicitudId, "SE CREO LA SOLICITUD");

                                    //Metodo EnviarCorreo para Pase a Produccion


                                    string solicitante = fc["Solicitante"];

                                    var employee = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == solicitante).FirstOrDefault();

                                    if (Tb.Tipo_SolicitudId == 1020 || Tb.Tipo_SolicitudId == 35)
                                    {

                                        AprobacioneServicios ap = new AprobacioneServicios();
                                        ap.DetallePaseProduccion = A.DescripcionSolicitud;
                                        ap.FechaCreacion = DateTime.Now;
                                        ap.ActividadId = A.Tipo_SolicitudId;
                                        ap.Solicitante = employee.nombre.ToUpper();
                                        ap.SubActividadId = A.Tipo_Sub_SolicitudId;
                                        ap.usuario = Session["usuario"].ToString();
                                        ap.ApEstatusId = 3;
                                        ap.Prioridad = fc["Prioridad"];
                                        ap.FaseProyecto = fc["FaseProyecto"];
                                        ap.Impacto = fc["Impacto"];
                                        ap.Riesgo = fc["Riesgo"];
                                        ap.SolicitudId = A.SolicitudId;
                                        db.AprobacioneServicios.Add(ap);
                                        db.SaveChanges();

                                      

                                        var aprobacioneServicio = (from s in  db.AprobacioneServicios
                                                                 orderby s.AprobacionesId descending 
                                                                select new { ID = s.AprobacionesId}).FirstOrDefault();

                                        for (int a =0; a <= AreasImplicadas.Length - 1; a++)
                                        {
                                            AreasAprobacionesProduccion areasAprobaciones = new AreasAprobacionesProduccion();

                                            areasAprobaciones.AprobacionesId = aprobacioneServicio.ID;
                                            areasAprobaciones.AreasImplicadasId = AreasImplicadas[a];
                                            db.AreasAprobacionesProduccion.Add(areasAprobaciones);
                                          
                                        }

                                        db.SaveChanges();


                                        for (int i = 0; i <= CategoriasProduccion.Length - 1; i++)
                                        {
                                            CategoriasAprobacionesProduccion categoria = new CategoriasAprobacionesProduccion();


                                            categoria.AprobacionesId = aprobacioneServicio.ID;
                                            categoria.CategoriasProduccionId = CategoriasProduccion[i];
                                            db.CategoriasAprobacionesProduccion.Add(categoria);
                                           
                                        }
                                        db.SaveChanges();

                                        string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/PaseProduccion?aprobacion={ap.AprobacionesId}";
                                        SendCorreo( "SOLICITUD DE PASE A PRODUCCION", SolicitudAprobacionPaseProduccion(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, url, Descripcion));
                                        //cor.EnviarCorreoProduccion("maria.rodriguez@pgr.gob.do", "USTED HA RECIBIDO UN MENSAJE de Pase a Produccion", );

                                    }
                                    else
                                    {
                                        EnviarCorreo(correo_encargado, DomainUser, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, Direccion + Valor, Descripcion));


                                    }
                                    TempData["Msj"] = "SE CREO LA SOLICITUD CORRECTAMENTE.";
                                }
                            }
                            else
                            {
                                Tb.SolicitanteId = Cdg;
                                Tb.DomainUserSolicitante = correo_usuario_en_sesion;
                                Tb.TecnicoId = Supervisor.SupervisorId;
                                Tb.DomainUserTecnico = correo_encargado;
                                Tb.DepartamentoId = CodigoDepartamento;
                                Tb.Tipo_SolicitudId = CodigoTipoSolicitud;
                                Tb.Tipo_Sub_SolicitudId = CodigoSubTipoSolicitud;
                                Tb.DescripcionSolicitud = DescripcionSolicitud;
                                Tb.EstadoId = 1;
                                Tb.Estado = true;
                                Tb.Horas = Horas;
                                Tb.TipodeAsistencia = 4;
                                Tb.FhInicioSolicitud = DateTime.Now;
                                Tb.FhFinalSolicitud = DateTime.Now;
                                Tb.FhModificacion = DateTime.Now;
                                Tb.FhCreacion = DateTime.Now;
                                obj.Sol_Registro_Solicitud.Add(Tb);
                                obj.SaveChanges();

                                var A = obj.Sol_Registro_Solicitud.OrderByDescending(cd => cd.SolicitudId).Take(1).FirstOrDefault();

                                string Valor = A.SolicitudId.ToString() + "#Ancla";
                                string Descripcion = Mth_Mostrar_Actividad(A.Tipo_SolicitudId);

                                InsertarLog(A.SolicitudId, "SE CREO LA SOLICITUD");

                                //Metodo EnviarCorreo

                                string NombreCompletoCo = "";

                                string DepartamentoCo = "";

                                NombreCompletoCo = HttpContext.Session["nombre"].ToString();
                                DepartamentoCo = HttpContext.Session["departamento"].ToString();
                                string solicitante = fc["Solicitante"];

                                var employee = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == solicitante).FirstOrDefault();

                                if (Tb.Tipo_SolicitudId == 1020 || Tb.Tipo_SolicitudId == 35)
                                {

                                    AprobacioneServicios ap = new AprobacioneServicios();
                                    ap.DetallePaseProduccion = A.DescripcionSolicitud;
                                    ap.FechaCreacion = DateTime.Now;
                                    ap.ActividadId = A.Tipo_SolicitudId;
                                    ap.Solicitante = employee.nombre.ToUpper();
                                    ap.SubActividadId = A.Tipo_Sub_SolicitudId;
                                    ap.usuario = Session["usuario"].ToString();
                                    ap.ApEstatusId = 3;
                                    ap.Prioridad = fc["Prioridad"];
                                    ap.FaseProyecto = fc["FaseProyecto"];
                                    ap.Impacto = fc["Impacto"];
                                    ap.Riesgo = fc["Riesgo"];
                                    ap.SolicitudId = A.SolicitudId;
                                    db.AprobacioneServicios.Add(ap);
                                    db.SaveChanges();




                                    var aprobacioneServicio = (from s in db.AprobacioneServicios
                                                               orderby s.AprobacionesId descending
                                                               select new { ID = s.AprobacionesId }).FirstOrDefault();

                                    for (int a = 0; a <= AreasImplicadas.Length - 1; a++)
                                    {
                                        AreasAprobacionesProduccion areasAprobaciones = new AreasAprobacionesProduccion();

                                        areasAprobaciones.AprobacionesId = aprobacioneServicio.ID;
                                        areasAprobaciones.AreasImplicadasId = AreasImplicadas[a];
                                        db.AreasAprobacionesProduccion.Add(areasAprobaciones);

                                    }

                                    db.SaveChanges();


                                    for (int i = 0; i <= CategoriasProduccion.Length - 1; i++)
                                    {
                                        CategoriasAprobacionesProduccion categoria = new CategoriasAprobacionesProduccion();


                                        categoria.AprobacionesId = aprobacioneServicio.ID;
                                        categoria.CategoriasProduccionId = CategoriasProduccion[i];
                                        db.CategoriasAprobacionesProduccion.Add(categoria);

                                    }
                                    db.SaveChanges();
                                    string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/PaseProduccion?aprobacion={ap.AprobacionesId}";
                                    SendCorreo("SOLICITUD PASE A PRODUCCION", SolicitudAprobacionPaseProduccion(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, url, Descripcion));
                                    //cor.EnviarCorreoProduccion("maria.rodriguez@pgr.gob.do", "USTED HA RECIBIDO UN MENSAJE de Pase a Produccion", );

                                }
                                else
                                {


                                    EnviarCorreo(correo_encargado, correo_usuario_en_sesion, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO" + Supervisor.Descripcion, DescripcionCorreo(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, Direccion + Valor, Descripcion));
                                }
                                TempData["Msj"] = "SE CREO LA SOLICITUD CORRECTAMENTE.";

                            }
                        }
                        else if ((CdgSupervisor(Cdg) == 2) || (CdgDirectores(Cdg) == 2) || (CdgOtrosTecnicos(Cdg) == 2))
                        {
                            Tb.SolicitanteId = Cdg;
                            Tb.DomainUserSolicitante = correo_usuario_en_sesion;
                            Tb.TecnicoId = Supervisor.SupervisorId;
                            Tb.DomainUserTecnico = correo_encargado;
                            Tb.DepartamentoId = CodigoDepartamento;
                            Tb.Tipo_SolicitudId = CodigoTipoSolicitud;
                            Tb.Tipo_Sub_SolicitudId = CodigoSubTipoSolicitud;
                            Tb.DescripcionSolicitud = DescripcionSolicitud;
                            Tb.EstadoId = 1;
                            Tb.Estado = true;
                            Tb.Horas = Horas;
                            Tb.TipodeAsistencia = 4;
                            Tb.FhInicioSolicitud = DateTime.Now;
                            Tb.FhFinalSolicitud = DateTime.Now;
                            Tb.FhModificacion = DateTime.Now;
                            Tb.FhCreacion = DateTime.Now;
                            obj.Sol_Registro_Solicitud.Add(Tb);
                            obj.SaveChanges();


                            var A = obj.Sol_Registro_Solicitud.OrderByDescending(cd => cd.SolicitudId).Take(1).FirstOrDefault();

                            int Valor = A.SolicitudId;
                            string Descripcion = Mth_Mostrar_Actividad(A.Tipo_SolicitudId);

                            InsertarLog(Valor, "SE CREO LA SOLICITUD");

                            //Metodo EnviarCorreo

                            string NombreCompletoCo = "";

                            string DepartamentoCo = "";


                            NombreCompletoCo = HttpContext.Session["nombre"].ToString();
                            DepartamentoCo = HttpContext.Session["departamento"].ToString();
                            string solicitante = fc["Solicitante"];

                            var employee = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == solicitante).FirstOrDefault();
                            if (Tb.Tipo_SolicitudId == 1020 || Tb.Tipo_SolicitudId == 35)
                            {

                                AprobacioneServicios ap = new AprobacioneServicios();
                                ap.DetallePaseProduccion = A.DescripcionSolicitud;
                                ap.FechaCreacion = DateTime.Now;
                                ap.ActividadId = A.Tipo_SolicitudId;
                                ap.Solicitante = employee.nombre.ToUpper();
                                ap.SubActividadId = A.Tipo_Sub_SolicitudId;
                                ap.usuario = Session["usuario"].ToString();
                                ap.ApEstatusId = 3;
                                ap.Prioridad = fc["Prioridad"];
                                ap.FaseProyecto = fc["FaseProyecto"];
                                ap.Impacto = fc["Impacto"];
                                ap.Riesgo = fc["Riesgo"];
                                ap.SolicitudId = A.SolicitudId;
                                db.AprobacioneServicios.Add(ap);
                                db.SaveChanges();

                                var aprobacioneServicio = (from s in db.AprobacioneServicios
                                                           orderby s.AprobacionesId descending
                                                           select new { ID = s.AprobacionesId }).FirstOrDefault();

                                for (int a = 0; a <= AreasImplicadas.Length - 1; a++)
                                {
                                    AreasAprobacionesProduccion areasAprobaciones = new AreasAprobacionesProduccion();

                                    areasAprobaciones.AprobacionesId = aprobacioneServicio.ID;
                                    areasAprobaciones.AreasImplicadasId = AreasImplicadas[a];
                                    db.AreasAprobacionesProduccion.Add(areasAprobaciones);

                                }

                                db.SaveChanges();


                                for (int i = 0; i <= CategoriasProduccion.Length - 1; i++)
                                {
                                    CategoriasAprobacionesProduccion categoria = new CategoriasAprobacionesProduccion();


                                    categoria.AprobacionesId = aprobacioneServicio.ID;
                                    categoria.CategoriasProduccionId = CategoriasProduccion[i];
                                    db.CategoriasAprobacionesProduccion.Add(categoria);

                                }
                                db.SaveChanges();
                                string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/PaseProduccion?aprobacion={ap.AprobacionesId}";
                                SendCorreo("SOLICITUD DE PASE A PRODUCCION", SolicitudAprobacionPaseProduccion(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, url, Descripcion));
                                //cor.EnviarCorreoProduccion("maria.rodriguez@pgr.gob.do", "USTED HA RECIBIDO UN MENSAJE de Pase a Produccion", );
                                TempData["Msj"] = "SE CREO LA SOLICITUD CORRECTAMENTE.";
                            }
                            else
                            {

                                EnviarCorreo(correo_encargado, correo_usuario_en_sesion, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO" + Supervisor.Descripcion, DescripcionCorreo(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, Direccion + Valor, Descripcion));
                            }
                            TempData["Msj"] = "SE CREO LA SOLICITUD CORRECTAMENTE.";
                        }

                        MostrarCantidaddeSolicitud();
                        MostrarCantidadAsignados();

                        return RedirectToAction("Solicitud", "HelpDesk");
                    }
                    catch (SqlException sql)
                    {
                        sql.Message.ToString();
                    }
                }
                else if (solicitudModel.CodigoDependencia > 1)
                {
                    try
                    {
                        #region Commented Code
                        //SqlCommand cmd = new SqlCommand("Sp_Insertar_Registro_Solicitud", Enlace);
                        //cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.Clear();
                        //cmd.Parameters.AddWithValue("@CodigoSolicitante", Cdg);
                        //cmd.Parameters.AddWithValue("@CodigoDepartamento", CodigoDepartamento);
                        //cmd.Parameters.AddWithValue("@CodigoTipoSolicitud", CodigoTipoSolicitud);
                        //cmd.Parameters.AddWithValue("@DescripcionSolicitud", DescripcionSolicitud);
                        //cmd.Parameters.AddWithValue("@CodigoEstado ", 1);
                        //cmd.Parameters.AddWithValue("@Estado", true);
                        //cmd.Parameters.AddWithValue("@Horas", Horas);
                        //cmd.Parameters.AddWithValue("@FhInicioSolicitud", Convert.ToDateTime(DateTime.Now.ToString()));
                        //cmd.Parameters.AddWithValue("@FhModificacion ", Convert.ToDateTime(DateTime.Now.ToString()));
                        //cmd.ExecuteNonQuery();
                        #endregion

                        var Supervisor = obj.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == solicitudModel.CodigoDependencia && cd.DependenciaEstado == true).FirstOrDefault();

                        var Supervisor2 = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == Supervisor.DependenciaSupervisorId.ToString()).FirstOrDefault();

                        string correo_encargado = "";

                        #region Commented Code
                        //if (Supervisor2.CodigoDependencia == "10001" && Supervisor2.CodigoDepartamento == "15")
                        //    correo_encargado = Code.Utilities2.obtenerCorreoUsuario(Supervisor2.Cedula);
                        //else
                        //    correo_encargado = correoUsuario(Supervisor2.Cedula);
                        #endregion

                        correo_encargado = Supervisor.DependenciaSupervisorCorreo;

                        var UsuarioEnSesion = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == Cdg.ToString()).FirstOrDefault();

                        string correo_usuario_en_sesion = "";

                        if (UsuarioEnSesion.CodigoDependencia == "10001" && UsuarioEnSesion.CodigoDepartamento == "15")
                        {
                            correo_usuario_en_sesion = Code.Utilities2.obtenerCorreoUsuario(UsuarioEnSesion.Cedula);
                        }
                        else
                        {
                            try
                            {
                                correo_usuario_en_sesion = correoUsuario(UsuarioEnSesion.Cedula);
                            }
                            catch (Exception e)
                            {
                                try
                                {
                                    correo_usuario_en_sesion = Code.Utilities2.obtenerCorreoUsuario(UsuarioEnSesion.Cedula);
                                }
                                catch (Exception e1)
                                {
                                    correo_usuario_en_sesion = "";
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(correo_usuario_en_sesion))
                        {
                            correo_usuario_en_sesion = "SINREGISTRODOMAIN";
                        }

                        Sol_Registro_Solicitud Tb = new Sol_Registro_Solicitud();

                        if ((CdgSupervisor(Cdg) == 1) || (CdgDirectores(Cdg) == 1) || (CdgOtrosTecnicos(Cdg) == 1))
                        {
                            if (solicitudModel.CodigoOtrosEmpleados == 0)
                            {
                                solicitudModel.CodigoOtrosEmpleados = Convert.ToInt32(HttpContext.Session["empleadoId"].ToString());
                            }

                            if (solicitudModel.CodigoOtrosEmpleados > 0)
                            {
                                string Codigos = "";

                                Codigos = solicitudModel.CodigoOtrosEmpleados.ToString();

                                var DatosPersona = obj.Sol_Mostrar_Personal_PGR.Where(cd => cd.Codigo == Codigos).FirstOrDefault();

                                Tb.SolicitanteId = int.Parse(DatosPersona.Codigo);

                                string DomainUser = "";

                                if (DatosPersona.CodigoDependencia == "10001" && DatosPersona.CodigoDepartamento == "15")
                                {
                                    DomainUser = Code.Utilities2.obtenerCorreoUsuario(DatosPersona.Cedula);
                                }
                                else
                                {
                                    try
                                    {
                                        DomainUser = correoUsuario(DatosPersona.Cedula);
                                    }
                                    catch (Exception e)
                                    {
                                        try
                                        {
                                            DomainUser = Code.Utilities2.obtenerCorreoUsuario(DatosPersona.Cedula);
                                        }
                                        catch (Exception e1)
                                        {
                                            DomainUser = "SINREGISTRODOMAIN";
                                        }
                                    }
                                }

                                if (DomainUser == "SINREGISTRODOMAIN")
                                {
                                    TempData["Msj"] = "EL USUARIO DEL DOMINIO TIENE INCONVENIENTES, FAVOR CONTACTARSE CON LA EXTENSION 174.";
                                }
                                else
                                {
                                    Tb.DomainUserSolicitante = DomainUser;
                                    Tb.TecnicoId = Supervisor.DependenciaSupervisorId;
                                    Tb.DomainUserTecnico = correo_encargado;
                                    Tb.DepartamentoId = 15;
                                    Tb.Tipo_SolicitudId = -1;
                                    Tb.Tipo_Sub_SolicitudId = null;
                                    Tb.DescripcionSolicitud = DescripcionSolicitud;
                                    Tb.EstadoId = 1;
                                    Tb.Estado = true;
                                    Tb.Horas = Horas;
                                    Tb.TipodeAsistencia = 4;
                                    Tb.FhInicioSolicitud = DateTime.Now;
                                    Tb.FhFinalSolicitud = DateTime.Now;
                                    Tb.FhModificacion = DateTime.Now;
                                    Tb.FhCreacion = DateTime.Now;
                                    Tb.DependenciaRegionalId = solicitudModel.CodigoDependencia;
                                    obj.Sol_Registro_Solicitud.Add(Tb);
                                    obj.SaveChanges();

                                    var A = obj.Sol_Registro_Solicitud.OrderByDescending(cd => cd.SolicitudId).Take(1).FirstOrDefault();

                                    string Valor = A.SolicitudId.ToString() + "#Ancla";

                                    string Descripcion = Mth_Mostrar_Dependencia((int)A.DependenciaRegionalId);

                                    InsertarLog(A.SolicitudId, "SE CREO LA SOLICITUD");

                                    //Metodo EnviarCorreo

                                    string NombreCompletoCo = "";

                                    string DepartamentoCo = "";

                                    NombreCompletoCo = DatosPersona.NombreCompleto;
                                    DepartamentoCo = DatosPersona.Departamento;
                                    string solicitante = fc["Solicitante"];

                                    var employee = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == solicitante).FirstOrDefault();

                                    if (Tb.Tipo_SolicitudId == 1020 || Tb.Tipo_SolicitudId == 35)
                                    {

                                        AprobacioneServicios ap = new AprobacioneServicios();
                                        ap.DetallePaseProduccion = A.DescripcionSolicitud;
                                        ap.FechaCreacion = DateTime.Now;
                                        ap.ActividadId = A.Tipo_SolicitudId;
                                        ap.Solicitante = employee.nombre.ToUpper();
                                        ap.SubActividadId = A.Tipo_Sub_SolicitudId;
                                        ap.usuario = Session["usuario"].ToString();
                                        ap.ApEstatusId = 3;
                                        ap.Prioridad = fc["Prioridad"];
                                        ap.FaseProyecto = fc["FaseProyecto"];
                                        ap.Impacto = fc["Impacto"];
                                        ap.Riesgo = fc["Riesgo"];
                                        ap.SolicitudId = A.SolicitudId;
                                        db.AprobacioneServicios.Add(ap);
                                        db.SaveChanges();


                                        var aprobacioneServicio = (from s in db.AprobacioneServicios
                                                                   orderby s.AprobacionesId descending
                                                                   select new { ID = s.AprobacionesId }).FirstOrDefault();

                                        for (int a = 0; a <= AreasImplicadas.Length - 1; a++)
                                        {
                                            AreasAprobacionesProduccion areasAprobaciones = new AreasAprobacionesProduccion();

                                            areasAprobaciones.AprobacionesId = aprobacioneServicio.ID;
                                            areasAprobaciones.AreasImplicadasId = AreasImplicadas[a];
                                            db.AreasAprobacionesProduccion.Add(areasAprobaciones);

                                        }

                                        db.SaveChanges();


                                        for (int i = 0; i <= CategoriasProduccion.Length - 1; i++)
                                        {
                                            CategoriasAprobacionesProduccion categoria = new CategoriasAprobacionesProduccion();


                                            categoria.AprobacionesId = aprobacioneServicio.ID;
                                            categoria.CategoriasProduccionId = CategoriasProduccion[i];
                                            db.CategoriasAprobacionesProduccion.Add(categoria);

                                        }
                                        db.SaveChanges();
                                        string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/PaseProduccion?aprobacion={ap.AprobacionesId}";
                                        SendCorreo( "SOLICITUD DE PASE A PRODUCCION", SolicitudAprobacionPaseProduccion(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, url, Descripcion));
                                        //cor.EnviarCorreoProduccion("maria.rodriguez@pgr.gob.do", "USTED HA RECIBIDO UN MENSAJE de Pase a Produccion", );

                                    }
                                    else
                                    {

                                        EnviarCorreo(correo_encargado, DomainUser, "USTED HA RECIBIDO UN MENSAJE", DescripcionCorreo(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, Direccion + Valor, Descripcion));
                                    }
                                    TempData["Msj"] = "SE CREO LA SOLICITUD CORRECTAMENTE.";
                                }
                            }
                            else
                            {
                                Tb.SolicitanteId = Cdg;
                                Tb.DomainUserSolicitante = correo_usuario_en_sesion;
                                Tb.TecnicoId = Supervisor.DependenciaSupervisorId;
                                Tb.DomainUserTecnico = correo_encargado;
                                Tb.DepartamentoId = 15;
                                Tb.Tipo_SolicitudId = -1;
                                Tb.Tipo_Sub_SolicitudId = null;
                                Tb.DescripcionSolicitud = DescripcionSolicitud;
                                Tb.EstadoId = 1;
                                Tb.Estado = true;
                                Tb.Horas = Horas;
                                Tb.TipodeAsistencia = 4;
                                Tb.FhInicioSolicitud = DateTime.Now;
                                Tb.FhFinalSolicitud = DateTime.Now;
                                Tb.FhModificacion = DateTime.Now;
                                Tb.FhCreacion = DateTime.Now;
                                Tb.DependenciaRegionalId = solicitudModel.CodigoDependencia;
                                obj.Sol_Registro_Solicitud.Add(Tb);
                                obj.SaveChanges();

                                var A = obj.Sol_Registro_Solicitud.OrderByDescending(cd => cd.SolicitudId).Take(1).FirstOrDefault();

                                string Valor = A.SolicitudId.ToString() + "#Ancla";
                                string Descripcion = Mth_Mostrar_Dependencia((int)A.DependenciaRegionalId);

                                InsertarLog(A.SolicitudId, "SE CREO LA SOLICITUD");

                                //Metodo EnviarCorreo

                                string NombreCompletoCo = "";

                                string DepartamentoCo = "";


                                NombreCompletoCo = HttpContext.Session["nombre"].ToString();
                                DepartamentoCo = HttpContext.Session["departamento"].ToString();
                                string solicitante = fc["Solicitante"];

                                var employee = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == solicitante).FirstOrDefault();

                                if (Tb.Tipo_SolicitudId == 1020 || Tb.Tipo_SolicitudId == 35)
                                {

                                    AprobacioneServicios ap = new AprobacioneServicios();
                                    ap.DetallePaseProduccion = A.DescripcionSolicitud;
                                    ap.FechaCreacion = DateTime.Now;
                                    ap.ActividadId = A.Tipo_SolicitudId;
                                    ap.Solicitante = employee.nombre.ToUpper();
                                    ap.SubActividadId = A.Tipo_Sub_SolicitudId;
                                    ap.usuario = Session["usuario"].ToString();
                                    ap.ApEstatusId = 3;
                                    ap.Prioridad = fc["Prioridad"];
                                    ap.FaseProyecto = fc["FaseProyecto"];
                                    ap.Impacto = fc["Impacto"];
                                    ap.Riesgo = fc["Riesgo"];
                                    ap.SolicitudId = A.SolicitudId;
                                    db.AprobacioneServicios.Add(ap);
                                    db.SaveChanges();


                                    var aprobacioneServicio = (from s in db.AprobacioneServicios
                                                               orderby s.AprobacionesId descending
                                                               select new { ID = s.AprobacionesId }).FirstOrDefault();

                                    for (int a = 0; a <= AreasImplicadas.Length - 1; a++)
                                    {
                                        AreasAprobacionesProduccion areasAprobaciones = new AreasAprobacionesProduccion();

                                        areasAprobaciones.AprobacionesId = aprobacioneServicio.ID;
                                        areasAprobaciones.AreasImplicadasId = AreasImplicadas[a];
                                        db.AreasAprobacionesProduccion.Add(areasAprobaciones);

                                    }

                                    db.SaveChanges();


                                    for (int i = 0; i <= CategoriasProduccion.Length - 1; i++)
                                    {
                                        CategoriasAprobacionesProduccion categoria = new CategoriasAprobacionesProduccion();


                                        categoria.AprobacionesId = aprobacioneServicio.ID;
                                        categoria.CategoriasProduccionId = CategoriasProduccion[i];
                                        db.CategoriasAprobacionesProduccion.Add(categoria);

                                    }
                                    db.SaveChanges();
                                    string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/PaseProduccion?aprobacion={ap.AprobacionesId}";
                                    SendCorreo("SOLICITUD DE PASE A PRODUCCION", SolicitudAprobacionPaseProduccion(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, url, Descripcion));
                                    //cor.EnviarCorreoProduccion("maria.rodriguez@pgr.gob.do", "USTED HA RECIBIDO UN MENSAJE de Pase a Produccion", );

                                }
                                else
                                {

                                    EnviarCorreo(correo_encargado, correo_usuario_en_sesion, "USTED HA RECIBIDO UN MENSAJE", DescripcionCorreo(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, Direccion + Valor, Descripcion));
                                }
                                TempData["Msj"] = "SE CREO LA SOLICITUD CORRECTAMENTE.";

                            }
                        }
                        else if ((CdgSupervisor(Cdg) == 2) || (CdgDirectores(Cdg) == 2) || (CdgOtrosTecnicos(Cdg) == 2))
                        {
                            Tb.SolicitanteId = Cdg;
                            Tb.DomainUserSolicitante = correo_usuario_en_sesion;
                            Tb.TecnicoId = Supervisor.DependenciaSupervisorId;
                            Tb.DomainUserTecnico = correo_encargado;
                            Tb.DepartamentoId = 15;
                            Tb.Tipo_SolicitudId = -1;
                            Tb.Tipo_Sub_SolicitudId = null;
                            Tb.DescripcionSolicitud = DescripcionSolicitud;
                            Tb.EstadoId = 1;
                            Tb.Estado = true;
                            Tb.Horas = Horas;
                            Tb.TipodeAsistencia = 4;
                            Tb.FhInicioSolicitud = DateTime.Now;
                            Tb.FhFinalSolicitud = DateTime.Now;
                            Tb.FhModificacion = DateTime.Now;
                            Tb.FhCreacion = DateTime.Now;
                            Tb.DependenciaRegionalId = solicitudModel.CodigoDependencia;
                            obj.Sol_Registro_Solicitud.Add(Tb);
                            obj.SaveChanges();


                            var A = obj.Sol_Registro_Solicitud.OrderByDescending(cd => cd.SolicitudId).Take(1).FirstOrDefault();

                            int Valor = A.SolicitudId;
                            string Descripcion = Mth_Mostrar_Dependencia((int)A.DependenciaRegionalId);

                            InsertarLog(Valor, "SE CREO LA SOLICITUD");

                            //Metodo EnviarCorreo

                            string NombreCompletoCo = "";

                            string DepartamentoCo = "";

                            NombreCompletoCo = HttpContext.Session["nombre"].ToString();
                            DepartamentoCo = HttpContext.Session["departamento"].ToString();
                            string solicitante = fc["Solicitante"];

                            var employee = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == solicitante).FirstOrDefault();

                            if (Tb.Tipo_SolicitudId == 1020 || Tb.Tipo_SolicitudId == 35)
                            {

                                AprobacioneServicios ap = new AprobacioneServicios();
                                ap.DetallePaseProduccion = A.DescripcionSolicitud;
                                ap.FechaCreacion = DateTime.Now;
                                ap.ActividadId = A.Tipo_SolicitudId;
                                ap.Solicitante = employee.nombre.ToUpper();
                                ap.SubActividadId = A.Tipo_Sub_SolicitudId;
                                ap.usuario = Session["usuario"].ToString();
                                ap.ApEstatusId = 3;
                                ap.Prioridad = fc["Prioridad"];
                                ap.FaseProyecto = fc["FaseProyecto"];
                                ap.Impacto = fc["Impacto"];
                                ap.Riesgo = fc["Riesgo"];
                                ap.SolicitudId = A.SolicitudId;
                                db.AprobacioneServicios.Add(ap);
                                db.SaveChanges();


                                var aprobacioneServicio = (from s in db.AprobacioneServicios
                                                           orderby s.AprobacionesId descending
                                                           select new { ID = s.AprobacionesId }).FirstOrDefault();

                                for (int a = 0; a <= AreasImplicadas.Length - 1; a++)
                                {
                                    AreasAprobacionesProduccion areasAprobaciones = new AreasAprobacionesProduccion();

                                    areasAprobaciones.AprobacionesId = aprobacioneServicio.ID;
                                    areasAprobaciones.AreasImplicadasId = AreasImplicadas[a];
                                    db.AreasAprobacionesProduccion.Add(areasAprobaciones);

                                }

                                db.SaveChanges();


                                for (int i = 0; i <= CategoriasProduccion.Length - 1; i++)
                                {
                                    CategoriasAprobacionesProduccion categoria = new CategoriasAprobacionesProduccion();


                                    categoria.AprobacionesId = aprobacioneServicio.ID;
                                    categoria.CategoriasProduccionId = CategoriasProduccion[i];
                                    db.CategoriasAprobacionesProduccion.Add(categoria);

                                }
                                db.SaveChanges();
                                string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/PaseProduccion?aprobacion={ap.AprobacionesId}";
                                SendCorreo("SOLICITUD DE PASE A PRODUCCION", SolicitudAprobacionPaseProduccion(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, url, Descripcion));
                                //cor.EnviarCorreoProduccion("maria.rodriguez@pgr.gob.do", "USTED HA RECIBIDO UN MENSAJE de Pase a Produccion", );

                            }
                            else
                            { 

                                EnviarCorreo(correo_encargado, correo_usuario_en_sesion, "USTED HA RECIBIDO UN MENSAJE", DescripcionCorreo(DepartamentoCo, NombreCompletoCo, solicitudModel.Descripcion, Direccion + Valor, Descripcion));
                            }
                            TempData["Msj"] = "SE CREO LA SOLICITUD CORRECTAMENTE.";
                        }

                        MostrarCantidaddeSolicitud();
                        MostrarCantidadAsignados();

                        return RedirectToAction("Solicitud", "HelpDesk");
                    }
                    catch (SqlException sql)
                    {
                        sql.Message.ToString();
                    }
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            return View("Solicitud", smodel);
        }

        [HttpPost]
        public JsonResult CargarFlotaExtension(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                try
                {
                    int id2 = Convert.ToInt32(id);

                    using (dbIntranet db = new dbIntranet())
                    {
                        var datos_flota = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpEmpleadoId == id2).Select(a => new
                        {
                            flota = a.FlotExtEmpFlota,
                            extension = a.FlotExtEmpExtesion,
                            flota_verificada = a.FlotExtEmpDatosConfirmados
                        }).FirstOrDefault();

                        return Json(datos_flota);
                    }
                }
                catch (Exception)
                {
                    return Json("Error");
                }
            }
            else
            {
                return Json("Error");
            }
        }

        #region MostrarDependenciasRegionales
        public string Mth_Mostrar_Dependencia(int id)
        {
            var dependencia = obj.Sol_DependenciasRegionales.Find(id);

            if (dependencia != null)
                return dependencia.DependenciaNombre;
            else
                return null;
        }
        #endregion
    }

}