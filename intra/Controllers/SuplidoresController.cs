﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;

namespace intra.Controllers
{
    public class SuplidoresController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: Suplidores
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo de Suplidores");
            return View(db.Suplidores.ToList());
        }

        [CustomAuthorize]
        // GET: Suplidores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Suplidores suplidores = db.Suplidores.Find(id);
            if (suplidores == null)
            {
                return HttpNotFound();
            }
            return View(suplidores);
        }

        [CustomAuthorize]
        // GET: Suplidores/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Suplidores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SuplidorId,SuplidorNombre,SuplidorDireccion,SuplidorTelefono,SuplidorUsuario,SuplidorContacto")] Suplidores suplidores)
        {
            suplidores.SuplidorUsuario = Session["usuario"].ToString();

            if (ModelState.IsValid)
            {

                string empleadoid = @Session["empleadoId"].ToString();

                db.Suplidores.Add(suplidores);
                db.SaveChanges();
                           
                RegisterLogs rl = new RegisterLogs(empleadoid, "Creo un registro en el Modulo de Suplidores llamado ("+suplidores.SuplidorNombre+")");

                return RedirectToAction("Index");

            }


            return View(suplidores);
        }

        [CustomAuthorize]
        // GET: Suplidores/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Suplidores suplidores = db.Suplidores.Find(id);
            if (suplidores == null)
            {
                return HttpNotFound();
            }
            return View(suplidores);
        }

        // POST: Suplidores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SuplidorId,SuplidorNombre,SuplidorDireccion,SuplidorTelefono,SuplidorUsuario,SuplidorContacto")] Suplidores suplidores)
        {
            if (ModelState.IsValid)
            {
               
                db.Entry(suplidores).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito un registro en el Modulo de Suplidores id (" + suplidores.SuplidorId + ")");
                return RedirectToAction("Index");
            }
            return View(suplidores);
        }

        [CustomAuthorize]
        // GET: Suplidores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Suplidores suplidores = db.Suplidores.Find(id);
            if (suplidores == null)
            {
                return HttpNotFound();
            }
            return View(suplidores);
        }

        // POST: Suplidores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Suplidores suplidores = db.Suplidores.Find(id);
            db.Suplidores.Remove(suplidores);
            db.SaveChanges();
            string empId = HttpContext.Session["empleadoId"].ToString();

            RegisterLogs rl = new RegisterLogs(empId, "Elimino un registro en el Modulo de Suplidores llamado (" + suplidores.SuplidorNombre + ")");
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
