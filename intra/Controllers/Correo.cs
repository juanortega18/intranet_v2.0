﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Configuration;
using System.Threading;

/// <summary>
/// Summary description for Correo
/// </summary>
public class Correo
{

    //Variable WebConfig
    public static string Direccion = ConfigurationManager.AppSettings["Direccion"];
    public static string Usuario = ConfigurationManager.AppSettings["Usuario"];
    public static string Contrasena = ConfigurationManager.AppSettings["Contrasena"];
    public static string Host = ConfigurationManager.AppSettings["Host"];

    public Correo()
	{
        
    }


   

    public void CorreoPaseProduccion(string correoResponsable, string sujeto, string detalle, string Logo, string Flecha, string LogoDTI)
    {
        if ( (!string.IsNullOrEmpty(correoResponsable)) && (sujeto != null) && (detalle != null))
        {
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress(Direccion);
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 25;
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(Usuario, Contrasena);
            smtp.Host = Host;

            //if (correoSupervisor != "")
            //{
            //    mail.To.Add(new MailAddress(correoSupervisor));
            //}

            mail.To.Add(new MailAddress(correoResponsable));
            mail.IsBodyHtml = true;

            string bd = detalle;
            string sj = sujeto;
            //mail.Body = bd;
            mail.Subject = sj;

            //AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");
            LinkedResource inline = new LinkedResource(Logo);
            inline.ContentId = "header";
            avHtml.LinkedResources.Add(inline);

            LinkedResource inline1 = new LinkedResource(Flecha);
            inline1.ContentId = "footer";
            avHtml.LinkedResources.Add(inline1);

            LinkedResource inline2 = new LinkedResource(LogoDTI);
            inline2.ContentId = "bonton_servicio_asignado";
            avHtml.LinkedResources.Add(inline2);

            mail.AlternateViews.Add(avHtml);

            //LinkedResource Imagenes = new LinkedResource(Logo);
            //Imagenes.ContentId = "Logo";

            //htmlView.LinkedResources.Add(Imagenes);

            //mail.AlternateViews.Add(htmlView);

            try
            {
                Thread Hilo = new Thread(delegate ()
                {
                    smtp.Send(mail);
                });

                Hilo.Start();

            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
                        System.Threading.Thread.Sleep(5000);
                        smtp.Send(mail);
                    }
                    else
                    {
                        Console.WriteLine("No se pudo entregar el mensaje a {0}",
                            ex.InnerExceptions[i].FailedRecipient);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
                        ex.ToString());
            }

        }
        else
        {
            Console.WriteLine("Procedo Incorrecto");
        }
    }

   


    #region EnviarCorreo
    public void EnviarCorreo(string correoSupervisor, string correoResponsable, string sujeto, string detalle)
    {
        if ((correoSupervisor != null) && (correoResponsable != null) && (sujeto != null) && (detalle != null))
        {

            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress("intranet@pgr.gob.do");
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 25;
            smtp.EnableSsl =    false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("intranet", "Cven7777");
            smtp.Host = "pgrpx03.pgr.local";

            if (correoSupervisor!="")
            {
                mail.To.Add(new MailAddress(correoSupervisor));
            }
            
            mail.To.Add(new MailAddress(correoResponsable));
            mail.IsBodyHtml = true;
            string bd = detalle;
            string sj = sujeto;
            mail.Body = bd;
            mail.Subject = sj;
            try
            {
                smtp.Send(mail);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
                        System.Threading.Thread.Sleep(5000);
                        smtp.Send(mail);
                    }
                    else
                    {
                        Console.WriteLine("No se pudo entregar el mensaje a {0}",
                            ex.InnerExceptions[i].FailedRecipient);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
                        ex.ToString());
            }
     
        }
        else
        {
            Console.WriteLine("Procedo Incorrecto");
        }
    }
    #endregion



    #region EncuestasCorreo
    public void EncuestasCorreo(string Solicitante, string sujeto, string detalle, string Logo, string Flecha, string LogoDTI)
    {
        if ((Solicitante != null) && (sujeto != null) && (detalle != null))
        {
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress(Direccion);
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 25;
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(Usuario, Contrasena);
            smtp.Host = Host;



            mail.To.Add(new MailAddress(Solicitante.Trim()));
            mail.IsBodyHtml = true;

            string bd = detalle;
            string sj = sujeto;
            //mail.Body = bd;
            mail.Subject = sj;

            //AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");
            LinkedResource inline = new LinkedResource(Logo);
            inline.ContentId = "header";
            avHtml.LinkedResources.Add(inline);

            LinkedResource inline1 = new LinkedResource(Flecha);
            inline1.ContentId = "footer";
            avHtml.LinkedResources.Add(inline1);

            LinkedResource inline2 = new LinkedResource(LogoDTI);
            inline2.ContentId = "bonton_servicio_asignado";
            avHtml.LinkedResources.Add(inline2);

            mail.AlternateViews.Add(avHtml);

            //LinkedResource Imagenes = new LinkedResource(Logo);
            //Imagenes.ContentId = "Logo";

            //htmlView.LinkedResources.Add(Imagenes);

            //mail.AlternateViews.Add(htmlView);

            try
            {
                Thread Hilo = new Thread(delegate ()
                {
                    smtp.Send(mail);
                });

                Hilo.Start();

            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
                        System.Threading.Thread.Sleep(5000);
                        smtp.Send(mail);
                    }
                    else
                    {
                        Console.WriteLine("No se pudo entregar el mensaje a {0}",
                            ex.InnerExceptions[i].FailedRecipient);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
                        ex.ToString());
            }

        }
        else
        {
            Console.WriteLine("Procedo Incorrecto");
        }
    }
    #endregion





    #region EncuestasCorreo
    public void EnviarCorreoConcursoMadres( string Solicitante, string Concursante, string sujeto, string detalle, string Logo, string Flecha, string LogoDTI)
    {
        if ( (Solicitante != null) && (sujeto != null) && (detalle != null)&&(Concursante != null))
        {
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress(Direccion);
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 25;
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(Usuario, Contrasena);
            smtp.Host = Host;

          

            mail.To.Add(new MailAddress(Solicitante.Trim()));
            mail.IsBodyHtml = true;

            string bd = detalle;
            string sj = sujeto;
            //mail.Body = bd;
            mail.Subject = sj;

            //AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");
            LinkedResource inline = new LinkedResource(Logo);
            inline.ContentId = "header";
            avHtml.LinkedResources.Add(inline);

            LinkedResource inline1 = new LinkedResource(Flecha);
            inline1.ContentId = "footer";
            avHtml.LinkedResources.Add(inline1);

            LinkedResource inline2 = new LinkedResource(LogoDTI);
            inline2.ContentId = "bonton_servicio_asignado";
            avHtml.LinkedResources.Add(inline2);

            mail.AlternateViews.Add(avHtml);

            //LinkedResource Imagenes = new LinkedResource(Logo);
            //Imagenes.ContentId = "Logo";

            //htmlView.LinkedResources.Add(Imagenes);

            //mail.AlternateViews.Add(htmlView);

            try
            {
                Thread Hilo = new Thread(delegate ()
                {
                    smtp.Send(mail);
                });

                Hilo.Start();

            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
                        System.Threading.Thread.Sleep(5000);
                        smtp.Send(mail);
                    }
                    else
                    {
                        Console.WriteLine("No se pudo entregar el mensaje a {0}",
                            ex.InnerExceptions[i].FailedRecipient);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
                        ex.ToString());
            }

        }
        else
        {
            Console.WriteLine("Procedo Incorrecto");
        }
    }
    #endregion

    #region EncuestasCorreo
    public void CorreoGestionHumana(string Solicitante, string sujeto, string detalle, string Logo, string Flecha, string LogoDTI)
    {
        if ((Solicitante != null) && (sujeto != null) && (detalle != null))
        {
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress(Direccion);
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 25;
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(Usuario, Contrasena);
            smtp.Host = Host;



            mail.To.Add(new MailAddress(Solicitante));
            mail.IsBodyHtml = true;

            string bd = detalle;
            string sj = sujeto;
            //mail.Body = bd;
            mail.Subject = sj;

            //AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");
            LinkedResource inline = new LinkedResource(Logo);
            inline.ContentId = "header";
            avHtml.LinkedResources.Add(inline);

            LinkedResource inline1 = new LinkedResource(Flecha);
            inline1.ContentId = "footer";
            avHtml.LinkedResources.Add(inline1);

            LinkedResource inline2 = new LinkedResource(LogoDTI);
            inline2.ContentId = "bonton_servicio_asignado";
            avHtml.LinkedResources.Add(inline2);

            mail.AlternateViews.Add(avHtml);

            //LinkedResource Imagenes = new LinkedResource(Logo);
            //Imagenes.ContentId = "Logo";

            //htmlView.LinkedResources.Add(Imagenes);

            //mail.AlternateViews.Add(htmlView);

            try
            {
                Thread Hilo = new Thread(delegate ()
                {
                    smtp.Send(mail);
                });

                Hilo.Start();

            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
                        System.Threading.Thread.Sleep(5000);
                        smtp.Send(mail);
                    }
                    else
                    {
                        Console.WriteLine("No se pudo entregar el mensaje a {0}",
                            ex.InnerExceptions[i].FailedRecipient);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
                        ex.ToString());
            }

        }
        else
        {
            Console.WriteLine("Procedo Incorrecto");
        }
    }
    #endregion

    public void EnviarCorreoCartaProcedencia_(string correo, string sujeto, string detalle, string logo, string logoDTI, byte[] documento, string documentoNombre)
    {
        if ((correo != null) && (sujeto != null) && (detalle != null) && (logo != null) && (logoDTI != null))
        {
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress(Direccion);
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 25;
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(Usuario, Contrasena);
            smtp.Host = Host;

            if (correo!="") {
                mail.To.Add(new MailAddress(correo));
            }

            System.Net.Mail.Attachment archivo = new Attachment(new MemoryStream(documento), documentoNombre);

            mail.Attachments.Add(archivo);

            mail.To.Add(new MailAddress("correderecursoshumanoaqui@pgr.gob.do"));

            mail.IsBodyHtml = true;

            string bd = detalle;
            string sj = sujeto;
            //mail.Body = bd;
            mail.Subject = sj;

            //AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");
            LinkedResource inline = new LinkedResource(logo);
            inline.ContentId = "header";
            avHtml.LinkedResources.Add(inline);

            //LinkedResource inline1 = new LinkedResource(Flecha);
            //inline1.ContentId = "footer";
            //avHtml.LinkedResources.Add(inline1);

            LinkedResource inline2 = new LinkedResource(logoDTI);
            inline2.ContentId = "bonton_servicio_asignado";
            avHtml.LinkedResources.Add(inline2);

            mail.AlternateViews.Add(avHtml);

            //LinkedResource Imagenes = new LinkedResource(Logo);
            //Imagenes.ContentId = "Logo";

            //htmlView.LinkedResources.Add(Imagenes);

            //mail.AlternateViews.Add(htmlView);

            try
            {
                Thread Hilo = new Thread(delegate ()
                {
                    smtp.Send(mail);
                });

                Hilo.Start();

            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
                        System.Threading.Thread.Sleep(5000);
                        smtp.Send(mail);
                    }
                    else
                    {
                        Console.WriteLine("No se pudo entregar el mensaje a {0}",
                            ex.InnerExceptions[i].FailedRecipient);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
                        ex.ToString());
            }

        }

    }

    public void EnviarCorreoServicioRepatriado_(string[] Correos, string sujeto, string detalle, string Logo, string Flecha, string LogoDTI)
    {
        if ((sujeto != null) && (detalle != null))
        {
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress(Direccion);
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 25;
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(Usuario, Contrasena);
            smtp.Host = Host;

            for (int i = 0; i < Correos.Length; i++)
            {
                if (Correos[i] != "") mail.To.Add(new MailAddress(Correos[i]));
            }

            mail.IsBodyHtml = true;

            string bd = detalle;
            string sj = sujeto;
            mail.Subject = sj;

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");
            LinkedResource inline = new LinkedResource(Logo);
            inline.ContentId = "header";
            avHtml.LinkedResources.Add(inline);

            LinkedResource inline1 = new LinkedResource(Flecha);
            inline1.ContentId = "footer";
            avHtml.LinkedResources.Add(inline1);

            LinkedResource inline2 = new LinkedResource(LogoDTI);
            inline2.ContentId = "bonton_servicio_asignado";
            avHtml.LinkedResources.Add(inline2);

            mail.AlternateViews.Add(avHtml);

            try
            {
                Thread Hilo = new Thread(delegate ()
                {
                    smtp.Send(mail);
                });

                Hilo.Start();

            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
                        System.Threading.Thread.Sleep(5000);
                        smtp.Send(mail);
                    }
                    else
                    {
                        Console.WriteLine("No se pudo entregar el mensaje a {0}",
                            ex.InnerExceptions[i].FailedRecipient);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
                        ex.ToString());
            }

        }
        else
        {
            Console.WriteLine("Procedo Incorrecto");
        }
    }

    //public void EnviarCorreoProduccion(string correo1, string sujeto, string detalle)
    //{

    //        MailMessage mail = new MailMessage();
    //        mail.From = new System.Net.Mail.MailAddress("intranet@pgr.gob.do");
    //        SmtpClient smtp = new SmtpClient();
    //        smtp.Port = 25;
    //        smtp.EnableSsl = false;
    //        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
    //        smtp.UseDefaultCredentials = false;
    //        smtp.Credentials = new NetworkCredential("intranet", "Cven7777");
    //        smtp.Host = "pgrpx03.pgr.local";
    //        correo1 = "maria.rodriguez@pgr.gob.do";

    //        mail.To.Add(new MailAddress(correo1));
    //        mail.IsBodyHtml = true;
    //        string bd = detalle;
    //        string sj = sujeto;
    //        mail.Body = bd;
    //        mail.Subject = sj;
    //        try
    //        {
    //            smtp.Send(mail);
    //        }
    //        catch (SmtpFailedRecipientsException ex)
    //        {
    //            for (int i = 0; i < ex.InnerExceptions.Length; i++)
    //            {
    //                SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
    //                if (status == SmtpStatusCode.MailboxBusy ||
    //                    status == SmtpStatusCode.MailboxUnavailable)
    //                {
    //                    Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
    //                    System.Threading.Thread.Sleep(5000);
    //                    smtp.Send(mail);
    //                }
    //                else
    //                {
    //                    Console.WriteLine("No se pudo entregar el mensaje a {0}",
    //                        ex.InnerExceptions[i].FailedRecipient);
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
    //                    ex.ToString());
    //        }

    //    }





    #region EnviarCorreo_
    public void EnviarCorreo_(string[] Correos, string sujeto, string detalle, string Logo,string Flecha,string LogoDTI)
    {

        if ((sujeto != null) && (detalle != null))
        {
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress(Direccion);
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 25;
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(Usuario, Contrasena);
            smtp.Host = Host;

            for (int i = 0; i < Correos.Length; i++)
            {
                if (Correos[i] != "") mail.To.Add(new MailAddress(Correos[i]));
            }

            mail.IsBodyHtml = true;
            
            string bd = detalle;
            string sj = sujeto;
            //mail.Body = bd;
            mail.Subject = sj;
            
            //AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");
            
            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");
            LinkedResource inline = new LinkedResource(Logo);
            inline.ContentId = "header";
            avHtml.LinkedResources.Add(inline);

            LinkedResource inline1 = new LinkedResource(Flecha);
            inline1.ContentId = "footer";
            avHtml.LinkedResources.Add(inline1);

            LinkedResource inline2 = new LinkedResource(LogoDTI);
            inline2.ContentId = "bonton_servicio_asignado";
            avHtml.LinkedResources.Add(inline2);

            mail.AlternateViews.Add(avHtml);
            
            //LinkedResource Imagenes = new LinkedResource(Logo);
            //Imagenes.ContentId = "Logo";
            
            //htmlView.LinkedResources.Add(Imagenes);
            
            //mail.AlternateViews.Add(htmlView);

            try
            {
                Thread Hilo = new Thread(delegate ()
                {
                    smtp.Send(mail);
                });

                Hilo.Start();
                
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
                        System.Threading.Thread.Sleep(5000);
                        smtp.Send(mail);
                    }
                    else
                    {
                        Console.WriteLine("No se pudo entregar el mensaje a {0}",
                            ex.InnerExceptions[i].FailedRecipient);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
                        ex.ToString());
            }

        }
        else
        {
            Console.WriteLine("Procedo Incorrecto");
        }
    }

    public void EnviarCorreo_(string correoSolicitante, string correoSupervisor, string correoResponsable, string sujeto, string detalle, string Logo, string Flecha, string LogoDTI)
    {
        var correoSupervisorValido = !string.IsNullOrEmpty(correoSupervisor);
        var correoResponsableValido = !string.IsNullOrEmpty(correoResponsable);
        var correoSolicitanteValido = !string.IsNullOrEmpty(correoSolicitante);

        if ( (correoSupervisorValido || correoResponsableValido || correoSolicitanteValido) && (sujeto != null) && (detalle != null))
        {
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress(Direccion);
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 25;
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(Usuario, Contrasena);
            smtp.Host = Host;

            if (correoSupervisorValido) mail.To.Add(new MailAddress(correoSupervisor.Trim()));

            if (correoResponsableValido) mail.To.Add(new MailAddress(correoResponsable.Trim()));

            if (correoSolicitanteValido) mail.To.Add(new MailAddress(correoSolicitante.Trim()));

            mail.IsBodyHtml = true;

            string bd = detalle;
            string sj = sujeto;
            //mail.Body = bd;
            mail.Subject = sj;

            //AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");
            LinkedResource inline = new LinkedResource(Logo);
            inline.ContentId = "header";
            avHtml.LinkedResources.Add(inline);

            LinkedResource inline1 = new LinkedResource(Flecha);
            inline1.ContentId = "footer";
            avHtml.LinkedResources.Add(inline1);

            LinkedResource inline2 = new LinkedResource(LogoDTI);
            inline2.ContentId = "bonton_servicio_asignado";
            avHtml.LinkedResources.Add(inline2);

            mail.AlternateViews.Add(avHtml);

            //LinkedResource Imagenes = new LinkedResource(Logo);
            //Imagenes.ContentId = "Logo";

            //htmlView.LinkedResources.Add(Imagenes);

            //mail.AlternateViews.Add(htmlView);

            try
            {
                Thread Hilo = new Thread(delegate ()
                {
                    smtp.Send(mail);
                });

                Hilo.Start();

            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
                        System.Threading.Thread.Sleep(5000);
                        smtp.Send(mail);
                    }
                    else
                    {
                        Console.WriteLine("No se pudo entregar el mensaje a {0}",
                            ex.InnerExceptions[i].FailedRecipient);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
                        ex.ToString());
            }

        }
        else
        {
            Console.WriteLine("Procedo Incorrecto");
        }
    }
    #endregion


    public void CorreoPaseProduccion(string Lider, string EncargadoDTI, string EncargadoDesarrollo, string EncargadoProyecto, string EncargadoSubir,  string sujeto, string detalle, string Logo, string Flecha, string LogoDTI)
    {
        if ((EncargadoDTI != null) && (EncargadoDesarrollo != null) && (EncargadoSubir != null) && (EncargadoProyecto != null) && (sujeto != null) && (detalle != null))
        {
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress(Direccion);
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 25;
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(Usuario, Contrasena);
            smtp.Host = Host;

            if (EncargadoDTI != "")
            {
                mail.To.Add(new MailAddress(EncargadoDTI));
            }

            if (EncargadoDesarrollo != "")
            {
                mail.To.Add(new MailAddress(EncargadoDesarrollo));
            }

            if (EncargadoProyecto != "")
            {
                mail.To.Add(new MailAddress(EncargadoProyecto));
            }
            if (EncargadoSubir != "")
            {
                mail.To.Add(new MailAddress(EncargadoSubir));
            }

            mail.IsBodyHtml = true;

            string bd = detalle;
            string sj = sujeto;
            //mail.Body = bd;
            mail.Subject = sj;

            //AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(bd, null, "text/html");
            LinkedResource inline = new LinkedResource(Logo);
            inline.ContentId = "header";
            avHtml.LinkedResources.Add(inline);

            LinkedResource inline1 = new LinkedResource(Flecha);
            inline1.ContentId = "footer";
            avHtml.LinkedResources.Add(inline1);

            LinkedResource inline2 = new LinkedResource(LogoDTI);
            inline2.ContentId = "bonton_servicio_asignado";
            avHtml.LinkedResources.Add(inline2);

            mail.AlternateViews.Add(avHtml);

            //LinkedResource Imagenes = new LinkedResource(Logo);
            //Imagenes.ContentId = "Logo";

            //htmlView.LinkedResources.Add(Imagenes);

            //mail.AlternateViews.Add(htmlView);

            try
            {
                Thread Hilo = new Thread(delegate ()
                {
                    smtp.Send(mail);
                });

                Hilo.Start();

            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
                        System.Threading.Thread.Sleep(5000);
                        smtp.Send(mail);
                    }
                    else
                    {
                        Console.WriteLine("No se pudo entregar el mensaje a {0}",
                            ex.InnerExceptions[i].FailedRecipient);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
                        ex.ToString());
            }

        }
        else
        {
            Console.WriteLine("Procedo Incorrecto");
        }
    }


}