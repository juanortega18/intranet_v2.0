﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Models.Repositorio;

namespace intra.Controllers
{
    public class SubSeccionRepositorioController : Controller
    {
        private RepositorioDB db = new RepositorioDB();

        // GET: SubSeccionRepositorio
        public ActionResult Index(int seccionRepositorioId)
        {
            var subSeccionRepositorio = db.SubSeccionRepositorio.Where(s => s.SeccionRepositorioId == seccionRepositorioId).Distinct();
            return View(subSeccionRepositorio.ToList());
        }

        // GET: SubSeccionRepositorio/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubSeccionRepositorio subSeccionRepositorio = db.SubSeccionRepositorio.Find(id);
            if (subSeccionRepositorio == null)
            {
                return HttpNotFound();
            }
            return View(subSeccionRepositorio);
        }

        // GET: SubSeccionRepositorio/Create
        public ActionResult Create()
        {
            ViewBag.SeccionRepositorioId = new SelectList(db.SeccionRepositorio, "SeccionRepositorioId", "SeccionRepositorioDescripcion");
            return View();
        }

        // POST: SubSeccionRepositorio/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SubSeccionRepositorioId,SeccionRepositorioId,SubSeccionRepositorioDescripcion")] SubSeccionRepositorio subSeccionRepositorio)
        {
            if (ModelState.IsValid)
            {
                db.SubSeccionRepositorio.Add(subSeccionRepositorio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SeccionRepositorioId = new SelectList(db.SeccionRepositorio, "SeccionRepositorioId", "SeccionRepositorioDescripcion", subSeccionRepositorio.SeccionRepositorioId);
            return View(subSeccionRepositorio);
        }

        // GET: SubSeccionRepositorio/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubSeccionRepositorio subSeccionRepositorio = db.SubSeccionRepositorio.Find(id);
            if (subSeccionRepositorio == null)
            {
                return HttpNotFound();
            }
            ViewBag.SeccionRepositorioId = new SelectList(db.SeccionRepositorio, "SeccionRepositorioId", "SeccionRepositorioDescripcion", subSeccionRepositorio.SeccionRepositorioId);
            return View(subSeccionRepositorio);
        }

        // POST: SubSeccionRepositorio/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SubSeccionRepositorioId,SeccionRepositorioId,SubSeccionRepositorioDescripcion")] SubSeccionRepositorio subSeccionRepositorio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subSeccionRepositorio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SeccionRepositorioId = new SelectList(db.SeccionRepositorio, "SeccionRepositorioId", "SeccionRepositorioDescripcion", subSeccionRepositorio.SeccionRepositorioId);
            return View(subSeccionRepositorio);
        }

        // GET: SubSeccionRepositorio/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubSeccionRepositorio subSeccionRepositorio = db.SubSeccionRepositorio.Find(id);
            if (subSeccionRepositorio == null)
            {
                return HttpNotFound();
            }
            return View(subSeccionRepositorio);
        }

        // POST: SubSeccionRepositorio/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubSeccionRepositorio subSeccionRepositorio = db.SubSeccionRepositorio.Find(id);
            db.SubSeccionRepositorio.Remove(subSeccionRepositorio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
