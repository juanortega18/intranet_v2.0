﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.FlotasExtensiones;

namespace intra.Controllers
{
    public class ModeloFlotasController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: ModeloFlotas
        public ActionResult vModeloFlota()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
           var Modelos =  db.ModeloFlotas.ToList();
          
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo de Modelo Flotas");
            return View(Modelos);
        }

        public PartialViewResult vMarcas()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo de Modelo Flotas");

            var marcas = db.MarcasFlotas.ToList();

            return PartialView(marcas);
        }

        

        [CustomAuthorize]
        // GET: ModeloFlotas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModeloFlotas modeloFlotas = db.ModeloFlotas.Find(id);
            if (modeloFlotas == null)
            {
                return HttpNotFound();
            }
            return View(modeloFlotas);
        }


        public ActionResult CrearMarca()
        {
            ViewBag.MarcaId = new SelectList(db.MarcasFlotas.ToList(), "MarcaId", "MarcaDescripcion");
            return View();
        }

        [HttpPost]
        public ActionResult CrearMarca( FormCollection fc, MarcasFlotas marca)
        {
            ViewBag.CrearMarca = "";
            try
            {
                if (ModelState.IsValid)
                {


                    marca.MarcaDescripcion = fc["MarcaDescripcion"].ToString();
                    marca.MarcaFhCreacion = DateTime.Now;
                    marca.MarcaUsuario = HttpContext.Session["usuario"].ToString();
                    db.MarcasFlotas.Add(marca);
                    db.SaveChanges();
                    ViewBag.MarcaId = new SelectList(db.MarcasFlotas.ToList(), "MarcaId", "MarcaDescripcion");

                    TempData["CrearMarca"] = "OK";

                    return RedirectToAction("MarcasModelosFlotas", "Modeloflotas");

                }
                else
                {
                    TempData["Error"] = "No se puedo crear esta solicitud";

                }
            }


            catch
            {
                TempData["Error"] = "Ocurrio un error al registrar esta solicitud";

            }
            return View("MarcasModelosFlotas");
        }
     


        [CustomAuthorize]
        // GET: ModeloFlotas/Create
        public ActionResult CrearModelo()
        {
            ViewBag.MarcaId = new SelectList(db.MarcasFlotas.ToList(), "MarcaId", "MarcaDescripcion");
            return View();

        }

        // POST: ModeloFlotas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearModelo([Bind(Include = "ModeloId,ModeloDescripcion,ModeloFhCreacion,ModeloUsuario,MarcaId")] ModeloFlotas modeloFlotas, FormCollection fc)
        {

            try { 
            if (ModelState.IsValid)
            {
                modeloFlotas.MarcaId = int.Parse(fc["MarcaId"].ToString());
                modeloFlotas.ModeloDescripcion = fc["ModeloDescripcion"];
                modeloFlotas.ModeloFhCreacion = DateTime.Now;
                modeloFlotas.ModeloUsuario = Session["usuario"].ToString();
                db.ModeloFlotas.Add(modeloFlotas);
                db.SaveChanges();

                TempData["CrearModelo"] = "OK";
                    string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo un registro en el Modulo de Modelo Flotas id (" + modeloFlotas.ModeloId + ")");
                    return RedirectToAction("MarcasModelosFlotas", "Modeloflotas");
                }
            else
                {
                    TempData["Error"] = "No se puedo crear esta solicitud";

                }
            }
            catch
            {
                TempData["Error"] = "Ocurrio un error al registrar esta solicitud";
            }
            ViewBag.MarcaId = new SelectList(db.MarcasFlotas.ToList(), "MarcaId", "MarcaDescripcion");
            return RedirectToAction("MarcasModelosFlotas", "Modeloflotas");
        }

        [CustomAuthorize]
        // GET: ModeloFlotas/Edit/5
        public ActionResult EditarModelo(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModeloFlotas modeloFlotas = db.ModeloFlotas.Find(id);
            if (modeloFlotas == null)
            {
                return HttpNotFound();
            }
            ViewBag.MarcaId = new SelectList(db.MarcasFlotas.ToList(), "MarcaId", "MarcaDescripcion");
            return View(modeloFlotas);
        }

        // POST: ModeloFlotas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarModelo([Bind(Include = "ModeloId,ModeloDescripcion,ModeloFhCreacion,ModeloUsuario,MarcaId")] ModeloFlotas modeloFlotas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(modeloFlotas).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo un registro en el Modulo de Modelo Flotas id (" + modeloFlotas.ModeloId + ")");
               
            }
            ViewBag.MarcaId = new SelectList(db.MarcasFlotas.ToList(), "MarcaId", "MarcaDescripcion");
            return RedirectToAction("MarcasModelosFlotas", "Modeloflotas");
        }

        [CustomAuthorize]
        // GET: ModeloFlotas/Edit/5
        public ActionResult EditarMarca(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarcasFlotas MarcasFlotas = db.MarcasFlotas.Find(id);
            if (MarcasFlotas == null)
            {
                return HttpNotFound();
            }
            return View(MarcasFlotas);
        }

        // POST: ModeloFlotas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarMarca(FormCollection fc, MarcasFlotas MarcasFlotas)
        {
            if (ModelState.IsValid)
            {
                MarcasFlotas.MarcaId = MarcasFlotas.MarcaId;
                MarcasFlotas.MarcaFhCreacion = DateTime.Now;
                MarcasFlotas.MarcaUsuario = Session["usuario"].ToString();
                MarcasFlotas.MarcaDescripcion = fc["MarcaDescripcion"];
                db.Entry(MarcasFlotas).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo un registro en el Modulo de Marca Flotas id (" + MarcasFlotas.MarcaId + ")");
              
            }
            return RedirectToAction("MarcasModelosFlotas", "Modeloflotas");
        }


        [CustomAuthorize]
        // GET: ModeloFlotas/Delete/5
        public ActionResult MarcasModelosFlotas()
        {
            ViewBag.MarcaId = new SelectList(db.MarcasFlotas.ToList(), "MarcaId", "MarcaDescripcion");

            return View(db.ModeloFlotas.ToList());
        }

        // POST: ModeloFlotas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ModeloFlotas modeloFlotas = db.ModeloFlotas.Find(id);
            db.ModeloFlotas.Remove(modeloFlotas);
            db.SaveChanges();
            string empId = HttpContext.Session["empleadoId"].ToString();

            RegisterLogs rl = new RegisterLogs(empId, "Creo un registro en el Modulo de Modelo Flotas id (" + modeloFlotas.ModeloId + ")");
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
