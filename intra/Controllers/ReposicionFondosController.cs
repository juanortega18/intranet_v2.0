﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models;
using Rotativa;
using Rotativa.Options;

namespace intra.Controllers
{
    public class ReposicionFondosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        private string session = "";

        private dbIntranet fiscalia = new dbIntranet();

        [CustomAuthorize]
        // GET: ReposicionFondos
        public ActionResult Index()
        {
            return View(db.vw_ReposicionFondos.ToList());
        }

        [CustomAuthorize]
        // GET: ReposicionFondos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReposicionFondos reposicionFondos = db.ReposicionFondos.Find(id);
            if (reposicionFondos == null)
            {
                return HttpNotFound();
            }
            return View(reposicionFondos);
        }

        [CustomAuthorize]
        // GET: ReposicionFondos/Create
        public ActionResult Create()
        {
            ViewBag.TipoFondo  = new SelectList(db.TiposFondos.ToList(), "tipoFondoId", "tipoFondoNombre");
            ViewBag.Fiscalia = new SelectList(fiscalia.Recintos.ToList(), "recintoId", "recintoNombre");

            return View();
        }

        // POST: ReposicionFondos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "fondoId,fiscaliaId,fondoTitular,tipoFondoId,fondoMontoReposicion,fondoResponsable,fondoAuditor,fondoObjetivo,fondoAlcance,fondoHallazgos,fondoLeyes,fondoMejoras,contralorId,fondoAprobado,fondoFechaAprobacion,fondoFechaCreacion,usuario")] ReposicionFondos reposicionFondos)
        {
            reposicionFondos.fondoFechaCreacion = DateTime.Now;
            reposicionFondos.usuario = Session["usuario"].ToString();
            if (ModelState.IsValid)
            {
                db.ReposicionFondos.Add(reposicionFondos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TipoFondo = new SelectList(db.TiposFondos.ToList(), "tipoFondoId", "tipoFondoNombre");
            ViewBag.Fiscalia = new SelectList(fiscalia.Recintos.ToList(), "recintoId", "recintoNombre");

            return View(reposicionFondos);
        }

        [CustomAuthorize]
        // GET: ReposicionFondos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReposicionFondos reposicionFondos = db.ReposicionFondos.Find(id);
            if (reposicionFondos == null)
            {
                return HttpNotFound();
            }

            ViewBag.TipoFondo = new SelectList(db.TiposFondos.ToList(), "tipoFondoId", "tipoFondoNombre");
            ViewBag.Fiscalia = new SelectList(fiscalia.Recintos.ToList(), "recintoId", "recintoNombre");
            return View(reposicionFondos);
        }

        // POST: ReposicionFondos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ReposicionFondos reposicionFondos)
        {
            reposicionFondos.fondoFechaAprobacion = DateTime.Now;
            string empId = HttpContext.Session["empleadoId"].ToString();
            session = empId;
            reposicionFondos.contralorId = int.Parse(empId);

            
            if (ModelState.IsValid)
            {
                db.Entry(reposicionFondos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TipoFondo = new SelectList(db.TiposFondos.ToList(), "tipoFondoId", "tipoFondoNombre");
            ViewBag.Fiscalia = new SelectList(fiscalia.Recintos.ToList(), "recintoId", "recintoNombre");
            return View(/*reposicionFondos*/);
        }

        [CustomAuthorize]
        // GET: ReposicionFondos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReposicionFondos reposicionFondos = db.ReposicionFondos.Find(id);

            ViewBag.Fiscalia = fiscalia.Recintos.Where(x => x.recintoId == reposicionFondos.fiscaliaId).FirstOrDefault().recintoNombre;
            ViewBag.Fondo = db.TiposFondos.Where(x=>x.tipoFondoId==reposicionFondos.tipoFondoId).FirstOrDefault().tipoFondoNombre;
            ViewBag.Aprobacion = reposicionFondos.fondoAprobado == 1 ? "Sí" : "No";
            ViewBag.Contralor = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == reposicionFondos.contralorId.ToString()).FirstOrDefault().NombreCompleto;

            if (reposicionFondos == null)
            {
                return HttpNotFound();
            }
            return View(reposicionFondos);
        }

        // POST: ReposicionFondos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ReposicionFondos reposicionFondos = db.ReposicionFondos.Find(id);
            db.ReposicionFondos.Remove(reposicionFondos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        
        public ActionResult ReporteContralor(int? id)
        {
            var reposicionFondos = db.ReposicionFondos.Find(id);

            reposicionFondos.fondoHallazgos = reposicionFondos.fondoHallazgos.Replace("\r", "");
            ViewBag.Hallazgo = reposicionFondos.hallazgo_array = string.IsNullOrEmpty(reposicionFondos.fondoHallazgos) ? new string[0] : reposicionFondos.fondoHallazgos.Split('\n');
            
            reposicionFondos.fondoMejoras = string.IsNullOrEmpty(reposicionFondos.fondoMejoras) ? "" : reposicionFondos.fondoMejoras.Replace("\r", "");

            ViewBag.Recomendacion = reposicionFondos.recomendacion_array = string.IsNullOrEmpty(reposicionFondos.fondoMejoras) ? new string[0] : reposicionFondos.fondoMejoras.Split('\n');

            ViewBag.VistaFondos = db.vw_ReposicionFondos.FirstOrDefault(x => x.fondoId == id);
            ViewBag.Fechaimpresion = DateTime.Now;

            string empId = db.VISTA_EMPLEADOS.Where(x=>x.empleadoid==reposicionFondos.contralorId.ToString()).FirstOrDefault().nombre;
            
            ViewBag.NombreContralor = empId;

            return View();
        }
        
        public ActionResult ReporteRot(int id)
        {

            return new ActionAsPdf("ReporteContralor", new { id = id });
        }

    }
}
