﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;

namespace intra.Controllers
{
    public class IncidenciasAreasController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: IncidenciasAreas
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo de IncidenciasAreas");
            return View(db.IncidenciasAreas.ToList());
        }

        [CustomAuthorize]
        // GET: IncidenciasAreas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncidenciasAreas incidenciasAreas = db.IncidenciasAreas.Find(id);
            if (incidenciasAreas == null)
            {
                return HttpNotFound();
            }
            return View(incidenciasAreas);
        }

        [CustomAuthorize]
        // GET: IncidenciasAreas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IncidenciasAreas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IncidenciasAreaId,IncidenciasAreaNombre")] IncidenciasAreas incidenciasAreas)
        {
            if (ModelState.IsValid)
            {
                db.IncidenciasAreas.Add(incidenciasAreas);
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo un registro en el Modulo de IncidenciasAreas llamado("+incidenciasAreas.IncidenciasAreaNombre+")");
                return RedirectToAction("Index");
            }

            return View(incidenciasAreas);
        }

        [CustomAuthorize]
        // GET: IncidenciasAreas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncidenciasAreas incidenciasAreas = db.IncidenciasAreas.Find(id);
            if (incidenciasAreas == null)
            {
                return HttpNotFound();
            }
            return View(incidenciasAreas);
        }

        // POST: IncidenciasAreas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IncidenciasAreaId,IncidenciasAreaNombre")] IncidenciasAreas incidenciasAreas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(incidenciasAreas).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito un registro en el Modulo de IncidenciasAreas id(" + incidenciasAreas.IncidenciasAreaId + ")");
                return RedirectToAction("Index");
            }
            return View(incidenciasAreas);
        }

        [CustomAuthorize]
        // GET: IncidenciasAreas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncidenciasAreas incidenciasAreas = db.IncidenciasAreas.Find(id);
            if (incidenciasAreas == null)
            {
                return HttpNotFound();
            }
            return View(incidenciasAreas);
        }

        // POST: IncidenciasAreas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IncidenciasAreas incidenciasAreas = db.IncidenciasAreas.Find(id);
            db.IncidenciasAreas.Remove(incidenciasAreas);
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Elimino un registro en el Modulo de IncidenciasAreas llamado(" + incidenciasAreas.IncidenciasAreaNombre + ")");
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
