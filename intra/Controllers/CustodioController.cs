﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;
using System.Net;
using Newtonsoft.Json;
using System.Data.Entity;
using intra.Models.BienesIncautados;

namespace intra.Controllers
{
    [Authorize(Roles = "BI,BIA,BIWR")]
    public class CustodioController : Controller
    {
        private inv db = new inv();

        // GET: Custodio
        //[CustomAuthorize]
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();

            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo BI");

            try
            {
                return View(db.INV_CUSTODIO.Where(x=>x.CUST_ID != 1 && x.CUST_ESTADO==true).OrderBy(x=>x.CUST_NOMBRE).ToList());
            }
            catch (ArgumentNullException error)
            {
                ViewBag.error = error;
                return View();
            }
        }

        // GET: incautaciones/Details/5
        [CustomAuthorize]
        //[Authorize(Roles = "BI,BIA,BIWR")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INV_CUSTODIO iNV_CUSTODIO = db.INV_CUSTODIO.Find(id);
            
            if (iNV_CUSTODIO == null)
            {
                return HttpNotFound();
            }

            if(iNV_CUSTODIO.CUST_ID == 1)
            {
                return RedirectToAction("Index", "Custodio");
            }

            return View(iNV_CUSTODIO);
        }

        // GET: custodio/Create
        [Authorize(Roles = "BIA,BIWR")]
        [CustomAuthorize]
        public ActionResult Create()
        {
            //ViewBag.MONEDA_TIPO_ID = new SelectList(db.INV_MONEDA_TIPO, "MONEDA_TIPO_ID", "MONEDA_TIPO_DESCRIPCION_SIMBOLO");

            return View();
        }

        // POST: custodio/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult Create(INV_CUSTODIO view_object)
        {
            //view_object.CUST_VALOR = ConvertirMoneda(Request.Form["VALOR"]);

            if (view_object.CUST_NOMBRE==null)
            {
                view_object.CUST_NOMBRE = "Ninguno";
            }

            if(view_object.CUST_CEDULA==null)
            {
                view_object.CUST_CEDULA = "0";
            }

            if (view_object.CUST_TIPO == "Persona")
            {
                var existe = db.INV_CUSTODIO.Where(x => x.CUST_CEDULA == view_object.CUST_CEDULA).FirstOrDefault();

                if (existe != null)
                {
                    TempData["msjError"] = "Este custodio ha sido registrado";
                    return RedirectToAction("Index","Custodio");
                }
            }
            else if (view_object.CUST_TIPO == "Departamento")
            {
                var existe = db.INV_CUSTODIO.Where(x => x.CUST_DEPARTAMENTO == view_object.CUST_DEPARTAMENTO && x.CUST_DIRECTORDPT == view_object.CUST_DIRECTORDPT).FirstOrDefault();

                if (existe != null)
                {
                    TempData["msjError"] = "Este custodio ha sido registrado";
                    return RedirectToAction("Index", "Custodio");
                }
            }

            view_object.CUST_ESTADO = true;

            var SeGuardo = Guardar(view_object);

            if (SeGuardo)
            {
                return RedirectToAction("Index");
            }else
            {
                //ViewBag.MONEDA_TIPO = new SelectList(db.INV_MONEDA_TIPO, "MONEDA_TIPO_ID", "MONEDA_TIPPO_DESCRIPCION");
                return View(view_object);
            }
        }

        [HttpPost, ValidateAntiForgeryToken, Authorize(Roles = "BIA,BIWR")]
        public JsonResult CreateModal(INV_CUSTODIO view_object)
        {
            if (view_object.CUST_NOMBRE == null)
            {
                view_object.CUST_NOMBRE = "Ninguno";
            }

            if (view_object.CUST_CEDULA == null)
            {
                view_object.CUST_CEDULA = "";
            }

            if(view_object.CUST_TIPO=="Persona")
            {
                var existe = db.INV_CUSTODIO.Where(x => x.CUST_CEDULA == view_object.CUST_CEDULA).FirstOrDefault();

                if(existe != null)
                {
                    return Json("Este custodio ha sido registrado");
                }
            }
            else if(view_object.CUST_TIPO == "Departamento")
            {
                var existe = db.INV_CUSTODIO.Where(x => x.CUST_DEPARTAMENTO == view_object.CUST_DEPARTAMENTO && x.CUST_DIRECTORDPT == view_object.CUST_DIRECTORDPT).FirstOrDefault();

                if (existe != null)
                {
                    return Json("Este custodio ha sido registrado");
                }
            }

            view_object.CUST_ESTADO = true;

            var SeGuardo = Guardar(view_object);

            if (SeGuardo)
                return Json("Se ha creado el usuario correctamente");
            else
                return Json("No se ha podido crear el usuario");
        }

        public bool Guardar(INV_CUSTODIO view_object)
        {
            view_object.USUA_ID = Session["usuario"].ToString();

            if (ModelState.IsValid)
            {
                if(view_object.CUST_TIPO.ToUpper()!="PERSONA")
                {
                    view_object.CUST_CEDULA = "";

                    if (view_object.CUST_TIPO.ToUpper() == "DEPARTAMENTO")
                    {
                        view_object.CUST_NOMBRE = view_object.CUST_DEPARTAMENTO;
                    }
                }

                db.INV_CUSTODIO.Add(view_object);
                db.SaveChanges();

                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo un custodio en el modulo BI con el nombre (" + view_object.CUST_NOMBRE + ")");
                return true;
            }

            var errors = ModelState.Values.SelectMany(v => v.Errors);
            return false;
        }

        // GET: custodio/Edit/5
        [Authorize(Roles = "BIA,BIWR")]
        [CustomAuthorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            INV_CUSTODIO iNV_CUSTODIO = db.INV_CUSTODIO.FirstOrDefault(x=>x.CUST_ID == id && x.CUST_ESTADO == true);
            
            if (iNV_CUSTODIO == null)
            {
                return HttpNotFound();
            }

            if (iNV_CUSTODIO.CUST_ID == 1)
            {
                return RedirectToAction("Index", "Custodio");
            }

            return View(iNV_CUSTODIO);
        }

        // POST: custodio/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult Edit(INV_CUSTODIO view_object)
        {
            //view_object.CUST_VALOR = ConvertirMoneda(Request.Form["VALOR"]);

            if (view_object.CUST_NOMBRE == null)
            {
                view_object.CUST_NOMBRE = "Ninguno";
            }

            if (view_object.CUST_CEDULA == null)
            {
                view_object.CUST_CEDULA = "0";
            }

            if (ModelState.IsValid)
            {
                if (view_object.CUST_TIPO.ToUpper() != "PERSONA")
                {
                    view_object.CUST_CEDULA = "";
                    view_object.CUST_NOMBRE = view_object.CUST_DEPARTAMENTO;
                }

                db.Entry(view_object).State = EntityState.Modified;
                db.SaveChanges();

                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Actualizo un registro de custodio en el modulo BI con el nombre (" + view_object.CUST_NOMBRE + ")");
                
                return RedirectToAction("Index");
            }

            var errors = ModelState.Values.SelectMany(v => v.Errors);
            
            return View(view_object);
        }

        [HttpPost]
        [Authorize(Roles = "BIA")]
        public JsonResult Delete(int? id)
        {
            try
            {
                INV_CUSTODIO iNV_CUSTODIO = db.INV_CUSTODIO.Find(id);

                iNV_CUSTODIO.CUST_ESTADO = false;

                db.SaveChanges();

                return Json("Eliminado");
            }
            catch (Exception e)
            {
                e.ToString();
                return Json("Error");
            }

        }

        [HttpPost]
        public JsonResult ValidarCedula(string cedula)
        {
            using (var WebClient = new WebClient())
            {
                try
                {
                    var url = Utilities.UrlWebService(cedula);
                    var result = WebClient.DownloadString(url);
                    var objeto = JsonConvert.DeserializeObject(result, typeof(Persona)) as Persona;
                    
                    if (objeto.Respuesta == "OK")
                    {
                        return Json(objeto);
                    }
                    else if(objeto.Respuesta == "Error")
                    {
                        return Json("Error en la validación");
                    }
                    else if (objeto.Respuesta == "TimeOut")
                    {
                        return Json("Tiempo de espera agotado. Intente de nuevo más tarde.");
                    }
                    else if (objeto.Respuesta == "NotFound")
                    {
                        return Json("No se ha encontrado la cédula digitada. Digite una existente.");
                    }
                    else if (objeto.Respuesta == "InvalidRequest")
                    {
                        return Json("Formato de cédula invalido. Por favor, digite uno correcto.");
                    }
                    else
                    {
                        return Json("Se ha producido un error. Intente más tarde.");
                    }
                }
                catch (Exception)
                {
                    return Json("Se ha producido un error. Intente más tarde.");
                }
            }
        }

        [CustomAuthorize]
        public ActionResult CreateModal()
        {
            return View();
        }

        public JsonResult GetCustodioHistorial(int inca_ID) {

            var inv_incalogs = db.INV_INCALOGS.Where(x => x.INCA_ID == inca_ID && x.CUST_CAMBIO).ToList();

            var inv_incalogid = inv_incalogs.Select(x=>x.LogId).ToArray();

            dbIntranet intra = new dbIntranet();
            
            var logs = intra.Logs.Where(x => inv_incalogid.Contains(x.LogId)).ToList();

            var empleados_id = logs.Select(x => x.LogUsuario).ToArray();

            var empleados = intra.VISTA_EMPLEADOS.Where(x => empleados_id.Contains(x.empleadoid));

            var HistorialCustodio = new List<HistorialCustodioVM>();

            foreach (var inv_incalog in inv_incalogs)
            {
                var log = logs.FirstOrDefault(l => l.LogId == inv_incalog.LogId);

                log.LogUsuarioNombre = empleados.FirstOrDefault(x => x.empleadoid == log.LogUsuario).nombre;

                HistorialCustodio.Add(new HistorialCustodioVM() { Custodio = inv_incalog.Custodio, Log = log });
            }
            return Json(HistorialCustodio);
        }

        public decimal ConvertirMoneda(string valor) {

            decimal ValorDesignado;

            if (!string.IsNullOrEmpty(valor) && decimal.TryParse(valor.Replace(",", "").Replace(".", ","), out ValorDesignado))
                return ValorDesignado;
            else
                return 0;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}