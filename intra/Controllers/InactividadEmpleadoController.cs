﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.SeguridadInformatica;
using intra.Models;
using System.Data.SqlClient;
using intra.Models.Almacen;


namespace intra.Controllers
{
    public class InactividadEmpleadoController : Controller
    {
        private dbIntranet db = new dbIntranet();
        private AlmacenEntities dbAlmacen = new AlmacenEntities();


        // GET: vw_Inactividad
        public ActionResult Inactividad()
        {
            ViewBag.DependenciaID = new SelectList(db.vw_Dependencias, "DependenciaID", "Descripcion").ToList();
            ViewBag.DepartamentoId = new SelectList(db.vw_Departamentos.ToList(), "DeptoID", "Descripcion");

            return View();
        }

        [HttpPost]
        public JsonResult SeleccionarDepartamento(int DependenciaId)
        {
            var Departamento = db.vw_Departamentos.Where(x => x.DependenciasID == DependenciaId.ToString()).ToList();
            return Json(Departamento);
        }


        [HttpPost]
        public JsonResult Inactividad(int id, string buscar, string dependencia, int? actividad)
        {
       
         
            if(buscar == "1")
            {
                try {
                    SqlParameter param1 = new SqlParameter("@EmpleadoId", id);
                   // var a = string.Format("exec sp_InactividadInfo {0}", id);
             var inactividad = db.Database.SqlQuery<sp_InactividadInfo>("exec sp_InactividadInfo @EmpleadoId", param1).Take(1).ToList();

              
                return Json(inactividad);
                }
                catch( Exception e)
                {
                    e.ToString();
                    return Json(new {success = false, Mensaje= "Ocurrio un error mientras hacia solicitud, Intente mas tarde" }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (buscar == "2")
            {
                try
                {
                    var inactividad = db.Database.SqlQuery<sp_InactividadInfo>(string.Format("exec sp_InactividadInfo {0}, {1}, {2}, {3},{4}", 0, id, 0, actividad, dependencia)).ToList();

                    return Json(inactividad);
                }
                catch (Exception e)
                {
                    e.ToString();
                    return Json(new { success = false, Mensaje = "Ocurrio un error mientras hacia solicitud, Intente mas tarde" }, JsonRequestBehavior.AllowGet);
                }

            }
            else if (buscar == "3")
            {
                var inactividad = db.Database.SqlQuery<sp_InactividadInfo>(string.Format("exec sp_InactividadInfo {0}, {1}, {2}, {3}, {4}", 0, 0,id, 0 , dependencia)).ToList();
                return Json(inactividad);

            }
            else if (buscar == "4")
            {
           
                var inactividad = db.Database.SqlQuery<sp_InactividadInfo>(string.Format("exec sp_InactividadInfo {0}, {1}, {2}, {3}, {4}", 0, 0, 0, id, dependencia)).ToList();

                return Json(inactividad);

            }
            //else if (buscar == "1")
            //{
            //    var inactividad = db.Database.SqlQuery<sp_InactividadInfo>(string.Format("exec sp_InactividadInfo {0}, {1}, {2}, {3}, {4}", 0, 0, 0, 0, id)).ToList();
            //    return Json(inactividad);

            //}



                return Json("");
        }


        // GET: vw_Inactividad/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vw_Inactividad vw_Inactividad = db.vw_Inactividad.Find(id);
            if (vw_Inactividad == null)
            {
                return HttpNotFound();
            }
            return View(vw_Inactividad);
        }

        // GET: vw_Inactividad/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: vw_Inactividad/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CodigoEmpleado,NombreCompleto,Dependencia,Departamento,Cargo,Cedula,EstadoEmpleado,InactividadTemporal,JustificacionInactividad")] vw_Inactividad vw_Inactividad)
        {
            if (ModelState.IsValid)
            {
                db.vw_Inactividad.Add(vw_Inactividad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vw_Inactividad);
        }

        // GET: vw_Inactividad/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vw_Inactividad vw_Inactividad = db.vw_Inactividad.Find(id);
            if (vw_Inactividad == null)
            {
                return HttpNotFound();
            }
            return View(vw_Inactividad);
        }

        // POST: vw_Inactividad/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CodigoEmpleado,NombreCompleto,Dependencia,Departamento,Cargo,Cedula,EstadoEmpleado,InactividadTemporal,JustificacionInactividad")] vw_Inactividad vw_Inactividad)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vw_Inactividad).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vw_Inactividad);
        }

        // GET: vw_Inactividad/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vw_Inactividad vw_Inactividad = db.vw_Inactividad.Find(id);
            if (vw_Inactividad == null)
            {
                return HttpNotFound();
            }
            return View(vw_Inactividad);
        }

        // POST: vw_Inactividad/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            vw_Inactividad vw_Inactividad = db.vw_Inactividad.Find(id);
            db.vw_Inactividad.Remove(vw_Inactividad);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
