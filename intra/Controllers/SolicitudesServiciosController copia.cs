﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Code;
using intra.Models.ViewModel;
using System.Configuration;
using System.IO;
using intra;
using System.Net.Sockets;
using intra.Models;
using intra.Models.Servicios;

namespace intra.Controllers
{
    public class SolicitudesServiciosController : Controller
    {
        public static string Direccion = ConfigurationManager.AppSettings["URLINTRA"];
        private dbIntranet db = new dbIntranet();
        Correo correo = new Correo();
        intra.Code.Utilities2 domain = new intra.Code.Utilities2();

        #region ObtenerEtiquetaHTML
        public string DescripcionCorreo(string Tema, string Departamento, string NombreCompleto, string Problema, string Link, string descripcion)
        {
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/Solicitud-nuevoServicio.html");

            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {
                Archivo = sr.ReadToEnd();
            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", Tema);
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", descripcion);

            return Archivo;
        }

        #endregion

        #region EnviarCorreo
        public void EnviarCorreo(string Supervisor, string Responsable, string Sujeto, string Descripcion)
        {
            string PrimerCorreo = "";

            string SegundoCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            if (Supervisor != "")
            {
                PrimerCorreo = Supervisor + "@PGR.GOB.DO";
            }
            else
            {
                PrimerCorreo = "";
            }

            SegundoCorreo = Responsable + "@PGR.GOB.DO";

            //ObjCorreo.EnviarCorreo(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion);

            correo.EnviarCorreo_(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion, Logo, Flecha, LogoDTI);
        }
        #endregion

     
        public string obtenerCorreoUsuario(string codigoEmpleado)
        {
            var cedulaEmpleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == codigoEmpleado).FirstOrDefault();
            var user = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", cedulaEmpleado.cedula)).FirstOrDefault();
            string correo = $"{user.samAccountName}@pgr.gob.do";

            return correo;
        }
        #region SendCorreo
        private void SendCorreo(string Sujeto, string Descripcion)
        {

            string codigoEmpleado = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoDTI" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoDTI = obtenerCorreoUsuario(codigoEmpleado);

            string codigoEmpleadoEncargadoDesarrollo = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoDesarrollo" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoDesarrollo = obtenerCorreoUsuario(codigoEmpleadoEncargadoDesarrollo);

            //string codigoEmpleadoEncargadoProyecto = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoProyecto" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            //string correoEncargadoProyecto = obtenerCorreoUsuario(codigoEmpleadoEncargadoProyecto);

            //string lider = Session["usuario"].ToString();
            //string correoLider = lider + "@pgr.gob.do";
            //Lider
            //string codigoEmpleadoLiderEquipo = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "Lider" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            //string correoLiderEquipo = obtenerCorreoUsuario(codigoEmpleadoLiderEquipo);

            string UsuarioResponsable = "";

            //   string PrimerCorreo = "";

            string SegundoCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            //CorreoSupervisor = Supervisor;

            // PrimerCorreo = Supervisor + "@PGR.GOB.DO";

            // UsuarioResponsable = Responsable + "@PGR.GOB.DO";

            SegundoCorreo = UsuarioResponsable;// + "@PGR.GOB.DO";

            // objCorreo.CorreoPaseProduccion(SegundoCorreo, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            correo.CorreoPaseProduccion(correoEncargadoDesarrollo, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            correo.CorreoPaseProduccion(correoEncargadoDTI, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            //correo.CorreoPaseProduccion(correoEncargadoProyecto, Sujeto, Descripcion, Logo, Flecha, LogoDTI);

        }
        #endregion
        #region ObtenerEtiquetaHTML
        public string SolicitudAprobacionPaseProduccion(string Departamento, string NombreCompleto, string Problema, string Link, string Descripcion)
        {


            //string NombreArchivo = Server.MapPath (@"~/EtiquetasHtml/Solicitud_nuevoServicio_PGR.html");
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/SolicitudPaseProduccion.html");


            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", "SOLICITUD PASE PRODUCCION");
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", Descripcion);



            return Archivo;
        }

        #endregion

        #region ObtenerEtiquetaHTML
        public string DescripcionCorreo(string Departamento, string NombreCompleto, string Problema, string Link, string Descripcion)
        {


            //string NombreArchivo = Server.MapPath (@"~/EtiquetasHtml/Solicitud_nuevoServicio_PGR.html");
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/Solicitud-nuevoServicio.html");


            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", "SERVICIO ASIGNADO");
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", Descripcion);



            return Archivo;
        }

        #endregion






        // GET: SolicitudesServicios
        //[HttpGet]
        [CustomAuthorize]
        public ActionResult ServiciosSolicitados()
        {

            int codigo = int.Parse(Session["empleadoId"].ToString());

            var Solicitado = (from r in db.vw_FormularioSolicitudServicios

                              select new vServiciosIntranet
                              {
                                  SolicitudId = r.CodigoSolicitud,
                                  SolicitanteId = r.CodigoSolicitante,
                                  ResponsableId = r.CodigoTecnico,
                                  Solicitante = r.NombreSolicitante,
                                  Responsable = r.NombreTecnico.ToUpper(),
                                  DependenciaId = r.DependenciaId,
                                  Dependencia = r.DependenciaNombre,
                                  TipoSolicitud = r.TipoSolicitud,
                                  TipoSolicitudId = r.CodigoTipoSolicitud,
                                  DescripcionSolicitud = r.DescripcionSolicitud,
                                  FechaSolicitud = r.FechaCreacion,
                                  Estatus = r.EstadoSolicitud,
                                  EstadoId = r.CodigoEstadoSolicitud,
                                  SubActividad = r.SubTipoSolicitud,
                                  CodigoSubactividad = r.CodigoSubTipoSolicitud,



                              }).Where(x => x.SolicitanteId == codigo && x.Estatus == "Pendiente")
                             .OrderByDescending(x => x.FechaSolicitud).ToList();


            return View(Solicitado);
        }

        [HttpPost]
        public JsonResult ServiciosSolicitados(int? estado, string filtro, string responsable, string FechaInicial, string FechaFinal, FormCollection fc)
        {


            int codigo = int.Parse(Session["empleadoId"].ToString());


            DateTime inicio = DateTime.ParseExact(FechaInicial, "dd/MM/yyyy", null);

            DateTime final = DateTime.ParseExact(FechaFinal, "dd/MM/yyyy", null);

            //Para que la fecha haga el calculo de los minutos y segundos
            DateTime Fechafinal = final.AddDays(1).AddSeconds(-1);

            var Solicitado = (from r in db.vw_FormularioSolicitudServicios

                              select new vServiciosIntranet
                              {
                                  SolicitudId = r.CodigoSolicitud,
                                  SolicitanteId = r.CodigoSolicitante,
                                  ResponsableId = r.CodigoTecnico,
                                  Solicitante = r.NombreSolicitante,
                                  Responsable = r.NombreTecnico.ToUpper(),
                                  DependenciaId = r.DependenciaId,
                                  Dependencia = r.DependenciaNombre,
                                  TipoSolicitud = r.TipoSolicitud,
                                  TipoSolicitudId = r.CodigoTipoSolicitud,
                                  DescripcionSolicitud = r.DescripcionSolicitud,
                                  FechaSolicitud = r.FechaCreacion,
                                  Estatus = r.EstadoSolicitud,
                                  EstadoId = r.CodigoEstadoSolicitud,
                                  SubActividad = r.SubTipoSolicitud,
                                  CodigoSubactividad = r.CodigoSubTipoSolicitud
                              }).Where(x => x.SolicitanteId == codigo).OrderByDescending(x=> x.FechaSolicitud).ToList();


            if (estado == 4 && filtro == "")
            {
                return Json(Solicitado);
            }

            else if (estado == 4 && filtro == "2" && responsable != "" )
            {
                var Solicitado41 = Solicitado.Where(x => x.Responsable.Contains(responsable.ToUpper()) ).OrderByDescending(x => x.FechaSolicitud).ToList();

                return Json(Solicitado41);
            }
            else if (filtro == "3" && estado == 4 && Fechafinal.ToString() != "" && FechaInicial != "")
            {
                var Solicitado42 = Solicitado.Where(x => x.SolicitanteId == codigo && x.FechaSolicitud >= inicio &&
                                x.FechaSolicitud <= Fechafinal).OrderByDescending(x => x.FechaSolicitud).ToList();

                return Json(Solicitado42);


            }

            else if (filtro == "3" && estado != 0 && Fechafinal.ToString() != "" && FechaInicial != "")
            {
                var Solicitado1 = Solicitado.Where(x => x.SolicitanteId == codigo && x.EstadoId == estado
                                && x.FechaSolicitud >= inicio &&
                                x.FechaSolicitud <= Fechafinal).OrderByDescending(x => x.FechaSolicitud).ToList();



                return Json(Solicitado1);


            }
            else if (filtro == "2" && responsable != "" && estado != 0)
            {

                var Solicitado2 = Solicitado.Where(x => x.EstadoId == estado
                      && x.Responsable.Contains(responsable.ToUpper())).OrderByDescending(x => x.FechaSolicitud).ToList();

                return Json(Solicitado2);

            }

            else if (estado != 0 && filtro == "" || filtro == null || filtro == "2" || filtro == "3" && responsable == "")
            {
                var Solicitado3 = Solicitado.Where(x => x.SolicitanteId == codigo && x.EstadoId == estado
                                  ).OrderByDescending(x => x.FechaSolicitud).ToList();

                return Json(Solicitado3);

            }

            return Json(Solicitado);

        }


        [CustomAuthorize]
        public ActionResult ServiciosAsignados()
        {

            int codigo = int.Parse(Session["empleadoId"].ToString());
            string dependenciaId = Session["dependenciaId"].ToString();
            string departamentoId = Session["departamentoId"].ToString();
            int Departamento = int.Parse(departamentoId);
            var Asignado = db.Sol_Registro_Solicitud.Where(x => x.TecnicoId == codigo && x.EstadoId == 1).ToList();
            var solicitado = db.Sol_Registro_Solicitud.Where(x => x.SolicitanteId == codigo && x.EstadoId == 1).ToList();

            HttpContext.Session["CantidadAsignados"] = Asignado.Count();
            HttpContext.Session["CantidadSolicitados"] = solicitado.Count();


            var Otrostecnicos = db.Sol_Otros_Tecnicos.Where(x => x.TecnicoId == codigo && x.Estado == true).FirstOrDefault();
            if (Otrostecnicos != null)
            {
                ViewBag.Otrostecnicos = "Otrostecnico";
            }

            var director = db.Sol_Departamento.Where(x => x.DirectorId == codigo && x.Estado == true).FirstOrDefault();
            if (director != null)
            {
                ViewBag.Director = "Director";
            }
            var Supervisor = db.Sol_Actividades.Where(x => x.SupervisorId == codigo && x.Estado == true).FirstOrDefault();
            if (Supervisor != null)
            {
                ViewBag.Supervisor = "Supervisor";
            }

            var TecnicoAlternativo = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo && x.Estado == true).ToList();
            if (TecnicoAlternativo.Count > 0)
            {
                ViewBag.TecnicoAlternativo = "tecnicoAlternativo";
            }
           

            var asignadoEmpleado = (from a in db.Sol_Mostrar_Personal_PGR
                                    where a.CodigoDependencia == dependenciaId && a.CodigoDepartamento == departamentoId
                                    orderby a.NombreCompleto ascending
                                    select new PersonalPgrModel
                                    {
                                        EmpleadoId = a.Codigo,
                                        Descripcion = a.NombreCompleto,
                                        Cedula = a.Cedula,


                                    }).ToList();
            ViewBag.tecnico = asignadoEmpleado;



            // var Asignados = db.Sol_Detalle_Lista.Where(x=> x.CodigoTecnico == codigo && x.CodigoEstadoSolicitud == 1).ToList();

            //var Asignados = db.Sol_Registro_Solicitud.ToList();

          //  List<int> solicitudIds = db.Sol_Registro_Solicitud.Where(x => x.TecnicoId == codigo && x.EstadoId == 1).Select(x => x.SolicitudId).ToList();

            var Asignados = (from r in db.vw_FormularioSolicitudServicios

                             select new vServiciosIntranet
                             {
                                 SolicitudId = r.CodigoSolicitud,
                                 SolicitanteId = r.CodigoSolicitante,
                                 ResponsableId = r.CodigoTecnico,
                                 Solicitante = r.NombreSolicitante,
                                 Responsable = r.NombreTecnico.ToUpper(),
                                 DependenciaId = r.DependenciaId,
                                 Dependencia = r.DependenciaNombre,
                                 TipoSolicitud = r.TipoSolicitud,
                                 TipoSolicitudId = r.CodigoTipoSolicitud,
                                 DescripcionSolicitud = r.DescripcionSolicitud,
                                 FechaSolicitud = r.FechaCreacion,
                                 Estatus = r.EstadoSolicitud,
                                 EstadoId = r.CodigoEstadoSolicitud,
                                 SubActividad = r.SubTipoSolicitud,
                                 CodigoSubactividad = r.CodigoSubTipoSolicitud
                                 //SolicitudId = r.CodigoSolicitud,
                                 //SolicitanteId = r.CodigoSolicitante,
                                 //ResponsableId = r.CodigoTecnico,
                                 //Solicitante = r.NombreSolicitante.ToUpper(),
                                 //Responsable = r.NombreTecnico,
                                 //DependenciaId = r.DependenciaId,
                                 //Dependencia = r.DependenciaNombre,
                                 //TipoSolicitud = r.TipoSolicitud,
                                 //TipoSolicitudId = r.CodigoTipoSolicitud,
                                 //DescripcionSolicitud = r.DescripcionSolicitud,
                                 //FechaSolicitud = r.FechaCreacion,
                                 //Estatus = r.EstadoSolicitud,
                                 //EstadoId = r.CodigoEstadoSolicitud,
                                 //SubActividad = r.SubTipoSolicitud,
                                 //DepartamentoId = int.Parse( r.CodigoDepartamento.ToString()),
                                 //Departamento = r.Departamento,
                                 //CodigoSubactividad = r.CodigoSubTipoSolicitud
                             }).Where(x => x.ResponsableId == codigo  && x.EstadoId == 1).ToList();

            //return View(Asignados.Where(x => x.EstadoId == 1).OrderByDescending(x => x.FechaSolicitud).ToList());

            if (director == null && TecnicoAlternativo == null && Supervisor == null)
            {
                var AsignadosUsuariosNormal = Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == 1)
                             .OrderByDescending(x => x.FechaSolicitud).ToList();
                return View(AsignadosUsuariosNormal);

            }
            else if (director != null)
            {
                var AsignadosDirector = Asignados.Where(x => x.DepartamentoId == director.DepartamentoId && x.Estatus == "Pendiente").ToList();

                return View(AsignadosDirector);
            }
            else if (Supervisor != null)
            {
                var AsignadoSupervisor = Asignados.Where(x => x.TipoSolicitudId == Supervisor.ActividadId && codigo == Supervisor.SupervisorId && x.Estatus == "Pendiente").ToList();

                return View(AsignadoSupervisor);
            }
            else if (TecnicoAlternativo != null)
            {
                var AsignadoTecnicoAlternativo = Asignados;


                var actividades = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo).ToList();


                foreach (var item in actividades)
                {
                    var Sol_actividades = (from r in db.vw_FormularioSolicitudServicios

                                           select new vServiciosIntranet
                                           {
                                               SolicitudId = r.CodigoSolicitud,
                                               SolicitanteId = r.CodigoSolicitante,
                                               ResponsableId = r.CodigoTecnico,
                                               Solicitante = r.NombreSolicitante,
                                               Responsable = r.NombreTecnico.ToUpper(),
                                               DependenciaId = r.DependenciaId,
                                               Dependencia = r.DependenciaNombre,
                                               TipoSolicitud = r.TipoSolicitud,
                                               TipoSolicitudId = r.CodigoTipoSolicitud,
                                               DescripcionSolicitud = r.DescripcionSolicitud,
                                               FechaSolicitud = r.FechaCreacion,
                                               Estatus = r.EstadoSolicitud,
                                               EstadoId = r.CodigoEstadoSolicitud,
                                               SubActividad = r.SubTipoSolicitud,
                                               CodigoSubactividad = r.CodigoSubTipoSolicitud
                                               //SolicitudId = r.CodigoSolicitud,
                                               //SolicitanteId = r.CodigoSolicitante,
                                               //ResponsableId = r.CodigoTecnico,
                                               //Solicitante = r.NombreSolicitante.ToUpper(),
                                               //Responsable = r.NombreTecnico,
                                               //DependenciaId = r.DependenciaId,
                                               //Dependencia = r.DependenciaNombre,
                                               //TipoSolicitud = r.TipoSolicitud,
                                               //TipoSolicitudId = r.CodigoTipoSolicitud,
                                               //DescripcionSolicitud = r.DescripcionSolicitud,
                                               //FechaSolicitud = r.FechaCreacion,
                                               //Estatus = r.EstadoSolicitud,
                                               //EstadoId = r.CodigoEstadoSolicitud,
                                               //SubActividad = r.SubTipoSolicitud,
                                               //DepartamentoId = r.CodigoDepartamento,
                                               //Departamento = r.Departamento,
                                               //CodigoSubactividad = r.CodigoSubTipoSolicitud
                                           }).Where(x => (x.TipoSolicitudId == item.ActividadId) && x.Estatus == "Pendiente").ToList();
                    //var Sol_actividades =
                    //     (from r in  db.Sol_Detalle_Lista.Where(x => (x.CodigoTipoSolicitud == item.ActividadId) && x.EstadoSolicitud == "Pendiente")
                    //    .ToList();
                    foreach (var item2 in Sol_actividades)
                    {
                        if (!AsignadoTecnicoAlternativo.Contains(item2))
                        {
                            AsignadoTecnicoAlternativo.Add(item2);
                        }
                    }
                }

                return View(AsignadoTecnicoAlternativo);


                }


                // return View(Asignados.Where(x => x.ResponsableId == codigo).ToList());
                return View(Asignados);
        }


        [HttpPost]
        public JsonResult ServiciosAsignados(int? estado, string filtro, string responsable, string solicitante, string fechaInicial, string fechaFinal, string TipoSolicitudId, FormCollection fc)
        {
            //estado = int.Parse(Request.Form["Estados"]);
            //filtro = Request.Form["Filtro"];
            //responsable = Request.Form["Responsable"];
            //fechaInicial = Request.Form["FechaInicial"];
            //fechaFinal = Request.Form["FechaFinal"];
            ////TipoSolicitudId = Request.Form["ActividadId"];
            ////int tiposolicitud = int.Parse(TipoSolicitudId);

            int codigo = int.Parse(Session["empleadoId"].ToString());

            //string dependenciaId = Session["dependenciaId"].ToString();
            //string departamentoId = Session["departamentoId"].ToString();

            DateTime inicio = DateTime.ParseExact(fechaInicial, "dd/MM/yyyy", null);

            DateTime final = DateTime.ParseExact(fechaFinal, "dd/MM/yyyy", null);


            //Para que la fecha haga el calculo de los minutos y segundos
            DateTime Fechafinal = final.AddDays(1).AddSeconds(-1);

            var state = db.Sol_Estado_Solicitud.Where(x => x.EstadoId == estado).FirstOrDefault();

            var Otrostecnicos = db.Sol_Otros_Tecnicos.Where(x => x.TecnicoId == codigo && x.Estado == true).FirstOrDefault();

            var director = db.Sol_Departamento.Where(x => x.DirectorId == codigo && x.Estado == true).FirstOrDefault();

            var Supervisor = db.Sol_Actividades.Where(x => x.SupervisorId == codigo && x.Estado == true).FirstOrDefault();
 
            var Asignados = (from r in db.Sol_Detalle_Lista

                             select new vServiciosIntranet
                             {
                                 SolicitudId = r.CodigoSolicitud,
                                 SolicitanteId = r.CodigoSolicitante,
                                 ResponsableId = r.CodigoTecnico,
                                 Solicitante = r.NombreSolicitante.ToUpper(),
                                 Responsable = r.NombreTecnico,
                                 DependenciaId = r.DependenciaId,
                                 Dependencia = r.DependenciaNombre,
                                 TipoSolicitud = r.TipoSolicitud,
                                 TipoSolicitudId = r.CodigoTipoSolicitud,
                                 DescripcionSolicitud = r.DescripcionSolicitud,
                                 FechaSolicitud = r.FechaCreacion,
                                 Estatus = r.EstadoSolicitud,
                                 EstadoId = r.CodigoEstadoSolicitud,
                                 SubActividad = r.SubTipoSolicitud,
                                 CodigoSubactividad = r.CodigoSubTipoSolicitud

                             }).Where(x =>  x.ResponsableId == codigo).ToList();

            if (filtro == "" && estado == null)
            {
                if (director == null && /*TecnicoAlternativo == null &&*/ Supervisor == null)
                {
                    var AsignadosUsuariosNormal = Asignados.Where(x => x.ResponsableId == codigo).ToList();
                    return Json(AsignadosUsuariosNormal);

                }
                else if (director != null)
                {
                    var AsignadosDirector = Asignados.Where(x => x.DepartamentoId == director.DepartamentoId).OrderByDescending(x => x.FechaSolicitud).ToList();
                    return Json(AsignadosDirector);
                }
                else if (Supervisor != null)
                {
                    var AsignadoSupervisor = Asignados.Where(x => x.TipoSolicitudId == Supervisor.ActividadId && codigo == Supervisor.SupervisorId).OrderByDescending(x => x.FechaSolicitud).ToList();
                    return Json(AsignadoSupervisor);
                }
            }

            if (filtro == "3" && estado != 0 && Fechafinal.ToString() != "" && fechaInicial != "")
            {

                if (director == null && /*TecnicoAlternativo == null &&*/ Supervisor == null)
                {
                    var AsignadosUsuariosNormal = Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == estado
                             && x.FechaSolicitud >= inicio &&
                             x.FechaSolicitud <= Fechafinal)
                                 .OrderByDescending(x => x.FechaSolicitud).ToList();
                    return Json(AsignadosUsuariosNormal);

                }
                else if (director != null)
                {
                    var AsignadosDirector = Asignados.Where(x => x.DepartamentoId == director.DepartamentoId && x.EstadoId == estado
                             && x.FechaSolicitud >= inicio &&
                             x.FechaSolicitud <= Fechafinal).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadosDirector);
                }
                else if (Supervisor != null)
                {
                    var AsignadoSupervisor = Asignados.Where(x => x.TipoSolicitudId == Supervisor.ActividadId && codigo == Supervisor.SupervisorId && x.EstadoId == estado
                             && x.FechaSolicitud >= inicio &&
                             x.FechaSolicitud <= Fechafinal).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadoSupervisor);
                }

                #region comentario de codigo filtro tecnico altenativo ya no es necesario 
                //else if (TecnicoAlternativo != null)
                //{
                //    var AsignadoTecnicoAlternativo = Asignados;


                //    var actividades = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo).ToList();


                //    foreach (var item in actividades)
                //    {
                //        var Sol_actividades = (from r in db.Sol_Detalle_Lista

                //                               select new vServiciosIntranet
                //                               {
                //                                   SolicitudId = r.CodigoSolicitud,
                //                                   SolicitanteId = r.CodigoSolicitante,
                //                                   ResponsableId = r.CodigoTecnico,
                //                                   Solicitante = r.NombreSolicitante.ToUpper(),
                //                                   Responsable = r.NombreTecnico,
                //                                   DependenciaId = r.DependenciaId,
                //                                   Dependencia = r.DependenciaNombre,
                //                                   TipoSolicitud = r.TipoSolicitud,
                //                                   TipoSolicitudId = r.CodigoTipoSolicitud,
                //                                   DescripcionSolicitud = r.DescripcionSolicitud,
                //                                   FechaSolicitud = r.FechaCreacion,
                //                                   Estatus = r.EstadoSolicitud,
                //                                   EstadoId = r.CodigoEstadoSolicitud,
                //                                   SubActividad = r.SubTipoSolicitud,
                //                                   DepartamentoId = r.CodigoDepartamento,
                //                                   Departamento = r.Departamento,
                //                                   CodigoSubactividad = r.CodigoSubTipoSolicitud
                //                               }).Where(x => (x.TipoSolicitudId == item.ActividadId) && x.Estatus == "Pendiente").ToList();
                //        //var Sol_actividades =
                //        //     (from r in  db.Sol_Detalle_Lista.Where(x => (x.CodigoTipoSolicitud == item.ActividadId) && x.EstadoSolicitud == "Pendiente")
                //        //    .ToList();
                //        foreach (var item2 in Sol_actividades)
                //        {
                //            if (!AsignadoTecnicoAlternativo.Contains(item2))
                //            {
                //                AsignadoTecnicoAlternativo.Add(item2);
                //            }
                //        }
                //    }

                //    return Json(AsignadoTecnicoAlternativo);

                //}

                #endregion

            }

            else if (filtro == "2" && responsable != "" && estado != 0)
            {
                if (director == null && /*TecnicoAlternativo == null &&*/ Supervisor == null)
                {
                    var AsignadosUsuariosNormal = Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == estado
                            && x.Responsable.ToLower().Contains(responsable.ToLower())).ToList();
                    return Json(AsignadosUsuariosNormal);

                }
                else if (director != null)
                {
                    var AsignadosDirector = Asignados.Where(x => x.DepartamentoId == director.DepartamentoId && x.EstadoId == estado
                            && x.Responsable.ToLower().Contains(responsable.ToLower())).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadosDirector);
                }
                else if (Supervisor != null)
                {
                    var AsignadoSupervisor = Asignados.Where(x => x.TipoSolicitudId == Supervisor.ActividadId && codigo == Supervisor.SupervisorId && x.EstadoId == estado
                            && x.Responsable.ToLower().Contains(responsable.ToLower())).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadoSupervisor);
                }

                #region comentario de codigo filtro tecnico altenativo ya no es necesario
                //else if (TecnicoAlternativo != null)
                //{
                //    var AsignadoTecnicoAlternativo = Asignados.Where(x => x.ResponsableId == codigo).ToList();


                //    var actividades = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo).ToList();


                //    foreach (var item in actividades)
                //    {
                //        var Sol_actividades = Asignados.Where(x => (x.TipoSolicitudId == item.ActividadId) && x.EstadoId == estado
                //            && x.Solicitante.ToLower().Contains(responsable.ToLower())).OrderByDescending(x => x.FechaSolicitud).ToList();
                //        foreach (var item2 in Sol_actividades)
                //        {
                //            if (!AsignadoTecnicoAlternativo.Contains(item2))
                //            {
                //                AsignadoTecnicoAlternativo.Add(item2);
                //            }
                //        }

                //    }
                //    return Json(AsignadoTecnicoAlternativo.Where(x=> x.Solicitante.ToLower().Contains(responsable.ToLower())).ToList());


                //}
                #endregion

            }



            else if (filtro == "4" && solicitante != "" && estado != 0)
            {
                if (director == null && /*TecnicoAlternativo == null &&*/ Supervisor == null)
                {
                    var AsignadosUsuariosNormal = Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == estado
                            && x.Solicitante.ToLower().Contains(solicitante.ToLower())).ToList();
                    return Json(AsignadosUsuariosNormal);

                }
                else if (director != null)
                {
                    var AsignadosDirector = Asignados.Where(x => x.DepartamentoId == director.DepartamentoId && x.EstadoId == estado
                            && x.Solicitante.ToLower().Contains(solicitante.ToLower())).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadosDirector);
                }
                else if (Supervisor != null)
                {
                    var AsignadoSupervisor = Asignados.Where(x => x.TipoSolicitudId == Supervisor.ActividadId && codigo == Supervisor.SupervisorId && x.EstadoId == estado
                            && x.Solicitante.ToLower().Contains(solicitante.ToLower())).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadoSupervisor);
                }
                #region comentario de codigo filtro tecnico altenativo ya no es necesario
                //else if (TecnicoAlternativo != null)
                //{
                //    var AsignadoTecnicoAlternativo = Asignados.Where(x => x.ResponsableId == codigo).ToList();


                //    var actividades = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo).ToList();


                //    foreach (var item in actividades)
                //    {
                //        var Sol_actividades = Asignados.Where(x => (x.TipoSolicitudId == item.ActividadId) && x.EstadoId == estado
                //            && x.Solicitante.ToLower().Contains(solicitante.ToLower())).OrderByDescending(x => x.FechaSolicitud).ToList();
                //        foreach (var item2 in Sol_actividades)
                //        {
                //            if (!AsignadoTecnicoAlternativo.Contains(item2))
                //            {
                //                AsignadoTecnicoAlternativo.Add(item2);
                //            }
                //        }

                //    }
                //    return Json(AsignadoTecnicoAlternativo.Where(x => x.Solicitante.Contains(solicitante.ToLower())).ToList());


                //}
                #endregion

            }



            else if (estado != 0 && filtro == "" || filtro == null || filtro == "2" || filtro == "3" && responsable == "")
            {

                if (director == null && /*TecnicoAlternativo == null &&*/ Supervisor == null)
                {
                    var AsignadosUsuariosNormal = Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == estado
                            ).ToList();
                    return Json(AsignadosUsuariosNormal);

                }
                else if (director != null)
                {
                    var AsignadosDirector = Asignados.Where(x => x.DepartamentoId == director.DepartamentoId && x.EstadoId == estado
                            ).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadosDirector);
                }
                else if (Supervisor != null)
                {
                    var AsignadoSupervisor = Asignados.Where(x => x.TipoSolicitudId == Supervisor.ActividadId && codigo == Supervisor.SupervisorId && x.EstadoId == estado
                           ).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadoSupervisor);
                }
                //else if (false /*TecnicoAlternativo != null*/)
                //{
                //    var AsignadoTecnicoAlternativo = Asignados;

                //    var listadoID = AsignadoTecnicoAlternativo.Select(x=>x.SolicitudId).ToList();                    

                //    var actividades = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo).ToList();


                //    //foreach (var item in actividades)
                //    //{
                //    //    var Sol_actividades = (from r in db.Sol_Detalle_Lista

                //    //                           select new vServiciosIntranet
                //    //                           {
                //    //                               SolicitudId = r.CodigoSolicitud,
                //    //                               SolicitanteId = r.CodigoSolicitante,
                //    //                               ResponsableId = r.CodigoTecnico,
                //    //                               Solicitante = r.NombreSolicitante.ToUpper(),
                //    //                               Responsable = r.NombreTecnico,
                //    //                               DependenciaId = r.DependenciaId,
                //    //                               Dependencia = r.DependenciaNombre,
                //    //                               TipoSolicitud = r.TipoSolicitud,
                //    //                               TipoSolicitudId = r.CodigoTipoSolicitud,
                //    //                               DescripcionSolicitud = r.DescripcionSolicitud,
                //    //                               FechaSolicitud = r.FechaCreacion,
                //    //                               Estatus = r.EstadoSolicitud,
                //    //                               EstadoId = r.CodigoEstadoSolicitud,
                //    //                               SubActividad = r.SubTipoSolicitud,
                //    //                               DepartamentoId = r.CodigoDepartamento,
                //    //                               Departamento = r.Departamento,
                //    //                               CodigoSubactividad = r.CodigoSubTipoSolicitud
                //    //                           }).Where(x => !listadoID.Contains(x.SolicitudId) && x.TipoSolicitudId == item.ActividadId && x.Estatus == "Pendiente" ).ToList();

                //    //    //foreach (var item2 in Sol_actividades)
                //    //    //{
                //    //    //    if (!AsignadoTecnicoAlternativo.Contains(item2))
                //    //    //    {
                //    //    //        AsignadoTecnicoAlternativo.Add(item2);
                //    //    //    }
                //    //    //}

                //    //    AsignadoTecnicoAlternativo.AddRange(Sol_actividades);

                //    //}

                //    var lActividades = actividades.Select(x => x.ActividadId).ToList();
                //    var rawQuer = (db.Sol_Detalle_Lista.Where(x => x.EstadoSolicitud == "Pendiente" && !listadoID.Contains(x.CodigoSolicitud) && lActividades.Contains(x.CodigoTipoSolicitud)).Select(r =>
                //           new vServiciosIntranet
                //           {
                //               SolicitudId = r.CodigoSolicitud,
                //               SolicitanteId = r.CodigoSolicitante,
                //               ResponsableId = r.CodigoTecnico,
                //               Solicitante = r.NombreSolicitante.ToUpper(),
                //               Responsable = r.NombreTecnico,
                //               DependenciaId = r.DependenciaId,
                //               Dependencia = r.DependenciaNombre,
                //               TipoSolicitud = r.TipoSolicitud,
                //               TipoSolicitudId = r.CodigoTipoSolicitud,
                //               DescripcionSolicitud = r.DescripcionSolicitud,
                //               FechaSolicitud = r.FechaCreacion,
                //               Estatus = r.EstadoSolicitud,
                //               EstadoId = r.CodigoEstadoSolicitud,
                //               SubActividad = r.SubTipoSolicitud,
                //               DepartamentoId = r.CodigoDepartamento,
                //               Departamento = r.Departamento,
                //               CodigoSubactividad = r.CodigoSubTipoSolicitud
                //           }));
                //    //AsignadoTecnicoAlternativo.AddRange((from r in db.Sol_Detalle_Lista

                //    //                                     select new vServiciosIntranet
                //    //                                     {
                //    //                                         SolicitudId = r.CodigoSolicitud,
                //    //                                         SolicitanteId = r.CodigoSolicitante,
                //    //                                         ResponsableId = r.CodigoTecnico,
                //    //                                         Solicitante = r.NombreSolicitante.ToUpper(),
                //    //                                         Responsable = r.NombreTecnico,
                //    //                                         DependenciaId = r.DependenciaId,
                //    //                                         Dependencia = r.DependenciaNombre,
                //    //                                         TipoSolicitud = r.TipoSolicitud,
                //    //                                         TipoSolicitudId = r.CodigoTipoSolicitud,
                //    //                                         DescripcionSolicitud = r.DescripcionSolicitud,
                //    //                                         FechaSolicitud = r.FechaCreacion,
                //    //                                         Estatus = r.EstadoSolicitud,
                //    //                                         EstadoId = r.CodigoEstadoSolicitud,
                //    //                                         SubActividad = r.SubTipoSolicitud,
                //    //                                         DepartamentoId = r.CodigoDepartamento,
                //    //                                         Departamento = r.Departamento,
                //    //                                         CodigoSubactividad = r.CodigoSubTipoSolicitud
                //    //                                     }).Where(x => x.Estatus == "Pendiente" && !listadoID.Contains(x.SolicitudId) && lActividades.Contains(x.TipoSolicitudId)));
                      


                //    return Json(AsignadoTecnicoAlternativo.Where(x=> x.EstadoId == estado));

                //}


                }

                //        var Asignados3 = Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == estado
                //                         ).OrderByDescending(x => x.FechaSolicitud).ToList();


                //    ViewBag.Estado = state.Descripcion;
                //    return View("ServiciosAsignados", Asignados3);
                //}
                return Json(Asignados.Where(x=> x.ResponsableId == codigo && x.EstadoId == estado ).ToList());

            
        }

        [HttpPost]
        public JsonResult TecnicoAlternativo(int? estado, string filtro, string responsable, string solicitante, string fechaInicial, string fechaFinal, string TipoSolicitudId)
        {
           int  codigo = int.Parse(Session["empleadoId"].ToString());




            var Asignados = (from r in db.Sol_Detalle_Lista

                             select new vServiciosIntranet
                             {
                                 SolicitudId = r.CodigoSolicitud,
                                 SolicitanteId = r.CodigoSolicitante,
                                 ResponsableId = r.CodigoTecnico,
                                 Solicitante = r.NombreSolicitante.ToUpper(),
                                 Responsable = r.NombreTecnico,
                                 DependenciaId = r.DependenciaId,
                                 Dependencia = r.DependenciaNombre,
                                 TipoSolicitud = r.TipoSolicitud,
                                 TipoSolicitudId = r.CodigoTipoSolicitud,
                                 DescripcionSolicitud = r.DescripcionSolicitud,
                                 FechaSolicitud = r.FechaCreacion,
                                 Estatus = r.EstadoSolicitud,
                                 EstadoId = r.CodigoEstadoSolicitud,
                                 SubActividad = r.SubTipoSolicitud,
                                 DepartamentoId = r.CodigoDepartamento,
                                 Departamento = r.Departamento,
                                 CodigoSubactividad = r.CodigoSubTipoSolicitud
                             }).Where(x => x.ResponsableId == codigo && x.EstadoId == 1).ToList();


            var AsignadoTecnicoAlternativo = Asignados;


            var actividades = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo).ToList();


            foreach (var item in actividades)
            {
                var Sol_actividades = (from r in db.Sol_Detalle_Lista

                                       select new vServiciosIntranet
                                       {
                                           SolicitudId = r.CodigoSolicitud,
                                           SolicitanteId = r.CodigoSolicitante,
                                           ResponsableId = r.CodigoTecnico,
                                           Solicitante = r.NombreSolicitante.ToUpper(),
                                           Responsable = r.NombreTecnico,
                                           DependenciaId = r.DependenciaId,
                                           Dependencia = r.DependenciaNombre,
                                           TipoSolicitud = r.TipoSolicitud,
                                           TipoSolicitudId = r.CodigoTipoSolicitud,
                                           DescripcionSolicitud = r.DescripcionSolicitud,
                                           FechaSolicitud = r.FechaCreacion,
                                           Estatus = r.EstadoSolicitud,
                                           EstadoId = r.CodigoEstadoSolicitud,
                                           SubActividad = r.SubTipoSolicitud,
                                           DepartamentoId = r.CodigoDepartamento,
                                           Departamento = r.Departamento,
                                           CodigoSubactividad = r.CodigoSubTipoSolicitud
                                       }).Where(x => (x.TipoSolicitudId == item.ActividadId) && x.Estatus == "Pendiente").ToList();
               
                foreach (var item2 in Sol_actividades)
                {
                    if (!AsignadoTecnicoAlternativo.Contains(item2))
                    {
                        AsignadoTecnicoAlternativo.Add(item2);
                    }
                }
            }

          return Json(AsignadoTecnicoAlternativo,JsonRequestBehavior.AllowGet);

           // return ccc ("ServiciosAsignados", "SolicitudesServicios", AsignadoTecnicoAlternativo);

        }


        [HttpPost]
        public JsonResult Asignar(int[] solicitudId, string[] tecnico1)
        {
            if (tecnico1 == null)
            {
                return Json(new { success = false, Mensaje = "Seleccione la persona que se le va a asignar el servicio" });
            }
            string empleadoId = Session["empleadoId"].ToString();

            for (int i = 0; i < solicitudId.Length; i++)
            {

                int sol = solicitudId[i];
                var asignado = db.Sol_Registro_Solicitud.Where(x => x.SolicitudId == sol).FirstOrDefault();

                //for (int a = 0; a <= tecnico1.Length; a++)
                //{

                asignado.TecnicoId = int.Parse(tecnico1[i]);

                var tecnico = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == asignado.TecnicoId.ToString()).FirstOrDefault();

                asignado.DomainUserTecnico = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", tecnico.Cedula)).FirstOrDefault().samAccountName; ;

                asignado.TecnicoId = asignado.TecnicoId;
                asignado.DomainUserTecnico = asignado.DomainUserTecnico;
                asignado.FhFinalSolicitud = DateTime.Now;
                asignado.FhModificacion = DateTime.Now;
                db.SaveChanges();

                var empleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == asignado.TecnicoId.ToString()).FirstOrDefault();
                var supervisor = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == empleadoId).FirstOrDefault();
                var actividad = db.Sol_Actividades.Where(x => x.ActividadId == asignado.Tipo_SolicitudId).FirstOrDefault();


                string descripcion = "";

                descripcion = "SE REASIGNO " + empleado.nombre.ToUpper() + " POR " + supervisor.nombre.ToUpper();


                //Obtener Direccion Ip de la Pc
                string IP4Address = String.Empty;

                foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
                {
                    if (IPA.AddressFamily == AddressFamily.InterNetwork)
                    {
                        IP4Address = IPA.ToString();
                        break;
                    }
                }

             
                    if (HttpContext.Session["empleadoId"] != null)
                    {
                        Sol_Logs Tb = new Sol_Logs();
                        Tb.SistemaId = 1;
                        Tb.SolicitudId = asignado.SolicitudId;
                        Tb.logIP = IP4Address;
                        Tb.logUsuario = HttpContext.Session["empleadoId"].ToString();
                        Tb.logFecha = DateTime.Now;
                        Tb.logAccion = descripcion;
                        db.Sol_Logs.Add(Tb);
                        db.SaveChanges();
                    }

                //reg.InsertarLog(asignado.SolicitudId, descripcion);

                if (asignado.DependenciaRegionalId == 1)
                {
                    int CdgTipoSolicitud = asignado.SolicitudId;

                    var Supervisor = db.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                    string correo_supervisor = "";

                    if (Supervisor != null)
                    {
                        if (Supervisor.CodigoDepartamento == 15)
                        {
                            correo_supervisor = Code.Utilities2.obtenerCorreoUsuario(Supervisor.Cedula);
                        }
                        else
                        {
                            correo_supervisor = Code.Utilities2.obtenerCorreoUsuario(Supervisor.Cedula);
                        }
                    }

                    String CorreoUsuario = "";

                    var empleado_Cedula = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == empleado.cedula).FirstOrDefault();

                    if (empleado_Cedula != null)
                    {
                        if (empleado_Cedula.CodigoDependencia == "10001" && empleado_Cedula.CodigoDepartamento == "15")
                        {
                            CorreoUsuario = Code.Utilities2.obtenerCorreoUsuario(empleado_Cedula.Cedula);
                        }
                        else
                        {
                            CorreoUsuario = Code.Utilities2.obtenerCorreoUsuario(empleado_Cedula.Cedula);
                        }
                    }

                    var solicitante_obj = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == asignado.SolicitanteId.ToString()).FirstOrDefault();

                    string solicitante_correo = "";

                    if (solicitante_obj != null)
                    {
                        if (solicitante_obj.CodigoDependencia == "10001" && solicitante_obj.CodigoDepartamento == "15")
                        {
                            solicitante_correo = Code.Utilities2.obtenerCorreoUsuario(solicitante_obj.Cedula);
                        }
                        else
                        {
                            solicitante_correo = Code.Utilities2.obtenerCorreoUsuario(solicitante_obj.Cedula);
                        }
                    }

                    string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/RegistroSolicitud?id={asignado.SolicitudId}";

                    EnviarCorreo(asignado.DomainUserSolicitante, asignado.DomainUserTecnico, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + supervisor.dependencia.ToUpper(), DescripcionCorreo("REASIGNACIÓN DE SOLICITUD", solicitante_obj.Departamento.ToUpper(), solicitante_obj.NombreCompleto.ToUpper(), asignado.DescripcionSolicitud, url, actividad.Descripcion));

                }


                else
                {
                    var Supervisor = db.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == asignado.DependenciaRegionalId && cd.DependenciaEstado == true).FirstOrDefault();

                    string correo_supervisor = "";

                    if (Supervisor != null)
                    {
                        var Supervisor_DatosPersonales = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == Supervisor.DependenciaSupervisorId.ToString()).FirstOrDefault();

                        correo_supervisor = Code.Utilities2.obtenerCorreoUsuario(Supervisor_DatosPersonales.Cedula);
                    }

                    String CorreoUsuario = "";

                    var empleado_Cedula = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == empleado.cedula).FirstOrDefault();

                    if (empleado_Cedula != null)
                    {
                        if (empleado_Cedula.CodigoDependencia == "10001" && empleado_Cedula.CodigoDepartamento == "15")
                        {
                            CorreoUsuario = Code.Utilities2.obtenerCorreoUsuario(empleado_Cedula.Cedula);
                        }
                        else
                        {
                            CorreoUsuario = Code.Utilities2.obtenerCorreoUsuario(empleado_Cedula.Cedula);
                        }
                    }

                    var solicitante_obj = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == asignado.SolicitanteId.ToString()).FirstOrDefault();

                    string solicitante_correo = "";

                    if (solicitante_obj != null)
                    {
                        if (solicitante_obj.CodigoDependencia == "10001" && solicitante_obj.CodigoDepartamento == "15")
                        {
                            solicitante_correo = Code.Utilities2.obtenerCorreoUsuario(solicitante_obj.Cedula);
                        }
                        else
                        {
                            solicitante_correo = Code.Utilities2.obtenerCorreoUsuario(solicitante_obj.Cedula);
                        }
                    }
                    string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/RegistroSolicitud?id={asignado.SolicitudId}";

                    EnviarCorreo(solicitante_correo, asignado.DomainUserTecnico, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + supervisor.dependencia, DescripcionCorreo("REASIGNACIÓN DE SOLICITUD", supervisor.departamento.ToUpper(), supervisor.nombre.ToUpper(), actividad.Descripcion, url, asignado.DescripcionSolicitud));


                }
            }

            // reg.Mostrar_Registro_Solicitud(asignado.SolicitudId);
            return Json(new { success = true, Mensaje = "Solicitud Asignada Correctamente" });
        }




        // GET: SolicitudesServicios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sol_Registro_Solicitud sol_Registro_Solicitud = db.Sol_Registro_Solicitud.Find(id);
            if (sol_Registro_Solicitud == null)
            {
                return HttpNotFound();
            }
            return View(sol_Registro_Solicitud);
        }


    



        // POST: SolicitudesServicios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Solicitudes(Sol_Registro_Solicitud sol_Registro_Solicitud)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sol_Registro_Solicitud).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sol_Registro_Solicitud);
        }

        // GET: SolicitudesServicios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sol_Registro_Solicitud sol_Registro_Solicitud = db.Sol_Registro_Solicitud.Find(id);
            if (sol_Registro_Solicitud == null)
            {
                return HttpNotFound();
            }
            return View(sol_Registro_Solicitud);
        }

        // POST: SolicitudesServicios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sol_Registro_Solicitud sol_Registro_Solicitud = db.Sol_Registro_Solicitud.Find(id);
            db.Sol_Registro_Solicitud.Remove(sol_Registro_Solicitud);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
