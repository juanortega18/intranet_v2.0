﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.Cumpleanos;

namespace intra.Controllers
{
    public class Vw_Mostrar_Empleados_CumpleanosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: Vw_Mostrar_Empleados_Cumpleanos
        public ActionResult Index(FormCollection vc)
        {


            string EmpId = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                {
                    EmpId = HttpContext.Session["empleadoId"].ToString();
                }
                else
                {
                    return Redirect("~/login/Index");
                }
            }catch(Exception)
            {
                return Redirect("~/login/Index");
            }

            int dia = DateTime.Now.Day;
            int mes = DateTime.Now.Month;

            var Dependencia = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == EmpId).Select(x => x.dependencia).FirstOrDefault();
           

            var CumpleNow = (from c in db.Vw_Mostrar_Empleados_Cumpleanos
                             where c.Dia == dia
                             where c.Mes == mes

                             where c.Dependencia == Dependencia
                             select new
                             {
                                 nombre = c.NombreCompleto,
                                 departamento = c.Departamento,
                                 dependencia = c.Dependencia,
                                 codigo = c.Codigo

                             }).ToList().Select(x => new Vw_Mostrar_Empleados_Cumpleanos {
                                 NombreCompleto = x.nombre,
                                 Dependencia = x.dependencia,
                                 Departamento = x.departamento,
                                 Codigo = x.codigo
                                 
                             }).ToList();
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso a Modulo de Cumpleaños");
            return View(CumpleNow);
        }

        [CustomAuthorize]
        public PartialViewResult vMisFelicitaciones()
        {
         
            
            string EmpId = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                    EmpId = HttpContext.Session["empleadoId"].ToString();
                else
                    EmpId = "Usuario de acceso público";
            }
            catch(Exception)
            {
                EmpId = "Usuario de acceso público";
            }

            
         

            int dia = DateTime.Now.Day;
            int mes = DateTime.Now.Month;
         

            var Dependencia = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == EmpId).Select(x => x.dependencia).FirstOrDefault();
           


            var Cumpleanos = (from c in db.Vw_Mostrar_Empleados_Cumpleanos
                             where c.Codigo == EmpId
                             where c.Mes == mes
                
                             select new
                             {
                                 nombre = c.NombreCompleto,
                                 departamento = c.Departamento,
                                 dependencia = c.Dependencia,
                                 codigo = c.Codigo

                             }).ToList().Select(x => new Vw_Mostrar_Empleados_Cumpleanos
                             {
                                 NombreCompleto = x.nombre,
                                 Dependencia = x.dependencia,
                                 Departamento = x.departamento,
                                 Codigo = x.codigo

                             }).ToList();

            string empId;

            try
            {
                if(string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                    empId = HttpContext.Session["empleadoId"].ToString();
                else
                    empId = "Usuario de acceso público";
            }
            catch(Exception)
            {
                empId = "Usuario de acceso público";
            }
            
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso a Modulo de Cumpleaños a ver sus Felicitaciones");

            return PartialView(Cumpleanos);

        }

        [CustomAuthorize]
        public PartialViewResult vCumpleanos()
        {
            string EmpId = string.Empty;

            EmpId = HttpContext.Session["empleadoId"].ToString();

            int dia = DateTime.Now.Day;
            int mes = DateTime.Now.Month;

            var Dependencia = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == EmpId).Select(x => x.dependencia).FirstOrDefault();


            var CumpleNow = (from c in db.Vw_Mostrar_Empleados_Cumpleanos
                             where c.Dia == dia
                             where c.Mes == mes
                             where c.Dependencia == Dependencia
                             select new
                             {
                                 nombre = c.NombreCompleto,
                                 departamento = c.Departamento,
                                 dependencia = c.Dependencia,
                                 codigo = c.Codigo

                             }).ToList().Select(x => new Vw_Mostrar_Empleados_Cumpleanos
                             {
                                 NombreCompleto = x.nombre,
                                 Dependencia = x.dependencia,
                                 Departamento = x.departamento,
                                 Codigo = x.codigo

                             }).ToList();


             if(CumpleNow == null || CumpleNow.Count == 0)
            {
                ViewBag.NohayCumpleaños = "No";
            }

           

            return PartialView(CumpleNow);
        }


        [CustomAuthorize]
        public ActionResult Felicitar(string id)
        {
            
            var EmpFelicitar = db.Vw_Mostrar_Empleados_Cumpleanos.Where(x => x.Codigo == id).FirstOrDefault();


            ViewBag.Felicitaciones = (from f in db.FelicitacionesCumpleanos
                                      from v in db.VISTA_EMPLEADOS
                                      where f.CodigoEmpleado.ToString() == v.empleadoid
                                      where f.CodigoFestejado.ToString() == id
                                      select new
                                      {
                                          Nombre = v.nombre,
                                          Mensaje = f.Mensaje,
                                          Fecha = f.Fecha

                                      }).ToList().Select(x => new MostraFelicitaciones
                                      {

                                          NombreEmpleado = x.Nombre,
                                          Mensaje = x.Mensaje,
                                          Fecha = x.Fecha
                                      }).ToList();


            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso a Modulo de Felicitaciones de Cumpleaños");

            return View(EmpFelicitar);
        }

        [CustomAuthorize]
        public ActionResult EnviarFelicitar(FormCollection fc)
        {
            FelicitacionesCumpleanos fcumple = new FelicitacionesCumpleanos();

           

            int EmpId = Convert.ToInt16(HttpContext.Session["empleadoId"]);
            string EmpFestejado = fc["Codigo"].ToString();

            if (string.IsNullOrEmpty(fc["TxtFelicitaciones"])) {
                ViewBag.Error = "Debe Escribir Un Mensaje en el cuadro Indicado";

                return Redirect("Felicitar?id=" + EmpFestejado + " ");
            }

            fcumple.CodigoEmpleado = EmpId;
            fcumple.CodigoFestejado = Convert.ToInt32(fc["Codigo"]);
            fcumple.Mensaje = fc["TxtFelicitaciones"];
            fcumple.Fecha = DateTime.Now;

            if (ModelState.IsValid) {
                db.FelicitacionesCumpleanos.Add(fcumple);
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Ingreso a Modulo de Felicitaciones, Felicito al Empleado id ("+fcumple.CodigoFestejado+")");
                return Redirect("Felicitar?id="+EmpFestejado+" ");
            }
            
             
             ViewBag.ErrorComment = "Error, No se Pudo Enviar su Comentario de Felicitacion";

            return Redirect("Felicitar?id=" + EmpFestejado + " ");
            
        }

        [CustomAuthorize]
        // GET: Vw_Mostrar_Empleados_Cumpleanos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vw_Mostrar_Empleados_Cumpleanos vw_Mostrar_Empleados_Cumpleanos = db.Vw_Mostrar_Empleados_Cumpleanos.Find(id);
            if (vw_Mostrar_Empleados_Cumpleanos == null)
            {
                return HttpNotFound();
            }
            return View(vw_Mostrar_Empleados_Cumpleanos);
        }

        [CustomAuthorize]
        // GET: Vw_Mostrar_Empleados_Cumpleanos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vw_Mostrar_Empleados_Cumpleanos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Codigo,NombreCompleto,CodigoDependencia,Dependencia,CodigoDepartamento,Departamento,FechaNacimiento,Dia,Mes")] Vw_Mostrar_Empleados_Cumpleanos vw_Mostrar_Empleados_Cumpleanos)
        {
            if (ModelState.IsValid)
            {
                db.Vw_Mostrar_Empleados_Cumpleanos.Add(vw_Mostrar_Empleados_Cumpleanos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vw_Mostrar_Empleados_Cumpleanos);
        }

        [CustomAuthorize]
        // GET: Vw_Mostrar_Empleados_Cumpleanos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vw_Mostrar_Empleados_Cumpleanos vw_Mostrar_Empleados_Cumpleanos = db.Vw_Mostrar_Empleados_Cumpleanos.Find(id);
            if (vw_Mostrar_Empleados_Cumpleanos == null)
            {
                return HttpNotFound();
            }
            return View(vw_Mostrar_Empleados_Cumpleanos);
        }

        // POST: Vw_Mostrar_Empleados_Cumpleanos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Codigo,NombreCompleto,CodigoDependencia,Dependencia,CodigoDepartamento,Departamento,FechaNacimiento,Dia,Mes")] Vw_Mostrar_Empleados_Cumpleanos vw_Mostrar_Empleados_Cumpleanos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vw_Mostrar_Empleados_Cumpleanos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vw_Mostrar_Empleados_Cumpleanos);
        }

        [CustomAuthorize]
        // GET: Vw_Mostrar_Empleados_Cumpleanos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vw_Mostrar_Empleados_Cumpleanos vw_Mostrar_Empleados_Cumpleanos = db.Vw_Mostrar_Empleados_Cumpleanos.Find(id);
            if (vw_Mostrar_Empleados_Cumpleanos == null)
            {
                return HttpNotFound();
            }
            return View(vw_Mostrar_Empleados_Cumpleanos);
        }

        // POST: Vw_Mostrar_Empleados_Cumpleanos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vw_Mostrar_Empleados_Cumpleanos vw_Mostrar_Empleados_Cumpleanos = db.Vw_Mostrar_Empleados_Cumpleanos.Find(id);
            db.Vw_Mostrar_Empleados_Cumpleanos.Remove(vw_Mostrar_Empleados_Cumpleanos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
