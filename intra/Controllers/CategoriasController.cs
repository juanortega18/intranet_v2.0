﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.RolesUsuarios;

namespace intra.Controllers
{
    public class CategoriasController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: Categorias
        [CustomAuthorize]
        [Authorize(Roles="RUA,RUWR")]
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Mantenimiento Categorias");
            return View(db.Categorias.ToList());
        }

        // GET: Categorias/Details/5
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categorias categorias = db.Categorias.Find(id);
            if (categorias == null)
            {
                return HttpNotFound();
            }
            return View(categorias);
        }

        // GET: Categorias/Create
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Create()
        {
            ViewBag.Sistemas = new SelectList(db.Sistemas.ToList(), "SistemaId", "SistemaNombre");
            return View();
        }

        // POST: Categorias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Create([Bind(Include = "CategoriaId,CategoriaNombre,CategoriaDescripcion,SistemaId")] Categorias categorias)
        {
            if (ModelState.IsValid)
            {
                db.Categorias.Add(categorias);
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
               RegisterLogs rl = new RegisterLogs(empId, "Creao una Categoria llamada("+categorias.CategoriaNombre+")");
                return RedirectToAction("Index");
            }

            return View(categorias);
        }

        // GET: Categorias/Edit/5
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categorias categorias = db.Categorias.Find(id);
            if (categorias == null)
            {
                return HttpNotFound();
            }

            ViewBag.Sistemas = new SelectList(db.Sistemas.ToList(), "SistemaId", "SistemaNombre");
            return View(categorias);
        }

        // POST: Categorias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA,RUWR")]
        public ActionResult Edit([Bind(Include = "CategoriaId,CategoriaNombre,CategoriaDescripcion,SistemaId")] Categorias categorias)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categorias).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito la Categoria id(" + categorias.CategoriaId + ")");
                return RedirectToAction("Index");
            }

            ViewBag.Sistemas = new SelectList(db.Sistemas.ToList(), "SistemaId", "SistemaNombre");
            return View(categorias);
        }

        // GET: Categorias/Delete/5
        [CustomAuthorize]
        [Authorize(Roles = "RUA")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categorias categorias = db.Categorias.Find(id);
            if (categorias == null)
            {
                return HttpNotFound();
            }
            return View(categorias);
        }

        // POST: Categorias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RUA")]
        public ActionResult DeleteConfirmed(int id)
        {
            Categorias categorias = db.Categorias.Find(id);
            db.Categorias.Remove(categorias);
            db.SaveChanges();
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Elimino la Categoria(" + categorias.CategoriaNombre + ")");
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
