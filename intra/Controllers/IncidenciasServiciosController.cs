﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;

namespace intra.Controllers
{
    public class IncidenciasServiciosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: IncidenciasServicios
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo de IncidenciasServicios ");
            return View(db.VistaIncidencias.ToList());
            //return View(db.IncidenciasServicios.ToList());
        }
        [HttpGet]
        public JsonResult GetSuplidor(int id)
        {


            List<SelectListItem> Suplidor = new List<SelectListItem>();
            if (id < 1)
            {

                RedirectToAction("Create");
                
            }

            var SuplidorTipos = (from s in db.Suplidores
                                 join st in db.SuplidorTipoes on s.SuplidorId equals st.SuplidorId
                                 join it in db.IncidenciasTipos on st.IncidenciasTipoId equals it.IncidenciasTipoId
                                 where s.SuplidorId == id
                                 select new
                                 {
                                     idIncidencia = it.IncidenciasTipoId,
                                     IncidenciaName = it.IncidenciasTipoNombre

                                 }).ToList().Select(x => new IncidenciasTipos
                                 {
                                     IncidenciasTipoId = x.idIncidencia,
                                     IncidenciasTipoNombre = x.IncidenciaName

                                 }).ToList();

            foreach (var suplidor in SuplidorTipos)
            {
                Suplidor.Add(new SelectListItem { Text = suplidor.IncidenciasTipoNombre.ToString(), Value = suplidor.IncidenciasTipoId.ToString() });
            }

            return Json(new SelectList(Suplidor, "value", "Text"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProvincia(int id)
        {


            List<SelectListItem> listProvincia = new List<SelectListItem>();
            if (id < 1)
            {

                RedirectToAction("Create");

            }

            var Provinces = (from r in db.Provinces
                             from i in db.Region
                             from rp in db.RegionProvincia
                             where i.RegionId == rp.RegionId
                             where r.Province_ID == rp.Province_ID
                             where i.RegionId == id
                             orderby r.Province_Name ascending
                             select new
                             {
                                 r.Province_Name,
                                 r.Province_ID

                             }).ToList();

            foreach (var Province in Provinces)
            {
                listProvincia.Add(new SelectListItem { Text = Province.Province_Name.ToString(), Value = Province.Province_ID.ToString() });

            }

            return Json(new SelectList(listProvincia, "value", "Text"), JsonRequestBehavior.AllowGet);
        }

        //Get Municipio
        [HttpPost]
        public JsonResult Municipio(string Provincia)
        {
            List<SelectListItem> Municipios;

            int cl = int.Parse(Provincia);
            Municipios = db.Municipalities.Where(x => x.Province_ID == cl).Select(x => new SelectListItem() { Text = x.Municipality_Name, Value = x.Municipality_ID.ToString() }).ToList();


            return Json(new SelectList(Municipios, "Value", "Text"));
        }

        [CustomAuthorize]
        public ActionResult Localidad(string Municipio)
        {
            var muni = int.Parse(Municipio);
            var Localidad = new List<SelectListItem>();
            var localidad = db.Localidad.Where(x => x.Municipality_ID == muni).Select(x => new SelectListItem() { Text = x.LocalidadNombre, Value = x.LocalidadId.ToString() }).ToList();
            return Json(new SelectList(localidad, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        // GET: IncidenciasServicios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncidenciasServicios incidenciasServicios = db.IncidenciasServicios.Find(id);
            if (incidenciasServicios == null)
            {
                return HttpNotFound();
            }
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al detalle de un registro en el Modulo de incidenciasServicios id(" + incidenciasServicios.IncidenciasId + ")");
            return View(incidenciasServicios);
        }

        [CustomAuthorize]
        // GET: IncidenciasServicios/Create
        public ActionResult Create()
        {
            
            ViewBag.Suplidor = new SelectList(db.Suplidores.ToList(), "SuplidorId", "SuplidorNombre");
            ViewBag.IncidenciasTipos = new SelectList(db.IncidenciasTipos.ToList(), "IncidenciasTipoId", "IncidenciasTipoNombre");
            ViewBag.IncidenciasAreas = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Estado = new SelectList(db.EstadosIncidencias.ToList(), "EstadoId", "EstadoDescripcion");
            ViewBag.Servicio = new SelectList(db.ConectividadInventarios.ToList(), "ConectividadId", "ConectividadServicio");
            ViewBag.Medio = new SelectList(db.MediosInventario.ToList(), "MedioId", "MedioDescripcion");
            ViewBag.Velocidad = new SelectList(db.VelocidadInventario.ToList(), "VelocidadId", "VelocidadDescripcion");

         

            return View();
        }

        // POST: IncidenciasServicios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IncidenciasId,IncidenciasFechaCreacion,IncidenciasUsuario,IncidenciasSolucion,IncidenciasFechaSolucion,IncidenciasEstatus,SuplidorId,IncidenciasTipoId,LocalidadId,IncidenciasObservaciones,IncidenciasNoReporte,IncidenciasTecnicoNoReporte,IncidenciasTiempoCompromiso,Municipality_ID,RegionId,Province_ID,EstadoId,ConectividadId,MedioId")] IncidenciasServicios incidenciasServicios)
        {
            incidenciasServicios.IncidenciasUsuario = Session["usuario"].ToString();
            incidenciasServicios.IncidenciasFechaCreacion = DateTime.Now;

            if (ModelState.IsValid)
            {
                db.IncidenciasServicios.Add(incidenciasServicios);
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo un registro en el Modulo de incidenciasServicios id(" + incidenciasServicios.IncidenciasId + ")");
                return RedirectToAction("Index");
            }

            var errors = ModelState.Values.SelectMany(v => v.Errors);

            ViewBag.Suplidor = new SelectList(db.Suplidores.ToList(), "SuplidorId", "SuplidorNombre");
            ViewBag.IncidenciasTipos = new SelectList(db.IncidenciasTipos.ToList(), "IncidenciasTipoId", "IncidenciasTipoNombre");
            ViewBag.IncidenciasAreas = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Estado = new SelectList(db.EstadosIncidencias.ToList(), "EstadoId", "EstadoDescripcion");
            ViewBag.Servicio = new SelectList(db.ConectividadInventarios.ToList(), "ConectividadId", "ConectividadServicio");
            ViewBag.Medio = new SelectList(db.MediosInventario.ToList(), "MedioId", "MedioDescripcion");
            ViewBag.Velocidad = new SelectList(db.VelocidadInventario.ToList(), "VelocidadId", "VelocidadDescripcion");
            return View(incidenciasServicios);
        }

        [CustomAuthorize]
        // GET: IncidenciasServicios/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.Suplidor = new SelectList(db.Suplidores.ToList(), "SuplidorId", "SuplidorNombre");
            ViewBag.IncidenciasTipos = new SelectList(db.IncidenciasTipos.ToList(), "IncidenciasTipoId", "IncidenciasTipoNombre");
            ViewBag.IncidenciasAreas = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Estado = new SelectList(db.EstadosIncidencias.ToList(), "EstadoId", "EstadoDescripcion");
            ViewBag.Servicio = new SelectList(db.ConectividadInventarios.ToList(), "ConectividadId", "ConectividadServicio");
            ViewBag.Medio = new SelectList(db.MediosInventario.ToList(), "MedioId", "MedioDescripcion");
            ViewBag.Velocidad = new SelectList(db.VelocidadInventario.ToList(), "VelocidadId", "VelocidadDescripcion");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncidenciasServicios incidenciasServicios = db.IncidenciasServicios.Find(id);
            if (incidenciasServicios == null)
            {
                return HttpNotFound();
            }
            return View(incidenciasServicios);
        }

        // POST: IncidenciasServicios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IncidenciasId,IncidenciasFechaCreacion,IncidenciasUsuario,IncidenciasSolucion,IncidenciasFechaSolucion,IncidenciasEstatus,SuplidorId,IncidenciasTipoId,IncidenciasObservaciones,LocalidadId,IncidenciasNoReporte,IncidenciasTecnicoNoReporte,IncidenciasTiempoCompromiso,Municipality_ID,RegionId,Province_ID,EstadoId,ConectividadId,MedioId")] IncidenciasServicios incidenciasServicios)
        {
            if (ModelState.IsValid)
            {
                db.Entry(incidenciasServicios).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito un registro en el Modulo de incidenciasServicios id(" + incidenciasServicios.IncidenciasId + ")");
                return RedirectToAction("Index");

            }
            ViewBag.Suplidor = new SelectList(db.Suplidores.ToList(), "SuplidorId", "SuplidorNombre");
            ViewBag.IncidenciasTipos = new SelectList(db.IncidenciasTipos.ToList(), "IncidenciasTipoId", "IncidenciasTipoNombre");
            ViewBag.IncidenciasAreas = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Estado = new SelectList(db.EstadosIncidencias.ToList(), "EstadoId", "EstadoDescripcion");
            ViewBag.Servicio = new SelectList(db.ConectividadInventarios.ToList(), "ConectividadId", "ConectividadServicio");
            ViewBag.Medio = new SelectList(db.MediosInventario.ToList(), "MedioId", "MedioDescripcion");
            ViewBag.Velocidad = new SelectList(db.VelocidadInventario.ToList(), "VelocidadId", "VelocidadDescripcion");
            return View(incidenciasServicios);
        }

        [CustomAuthorize]
        // GET: IncidenciasServicios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncidenciasServicios incidenciasServicios = db.IncidenciasServicios.Find(id);
            if (incidenciasServicios == null)
            {
                return HttpNotFound();
            }
            return View(incidenciasServicios);
        }

        // POST: IncidenciasServicios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IncidenciasServicios incidenciasServicios = db.IncidenciasServicios.Find(id);
            db.IncidenciasServicios.Remove(incidenciasServicios);
            db.SaveChanges();
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Elimino un registro en el Modulo de incidenciasServicios id(" + incidenciasServicios.IncidenciasId + ")");
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
