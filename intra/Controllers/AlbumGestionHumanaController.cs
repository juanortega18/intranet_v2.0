﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using intra.Models;
using intra.Models.GestionHumana;
using System.IO;

namespace intra.Controllers
{
    public class AlbumGestionHumanaController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: AlbumGestionHumana
        public ActionResult Index()
        {
            var albums = db.AlbumGestionHumana.Where(x => x.AlbumEstado).OrderByDescending(x => x.AlbumGHId).ToList();

            var _albumsId = albums.Select(albumsId => albumsId.AlbumGHId).ToList();

            var _fotos = (from fotos in db.FotoGestionHumana
                          where fotos.FotoEstado
                          select new
                          {
                              fotos.FotoGHId,
                              fotos.FotoGHNombre,
                              fotos.FotoGHExtension,
                              fotos.FotoGHFhSubida,
                              fotos.FotoGHAncho,
                              fotos.FotoGHAlto,
                              fotos.AlbumGHId,
                              fotos.FotoGHDescripcion,
                              fotos.FotoGHFhCreacion,
                              fotos.FotoGHUsuarioCreador,
                              fotos.FotoEstado
                          }).ToList().Select(obj => new FotoGestionHumana
                          {
                              FotoGHId = obj.FotoGHId,
                              FotoGHNombre = obj.FotoGHNombre,
                              FotoGHExtension = obj.FotoGHExtension,
                              FotoGHFhSubida = obj.FotoGHFhSubida,
                              FotoGHAncho = obj.FotoGHAncho,
                              FotoGHAlto = obj.FotoGHAlto,
                              AlbumGHId = obj.AlbumGHId,
                              FotoGHDescripcion = obj.FotoGHDescripcion,
                              FotoGHFhCreacion = obj.FotoGHFhCreacion,
                              FotoGHUsuarioCreador = obj.FotoGHUsuarioCreador,
                              FotoEstado = obj.FotoEstado
                          });
            
                albums.ForEach(x => x.FotoGestionHumana = _fotos.Where(y => y.AlbumGHId == x.AlbumGHId));

            return View(albums);
        }

        public ActionResult GestionHumana()
        {


            return View();
        }

        // GET: AlbumGestionHumana/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AlbumGestionHumana/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult ImagenGestionHumana(int AlbumId)
        {
            var photos = db.FotoGestionHumana.Where(x => x.AlbumGHId == AlbumId && x.FotoEstado == true).ToList();
            var album = db.AlbumGestionHumana.Find(AlbumId);

            ViewBag.NombreAlbum = album.AlbumGHNombre;
            ViewBag.IdAlbum = album.AlbumGHId;

            return View(photos);
        }


        // POST: AlbumGestionHumana/Create
        [HttpPost]
        public ActionResult Create(AlbumGestionHumana view_object, FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
              
                if (ModelState.IsValid)
                    view_object.AlbumGHFhCreacion = DateTime.Now;
                {
                    view_object.AlbumGHUsuarioCreador = Convert.ToInt32(Session["empleadoId"]);

                    List<FotoGestionHumana> fotos_commit = Utilities.UploadPhotoGH(view_object.FotoVista);

                    if (collection["fotoDescripcion"] != null)
                    {
                        string descripcion = collection["fotoDescripcion"].ToString();
                        string[] arrayDescripcion = descripcion.Split(',');
                        int i = 0;
                        view_object.AlbumEstado = true;
                        db.AlbumGestionHumana.Add(view_object);
                        db.SaveChanges();
                        var almbumnID = db.AlbumGestionHumana.ToList().LastOrDefault().AlbumGHId;

                        FotoGestionHumana fotos = new FotoGestionHumana();
                        foreach (var item in fotos_commit)
                        {
                        
                            fotos.FotoEstado = true;
                            fotos.FotoGHAlto = item.FotoGHAlto;
                            fotos.FotoGHAncho = item.FotoGHAncho;
                            fotos.FotoGHArchivo = item.FotoGHArchivo;
                            fotos.FotoGHDescripcion = item.FotoGHDescripcion;
                            fotos.FotoGHExtension = item.FotoGHExtension;
                            fotos.FotoGHFhCreacion = DateTime.Now;
                            fotos.FotoGHFhSubida = item.FotoGHFhSubida;
                            fotos.FotoGHNombre = item.FotoGHNombre;
                            fotos.FotoGHUsuarioCreador = Convert.ToInt32(Session["empleadoId"]);
                            fotos.AlbumGHId = almbumnID;

                            var pic_name = Path.GetFileName(item.FotoGHNombre);
                            var path = Path.Combine(HttpRuntime.AppDomainAppPath + "\\Content\\images\\GH_IMAGES\\AlbumFotos", pic_name);
                            view_object.FotoVista[i].SaveAs(path);
                            i++;
                            db.FotoGestionHumana.Add(fotos);
                            db.SaveChanges();

                        }
                    }

                    //if (fotos_commit != null && fotos_commit.Count > 0)
                    //{
                    //    //view_object.FotoGestionHumana = fotos_commit;
                    //    //db.SaveChanges();

                    //    //view_object.AlbumEstado = true;
                    //    //db.AlbumGestionHumana.Add(view_object);
                    //    //db.SaveChanges();

                    //}
                }


                return RedirectToAction("Index", "AlbumGestionHumana");
            }
            catch (Exception ex)
            {
                ex.ToString();
                return View();
            }
        }

        // GET: AlbumGestionHumana/Edit/5
        public ActionResult Edit(int id)
        {
            var album = db.AlbumGestionHumana.Find(id);

            if (album == null)
            {
                return HttpNotFound();
            }

            var nombre_usuario_creador = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == album.AlbumGHUsuarioCreador.Value.ToString()).FirstOrDefault().NombreCompleto;

            ViewBag.NombreUsuario = nombre_usuario_creador;

            return View(album);
        }

        // POST: AlbumGestionHumana/Edit/5
        [HttpPost]
        public ActionResult Edit(AlbumGestionHumana view_object)
        {
            try
            {
                // TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    db.Entry(view_object).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }

                return RedirectToAction("GestionHumana", "AlbumGestionHumana");
            }
            catch
            {
                TempData["MsjError"] = "No se pudo actualizar el álbum correctamente";
                return View();
            }
        }

        // GET: AlbumGestionHumana/Delete/5
        public JsonResult EliminarAlbum(int? id)
        {
            try
            {
                var album = db.AlbumGestionHumana.Find(id);
                album.AlbumEstado = false;
                db.SaveChanges();

                return Json(new { success = true, Mensaje = "Eliminado", JsonRequestBehavior.AllowGet });
            }
            catch (Exception ex)
            {
                ex.ToString();

                return Json(new { success = false, Mensaje = "Ha ocurrido un error favor verificar", JsonRequestBehavior.AllowGet });
            }
        }
    }
}
