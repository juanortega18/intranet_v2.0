﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Code;
using intra.Models.ViewModel;
using System.Configuration;
using System.IO;
using System.Net.Sockets;
using intra.Models;
using intra.Models.Servicios;
using Rotativa;
using intra.Models.PaseProduccion;
using intra.Models.FlotasExtensiones;
using intra.Models.GestionHumana;



namespace intra.Controllers
{
    public class SolicitudesServiciosController : Controller
    {
        public static string Direccion = ConfigurationManager.AppSettings["URLINTRA"];
        private dbIntranet db = new dbIntranet();
        Correo correo = new Correo();
        intra.Code.Utilities2 domain = new intra.Code.Utilities2();

        #region ObtenerEtiquetaHTML
        public string DescripcionCorreo(string Tema, string Departamento, string NombreCompleto, string Problema, string Link, string descripcion)
        {
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/Solicitud-nuevoServicio.html");

            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {
                Archivo = sr.ReadToEnd();
            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", Tema);
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", descripcion);

            return Archivo;
        }

        #endregion

        #region EnviarCorreo
        public void EnviarCorreo(string Supervisor, string Responsable, string Sujeto, string Descripcion)
        {
            string PrimerCorreo = "";

            string SegundoCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            if (Supervisor != "")
            {
                PrimerCorreo = Code.Utilities2.obtenerSp_UserPorUsuario(Supervisor)?.mail;
            }

            SegundoCorreo = Code.Utilities2.obtenerSp_UserPorUsuario(Responsable)?.mail;

            //ObjCorreo.EnviarCorreo(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion);

            correo.EnviarCorreo_(new string[] { PrimerCorreo, SegundoCorreo }, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
        }
        #endregion

        //public string obtenerCorreoUsuario(string codigoEmpleado)
        //{
        //    var cedulaEmpleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == codigoEmpleado).FirstOrDefault();
        //    var user = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", cedulaEmpleado.cedula)).FirstOrDefault();
        //    string correo = $"{user.samAccountName}@pgr.gob.do";

        //    return correo;
        //}

        //public string obtenerCorreoPorUsuario(string usuario)
        //{
        //    var user = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldap '{0}'", usuario)).FirstOrDefault();
        //    string correo = $"{user.mail}";

        //    return correo;
        //}

        #region SendCorreo
        private void SendCorreo(string Sujeto, string CorreoSolicitante, string Descripcion)
        {

            string codigoEmpleado = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoDTI" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoDTI = Code.Utilities2.obtenerSp_UserPorCodigo(codigoEmpleado)?.mail;

            string codigoEmpleadoEncargadoDesarrollo = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoDesarrollo" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoDesarrollo = Code.Utilities2.obtenerSp_UserPorCodigo(codigoEmpleadoEncargadoDesarrollo)?.mail;

            string codigoEmpleadoEncargadoProyecto = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoProyecto" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoProyecto = Code.Utilities2.obtenerSp_UserPorCodigo(codigoEmpleadoEncargadoProyecto)?.mail;

            //string lider = Session["usuario"].ToString();
            //string correoLider = lider + "@pgr.gob.do";
            //Lider
            //string codigoEmpleadoLiderEquipo = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "Lider" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            //string correoLiderEquipo = obtenerCorreoUsuario(codigoEmpleadoLiderEquipo);

            string UsuarioResponsable = "";

            //   string PrimerCorreo = "";

            string SegundoCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            //CorreoSupervisor = Supervisor;

            // PrimerCorreo = Supervisor + "@PGR.GOB.DO";

            // UsuarioResponsable = Responsable + "@PGR.GOB.DO";

            SegundoCorreo = UsuarioResponsable;// + "@PGR.GOB.DO";

            // objCorreo.CorreoPaseProduccion(SegundoCorreo, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            correo.CorreoPaseProduccion(correoEncargadoDesarrollo, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            correo.CorreoPaseProduccion(correoEncargadoDTI, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            correo.CorreoPaseProduccion(correoEncargadoProyecto, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            correo.CorreoPaseProduccion(CorreoSolicitante, Sujeto, Descripcion, Logo, Flecha, LogoDTI);

        }
        #endregion

        #region SendCorreoSeguridaDTI
        private void SendCorreoSeguridaDTI(string Sujeto, string CorreoSolicitante, string Descripcion)
        {

            string codigoEmpleado = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoDTI" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoDTI = Code.Utilities2.obtenerSp_UserPorCodigo(codigoEmpleado)?.mail;

            string codigoEmpleadoEncargadoOperaciones = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoOperaciones" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoOperaciones = Code.Utilities2.obtenerSp_UserPorCodigo(codigoEmpleadoEncargadoOperaciones)?.mail;

            string codigoEmpleadoEncargadoSeguridadDTI = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoSeguridadTI" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoSeguridadDTI = Code.Utilities2.obtenerSp_UserPorCodigo(codigoEmpleadoEncargadoSeguridadDTI)?.mail;

            //string lider = Session["usuario"].ToString();
            //string correoLider = lider + "@pgr.gob.do";
            //Lider
            //string codigoEmpleadoLiderEquipo = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "Lider" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            //string correoLiderEquipo = obtenerCorreoUsuario(codigoEmpleadoLiderEquipo);

            string UsuarioResponsable = "";

            //   string PrimerCorreo = "";

            string SegundoCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            //CorreoSupervisor = Supervisor;

            // PrimerCorreo = Supervisor + "@PGR.GOB.DO";

            // UsuarioResponsable = Responsable + "@PGR.GOB.DO";

            SegundoCorreo = UsuarioResponsable;// + "@PGR.GOB.DO";

            // objCorreo.CorreoPaseProduccion(SegundoCorreo, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            correo.CorreoPaseProduccion(correoEncargadoOperaciones, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            correo.CorreoPaseProduccion(correoEncargadoDTI, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            correo.CorreoPaseProduccion(correoEncargadoSeguridadDTI, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            correo.CorreoPaseProduccion(CorreoSolicitante, Sujeto, Descripcion, Logo, Flecha, LogoDTI);

        }
        #endregion


        #region ObtenerEtiquetaHTML
        public string SolicitudAprobacionPaseProduccion(string Departamento, string NombreCompleto, string Problema, string Link, string Descripcion)
        {


            //string NombreArchivo = Server.MapPath (@"~/EtiquetasHtml/Solicitud_nuevoServicio_PGR.html");
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/SolicitudPaseProduccion.html");


            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", "SOLICITUD PASE PRODUCCION");
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", Descripcion);



            return Archivo;
        }

        #endregion


        #region ObtenerEtiquetaHTML
        public string SolicitudAprobacionPaseProduccionSeguridadDTI(string Departamento, string NombreCompleto, string Problema, string Link, string Descripcion)
        {


            //string NombreArchivo = Server.MapPath (@"~/EtiquetasHtml/Solicitud_nuevoServicio_PGR.html");
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/SolicitudPaseProduccion.html");


            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", "SOLICITUD CONTROL DE CAMBIOS");
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", Descripcion);



            return Archivo;
        }

        #endregion

        #region ObtenerEtiquetaHTML
        public string DescripcionCorreo(string Departamento, string NombreCompleto, string Problema, string Link, string Descripcion)
        {


            //string NombreArchivo = Server.MapPath (@"~/EtiquetasHtml/Solicitud_nuevoServicio_PGR.html");
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/Solicitud-nuevoServicio.html");


            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", "SERVICIO ASIGNADO");
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", Descripcion);



            return Archivo;
        }

        #endregion


        [AllowAnonymous]
        // [HttpPost]
        public ActionResult ReporteConstanciaViaticos(int? id)
        {

            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }

            // id = 49114;
            var user = db.EmpleadoViaticos.Where(x => x.SolicitudId == id).FirstOrDefault();
            var sol = db.Sol_Registro_Solicitud.Where(x => x.SolicitudId == id).FirstOrDefault();
            var actividad = db.Sol_Actividades.Where(x => x.ActividadId == sol.Tipo_SolicitudId).FirstOrDefault();
            var subActividad = db.Sol_Sub_Actividades.Where(x => x.SubActividadId == sol.Tipo_Sub_SolicitudId).FirstOrDefault();
            var employee = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == sol.SolicitanteId.ToString()).FirstOrDefault();
            ViewBag.SolicitanteId2 = employee.nombre.ToUpper();
            if (user == null)
            {

                TempData["ReporteConstanciaViaticos"] = "Ok";

                return RedirectToAction("ConstanciaViaticos");
            }

            var viaticos = (from a in db.Sol_Registro_Solicitud

                            where a.SolicitudId == id
                            select new vServiciosIntranet
                            {
                                Solicitante = a.DomainUserSolicitante,
                                FechaSolicitud = a.FhCreacion,
                                DescripcionSolicitud = a.DescripcionSolicitud,
                                TipoSolicitud = actividad.Descripcion,
                                SubActividad = subActividad.Descripcion,
                                SolicitudId = a.SolicitudId,


                            }).FirstOrDefault();

            viaticos.EmpleadoViaticos = db.EmpleadoViaticos.Where(x => x.SolicitudId == id).ToList();



            return new ViewAsPdf("ReporteConstanciaViaticos", viaticos)
            {
                PageSize = Rotativa.Options.Size.Letter
            };


        }



        [CustomAuthorize]
        public ActionResult ConstanciaViaticos(int? id)
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }
            var user = db.EmpleadoViaticos.Where(x => x.SolicitudId == id).FirstOrDefault();
            var sol = db.Sol_Registro_Solicitud.Where(x => x.SolicitudId == id).FirstOrDefault();
            var actividad = db.Sol_Actividades.Where(x => x.ActividadId == sol.Tipo_SolicitudId).FirstOrDefault();
            var subActividad = db.Sol_Sub_Actividades.Where(x => x.SubActividadId == sol.Tipo_Sub_SolicitudId).FirstOrDefault();
            var employee = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == sol.SolicitanteId.ToString()).FirstOrDefault();
            ViewBag.SolicitanteId2 = employee.nombre.ToUpper();
            if (user == null)
            {

                TempData["ReporteConstanciaViaticos"] = "Ok";

                return RedirectToAction("Viaticos");
            }

            var viaticos = (from a in db.Sol_Registro_Solicitud

                            where a.SolicitudId == id
                            select new vServiciosIntranet
                            {
                                Solicitante = a.DomainUserSolicitante,
                                FechaSolicitud = a.FhCreacion,
                                DescripcionSolicitud = a.DescripcionSolicitud,
                                TipoSolicitud = actividad.Descripcion,
                                SubActividad = subActividad.Descripcion,
                                SolicitudId = a.SolicitudId,


                            }).FirstOrDefault();

            viaticos.EmpleadoViaticos = db.EmpleadoViaticos.Where(x => x.SolicitudId == id).ToList();

            return View(viaticos);

        }

        [HttpPost]
        public JsonResult ConstanciaViatico(int[] solicitud, string[] monto, string fechaInicio)
        {
            if (monto == null) return Json(new { success = false, Mensaje = "Favor Introduccir un monto" });
            if (solicitud == null) return Json(new { success = false, Mensaje = "Favor Elegir una solicitud valida" });

            for (int i = 0; i < solicitud.Length; i++)
            {

                //var fechaInicio = fc["FechaSolicitud1"];
                // DateTime fecha1 = DateTime.ParseExact(fechaInicio, "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture);
                int viatico = solicitud[i];
                var constancia = db.EmpleadoViaticos.Where(x => x.EmpleadoViaticoId == viatico).FirstOrDefault();
                if (constancia.MontoViaticos != null && constancia.MontoViaticos != 0)
                {
                    constancia.FechaSolicitud = DateTime.Parse(fechaInicio.ToString());
                    db.SaveChanges();
                }
                else { 
                  decimal monto1 = Convert.ToDecimal(monto[i]);
                constancia.MontoViaticos = monto1;
                constancia.FechaSolicitud = DateTime.Parse(fechaInicio.ToString());
                db.SaveChanges();
            }

            }

            return Json(new { success = true, Mensaje = "Datos Creados Correctamente!" });
        }


        [CustomAuthorize]
        public ActionResult SolicitudesViaticos()
        {
            string FechaInicio = "2018-09-25";
            DateTime fhViaticos = DateTime.Parse(FechaInicio);

            var actividad = db.Sol_Actividades.Where(x => x.ActividadId == 2031 || x.ActividadId == 1037).FirstOrDefault();
            var viaticos = (from a in db.Sol_Registro_Solicitud
                            join ss in db.Sol_Sub_Actividades
                            on a.Tipo_Sub_SolicitudId equals ss.SubActividadId
                            where a.Tipo_SolicitudId == 1037 || a.Tipo_SolicitudId == 2031
                            && a.EstadoId == 1
                            select new vServiciosIntranet
                            {
                                Solicitante = a.DomainUserSolicitante,
                                SolicitanteId = a.SolicitanteId,
                                FechaSolicitud = a.FhCreacion,
                                DescripcionSolicitud = a.DescripcionSolicitud,
                                TipoSolicitud = actividad.Descripcion,
                                SubActividad = ss.Descripcion,
                                SolicitudId = a.SolicitudId,

                            }).OrderByDescending(x => x.SolicitudId).Where(x => x.FechaSolicitud >= fhViaticos)
                            .ToList();



            return View(viaticos);

        }



        //Reporte Pase a Produccion

        [AllowAnonymous]
        // [HttpPost]
        public ActionResult ReporteViaticos(int id)
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }

            var sol = db.Sol_Registro_Solicitud.Where(x => x.SolicitudId == id).FirstOrDefault();
            var user = db.EmpleadoViaticos.Where(x => x.SolicitudId == id).FirstOrDefault();
            var actividad = db.Sol_Actividades.Where(x => x.ActividadId == sol.Tipo_SolicitudId).FirstOrDefault();
            var subActividad = db.Sol_Sub_Actividades.Where(x => x.SubActividadId == sol.Tipo_Sub_SolicitudId).FirstOrDefault();
            var employee = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == sol.SolicitanteId.ToString()).FirstOrDefault();
            ViewBag.SolicitanteId2 = employee.nombre.ToUpper();

            if (user == null)
            {
                TempData["ReporteViaticos"] = "Ok";

                return RedirectToAction("SolicitudesViaticos");
            }

            //if (user.EmpleadoId.ToString() == "" || user.EmpleadoId.ToString() == null)
            //{

            //    TempData["Reporte"] = "Ok";

            //    return RedirectToAction("Viaticos");
            //}

            var reporte = (from a in db.Sol_Registro_Solicitud

                           where a.SolicitudId == id
                           select new vServiciosIntranet
                           {
                               Solicitante = a.DomainUserSolicitante,
                               FechaSolicitud = a.FhCreacion,
                               DescripcionSolicitud = a.DescripcionSolicitud,
                               TipoSolicitud = actividad.Descripcion,
                               SubActividad = subActividad.Descripcion,
                               SolicitudId = a.SolicitudId,


                           }).FirstOrDefault();

            reporte.EmpleadoViaticos = db.EmpleadoViaticos.Where(x => x.SolicitudId == id).ToList();



            return new ViewAsPdf("ReporteViaticos", reporte)
            {
                PageSize = Rotativa.Options.Size.Letter
            };


        }



        // GET: SolicitudesServicios
        //[HttpGet]
        [CustomAuthorize]
        public ActionResult ServiciosSolicitados()
        {

            int codigo = int.Parse(Session["empleadoId"].ToString());

            var Solicitado = (from r in db.vw_FormularioSolicitudServicios

                              select new vServiciosIntranet
                              {
                                  SolicitudId = r.CodigoSolicitud,
                                  SolicitanteId = r.CodigoSolicitante,
                                  ResponsableId = r.CodigoTecnico,
                                  Solicitante = r.NombreSolicitante,
                                  Responsable = r.NombreTecnico.ToUpper(),
                                  DependenciaId = r.DependenciaId,
                                  Dependencia = r.DependenciaNombre,
                                  TipoSolicitud = r.TipoSolicitud,
                                  TipoSolicitudId = r.CodigoTipoSolicitud,
                                  DescripcionSolicitud = r.DescripcionSolicitud,
                                  FechaSolicitud = r.FechaCreacion,
                                  Estatus = r.EstadoSolicitud,
                                  EstadoId = r.CodigoEstadoSolicitud,
                                  SubActividad = r.SubTipoSolicitud,
                                  CodigoSubactividad = r.CodigoSubTipoSolicitud,



                              }).Where(x => x.SolicitanteId == codigo && x.Estatus == "Pendiente")
                             .OrderByDescending(x => x.FechaSolicitud).ToList();


            return View(Solicitado);
        }

        [HttpPost]
        public JsonResult ServiciosSolicitados(int? estado, string filtro, string responsable, string FechaInicial, string FechaFinal, FormCollection fc)
        {


            int codigo = int.Parse(Session["empleadoId"].ToString());


            DateTime inicio = DateTime.ParseExact(FechaInicial, "dd/MM/yyyy", null);

            DateTime final = DateTime.ParseExact(FechaFinal, "dd/MM/yyyy", null);

            //Para que la fecha haga el calculo de los minutos y segundos
            DateTime Fechafinal = final.AddDays(1).AddSeconds(-1);

            var Solicitado = (from r in db.vw_FormularioSolicitudServicios

                              select new vServiciosIntranet
                              {
                                  SolicitudId = r.CodigoSolicitud,
                                  SolicitanteId = r.CodigoSolicitante,
                                  ResponsableId = r.CodigoTecnico,
                                  Solicitante = r.NombreSolicitante,
                                  Responsable = r.NombreTecnico.ToUpper(),
                                  DependenciaId = r.DependenciaId,
                                  Dependencia = r.DependenciaNombre,
                                  TipoSolicitud = r.TipoSolicitud,
                                  TipoSolicitudId = r.CodigoTipoSolicitud,
                                  DescripcionSolicitud = r.DescripcionSolicitud,
                                  FechaSolicitud = r.FechaCreacion,
                                  Estatus = r.EstadoSolicitud,
                                  EstadoId = r.CodigoEstadoSolicitud,
                                  SubActividad = r.SubTipoSolicitud,
                                  CodigoSubactividad = r.CodigoSubTipoSolicitud
                              }).Where(x => x.SolicitanteId == codigo).OrderByDescending(x => x.FechaSolicitud).ToList();


            if (estado == 4 && filtro == "")
            {
                return Json(Solicitado);
            }

            else if (estado == 4 && filtro == "2" && responsable != "")
            {
                var Solicitado41 = Solicitado.Where(x => x.Responsable.Contains(responsable.ToUpper())).OrderByDescending(x => x.FechaSolicitud).ToList();

                return Json(Solicitado41);
            }
            else if (filtro == "3" && estado == 4 && Fechafinal.ToString() != "" && FechaInicial != "")
            {
                var Solicitado42 = Solicitado.Where(x => x.SolicitanteId == codigo && x.FechaSolicitud >= inicio &&
                                x.FechaSolicitud <= Fechafinal).OrderByDescending(x => x.FechaSolicitud).ToList();

                return Json(Solicitado42);


            }

            else if (filtro == "3" && estado != 0 && Fechafinal.ToString() != "" && FechaInicial != "")
            {
                var Solicitado1 = Solicitado.Where(x => x.SolicitanteId == codigo && x.EstadoId == estado
                                && x.FechaSolicitud >= inicio &&
                                x.FechaSolicitud <= Fechafinal).OrderByDescending(x => x.FechaSolicitud).ToList();



                return Json(Solicitado1);


            }
            else if (filtro == "2" && responsable != "" && estado != 0)
            {

                var Solicitado2 = Solicitado.Where(x => x.EstadoId == estado
                      && x.Responsable.Contains(responsable.ToUpper())).OrderByDescending(x => x.FechaSolicitud).ToList();

                return Json(Solicitado2);

            }

            else if (estado != 0 && filtro == "" || filtro == null || filtro == "2" || filtro == "3" && responsable == "")
            {
                var Solicitado3 = Solicitado.Where(x => x.SolicitanteId == codigo && x.EstadoId == estado
                                  ).OrderByDescending(x => x.FechaSolicitud).ToList();

                return Json(Solicitado3);

            }

            return Json(Solicitado);

        }


        [CustomAuthorize]
        public ActionResult ServiciosAsignados()
        {

            int codigo = int.Parse(Session["empleadoId"].ToString());
            string dependenciaId = Session["dependenciaId"].ToString();
            string departamentoId = Session["departamentoId"].ToString();
            int Departamento = int.Parse(departamentoId);
            var Asignado = db.Sol_Registro_Solicitud.Where(x => x.TecnicoId == codigo && x.EstadoId == 1).ToList();
            var solicitado = db.Sol_Registro_Solicitud.Where(x => x.SolicitanteId == codigo && x.EstadoId == 1).ToList();

            HttpContext.Session["CantidadAsignados"] = Asignado.Count();
            HttpContext.Session["CantidadSolicitados"] = solicitado.Count();


            var Otrostecnicos = db.Sol_Otros_Tecnicos.Where(x => x.TecnicoId == codigo && x.Estado == true).FirstOrDefault();
            if (Otrostecnicos != null)
            {
                ViewBag.Otrostecnicos = "Otrostecnico";
            }

            var director = db.Sol_Departamento.Where(x => x.DirectorId == codigo && x.Estado == true).FirstOrDefault();
            if (director != null)
            {
                ViewBag.Director = "Director";
            }
            var Supervisor = db.Sol_Actividades.Where(x => x.SupervisorId == codigo && x.Estado == true).FirstOrDefault();
            if (Supervisor != null)
            {
                ViewBag.Supervisor = "Supervisor";
            }

            var TecnicoAlternativo = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo && x.Estado == true).ToList();
            if (TecnicoAlternativo.Count > 0)
            {
                ViewBag.TecnicoAlternativo = "tecnicoAlternativo";
            }


            var asignadoEmpleado = (from a in db.Sol_Mostrar_Personal_PGR
                                    where a.CodigoDependencia == dependenciaId && a.CodigoDepartamento == departamentoId
                                    orderby a.NombreCompleto ascending
                                    select new PersonalPgrModel
                                    {
                                        EmpleadoId = a.Codigo,
                                        Descripcion = a.NombreCompleto,
                                        Cedula = a.Cedula,


                                    }).ToList();
            ViewBag.tecnico = asignadoEmpleado;



            // var Asignados = db.Sol_Detalle_Lista.Where(x=> x.CodigoTecnico == codigo && x.CodigoEstadoSolicitud == 1).ToList();

            //var Asignados = db.Sol_Registro_Solicitud.ToList();

            //  List<int> solicitudIds = db.Sol_Registro_Solicitud.Where(x => x.TecnicoId == codigo && x.EstadoId == 1).Select(x => x.SolicitudId).ToList();

            var Asignados = (from r in db.vw_FormularioSolicitudServicios

                             select new vServiciosIntranet
                             {
                                 SolicitudId = r.CodigoSolicitud,
                                 SolicitanteId = r.CodigoSolicitante,
                                 ResponsableId = r.CodigoTecnico,
                                 Solicitante = r.NombreSolicitante,
                                 Responsable = r.NombreTecnico.ToUpper(),
                                 DependenciaId = r.DependenciaId,
                                 Dependencia = r.DependenciaNombre,
                                 TipoSolicitud = r.TipoSolicitud,
                                 TipoSolicitudId = r.CodigoTipoSolicitud,
                                 DescripcionSolicitud = r.DescripcionSolicitud,
                                 FechaSolicitud = r.FechaCreacion,
                                 Estatus = r.EstadoSolicitud,
                                 EstadoId = r.CodigoEstadoSolicitud,
                                 SubActividad = r.SubTipoSolicitud,
                                 CodigoSubactividad = r.CodigoSubTipoSolicitud

                             }).Where(x => x.ResponsableId == codigo && x.EstadoId == 1).ToList();



            if (director == null && TecnicoAlternativo == null && Supervisor == null)
            {
                var AsignadosUsuariosNormal = Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == 1)
                             .OrderByDescending(x => x.FechaSolicitud).ToList();
                return View(AsignadosUsuariosNormal);

            }
            else if (director != null)
            {
                var AsignadosDirector = Asignados.Where(x => x.DepartamentoId == director.DepartamentoId && x.Estatus == "Pendiente").ToList();

                return View(AsignadosDirector);
            }
            else if (Supervisor != null && TecnicoAlternativo == null)
            {
                var AsignadoSupervisor = Asignados.Where(x => codigo == Supervisor.SupervisorId && x.Estatus == "Pendiente").ToList();

                return View(AsignadoSupervisor);
            }
            else if (TecnicoAlternativo != null && Supervisor == null)
            {
                //revisar aqui

                Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == 1).ToList();


                var AsignadoTecnicoAlternativo = Asignados;


                var actividades = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo).ToList();

                // var depedencias = actividades.Select(x=>x.DependenciaRegionalId).Distinct();


                foreach (var item in actividades)
                {



                    var Sol_actividades = (from r in db.vw_FormularioSolicitudServicios

                                           select new vServiciosIntranet
                                           {
                                               SolicitudId = r.CodigoSolicitud,
                                               SolicitanteId = r.CodigoSolicitante,
                                               ResponsableId = r.CodigoTecnico,
                                               Solicitante = r.NombreSolicitante.ToUpper(),
                                               Responsable = r.NombreTecnico,
                                               DependenciaId = r.DependenciaId,
                                               Dependencia = r.DependenciaNombre,
                                               TipoSolicitud = r.TipoSolicitud,
                                               TipoSolicitudId = r.CodigoTipoSolicitud,
                                               DescripcionSolicitud = r.DescripcionSolicitud,
                                               FechaSolicitud = r.FechaCreacion,
                                               Estatus = r.EstadoSolicitud,
                                               EstadoId = r.CodigoEstadoSolicitud,
                                               SubActividad = r.SubTipoSolicitud,
                                               CodigoSubactividad = r.CodigoSubTipoSolicitud,


                                           }).Where(x => (x.TipoSolicitudId == item.ActividadId || x.DependenciaId == item.DependenciaRegionalId) && x.EstadoId == 1).OrderByDescending(x => x.FechaSolicitud).ToList();

                    foreach (var item2 in Sol_actividades)
                    {

                        if (!AsignadoTecnicoAlternativo.Contains(item2))
                        {
                            AsignadoTecnicoAlternativo.Add(item2);
                        }
                    }
                }

                return View(AsignadoTecnicoAlternativo);

            }



            return View(Asignados);
        }


        [HttpPost]
        public JsonResult ServiciosAsignados(int? estado, string filtro, string responsable, string solicitante, string fechaInicial, string fechaFinal, string TipoSolicitudId, FormCollection fc)
        {


            int codigo = int.Parse(Session["empleadoId"].ToString());


            DateTime inicio = DateTime.ParseExact(fechaInicial, "dd/MM/yyyy", null);

            DateTime final = DateTime.ParseExact(fechaFinal, "dd/MM/yyyy", null);


            //Para que la fecha haga el calculo de los minutos y segundos
            DateTime Fechafinal = final.AddDays(1).AddSeconds(-1);

            var state = db.Sol_Estado_Solicitud.Where(x => x.EstadoId == estado).FirstOrDefault();

            var Otrostecnicos = db.Sol_Otros_Tecnicos.Where(x => x.TecnicoId == codigo && x.Estado == true).FirstOrDefault();

            var director = db.Sol_Departamento.Where(x => x.DirectorId == codigo && x.Estado == true).FirstOrDefault();

            var Supervisor = db.Sol_Actividades.Where(x => x.SupervisorId == codigo && x.Estado == true).FirstOrDefault();

            var Asignados = (from r in db.vw_FormularioSolicitudServicios

                             select new vServiciosIntranet
                             {
                                 SolicitudId = r.CodigoSolicitud,
                                 SolicitanteId = r.CodigoSolicitante,
                                 ResponsableId = r.CodigoTecnico,
                                 Solicitante = r.NombreSolicitante.ToUpper(),
                                 Responsable = r.NombreTecnico,
                                 DependenciaId = r.DependenciaId,
                                 Dependencia = r.DependenciaNombre,
                                 TipoSolicitud = r.TipoSolicitud,
                                 TipoSolicitudId = r.CodigoTipoSolicitud,
                                 DescripcionSolicitud = r.DescripcionSolicitud,
                                 FechaSolicitud = r.FechaCreacion,
                                 Estatus = r.EstadoSolicitud,
                                 EstadoId = r.CodigoEstadoSolicitud,
                                 SubActividad = r.SubTipoSolicitud,
                                 CodigoSubactividad = r.CodigoSubTipoSolicitud

                             }).Where(x => x.ResponsableId == codigo).ToList();

            if (filtro == "" && estado == null)
            {
                if (director == null && /*TecnicoAlternativo == null &&*/ Supervisor == null)
                {
                    var AsignadosUsuariosNormal = Asignados.Where(x => x.ResponsableId == codigo).ToList();
                    return Json(AsignadosUsuariosNormal);

                }
                else if (director != null)
                {
                    var AsignadosDirector = Asignados.Where(x => x.DepartamentoId == director.DepartamentoId).OrderByDescending(x => x.FechaSolicitud).ToList();
                    return Json(AsignadosDirector);
                }
                else if (Supervisor != null)
                {
                    var AsignadoSupervisor = Asignados.Where(x => x.TipoSolicitudId == Supervisor.ActividadId && codigo == Supervisor.SupervisorId).OrderByDescending(x => x.FechaSolicitud).ToList();
                    return Json(AsignadoSupervisor);
                }
            }

            if (filtro == "3" && estado != 0 && Fechafinal.ToString() != "" && fechaInicial != "")
            {

                if (director == null && /*TecnicoAlternativo == null &&*/ Supervisor == null)
                {
                    var AsignadosUsuariosNormal = Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == estado
                             && x.FechaSolicitud >= inicio &&
                             x.FechaSolicitud <= Fechafinal)
                                 .OrderByDescending(x => x.FechaSolicitud).ToList();
                    return Json(AsignadosUsuariosNormal);

                }
                else if (director != null)
                {
                    var AsignadosDirector = Asignados.Where(x => x.DepartamentoId == director.DepartamentoId && x.EstadoId == estado
                             && x.FechaSolicitud >= inicio &&
                             x.FechaSolicitud <= Fechafinal).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadosDirector);
                }
                else if (Supervisor != null)
                {
                    var AsignadoSupervisor = Asignados.Where(x => x.TipoSolicitudId == Supervisor.ActividadId && codigo == Supervisor.SupervisorId && x.EstadoId == estado
                             && x.FechaSolicitud >= inicio &&
                             x.FechaSolicitud <= Fechafinal).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadoSupervisor);
                }

                #region comentario de codigo filtro tecnico altenativo ya no es necesario 
                //else if (TecnicoAlternativo != null)
                //{
                //    var AsignadoTecnicoAlternativo = Asignados;


                //    var actividades = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo).ToList();


                //    foreach (var item in actividades)
                //    {
                //        var Sol_actividades = (from r in db.Sol_Detalle_Lista

                //                               select new vServiciosIntranet
                //                               {
                //                                   SolicitudId = r.CodigoSolicitud,
                //                                   SolicitanteId = r.CodigoSolicitante,
                //                                   ResponsableId = r.CodigoTecnico,
                //                                   Solicitante = r.NombreSolicitante.ToUpper(),
                //                                   Responsable = r.NombreTecnico,
                //                                   DependenciaId = r.DependenciaId,
                //                                   Dependencia = r.DependenciaNombre,
                //                                   TipoSolicitud = r.TipoSolicitud,
                //                                   TipoSolicitudId = r.CodigoTipoSolicitud,
                //                                   DescripcionSolicitud = r.DescripcionSolicitud,
                //                                   FechaSolicitud = r.FechaCreacion,
                //                                   Estatus = r.EstadoSolicitud,
                //                                   EstadoId = r.CodigoEstadoSolicitud,
                //                                   SubActividad = r.SubTipoSolicitud,
                //                                   DepartamentoId = r.CodigoDepartamento,
                //                                   Departamento = r.Departamento,
                //                                   CodigoSubactividad = r.CodigoSubTipoSolicitud
                //                               }).Where(x => (x.TipoSolicitudId == item.ActividadId) && x.Estatus == "Pendiente").ToList();
                //        //var Sol_actividades =
                //        //     (from r in  db.Sol_Detalle_Lista.Where(x => (x.CodigoTipoSolicitud == item.ActividadId) && x.EstadoSolicitud == "Pendiente")
                //        //    .ToList();
                //        foreach (var item2 in Sol_actividades)
                //        {
                //            if (!AsignadoTecnicoAlternativo.Contains(item2))
                //            {
                //                AsignadoTecnicoAlternativo.Add(item2);
                //            }
                //        }
                //    }

                //    return Json(AsignadoTecnicoAlternativo);

                //}

                #endregion

            }

            else if (filtro == "2" && responsable != "" && estado != 0)
            {
                if (director == null && /*TecnicoAlternativo == null &&*/ Supervisor == null)
                {
                    var AsignadosUsuariosNormal = Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == estado
                            && x.Responsable.ToLower().Contains(responsable.ToLower())).ToList();
                    return Json(AsignadosUsuariosNormal);

                }
                else if (director != null)
                {
                    var AsignadosDirector = Asignados.Where(x => x.DepartamentoId == director.DepartamentoId && x.EstadoId == estado
                            && x.Responsable.ToLower().Contains(responsable.ToLower())).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadosDirector);
                }
                else if (Supervisor != null)
                {
                    var AsignadoSupervisor = Asignados.Where(x => x.TipoSolicitudId == Supervisor.ActividadId && codigo == Supervisor.SupervisorId && x.EstadoId == estado
                            && x.Responsable.ToLower().Contains(responsable.ToLower())).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadoSupervisor);
                }



            }

            else if (filtro == "4" && solicitante != "" && estado != 0)
            {
                if (director == null && /*TecnicoAlternativo == null &&*/ Supervisor == null)
                {
                    var AsignadosUsuariosNormal = Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == estado
                            && x.Solicitante.ToLower().Contains(solicitante.ToLower())).ToList();
                    return Json(AsignadosUsuariosNormal);

                }
                else if (director != null)
                {
                    var AsignadosDirector = Asignados.Where(x => x.DepartamentoId == director.DepartamentoId && x.EstadoId == estado
                            && x.Solicitante.ToLower().Contains(solicitante.ToLower())).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadosDirector);
                }
                else if (Supervisor != null)
                {
                    var AsignadoSupervisor = Asignados.Where(x => x.TipoSolicitudId == Supervisor.ActividadId && codigo == Supervisor.SupervisorId && x.EstadoId == estado
                            && x.Solicitante.ToLower().Contains(solicitante.ToLower())).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadoSupervisor);
                }
                #region comentario de codigo filtro tecnico altenativo ya no es necesario
                //else if (TecnicoAlternativo != null)
                //{
                //    var AsignadoTecnicoAlternativo = Asignados.Where(x => x.ResponsableId == codigo).ToList();


                //    var actividades = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo).ToList();


                //    foreach (var item in actividades)
                //    {
                //        var Sol_actividades = Asignados.Where(x => (x.TipoSolicitudId == item.ActividadId) && x.EstadoId == estado
                //            && x.Solicitante.ToLower().Contains(solicitante.ToLower())).OrderByDescending(x => x.FechaSolicitud).ToList();
                //        foreach (var item2 in Sol_actividades)
                //        {
                //            if (!AsignadoTecnicoAlternativo.Contains(item2))
                //            {
                //                AsignadoTecnicoAlternativo.Add(item2);
                //            }
                //        }

                //    }
                //    return Json(AsignadoTecnicoAlternativo.Where(x => x.Solicitante.Contains(solicitante.ToLower())).ToList());


                //}
                #endregion

            }



            else if (estado != 0 && filtro == "" || filtro == null || filtro == "2" || filtro == "3" && responsable == "")
            {

                if (director == null && /*TecnicoAlternativo == null &&*/ Supervisor == null)
                {
                    var AsignadosUsuariosNormal = Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == estado
                            ).ToList();
                    return Json(AsignadosUsuariosNormal);

                }
                else if (director != null)
                {
                    var AsignadosDirector = Asignados.Where(x => x.DepartamentoId == director.DepartamentoId && x.EstadoId == estado
                            ).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadosDirector);
                }
                else if (Supervisor != null)
                {
                    var AsignadoSupervisor = Asignados.Where(x => x.TipoSolicitudId == Supervisor.ActividadId && codigo == Supervisor.SupervisorId && x.EstadoId == estado
                           ).OrderByDescending(x => x.FechaSolicitud).ToList();

                    return Json(AsignadoSupervisor);
                }
            }

            return Json(Asignados.Where(x => x.ResponsableId == codigo && x.EstadoId == estado).ToList());



        }

        [HttpPost]
        public JsonResult TecnicoAlternativo(int? estado, string filtro, string responsable, string solicitante, string fechaInicial, string fechaFinal, string TipoSolicitudId)
        {
            int codigo = int.Parse(Session["empleadoId"].ToString());




            var Asignados = (from r in db.vw_FormularioSolicitudServicios

                             select new vServiciosIntranet
                             {
                                 SolicitudId = r.CodigoSolicitud,
                                 SolicitanteId = r.CodigoSolicitante,
                                 ResponsableId = r.CodigoTecnico,
                                 Solicitante = r.NombreSolicitante.ToUpper(),
                                 Responsable = r.NombreTecnico,
                                 DependenciaId = r.DependenciaId,
                                 Dependencia = r.DependenciaNombre,
                                 TipoSolicitud = r.TipoSolicitud,
                                 TipoSolicitudId = r.CodigoTipoSolicitud,
                                 DescripcionSolicitud = r.DescripcionSolicitud,
                                 FechaSolicitud = r.FechaCreacion,
                                 Estatus = r.EstadoSolicitud,
                                 EstadoId = r.CodigoEstadoSolicitud,
                                 SubActividad = r.SubTipoSolicitud,
                                 CodigoSubactividad = r.CodigoSubTipoSolicitud

                             }).Where(x => x.ResponsableId == codigo && x.EstadoId == 1).ToList();


            var AsignadoTecnicoAlternativo = Asignados;


            var actividades = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo).ToList();


            foreach (var item in actividades)
            {
                var Sol_actividades = (from r in db.vw_FormularioSolicitudServicios

                                       select new vServiciosIntranet
                                       {
                                           SolicitudId = r.CodigoSolicitud,
                                           SolicitanteId = r.CodigoSolicitante,
                                           ResponsableId = r.CodigoTecnico,
                                           Solicitante = r.NombreSolicitante.ToUpper(),
                                           Responsable = r.NombreTecnico,
                                           DependenciaId = r.DependenciaId,
                                           Dependencia = r.DependenciaNombre,
                                           TipoSolicitud = r.TipoSolicitud,
                                           TipoSolicitudId = r.CodigoTipoSolicitud,
                                           DescripcionSolicitud = r.DescripcionSolicitud,
                                           FechaSolicitud = r.FechaCreacion,
                                           Estatus = r.EstadoSolicitud,
                                           EstadoId = r.CodigoEstadoSolicitud,
                                           SubActividad = r.SubTipoSolicitud,
                                           CodigoSubactividad = r.CodigoSubTipoSolicitud
                                       }).Where(x => (x.TipoSolicitudId == item.ActividadId) && x.Estatus == "Pendiente").ToList();

                foreach (var item2 in Sol_actividades)
                {
                    if (!AsignadoTecnicoAlternativo.Contains(item2))
                    {
                        AsignadoTecnicoAlternativo.Add(item2);
                    }
                }
            }

            return Json(AsignadoTecnicoAlternativo, JsonRequestBehavior.AllowGet);

            // return ccc ("ServiciosAsignados", "SolicitudesServicios", AsignadoTecnicoAlternativo);

        }


        [HttpPost]
        public JsonResult Asignar(int[] solicitudId, string[] tecnico1)
        {
            if (tecnico1 == null)
            {
                return Json(new { success = false, Mensaje = "Seleccione la persona que se le va a asignar el servicio" });
            }
            string empleadoId = Session["empleadoId"].ToString();

            for (int i = 0; i < solicitudId.Length; i++)
            {

                int sol = solicitudId[i];
                var asignado = db.Sol_Registro_Solicitud.Where(x => x.SolicitudId == sol).FirstOrDefault();

                //for (int a = 0; a <= tecnico1.Length; a++)
                //{

                asignado.TecnicoId = int.Parse(tecnico1[i]);

                var tecnico = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == asignado.TecnicoId.ToString()).FirstOrDefault();

                asignado.DomainUserTecnico = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", tecnico.Cedula)).FirstOrDefault().samAccountName; ;

                asignado.TecnicoId = asignado.TecnicoId;
                asignado.DomainUserTecnico = asignado.DomainUserTecnico;
                asignado.FhFinalSolicitud = DateTime.Now;
                asignado.FhModificacion = DateTime.Now;
                db.SaveChanges();

                var empleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == asignado.TecnicoId.ToString()).FirstOrDefault();
                var supervisor = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == empleadoId).FirstOrDefault();
                var actividad = db.Sol_Actividades.Where(x => x.ActividadId == asignado.Tipo_SolicitudId).FirstOrDefault();


                string descripcion = "";

                descripcion = "SE REASIGNO " + empleado.nombre.ToUpper() + " POR " + supervisor.nombre.ToUpper();


                //Obtener Direccion Ip de la Pc
                string IP4Address = String.Empty;

                foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
                {
                    if (IPA.AddressFamily == AddressFamily.InterNetwork)
                    {
                        IP4Address = IPA.ToString();
                        break;
                    }
                }


                if (HttpContext.Session["empleadoId"] != null)
                {
                    Sol_Logs Tb = new Sol_Logs();
                    Tb.SistemaId = 1;
                    Tb.SolicitudId = asignado.SolicitudId;
                    Tb.logIP = IP4Address;
                    Tb.logUsuario = HttpContext.Session["empleadoId"].ToString();
                    Tb.logFecha = DateTime.Now;
                    Tb.logAccion = descripcion;
                    db.Sol_Logs.Add(Tb);
                    db.SaveChanges();
                }

                //reg.InsertarLog(asignado.SolicitudId, descripcion);

                if (asignado.DependenciaRegionalId == 1)
                {
                    int CdgTipoSolicitud = asignado.SolicitudId;

                    var Supervisor = db.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                    string correo_supervisor = "";

                    if (Supervisor != null)
                    {
                        if (Supervisor.CodigoDepartamento == 15)
                        {
                            correo_supervisor = Code.Utilities2.obtenerCorreoUsuario(Supervisor.Cedula);
                        }
                        else
                        {
                            correo_supervisor = Code.Utilities2.obtenerCorreoUsuario(Supervisor.Cedula);
                        }
                    }

                    String CorreoUsuario = "";

                    var empleado_Cedula = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == empleado.cedula).FirstOrDefault();

                    if (empleado_Cedula != null)
                    {
                        if (empleado_Cedula.CodigoDependencia == "10001" && empleado_Cedula.CodigoDepartamento == "15")
                        {
                            CorreoUsuario = Code.Utilities2.obtenerCorreoUsuario(empleado_Cedula.Cedula);
                        }
                        else
                        {
                            CorreoUsuario = Code.Utilities2.obtenerCorreoUsuario(empleado_Cedula.Cedula);
                        }
                    }

                    var solicitante_obj = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == asignado.SolicitanteId.ToString()).FirstOrDefault();

                    string solicitante_correo = "";

                    if (solicitante_obj != null)
                    {
                        if (solicitante_obj.CodigoDependencia == "10001" && solicitante_obj.CodigoDepartamento == "15")
                        {
                            solicitante_correo = Code.Utilities2.obtenerCorreoUsuario(solicitante_obj.Cedula);
                        }
                        else
                        {
                            solicitante_correo = Code.Utilities2.obtenerCorreoUsuario(solicitante_obj.Cedula);
                        }
                    }

                    string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/RegistroSolicitud?id={asignado.SolicitudId}";

                    EnviarCorreo(asignado.DomainUserSolicitante, asignado.DomainUserTecnico, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + supervisor.dependencia.ToUpper(), DescripcionCorreo("REASIGNACIÓN DE SOLICITUD", solicitante_obj.Departamento.ToUpper(), solicitante_obj.NombreCompleto.ToUpper(), asignado.DescripcionSolicitud, url, actividad.Descripcion));

                }


                else
                {
                    var Supervisor = db.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == asignado.DependenciaRegionalId && cd.DependenciaEstado == true).FirstOrDefault();

                    string correo_supervisor = "";

                    if (Supervisor != null)
                    {
                        var Supervisor_DatosPersonales = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == Supervisor.DependenciaSupervisorId.ToString()).FirstOrDefault();

                        correo_supervisor = Code.Utilities2.obtenerCorreoUsuario(Supervisor_DatosPersonales.Cedula);
                    }

                    String CorreoUsuario = "";

                    var empleado_Cedula = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == empleado.cedula).FirstOrDefault();

                    if (empleado_Cedula != null)
                    {
                        if (empleado_Cedula.CodigoDependencia == "10001" && empleado_Cedula.CodigoDepartamento == "15")
                        {
                            CorreoUsuario = Code.Utilities2.obtenerCorreoUsuario(empleado_Cedula.Cedula);
                        }
                        else
                        {
                            CorreoUsuario = Code.Utilities2.obtenerCorreoUsuario(empleado_Cedula.Cedula);
                        }
                    }

                    var solicitante_obj = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == asignado.SolicitanteId.ToString()).FirstOrDefault();

                    string solicitante_correo = "";

                    if (solicitante_obj != null)
                    {
                        if (solicitante_obj.CodigoDependencia == "10001" && solicitante_obj.CodigoDepartamento == "15")
                        {
                            solicitante_correo = Code.Utilities2.obtenerCorreoUsuario(solicitante_obj.Cedula);
                        }
                        else
                        {
                            solicitante_correo = Code.Utilities2.obtenerCorreoUsuario(solicitante_obj.Cedula);
                        }
                    }
                    string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/RegistroSolicitud?id={asignado.SolicitudId}";

                    EnviarCorreo(solicitante_correo, asignado.DomainUserTecnico, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + supervisor.dependencia, DescripcionCorreo("REASIGNACIÓN DE SOLICITUD", supervisor.departamento.ToUpper(), supervisor.nombre.ToUpper(), actividad.Descripcion, url, asignado.DescripcionSolicitud));


                }
            }

            // reg.Mostrar_Registro_Solicitud(asignado.SolicitudId);
            return Json(new { success = true, Mensaje = "Solicitud Asignada Correctamente" });
        }

        // GET: SolicitudesServicios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sol_Registro_Solicitud sol_Registro_Solicitud = db.Sol_Registro_Solicitud.Find(id);
            if (sol_Registro_Solicitud == null)
            {
                return HttpNotFound();
            }
            return View(sol_Registro_Solicitud);
        }

        // GET: SolicitudesServicios/Create
        public ActionResult CrearServicio(Sol_Registro_Solicitud Servicio = null)
        {
            if (HttpContext.Session["empleadoId"] != null)
            {
                TempData["AgregarServicio"] = "";


                int codigo = int.Parse(Session["empleadoId"].ToString());
                ViewBag.code = Session["empleadoId"].ToString();
                string codigoProduccion = Session["empleadoId"].ToString();

                var Otrostecnicos = db.Sol_Otros_Tecnicos.Where(x => x.TecnicoId == codigo && x.Estado).FirstOrDefault();
                if (Otrostecnicos != null)
                {
                    ViewBag.Otrostecnicos = "Otrostecnico";
                }

                var director = db.Sol_Departamento.Where(x => x.DirectorId == codigo && x.Estado).FirstOrDefault();
                if (director != null)
                {
                    ViewBag.Director = "Director";
                }
                var Supervisor = db.Sol_Actividades.Where(x => x.SupervisorId == codigo && x.Estado).FirstOrDefault();
                if (Supervisor != null)
                {
                    ViewBag.Supervisor = "Supervisor";
                }

                ViewBag.Descripcion = Servicio?.DescripcionSolicitud;

                var Dependencia = db.Sol_Tecnicos_Dependencias.Where(x => x.TecnicoDependenciaEmpleadoId == codigo && x.TecnicoDependenciaEstado && x.TecnicoDependenciaCrearSolicitante).FirstOrDefault();
                if (Dependencia != null)
                {
                    ViewBag.Dependencia = "Dependencia";
                }

                var TecnicoAlternartivo = db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == codigo && x.Estado).FirstOrDefault();

                if (TecnicoAlternartivo != null)
                {
                    ViewBag.TecnicoAlternartivo = "TecnicoAlternartivo";

                }

                if (TecnicoAlternartivo?.AgregarServicio == true)
                {
                    TempData["AgregarServicio"] = "OK";
                }
                var paseProduccion = db.EncargadoPaseProduccion.Where(x => x.EmpleadoId == codigoProduccion).FirstOrDefault();

                if (paseProduccion != null)
                {
                    ViewBag.Produccion = "Produccion";
                }

                string dependenciaId = Session["dependenciaId"].ToString();
                string departamentoId = Session["departamentoId"].ToString();
                ViewBag.DependenciaRegionalId = (from d in db.Sol_DependenciasRegionales
                                                 select new
                                                 {
                                                     dRegionalId = d.DependenciaId,
                                                     dNombre = d.DependenciaNombre
                                                 }).ToList();
                var departamento = db.Sol_Departamento.Where(x => x.Estado).Select(x => new
                {
                    DepartamentoId = x.DepartamentoId,
                    DepNombre = x.Descripcion
                }).ToList();
                ViewBag.DepartamentoId = departamento;

                var actividades = (from t in db.Sol_Actividades
                                   where t.Estado
                                   select new
                                   {
                                       tActividadId = t.ActividadId,
                                       tActividadNombre = t.Descripcion
                                   }).OrderBy(x => x.tActividadNombre).ToList();
                ViewBag.ActividadesId = actividades;

                var ServicioSolicitud = (from subactividad in db.Sol_Sub_Actividades
                                         where subactividad.Estado
                                         select new
                                         {
                                             SubactividadId = subactividad.SubActividadId,
                                             SubActividadNombre = subactividad.Descripcion
                                         }).ToList();
                ViewBag.SubActividadesId = ServicioSolicitud;


                var DatosEmpleado = (from A in db.VISTA_EMPLEADOS
                                     orderby A.nombre.ToUpper() ascending
                                     select new PersonalPgrModel
                                     {
                                         EmpleadoId = A.empleadoid,
                                         Descripcion = A.nombre.ToUpper(),
                                         Cedula = A.cedula,
                                     }).ToList();

                ViewBag.Viaticos = DatosEmpleado;

                ViewBag.DatosEmpleado = DatosEmpleado.Where(x => x.EmpleadoId != "2917" && x.EmpleadoId != "16983" && x.EmpleadoId != "18109"
                                     && x.EmpleadoId != "18106" && x.EmpleadoId != "16094"
                                     && x.EmpleadoId != "16098"
                                     && x.EmpleadoId != "16090" && x.EmpleadoId != "17705");

                var flotas = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpEmpleadoId == codigo).FirstOrDefault();
                if (flotas == null)
                {
                    ViewBag.FlotaNum = "";
                    ViewBag.flotaExtencion = "";

                }
                else
                {
                    ViewBag.FlotaNum = flotas.FlotExtEmpFlota;
                    ViewBag.flotaExtencion = flotas.FlotExtEmpExtesion;
                }

                ViewBag.AreasImplicadas = (from a in db.AreasImplicadasPaseProduccion
                                           select new AreasProduccionVM
                                           {
                                               AreaId = a.AreasImplicadasId,
                                               AreasDescripcion = a.AreasImplicadasDescripcion
                                           }).ToList();

                ViewBag.CategoriasProduccion = (from c in db.CategoriasProduccion
                                                select new CategoriaProduccioVM
                                                {
                                                    CategoriaId = c.CategoriasProduccionId,
                                                    CategoriaDescripcion = c.CategoriasProduccionDescripcion
                                                }).ToList();



                ViewBag.TipoEvento = db.TipoEventos.ToList();
                ViewBag.tipoCoberturas = db.TipoCobertura.ToList();

                var solicitudEspacios = db.Sol_Registro_Solicitud.SelectMany(x => x.Eventos.Where(y => y.EventoFhFinal > DateTime.Now && x.Tipo_SolicitudId == 1016 || x.Tipo_SolicitudId == 17).ToList()).OrderBy(x => x.EventoFhInicio).ToList();
                ViewBag.fechasSolicEsp = solicitudEspacios;

                var coberturaPrensa = db.Sol_Registro_Solicitud.SelectMany(x => x.Eventos.Where(y => y.EventoFhFinal > DateTime.Now && x.Tipo_SolicitudId == 1017 || x.Tipo_SolicitudId == 18).ToList()).OrderBy(x => x.EventoFhInicio).ToList();
                ViewBag.fechasCoberturaPrensa = coberturaPrensa;

                var MontajeEventos = db.Sol_Registro_Solicitud.SelectMany(x => x.Eventos.Where(y => y.EventoFhFinal > DateTime.Now && x.Tipo_SolicitudId == 1018 || x.Tipo_SolicitudId == 19).ToList()).OrderBy(x => x.EventoFhInicio).ToList();
                ViewBag.fechasMontajeEventos = MontajeEventos;

                ViewBag.Evento = new intra.Models.Eventos();
            }
            return View();
        }

        // POST: SolicitudesServicios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearServicio(Sol_Registro_Solicitud Servicio, SolicitudModel SolicitudModel, FormCollection fc, int[] AreasImplicadas, int[] CategoriasProduccion, int[] Viaticos)
        {

            try
            {

                int codigo = int.Parse(Session["empleadoId"].ToString());
                int DependenciaRegionalId = int.Parse(fc["DependenciaRegionalId"]);

                string SolicitanteAsignado = fc["tecnico"]; // que rayos???
                string SolicitantePAP = fc["tecnico"];
                string Responsable = fc["ResponsableServicioId"];
                string ResponsableAsignado = "";

                var DatosPersona = db.Sol_Mostrar_Personal_PGR.Where(x => x.Codigo == codigo.ToString()).FirstOrDefault();

                string UsuarioDominio = "";

                string NombreCompletoCo = "";
                string DepartamentoCo = "";

                string dependenciaId = Session["dependenciaId"].ToString();
                string departamentoId = Session["departamentoId"].ToString();

                if (DependenciaRegionalId == 1)
                {

                    int actividadSupervisor = int.Parse(fc["Tipo_SolicitudId"]);
                    var SupervisorSedeCentral = db.Sol_Actividades.Where(x => x.ActividadId == actividadSupervisor).FirstOrDefault();

                    if ((SolicitanteAsignado != "" || SolicitanteAsignado == null || Responsable != "" || Responsable == null) && (fc["Tipo_SolicitudId"] == "32" /* || fc["Tipo_SolicitudId"] == "2"*/ ))
                    {

                        Servicio.DomainUserSolicitante = Session["usuario"].ToString();
                        Servicio.SolicitanteId = int.Parse(Session["empleadoid"].ToString());
                        Servicio.TecnicoId = SupervisorSedeCentral.SupervisorId;
                        Servicio.DomainUserTecnico = SupervisorSedeCentral.Correo;

                    }
                    else if (!string.IsNullOrEmpty(SolicitanteAsignado) && !string.IsNullOrEmpty(Responsable))
                    {

                        var DatosPersonaSeleccionada = db.Sol_Mostrar_Personal_PGR.Where(x => x.Codigo == SolicitanteAsignado).FirstOrDefault();
                        var DatosResponsable = db.Sol_Mostrar_Personal_PGR.Where(x => x.Codigo == Responsable).FirstOrDefault();


                        UsuarioDominio = Code.Utilities2.obtenerCorreoUsuario(DatosPersonaSeleccionada.Cedula);
                        ResponsableAsignado = Code.Utilities2.obtenerCorreoUsuario(DatosResponsable.Cedula);

                        Servicio.DomainUserSolicitante = UsuarioDominio;
                        Servicio.SolicitanteId = int.Parse(SolicitanteAsignado); //int.Parse(fc["tecnico"]);
                        Servicio.TecnicoId = int.Parse(Responsable);
                        Servicio.DomainUserTecnico = ResponsableAsignado;


                    }

                    else if (!string.IsNullOrEmpty(Responsable) && string.IsNullOrEmpty(SolicitanteAsignado))
                    {
                        var DatosResponsable = db.Sol_Mostrar_Personal_PGR.Where(x => x.Codigo == Responsable).FirstOrDefault();


                        ResponsableAsignado = Code.Utilities2.obtenerCorreoUsuario(DatosResponsable.Cedula);
                        Servicio.DomainUserSolicitante = Session["usuario"].ToString();
                        Servicio.SolicitanteId = int.Parse(Session["empleadoid"].ToString());
                        Servicio.TecnicoId = int.Parse(fc["ResponsableServicioId"]);
                        Servicio.DomainUserTecnico = ResponsableAsignado;

                    }
                    else if (!string.IsNullOrEmpty(SolicitanteAsignado) && string.IsNullOrEmpty(Responsable))
                    {
                        var DatosPersonaSeleccionada = db.Sol_Mostrar_Personal_PGR.Where(x => x.Codigo == SolicitanteAsignado).FirstOrDefault();

                        UsuarioDominio = Code.Utilities2.obtenerCorreoUsuario(DatosPersonaSeleccionada.Cedula);

                        Servicio.DomainUserSolicitante = UsuarioDominio;
                        Servicio.SolicitanteId = int.Parse(fc["tecnico"]);
                        Servicio.TecnicoId = SupervisorSedeCentral.SupervisorId;
                        Servicio.DomainUserTecnico = SupervisorSedeCentral.Correo;
                    }

                    else
                    {
                        Servicio.DomainUserSolicitante = Session["usuario"].ToString();
                        Servicio.SolicitanteId = int.Parse(Session["empleadoid"].ToString());
                        Servicio.TecnicoId = SupervisorSedeCentral.SupervisorId;
                        Servicio.DomainUserTecnico = SupervisorSedeCentral.Correo;

                    }

                    Servicio.Tipo_SolicitudId = int.Parse(fc["Tipo_SolicitudId"]);
                    Servicio.Tipo_Sub_SolicitudId = int.Parse(fc["Tipo_Sub_SolicitudId"]);
                    Servicio.DependenciaRegionalId = int.Parse(fc["DependenciaRegionalId"]);
                    Servicio.DescripcionSolicitud = fc["DescripcionSolicitud"];
                    Servicio.DepartamentoId = int.Parse(fc["DepartamentoId"]);
                    Servicio.EstadoId = 1;
                    Servicio.Horas = 24;
                    Servicio.Estado = true;
                    Servicio.TipodeAsistencia = 4;
                    Servicio.FhCreacion = DateTime.Now;
                    Servicio.FhInicioSolicitud = DateTime.Now;
                    Servicio.FhFinalSolicitud = DateTime.Now;
                    Servicio.FhModificacion = DateTime.Now;
                    db.Sol_Registro_Solicitud.Add(Servicio);
                    db.SaveChanges();

                }
                else
                //Aqui va lo de la dependencias ! a General
                {
                    var SupervisorDependencia = db.Sol_DependenciasRegionales.Where(x => x.DependenciaId == DependenciaRegionalId).FirstOrDefault();

                    if (!string.IsNullOrEmpty(SolicitanteAsignado))
                    {
                        var DatosPersonaSeleccionada = db.Sol_Mostrar_Personal_PGR.Where(x => x.Codigo == SolicitanteAsignado).FirstOrDefault();

                        Servicio.DomainUserSolicitante = Code.Utilities2.obtenerCorreoUsuario(DatosPersonaSeleccionada.Cedula);
                        Servicio.SolicitanteId = int.Parse(SolicitanteAsignado);
                    }
                    else
                    {
                        Servicio.SolicitanteId = codigo;
                        Servicio.DomainUserSolicitante = Session["usuario"].ToString();

                    }

                    Servicio.DependenciaRegionalId = int.Parse(fc["DependenciaRegionalId"]);
                    Servicio.TecnicoId = SupervisorDependencia.DependenciaSupervisorId;
                    Servicio.DomainUserTecnico = SupervisorDependencia.DependenciaSupervisorCorreo;
                    Servicio.DepartamentoId = 15;
                    Servicio.DescripcionSolicitud = fc["DescripcionSolicitud"];
                    Servicio.Tipo_SolicitudId = -1;

                    Servicio.EstadoId = 1;
                    Servicio.Estado = true;
                    Servicio.Tipo_Sub_SolicitudId = null;
                    Servicio.Horas = 24;
                    Servicio.TipodeAsistencia = 4;
                    Servicio.FhInicioSolicitud = DateTime.Now;
                    Servicio.FhFinalSolicitud = DateTime.Now;
                    Servicio.FhModificacion = DateTime.Now;
                    Servicio.FhCreacion = DateTime.Now;
                    db.Sol_Registro_Solicitud.Add(Servicio);
                    db.SaveChanges();

                }

                try
                {

                    var A = db.Sol_Registro_Solicitud.OrderByDescending(cd => cd.SolicitudId).Take(1).FirstOrDefault();

                    var actividad = db.Sol_Actividades.Where(x => x.ActividadId == A.Tipo_SolicitudId).FirstOrDefault();
                    var DependenciaRegional = db.Sol_DependenciasRegionales.Where(x => x.DependenciaId == A.DependenciaRegionalId).FirstOrDefault();

                    var empleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == SolicitantePAP).FirstOrDefault();


                    var employee = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == Servicio.SolicitanteId.ToString()).FirstOrDefault();
                    NombreCompletoCo = employee.nombre.ToUpper();
                    DepartamentoCo = employee.departamento.ToUpper();


                    string Valor = A.SolicitudId.ToString() + "#Ancla";

                    // string Descripcion = Mth_Mostrar_Actividad(A.Tipo_SolicitudId);
                    string IP4Address = String.Empty;

                    foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
                    {
                        if (IPA.AddressFamily == AddressFamily.InterNetwork)
                        {
                            IP4Address = IPA.ToString();
                            break;
                        }
                    }


                    if (HttpContext.Session["empleadoId"] != null)
                    {
                        Sol_Logs Tb = new Sol_Logs();
                        Tb.SistemaId = 1;
                        Tb.SolicitudId = A.SolicitudId;
                        Tb.logIP = IP4Address;
                        Tb.logUsuario = HttpContext.Session["empleadoId"].ToString();
                        Tb.logFecha = DateTime.Now;
                        Tb.logAccion = "SE CREO LA SOLICITUD";
                        db.Sol_Logs.Add(Tb);
                        db.SaveChanges();
                    }

                    //  InsertarLog(A.SolicitudId, "SE CREO LA SOLICITUD");

                    var flotas = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpEmpleadoId == codigo).FirstOrDefault();

                    if (!string.IsNullOrEmpty(fc["Extension"]))
                    {
                        FlotaExtensionEmpleado flota = new FlotaExtensionEmpleado();
                        flota.FlotExtEmpEmpleadoId = codigo;
                        flota.FlotExtEmpExtesion = fc["Extension"];
                        flota.FlotExtEmpFlota = fc["Flota"];
                        db.SaveChanges();

                    }
                    else
                    {
                        var flota1 = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpId == flotas.FlotExtEmpId).FirstOrDefault();
                        flota1.FlotExtEmpId = flota1.FlotExtEmpId;
                        flota1.FlotExtEmpEmpleadoId = codigo;
                        flota1.FlotExtEmpExtesion = fc["Extension"];
                        flota1.FlotExtEmpFlota = fc["Flota"];
                        db.SaveChanges();

                    }

                    //Modulo de Viaticos

                    if (fc["Tipo_Sub_SolicitudId"] == "123")
                    // || fc["Tipo_Sub_SolicitudId"] == "298" || fc["Tipo_SolicitudId"] == "2031" || fc["Tipo_SolicitudId"] == "2031")
                    {
                        if (string.IsNullOrEmpty(fc["Viaticos"]))
                        {
                            TempData["PersonalViaticos"] = "Ok";
                            return Redirect("~/SolicitudesServicios/CrearServicio");
                        }


                        int solicitud = A.SolicitudId;
                        for (int a = 0; a <= Viaticos.Length - 1; a++)
                        {
                            EmpleadoViaticos empleadoViaticos = new EmpleadoViaticos();

                            empleadoViaticos.SolicitudId = solicitud;
                            empleadoViaticos.EmpleadoId = Viaticos[a];
                            db.EmpleadoViaticos.Add(empleadoViaticos);

                        }
                        db.SaveChanges();
                    }



                    if (Servicio.Tipo_SolicitudId == 1020 || Servicio.Tipo_SolicitudId == 35)
                    {

                        AprobacioneServicios ap = new AprobacioneServicios();
                        ap.DetallePaseProduccion = A.DescripcionSolicitud;
                        ap.FechaCreacion = DateTime.Now;
                        ap.ActividadId = A.Tipo_SolicitudId;
                        ap.SolicitanteId = int.Parse(empleado.empleadoid);
                        ap.Solicitante = empleado.nombre.ToUpper();
                        ap.SubActividadId = A.Tipo_Sub_SolicitudId;
                        ap.usuario = Session["usuario"].ToString();
                        ap.CodigoEmpLider = Convert.ToInt32(Session["empleadoId"]);
                        ap.ApEstatusId = 3;
                        ap.ConteoFinal = 1;
                        ap.Prioridad = fc["Prioridad"];
                        ap.FaseProyecto = fc["FaseProyecto"];
                        ap.Impacto = fc["Impacto"];
                        ap.Riesgo = fc["Riesgo"];
                        ap.SolicitudId = A.SolicitudId;
                        db.AprobacioneServicios.Add(ap);
                        db.SaveChanges();

                        var aprobacioneServicio = (from s in db.AprobacioneServicios
                                                   orderby s.AprobacionesId descending
                                                   select new
                                                   {
                                                       ID = s.AprobacionesId,
                                                       empleadoId = s.SolicitanteId
                                                   }).FirstOrDefault();

                        string CorreoSolicitante = Code.Utilities2.obtenerSp_UserPorCodigo(aprobacioneServicio.empleadoId.ToString())?.mail;

                        for (int a = 0; a <= AreasImplicadas.Length - 1; a++)
                        {
                            AreasAprobacionesProduccion areasAprobaciones = new AreasAprobacionesProduccion();

                            areasAprobaciones.AprobacionesId = aprobacioneServicio.ID;
                            areasAprobaciones.AreasImplicadasId = AreasImplicadas[a];
                            db.AreasAprobacionesProduccion.Add(areasAprobaciones);

                        }



                        db.SaveChanges();


                        for (int i = 0; i <= CategoriasProduccion.Length - 1; i++)
                        {
                            CategoriasAprobacionesProduccion categoria = new CategoriasAprobacionesProduccion();


                            categoria.AprobacionesId = aprobacioneServicio.ID;
                            categoria.CategoriasProduccionId = CategoriasProduccion[i];
                            db.CategoriasAprobacionesProduccion.Add(categoria);

                        }
                        db.SaveChanges();


                        AprobacionesActividad AprobacionesActividad = new AprobacionesActividad();
                        AprobacionesActividad.ApEstatusId = 3;
                        AprobacionesActividad.usuario = ap.usuario;
                        AprobacionesActividad.EmpleadoId = codigo.ToString();
                        AprobacionesActividad.AprobacionesId = aprobacioneServicio.ID;
                        AprobacionesActividad.LiderEquipo = DatosPersona.NombreCompleto;
                        AprobacionesActividad.FechaCreacion = DateTime.Now;
                        db.AprobacionesActividad.Add(AprobacionesActividad);
                        db.SaveChanges();

                        //  var aproba = db.AprobacioneServicios.OrderByDescending(x => x.AprobacionesId).FirstOrDefault();
                        //var Lider = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == codigo.ToString()).FirstOrDefault();
                        if(Servicio.Tipo_Sub_SolicitudId != 315)
                        { 

                        string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/PaseProduccion?aprobacion={ap.AprobacionesId}";
                        SendCorreo("SOLICITUD DE PASE A PRODUCCION", CorreoSolicitante, SolicitudAprobacionPaseProduccion(DepartamentoCo, NombreCompletoCo, Servicio.DescripcionSolicitud, url, actividad.Descripcion));
                        }
                        else
                        {
                            string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/PaseProduccion?aprobacion={ap.AprobacionesId}";
                            SendCorreoSeguridaDTI("SOLICITUD DE CONTROL DE CAMBIOS", CorreoSolicitante, SolicitudAprobacionPaseProduccionSeguridadDTI(DepartamentoCo, NombreCompletoCo, Servicio.DescripcionSolicitud, url, actividad.Descripcion));


                        }
                    }
                    else
                    {

                        if ((DependenciaRegionalId == 1) && (fc["ResponsableServicioId"] == null || fc["ResponsableServicioId"] == ""))
                        {
                            EnviarCorreo(A.DomainUserTecnico, A.DomainUserSolicitante, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + actividad.Descripcion, DescripcionCorreo(DepartamentoCo, NombreCompletoCo, Servicio.DescripcionSolicitud, Direccion + Valor, actividad.Descripcion));
                        }

                        else if ((DependenciaRegionalId == 1) && (fc["ResponsableServicioId"] != null || fc["ResponsableServicioId"] != ""))
                        {
                            EnviarCorreo(A.DomainUserTecnico, A.DomainUserSolicitante, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + actividad.Descripcion, DescripcionCorreo(DepartamentoCo, NombreCompletoCo, Servicio.DescripcionSolicitud, Direccion + Valor, actividad.Descripcion));
                        }
                        else
                        {
                            EnviarCorreo(A.DomainUserTecnico, A.DomainUserSolicitante, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + DependenciaRegional.DependenciaNombre, DescripcionCorreo(DepartamentoCo, NombreCompletoCo, Servicio.DescripcionSolicitud, Direccion + Valor, DependenciaRegional.DependenciaNombre));
                        }

                    }
                    TempData["Msj"] = "CREADO";
                }
                //Catch si no se envia el correo
                catch (Exception e)
                {
                    e.ToString();
                    TempData["Msj"] = "GUARDADO";
                }
                return Redirect("~/SolicitudesServicios/CrearServicio");
            }
            //Catch si no se guarda
            catch (Exception e)
            {
                e.ToString();
                TempData["Error"] = "HA OCURRIDO UN ERROR, LA SOLICITUD NO SE PUDO GUARDAR CORRECTAMENTE.";
            }
            return CrearServicio(Servicio);
        }


        // POST: SolicitudesServicios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Solicitudes(Sol_Registro_Solicitud sol_Registro_Solicitud)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sol_Registro_Solicitud).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sol_Registro_Solicitud);
        }

        #region Mostrar_Tipo_Servicios
        public JsonResult GetActividades(int id)
        {
            string codigo = Session["empleadoId"].ToString();

            var encargadoPaseProduccion = id == 15 ? db.EncargadoPaseProduccion.Any(x => x.EmpleadoId == codigo) : false;

            var departamento = Session["departamentoId"].ToString();

            var Tipo_Actividades = db.Sol_Actividades.Where(cd => cd.DepartamentoId == id && cd.Estado &&
                ((cd.ActividadId != 1037) || departamento == "15") &&
                ((cd.ActividadId != 1020 && cd.ActividadId != 35) || encargadoPaseProduccion)).OrderBy(x => x.Descripcion)
                .Select(x => new SelectListItem { Value = x.ActividadId.ToString(), Text = x.Descripcion }).ToList();

            return Json(Tipo_Actividades);
        }

        #endregion

        #region Mostrar_Tipos_Actividades_Servicios
        public JsonResult GetSubActividades(int id)
        {
            var Sub_Tipo_Actividades = db.Sol_Sub_Actividades.Where(cd => cd.ActividadId == id && cd.Estado).OrderBy(x => x.Descripcion)
                .Select(x => new SelectListItem { Value = x.SubActividadId.ToString(), Text = x.Descripcion }).ToList();

            return Json(Sub_Tipo_Actividades);
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
