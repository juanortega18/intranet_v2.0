﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;

namespace intra.Controllers
{
    public class EstadisticasController : Controller
    {

        //Instaciar Clases 
        dbIntranet db = new dbIntranet();


        // GET: Estadisticas
        [CustomAuthorize]
        [Authorize(Roles = "EA")]
        public ActionResult Index()
        {



            return RedirectToAction("cantidades");
            //return View();
        }
        [CustomAuthorize]
        [Authorize(Roles = "EA")]
        public ActionResult Cantidades()
        {
            if (Session["departamento"] == null)
                return Redirect("~/login");


            string emp_id = (string)Session["empleadoId"];
            string dept_id = null;
            string dependency_id = null;

            dbIntranet db = new dbIntranet();
            EstadisticasViewModel smodel = new EstadisticasViewModel();
            // Get the department id required
            // select departamentoid,dependenciaid from VISTA_EMPLEADOS where empleadoid = _@ID_SUPERVISOR;
            var requiredIds = (from t in db.VISTA_EMPLEADOS
                               where t.empleadoid == emp_id
                               select new { t.departamentoid, t.dependenciaid }).First();

            dept_id = requiredIds.departamentoid;
            dependency_id = requiredIds.dependenciaid;

            // Get every employee per department
            // select empleadoid, nombre from VISTA_EMPLEADOS where departamentoid = @dept_id and dependenciaid = @dependencia_id and not empleadoid = _@ID_SUPERVISOR;
            var employees = from t in db.VISTA_EMPLEADOS
                            where t.departamentoid == dept_id && t.dependenciaid == dependency_id && t.empleadoid != emp_id
                            select new { t.empleadoid, t.nombre };


            // Data required for the ViewModel
            var cantidades = new List<EstadisticasCantidades>();
            var años = new List<string>();
            var meses = new List<string>();
            var mesesPorAños = new Dictionary<string, string>();
            var cantidadesPorMeses = new Dictionary<string, List<string[]>>();
            var cantidadesUltimaSemana = new List<string[]>();
            var temp_lista_tiempo_repuesta = new List<VistaTiempoRespuesta>();
            var listaTiempoResp = new List<VistaTiempoRespuesta>();
            var temp_lista_tiempo_respuesta_mensual = new List<Vw_Tiempo_Excedido_Mensual>();
            var tecnico = new List<string>();
            var tiempo = new List<int>();
            var respuesta_mensual = new Dictionary<int, string>();

            foreach (var a in employees)
            {
                var temp_empleado_id = Convert.ToInt32(a.empleadoid);
                var temp_nom_empl = a.nombre;
                // Amount of requests:
                // select FhInicioSolicitud,FhFinalSolicitud from Sol_Registro_Solicitud where TecnicoId=_@ID_TECNICO;
                var requestAmounts = from t in db.Sol_Registro_Solicitud
                                     where t.TecnicoId == temp_empleado_id
                                     select new { t.TecnicoId, t.FhInicioSolicitud, t.FhFinalSolicitud };

                //Get amount time respond to tecnics
                var requestEmpl = (from t in db.VistaTiempoRespuesta
                                   where t.TecnicoId == temp_empleado_id
                                   select new
                                   {

                                       Tecnico_tec = t.TecnicoId,
                                       Cantidad_Tec = t.Cantidad

                                   }).ToList().Select(x => new VistaTiempoRespuesta
                                   {
                                       TecnicoId = x.Tecnico_tec,
                                       Cantidad = x.Cantidad_Tec

                                   }).GroupBy(s => s.TecnicoId);

                foreach (var r in requestEmpl)
                {
                    foreach (var b in r)
                    {


                        var nombre_empl = temp_nom_empl;


                        listaTiempoResp.Add(new VistaTiempoRespuesta
                        {
                            Cantidad = b.Cantidad,
                            Nombre = nombre_empl


                        });
                        smodel.ListaTiempoRespuesta = listaTiempoResp;
                    }
                }


                var requestEmplTiempo = (from t in db.VistaTiempoRespuestaMensual
                                         where t.TecnicoId == temp_empleado_id
                                         select new
                                         {
                                             Tecnico_tec = t.TecnicoId,
                                             Tiempo_Rep_Mens = t.TiempoRespMensual

                                         }).ToList().Select(x => new Vw_Tiempo_Respuesta_Mensual
                                         {
                                             TecnicoId = x.Tecnico_tec,
                                             TiempoRespMensual = x.Tiempo_Rep_Mens


                                         }).GroupBy(s => s.TecnicoId);

                foreach (var r in requestEmplTiempo)
                {


                    foreach (var b in r)
                    {

                        var temp_tecnico = a.nombre;

                        if (!tecnico.Contains(temp_tecnico)) tecnico.Add(temp_tecnico);

                        var temp_mes_resp = b.TiempoRespMensual;

                        if (!tiempo.Contains(temp_mes_resp)) tiempo.Add(temp_mes_resp);


                        if (!respuesta_mensual.ContainsKey(b.TecnicoId)) respuesta_mensual.Add(b.TecnicoId, a.nombre + "|" + temp_mes_resp);

                        smodel.ListaTiempRespMensual = respuesta_mensual.Values.ToList();

                    }

                }


                foreach (var b in requestAmounts)
                {
                    // Add it to the list taking the starting date of the request as reference
                    var temp_mes = b.FhInicioSolicitud.Month.ToString();

                    if (!meses.Contains(temp_mes))
                        meses.Add(temp_mes);
                    var temp_año = b.FhInicioSolicitud.Year.ToString();
                    if (!años.Contains(temp_año))
                        años.Add(temp_año);
                    if (mesesPorAños.ContainsKey(temp_mes))
                    {
                        if (!mesesPorAños[temp_mes].Contains(temp_año))
                            mesesPorAños[temp_mes] += "|" + temp_año;
                    }
                    else
                        mesesPorAños.Add(temp_mes, temp_año);
                    // Add it to the list taking the finishing date of the request as reference
                    temp_mes = b.FhFinalSolicitud.Month.ToString();

                    if (!meses.Contains(temp_mes))
                        meses.Add(temp_mes);
                    temp_año = b.FhFinalSolicitud.Year.ToString();
                    if (!años.Contains(temp_año))
                        años.Add(temp_año);

                    if (mesesPorAños.ContainsKey(temp_mes))
                    {
                        if (!mesesPorAños[temp_mes].Contains(temp_año))
                            mesesPorAños[temp_mes] += "|" + temp_año;
                    }
                    else
                        mesesPorAños.Add(temp_mes, temp_año);


                    cantidades.Add(new EstadisticasCantidades
                    {
                        IdTecnico = a.empleadoid,
                        Nombre = a.nombre,
                        InicioSolicitud = b.FhInicioSolicitud,
                        FinalSolicitud = b.FhFinalSolicitud



                    });

                }

                // Populate the dictionary that stores the filtered "last week" data //
                var currentDate = DateTime.Now;
                var SevenDaysAgo = currentDate.AddDays(-7);
                // Get the amount of requests asigned to a certain technician
                var requestAmountPerPerson_week = requestAmounts.Count((obj) =>
                    // where TecnicoId == empleadoid
                    obj.TecnicoId.ToString() == a.empleadoid &&
                    // and (starting_date.day >= date_now.day-7 and starting_date.day <= date_now.day)
                    ((obj.FhInicioSolicitud >= SevenDaysAgo && obj.FhInicioSolicitud <= currentDate) ||
                    // or (finishing_date.day >= date_now.day-7 and finishing_date.day <= date_now.day)
                    (obj.FhFinalSolicitud >= SevenDaysAgo && obj.FhFinalSolicitud <= currentDate)));
                // If the value is not 0:
                if (requestAmountPerPerson_week > 0)
                    cantidadesUltimaSemana.Add(new string[] { a.nombre, requestAmountPerPerson_week.ToString() });

                // Populate the dictionary that stores the filtered monthly data //
                foreach (var b in meses)
                {
                    // Get all the years that include a specific month
                    var year_group = mesesPorAños[b].Split('|');

                    foreach (var c in year_group)
                    {
                        // Get the amount of requests asigned to a certain technician
                        var requestAmountPerPerson = requestAmounts.Count((obj) =>
                            // where TecnicoId == empleadoid
                            obj.TecnicoId.ToString() == a.empleadoid &&
                            // and (starting_date.year == specific_year and starting_date.month == specific_month)
                            ((obj.FhInicioSolicitud.Year.ToString() == c && obj.FhInicioSolicitud.Month.ToString() == b) ||
                            // or (finishing_date.year == specific_year and finishing_date.month == specific_month)
                            (obj.FhFinalSolicitud.Year.ToString() == c && obj.FhFinalSolicitud.Month.ToString() == b)));

                        if (requestAmountPerPerson == 0)
                            continue;

                        // Create a key with the format ["year|month"]
                        if (!cantidadesPorMeses.ContainsKey(c + "|" + b))
                            cantidadesPorMeses.Add(c + "|" + b, new List<string[]>());

                        // Add the values { technician_name, request_amounts }
                        cantidadesPorMeses[c + "|" + b].Add(new string[] { a.nombre, requestAmountPerPerson.ToString() });
                    }
                }
            }



            foreach (var a in smodel.ListaTiempoRespuesta)
                // Verificar que entre los valores que contiene la lista no exista el valor actaul de la iteración
                if (temp_lista_tiempo_repuesta.Where((obj) => obj.Nombre == a.Nombre)
                    // Si el valor existe: retorna un string vacío
                    // De lo contrario: retorna nulo
                    .Select(obj => "").FirstOrDefault() == null)
                    // Si el valor retornado es nulo: (es decir, que no existe en la lista)
                    // añado a mi lista lo que contiene la var a
                    temp_lista_tiempo_repuesta.Add(new VistaTiempoRespuesta
                    {
                        Nombre = a.Nombre,

                        Cantidad = smodel.ListaTiempoRespuesta.Where((obj) => obj.Nombre == a.Nombre).Sum((obj) => obj.Cantidad) /
                            smodel.ListaTiempoRespuesta.Where((obj) => obj.Nombre == a.Nombre).Count()
                    });

            return View(new EstadisticasViewModel
            {
                ListaCantidades = cantidades,
                Años = años,
                Meses = meses,
                MesesPorAños = mesesPorAños,
                CantidadesPorMeses = cantidadesPorMeses,
                CantidadesUltimaSemana = cantidadesUltimaSemana,
                ListaTiempoRespuesta = temp_lista_tiempo_repuesta,
                ListaTiempRespMensual = smodel.ListaTiempRespMensual


            });


        }
        [CustomAuthorize]
        [Authorize(Roles = "EA")]
        public ActionResult Asignadas()
        {
            if (Session["departamento"] == null)
                return Redirect("~/login");

            string emp_id = (string)Session["empleadoId"];
            string dept_id = null;
            string dependency_id = null;

            dbIntranet db = new dbIntranet();

            // Get the department id required
            // select departamentoid,dependenciaid from VISTA_EMPLEADOS where empleadoid = _@ID_SUPERVISOR;
            var requiredIds = (from t in db.VISTA_EMPLEADOS
                               where t.empleadoid == emp_id
                               select new { t.departamentoid, t.dependenciaid }).First();

            dept_id = requiredIds.departamentoid;
            dependency_id = requiredIds.dependenciaid;

            // Get every employee per department
            // select empleadoid, nombre from VISTA_EMPLEADOS where departamentoid = @dept_id and dependenciaid = @dependencia_id and not empleadoid = _@ID_SUPERVISOR;
            var employees = from t in db.VISTA_EMPLEADOS
                            where t.departamentoid == dept_id && t.dependenciaid == dependency_id && t.empleadoid != emp_id
                            select new { t.empleadoid, t.nombre };


            var cantidadesAbiertasUltimaSemana = new List<string[]>();
            var cantidadesCerradasUltimaSemana = new List<string[]>();

            foreach (var a in employees)
            {
                var temp_empleado_id = Convert.ToInt32(a.empleadoid);
                // Open requests:
                // select FhInicioSolicitud, FhFinalSolicitud from Sol_Registro_Solicitud where TecnicoId = _@ID_TECNICO and estadoid = 1;
                // Closed requests:
                // select FhInicioSolicitud, FhFinalSolicitud from Sol_Registro_Solicitud where TecnicoId = _@ID_TECNICO and estadoid = 2;
                var requestAmounts = from t in db.Sol_Registro_Solicitud
                                     where t.TecnicoId == temp_empleado_id && (t.EstadoId == 1 || t.EstadoId == 2)
                                     select new { t.TecnicoId, t.FhInicioSolicitud, t.FhFinalSolicitud, t.EstadoId, t.Horas };

                // Populate the dictionary that stores the filtered "last week open requests" data //
                var currentDate = DateTime.Now;
                var SevenDaysAgo = currentDate.AddDays(-7);


                // Get the amount of requests asigned to a certain technician
                var requestOpenAmountPerPerson_week = requestAmounts.Count((obj) =>
                    // where TecnicoId == empleadoid
                    obj.TecnicoId.ToString() == a.empleadoid &&
                    // and EstadoId == 1 (open)
                    obj.EstadoId == 1 &&
                    // and (starting_date.day >= date_now.day-7 and starting_date.day <= date_now.day)
                    ((obj.FhInicioSolicitud >= SevenDaysAgo && obj.FhInicioSolicitud <= currentDate) ||
                    // or (finishing_date.day >= date_now.day-7 and finishing_date.day <= date_now.day)
                    (obj.FhFinalSolicitud >= SevenDaysAgo && obj.FhFinalSolicitud <= currentDate)));
                // If the amount is not 0:
                if (requestOpenAmountPerPerson_week > 0)
                    cantidadesAbiertasUltimaSemana.Add(new string[] { a.nombre, requestOpenAmountPerPerson_week.ToString() });


                // Populate the dictionary that stores the filtered "last week closed requests" data //
                // Get the amount of requests asigned to a certain technician
                var requestClosedAmountPerPerson_week = requestAmounts.Count((obj) =>
                    // where TecnicoId == empleadoid
                    obj.TecnicoId.ToString() == a.empleadoid &&
                    // and EstadoId == 2 (closed)
                    obj.EstadoId == 2 &&
                    // and (starting_date.day >= date_now.day-7 and starting_date.day <= date_now.day)
                    ((obj.FhInicioSolicitud >= SevenDaysAgo && obj.FhInicioSolicitud <= currentDate) ||
                    // or (finishing_date.day >= date_now.day-7 and finishing_date.day <= date_now.day)
                    (obj.FhFinalSolicitud >= SevenDaysAgo && obj.FhFinalSolicitud <= currentDate)));
                // If the amount is not 0:
                if (requestClosedAmountPerPerson_week > 0)
                    cantidadesCerradasUltimaSemana.Add(new string[] { a.nombre, requestClosedAmountPerPerson_week.ToString() });
            }

            // Cantidad de solicitudes pendientes:
            // select FhInicioSolicitud, FhFinalSolicitud from Sol_Registro_Solicitud where TecnicoId = _@ID_TECNICO and estadoid = 1;

            return View(new EstadisticasAsignadas
            {
                CantidadesAbiertas = cantidadesAbiertasUltimaSemana,
                CantidadesCerradas = cantidadesCerradasUltimaSemana
            });
        }
        [CustomAuthorize]
        [Authorize(Roles = "EA")]
        public ActionResult Gerencias()
        {
            if (Session["departamento"] == null)
                return Redirect("~/login");

            dbIntranet db = new dbIntranet();

            // Get all the request per management
            // select SolicitanteId from Sol_Registro_Solicitud
            // Get the department information from the employee
            // select departamentoid,departamento from VISTA_EMPLEADOS where empleadoid = _@_EMP_ID;
            var requiredInformation = from t in db.Sol_Registro_Solicitud
                                      join t2 in db.VISTA_EMPLEADOS
                                      on t.SolicitanteId.ToString() equals t2.empleadoid
                                      select new { t.SolicitanteId, t2.nombre, t2.departamentoid, t2.departamento, t.FhInicioSolicitud, t.FhFinalSolicitud };

            // Data required for the ViewModel
            var cantidades = new List<EstadisticasCantidades>();
            var años = new List<string>();
            var meses = new List<string>();
            var mesesPorAños = new Dictionary<string, string>();
            //var cantidadesPorMeses = new Dictionary<string, List<string[]>>();
            //var infoPorGerencia = new Dictionary<string, string[]>();
            //var infoPorUsuario = new Dictionary<int, string[]>();
            var infoGerenciaPorMes = new Dictionary<string, List<string[]>>();
            var infoUsuarioPorMes = new Dictionary<string, List<string[]>>();
            var listaGerenciasVerificadas = new List<string>();
            var listaUsuariosVerificados = new List<string>();

            foreach (var a in requiredInformation)
            {
                #region FILTER YEAR-MONTH
                // Add it to the list taking the starting date of the request as reference
                var temp_mes = a.FhInicioSolicitud.Month.ToString();
                if (!meses.Contains(temp_mes))
                    meses.Add(temp_mes);
                var temp_año = a.FhInicioSolicitud.Year.ToString();
                if (!años.Contains(temp_año))
                    años.Add(temp_año);
                if (mesesPorAños.ContainsKey(temp_mes))
                {
                    if (!mesesPorAños[temp_mes].Contains(temp_año))
                        mesesPorAños[temp_mes] += "|" + temp_año;
                }
                else
                    mesesPorAños.Add(temp_mes, temp_año);
                // Add it to the list taking the finishing date of the request as reference
                temp_mes = a.FhFinalSolicitud.Month.ToString();
                if (!meses.Contains(temp_mes))
                    meses.Add(temp_mes);
                temp_año = a.FhFinalSolicitud.Year.ToString();
                if (!años.Contains(temp_año))
                    años.Add(temp_año);
                if (mesesPorAños.ContainsKey(temp_mes))
                {
                    if (!mesesPorAños[temp_mes].Contains(temp_año))
                        mesesPorAños[temp_mes] += "|" + temp_año;
                }
                else
                    mesesPorAños.Add(temp_mes, temp_año);

                cantidades.Add(new EstadisticasCantidades
                {
                    //IdTecnico = a.empleadoid,
                    Nombre = a.nombre,
                    InicioSolicitud = a.FhInicioSolicitud,
                    FinalSolicitud = a.FhFinalSolicitud
                });
                #endregion

                foreach (var b in meses)
                {
                    // Get all the years that include a specific month
                    var year_group = mesesPorAños[b].Split('|');

                    foreach (var c in year_group)
                    {
                        if (!listaGerenciasVerificadas.Contains(c + "|" + b + "|" + a.departamento))
                        {
                            // Get the amount of requests a specific management does
                            var countManagement = requiredInformation
                                .Where((obj) =>
                                (obj.FhInicioSolicitud.Year.ToString() == c && obj.FhInicioSolicitud.Month.ToString() == b) ||
                                (obj.FhFinalSolicitud.Year.ToString() == c && obj.FhFinalSolicitud.Month.ToString() == b))
                                .Count((obj) =>
                                obj.departamentoid == a.departamentoid);
                            //infoPorGerencia.Add(a.departamento, new string[] { a.departamento, countManagement.ToString() });
                            listaGerenciasVerificadas.Add(c + "|" + b + "|" + a.departamento);

                            if (!infoGerenciaPorMes.ContainsKey(c + "|" + b))
                                infoGerenciaPorMes.Add(c + "|" + b, new List<string[]>());

                            infoGerenciaPorMes[c + "|" + b].Add(new string[] { a.departamento, countManagement.ToString() });
                        }

                        if (!listaUsuariosVerificados.Contains(c + "|" + b + "|" + a.SolicitanteId))
                        {
                            // Get the amount of requests a specific user does
                            var countRequirements = requiredInformation
                                .Where((obj) =>
                                (obj.FhInicioSolicitud.Year.ToString() == c && obj.FhInicioSolicitud.Month.ToString() == b) ||
                                (obj.FhFinalSolicitud.Year.ToString() == c && obj.FhFinalSolicitud.Month.ToString() == b))
                                .Count((obj) =>
                                obj.SolicitanteId == a.SolicitanteId);
                            //infoPorUsuario.Add(a.SolicitanteId, new string[] { a.nombre, countRequirements.ToString() });
                            listaUsuariosVerificados.Add(c + "|" + b + "|" + a.SolicitanteId);

                            if (!infoUsuarioPorMes.ContainsKey(c + "|" + b))
                                infoUsuarioPorMes.Add(c + "|" + b, new List<string[]>());

                            infoUsuarioPorMes[c + "|" + b].Add(new string[] { a.nombre, countRequirements.ToString() });
                        }
                    }
                }
            }

            return View(new EstadisticasViewModel
            {
                Años = años,
                Meses = meses,
                MesesPorAños = mesesPorAños,
                infoGerenciaPorMeses = infoGerenciaPorMes,
                infoUsusarioPorMeses = infoUsuarioPorMes,
                ListaCantidades = cantidades
            }

            );
        }

        [CustomAuthorize]
        [Authorize(Roles = "EA")]
        public ActionResult ValorHoras()
        {
            if (Session["departamento"] == null)
                return Redirect("~/login");

            //MostrarTiempoExcedido();
            //MostrarTiempoExcedidoMensual();

            string emp_id = (string)Session["empleadoId"];
            string dept_id = null;
            string dependency_id = null;

            dbIntranet db = new dbIntranet();
            EstadisticasViewModel smodel = new EstadisticasViewModel();

            // Get the department id required
            // select departamentoid,dependenciaid from VISTA_EMPLEADOS where empleadoid = _@ID_SUPERVISOR;
            var requiredIds = (from t in db.VISTA_EMPLEADOS
                               where t.empleadoid == emp_id
                               select new { t.departamentoid, t.dependenciaid }).First();

            dept_id = requiredIds.departamentoid;
            dependency_id = requiredIds.dependenciaid;

            // Get every employee per department
            // select empleadoid, nombre from VISTA_EMPLEADOS where departamentoid = @dept_id and dependenciaid = @dependencia_id and not empleadoid = _@ID_SUPERVISOR;
            var employees = from t in db.VISTA_EMPLEADOS
                            where t.departamentoid == dept_id && t.dependenciaid == dependency_id && t.empleadoid != emp_id
                            select new { t.empleadoid, t.nombre };

            var cantidades = new List<EstadisticasCantidades>();
            var años = new List<string>();
            var meses = new List<string>();
            var mesesPorAños = new Dictionary<string, string>();
            var cantidadesCerradasPorMeses = new Dictionary<string, List<string[]>>();
            var cantidadesCerradasUltimaSemana = new List<string[]>();
            var promedioCantidadesMeses = new Dictionary<string, List<string[]>>();
            var promedioCantidadesUtlimaSemana = new List<string[]>();
            var listaTiempoExc = new List<VistaTiempoRespuesta>();
            var temp_lista_tiempo_excedido = new List<VistaTiempoRespuesta>();
            var temp_lista_tiempo_excedido_mensual = new List<Vw_Tiempo_Excedido_Mensual>();
            var tecnico = new List<string>();
            var tiempo = new List<int>();
            var excedido = new Dictionary<int, string>();

            foreach (var a in employees)
            {
                var temp_empleado_id = Convert.ToInt32(a.empleadoid);
                var temp_nom_empl = a.nombre;
                // Closed requests:
                // select FhInicioSolicitud, FhFinalSolicitud from Sol_Registro_Solicitud where TecnicoId = _@ID_TECNICO and estadoid = 2;
                var requestAmounts = from t in db.Sol_Registro_Solicitud
                                     where t.TecnicoId == temp_empleado_id && t.EstadoId == 2
                                     select new { t.TecnicoId, t.FhInicioSolicitud, t.FhFinalSolicitud, t.Horas, t.FhModificacion };


                var requestEmpl = (from t in db.VistaTiempoRespuesta
                                   where t.TecnicoId == temp_empleado_id
                                   select new
                                   {

                                       Tecnico_tec = t.TecnicoId,
                                       Tiempo_tec = t.TiempoExcedido

                                   }).ToList().Select(x => new VistaTiempoRespuesta
                                   {
                                       TecnicoId = x.Tecnico_tec,
                                       TiempoExcedido = x.Tiempo_tec

                                   }).GroupBy(s => s.TecnicoId);


                foreach (var r in requestEmpl)
                {

                    foreach (var b in r)
                    {


                        var nombre_empl = temp_nom_empl;


                        listaTiempoExc.Add(new VistaTiempoRespuesta
                        {
                            TiempoExcedido = b.TiempoExcedido,
                            Nombre = nombre_empl


                        });
                        smodel.ListaTiempoExcedido = listaTiempoExc;
                    }

                }
                ///TIEMPO EXCEDIDO MENSUAL
                /// 

                var requestEmplTiempo = (from t in db.VistaTiempoExcedidoMensual
                                         where t.TecnicoId == temp_empleado_id
                                         select new
                                         {
                                             Tecnico_tec = t.TecnicoId,
                                             Tiempo_Exc_Mens = t.TiempoExcMensual

                                         }).ToList().Select(x => new Vw_Tiempo_Excedido_Mensual
                                         {
                                             TecnicoId = x.Tecnico_tec,
                                             TiempoExcMensual = x.Tiempo_Exc_Mens


                                         }).GroupBy(s => s.TecnicoId);

                foreach (var r in requestEmplTiempo)
                {


                    foreach (var b in r)
                    {

                        var temp_tecnico = a.nombre;
                        var temp_tecnicoId = b.TecnicoId;

                        if (!tecnico.Contains(temp_tecnico)) tecnico.Add(temp_tecnico);

                        var temp_mes_excd = b.TiempoExcMensual;

                        if (!tiempo.Contains(temp_mes_excd)) tiempo.Add(temp_mes_excd);


                        if (!excedido.ContainsKey(temp_tecnicoId)) excedido.Add(temp_tecnicoId, temp_tecnico + "|" + temp_mes_excd);



                        smodel.ListaTiempExcedMensual = excedido.Values.ToList();

                    }

                }


                foreach (var b in requestAmounts)
                {
                    // Add it to the list taking the starting date of the request as reference
                    var temp_mes = b.FhInicioSolicitud.Month.ToString();
                    if (!meses.Contains(temp_mes))
                        meses.Add(temp_mes);
                    var temp_año = b.FhInicioSolicitud.Year.ToString();

                    //var temp_modif = b.FhModificacion.Day.ToString();
                    //var temp_inicio = b.FhInicioSolicitud.Day.ToString();

                    //int temp_horas = b.Horas;
                    //int tempFechI = int.Parse(temp_inicio);
                    //int tempFechM = int.Parse(temp_año);
                    //int tempTiempModf = (tempFechM - tempFechI) / temp_horas;

                    //if (!fchModf.Contains(temp_inicio))
                    //    fchModf.Add(temp_inicio);

                    //if (!fchModf.Contains(temp_modif))
                    //    fchModf.Add(temp_modif);

                    if (!años.Contains(temp_año))
                        años.Add(temp_año);

                    if (!años.Contains(temp_año))
                        años.Add(temp_año);

                    if (!años.Contains(temp_año))
                        años.Add(temp_año);
                    if (mesesPorAños.ContainsKey(temp_mes))
                    {
                        if (!mesesPorAños[temp_mes].Contains(temp_año))
                            mesesPorAños[temp_mes] += "|" + temp_año;
                    }
                    else
                        mesesPorAños.Add(temp_mes, temp_año);
                    // Add it to the list taking the finishing date of the request as reference
                    temp_mes = b.FhFinalSolicitud.Month.ToString();
                    if (!meses.Contains(temp_mes))
                        meses.Add(temp_mes);
                    temp_año = b.FhFinalSolicitud.Year.ToString();
                    if (!años.Contains(temp_año))
                        años.Add(temp_año);
                    if (mesesPorAños.ContainsKey(temp_mes))
                    {
                        if (!mesesPorAños[temp_mes].Contains(temp_año))
                            mesesPorAños[temp_mes] += "|" + temp_año;
                    }
                    else
                        mesesPorAños.Add(temp_mes, temp_año);

                    cantidades.Add(new EstadisticasCantidades
                    {
                        IdTecnico = a.empleadoid,
                        Nombre = a.nombre,
                        InicioSolicitud = b.FhInicioSolicitud,
                        FinalSolicitud = b.FhFinalSolicitud,
                        //tiempoexc = tempTiempModf

                    });
                }


                // Populate the dictionary that stores the filtered "last week closed requests" data //
                var currentDate = DateTime.Now;
                var SevenDaysAgo = currentDate.AddDays(-7);


                // (obj.FhFinalSolicitud - obj.FhInicioSolicitud).Hours
                // 
                var cantidadHorasTrabajoUltimaSemana = requestAmounts.Where((obj) =>
                    obj.TecnicoId.ToString() == a.empleadoid &&
                   ((obj.FhInicioSolicitud >= SevenDaysAgo && obj.FhInicioSolicitud <= currentDate) ||
                    (obj.FhFinalSolicitud >= SevenDaysAgo && obj.FhFinalSolicitud <= currentDate)))
                    //.Select((obj) => new { a.nombre, Hours = System.Data.Entity.DbFunctions.DiffHours(obj.FhFinalSolicitud, obj.FhInicioSolicitud) });
                    .Select((obj) => new { a.nombre, Hours = System.Data.Entity.DbFunctions.DiffHours(obj.FhInicioSolicitud, obj.FhFinalSolicitud) }).Distinct();

                foreach (var b in cantidadHorasTrabajoUltimaSemana)
                    cantidadesCerradasUltimaSemana.Add(new string[] { b.nombre, b.Hours.ToString() });

                // Populate the dictionary that stores the filtered monthly data //
                foreach (var b in meses)
                {
                    // Get all the years that include a specific month
                    var year_group = mesesPorAños[b].Split('|');

                    foreach (var c in year_group)
                    {
                        // Create a key with the format ["year|month"]
                        if (!cantidadesCerradasPorMeses.ContainsKey(c + "|" + b))
                            cantidadesCerradasPorMeses.Add(c + "|" + b, new List<string[]>());

                        // 
                        var cantidadHorasTrabajoMeses = requestAmounts.Where((obj) =>
                            obj.TecnicoId.ToString() == a.empleadoid &&
                            ((obj.FhInicioSolicitud.Year.ToString() == c && obj.FhInicioSolicitud.Month.ToString() == b) ||
                            (obj.FhFinalSolicitud.Year.ToString() == c && obj.FhFinalSolicitud.Month.ToString() == b)))
                            .Select((obj) => new { a.nombre, Hours = System.Data.Entity.DbFunctions.DiffHours(obj.FhInicioSolicitud, obj.FhFinalSolicitud) }).Distinct();

                        // Add the values { technician_name, hour_amounts }
                        foreach (var d in cantidadHorasTrabajoMeses)
                            cantidadesCerradasPorMeses[c + "|" + b].Add(new string[] { d.nombre, d.Hours.ToString() });
                    }
                }

            }

            foreach (var b in cantidadesCerradasPorMeses.Keys)
            {
                promedioCantidadesMeses.Add(b, new List<string[]>());

                foreach (var c in employees)
                {
                    int count = cantidadesCerradasPorMeses[b].Count((obj) => obj[0] == c.nombre);

                    if (count == 0)
                        continue;

                    int sum = cantidadesCerradasPorMeses[b].Where((obj) => obj[0] == c.nombre).Sum((obj) => Convert.ToInt32(obj[1]));
                    double result = (double)sum / count;

                    promedioCantidadesMeses[b].Add(new string[] { c.nombre, result.ToString() });
                }
            }

            foreach (var b in cantidadesCerradasUltimaSemana)
                foreach (var c in employees)
                {
                    int count = cantidadesCerradasUltimaSemana.Count((obj) => obj[0] == c.nombre);

                    if (count == 0)
                        continue;

                    int sum = cantidadesCerradasUltimaSemana.Where((obj) => obj[0] == c.nombre).Sum((obj) => Convert.ToInt32(obj[1]));
                    double result = (double)sum / count;

                    promedioCantidadesUtlimaSemana.Add(new string[] { c.nombre, result.ToString() });
                }


            // var cantidades = new List<EstadisticasCantidades>();
            //var años = new List<string>();
            //var meses = new List<string>();
            //var mesesPorAños = new Dictionary<string, string>();
            //var cantidadesCerradasPorMeses = new Dictionary<string, List<string[]>>();
            //var cantidadesCerradasUltimaSemana = new List<string[]>();



            foreach (var a in smodel.ListaTiempoExcedido)
                // Verificar que entre los valores que contiene la lista no exista el valor actaul de la iteración
                if (temp_lista_tiempo_excedido.Where((obj) => obj.Nombre == a.Nombre)
                    // Si el valor existe: retorna un string vacío
                    // De lo contrario: retorna nulo
                    .Select(obj => "").FirstOrDefault() == null)
                    // Si el valor retornado es nulo: (es decir, que no existe en la lista)
                    // añado a mi lista lo que contiene la var a
                    temp_lista_tiempo_excedido.Add(new VistaTiempoRespuesta
                    {
                        Nombre = a.Nombre,
                        //Aqui se hace ua
                        TiempoExcedido = smodel.ListaTiempoExcedido.Where((obj) => obj.Nombre == a.Nombre).Sum((obj) => obj.TiempoExcedido) /
                            smodel.ListaTiempoExcedido.Where((obj) => obj.Nombre == a.Nombre).Count()
                    });



            return View(new EstadisticasViewModel
            {
                ListaCantidades = cantidades,
                Años = años,
                Meses = meses,
                MesesPorAños = mesesPorAños,
                CantidadesPorMeses = promedioCantidadesMeses,
                CantidadesUltimaSemana = promedioCantidadesUtlimaSemana,
                ListaTiempoExcedido = temp_lista_tiempo_excedido,
                ListaTiempExcedMensual = smodel.ListaTiempExcedMensual

            });


        }



    }
}



