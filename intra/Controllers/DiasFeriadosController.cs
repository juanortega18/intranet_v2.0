﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models.GestionHumana;
using intra.Models;
using intra.Code;
using System.Globalization;

namespace intra.Controllers
{
    public class DiasFeriadosController : Controller
    {
        private DFMPModel db = new DFMPModel();
        private const string salt = "HBK97548OIQPR";

        [CustomAuthorize]
        [Authorize(Roles ="RRHHA")]
        // GET: DiasFeriados
        public ActionResult Index()
        {
            var holydays = db.DiasFeriados.ToList();

            return View(holydays);
        }

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        // GET: DiasFeriados/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult DiasFeriados()
        {
            return View(db.DiasFeriados.ToList());
        }

        public ActionResult Feriados()
        {
            return View(db.DiasFeriados.ToList());
        }

        // POST: DiasFeriados/Create
        [HttpPost]
        public ActionResult Create(DiasFeriados view_object)
        {
            try
            {
                DateTime fecha_feriada;

                bool fecha_feriado_valida = DateTime.TryParseExact(Request.Form["DiaFeriadoFecha"], "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out fecha_feriada);

                if (fecha_feriado_valida)
                {
                    view_object.DiasFeriadosFecha = fecha_feriada;
                }
                else
                {
                    ModelState.Clear();
                    ModelState.AddModelError("DiasFeriadosFecha", "Fecha inváida");
                    return View(view_object);
                }

                if (!ModelState.IsValid)
                    return View(view_object);

                if (view_object.DiasFeriadosDescripcion == null || view_object.DiasFeriadosDescripcion == "")
                {
                    ModelState.AddModelError("DiasFeriadosDescripcion", "El campo no puede estar vacío");
                    return View(view_object);
                }

                //if(view_object.DiasFeriadosFecha < DateTime.Now)
                //{
                //    ModelState.AddModelError("DiasFeriadosFecha", "La fecha no puede ser anterior a la actual");
                //    return View(view_object);
                //}

                var is_in = db.DiasFeriados.Where(x => x.DiasFeriadosFecha == view_object.DiasFeriadosFecha).FirstOrDefault();

                if (is_in != null)
                {
                    ModelState.AddModelError("DiasFeriadosFecha", "Ya ha asignado un día feriado para esta fecha");
                    return View(view_object);
                }
                
                view_object.DiasFeriadosCodigoUsuario = HttpContext.Session["empleadoId"].ToString();
                view_object.DiasFeriadosNombreUsuario = Session["usuario"].ToString();
                view_object.DiasFeriadosDescripcion = view_object.DiasFeriadosDescripcion.ToUpper();


                db.DiasFeriados.Add(view_object);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch(Exception error)
            {
                var message = error.Message;

                return View();
            }
        }

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        // GET: DiasFeriados/Edit/5
        public ActionResult Edit(int id)
        {
            var holyday = db.DiasFeriados.Find(id);

            holyday.qken = UtilityMethods.SetSHA1(holyday.DiasFeriadosId + salt);

            return View(holyday);
        }

        // POST: DiasFeriados/Edit/5
        [HttpPost]
        public ActionResult Edit(DiasFeriados view_object)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(view_object);

                string real_salt = UtilityMethods.SetSHA1(view_object.DiasFeriadosId + salt);

                if (real_salt != view_object.qken)
                    return View(view_object);
                
                if (view_object.DiasFeriadosFecha == null)
                {
                    ModelState.AddModelError("DiasFeriadosFecha", "El campo no puede estar vacío");
                    return View(view_object);
                }

                if (view_object.DiasFeriadosDescripcion == null || view_object.DiasFeriadosDescripcion == "")
                {
                    ModelState.AddModelError("DiasFeriadosDescripcion", "El campo no puede estar vacío");
                    return View(view_object);
                }


                //if (view_object.DiasFeriadosFecha < DateTime.Now)
                //{
                //    ModelState.AddModelError("DiasFeriadosFecha", "La fecha no puede ser anterior a la actual");
                //    return View(view_object);
                //}

                view_object.DiasFeriadosDescripcion = view_object.DiasFeriadosDescripcion.ToUpper();

                db.Entry(view_object).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(view_object);
            }
        }

        [CustomAuthorize]
        // GET: DiasFeriados/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        [CustomAuthorize]
        // GET: DiasFeriados/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DiasFeriados/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
