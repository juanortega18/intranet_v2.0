﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using System.IO;
using intra.Models.GestionHumana;



using intra.Models;

namespace intra.Controllers
{
    public class DocumentosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        
        // GET: Documentos
        public ActionResult Index()
        {
            return View(db.Documentos.ToList());
        }

        [CustomAuthorize]
        // GET: Documentos/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Documentos documentos = db.Documentos.Find(id);
        //    if (documentos == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(documentos);
        //}

        // GET: Documentos/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Details()
        {
            string EmpId = string.Empty;
            EmpId = HttpContext.Session["empleadoId"].ToString();

            if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
            {
                RedirectToAction("Index", "Home");
            }

            
                var documentoSeguridad = db.CarpetasDocumentosGH.Where(x => x.CarpetaId == 16 || x.CarpetaNombre == "SEGURIDAD INFORMATICA").FirstOrDefault();
                var documentosGestionHumana = db.DocumentoGestionHumana.Where(x => x.CarpetaId == documentoSeguridad.CarpetaId).ToList();

            return View(documentosGestionHumana);
            }

       

        //POST: Documentos/Create
        //To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Documentos documentos)
        {
            if (ModelState.IsValid)
            {
                if (documentos.ArchivoVista != null)
                { 

                    using (MemoryStream ms = new MemoryStream())
                    {
                        documentos.ArchivoVista.InputStream.CopyTo(ms);
                        documentos.DocArchivo = ms.GetBuffer();
                    }
                }


                db.Documentos.Add(documentos);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(documentos);
        }
        

        public ActionResult VisualizarDocumento()
        {
            string EmpId = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                {
                    EmpId = HttpContext.Session["empleadoId"].ToString();
                }
                else
                {
                    EmpId = "public_user";
                }
            }
            catch (Exception)
            {
                EmpId = "public_user";
            }

            ViewBag.access = EmpId;

            return View();
        }
       

        // GET: Documentos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Documentos documentos = db.Documentos.Find(id);
            if (documentos == null)
            {
                return HttpNotFound();
            }
            return View(documentos);
        }

        // POST: Documentos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DocId,DocTitulo,DocArchivo")] Documentos documentos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(documentos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(documentos);
        }

        // GET: Documentos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Documentos documentos = db.Documentos.Find(id);
            if (documentos == null)
            {
                return HttpNotFound();
            }
            return View(documentos);
        }

        // POST: Documentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Documentos documentos = db.Documentos.Find(id);
            db.Documentos.Remove(documentos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
