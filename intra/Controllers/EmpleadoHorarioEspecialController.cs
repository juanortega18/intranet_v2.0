﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models;
using intra.Models.ViewModel;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
    public class EmpleadoHorarioEspecialController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: EmpleadoHorarioEspecial
        public ActionResult Index()
        {
            return View(db.EmpleadoHorarioEspecial.ToList());
        }

        [HttpPost]
        public JsonResult BuscarEmpleado(string Codigo)
        {
            int cod = int.Parse(Codigo.ToString());

            var empleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == cod.ToString()) .Select(x => new EmpleadosVM { EmpleadoId = x.empleadoid, Nombre = x.nombre }).FirstOrDefault();

            return Json(empleado);
        }

        // GET: EmpleadoHorarioEspecial/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EmpleadoHorarioEspecial/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        
      // public ActionResult Create( EmpleadoHorarioEspecial EmpleadoHorarioEspecial, FormCollection fc)
       public JsonResult Create(EmpleadoHorarioEspecial EmpleadoHorarioEspecial, FormCollection fc)
        {
           
            try {

          var empleado = db.EmpleadoHorarioEspecial.Find(EmpleadoHorarioEspecial.EmpleadoId);
                if (empleado != null)
                    {
                    
                        return Json(new { success = false, message="ya existe una persona con este codigo" });
                }
                else
                {

                        EmpleadoHorarioEspecial.EmpleadoId = int.Parse(fc["EmpleadoId"].ToString());
                        EmpleadoHorarioEspecial.EmpExeHoraEntrada = EmpleadoHorarioEspecial.EmpExeHoraEntrada;
                        EmpleadoHorarioEspecial.EmpExeHoraSalida = EmpleadoHorarioEspecial.EmpExeHoraSalida;
                        EmpleadoHorarioEspecial.EmpExeNombre = fc["NombresPersonas"];
                        db.EmpleadoHorarioEspecial.Add(EmpleadoHorarioEspecial);
                        db.SaveChanges();
                        return Json(new { success = true, message = "Datos Guardados Correctamente" });
                    }
               

            }
            catch(Exception e)
            {
                e.ToString();
                    return Json(new { success = false, message = "Error, No se pudo Agregar el Empleado" });
            }

            //return Json(EmpleadoHorarioEspecial);
        } 

        // GET: EmpleadoHorarioEspecial/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmpleadoHorarioEspecial empleadoHorarioEspecial = db.EmpleadoHorarioEspecial.Find(id);
            if (empleadoHorarioEspecial == null)
            {
                return HttpNotFound();
            }
            return View(empleadoHorarioEspecial);
        }

        // POST: EmpleadoHorarioEspecial/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(EmpleadoHorarioEspecial horarioEspecial)
        {
            try
            {
                var empleado = db.EmpleadoHorarioEspecial.Find(horarioEspecial.EmpleadoId);
                if (empleado != null)
                {
                    empleado.EmpExeHoraEntrada = horarioEspecial.EmpExeHoraEntrada;
                    empleado.EmpExeHoraSalida = horarioEspecial.EmpExeHoraSalida;
                    db.Entry(empleado).State = EntityState.Modified;
                    db.SaveChanges();

                }
                return Json(new { success = true, message = "Datos se actualizo Correctamente" });


            }
            catch (Exception ex)
            {
                ex.ToString();

                return Json(new { success = false, message = "Ha ocurrido un error al intentar editar" });


            }
        }

       

        // POST: EmpleadoHorarioEspecial/Delete/5
        [HttpPost]
        public JsonResult EliminarHorario(int? id)
        {
            try { 
            EmpleadoHorarioEspecial empleadoHorarioEspecial = db.EmpleadoHorarioEspecial.Find(id);
            db.EmpleadoHorarioEspecial.Remove(empleadoHorarioEspecial);
            db.SaveChanges();
            return Json(new { success = true, Mensaje = "Datos Eliminados Correctamente" });
            }catch (Exception ex)
            {
                ex.ToString();
                return Json(new { success = false, Mensaje = "Ha ocurrido un error al intentar eliminar el registro" });


            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
