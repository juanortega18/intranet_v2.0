﻿using intra.Models.TallerPgr;
using intra.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    [Authorize(Roles = "TA")]
    public class MarcaEquipoController : Controller
    {
        private TallerPgrDM db = new TallerPgrDM();
        // GET: MarcaEquipo
        public ActionResult Index()
        {
            var marcaequipo = db.MarcaEquipo.ToList();
            return View(marcaequipo);
        }

        // GET: MarcaEquipo/Create
        public ActionResult Create()
        {
            var marcaTipoViewModel = new MarcaTipoViewModel { ListaTipoEquipo = db.TipoEquipo.ToList(), TipoSelecionados = new List<int>() };
            return View(marcaTipoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(string[] TipoSelecionados)
        {
            var marca = new MarcaEquipo() { MarcaEquipoDescripcion = Request.Form["MarcaEquipo.MarcaEquipoDescripcion"].ToUpper() };

            var resultado = ValidarMarca(marca);

            if (resultado != "Valido") {
                ViewBag.error = resultado;
                UpdateMarcaTipos(TipoSelecionados, marca);
                var marcaTipoViewModel = new MarcaTipoViewModel { ListaTipoEquipo = db.TipoEquipo.ToList(), MarcaEquipo = marca };
                return View(marcaTipoViewModel);
            }

            var nuevamarca = new MarcaEquipo { MarcaEquipoDescripcion = marca.MarcaEquipoDescripcion };
            UpdateMarcaTipos(TipoSelecionados, nuevamarca);
            db.MarcaEquipo.Add(nuevamarca);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: MarcaEquipo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var marcaEquipoViewModel = new MarcaTipoViewModel { MarcaEquipo = db.MarcaEquipo.Find(id) };
            if (marcaEquipoViewModel.MarcaEquipo == null)
            {
                return HttpNotFound();
            }
            marcaEquipoViewModel.ListaTipoEquipo = db.TipoEquipo.ToList();
            return View(marcaEquipoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string[] TipoSelecionados)
        {
            int id = Convert.ToInt16(Request.Form["MarcaEquipo.MarcaEquipoId"]);

            var marcaresultado = db.MarcaEquipo.Include(t => t.TipoEquipo).Where(i => i.MarcaEquipoId == id).SingleOrDefault();

            if (marcaresultado == null) {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                if (TryUpdateModel(marcaresultado, "", new string[] { "Nombre" }))
                {
                    try
                    {
                        UpdateMarcaTipos(TipoSelecionados, marcaresultado);
                        marcaresultado.MarcaEquipoDescripcion = Request.Form["MarcaEquipo.MarcaEquipoDescripcion"].ToUpper();
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    catch (RetryLimitExceededException)
                    {
                        ModelState.AddModelError("", "No funciono.");
                    }
                }
            }
            return View();
        }

        // POST: MarcaEquipo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MarcaEquipo marcaEquipo = db.MarcaEquipo.Find(id);
            db.MarcaEquipo.Remove(marcaEquipo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public JsonResult AgregarMarcaModal(string[] TipoSelecionados, string marcaNombre)
        {
            MarcaEquipo marca = new MarcaEquipo() { MarcaEquipoDescripcion = marcaNombre.ToUpper() };

            var resultado = ValidarMarca(marca);

            if (resultado == "Valido") {
                UpdateMarcaTipos(TipoSelecionados, marca);
                db.MarcaEquipo.Add(marca);
                db.SaveChanges();
                return Json("Guardado");
            }

            return Json(resultado); 
        }

        private string ValidarMarca(MarcaEquipo marca)
        {
            if (marca.MarcaEquipoDescripcion == null || marca.MarcaEquipoDescripcion == "")
            { return "El nombre de la marca es obligatorio."; }
            else if (db.MarcaEquipo.Where(x => x.MarcaEquipoDescripcion == marca.MarcaEquipoDescripcion).FirstOrDefault() != null)
            { return "Esta marca ya esta creada."; }
            return "Valido";
        }

        private void UpdateMarcaTipos(string[] TipoSelecionados, MarcaEquipo Marca)
        {
            if (TipoSelecionados == null)
            {
                Marca.TipoEquipo = new List<TipoEquipo>();
                return;
            }

            var tiposelectionadosHS = new HashSet<string>(TipoSelecionados);
            var marcaTipos = new HashSet<int>(Marca.TipoEquipo.Select(t => t.TipoEquipoId));

            foreach (var item in db.TipoEquipo)
            {
                if (tiposelectionadosHS.Contains(item.TipoEquipoId.ToString()))
                {
                    if (!marcaTipos.Contains(item.TipoEquipoId))
                    {
                        Marca.TipoEquipo.Add(item);
                    }
                }
                else
                {
                    if (marcaTipos.Contains(item.TipoEquipoId))
                    {
                        Marca.TipoEquipo.Remove(item);
                    }
                }
            }
        }

        [HttpPost]
        public JsonResult CargarMarcas(int? id)
        {
            //if (id == null)
            //    return Json(db.MarcaEquipo.Select(x=> new {
            //                Value = x.MarcaEquipoId,
            //                Text = x.MarcaEquipoDescripcion
            //    }).ToList());

            var marcas = db.TipoEquipo.Find(id).MarcaEquipo.OrderBy(a => a.MarcaEquipoId).Select(x => new {
                Value = x.MarcaEquipoId,
                Text = x.MarcaEquipoDescripcion
            }).ToList();

            return Json(marcas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}