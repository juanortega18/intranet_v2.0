﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.GestionHumana.ConcursoMadres;
using System.IO;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.Entity.SqlServer;

namespace intra.Controllers.ConcursoMadres
{
    public class RH_ConcursoMadresController : Controller
    {
        private dbIntranet db = new dbIntranet();
        public Correo ObCorreo = new Correo();

        //CONCURSO DIA DE LAS MADRES
        #region ObtenerEtiquetaHTML
            [AllowAnonymous]
        public string DescripcionCorreoConcursoMadres(string Link)
        {

            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/ConcursoMadres.html");


            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", "CONCURSO DIA DE LAS MADRES");
            //Archivo = Archivo.Replace("$departamento", Departamento);
            //Archivo = Archivo.Replace("$nombres", NombreCompleto);
            // Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            return Archivo;
        }
        #endregion


        [AllowAnonymous]

        #region EncuestaCorreo
        public void ConcursoMadreCorreo(string Participante, string Sujeto, string Descripcion)
        {
            // string PrimerCorreo = "";

            string Correo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            Correo = Participante;

            //ObjCorreo.EnviarCorreo(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion);

            ObCorreo.EncuestasCorreo(Correo, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
        }
        #endregion

        [AllowAnonymous]

        #region EncuestaCorreo
        public void CancelarConcursoMadreCorreo(string Participante, string Concursante, string Sujeto, string Descripcion)
        {
            // string PrimerCorreo = "";

            string Correo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            Correo = Participante;

            //ObjCorreo.EnviarCorreo(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion);

            ObCorreo.EnviarCorreoConcursoMadres(Correo, Concursante, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
            ObCorreo.EnviarCorreoConcursoMadres(Concursante, Correo,  Sujeto, Descripcion, Logo, Flecha, LogoDTI);
        }
        #endregion

        [HttpPost]
        public JsonResult AprobacionConcursante(int idConcursante)
        {
            try { 
            var aprobacion = db.RH_ConcursoMadres.Find(idConcursante);
            aprobacion.EstadoConcursoId = 1;
                db.SaveChanges();


                var ConcursoMadres = db.RH_ConcursoMadres.Where(x => x.ConcursanteId == idConcursante).FirstOrDefault();
                string ConcursoId = ConcursoMadres.EmpleadoId.ToString();
                var SupervisorSugerencia = db.RH_SugerenciaSupervisor.Where(x => x.Estado == true).FirstOrDefault();


                string CorreoUsuario = Code.Utilities2.obtenerSp_UserPorCodigo(ConcursoId)?.mail;

                string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RH_ConcursoMadres/ListaConcursantes/";

                ConcursoMadreCorreo(CorreoUsuario, "CONCURSO MADRE", DescripcionCorreoConcursoMadres(url));



                return Json(new { success = true, Mensaje = "Participante aprobada correctamente" }, JsonRequestBehavior.AllowGet);

            }
            catch(Exception e)
            {
                e.ToString();
                return Json(new { success = false, Mensaje = "Ha ocurrido un error, favor verificar" }, JsonRequestBehavior.AllowGet);

            }

        }

        //Votacion Solicitud Participante
        [HttpPost]
        public JsonResult VotacionConcursante(int idConcursante, string EstadoId, int UsuarioId, RH_VotosMadres Votos, FormCollection fc)
        {


            try
            {
                var votaciones = db.RH_VotosMadres.Where(x => x.ConcursanteId == idConcursante && x.UsuarioId == UsuarioId).FirstOrDefault();
                if(votaciones != null)
                {
                    votaciones.VotoConteo = 1;
                    db.SaveChanges();
                    var Conteomadres = db.RH_ConcursoMadres.Where(x => x.ConcursanteId == votaciones.ConcursanteId && UsuarioId == votaciones.UsuarioId).FirstOrDefault();
                    Conteomadres.ConteoFinal = Conteomadres.ConteoFinal + 1;
                    db.SaveChanges();
                    return Json(new { success = true, Mensaje = "Votacion actualizada correctamente" }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    Votos.UsuarioId = UsuarioId;
                    Votos.VotoConteo = 1;
                    Votos.ConcursanteId = idConcursante;
                    db.RH_VotosMadres.Add(Votos);
                    db.SaveChanges();
                    var Conteomadres = db.RH_ConcursoMadres.Where(x => x.ConcursanteId == votaciones.ConcursanteId && UsuarioId == votaciones.UsuarioId).FirstOrDefault();
                    Conteomadres.ConteoFinal = Conteomadres.ConteoFinal + 1;
                    db.SaveChanges();
                    return Json(new { success = true, Mensaje = "Votacion Validada correctamente" }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception e)
            {
                e.ToString();
                return Json(new { success = false, Mensaje = "Ha ocurrido un error, favor verificar" }, JsonRequestBehavior.AllowGet);
            }

        }


        //Cancelar Votacion Solicitud Participante
        [HttpPost]
        public JsonResult CancelarVotacionConcursante(int idConcursante, string EstadoId, int UsuarioId, RH_VotosMadres Votos, FormCollection fc)
        {
            try
            {
                var votaciones = db.RH_VotosMadres.Where(x => x.ConcursanteId == idConcursante && x.UsuarioId == UsuarioId ).FirstOrDefault();
                if (votaciones != null)
                {

                    votaciones.VotoConteo = 0;
                    db.SaveChanges();
                    var Conteomadres = db.RH_ConcursoMadres.Where(x => x.ConcursanteId == votaciones.ConcursanteId && UsuarioId == votaciones.UsuarioId).FirstOrDefault();
                    Conteomadres.ConteoFinal = Conteomadres.ConteoFinal - 1;
                    db.SaveChanges();

                    return Json(new { success = true, Mensaje = "Votacion cancelada correctamente" }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    Votos.UsuarioId = UsuarioId;
                    Votos.VotoConteo = 0;
                    Votos.ConcursanteId = idConcursante;
                    db.RH_VotosMadres.Add(Votos);
                    db.SaveChanges();
                    var Conteomadres = db.RH_ConcursoMadres.Where(x => x.ConcursanteId == votaciones.ConcursanteId && UsuarioId == votaciones.UsuarioId).FirstOrDefault();
                    Conteomadres.ConteoFinal = Conteomadres.ConteoFinal - 1;
                    db.SaveChanges();

                    return Json(new { success = true, Mensaje = "Votacion cancelada correctamente" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                e.ToString();
                return Json(new { success = false, Mensaje = "Ha ocurrido un error, favor verificar" }, JsonRequestBehavior.AllowGet);

            }

        }



        //Cancelacion Solicitud Participante
        [HttpPost]
        public JsonResult CancelacionConcursante(int idConcursante, string motivo)
        {
            if (string.IsNullOrEmpty(motivo))
            {
                return Json(new { success = false, Mensaje = "Debe indicar el motivo de la cancelacion" }, JsonRequestBehavior.AllowGet);

            }

            try
            {
                var cancelacion = db.RH_ConcursoMadres.Find(idConcursante);
                cancelacion.EstadoConcursoId = 2;
                cancelacion.MotivoCancelacion = motivo;
                db.SaveChanges();

                var ConcursoMadres = db.RH_ConcursoMadres.Where(x=> x.ConcursanteId == idConcursante).FirstOrDefault();
                int ConcursoId = ConcursoMadres.EmpleadoId;
               
                var SupervisorSugerencia = db.RH_SugerenciaSupervisor.Where(x => x.Estado == true).FirstOrDefault();


                string CorreoUsuario = Code.Utilities2.obtenerSp_UserPorCodigo(SupervisorSugerencia.EmpleadoID.ToString())?.mail;
                string Concursante = Code.Utilities2.obtenerSp_UserPorCodigo("19592")?.mail;

                string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RH_ConcursoMadres/Details?id={idConcursante}";

                CancelarConcursoMadreCorreo(CorreoUsuario, Concursante, "CONCURSO MADRE", DescripcionCorreoConcursoMadres(url));


                return Json(new { success = true, Mensaje = "Solicitud cancelada correctamente" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                e.ToString();
                return Json(new { success = false, Mensaje = "Ha ocurrido un error, favor verificar" }, JsonRequestBehavior.AllowGet);

            }

        }
        public ActionResult Concurso()
        {

            return View();

        }

        // GET: RH_ConcursoMadres

        public ActionResult ListaConcursantes()
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }
            ViewBag.dependencias = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");
            int usuario = int.Parse(Session["empleadoId"].ToString());

            var Participantes = db.RH_ConcursoMadres.Where(x=> x.EstadoConcursoId == 1)
                .OrderByDescending(x=> x.ConteoFinal).ToList();


            return View(Participantes);
        }

        [HttpPost]
        public ActionResult ListaConcursantes(FormCollection fc)
        {
            string filtro = fc["filtro"];
            string nombre1 = fc["Busqueda"];
            string dependencias = fc["dependencias"];
            int dependenciaId = 0;
            ViewBag.dependencias = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");

            if (fc["dependencias"] != "")
            {
                dependenciaId = int.Parse(fc["dependencias"]);

            }
                string nombre = nombre1.ToUpper();
            var Participante = db.RH_ConcursoMadres.Where(x => x.EstadoConcursoId == 1).ToList();
            var dependencia = db.vw_Dependencias.Where(x => x.Descripcion == nombre).FirstOrDefault();
            

            if (!string.IsNullOrEmpty(nombre) && filtro == "1") {

               Participante = db.RH_ConcursoMadres.Where(x => x.EstadoConcursoId == 1 && x.Nombres.ToUpper().Contains(nombre.ToUpper())).ToList();
                return View(Participante);
                   }
                else if(dependenciaId != 0 && filtro == "2")
                {
                    Participante = db.RH_ConcursoMadres.Where(x => x.EstadoConcursoId == 1 &&  x.Dependencia == dependenciaId).ToList();
                return View(Participante);

            }               
            
            else
            {
                Participante = db.RH_ConcursoMadres.Where(x => x.EstadoConcursoId == 1).ToList();

            }
            return View(Participante);
        }

        public ActionResult ListaConcursoMadres()
        {

            var ConcursoMadres = db.RH_ConcursoMadres.Where(x => x.EstadoConcursoId == 3).OrderByDescending(x => x.ConcursanteId).ToList();
            return View(ConcursoMadres);
        }

        [HttpPost]
        public ActionResult ListaConcursoMadres(FormCollection fc)
        {
          
            string nombre1 = fc["Busqueda"];
            string nombre = nombre1.ToUpper();
            if (!string.IsNullOrEmpty(nombre)) {

                var Participante = db.RH_ConcursoMadres.Where(x => x.EstadoConcursoId == 3 && x.Nombres.ToUpper().Contains(nombre.ToUpper())).ToList();

                return View(Participante);
            }
            
           
            else {
                var Participante1 = db.RH_ConcursoMadres.Where(x => x.EstadoConcursoId == 3).ToList();

                return View(Participante1);
            }


        }

        [HttpPost]
        public JsonResult BuscarCedula(string cedula)
        {
            var consulta = db.VISTA_EMPLEADOS.Where(x => x.cedula == cedula).FirstOrDefault();

            if (consulta == null)
            {
                return Json(new { success = false, Mensaje = "Debe introducir una cedula valida" }, JsonRequestBehavior.AllowGet);

            }

            return Json(consulta);
        }

        
        // GET: RH_ConcursoMadres/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RH_ConcursoMadres rH_ConcursoMadres = db.RH_ConcursoMadres.Find(id);
            if (rH_ConcursoMadres == null)
            {
                return HttpNotFound();
            }
            return View(rH_ConcursoMadres);
        }

        public ActionResult DetalleParticipante(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RH_ConcursoMadres Concurso = db.RH_ConcursoMadres.Find(id);

            var Dependencia = db.vw_Dependencias.Where(x => x.DependenciaID == Concurso.Dependencia).FirstOrDefault();
            ViewBag.Dependencia = Dependencia.Descripcion;

            var departamento = db.vw_Departamentos.Where(x => x.DeptoID == Concurso.Departamento).FirstOrDefault();
            ViewBag.Departamento = departamento.Descripcion;

            if (Concurso == null)
            {
                return HttpNotFound();
            }
            return View(Concurso);
        }



        // GET: RH_ConcursoMadres/Create
        public ActionResult CrearConcursante()
        {
            ViewBag.EstadoConcursoId = new SelectList(db.RH_EstadoConcursoMadres, "EstadoConcursoId", "EstadoConcursoDescripcion");
            return View();
        }

        // POST: RH_ConcursoMadres/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult CrearConcursante(RH_ConcursoMadres rH_ConcursoMadres, FormCollection fc, HttpPostedFileBase file)
        {

            string Identificacion = fc["Cedula"];
            string Cedula = Identificacion.Replace("-", "");
         
            var concurso = db.RH_ConcursoMadres.Where(x => x.Cedula == Identificacion).FirstOrDefault();

            if(concurso != null && concurso.EstadoConcursoId == 3)
            {
                return Json(new { success = false, Mensaje = "Usted ya se registro en el concurso, debe esperar que su solicitud sea evaluada" }, JsonRequestBehavior.AllowGet);

            }

            if (concurso != null && concurso.EstadoConcursoId == 2)
            {
                concurso.EstadoConcursoId = 3;
                db.SaveChanges();
                return Json(new { success = true, Mensaje = "Usted sera evaluada nuevamente" }, JsonRequestBehavior.AllowGet);

            }

            try
            {
               if(string.IsNullOrEmpty(fc["Nombre1"]))
                    {
                    return Json(new { success = false, Mensaje = "El Nombre es Requerido" }, JsonRequestBehavior.AllowGet);

                     }
                if (string.IsNullOrEmpty(fc["CantidadHijos"]))
                {
                    return Json(new { success = false, Mensaje = "La cantidad de hijos es requerida" }, JsonRequestBehavior.AllowGet);

                }
                if (string.IsNullOrEmpty(fc["NumeroTelefonos"]))
                {
                    return Json(new { success = false, Mensaje = "El telefono es requerido" }, JsonRequestBehavior.AllowGet);

                }
                if (string.IsNullOrEmpty(fc["Correo"]))
                {
                    return Json(new { success = false, Mensaje = "El correo es requerido" }, JsonRequestBehavior.AllowGet);

                }
                if (string.IsNullOrEmpty(fc["Descripcion"]))
                {
                    return Json(new { success = false, Mensaje = "Debe Poner una frase o una nota que la describa" }, JsonRequestBehavior.AllowGet);
                }
                if (file == null && file.ContentLength < 0)
                {
                    return Json(new { success = false, Mensaje = "Debe agregar una imagen" }, JsonRequestBehavior.AllowGet);
                }

                rH_ConcursoMadres.Nombres = fc["Nombre1"];
                rH_ConcursoMadres.CantidadHijos = int.Parse(fc["CantidadHijos"]);
                rH_ConcursoMadres.Dependencia = int.Parse(fc["DependenciaId"]);
                rH_ConcursoMadres.Departamento = int.Parse(fc["DepartamentoId"]);
                rH_ConcursoMadres.EmpleadoId = int.Parse(fc["Empleado"]);
                rH_ConcursoMadres.NumeroTelefono = fc["NumeroTelefonos"];
                rH_ConcursoMadres.Correo = fc["Correo"];
                rH_ConcursoMadres.Descripcion = fc["Descripcion"];
                rH_ConcursoMadres.FechaCreacion = DateTime.Now;
                rH_ConcursoMadres.Año = DateTime.Now.Year;
                rH_ConcursoMadres.ConteoFinal = 0;
                rH_ConcursoMadres.EstadoConcursoId = 3;

                if (file != null && file.ContentLength > 0)

                {
                    var archivo = Request.Files[0];

                    string directory = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/GH_IMAGES/";
                    string extension = file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                    string NombreImagen = file.FileName.Replace(file.FileName, Cedula + "." + extension);
                    string path = Path.Combine(Server.MapPath(directory), Path.GetFileName(NombreImagen));
                    file.SaveAs(path);
                    rH_ConcursoMadres.Imagen = directory + NombreImagen;
                }
                else
                {
                    return Json(new { success = false, Mensaje = "Debe agregar una imagen para el concurso" }, JsonRequestBehavior.AllowGet);
                }


                if (string.IsNullOrEmpty(rH_ConcursoMadres.Imagen))
                {
                    return Json(new { success = false, Mensaje = "Debe agregar una imagen para el concurso" }, JsonRequestBehavior.AllowGet);
                }

                db.RH_ConcursoMadres.Add(rH_ConcursoMadres);
                db.SaveChanges();

                var ConcursoMadres = db.RH_ConcursoMadres.OrderByDescending(x => x.ConcursanteId).FirstOrDefault();
                int ConcursoId = ConcursoMadres.ConcursanteId;
                var SupervisorSugerencia = db.RH_SugerenciaSupervisor.Where(x => x.Estado == true).FirstOrDefault();
                

               string CorreoUsuario = Code.Utilities2.obtenerSp_UserPorCodigo(SupervisorSugerencia.EmpleadoID.ToString())?.mail; 

                string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RH_ConcursoMadres/DetalleParticipante?id={ConcursoId}";

                ConcursoMadreCorreo(CorreoUsuario, "CONCURSO MADRE", DescripcionCorreoConcursoMadres(url));

                return Json(new { success = true, Mensaje = "Gracias por su interes de participar en nuestro concurso, verificaremos si su perfil cumple con los requisitos necesarios" }, JsonRequestBehavior.AllowGet);

                //return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ex.ToString();
                return Json(new { success = false, Mensaje = "Ha ocurrido un error, Favor Verificar" }, JsonRequestBehavior.AllowGet);

            }


            //ViewBag.EstadoConcursoId = new SelectList(db.RH_EstadoConcursoMadres, "EstadoConcursoId", "EstadoConcursoDescripcion", rH_ConcursoMadres.EstadoConcursoId);
          // return View(rH_ConcursoMadres);
        }

        // GET: RH_ConcursoMadres/Edit/5
        public ActionResult EditarConcursante(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RH_ConcursoMadres rH_ConcursoMadres = db.RH_ConcursoMadres.Find(id);
            if (rH_ConcursoMadres == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadoConcursoId = new SelectList(db.RH_EstadoConcursoMadres, "EstadoConcursoId", "EstadoConcursoDescripcion", rH_ConcursoMadres.EstadoConcursoId);
            return View(rH_ConcursoMadres);
        }

        // POST: RH_ConcursoMadres/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarConcursante([Bind(Include = "ConcursanteId,Cedula,EmpleadoId,Nombres,CantidadHijos,Dependencia,Departamento,NumeroTelefono,Correo,Descripcion,Imagen,EstadoConcursoId,FechaCreacion,Año")] RH_ConcursoMadres rH_ConcursoMadres)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rH_ConcursoMadres).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadoConcursoId = new SelectList(db.RH_EstadoConcursoMadres, "EstadoConcursoId", "EstadoConcursoDescripcion", rH_ConcursoMadres.EstadoConcursoId);
            return View(rH_ConcursoMadres);
        }

        // GET: RH_ConcursoMadres/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RH_ConcursoMadres rH_ConcursoMadres = db.RH_ConcursoMadres.Find(id);
            if (rH_ConcursoMadres == null)
            {
                return HttpNotFound();
            }
            return View(rH_ConcursoMadres);
        }

        // POST: RH_ConcursoMadres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RH_ConcursoMadres rH_ConcursoMadres = db.RH_ConcursoMadres.Find(id);
            db.RH_ConcursoMadres.Remove(rH_ConcursoMadres);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
