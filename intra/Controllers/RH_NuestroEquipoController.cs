﻿using intra.Models.GestionHumana.NuestroEquipo;
using intra.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    public class RH_NuestroEquipoController : Controller
    {

        private dbIntranet db = new dbIntranet();

        // GET: NuestroEquipo
        public ActionResult Index()
        {
            var nuestro_equipo = new NuestroEquipoVM();

            nuestro_equipo.NuestroEquipo = db.RH_NuestroEquipo.FirstOrDefault();

            var NuestroEquipoDepartamento = db.RH_NuestroEquipoDepartamento.ToList();

            var NuestroEquipoMiembroID = db.RH_NuestroEquipoMiembro.Select(x => x.EmpleadoID.ToString()).ToList();

            var empleados = db.VISTA_EMPLEADOS.ToList();

            var empleados_filtrados = empleados.Where(x => NuestroEquipoMiembroID.Contains(x.empleadoid)).ToList();

            ViewBag.EmpleadoID = new SelectList(empleados, "empleadoid", "nombre").ToList();

            NuestroEquipoDepartamento.ForEach(x => x.NuestroEquipoMiembro.ForEach(y => y.empleado = empleados.Where(z => z.empleadoid == y.EmpleadoID.ToString()).FirstOrDefault()));

            foreach (var departamento in NuestroEquipoDepartamento)
            {

                foreach (var miembro in departamento.NuestroEquipoMiembro)
                {

                    miembro.empleado = empleados.FirstOrDefault(x => x.empleadoid == miembro.EmpleadoID.ToString());
                }
            }

            nuestro_equipo.NuestroEquipoDepartamentos = NuestroEquipoDepartamento;

            return View(nuestro_equipo);
        }

        public ActionResult Index2() {

            var nuestro_equipo = new NuestroEquipoVM();

            nuestro_equipo.NuestroEquipo = db.RH_NuestroEquipo.FirstOrDefault();

            var NuestroEquipoDepartamento = db.RH_NuestroEquipoDepartamento.ToList();

            var empleadosID = db.RH_NuestroEquipoMiembro.Select(x => x.EmpleadoID.ToString()).ToList();

            var empleados = db.VISTA_EMPLEADOS.Where(x => empleadosID.Contains(x.empleadoid)).ToList();

            NuestroEquipoDepartamento.ForEach(x => x.NuestroEquipoMiembro.ForEach(y => y.empleado = empleados.Where(z => z.empleadoid == y.EmpleadoID.ToString()).FirstOrDefault()));


            foreach (var departamento in NuestroEquipoDepartamento)
            {

                foreach (var miembro in departamento.NuestroEquipoMiembro)
                {

                    miembro.empleado = empleados.FirstOrDefault(x => x.empleadoid == miembro.EmpleadoID.ToString());
                }
            }

            nuestro_equipo.NuestroEquipoDepartamentos = NuestroEquipoDepartamento;

            return View(nuestro_equipo);
        }

        [Authorize(Roles = "NE")]
        public JsonResult ActualizarNuestroEquipo(RH_NuestroEquipo NuestroEquipo, FormCollection fc, HttpPostedFileBase file) {

            try
            {
                var NuestroEquipoDB = db.RH_NuestroEquipo.ToList().FirstOrDefault();

                if (file != null && file.ContentLength > 0)
                {
                    string path = Path.Combine(Server.MapPath(HttpRuntime.AppDomainAppVirtualPath + "/Content/images/GH_IMAGES/NuestroEquipoFotos"),
                        Path.GetFileName(file.FileName));

                    file.SaveAs(path);

                    NuestroEquipoDB.NuestroEquipoFoto = file.FileName;
                }

                NuestroEquipoDB.NuestroEquipoDescripcion = fc["NuestroEquipoDescripcion"];
                NuestroEquipoDB.NuestroEquipoFhModificacion = DateTime.Now;

                db.SaveChanges();

                return Json("Actualizado");
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json("Error");
            }
        }

        [Authorize(Roles = "NE")]
        public JsonResult AgregarDepartamento(RH_NuestroEquipoDepartamento NuestroEquipoDepartamento)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(NuestroEquipoDepartamento.Descripcion))
                    return Json("campo vacio");

                var departamentoDuplicado = db.RH_NuestroEquipoDepartamento.FirstOrDefault(x => x.Descripcion.ToUpper() == NuestroEquipoDepartamento.Descripcion.ToUpper());

                if (departamentoDuplicado != null)
                {
                    if (departamentoDuplicado.Estado) return Json("duplicado");

                    departamentoDuplicado.Estado = true;
                }
                else {

                    NuestroEquipoDepartamento.Estado = true;
                    NuestroEquipoDepartamento.FhCreacion = DateTime.Now;

                    db.RH_NuestroEquipoDepartamento.Add(NuestroEquipoDepartamento);
                }

                db.SaveChanges();

                return Json("Actualizado");
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json("Error");
            }
        }

        [Authorize(Roles = "NE")]
        public JsonResult ActualizarDepartamento(RH_NuestroEquipoDepartamento NuestroEquipoDepartamento)
        {
            try
            {
                var departamento = db.RH_NuestroEquipoDepartamento.Find(NuestroEquipoDepartamento.NuestroEquipoDepartamentoID);

                if (string.IsNullOrWhiteSpace(NuestroEquipoDepartamento.Descripcion))
                    return Json("campo vacio");

                var departamentoDuplicado = db.RH_NuestroEquipoDepartamento.FirstOrDefault(x => x.Descripcion.ToUpper() == NuestroEquipoDepartamento.Descripcion.ToUpper()
                && x.NuestroEquipoDepartamentoID != departamento.NuestroEquipoDepartamentoID);

                if (departamentoDuplicado != null) {

                    if(departamentoDuplicado.Estado) return Json("duplicado");

                    departamento.NuestroEquipoMiembro.ForEach(x => x.NuestroEquipoDepartamentoID = departamentoDuplicado.NuestroEquipoDepartamentoID);

                    departamento.Estado = false;

                    departamentoDuplicado.Estado = true;

                }
                else {
                    departamento.Descripcion = NuestroEquipoDepartamento.Descripcion;
                }

                db.SaveChanges();

                return Json(new { success = departamentoDuplicado != null ? "intercambiado" : "Actualizado", nombre_nuevo  = departamento.Descripcion });
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json("error");
            }
        }

        [Authorize(Roles = "NE")]
        public JsonResult AgregarMiembro(RH_NuestroEquipoMiembro NuestroEquipoMiembro)
        {

            try
            {
                NuestroEquipoMiembro.NuestroEquipoDepartamentoID = int.Parse(Request.Form["NuestroEquipo_DepartamentoID"]);

                if (NuestroEquipoMiembro.EmpleadoID==0)
                    return Json("campo vacio");

                var miembroDuplicado = db.RH_NuestroEquipoMiembro.FirstOrDefault(x => x.EmpleadoID == NuestroEquipoMiembro.EmpleadoID &&
                    x.NuestroEquipoDepartamentoID == NuestroEquipoMiembro.NuestroEquipoDepartamentoID);

                if (miembroDuplicado != null)
                {
                    if(miembroDuplicado.Estado) return Json("duplicado");

                    miembroDuplicado.Estado = true;
                }
                else {

                    NuestroEquipoMiembro.Estado = true;
                    NuestroEquipoMiembro.FhCreacion = DateTime.Now;

                    db.RH_NuestroEquipoMiembro.Add(NuestroEquipoMiembro);
                }

                db.SaveChanges();

                return Json("Actualizado");
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json("Error");
            }
        }

        [Authorize(Roles = "NE")]
        public JsonResult ActualizarMiembro(RH_NuestroEquipoMiembro NuestroEquipoMiembro)
        {
            try
            {
                var miembro = db.RH_NuestroEquipoMiembro.Find(NuestroEquipoMiembro.MiembroID);

                if (db.RH_NuestroEquipoMiembro.Any(x => x.EmpleadoID == NuestroEquipoMiembro.EmpleadoID &&
                    x.NuestroEquipoDepartamentoID == NuestroEquipoMiembro.NuestroEquipoDepartamentoID &&
                    x.MiembroID != miembro.MiembroID
                )) return Json("duplicado");

                miembro.Ext = NuestroEquipoMiembro.Ext;
                miembro.Flota = NuestroEquipoMiembro.Flota;

                db.SaveChanges();

                return Json("Actualizado");
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json("Error");
            }
        }

        [Authorize(Roles = "NE")]
        public JsonResult CambiarEstadoDepartamento(int DepartamentoId) {

            try
            {
                var departamento = db.RH_NuestroEquipoDepartamento.Find(DepartamentoId);

                departamento.Estado = !departamento.Estado;

                if(!departamento.Estado) departamento.NuestroEquipoMiembro.ForEach(x => x.Estado = false);

                db.SaveChanges();

                return Json(departamento.Estado ? "Activo" : "Inactivo");

            }
            catch (Exception)
            {
                return Json("error");
            }
        }

        [Authorize(Roles = "NE")]
        public JsonResult CambiarEstadoMiembro(int MiembroID)
        {
            try
            {
                var miembro = db.RH_NuestroEquipoMiembro.Find(MiembroID);

                miembro.Estado = !miembro.Estado;

                db.SaveChanges();

                return Json(miembro.Estado ? "Activo" : "Inactivo");

            }
            catch (Exception)
            {
                return Json("error");
            }
        }
    }
}