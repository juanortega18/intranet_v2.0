﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    public class DependenciasRegionalesController : Controller
    {
        dbIntranet db = new dbIntranet();

        //PONER ATRIBUTO Authorize

        [OutputCache(Duration = 0)]
        public ActionResult Index()
        {
            var lista = new List<Sol_DependenciasRegionales>();
            var data = new List<Sol_DependenciasRegionales>();
            data = db.Sol_DependenciasRegionales.Where(x => x.DependenciaEstado == true).ToList();
            foreach (var item in data)
            {
                item.supervisor = db.VISTA_EMPLEADOS.FirstOrDefault(x => x.empleadoid == item.DependenciaSupervisorId.ToString()).nombre;
                lista.Add(item);
            }
            return View(lista);
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Sol_DependenciasRegionales Sol_DependenciasRegionales)
        {
            Sol_DependenciasRegionales.DependenciaEstado = true;
            Sol_DependenciasRegionales.DependenciaFhCreacion = DateTime.Now;
            Sol_DependenciasRegionales.DependenciaUsuarioCreador = HttpContext.Session["usuario"].ToString();
            if (ModelState.IsValid)
            {
                db.Sol_DependenciasRegionales.Add(Sol_DependenciasRegionales);
                db.SaveChanges();
                TempData["toastrMessage"] = "Guardado Correctamente!";
                return RedirectToAction("Index");
            }
            return View(Sol_DependenciasRegionales);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sol_DependenciasRegionales Sol_DependenciasRegionales = db.Sol_DependenciasRegionales.Find(id);
            if (Sol_DependenciasRegionales == null)
            {
                return HttpNotFound();
            }
         
            return View(Sol_DependenciasRegionales);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Sol_DependenciasRegionales sol_DependenciasRegionales)
        {
           
            if (ModelState.IsValid)
            {
                var dependencia = db.Sol_DependenciasRegionales.Find(sol_DependenciasRegionales.DependenciaId);

                if (!TryUpdateModel(dependencia, new string[] { "DependenciaNombre", "DependenciaSupervisorId", "DependenciaSupervisorCorreo" })) return View();
                db.SaveChanges();

                TempData["toastrMessage"] = "Editado Correctamente!";
                return RedirectToAction("Index");
            }
            return View(sol_DependenciasRegionales);
        }
    }
}