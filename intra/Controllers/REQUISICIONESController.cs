﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.RequisicionesCompra;

namespace intra.Controllers
{
    public class REQUISICIONESController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: REQUISICIONES
        public ActionResult Index()
        {

            var requisiciones = db.vw_REQUISICIONGRUPOS.OrderBy(x=> x.FECHA_ENTRADA_SISTEMA).ToList();

            ViewBag.CantidadPendiente = requisiciones.Count;

           
            return View(requisiciones);
     
        }

        public ActionResult Requisiciones(string id)
        {
            var requisiciones = db.vw_REQUISICION.Where(x=> x.NO_REQUISICION == id).ToList();
            return View(requisiciones);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
