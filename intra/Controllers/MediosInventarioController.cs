﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.Inventario;

namespace intra.Controllers
{
    public class MediosInventarioController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: MediosInventario
        public ActionResult Index()
        {
            return View(db.MediosInventario.ToList());
        }

        [CustomAuthorize]
        // GET: MediosInventario/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MediosInventario mediosInventario = db.MediosInventario.Find(id);
            if (mediosInventario == null)
            {
                return HttpNotFound();
            }
            return View(mediosInventario);
        }

        [CustomAuthorize]
        // GET: MediosInventario/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MediosInventario/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MedioId,MedioDescripcion")] MediosInventario mediosInventario)
        {
            if (ModelState.IsValid)
            {
                db.MediosInventario.Add(mediosInventario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mediosInventario);
        }

        [CustomAuthorize]
        // GET: MediosInventario/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MediosInventario mediosInventario = db.MediosInventario.Find(id);
            if (mediosInventario == null)
            {
                return HttpNotFound();
            }
            return View(mediosInventario);
        }

        // POST: MediosInventario/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MedioId,MedioDescripcion")] MediosInventario mediosInventario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mediosInventario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mediosInventario);
        }

        [CustomAuthorize]
        // GET: MediosInventario/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MediosInventario mediosInventario = db.MediosInventario.Find(id);
            if (mediosInventario == null)
            {
                return HttpNotFound();
            }
            return View(mediosInventario);
        }

        // POST: MediosInventario/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MediosInventario mediosInventario = db.MediosInventario.Find(id);
            db.MediosInventario.Remove(mediosInventario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
