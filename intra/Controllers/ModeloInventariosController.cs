﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Models.Inventario;

namespace intra.Controllers
{
    public class ModeloInventariosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: ModeloInventarios
        public ActionResult Index()
        {
            return View(db.ModeloInventario.ToList());
        }

        [CustomAuthorize]
        // GET: ModeloInventarios/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        [CustomAuthorize]
        // GET: ModeloInventarios/Create
        public ActionResult Create()
        {
            ViewBag.MarcaId = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            return View();
        }

        // POST: ModeloInventarios/Create
        [HttpPost]
        public ActionResult Create(ModeloInventario modeloInventario)
        {
           
            modeloInventario.ModeloFecha = DateTime.Now;
            modeloInventario.ModeloUsuario = Session["usuario"].ToString();
            if (ModelState.IsValid) 
            {
                db.ModeloInventario.Add(modeloInventario);
                db.SaveChanges();
                return RedirectToAction("Index");
                    
            }
            ViewBag.MarcaId = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            return View(modeloInventario);
        }

        [CustomAuthorize]
        // GET: ModeloInventarios/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.MarcaId = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");

            if (id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModeloInventario modeloInventario = db.ModeloInventario.Find(id);
            if(modeloInventario == null)
            {
                return HttpNotFound();
            }
            return View(modeloInventario);
        }

        // POST: ModeloInventarios/Edit/5
        [HttpPost]
        public ActionResult Edit([Bind(Include ="ModeloId,ModeloDescripcion,ModeloFecha,ModeloUsuario,MarcaId")] ModeloInventario modeloInventario  )
        {
            if (ModelState.IsValid)
            {
                db.Entry(modeloInventario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(modeloInventario);
        }

        [CustomAuthorize]
        // GET: ModeloInventarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModeloInventario modeloInventario = db.ModeloInventario.Find(id);
            if(modeloInventario==null)
            {
                return HttpNotFound();
            }
            return View(modeloInventario);
        }

        // POST: ModeloInventarios/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
