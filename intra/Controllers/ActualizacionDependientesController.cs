﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.ActualizacionDatos;

namespace intra.Controllers
{
    [AllowAnonymous]
    public class ActualizacionDependientesController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: ActualizacionDependientes
        public ActionResult ListaDependientes(int id )
        {
            id = int.Parse(Session["empleadoId"].ToString());

            var dependientes = (from a in db.ActualizacionDependientes
                                join p in db.Parentesco on a.ParentescoId equals p.ParentescoId
                                where a.EmpleadoId == id

                                select new vActualizacionDependiente
                                {
                                   // Genero = a.Genero == false ? "Masculino" : "Femenino",
                                    EstadoCivil = a.EstadoCivil == 1 ? "Casado(a)" : a.EstadoCivil == 2 ? "Divorciado(a)" : a.EstadoCivil == 3 ? "Soltero(a)" : "Viudo(a)",
                                    ParentescoId = p.Descripcion,
                                    Edad = a.Edad.ToString()

                                }).ToList();

            return View(dependientes);
        }

        // GET: ActualizacionDependientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActualizacionDependientes actualizacionDependientes = db.ActualizacionDependientes.Find(id);
            if (actualizacionDependientes == null)
            {
                return HttpNotFound();
            }
            return View(actualizacionDependientes);
        }

        // GET: ActualizacionDependientes/Create

        public ActionResult CrearDependientes()
        {
            ViewBag.parentesco = new SelectList(db.Parentesco, "ParentescoId", "Descripcion");
            ViewBag.EmpleadoId = 18211;
            return View();
        }

        // POST: ActualizacionDependientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
      //  [ValidateAntiForgeryToken]

        public JsonResult CrearDependientes(string[] DataEdad, string[] DataEstadoCivil,  string[] DataParentesco, ActualizacionDependientes Dependientes)
        {
            int CodigoEmpleado = int.Parse(Session["empleadoId"].ToString());
            if (DataEdad == null || DataEstadoCivil == null )
            {
                return Json(new { success = false, Mensaje = "Debe seleccionar al menos un dependiente para Guardar" });
            }

            for(int i=0; i < DataParentesco.Length; i++)
            {
                
                Dependientes.EmpleadoId = CodigoEmpleado;
                Dependientes.EstadoCivil = int.Parse(DataEstadoCivil[i]);
                //Dependientes.Genero = DataGenero[i];
                Dependientes.Edad = Convert.ToByte(DataEdad[i]);
                Dependientes.ParentescoId = int.Parse(DataParentesco[i]);
                db.ActualizacionDependientes.Add(Dependientes);
                db.SaveChanges();

            }

            return Json(new { success = true, Mensaje = "Solicitud Asignada Correctamente" });
        }




        // GET: ActualizacionDependientes/Create

        public ActionResult CrearHijosDependientes()
        {
            ViewBag.parentesco = new SelectList(db.Parentesco, "ParentescoId", "Descripcion").ToList();
            //ViewBag.EmpleadoId = 18211;
            return View();
        }

        // POST: ActualizacionDependientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]

        //public JsonResult CrearHijosDependientes(string[] Edad, bool[] Genero, string[] Parentesco, ActualizacionHijo Hijo)
        //{
        //    if (Edad == null || Genero == null)
        //    {
        //        return Json(new { success = false, Mensaje = "Debe seleccionar al menos un dependiente para Guardar" });
        //    }

        //    for (int i = 0; i < Parentesco.Length; i++)
        //    {
        //        //Dependientes.EstadoCivil = int.Parse(EstadoCivil[i]);
        //        Hijo.EmpleadoId = int.Parse(Session["empleadoId"].ToString());
        //        Hijo.Genero = Genero[i];
        //        Hijo.Edad = byte.Parse(Edad[i]);
        //        Hijo.ParentescoId = int.Parse(Parentesco[i]);
        //        db.ActualizacionHijo.Add(Hijo);
        //        db.SaveChanges();

        //    }


        //    return Json(new { success = true, Mensaje = "Datos Guardados Correctamente" });
        //}


        // GET: ActualizacionDependientes/Edit/5

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActualizacionDependientes actualizacionDependientes = db.ActualizacionDependientes.Find(id);
            if (actualizacionDependientes == null)
            {
                return HttpNotFound();
            }
            return View(actualizacionDependientes);
        }

        // POST: ActualizacionDependientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit([Bind(Include = "DependienteId,Genero,EstadoCivil,Edad")] ActualizacionDependientes actualizacionDependientes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(actualizacionDependientes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(actualizacionDependientes);
        }

        // GET: ActualizacionDependientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActualizacionDependientes actualizacionDependientes = db.ActualizacionDependientes.Find(id);
            if (actualizacionDependientes == null)
            {
                return HttpNotFound();
            }
            return View(actualizacionDependientes);
        }

        // POST: ActualizacionDependientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ActualizacionDependientes actualizacionDependientes = db.ActualizacionDependientes.Find(id);
            db.ActualizacionDependientes.Remove(actualizacionDependientes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
