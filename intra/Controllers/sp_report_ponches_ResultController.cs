﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using intra.Models;
using System.Globalization;
using Rotativa;
using Rotativa.Options;

using intra.Models.GestionHumana;

namespace intra.Controllers
{

    public class sp_report_ponches_ResultController : Controller
    {
        private dbIntranet db = new dbIntranet();
        private RHLogsEntities1 db2 = new RHLogsEntities1();
        private intra2 db3 = new intra2();
        private const string salt = "hghghghuTGYGy5656GHGh";
        private DFMPModel db4 = new DFMPModel();

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        // GET: sp_report_ponches_Result
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();

            var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados '{0}'", empId)).FirstOrDefault();

            if (j.Nombre != "N/A" && j.Procedencia != "N/A")
            {
                switch (j.Procedencia)
                {
                    case "DEPENDENCIA":
                        var dependency = db.vw_Dependencias.Where(x => x.DirectorID.ToString() == empId).FirstOrDefault();

                        if(dependency.DependenciaID.ToString() == "10001")
                        { 

                        var employees = db.Vw_Mostrar_Personal_Permisos_PGR.ToList();
                            string employee2 = Session["empleadoId"].ToString();
                            RegisterLogs rl = new RegisterLogs(employee2, "Ingresó al módulo de Administración de Ponches.");

                            return View(employees);
                        } else
                        {
                            var employees = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == dependency.DependenciaID.ToString()).ToList();

                            string employee2 = Session["empleadoId"].ToString();
                            RegisterLogs rl = new RegisterLogs(employee2, "Ingresó al módulo de Administración de Ponches.");

                            return View(employees);
                        }
                     

                    case "DEPARTAMENTO":
                        var department = db.vw_Departamentos.Where(x => x.EncargadoID.ToString() == empId).FirstOrDefault();
                     

                        var employees2 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == department.DependenciasID.ToString() && x.CodigoDepartamento == department.DeptoID.ToString()).ToList();

                        string employee3 = Session["empleadoId"].ToString();
                        RegisterLogs r2 = new RegisterLogs(employee3, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees2);

                    case "DIVISION":
                        var division = db.vw_Divisiones.Where(x => x.EncargadoID.ToString() == empId).FirstOrDefault();

                        var employees3 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == division.DependenciaID.ToString() && x.CodigoDepartamento == division.DeptoID.ToString() && x.CodigoDivision == division.DivisionID.ToString()).ToList();

                        string employee4 = Session["empleadoId"].ToString();
                        RegisterLogs r3 = new RegisterLogs(employee4, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees3);

                    case "SECCION":
                        var section = db.vw_Secciones.Where(x => x.EncargadoID.ToString() == empId).FirstOrDefault();

                        var employees4 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == section.DependenciaID.ToString() && x.CodigoDepartamento == section.DeptoID.ToString() && x.CodigoDivision == section.DivisionID.ToString() && x.CodigoSeccion == section.SeccionID.ToString()).ToList();

                        string employee5 = Session["empleadoId"].ToString();
                        RegisterLogs r4 = new RegisterLogs(employee5, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees4);

                    default:
                        return View();
                }
            }
            else
            {
                var ja = db.Database.SqlQuery<sp_verif_Auxiliar_Result>(string.Format("sp_verif_Auxiliar '{0}'", empId)).FirstOrDefault();

                switch (ja.Procedencia)
                {
                    case "DEPENDENCIA":
                        var dependency = db.vw_Dependencias.Where(x => x.AuxiliarID.ToString() == empId).FirstOrDefault();

                        var employees = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == dependency.DependenciaID.ToString()).ToList();

                        string employee2 = Session["empleadoId"].ToString();
                        RegisterLogs rl = new RegisterLogs(employee2, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees);

                    case "DEPARTAMENTO":
                        var department = db.vw_Departamentos.Where(x => x.AuxiliarID.ToString() == empId).FirstOrDefault();



                        var employees2 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == department.DependenciasID.ToString() && x.CodigoDepartamento == department.DeptoID.ToString()).ToList();

                        string employee3 = Session["empleadoId"].ToString();
                        RegisterLogs r2 = new RegisterLogs(employee3, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees2);

                    case "DIVISION":
                        var division = db.vw_Divisiones.Where(x => x.AuxiliarID.ToString() == empId).FirstOrDefault();

                        var employees3 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == division.DependenciaID.ToString() && x.CodigoDepartamento == division.DeptoID.ToString() && x.CodigoDivision == division.DivisionID.ToString()).ToList();

                        string employee4 = Session["empleadoId"].ToString();
                        RegisterLogs r3 = new RegisterLogs(employee4, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees3);

                    case "SECCION":
                        var section = db.vw_Secciones.Where(x => x.AuxiliarID.ToString() == empId).FirstOrDefault();

                        var employees4 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == section.DependenciaID.ToString() && x.CodigoDepartamento == section.DeptoID.ToString() && x.CodigoDivision == section.DivisionID.ToString() && x.CodigoSeccion == section.SeccionID.ToString()).ToList();

                        string employee5 = Session["empleadoId"].ToString();
                        RegisterLogs r4 = new RegisterLogs(employee5, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees4);

                    default:
                        return View();
                }
            }


        }

        [CustomAuthorize]
        // GET: incautaciones/Edit/5
        public ActionResult vPoncheEmpleado()
        {
            return View();
        }

        [CustomAuthorize]
        // GET: incautaciones/Edit/5
        public JsonResult JsonPonches()
        {

            string empId = HttpContext.Session["empleadoId"].ToString();
            int empIdInt = Convert.ToInt32(empId);
            var ponches = new List<sp_report_ponches_Result>();

            var employee = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == empId).FirstOrDefault();

            if (employee != null)
            {

                ponches = FindPonches(employee);

                if (employee.ponches.Count == 0)
                {
                    ViewBag.NoPoncha = 1;
                }
                else
                {
                    ViewBag.NoPoncha = 0;
                }

            }
            return Json(ponches.OrderBy(x => x.Fecha_Ponche));
        }

        [CustomAuthorize]
        // GET: incautaciones/Edit/5
        public ActionResult Edit(int? id)
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            int empIdInt = Convert.ToInt32(empId);
            string dependenciaSede = Session["dependenciaId"].ToString();

            var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", empIdInt)).FirstOrDefault();
            var in_charge = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == empId).FirstOrDefault();
            var ja = db.Database.SqlQuery<sp_verif_Auxiliar_Result>(string.Format("sp_verif_Auxiliar {0}", empIdInt)).FirstOrDefault();


            if (j.Nombre == "N/A" && j.Procedencia == "N/A")
            {
                if (ja.Nombre == "N/A" && ja.Procedencia == "N/A")
                {
                    return View("EditNoAccess");
                }
            }

            var employee = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == id.ToString()).FirstOrDefault();

            if (employee != null)
            {
                if (j.Nombre != "N/A" && j.Procedencia != "N/A")
                {
                    switch (j.Procedencia)
                    {
                        case "DEPENDENCIA":
                           
                            if (employee.CodigoDependencia == (db.vw_Dependencias.Where(x => x.DirectorID.ToString() == in_charge.Codigo).FirstOrDefault().DependenciaID.ToString()))
                            {
                                employee.ponches = FindPonches(employee);

                                if (employee.ponches.Count == 0)
                                {
                                    ViewBag.NoPoncha = 1;
                                }
                                else
                                {
                                    ViewBag.NoPoncha = 0;
                                }

                                ViewBag.Kpe = (Code.UtilityMethods.SetSHA1(employee.Codigo) + salt);

                                return View(employee);
                            }
                            else if(dependenciaSede == "10001" && in_charge.Codigo == "16340")
                            {
                                employee.ponches = FindPonches(employee);

                                if (employee.ponches.Count == 0)
                                {
                                    ViewBag.NoPoncha = 1;
                                }
                                else
                                {
                                    ViewBag.NoPoncha = 0;
                                }

                                ViewBag.Kpe = (Code.UtilityMethods.SetSHA1(employee.Codigo) + salt);

                                return View(employee);

                            }

                            break;

                        case "DEPARTAMENTO":
                            var department = db.vw_Departamentos.Where(x => x.EncargadoID.ToString() == in_charge.Codigo).FirstOrDefault();

                            if (employee.CodigoDepartamento == department.DeptoID.ToString() && employee.CodigoDependencia == department.DependenciasID)
                            {
                                employee.ponches = FindPonches(employee);

                                if (employee.ponches.Count == 0)
                                {
                                    ViewBag.NoPoncha = 1;
                                }
                                else
                                {
                                    ViewBag.NoPoncha = 0;
                                }

                                ViewBag.Kpe = (Code.UtilityMethods.SetSHA1(employee.Codigo) + salt);

                                return View(employee);
                            }
                            break;

                        case "DIVISION":
                            var division = db.vw_Divisiones.Where(x => x.EncargadoID.ToString() == in_charge.Codigo).FirstOrDefault();

                            if (employee.CodigoDivision == division.DivisionID.ToString() && employee.CodigoDepartamento == division.DeptoID.ToString() && employee.CodigoDependencia == division.DependenciaID.ToString())
                            {
                                employee.ponches = FindPonches(employee);

                                if (employee.ponches.Count == 0)
                                {
                                    ViewBag.NoPoncha = 1;
                                }
                                else
                                {
                                    ViewBag.NoPoncha = 0;
                                }

                                ViewBag.Kpe = (Code.UtilityMethods.SetSHA1(employee.Codigo) + salt);

                                return View(employee);
                            }
                            break;

                        case "SECCION":
                            var section = db.vw_Secciones.Where(x => x.EncargadoID.ToString() == in_charge.Codigo).FirstOrDefault();

                            if (employee.CodigoSeccion == section.SeccionID.ToString() && employee.CodigoDivision == section.DivisionID.ToString() && employee.CodigoDepartamento == section.DeptoID && employee.CodigoDependencia == section.DependenciaID.ToString())
                            {
                                employee.ponches = FindPonches(employee);

                                if (employee.ponches.Count == 0)
                                {
                                    ViewBag.NoPoncha = 1;
                                }
                                else
                                {
                                    ViewBag.NoPoncha = 0;
                                }

                                ViewBag.Kpe = (Code.UtilityMethods.SetSHA1(employee.Codigo) + salt);

                                return View(employee);
                            }

                            break;

                        default:
                            return View("EditNoAccess");
                    }
                }
                else
                {
                    switch (ja.Procedencia)
                    {
                        case "DEPENDENCIA":
                            if (employee.CodigoDependencia == (db.vw_Dependencias.Where(x => x.AuxiliarID.ToString() == in_charge.Codigo).FirstOrDefault().DependenciaID.ToString()))
                            {
                                employee.ponches = FindPonches(employee);

                                if (employee.ponches.Count == 0)
                                {
                                    ViewBag.NoPoncha = 1;
                                }
                                else
                                {
                                    ViewBag.NoPoncha = 0;
                                }

                                ViewBag.Kpe = (Code.UtilityMethods.SetSHA1(employee.Codigo) + salt);

                                return View(employee);
                            }

                            break;

                        case "DEPARTAMENTO":
                            var department = db.vw_Departamentos.Where(x => x.AuxiliarID.ToString() == in_charge.Codigo).FirstOrDefault();

                            if (employee.CodigoDepartamento == department.DeptoID.ToString() && employee.CodigoDependencia == department.DependenciasID)
                            {
                                employee.ponches = FindPonches(employee);

                                if (employee.ponches.Count == 0)
                                {
                                    ViewBag.NoPoncha = 1;
                                }
                                else
                                {
                                    ViewBag.NoPoncha = 0;
                                }

                                ViewBag.Kpe = (Code.UtilityMethods.SetSHA1(employee.Codigo) + salt);

                                return View(employee);
                            }
                            break;

                        case "DIVISION":
                            var division = db.vw_Divisiones.Where(x => x.AuxiliarID.ToString() == in_charge.Codigo).FirstOrDefault();

                            if (employee.CodigoDivision == division.DivisionID.ToString() && employee.CodigoDepartamento == division.DeptoID.ToString() && employee.CodigoDependencia == division.DependenciaID.ToString())
                            {
                                employee.ponches = FindPonches(employee);

                                if (employee.ponches.Count == 0)
                                {
                                    ViewBag.NoPoncha = 1;
                                }
                                else
                                {
                                    ViewBag.NoPoncha = 0;
                                }

                                ViewBag.Kpe = (Code.UtilityMethods.SetSHA1(employee.Codigo) + salt);

                                return View(employee);
                            }
                            break;

                        case "SECCION":
                            var section = db.vw_Secciones.Where(x => x.AuxiliarID.ToString() == in_charge.Codigo).FirstOrDefault();

                            if (employee.CodigoSeccion == section.SeccionID.ToString() && employee.CodigoDivision == section.DivisionID.ToString() && employee.CodigoDepartamento == section.DeptoID && employee.CodigoDependencia == section.DependenciaID.ToString())
                            {
                                employee.ponches = FindPonches(employee);

                                if (employee.ponches.Count == 0)
                                {
                                    ViewBag.NoPoncha = 1;
                                }
                                else
                                {
                                    ViewBag.NoPoncha = 0;
                                }

                                ViewBag.Kpe = (Code.UtilityMethods.SetSHA1(employee.Codigo) + salt);

                                return View(employee);
                            }

                            break;

                        default:
                            return View("EditNoAccess");
                    }
                }

                return View("EditNoAccess");
            }

            return View("EditNoAccess");
        }

        [CustomAuthorize]
        // GET: sp_report_ponches_Result
        public ActionResult Areas()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();

            var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados '{0}'", empId)).FirstOrDefault();

            if (j.Nombre != "N/A" && j.Procedencia != "N/A")
            {
                switch (j.Procedencia)
                {
                    case "DEPENDENCIA":
                        var dependency = db.vw_Dependencias.Where(x => x.DirectorID.ToString() == empId).FirstOrDefault();

                        var employees = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == dependency.DependenciaID.ToString()).ToList();

                        string employee2 = Session["empleadoId"].ToString();
                        RegisterLogs rl = new RegisterLogs(employee2, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees);

                    case "DEPARTAMENTO":
                        var department = db.vw_Departamentos.Where(x => x.EncargadoID.ToString() == empId).FirstOrDefault();

                        var employees2 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == department.DependenciasID.ToString() && x.CodigoDepartamento == department.DeptoID.ToString()).ToList();

                        string employee3 = Session["empleadoId"].ToString();
                        RegisterLogs r2 = new RegisterLogs(employee3, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees2);

                    case "DIVISION":
                        var division = db.vw_Divisiones.Where(x => x.EncargadoID.ToString() == empId).FirstOrDefault();

                        var employees3 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == division.DependenciaID.ToString() && x.CodigoDepartamento == division.DeptoID.ToString() && x.CodigoDivision == division.DivisionID.ToString()).ToList();

                        string employee4 = Session["empleadoId"].ToString();
                        RegisterLogs r3 = new RegisterLogs(employee4, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees3);

                    case "SECCION":
                        var section = db.vw_Secciones.Where(x => x.EncargadoID.ToString() == empId).FirstOrDefault();

                        var employees4 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == section.DependenciaID.ToString() && x.CodigoDepartamento == section.DeptoID.ToString() && x.CodigoDivision == section.DivisionID.ToString() && x.CodigoSeccion == section.SeccionID.ToString()).ToList();

                        string employee5 = Session["empleadoId"].ToString();
                        RegisterLogs r4 = new RegisterLogs(employee5, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees4);

                    default:
                        return View();
                }
            }
            else
            {
                var ja = db.Database.SqlQuery<sp_verif_Auxiliar_Result>(string.Format("sp_verif_Auxiliar {0}", empId)).FirstOrDefault();

                switch (ja.Procedencia)
                {
                    case "DEPENDENCIA":
                        var dependency = db.vw_Dependencias.Where(x => x.AuxiliarID.ToString() == empId).FirstOrDefault();

                        var employees = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == dependency.DependenciaID.ToString()).ToList();

                        string employee2 = Session["empleadoId"].ToString();
                        RegisterLogs rl = new RegisterLogs(employee2, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees);

                    case "DEPARTAMENTO":
                        var department = db.vw_Departamentos.Where(x => x.AuxiliarID.ToString() == empId).FirstOrDefault();

                        var employees2 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == department.DependenciasID.ToString() && x.CodigoDepartamento == department.DeptoID.ToString()).ToList();

                        string employee3 = Session["empleadoId"].ToString();
                        RegisterLogs r2 = new RegisterLogs(employee3, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees2);

                    case "DIVISION":
                        var division = db.vw_Divisiones.Where(x => x.AuxiliarID.ToString() == empId).FirstOrDefault();

                        var employees3 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == division.DependenciaID.ToString() && x.CodigoDepartamento == division.DeptoID.ToString() && x.CodigoDivision == division.DivisionID.ToString()).ToList();

                        string employee4 = Session["empleadoId"].ToString();
                        RegisterLogs r3 = new RegisterLogs(employee4, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees3);

                    case "SECCION":
                        var section = db.vw_Secciones.Where(x => x.AuxiliarID.ToString() == empId).FirstOrDefault();

                        var employees4 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == section.DependenciaID.ToString() && x.CodigoDepartamento == section.DeptoID.ToString() && x.CodigoDivision == section.DivisionID.ToString() && x.CodigoSeccion == section.SeccionID.ToString()).ToList();

                        string employee5 = Session["empleadoId"].ToString();
                        RegisterLogs r4 = new RegisterLogs(employee5, "Ingresó al módulo de Administración de Ponches.");

                        return View(employees4);

                    default:
                        return View();
                }
            }


        }

        [CustomAuthorize]
        public ActionResult EditNoAccess()
        {
            return View();
        }

        [HttpPost]
        public JsonResult LoadView()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            int empIdInt = Convert.ToInt32(empId);
            string dependenciaSede = Session["dependenciaId"].ToString();

            //var user = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == empId).FirstOrDefault();

            var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", empIdInt)).FirstOrDefault();

            Utilities2 ut2 = new Utilities2();

            if (j.Nombre != "N/A" && j.Procedencia != "N/A")
            {
                ut2.chain = j.Procedencia;

                switch (j.Procedencia)
                {
                    case "DEPENDENCIA":

                        var dependency = db.vw_Dependencias.Where(x => x.Descripcion == j.Nombre).FirstOrDefault();
                        ut2.select_list = LoadList(j.Procedencia, dependency.DependenciaID, 0, 0, 0);
                        ut2.hidden_dependency = dependency.DependenciaID.ToString();
                        ViewBag.Depto = ut2.select_list;
                        break;


                    case "DEPARTAMENTO":
                        
                        if (dependenciaSede != "10001" && empIdInt != 16340)
                        {
                            var department = db.vw_Departamentos.Where(x => x.Descripcion == j.Nombre).FirstOrDefault();

                            ut2.select_list = LoadList(j.Procedencia, Convert.ToInt32(department.DependenciasID), department.DeptoID, 0, 0);
                            ut2.hidden_dependency = department.DependenciasID;
                            ut2.hidden_department = department.DeptoID.ToString();
                            ViewBag.Division = ut2.select_list;

                        }
                        else
                        {
                            //Hasta aqui
                            var department = db.vw_Departamentos.Where(x => x.Descripcion == j.Nombre).FirstOrDefault();

                            ut2.select_list = LoadList(j.Procedencia, 0, department.DeptoID, 0, 0);


                            ut2.hidden_dependency = department.DependenciasID;
                            ut2.hidden_department = department.DeptoID.ToString();
                            ViewBag.Division = ut2.select_list;


                        }

                        break;

                    case "DIVISION":
                        var division = db.vw_Divisiones.Where(x => x.Descripcion == j.Nombre).FirstOrDefault();
                        ut2.select_list = LoadList(j.Procedencia, division.DependenciaID, division.DeptoID, division.DivisionID, 0);
                        ut2.hidden_dependency = division.DependenciaID.ToString();
                        ut2.hidden_department = division.DeptoID.ToString();
                        ut2.hidden_division = division.DivisionID.ToString();
                        ViewBag.Seccion = ut2.select_list;
                        break;

                    case "SECCION":
                        var section = db.vw_Secciones.Where(x => x.Descripcion == j.Nombre).FirstOrDefault();
                        //ut2.select_list = LoadList(j.Procedencia, section.SeccionID);
                        ut2.hidden_dependency = section.DependenciaID.ToString();
                        ut2.hidden_department = section.DeptoID;
                        ut2.hidden_division = section.DivisionID.ToString();
                        ut2.hidden_section = section.SeccionID.ToString();
                        break;

                    default:
                        return Json("Empty");
                }
            }
            else
            {
                var ja = db.Database.SqlQuery<sp_verif_Auxiliar_Result>(string.Format("sp_verif_Auxiliar {0}", empIdInt)).FirstOrDefault();

                ut2.chain = ja.Procedencia;

                switch (ja.Procedencia)
                {
                    case "DEPENDENCIA":
                        var dependency = db.vw_Dependencias.Where(x => x.Descripcion == ja.Nombre).FirstOrDefault();
                        ut2.select_list = LoadList(ja.Procedencia, dependency.DependenciaID, 0, 0, 0);
                        ut2.hidden_dependency = dependency.DependenciaID.ToString();
                        ViewBag.Depto = ut2.select_list;
                        break;


                    case "DEPARTAMENTO":
                        var department = db.vw_Departamentos.Where(x => x.Descripcion == ja.Nombre).FirstOrDefault();
                        ut2.select_list = LoadList(ja.Procedencia, Convert.ToInt32(department.DependenciasID), department.DeptoID, 0, 0);
                        ut2.hidden_dependency = department.DependenciasID;
                        ut2.hidden_department = department.DeptoID.ToString();
                        ViewBag.Division = ut2.select_list;
                        break;

                    case "DIVISION":
                        var division = db.vw_Divisiones.Where(x => x.Descripcion == ja.Nombre).FirstOrDefault();
                        ut2.select_list = LoadList(ja.Procedencia, division.DependenciaID, division.DeptoID, division.DivisionID, 0);
                        ut2.hidden_dependency = division.DependenciaID.ToString();
                        ut2.hidden_department = division.DeptoID.ToString();
                        ut2.hidden_division = division.DivisionID.ToString();
                        ViewBag.Seccion = ut2.select_list;
                        break;

                    case "SECCION":
                        var section = db.vw_Secciones.Where(x => x.Descripcion == ja.Nombre).FirstOrDefault();
                        //ut2.select_list = LoadList(j.Procedencia, section.SeccionID);
                        ut2.hidden_dependency = section.DependenciaID.ToString();
                        ut2.hidden_department = section.DeptoID;
                        ut2.hidden_division = section.DivisionID.ToString();
                        ut2.hidden_section = section.SeccionID.ToString();
                        break;

                    default:
                        return Json("Empty");
                }
            }



            return Json(ut2);
        }

        [HttpPost]
        public JsonResult ReloadView(string hierarchy, string department_value, string division_value, string section_value)
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            int empIdInt = Convert.ToInt32(empId);

            var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", empIdInt)).FirstOrDefault();
            var dependency_id = 0;
            var department_id = 0;
            var division_id = 0;
            var section_id = 0;

            if (j.Nombre != "N/A" && j.Procedencia != "N/A")
            {
                if (j.Procedencia == "DEPENDENCIA")
                {
                    dependency_id = db.vw_Dependencias.Where(x => x.DirectorID == empIdInt).FirstOrDefault().DependenciaID;

                    department_id = Convert.ToInt32(department_value);
                    division_id = Convert.ToInt32(division_value);
                    section_id = Convert.ToInt32(section_value);
                }
                else if (j.Procedencia == "DEPARTAMENTO")
                {
                    var department = db.vw_Departamentos.Where(x => x.EncargadoID == empIdInt).FirstOrDefault();

                    dependency_id = Convert.ToInt32(department.DependenciasID);
                    department_id = department.DeptoID;

                    division_id = Convert.ToInt32(division_value);
                    section_id = Convert.ToInt32(section_value);
                }
                else if (j.Procedencia == "DIVISION")
                {
                    var division = db.vw_Divisiones.Where(x => x.EncargadoID == empIdInt).FirstOrDefault();

                    dependency_id = division.DependenciaID;
                    department_id = division.DeptoID;
                    division_id = division.DivisionID;

                    section_id = Convert.ToInt32(section_value);

                }
            }
            else
            {
                var ja = db.Database.SqlQuery<sp_verif_Auxiliar_Result>(string.Format("sp_verif_Auxiliar {0}", empIdInt)).FirstOrDefault();

                if (ja.Procedencia == "DEPENDENCIA")
                {
                    dependency_id = db.vw_Dependencias.Where(x => x.AuxiliarID == empIdInt).FirstOrDefault().DependenciaID;

                    department_id = Convert.ToInt32(department_value);
                    division_id = Convert.ToInt32(division_value);
                    section_id = Convert.ToInt32(section_value);
                }
                else if (j.Procedencia == "DEPARTAMENTO")
                {
                    var department = db.vw_Departamentos.Where(x => x.AuxiliarID == empIdInt).FirstOrDefault();

                    dependency_id = Convert.ToInt32(department.DependenciasID);
                    department_id = department.DeptoID;

                    division_id = Convert.ToInt32(division_value);
                    section_id = Convert.ToInt32(section_value);
                }
                else if (j.Procedencia == "DIVISION")
                {
                    var division = db.vw_Divisiones.Where(x => x.AuxiliarID == empIdInt).FirstOrDefault();

                    dependency_id = division.DependenciaID;
                    department_id = division.DeptoID;
                    division_id = division.DivisionID;

                    section_id = Convert.ToInt32(section_value);
                }
            }


            //else if(j.Procedencia == "SECCION")
            //{
            //    var section = db.vw_Secciones.Where(x => x.EncargadoID == empIdInt).FirstOrDefault();

            //    dependency_id = section.DependenciaID;
            //    department_id = Convert.ToInt32(section.DeptoID);
            //    division_id = section.DivisionID;
            //    section_id = section.SeccionID;
            //}

            Utilities2 ut2 = new Utilities2();

            switch (hierarchy)
            {
                case "DEPARTAMENTO":
                    ut2.chain = hierarchy;
                    ut2.select_list = LoadList("DEPARTAMENTO", dependency_id, department_id, division_id, section_id);

                    break;

                case "DIVISION":
                    ut2.chain = hierarchy;
                    ut2.select_list = LoadList("DIVISION", dependency_id, department_id, division_id, section_id);

                    break;

                default:
                    return Json("Empty");
            }

            return Json(ut2);

        }

        [HttpPost]
        public JsonResult LoadWithoutInheritanceEmployees()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            int empIdInt = Convert.ToInt32(empId);

            var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", empIdInt)).FirstOrDefault();

            if (j.Nombre != "N/A" && j.Procedencia != "N/A")
            {
                if (j.Procedencia == "SECCION")
                {
                    var section = db.vw_Secciones.Where(x => x.EncargadoID == empIdInt && x.Descripcion == j.Nombre).FirstOrDefault();

                    var employees = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoSeccion == section.SeccionID.ToString() && x.CodigoDivision == section.DivisionID.ToString() && x.CodigoDepartamento == section.DeptoID && x.CodigoDependencia == section.DependenciaID.ToString())
                            .Select(a => new
                            {
                                Codigo = a.Codigo,
                                NombreCompleto = a.NombreCompleto
                            }).ToList();

                    return Json(employees);
                }
                else if (j.Procedencia == "DIVISION")
                {
                    var division = db.vw_Divisiones.Where(x => x.EncargadoID == empIdInt && x.Descripcion == j.Nombre).FirstOrDefault();

                    var employees2 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDivision == division.DivisionID.ToString() && x.CodigoDepartamento == division.DeptoID.ToString() && x.CodigoDependencia == division.DependenciaID.ToString())
                            .Select(a => new
                            {
                                Codigo = a.Codigo,
                                NombreCompleto = a.NombreCompleto
                            }).ToList();

                    return Json(employees2);
                }
                else
                {
                    return Json("Empty");
                }
            }
            else
            {
                var ja = db.Database.SqlQuery<sp_verif_Auxiliar_Result>(string.Format("sp_verif_Auxiliar {0}", empIdInt)).FirstOrDefault();

                if (ja.Procedencia == "SECCION")
                {
                    var section = db.vw_Secciones.Where(x => x.AuxiliarID == empIdInt && x.Descripcion == ja.Nombre).FirstOrDefault();

                    var employees = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoSeccion == section.SeccionID.ToString() && x.CodigoDivision == section.DivisionID.ToString() && x.CodigoDepartamento == section.DeptoID && x.CodigoDependencia == section.DependenciaID.ToString())
                            .Select(a => new
                            {
                                Codigo = a.Codigo,
                                NombreCompleto = a.NombreCompleto
                            }).ToList();

                    return Json(employees);
                }
                else if (ja.Procedencia == "DIVISION")
                {
                    var division = db.vw_Divisiones.Where(x => x.AuxiliarID == empIdInt && x.Descripcion == ja.Nombre).FirstOrDefault();

                    var employees2 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDivision == division.DivisionID.ToString() && x.CodigoDepartamento == division.DeptoID.ToString() && x.CodigoDependencia == division.DependenciaID.ToString())
                            .Select(a => new
                            {
                                Codigo = a.Codigo,
                                NombreCompleto = a.NombreCompleto
                            }).ToList();

                    return Json(employees2);
                }
                else
                {
                    return Json("Empty");
                }
            }



        }

        [HttpPost]
        public JsonResult LoadEmployees(string hierarchy, string department_value, string division_value, string section_value)
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            int empIdInt = Convert.ToInt32(empId);

            var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", empIdInt)).FirstOrDefault();

            var dependency_id = 0;
            var departament_id = 0;
            var division_id = 0;
            var section_id = 0;

            if (j.Nombre != "N/A" && j.Procedencia != "N/A")
            {
                if (j.Procedencia == "DEPENDENCIA")
                {
                    dependency_id = db.Nmdependencias_Permisos.Where(x => x.DirectorID == empIdInt).FirstOrDefault().DependenciaID;
                    departament_id = ((department_value == null || department_value == "") ? 0 : Convert.ToInt32(department_value));
                    division_id = ((division_value == null || division_value == "") ? 0 : Convert.ToInt32(division_value));
                    section_id = ((section_value == null || section_value == "") ? 0 : Convert.ToInt32(section_value));
                }
                else if (j.Procedencia == "DEPARTAMENTO")
                {
                    var department = db.Nmdeptos_Permisos.Where(x => x.EncargadoID == empIdInt).FirstOrDefault();

                    dependency_id = department.DependenciaID;
                    departament_id = department.DeptoID;
                    division_id = ((division_value == null || division_value == "") ? 0 : Convert.ToInt32(division_value));
                    section_id = ((section_value == null || section_value == "") ? 0 : Convert.ToInt32(section_value));
                }
                else if (j.Procedencia == "DIVISION")
                {
                    var division = db.Nmdivision_Permisos.Where(x => x.EncargadoID == empIdInt).FirstOrDefault();

                    dependency_id = division.DependenciaID;
                    departament_id = division.DeptoID;
                    division_id = division.DivisionID;
                    section_id = ((section_value == null || section_value == "") ? 0 : Convert.ToInt32(section_value));
                }
            }
            else
            {
                var ja = db.Database.SqlQuery<sp_verif_Auxiliar_Result>(string.Format("sp_verif_Auxiliar {0}", empIdInt)).FirstOrDefault();

                if (ja.Procedencia == "DEPENDENCIA")
                {
                    dependency_id = db.Nmdependencias_Permisos.Where(x => x.AuxiliarID == empIdInt).FirstOrDefault().DependenciaID;
                    departament_id = ((department_value == null || department_value == "") ? 0 : Convert.ToInt32(department_value));
                    division_id = ((division_value == null || division_value == "") ? 0 : Convert.ToInt32(division_value));
                    section_id = ((section_value == null || section_value == "") ? 0 : Convert.ToInt32(section_value));
                }
                else if (j.Procedencia == "DEPARTAMENTO")
                {
                    var department = db.Nmdeptos_Permisos.Where(x => x.AuxiliarID == empIdInt).FirstOrDefault();

                    dependency_id = department.DependenciaID;
                    departament_id = department.DeptoID;
                    division_id = ((division_value == null || division_value == "") ? 0 : Convert.ToInt32(division_value));
                    section_id = ((section_value == null || section_value == "") ? 0 : Convert.ToInt32(section_value));
                }
                else if (j.Procedencia == "DIVISION")
                {
                    var division = db.Nmdivision_Permisos.Where(x => x.AuxiliarID == empIdInt).FirstOrDefault();

                    dependency_id = division.DependenciaID;
                    departament_id = division.DeptoID;
                    division_id = division.DivisionID;
                    section_id = ((section_value == null || section_value == "") ? 0 : Convert.ToInt32(section_value));
                }
            }

            switch (hierarchy)
            {
                case "DEPARTAMENTO":
                    var employees = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDepartamento == departament_id.ToString() && x.CodigoDependencia == dependency_id.ToString())
                        .Select(a => new
                        {
                            Codigo = a.Codigo,
                            NombreCompleto = a.NombreCompleto
                        }).ToList();
                    return Json(employees);

                case "DIVISION":
                    var employees2 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDivision == division_id.ToString() && x.CodigoDepartamento == departament_id.ToString() && x.CodigoDependencia == dependency_id.ToString())
                        .Select(a => new
                        {
                            Codigo = a.Codigo,
                            NombreCompleto = a.NombreCompleto
                        }).ToList();
                    return Json(employees2);

                case "SECCION":
                    var employees3 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoSeccion == section_id.ToString() && x.CodigoDivision == division_id.ToString() && x.CodigoDepartamento == departament_id.ToString() && x.CodigoDependencia == dependency_id.ToString())
                        .Select(a => new
                        {
                            Codigo = a.Codigo,
                            NombreCompleto = a.NombreCompleto
                        }).ToList();
                    return Json(employees3);

                default:
                    return Json("empty");
            }
        }


        public List<SelectListItem> LoadList(string hierarchy, int dependency_id, int department_id, int division_id, int section_id)
        {
            if (hierarchy.Equals("DEPENDENCIA"))
            {
                string string_id = dependency_id.ToString();

                List<SelectListItem> listsli = new List<SelectListItem>();

                var vw_dptos = db.vw_Departamentos.Where(x => x.DependenciasID == string_id).ToList().Select(obj => new
                {
                    DeptoID = obj.DeptoID,
                    Descripcion = obj.Descripcion,
                    EncargadoID = (obj.EncargadoID == null ? "" : obj.EncargadoID.ToString())
                });

                foreach (var item in vw_dptos)
                {
                    listsli.Add(new SelectListItem
                    {
                        Text = item.Descripcion,
                        Value = item.DeptoID.ToString()
                    });
                }

                return listsli;
            }
            else if (hierarchy.Equals("DEPARTAMENTO"))
            {
                List<SelectListItem> listsli2 = new List<SelectListItem>();

                var vw_divisiones = db.vw_Divisiones.Where(x => x.DeptoID == department_id && x.DependenciaID == dependency_id).ToList().Select(obj => new
                {
                    DivisionID = obj.DivisionID,
                    Descripcion = obj.Descripcion,
                    EncargadoID = (obj.EncargadoID == null ? "" : obj.EncargadoID.ToString())
                });

                foreach (var item in vw_divisiones)
                {
                    listsli2.Add(new SelectListItem
                    {
                        Text = item.Descripcion,
                        Value = item.DivisionID.ToString()
                    });
                }

                return listsli2;
            }
            else if (hierarchy.Equals("DIVISION"))
            {
                List<SelectListItem> listsli3 = new List<SelectListItem>();

                var vw_secciones = db.vw_Secciones.Where(x => x.DivisionID == division_id && x.DeptoID == department_id.ToString() && x.DependenciaID == dependency_id).ToList().Select(obj => new
                {
                    SeccionID = obj.SeccionID,
                    Descripcion = obj.Descripcion,
                    EncargadoID = (obj.EncargadoID == null ? "" : obj.EncargadoID.ToString())
                });

                foreach (var item in vw_secciones)
                {
                    listsli3.Add(new SelectListItem
                    {
                        Text = item.Descripcion,
                        Value = item.SeccionID.ToString()
                    });
                }

                return listsli3;
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public JsonResult LoadPonches(string begin_date, string end_date, string employee_id, int only_unjustified)
        {
            CultureInfo ci = CultureInfo.InstalledUICulture;

            var languaje_name = ci.Name;

            if (languaje_name == "en-US")
            {
                if (begin_date == "")
                {
                    begin_date = "";
                }
                else
                {
                    string[] splitted_date = begin_date.Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    begin_date = english_date.ToString("MM/dd/yyyy");
                }

                if (end_date == "")
                {
                    end_date = "";
                }
                else
                {
                    string[] splitted_date = end_date.Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    end_date = english_date.ToString("MM/dd/yyyy");
                }
            }
            else if (languaje_name == "es-ES")
            {
                begin_date = begin_date == "" ? "" : begin_date;
                end_date = end_date == "" ? "" : end_date;
            }


            DateTime begin_date_date = begin_date != "" ? Convert.ToDateTime(begin_date) : new DateTime();
            DateTime end_date_date = end_date != "" ? Convert.ToDateTime(end_date) : new DateTime();

            int employee_id_int = Convert.ToInt32(employee_id);
            var employee_start = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == employee_id).FirstOrDefault().fechaingreso.ToString("dd/MM/yyyy");

            string[] splitted_date2 = employee_start.Split('/');

            var employee_start_date = new DateTime(Convert.ToInt32(splitted_date2[2]), Convert.ToInt32(splitted_date2[1]), Convert.ToInt32(splitted_date2[0]));

            using (RHLogsEntities1 db2 = new RHLogsEntities1())
            {
                if (begin_date_date > end_date_date)
                {
                    return Json("FIMY");
                }

                if (begin_date == "" && end_date == "")
                {
                    var begin_date2 = Utilities.EmbeddedDate();

                    var ponches_list = db2.Database.SqlQuery<sp_report_ponches_Result>(string.Format("exec sp_report_ponches '{0}','{1}','{2}','{3}','{4}','{5}','{6}'",
                                                begin_date2.Date.ToString("yyyy-MM-dd"), DateTime.Now.Date.ToString("yyyy-MM-dd"), 0, 0, 0, 0, employee_id_int)).Where(x => x.Fecha_Ponche.Value.Date >= employee_start_date.Date)
                                                .ToList();

                    if (ponches_list.Count > 0 || ponches_list == null)
                    {
                        var ponches_list2 = new List<sp_report_ponches_Result>();

                        var begin_date_date_short = begin_date_date.Date;
                        var end_date_date_short = end_date_date.Date;

                        var dias_feriados = db4.DiasFeriados.Where(x => x.DiasFeriadosFecha >= begin_date_date_short && x.DiasFeriadosFecha <= end_date_date_short).ToList();

                        foreach (var item in ponches_list)
                        {
                            bool is_hollyday = false;

                            foreach (var item2 in dias_feriados)
                            {
                                if (item.Fecha_Ponche.Value.Date == item2.DiasFeriadosFecha.Date)
                                    is_hollyday = true;
                            }

                            if (!is_hollyday)
                            {
                                if (ponches_list2.Where(x => x.Fecha_Ponche.Value.ToString("dd/MM/yyyy") == item.Fecha_Ponche.Value.ToString("dd/MM/yyyy")).FirstOrDefault() == null)
                                {
                                    ponches_list2.Add(item);
                                }
                            }
                        }

                        return Json(ponches_list2.Select(a => new
                        {
                            Fecha_Ponche = a.Fecha_Ponche != null ? a.Fecha_Ponche.Value.Date.ToString("dd-MM-yyyy") : "N/A",
                            Fecha_Ponche_IndexSort = a.Fecha_Ponche != null ? a.Fecha_Ponche.Value.Date.ToString("yyyyMMdd") : "N/A",
                            poncheENTRADA = a.poncheENTRADA != null ? a.poncheENTRADA.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A",
                            poncheSALIDA = a.poncheSALIDA != null ? a.poncheSALIDA.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A",
                            poncheENTRADA1 = a.poncheENTRADA1 != null ? a.poncheENTRADA1.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A",
                            poncheSALIDA1 = a.poncheSALIDA1 != null ? a.poncheSALIDA1.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A",
                            Observacion = string.IsNullOrEmpty(a.Observacion) ? "" : a.Observacion
                        }).ToList());
                    }
                    else
                    {
                        return Json("Empty");
                    }
                }
                else if (only_unjustified == 1)
                {
                    var ponches_list = db2.Database.SqlQuery<sp_report_ponches_Result>(string.Format("exec sp_report_ponches '{0}','{1}','{2}','{3}','{4}','{5}','{6}'",
                             begin_date_date.Date.ToString("yyyy-MM-dd"), end_date_date.Date.ToString("yyyy-MM-dd"), 0, 0, 0, 0, employee_id_int)).Where(x => x.Fecha_Ponche.Value.Date >= employee_start_date.Date).Where(x => !string.IsNullOrEmpty(x.Observacion) && !x.Observacion.ToUpper().StartsWith("JUSTIFICADO")).ToList();

                    if (ponches_list.Count > 0 || ponches_list == null)
                    {
                        var ponches_list2 = new List<sp_report_ponches_Result>();

                        var begin_date_date_short = begin_date_date.Date;
                        var end_date_date_short = end_date_date.Date;

                        var dias_feriados = db4.DiasFeriados.Where(x => x.DiasFeriadosFecha >= begin_date_date_short && x.DiasFeriadosFecha <= end_date_date_short).ToList();

                        foreach (var item in ponches_list)
                        {
                            bool is_hollyday = false;

                            foreach (var item2 in dias_feriados)
                            {
                                if (item.Fecha_Ponche.Value.Date == item2.DiasFeriadosFecha.Date)
                                    is_hollyday = true;
                            }

                            if (!is_hollyday)
                            {
                                if (ponches_list2.Where(x => x.Fecha_Ponche.Value.ToString("dd/MM/yyyy") == item.Fecha_Ponche.Value.ToString("dd/MM/yyyy")).FirstOrDefault() == null)
                                {
                                    ponches_list2.Add(item);
                                }
                            }
                        }

                        return Json(ponches_list2.Select(a => new
                        {
                            Fecha_Ponche = a.Fecha_Ponche != null ? a.Fecha_Ponche.Value.Date.ToString("dd-MM-yyyy") : "N/A",
                            Fecha_Ponche_IndexSort = a.Fecha_Ponche != null ? a.Fecha_Ponche.Value.Date.ToString("yyyyMMdd") : "N/A",
                            poncheENTRADA = a.poncheENTRADA != null ? a.poncheENTRADA.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A",
                            poncheSALIDA = a.poncheSALIDA != null ? a.poncheSALIDA.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A",
                            poncheENTRADA1 = a.poncheENTRADA1 != null ? a.poncheENTRADA1.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A",
                            poncheSALIDA1 = a.poncheSALIDA1 != null ? a.poncheSALIDA1.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A",
                            Observacion = string.IsNullOrEmpty(a.Observacion) ? "" : a.Observacion
                        }).ToList());
                    }
                    else
                    {
                        return Json("Empty");
                    }
                }
                else
                {
                    var ponches_list = db2.Database.SqlQuery<sp_report_ponches_Result>(string.Format("exec sp_report_ponches '{0}','{1}','{2}','{3}','{4}','{5}','{6}'",
                             begin_date_date.Date.ToString("yyyy-MM-dd"), end_date_date.Date.ToString("yyyy-MM-dd"), 0, 0, 0, 0, employee_id_int)).Where(x => x.Fecha_Ponche.Value.Date >= employee_start_date.Date).ToList();

                    if (ponches_list.Count > 0 || ponches_list == null)
                    {
                        var ponches_list2 = new List<sp_report_ponches_Result>();

                        var begin_date_date_short = begin_date_date.Date;
                        var end_date_date_short = end_date_date.Date;

                        var dias_feriados = db4.DiasFeriados.Where(x => x.DiasFeriadosFecha >= begin_date_date_short && x.DiasFeriadosFecha <= end_date_date_short).ToList();

                        foreach (var item in ponches_list)
                        {
                            bool is_hollyday = false;

                            foreach (var item2 in dias_feriados)
                            {
                                if (item.Fecha_Ponche.Value.Date == item2.DiasFeriadosFecha.Date)
                                    is_hollyday = true;
                            }

                            if (!is_hollyday)
                            {
                                if (ponches_list2.Where(x => x.Fecha_Ponche.Value.ToString("dd/MM/yyyy") == item.Fecha_Ponche.Value.ToString("dd/MM/yyyy")).FirstOrDefault() == null)
                                {
                                    ponches_list2.Add(item);
                                }
                            }
                        }

                        return Json(ponches_list2.Select(a => new
                        {
                            Fecha_Ponche = a.Fecha_Ponche != null ? a.Fecha_Ponche.Value.Date.ToString("dd-MM-yyyy") : "N/A",
                            Fecha_Ponche_IndexSort = a.Fecha_Ponche != null ? a.Fecha_Ponche.Value.Date.ToString("yyyyMMdd") : "N/A",
                            poncheENTRADA = a.poncheENTRADA != null ? a.poncheENTRADA.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A",
                            poncheSALIDA = a.poncheSALIDA != null ? a.poncheSALIDA.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A",
                            poncheENTRADA1 = a.poncheENTRADA1 != null ? a.poncheENTRADA1.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A",
                            poncheSALIDA1 = a.poncheSALIDA1 != null ? a.poncheSALIDA1.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A",
                            Observacion = string.IsNullOrEmpty(a.Observacion) ? "Ninguna" : a.Observacion
                        }).ToList());
                    }
                    else
                    {
                        return Json("Empty");
                    }
                }
            }
        }

        [HttpPost]
        public ActionResult Report(int? a, string b, string c, string d, string e)
        {
            string employee_code = "";

            try
            {
                if (Code.UtilityMethods.SetSHA1(a.Value.ToString()) + salt != d)
                    return RedirectToAction("Edit/" + a);
                else
                {
                    int? id = a;
                    string begin_date = "";
                    string end_date = "";

                    CultureInfo ci = CultureInfo.InstalledUICulture;

                    var languaje_name = ci.Name;

                    if (languaje_name == "en-US")
                    {
                        if (b == null || b == "")
                        {
                            begin_date = Utilities.EmbeddedDate().ToString("MM/dd/yyyy");
                        }
                        else
                        {
                            string[] splitted_date = b.Split('/');
                            DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                            begin_date = english_date.ToString("MM/dd/yyyy");
                        }

                        if (c == null || c == "")
                        {
                            end_date = DateTime.Now.ToString("MM/dd/yyyy");
                        }
                        else
                        {
                            string[] splitted_date = c.Split('/');
                            DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                            end_date = english_date.ToString("MM/dd/yyyy");
                        }
                    }
                    else if (languaje_name == "es-ES")
                    {
                        begin_date = b == "" ? Utilities.EmbeddedDate().ToString("dd/MM/yyyy") : b;
                        end_date = c == "" ? DateTime.Now.ToString("dd/MM/yyyy") : c;
                    }

                    DateTime begin_date_date = Convert.ToDateTime(begin_date);
                    DateTime end_date_date = Convert.ToDateTime(end_date);

                    id = id == null ? 0 : id;

                    var employee_start_date = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id.ToString()).FirstOrDefault().fechaingreso;

                    using (RHLogsEntities1 db2 = new RHLogsEntities1())
                    {
                        if (e == "1")
                        {
                            var ponches_list = db2.Database.SqlQuery<sp_report_ponches_Result>(string.Format("exec sp_report_ponches '{0}','{1}','{2}','{3}','{4}','{5}','{6}'",
                                     begin_date_date.Date.ToString("yyyy-MM-dd"), end_date_date.Date.ToString("yyyy-MM-dd"), 0, 0, 0, 0, id))
                                     .Where(x => !string.IsNullOrEmpty(x.Observacion) && !x.Observacion.ToUpper().StartsWith("JUSTIFICADO") && x.Fecha_Ponche.Value.Date >= employee_start_date.Date).ToList();

                            if (ponches_list.Count > 0 || ponches_list != null)
                            {
                                var ponches_list2 = new List<sp_report_ponches_Result>();

                                var begin_date_date_short = begin_date_date.Date;
                                var end_date_date_short = end_date_date.Date;

                                var dias_feriados = db4.DiasFeriados.Where(x => x.DiasFeriadosFecha >= begin_date_date_short && x.DiasFeriadosFecha <= end_date_date_short).ToList();

                                foreach (var item in ponches_list)
                                {
                                    bool is_hollyday = false;

                                    foreach (var item2 in dias_feriados)
                                    {
                                        if (item.Fecha_Ponche.Value.Date == item2.DiasFeriadosFecha.Date)
                                            is_hollyday = true;
                                    }

                                    if (!is_hollyday)
                                    {
                                        if (ponches_list2.Where(x => x.Fecha_Ponche.Value.ToString("dd/MM/yyyy") == item.Fecha_Ponche.Value.ToString("dd/MM/yyyy")).FirstOrDefault() == null)
                                        {
                                            ponches_list2.Add(item);
                                        }
                                    }
                                }

                                ViewBag.ponches = ponches_list2/*.OrderBy(o=>o.Fecha_Ponche)*/;
                            }
                        }
                        else
                        {
                            var ponches_list = db2.Database.SqlQuery<sp_report_ponches_Result>(string.Format("exec sp_report_ponches '{0}','{1}','{2}','{3}','{4}','{5}','{6}'",
                                     begin_date_date.Date.ToString("yyyy-MM-dd"), end_date_date.Date.ToString("yyyy-MM-dd"), 0, 0, 0, 0, id)).Where(x => x.Fecha_Ponche.Value.Date >= employee_start_date.Date).ToList();

                            if (ponches_list.Count > 0 || ponches_list != null)
                            {
                                var ponches_list2 = new List<sp_report_ponches_Result>();

                                var begin_date_date_short = begin_date_date.Date;
                                var end_date_date_short = end_date_date.Date;

                                var dias_feriados = db4.DiasFeriados.Where(x => x.DiasFeriadosFecha >= begin_date_date_short && x.DiasFeriadosFecha <= end_date_date_short).ToList();

                                foreach (var item in ponches_list)
                                {
                                    bool is_hollyday = false;

                                    foreach (var item2 in dias_feriados)
                                    {
                                        if (item.Fecha_Ponche.Value.Date == item2.DiasFeriadosFecha.Date)
                                            is_hollyday = true;
                                    }

                                    if (!is_hollyday)
                                    {
                                        if (ponches_list2.Where(x => x.Fecha_Ponche.Value.ToString("dd/MM/yyyy") == item.Fecha_Ponche.Value.ToString("dd/MM/yyyy")).FirstOrDefault() == null)
                                        {
                                            ponches_list2.Add(item);
                                        }
                                    }
                                }

                                ViewBag.ponches = ponches_list2/*.OrderBy(o=>o.Fecha_Ponche)*/;
                            }
                        }

                        var employee = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == id.ToString()).FirstOrDefault();

                        ViewBag.Nombre = employee.NombreCompleto;
                        ViewBag.Codigo = employee.Codigo;
                        employee_code = employee.Codigo;
                        ViewBag.Departamento = employee.Departamento;
                        ViewBag.Division = employee.Division;
                    }

                    string employee2 = Session["empleadoId"].ToString();
                    RegisterLogs rl = new RegisterLogs(employee2, "Tiró un reporte de ponches del empleado " + employee_code);

                    return new ViewAsPdf("Report")
                    {
                        PageOrientation = Orientation.Portrait,
                        PageSize = Rotativa.Options.Size.A4,
                        PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }
                    };
                }
            }
            catch (Exception ex)
            {
                var e7 = ex.Message;

                return RedirectToAction("Edit");
            }
        }

        [HttpGet]
        public ActionResult Report(int? a, string b, string c, bool is_from_email)
        {
            string employee_code = "";

            try
            {
                if (is_from_email)
                {
                    int? id = a;
                    string begin_date = "";
                    string end_date = "";

                    CultureInfo ci = CultureInfo.InstalledUICulture;

                    var languaje_name = ci.Name;

                    if (languaje_name == "en-US")
                    {
                        if (b == null || b == "")
                        {
                            begin_date = "";
                        }
                        else
                        {
                            string[] splitted_date = b.Split('/');
                            DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                            begin_date = english_date.ToString("MM/dd/yyyy");
                        }

                        if (c == null || c == "")
                        {
                            end_date = "";
                        }
                        else
                        {
                            string[] splitted_date = c.Split('/');
                            DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                            end_date = english_date.ToString("MM/dd/yyyy");
                        }
                    }
                    else if (languaje_name == "es-ES")
                    {
                        begin_date = b == "" ? "" : b;
                        end_date = c == "" ? "" : c;
                    }

                    DateTime begin_date_date = Convert.ToDateTime(begin_date);
                    DateTime end_date_date = Convert.ToDateTime(end_date);

                    id = id == null ? 0 : id;

                    var employee_start_date = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == id.ToString()).FirstOrDefault().fechaingreso;

                    using (RHLogsEntities1 db2 = new RHLogsEntities1())
                    {

                        var ponches_list = db2.Database.SqlQuery<sp_report_ponches_Result>(string.Format("exec sp_report_ponches '{0}','{1}','{2}','{3}','{4}','{5}','{6}'",
                                    begin_date_date.Date.ToString("yyyy-MM-dd"), end_date_date.Date.ToString("yyyy-MM-dd"), 0, 0, 0, 0, id)).Where(x => x.Fecha_Ponche.Value.Date >= employee_start_date.Date).ToList();

                        if (ponches_list.Count > 0 || ponches_list != null)
                        {
                            var ponches_list2 = new List<sp_report_ponches_Result>();

                            var begin_date_date_short = begin_date_date.Date;
                            var end_date_date_short = end_date_date.Date;

                            var dias_feriados = db4.DiasFeriados.Where(x => x.DiasFeriadosFecha >= begin_date_date_short && x.DiasFeriadosFecha <= end_date_date_short).ToList();

                            foreach (var item in ponches_list)
                            {
                                bool is_hollyday = false;

                                foreach (var item2 in dias_feriados)
                                {
                                    if (item.Fecha_Ponche.Value.Date == item2.DiasFeriadosFecha.Date)
                                        is_hollyday = true;
                                }

                                if (!is_hollyday)
                                {
                                    if (ponches_list2.Where(x => x.Fecha_Ponche.Value.ToString("dd/MM/yyyy") == item.Fecha_Ponche.Value.ToString("dd/MM/yyyy")).FirstOrDefault() == null)
                                    {
                                        ponches_list2.Add(item);
                                    }
                                }
                            }

                            ViewBag.ponches = ponches_list2/*.OrderBy(o=>o.Fecha_Ponche)*/;
                        }

                        var employee = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == id.ToString()).FirstOrDefault();

                        ViewBag.Nombre = employee.NombreCompleto;
                        ViewBag.Codigo = employee.Codigo;
                        employee_code = employee.Codigo;
                        ViewBag.Departamento = employee.Departamento;
                        ViewBag.Division = employee.Division;
                    }

                    string employee2 = Session["empleadoId"].ToString();
                    RegisterLogs rl = new RegisterLogs(employee2, "Tiró un reporte de ponches del empleado " + employee_code);

                    return new ViewAsPdf("Report")
                    {
                        PageOrientation = Orientation.Portrait,
                        PageSize = Rotativa.Options.Size.A4,
                        PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }
                    };
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Edit");
            }
        }

        [HttpPost]
        public ActionResult ReportAreas(string a, string b, string c, string d, string e, string f)
        {
            a = a == "0" ? "" : a;
            b = b == "0" ? "" : b;
            c = c == "0" ? "" : c;

            CultureInfo ci = CultureInfo.InstalledUICulture;

            var languaje_name = ci.Name;

            if (languaje_name == "en-US")
            {
                if (d == "")
                {
                    d = Utilities.EmbeddedDate().ToString("MM/dd/yyyy");
                }
                else
                {
                    string[] splitted_date = d.Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    d = english_date.ToString("MM/dd/yyyy");
                }

                if (e == "")
                {
                    e = DateTime.Now.ToString("MM/dd/yyyy");
                }
                else
                {
                    string[] splitted_date = e.Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    e = english_date.ToString("MM/dd/yyyy");
                }
            }
            else if (languaje_name == "es-ES")
            {
                d = d == "" ? Utilities.EmbeddedDate().ToString("dd/MM/yyyy") : d;
                e = e == "" ? DateTime.Now.ToString("dd/MM/yyyy") : e;
            }

            string empId = HttpContext.Session["empleadoId"].ToString();

            RegisterLogs rl1 = new RegisterLogs(empId, "Tiró un reporte de ponches de área, valores: a:(" + a + ") b:(" + b + ") c:(" + c + ") d:(" + d +") e:(" + e + ") f:(" + f + ")");

            var employee_incharge_area = db.Database.SqlQuery<sp_verif_Encargados_Result>("sp_verif_Encargados {0}", empId).FirstOrDefault();

            RegisterLogs rl2 = new RegisterLogs(empId, "reporte área incharge:" + employee_incharge_area.Nombre+" "+ employee_incharge_area.Procedencia);

            string employee_incharge_dependency = "";

            if (employee_incharge_area.Nombre != "N/A" && employee_incharge_area.Procedencia != "N/A")
            {
                switch (employee_incharge_area.Procedencia)
                {
                    case "DEPENDENCIA":
                        employee_incharge_dependency = db.Nmdependencias_Permisos.Where(x => x.DirectorID.Value.ToString() == empId).FirstOrDefault().DependenciaID.ToString();
                        break;

                    case "DEPARTAMENTO":
                        employee_incharge_dependency = db.Nmdeptos_Permisos.Where(x => x.EncargadoID.Value.ToString() == empId).FirstOrDefault().DependenciaID.ToString();
                        break;

                    case "DIVISION":
                        employee_incharge_dependency = db.Nmdivision_Permisos.Where(x => x.EncargadoID.Value.ToString() == empId).FirstOrDefault().DependenciaID.ToString();
                        break;

                    case "SECCION":
                        employee_incharge_dependency = db.NmSecciones_Permisos.Where(x => x.EncargadoID.Value.ToString() == empId).FirstOrDefault().DependenciaID.ToString();
                        break;

                    default:
                        employee_incharge_dependency = "";
                        break;
                }

            }
            else
            {
                var employee_auxiliar_area = db.Database.SqlQuery<sp_verif_Auxiliar_Result>("sp_verif_Auxiliar {0}", empId).FirstOrDefault();

                RegisterLogs rl3 = new RegisterLogs(empId, "reporte área auxiliar:" + employee_auxiliar_area.Nombre + " " + employee_auxiliar_area.Procedencia);

                if (employee_auxiliar_area.Nombre != "N/A" && employee_auxiliar_area.Procedencia != "N/A")
                {
                    switch (employee_auxiliar_area.Procedencia)
                    {
                        case "DEPENDENCIA":
                            employee_incharge_dependency = db.Nmdependencias_Permisos.Where(x => x.AuxiliarID.Value.ToString() == empId).FirstOrDefault().DependenciaID.ToString();
                            break;

                        case "DEPARTAMENTO":
                            employee_incharge_dependency = db.Nmdeptos_Permisos.Where(x => x.AuxiliarID.Value.ToString() == empId).FirstOrDefault().DependenciaID.ToString();
                            break;

                        case "DIVISION":
                            employee_incharge_dependency = db.Nmdivision_Permisos.Where(x => x.AuxiliarID.Value.ToString() == empId).FirstOrDefault().DependenciaID.ToString();
                            break;

                        case "SECCION":
                            employee_incharge_dependency = db.NmSecciones_Permisos.Where(x => x.AuxiliarID.Value.ToString() == empId).FirstOrDefault().DependenciaID.ToString();
                            break;

                        default:
                            employee_incharge_dependency = "";
                            break;
                    }
                }
            }

            var employees = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.CodigoDependencia == employee_incharge_dependency && (x.CodigoDepartamento == a || a == "0" || a == "") && (x.CodigoDivision == b || b == "0" || b == "0") && (x.CodigoSeccion == c || c == "0" || c == "0")).ToList();

            List<Vw_Mostrar_Personal_Permisos_PGR> to_delete = new List<Vw_Mostrar_Personal_Permisos_PGR>();

            foreach (var item in employees)
            {
                var is_exonerated = db3.EmpleadosNoPonche.Where(x => x.EmpNoPoncheCodigo.ToString() == item.Codigo).FirstOrDefault();

                if (is_exonerated != null)
                {
                    to_delete.Add(item);
                }
                //return Json(new { sucess = true, message = "Esta Persona No Poncha" });
            }

            foreach (var item in to_delete)
            {
                employees.Remove(item);
            }

            if (employees == null)
            {
                ViewBag.noPoncha = db4.MensajePonches.Where(x => x.MensajePonchesId == 5);
            }

            List<List<sp_report_ponches_Result>> employees_ponches_list = new List<List<sp_report_ponches_Result>>();

            foreach (var item in employees)
            {
                var employee_start_date = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == item.Codigo).FirstOrDefault().fechaingreso;

                List<sp_report_ponches_Result> individual_employee_ponches = new List<sp_report_ponches_Result>();

                if (f == "0")
                {
                    individual_employee_ponches = db2.Database.SqlQuery<sp_report_ponches_Result>(string.Format("sp_report_ponches '{0}','{1}',0,0,0,0,{2}", Convert.ToDateTime(d).ToString("yyyy-MM-dd"), Convert.ToDateTime(e).ToString("yyyy-MM-dd"), item.Codigo)).ToList().Where(x => Convert.ToDateTime(x.Fecha_Ponche).Date >= employee_start_date.Date).ToList();
                }
                else if (f == "1")
                {
                    individual_employee_ponches = db2.Database.SqlQuery<sp_report_ponches_Result>(string.Format("sp_report_ponches '{0}','{1}',0,0,0,0,{2}", Convert.ToDateTime(d).ToString("yyyy-MM-dd"), Convert.ToDateTime(e).ToString("yyyy-MM-dd"), item.Codigo)).ToList().Where(x => !string.IsNullOrEmpty(x.Observacion) && !x.Observacion.ToUpper().StartsWith("JUSTIFICADO")).ToList().Where(x => Convert.ToDateTime(x.Fecha_Ponche).Date >= employee_start_date.Date).ToList();
                }

                var ponches_list2 = new List<sp_report_ponches_Result>();

                var begin_date_date_short = Convert.ToDateTime(d).Date;
                var end_date_date_short = Convert.ToDateTime(e).Date;

                var dias_feriados = db4.DiasFeriados.Where(x => x.DiasFeriadosFecha >= begin_date_date_short && x.DiasFeriadosFecha <= end_date_date_short).ToList();

                foreach (var item2 in individual_employee_ponches)
                {
                    bool is_hollyday = false;

                    foreach (var item3 in dias_feriados)
                    {
                        if (item2.Fecha_Ponche.Value.Date == item3.DiasFeriadosFecha.Date)
                            is_hollyday = true;
                    }

                    if (!is_hollyday)
                    {
                        if (ponches_list2.Where(x => x.Fecha_Ponche.Value.ToString("dd/MM/yyyy") == item2.Fecha_Ponche.Value.ToString("dd/MM/yyyy")).FirstOrDefault() == null)
                        {
                            ponches_list2.Add(item2);
                        }
                    }
                }

                employees_ponches_list.Add(ponches_list2);
            }

            ViewBag.employees_ponches_list = employees_ponches_list;

            ViewBag.Dependency = "PROCURADURÍA GENERAL DE LA REPÚBLICA";

            string department = "";

            try
            {
                department = db.vw_Departamentos.Where(x => x.DependenciasID == "10001" && x.DeptoID.ToString() == a).FirstOrDefault().Descripcion;
            }
            catch (Exception)
            {

            }
            finally
            {
                ViewBag.Department = department;
            }

            string division = "";

            try
            {
                division = db.vw_Divisiones.Where(x => x.DependenciaID == 10001 && x.DeptoID.ToString() == a && x.DivisionID.ToString() == b).FirstOrDefault().Descripcion;
            }
            catch (Exception)
            {

            }
            finally
            {
                ViewBag.Division = division;
            }

            string section = "";

            try
            {
                section = db.vw_Secciones.Where(x => x.DependenciaID == 10001 && x.DeptoID == a && x.DivisionID.ToString() == b && x.SeccionID.ToString() == c).FirstOrDefault().Descripcion;
            }
            catch (Exception)
            {

            }
            finally
            {
                ViewBag.Section = section;
            }

            string employee2 = Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(employee2, "Tiró un reporte de ponches de área");

            return new ViewAsPdf("ReportAreas")
            {
                PageOrientation = Orientation.Portrait,
                PageSize = Rotativa.Options.Size.A4,
                PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }
            };
        }

        public List<sp_report_ponches_Result> FindPonches(Vw_Mostrar_Personal_Permisos_PGR employee)
        {
            if (employee != null)
            {
                using (RHLogsEntities1 db2 = new RHLogsEntities1())
                {
                    var fechaInicio = Utilities.EmbeddedDate();

                    var ponches_list = db2.Database.SqlQuery<sp_report_ponches_Result>(string.Format("exec sp_report_ponches '{0}','{1}','{2}','{3}','{4}','{5}','{6}'",
                        fechaInicio.Date.ToString("yyyy-MM-dd"), DateTime.Now.Date.ToString("yyyy-MM-dd"), 0, 0, 0, 0, employee.Codigo)).ToList();

                    employee.FechaInicio = "";
                    employee.FechaFin = "";

                    var ponches_list2 = new List<sp_report_ponches_Result>();

                    var begin_date_date_short = fechaInicio.Date;
                    var end_date_date_short = DateTime.Now.Date;

                    var dias_feriados = db4.DiasFeriados.Where(x => x.DiasFeriadosFecha >= begin_date_date_short && x.DiasFeriadosFecha <= end_date_date_short).ToList();

                    foreach (var item in ponches_list)
                    {
                        bool is_hollyday = false;

                        foreach (var item2 in dias_feriados)
                        {
                            if (item.Fecha_Ponche.Value.Date == item2.DiasFeriadosFecha.Date)
                                is_hollyday = true;
                        }

                        if (!is_hollyday)
                        {
                            if (employee.ponches.Where(x => x.Fecha_Ponche.Value.ToString("dd/MM/yyyy") == item.Fecha_Ponche.Value.ToString("dd/MM/yyyy")).FirstOrDefault() == null)
                            {
                                employee.ponches.Add(item);
                            }
                        }
                    }

                    using (intra2 db = new intra2())
                    {
                        EmpleadosNoPonche enp = db.EmpleadosNoPonche.Where(x => x.EmpNoPoncheCodigo.ToString() == employee.Codigo && x.EmpNoPoncheEstatus == 1).FirstOrDefault();

                        if (enp != null)
                        {
                            employee.ponches = new List<sp_report_ponches_Result>();
                        }

                    }

                    return employee.ponches;
                }
            }
            else
            {
                return null;
            }
        }

        #region Codigo Comentado

        [CustomAuthorize]
        // GET: incautaciones/Edit/5
        //public ActionResult vPoncheEmpleado()
        //{

        //    string empId = HttpContext.Session["empleadoId"].ToString();
        //    int empIdInt = Convert.ToInt32(empId);
        //    ViewBag.EmpleadoId = empIdInt;




        //    var employee = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == empId).FirstOrDefault();

        //    if (employee != null)
        //    {

        //        employee.ponches = FindPonches(employee);

        //        if (employee.ponches.Count == 0)
        //        {
        //            ViewBag.NoPoncha = 1;
        //        }
        //        else
        //        {
        //            ViewBag.NoPoncha = 0;
        //        }

        //        ViewBag.Kpe = (Code.UtilityMethods.SetSHA1(employee.Codigo) + salt);


        //    }
        //    return View(employee);
        //}

        #endregion

        public string GetEmailHTMLBodyReport()
        {
            return Server.MapPath(@"~/Views/sp_report_ponches_Result/ReportPDF.html");
        }
    }
}