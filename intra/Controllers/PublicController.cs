﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models;
using System.IO;
using System.Xml.Linq;
using System.Net;
using Newtonsoft.Json;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
    public class PublicController : Controller
    {
        private dbIntranet db = new dbIntranet();

        #region ValidarActividad
        public bool ValidarActividad(int CodigoSupervisor)
        {
            bool CdgActividad;


            var Valor = db.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSupervisor).FirstOrDefault();

            if (Valor != null)
            {
                CdgActividad = true;
            }
            else
            {
                CdgActividad = false;
            }


            return CdgActividad;

        }
        #endregion

        #region MostrarUsuarioPorActividad
        public int CodigoActividad(int CodigoSupervisor)
        {
            int CdgActividad = 0;

            var Valor = db.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSupervisor).FirstOrDefault();

            CdgActividad = Valor.CodigoActividad;

            return CdgActividad;

        }

        #endregion

        [HttpGet]
        public ActionResult Index()
        {
            string EmpId = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return View(db.Noticias.OrderByDescending(c => c.NoticiaId).Take(3).ToList());
                }
            }
            catch (Exception)
            {
                return View(db.Noticias.OrderByDescending(c => c.NoticiaId).Take(3).ToList());
            }
        }
        
        public PartialViewResult vLecturaDelDia()
        {
            try
            {
                WebRequest webRequest = WebRequest.Create("http://evangeliodeldia.org/rss/v2/evangelizo_rss-sp.xml");
                NetworkCredential nc = new NetworkCredential("Intranet","Cven7777");
                webRequest.Proxy = new WebProxy("http://172.18.1.9:8080/", true, null, nc);
                WebResponse response = webRequest.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);

                //   string lecturaSatanas = reader.ReadToEnd();

                XDocument doc = XDocument.Load(reader);

                string nowFecha = DateTime.Now.ToShortDateString();
                string linkAbsoluto = "main.php?language=SP&module=readings&localdate=" + nowFecha;

                var query = (from feed in doc.Descendants("item")
                                select new Feed
                                {
                                    titulo = feed.Element("title").Value,
                                    link = feed.Element("link").Value + linkAbsoluto,
                                    descripcion = feed.Element("description").Value,

                                }).FirstOrDefault();

                return PartialView(query);
            }
            catch (Exception)
            {
                return PartialView(new Feed() { titulo = "", link = "", descripcion = "" });
            }


        }

        private Boolean ValidarCertificado(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public PartialViewResult PalabraIngles()
        {
            //    System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(ValidarCertificado);

            //    //https://www.merriam-webster.com/wotd/feed/rss2
            //    //https://wordsmith.org/awad/rss1.xml
            //    WebRequest webRequest = WebRequest.Create("http://feeds.feedblitz.com/english-word-of-the-day-for-spanish&x=1.xml");
            //    NetworkCredential nc = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["Usuario"], System.Configuration.ConfigurationManager.AppSettings["Contrasena"]);
            //    webRequest.Proxy = new WebProxy("http://172.18.1.9:8080/", true, null, nc);
            //    WebResponse response = webRequest.GetResponse();
            //    Stream dataStream = response.GetResponseStream();
            //    StreamReader reader = new StreamReader(dataStream);

            //    XDocument doc = XDocument.Load(reader);

            //    string nowFecha = DateTime.Now.ToShortDateString();

            //    var query = (from feed in doc.Descendants("item")
            //                 select new Feed
            //                 {
            //                     titulo = feed.Element("title").Value,
            //                     link = feed.Element("link").Value,
            //                     descripcion = feed.Element("description").Value,

            //                 }).FirstOrDefault();

            return PartialView();
        }

        public JsonResult palabra()
        {
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(ValidarCertificado);

                //http://www.spanishdict.com/wordoftheday/feed

                WebRequest webRequest = WebRequest.Create("http://www.spanishdict.com/wordoftheday/feed");
                NetworkCredential nc = new NetworkCredential("intranet", "Cven7777");
                webRequest.Proxy = new WebProxy("http://172.18.1.9:8080/", true, null, nc);
                WebResponse response = webRequest.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);

                XDocument doc = XDocument.Load(reader);

                string nowFecha = DateTime.Now.ToShortDateString();

                var query = (from feed in doc.Descendants("item")
                                select new Feed
                                {
                                    titulo = feed.Element("title").Value,
                                    link = feed.Element("link").Value,
                                    descripcion = feed.Element("description").Value,

                                }).FirstOrDefault();
                var json = JsonConvert.SerializeObject(query);

                return Json(json, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("Error");
            }


        }
        
        #region MostrarCantidaddeSolicitud
        private void MostrarCantidaddeSolicitud(int CodigoEmpleado)
        {
            int Cdg = 0;

            Cdg = CodigoEmpleado;

            var Cantidad = db.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

            if (Cantidad.Count > 0)
            {

                HttpContext.Session["Cantidad"] = Cantidad.Count();
            }
            else
            {
                HttpContext.Session["Cantidad"] = 0;
            }



        }
        #endregion

        #region MostrarCantidadAsignados
        private void MostrarCantidadAsignados(int CodigoEmpleado)
        {
            int Cdg = 0;

            Cdg = CodigoEmpleado;


            if (ValidarActividad(Cdg) == true)
            {

                int CdgTipoSolicitud = 0;

                CdgTipoSolicitud = CodigoActividad(Cdg);

                var CantidadAsignado = db.Sol_Detalle_Lista.Where(cd => cd.CodigoTipoSolicitud == CdgTipoSolicitud && cd.CodigoEstadoSolicitud == 1 && cd.CodigoTecnico == Cdg).ToList();

                if (CantidadAsignado.Count > 0)
                {

                    HttpContext.Session["CantidadAsignado"] = CantidadAsignado.Count();
                }
                else
                {
                    HttpContext.Session["CantidadAsignado"] = 0;
                }
            }
            else
            {
                var CantidadAsignado = db.Sol_Detalle_Lista.Where(cd => cd.CodigoTecnico == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

                if (CantidadAsignado.Count > 0)
                {

                    HttpContext.Session["CantidadAsignado"] = CantidadAsignado.Count();
                }
                else
                {
                    HttpContext.Session["CantidadAsignado"] = 0;
                }
            }


        }
        #endregion

        #region MostrarCantidadChat
        private void MostrarCantidadChat(int CodigoEmpleado)
        {
            int Cdg = 0;

            Cdg = CodigoEmpleado;

            var CantidadChat = db.Sol_Mostrar_Lista_de_Asignacion_Chat.Where(cd => cd.Estado == true && cd.CodigoUsuario == Cdg).ToList();


            if (CantidadChat.Count > 0)
            {

                HttpContext.Session["CantidadChat"] = CantidadChat.Count();
            }
            else
            {
                HttpContext.Session["CantidadChat"] = 0;
            }



        }
        #endregion

        //#region MostrarCantidaddeSolicitud
        //private void MostrarCantidaddeSolicitud()
        //{
        //    int Cdg = 0;

        //    Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

        //    var Cantidad = db.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

        //    if (Cantidad.Count > 0)
        //    {

        //        HttpContext.Session["Cantidad"] = Cantidad.Count();
        //    }
        //    else
        //    {
        //        HttpContext.Session["Cantidad"] = 0;
        //    }



        //}
        //#endregion

        //#region MostrarCantidadAsignados
        //private void MostrarCantidadAsignados()
        //{
        //    int Cdg = 0;

        //    Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

        //    var CantidadAsignado = db.Sol_Detalle_Lista.Where(cd => cd.CodigoTecnico == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

        //    if (CantidadAsignado.Count > 0)
        //    {

        //        HttpContext.Session["CantidadAsignado"] = CantidadAsignado.Count();
        //    }
        //    else
        //    {
        //        HttpContext.Session["CantidadAsignado"] = 0;
        //    }



        //}
        //#endregion

        //#region MostrarCantidadChat
        //private void MostrarCantidadChat()
        //{
        //    int Cdg = 0;

        //    Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

        //    var CantidadChat = db.Sol_Mostrar_Lista_de_Asignacion_Chat.Where(cd => cd.Estado == true && cd.CodigoUsuario == Cdg).ToList();


        //    if (CantidadChat.Count > 0)
        //    {

        //        HttpContext.Session["CantidadChat"] = CantidadChat.Count();
        //    }
        //    else
        //    {
        //        HttpContext.Session["CantidadChat"] = 0;
        //    }



        //}
        //#endregion
    }

}