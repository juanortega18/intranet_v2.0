﻿using intra.Models;
using intra.Models.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
    [Authorize(Roles = "ASOL,SOL")]
    public class TipoSolicitudesController : Controller
    {
        dbIntranet db = new dbIntranet();

        public ActionResult Index() {

            var lista = new List<Sol_Actividades>();

            if (User.IsInRole("ASOL")) {
                lista = db.Sol_Actividades.Where(x => x.Estado == true && x.SupervisorId > 0).ToList();
            } else {
                var userId = Convert.ToInt32(Session["empleadoId"]);
                lista = db.Sol_Actividades.Where(x => x.Estado == true && x.SupervisorId > 0 && x.SupervisorId == userId).ToList();
            }

            foreach (Sol_Actividades actividad in lista) {
                actividad.Departamento = db.Sol_Departamento.Find(actividad.DepartamentoId);
            }

            return View(lista);
        }

        public ActionResult Detalles(int? id) {

            if (id == null || ValidarActividad(id) == false)
            {
                return RedirectToAction("Index");
            }

            var actividadVM = new TipoSolicitudVM();

            if (User.IsInRole("ASOL"))
            {
                actividadVM.Actividad = db.Sol_Actividades.Find(id);
            }
            else
            {
                //actividad = db.Sol_Actividades.Find(id);

                var userId = Convert.ToInt32(Session["empleadoId"]);

                actividadVM.Actividad = db.Sol_Actividades.Where(x => x.Estado == true && x.SupervisorId > 0 
                && x.SupervisorId == userId && x.SupervisorId == userId && x.ActividadId==id).FirstOrDefault();

                if (actividadVM == null) { return RedirectToAction("Index"); }
            }

            ViewBag.encargado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == actividadVM.Actividad.SupervisorId.ToString()).FirstOrDefault().nombre;
            actividadVM.Actividad.Departamento = db.Sol_Departamento.Find(actividadVM.Actividad.DepartamentoId);
            actividadVM.Actividad.SubActividades = db.Sol_Sub_Actividades.Where(x => x.ActividadId == actividadVM.Actividad.ActividadId).ToList();
            actividadVM.Tecnicos = (from t in db.Sol_Tecnicos_Alternativos
                                    from x in db.VISTA_EMPLEADOS
                                    where x.empleadoid == t.TecnicoId.ToString()
                                    where t.ActividadId == actividadVM.Actividad.ActividadId
                                    select new TecnicoDTO
                                    {
                                        TecnicoID = t.OtrosTecnicos,
                                        Nombre = x.nombre,
                                        Codigo = t.TecnicoId,
                                        FhCreacion = t.FhCreacion,
                                        Estado = t.Estado
                                    }).ToList();
                
            db.Sol_Tecnicos_Alternativos.Where(x => x.ActividadId == actividadVM.Actividad.ActividadId).ToList();

            return View(actividadVM);
        }

        [Authorize(Roles = "ASOL")]
        public ActionResult CrearTipoSolicitud() {
            ViewBag.departamentos = new SelectList(db.Sol_Departamento.Where(x => x.Estado == true), "DepartamentoId", "Descripcion");
            return View();
        }

        [HttpPost]
        public JsonResult CrearTipoSolicitud(int supervisorId, int departamento, string descripcion, string Sub_Actividad)
        {
            if (Sub_Actividad == "") { return Json("Debe ingresar un servicio para esta solicitud"); }
            var solicitud = new Sol_Actividades();
            var persona = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == supervisorId.ToString()).FirstOrDefault();

            if (persona==null) { return Json("Usuario inválido"); }

            solicitud.DepartamentoId = departamento;
            solicitud.SupervisorId = supervisorId;
            solicitud.Correo = BuscarUsuario(persona.cedula).samAccountName;
            solicitud.Descripcion = descripcion;
            solicitud.Estado = true;
            solicitud.Horas = 24;
            solicitud.Fhcreacion = DateTime.Now;

            var resultado = ValidarSolicitud(solicitud);

            if (resultado == "Valido") {

                db.Sol_Actividades.Add(solicitud);
                db.SaveChanges();

                var servicio = new Sol_Sub_Actividades();
                servicio.ActividadId = solicitud.ActividadId;
                servicio.DepartamentoId = departamento;
                servicio.Descripcion = Sub_Actividad;
                servicio.FhCreacion = DateTime.Now;
                servicio.Hora = 24;
                servicio.Estado = true;

                db.Sol_Sub_Actividades.Add(servicio);
                db.SaveChanges();

                return Json(solicitud.ActividadId);

            }

            return Json(resultado);
        }

        [HttpPost]
        public JsonResult CrearServicioSolicitud(int actividad, int departamento, string descripcion)
        {

            var servicio = new Sol_Sub_Actividades();
            servicio.ActividadId = actividad;
            servicio.DepartamentoId = departamento;
            servicio.Descripcion = descripcion;
            servicio.FhCreacion = DateTime.Now;
            servicio.Hora = 24;
            servicio.Estado = true;

            var resultado = ValidarServicio(servicio);

            if (resultado == "Valido") {

                db.Sol_Sub_Actividades.Add(servicio);
                db.SaveChanges();
                return Json(resultado);
            }

            return Json(resultado);
        }

        [Authorize(Roles = "ASOL")]
        public ActionResult EditarTipoSolicitud(int? id) {

            if (id == null) { return RedirectToAction("Index"); }

            var solicitud = db.Sol_Actividades.Find(id);

            if (solicitud == null) { return RedirectToAction("Index"); }

            ViewBag.departamentos = new SelectList(db.Sol_Departamento.Where(x => x.Estado == true), "DepartamentoId", "Descripcion");

            return View(solicitud);
        }

        [HttpPost]
        public JsonResult EditarTipoSolicitud(int id, int supervisorId, int departamento, string descripcion) {

            var actividad = db.Sol_Actividades.Find(id);

            if (actividad == null) { return Json("Un error a ocurrido, favor de refrescar la página"); }
            
            actividad.Descripcion = descripcion;
            actividad.DepartamentoId = departamento;

            if (actividad.SupervisorId != supervisorId)
            {
                var persona = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == supervisorId.ToString()).FirstOrDefault();
                actividad.SupervisorId = supervisorId;
                actividad.Correo = BuscarUsuario(persona.cedula).samAccountName;
            }

            var resultado = ValidarSolicitud(actividad);

            if (resultado == "Valido")
            {
                db.SaveChanges();

                return Json(actividad.ActividadId);
            }

            return Json(resultado);
        }

        public ActionResult EditarServicioSolicitud(int? id) {

            if (id == null) { return RedirectToAction("Index"); }

            var servicio = db.Sol_Sub_Actividades.Find(id);

            if (servicio == null) { return RedirectToAction("Index"); }

            if (!User.IsInRole("ASOL")) {

                var userId = Convert.ToInt32(Session["empleadoId"]);

                var actividad = db.Sol_Actividades.Where(x => x.Estado == true && x.SupervisorId > 0 && x.SupervisorId == userId
                && x.ActividadId == servicio.ActividadId).FirstOrDefault();

                if (actividad == null) { return RedirectToAction("Index"); }

            }

            ViewBag.departamentos = new SelectList(db.Sol_Departamento.Where(x => x.Estado == true), "DepartamentoId", "Descripcion");

            return View(servicio);
        }

        [HttpPost]
        public JsonResult EditarServicioSolicitud(int id, int? actividadId, int? departamento, string descripcion)
        {
            var servicio = db.Sol_Sub_Actividades.Find(id);

            if (servicio == null) { return Json("Un error a ocurrido, favor de refrescar la página"); }

            if (actividadId != null) {

                if (servicio.ActividadId != actividadId)
                {
                    if (db.Sol_Sub_Actividades.Where(x => x.SubActividadId != servicio.SubActividadId
                    && x.ActividadId == servicio.ActividadId && x.Estado == true).ToList().Count == 0)
                    { return Json("Este es el unico servicio activo de esta solicitud, no se puede cambiar de solicitud"); }

                    servicio.ActividadId = (int)actividadId;
                }
            }

            if (departamento != null) { servicio.DepartamentoId = (int)departamento; }

            servicio.Descripcion = descripcion;

            var resultado = ValidarServicio(servicio);

            if (resultado == "Valido")
            {
                db.SaveChanges();

                return Json(servicio.ActividadId);
            }

            return Json(resultado);
        }

        private string ValidarServicio(Sol_Sub_Actividades servicio)
        {
            if (servicio.Descripcion == null || servicio.Descripcion == "") { return "Ingrese el servicio que quiere agregar"; }
            if (!ValidarActividad(servicio.ActividadId)) { return "Ingrese un Tipo de Servicio Válido"; }
            if (!ValidarDepartamento(servicio.DepartamentoId)) { return "Ingrese un Departamento Válido"; }
            if (db.Sol_Actividades.Where(x=>x.DepartamentoId == servicio.DepartamentoId).ToList().Count == 0) { return "Esta solicitud no esta relacionada con el departemento"; }
            if (db.Sol_Sub_Actividades.Where(x=> x.SubActividadId != servicio.SubActividadId && x.Descripcion == servicio.Descripcion && x.DepartamentoId == servicio.DepartamentoId 
            && x.ActividadId == servicio.ActividadId).ToList().Count > 0) { return "Este Servicio ya ha sido registrada"; }

            return "Valido";
        }

        private string ValidarSolicitud(Sol_Actividades solicitud)
        {
            if (solicitud.Descripcion == "") { return "Ingrese el nombre del tipo de solicitud"; }
            if (!ValidarDepartamento(solicitud.DepartamentoId)) { return "Ingrese un Departamento Válido"; }
            if (db.Sol_Actividades.Where(x => x.ActividadId != solicitud.ActividadId && x.Descripcion == solicitud.Descripcion
            ).ToList().Count > 0) { return "Esta Solicitud ya ha sido registrada"; }
            return "Valido";
        }

        private bool ValidarActividad(int? id)
        {
            var actividad = db.Sol_Actividades.Find(id);

            if (actividad != null) { return true; }

            return false;
        }

        private bool ValidarDepartamento(int? id) {
            var actividad = db.Sol_Departamento.Where(x => x.Estado == true && x.DepartamentoId == id).FirstOrDefault();

            if (actividad != null) { return true; }

            return false;
        }

        [AllowAnonymous]
        public JsonResult GetEmpleado(string codigoEmpleado)
        {
            sp_users_ldapID empleado = Code.Utilities2.obtenerSp_UserPorCodigo(codigoEmpleado);
            return Json(empleado);
        }

        public JsonResult GetActividades(int DepartamentoId) {

            var actividades = db.Sol_Actividades.Where(x=>x.DepartamentoId == DepartamentoId && x.Estado == true).OrderBy(a => a.ActividadId).Select(x => new {
                Value = x.ActividadId,
                Text = x.Descripcion
            }).ToList();

            return Json(actividades);
        }

        [HttpPost]
        public JsonResult CambiarEstado(int id)
        {
            Sol_Sub_Actividades servicio = db.Sol_Sub_Actividades.Find(id);

            if (servicio == null) { return Json("Error"); }

            if (servicio.Estado == true && db.Sol_Sub_Actividades.Where(x => x.Estado == true && x.ActividadId == servicio.ActividadId).ToList().Count <= 1)
            { return Json("Incambiable"); }

            servicio.Estado = !servicio.Estado;
            db.SaveChanges();

            if (servicio.Estado == true) { return Json("Activado"); }

            return Json("Desactivado");
        }

        public SP_USERS_LDAP BuscarUsuario(string cedula)
        {
            SP_USERS_LDAP empleado = new SP_USERS_LDAP();

            SqlConnection Enlace = new SqlConnection(ConfigurationManager.ConnectionStrings["intra"].ConnectionString);

            if (Enlace.State == ConnectionState.Closed)
            {
                Enlace.Open();
            }

            SqlCommand cmd = new SqlCommand("sp_users_ldapID", Enlace);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@id", cedula);
            SqlDataAdapter Sda = new SqlDataAdapter(cmd);
            DataTable Dta = new DataTable();
            Sda.Fill(Dta);

            empleado.employeeid = Dta.Rows[0]["employeeid"].ToString();
            empleado.department = Dta.Rows[0]["department"].ToString();
            empleado.displayName = Dta.Rows[0]["displayName"].ToString();
            empleado.samAccountName = Dta.Rows[0]["samAccountName"].ToString();

            return empleado;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}