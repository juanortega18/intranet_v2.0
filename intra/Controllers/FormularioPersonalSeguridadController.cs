﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using PagedList;
using System.Web.Hosting;
using intra;
using intra.Controllers;
using System.Configuration;
using System.Globalization;
using intra.Models;
using intra.Models.PersonalSeguridad;
using intra.Models.vw_Models;



namespace intra.Controllers
{
    public class FormularioPersonalSeguridadController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: FormularioPersonalSeguridad
        [CustomAuthorize]
        [Authorize(Roles = "PS,PSA,PSWR")]
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo de Registro de Personal de Seguridad");
            return View(db.FormularioPersonalSeguridads.ToList());

        }

        [HttpPost]
        public JsonResult obtenerDepartamento(string dependencia)
        {

            //int cl = int.Parse(dependencia);

            var Departamento = db.vw_Departamentos.Where(x => x.DependenciasID == dependencia).Select(a => new SeguridadVm { Id = a.DeptoID, Nombre = a.Descripcion }).ToList();
            return Json(Departamento);
        }


        //Json que Consulta Informacion de los Rangos para llenar DropDown Dinamicamente..
        [HttpGet]

        public JsonResult GetRango(int id)
        {

            List<SelectListItem> listRangos = new List<SelectListItem>();
            if (id < 1)
            {

                RedirectToAction("Create");

            }

            var Rangos = (from r in db.Rangos
                          from i in db.Institucions
                          from ri in db.RangoInstitucions
                          where i.InstitucionID == ri.InstitucionID
                          where r.RangoId == ri.RangoId
                          where i.InstitucionID == id
                          select new
                          {
                              r.RangoNombre,
                              r.RangoId

                          }).ToList();

            foreach (var rango in Rangos)
            {
                listRangos.Add(new SelectListItem { Text = rango.RangoNombre.ToString(), Value = rango.RangoId.ToString() });
            }

            return Json(new SelectList(listRangos, "value", "Text"), JsonRequestBehavior.AllowGet);
        }


        // GET: FormularioPersonalSeguridad/Details/5
        [CustomAuthorize]
        [Authorize(Roles = "PS,PSA,PSWR")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormularioPersonalSeguridads formularioPersonalSeguridad = db.FormularioPersonalSeguridads.Find(id);
            if (formularioPersonalSeguridad == null)
            {
                return HttpNotFound();
            }
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo de Registro de Personal de Seguridad al detalle de (" + formularioPersonalSeguridad.Nombre + ")");

            if (string.IsNullOrEmpty(formularioPersonalSeguridad.DependenciaID.ToString())) { ViewBag.deptDescripcion = "No Tiene Depepartamento asignado"; }
            else
            {
                ViewBag.deptDescripcion = db.vw_Departamentos.Where(x => x.DependenciasID == formularioPersonalSeguridad.DependenciaID.ToString() && x.DeptoID == formularioPersonalSeguridad.DeptoID).FirstOrDefault().Descripcion;
            }
            if (string.IsNullOrEmpty(formularioPersonalSeguridad.DependenciaID.ToString())) { ViewBag.Dependencia = "No Tiene Dependencia asignada"; }
            else
            {
                ViewBag.Dependencia = db.vw_Dependencias.Where(x => x.DependenciaID == formularioPersonalSeguridad.DependenciaID).FirstOrDefault().Descripcion;

            }



            return View(formularioPersonalSeguridad);
        }
        //Get Cedula
        //public ActionResult Cedula(string cedula)
        //{
        //    //FormularioPersonalSeguridads formulario = new FormularioPersonalSeguridads();
        //    return View (db.FormularioPersonalSeguridads.Where(x => x.Cedula == cedula));

        //}

        // GET: FormularioPersonalSeguridad/Create
        [CustomAuthorize]
        [Authorize(Roles = "PSA,PSWR")]
        public ActionResult Create()
        {
           Institucions inst = new Institucions();
            ViewBag.Departamentos = new SelectList(db.vw_Departamentos, "DeptoID", "Descripcion");
            ViewBag.Dependencia = new SelectList(db.vw_Dependencias, "DependenciaID", "Descripcion");
            ViewBag.Rango = new SelectList(db.Rangos, "RangoId", "RangoNombre");
            ViewBag.Institucion = new SelectList(db.Institucions, "InstitucionID", "InstitucionNombre");


            return View();
        }

        // POST: FormularioPersonalSeguridad/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "PSA,PSWR")]
        public ActionResult Create([Bind(Include = "FormId,Nombre,Apellido,Cedula,RangoId,EstadoAsignacion,InstitucionID,FechaNacimiento,Telefono,Correo,TipoSangre,Direccion,TelefonoEmergencia,Designacion,ArmaFuego,ArmaMarca,ArmaModelo,ArmaSerie,FechaIngreso,FechaSalida,Estado,ObservacionGeneral")]HttpPostedFileBase file, FormCollection fc, FormularioPersonalSeguridads formularioPersonalSeguridad)
        {
            ViewBag.Guardar = "False";

            string ruta = (HttpRuntime.AppDomainAppVirtualPath); // "~\intra\Content\images\postImages\";
            //string ruta = string.Concat(System.Configuration.ConfigurationManager.AppSettings["intranet"], "Content/images/PersonalSeguridad/");

            if (file != null && file.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath(ruta), Path.GetFileName(file.FileName));
                file.SaveAs(path);
                formularioPersonalSeguridad.Imagen = ruta + file.FileName;
            }

            if (ModelState.IsValid)

            {
                formularioPersonalSeguridad.DependenciaID = int.Parse(fc["DependenciaId"].ToString());
                formularioPersonalSeguridad.DeptoID = int.Parse(fc["DeptoID"].ToString());
                db.FormularioPersonalSeguridads.Add(formularioPersonalSeguridad);
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                ViewBag.Guardar = "Ok";

                RegisterLogs rl = new RegisterLogs(empId, "Creo un nuevo registro en el Modulo de Registro de Personal de Seguridad (" + formularioPersonalSeguridad.Nombre + ")");
                return RedirectToAction("Index");
            }

            var error = ModelState.Values.SelectMany(v => v.Errors);
            ViewBag.Departamentos = new SelectList(db.vw_Departamentos, "DeptoID", "Descripcion");
            ViewBag.Dependencia = new SelectList(db.vw_Dependencias, "DependenciaID", "Descripcion");
            ViewBag.Rango = new SelectList(db.Rangos, "RangoId", "RangoNombre");
            ViewBag.Institucion = new SelectList(db.Institucions, "InstitucionID", "InstitucionNombre");
            return View(("Create"), formularioPersonalSeguridad);
        }


        // GET: FormularioPersonalSeguridad/Edit/5
        [CustomAuthorize]
        [Authorize(Roles = "PSA,PSWR")]
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormularioPersonalSeguridads formularioPersonalSeguridad = db.FormularioPersonalSeguridads.Find(id);
            ViewBag.DeptoID = new SelectList(db.vw_Departamentos, "DeptoID", "Descripcion", formularioPersonalSeguridad.DeptoID);
            ViewBag.DependenciaID = new SelectList(db.vw_Dependencias, "DependenciaID", "Descripcion", formularioPersonalSeguridad.DependenciaID);
            ViewBag.RangoId = new SelectList(db.Rangos, "RangoId", "RangoNombre", formularioPersonalSeguridad.RangoId);
            ViewBag.InstitucionID = new SelectList(db.Institucions, "InstitucionID", "InstitucionNombre", formularioPersonalSeguridad.InstitucionID);


            if (formularioPersonalSeguridad == null)
            {
                return HttpNotFound();
            }
            return View(formularioPersonalSeguridad);
        }

        // POST: FormularioPersonalSeguridad/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "PSA,PSWR")]
        public ActionResult Edit([Bind(Include = "FormId,Nombre,Apellido,Cedula,RangoId,EstadoAsignacion,InstitucionID,FechaNacimiento,Telefono,Correo,TipoSangre,Direccion,TelefonoEmergencia,Designacion,ArmaFuego,ArmaMarca,ArmaModelo,ArmaSerie,FechaIngreso,FechaSalida,Estado,ObservacionGeneral")]HttpPostedFileBase file, FormCollection fc, FormularioPersonalSeguridads formularioPersonalSeguridad)
        {
            string ruta = (HttpRuntime.AppDomainAppVirtualPath);
            // string ruta = string.Concat(System.Configuration.ConfigurationManager.AppSettings["intranet"], "Content/images/PersonalSeguridad/");

            //tutoriales.video = vd["__contenido"];

            if (file != null && file.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath(ruta), Path.GetFileName(file.FileName));
                file.SaveAs(path);
                formularioPersonalSeguridad.Imagen = ruta + file.FileName;
            }
            if (ModelState.IsValid)
            {
                formularioPersonalSeguridad.DependenciaID = int.Parse(fc["DependenciaID"].ToString());
                formularioPersonalSeguridad.DeptoID = int.Parse(fc["DeptoID"].ToString());
                db.Entry(formularioPersonalSeguridad).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito un registro en el Modulo de Registro de Personal de Seguridad id(" + formularioPersonalSeguridad.FormId + ")");
                return RedirectToAction("Index");
            }

            var erros = ModelState.Values.SelectMany(v => v.Errors);
            ViewBag.DeptoID = new SelectList(db.vw_Departamentos, "DeptoID", "Descripcion");
            ViewBag.DependenciaID = new SelectList(db.vw_Dependencias, "DependenciaID", "Descripcion");
            ViewBag.RangoId = new SelectList(db.Rangos, "RangoId", "RangoNombre");
            ViewBag.InstitucionID = new SelectList(db.Institucions, "InstitucionID", "InstitucionNombre");
            return View("Index", formularioPersonalSeguridad);

        }

        // GET: FormularioPersonalSeguridad/Delete/5
        [CustomAuthorize]
        [Authorize(Roles = "PSA")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormularioPersonalSeguridads formularioPersonalSeguridad = db.FormularioPersonalSeguridads.Find(id);
            if (formularioPersonalSeguridad == null)
            {
                return HttpNotFound();
            }
            return View(formularioPersonalSeguridad);
        }

        // POST: FormularioPersonalSeguridad/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "PSA")]
        public ActionResult DeleteConfirmed(int id)
        {
            FormularioPersonalSeguridads formularioPersonalSeguridad = db.FormularioPersonalSeguridads.Find(id);
            db.FormularioPersonalSeguridads.Remove(formularioPersonalSeguridad);
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Elimino un registro en el Modulo de Personal de Seguridad llamado (" + formularioPersonalSeguridad.Nombre + " " + formularioPersonalSeguridad.Apellido + " " + "Cedula:" + formularioPersonalSeguridad.Cedula + ")");
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

