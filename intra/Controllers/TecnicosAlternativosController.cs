﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.ViewModel;

namespace intra.Controllers
{
    public class TecnicosAlternativosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: TecnicosAlternativos
        public ActionResult Index()
        {
            var sol_Tecnicos_Alternativos = db.Sol_Tecnicos_Alternativos.Include(s => s.Sol_Actividades);
            return View(sol_Tecnicos_Alternativos.ToList());
        }

        // GET: TecnicosAlternativos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sol_Tecnicos_Alternativos sol_Tecnicos_Alternativos = db.Sol_Tecnicos_Alternativos.Find(id);
            if (sol_Tecnicos_Alternativos == null)
            {
                return HttpNotFound();
            }
            return View(sol_Tecnicos_Alternativos);
        }
        [HttpPost]
        public JsonResult SelecccionarSupervisor(int? id)
        {
            
            var  supervisor = db.Sol_Actividades.Where(x => x.ActividadId == id).Select(x => new SupervisoresVM { ActividadId = x.ActividadId, SupervisorId = x.SupervisorId, Usuario = x.Correo}).FirstOrDefault();

            return Json( supervisor);
        }

        [HttpPost]
        public JsonResult BuscarEmpleado(string Codigo)
        {
            
            var empleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == Codigo).Select(x => new EmpleadosVM { EmpleadoId = x.empleadoid, Nombre = x.nombre }).FirstOrDefault();

            return Json(empleado);
        }


        // GET: TecnicosAlternativos/Create
        public ActionResult Create()
        {
            ViewBag.ActividadId = new SelectList(db.Sol_Actividades, "ActividadId", "Descripcion");
            ViewBag.SupervisorId = (from i in db.Sol_Actividades
                                  select new SupervisoresVM
                                  {
                                      ActividadId = i.ActividadId,
                                      SupervisorId = i.SupervisorId,
                                      Usuario = i.Correo
                                  }).ToList();
            return View();
        }

        // POST: TecnicosAlternativos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OtrosTecnicos,ActividadId,SupervisorId,TecnicoId,Estado,FhCreacion,DependenciaRegionalId")] Sol_Tecnicos_Alternativos sol_Tecnicos_Alternativos, FormCollection fc)
        {
            if (ModelState.IsValid)
            {
                sol_Tecnicos_Alternativos.TecnicoId = int.Parse(fc["TecnicoId"].ToString());
                sol_Tecnicos_Alternativos.FhCreacion = DateTime.Now;
                sol_Tecnicos_Alternativos.Estado = bool.Parse(fc["Estado"].ToString());
                sol_Tecnicos_Alternativos.SupervisorId = int.Parse(fc["SupervisorId"].ToString());
                db.Sol_Tecnicos_Alternativos.Add(sol_Tecnicos_Alternativos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SupervisorId = (from i in db.Sol_Actividades
                                  select new SupervisoresVM
                                  {
                                      ActividadId = i.ActividadId,
                                      SupervisorId = i.SupervisorId,
                                      Usuario = i.Correo
                                  }).ToList();

            ViewBag.ActividadId = new SelectList(db.Sol_Actividades, "ActividadId", "Descripcion", sol_Tecnicos_Alternativos.ActividadId);
             

            return View(sol_Tecnicos_Alternativos);
        }

        [HttpPost]
        public JsonResult CreateJson(int actividadId , int tecnicoId ) {

            var actividad = db.Sol_Actividades.Find(actividadId);

            if (actividad == null) { return Json("Un error a ocurrido, favor de refrescar la página"); }

            var persona = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == tecnicoId.ToString()).FirstOrDefault();

            if (persona == null) { return Json("Un error a ocurrido, la persona no fue encontrada"); }

            if (db.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId == tecnicoId && x.ActividadId == actividadId).ToList().Count >= 1)
            { return Json("Esta persona ya es un Tecnico Alternativo para esta solcitud"); }

            var tecnico = new Sol_Tecnicos_Alternativos();

            tecnico.ActividadId = actividadId;
            tecnico.SupervisorId = actividad.SupervisorId;
            tecnico.TecnicoId = tecnicoId;
            tecnico.Estado = true;
            tecnico.FhCreacion = DateTime.Now;

            db.Sol_Tecnicos_Alternativos.Add(tecnico);
            db.SaveChanges();

            return Json("Guardado");

        }

        // GET: TecnicosAlternativos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sol_Tecnicos_Alternativos sol_Tecnicos_Alternativos = db.Sol_Tecnicos_Alternativos.Find(id);
            if (sol_Tecnicos_Alternativos == null)
            {
                return HttpNotFound();
            }
            ViewBag.ActividadId = new SelectList(db.Sol_Actividades, "ActividadId", "Descripcion", sol_Tecnicos_Alternativos.ActividadId);
            return View(sol_Tecnicos_Alternativos);
        }

        // POST: TecnicosAlternativos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OtrosTecnicos,ActividadId,SupervisorId,TecnicoId,Estado,FhCreacion,DependenciaRegionalId")] Sol_Tecnicos_Alternativos sol_Tecnicos_Alternativos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sol_Tecnicos_Alternativos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ActividadId = new SelectList(db.Sol_Actividades, "ActividadId", "Descripcion", sol_Tecnicos_Alternativos.ActividadId);
            return View(sol_Tecnicos_Alternativos);
        }


        // GET: TecnicosAlternativos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sol_Tecnicos_Alternativos sol_Tecnicos_Alternativos = db.Sol_Tecnicos_Alternativos.Find(id);
            if (sol_Tecnicos_Alternativos == null)
            {
                return HttpNotFound();
            }
            return View(sol_Tecnicos_Alternativos);
        }

        // POST: TecnicosAlternativos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sol_Tecnicos_Alternativos sol_Tecnicos_Alternativos = db.Sol_Tecnicos_Alternativos.Find(id);
            sol_Tecnicos_Alternativos.Estado = false;
            db.Sol_Tecnicos_Alternativos.Add(sol_Tecnicos_Alternativos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize(Roles = "ASOL,SOL")]
        public JsonResult CambiarEstadoTecnico(int id)
        {
            Sol_Tecnicos_Alternativos tecnico = db.Sol_Tecnicos_Alternativos.Find(id);

            if (tecnico == null) { return Json("Error"); }

            tecnico.Estado = !tecnico.Estado;
            db.SaveChanges();

            if (tecnico.Estado == true) { return Json("Activado"); }

            return Json("Desactivado");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
