﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using System.IO;
using PagedList;

namespace intra.Controllers
{
    public class noticiasController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: noticias   
        public ActionResult Index(int? page)
        {
            string empId;

            try
            {
                if(!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                {
                    empId = HttpContext.Session["empleadoId"].ToString();
                }
                else
                {
                    empId = "public_user";
                }
            }
            catch(Exception)
            {
                empId = "public_user";
            }

            ViewBag.access = empId;

            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo de Noticias");

            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }

            string EmpId = string.Empty;


           
            //Modulo de Noticias
            int pageSize = 4;
            int pageNumber = (page ?? 1);
            var noticias = db.Noticias.OrderByDescending(c => c.NoticiaId).Take(10);
            return View(noticias.ToPagedList(pageNumber,pageSize));
        }

        // GET: noticias/Details/5
        
        [Authorize(Roles="NS,NSA,NSWR")]
        [CustomAuthorize]
        public ActionResult gestorNoticias()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al gestor de Noticias");
            //Lista Descendete de Noticias
            return View(db.Noticias.OrderByDescending(c => c.NoticiaId).ToList());
        }

        // GET: noticias/Create
        [CustomAuthorize]
        [Authorize(Roles = "NSA,NSWR")]
        public ActionResult Create()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Create de Noticias");
            return View();
        }

        // POST: noticias/Create     
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "NSA,NSWR")]
        public ActionResult Create(HttpPostedFileBase file, FormCollection fC)
        {
            Noticias noticias = new Noticias();
           
            //Ruta Para Guardar Imagen en la BD y Combinar con la el Path Fisico
            string ruta = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/postImages/";

            //if (fC["Titulo"] != "" && fC["__contenido"] != "")

                if (fC["Titulo"] != "" && fC["__contenido"] != "")
                {
                noticias.NoticiaTitulo = fC["Titulo"];
                noticias.NoticiaCuerpo = fC["__contenido"];
                noticias.NoticiaPieImages = fC["PieImages"];
                noticias.NoticiaFechaCreacion = DateTime.Now;
                noticias.NoticiasUrl = "Interna";
                noticias.NoticiaAutor = HttpContext.Session["empleadoId"].ToString();

                //Validar si el file es diferente de nulo para procesar la carga
                if (file != null && file.ContentLength > 0)
                {
                    string path = HttpRuntime.AppDomainAppPath + "Content\\images\\postImages\\" + file.FileName;
                    file.SaveAs(path);
                    noticias.NoticiaImages = ruta + file.FileName;
                }
                else
                {
                    //Imagen por Defecto en caso de no subir ninguna
                 
                    //noticias.NoticiaImages = string.Concat(System.Configuration.ConfigurationManager.AppSettings["intranet"], "Content/images/postImages/logo.png");
                }

                if (ModelState.IsValid)
                {
                    db.Noticias.Add(noticias);
                    db.SaveChanges();
                    string empId = HttpContext.Session["empleadoId"].ToString();
                    RegisterLogs rl = new RegisterLogs(empId, "Creo Una Noticia");
                    return RedirectToAction("Index");
                }
            }
                else
            {
                noticias.NoticiaTitulo = fC["Titulo"];
                noticias.NoticiaCuerpo = fC["__contenido"];
                noticias.NoticiaPieImages = fC["PieImages"];
                noticias.NoticiaFechaCreacion = DateTime.Now;
                noticias.NoticiasUrl = fC["url"];
                noticias.NoticiaAutor = HttpContext.Session["empleadoId"].ToString();

                //Validar si el file es diferente de nulo para procesar la carga
                if (file != null && file.ContentLength > 0)
                {
                    string path = HttpRuntime.AppDomainAppPath + "Content\\images\\postImages\\" + file.FileName;
                    file.SaveAs(path);
                    noticias.NoticiaImages = ruta + file.FileName;
                }
                else
                {
                    //Imagen por Defecto en caso de no subir ninguna

                    //noticias.NoticiaImages = string.Concat(System.Configuration.ConfigurationManager.AppSettings["intranet"], "Content/images/postImages/logo.png");
                }

                if (ModelState.IsValid)
                {
                    db.Noticias.Add(noticias);
                    db.SaveChanges();
                    string empId = HttpContext.Session["empleadoId"].ToString();
                    RegisterLogs rl = new RegisterLogs(empId, "Creo Una Noticia");
                    return RedirectToAction("Index");
                }

            }

            return View("Create", noticias);
        }

        // GET: noticias/Edit/5
        [CustomAuthorize]
        [Authorize(Roles = "NSA,NSWR")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Noticias noticias = db.Noticias.Find(id);

            if(noticias.NoticiasUrl != "Interna")
            {
                TempData["NoticiaExterna"] = "Ok";
            }
            if (noticias == null)
            {
                return HttpNotFound();
            }
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Edit de Noticias");
            return View(noticias);
        }

        // POST: noticias/Edit/5  
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "NSA,NSWR")]
        public ActionResult Edit(HttpPostedFileBase file, FormCollection fC)
        {           
            //Id de la tabla para el update
            int noticiasId = Convert.ToInt16(fC["NoticiaId"]);

            //Ruta Para Guardar Imagen en la BD y Combinar con la el Path Fisico
            string ruta = HttpRuntime.AppDomainAppVirtualPath + "/Content/images/postImages/";

            //Find para Procesar el Update
            Noticias noticias = db.Noticias.Find(noticiasId);
            noticias.NoticiaTitulo = fC["Titulo"];
            noticias.NoticiaCuerpo = fC["__contenido"];
            noticias.NoticiaPieImages = fC["PieImages"];
            noticias.NoticiasUrl = fC["url"];
            //noticias.NoticiaAutor = Session["nombre"].ToString();
            //noticias.fechaCreacion = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"));
            noticias.NoticiaFechaCreacion = noticias.NoticiaFechaCreacion;
                        
            //Validar si el file es diferente de nulo para procesar la carga
            if (file != null && file.ContentLength > 0)
            {

                string path = HttpRuntime.AppDomainAppPath + "Content\\images\\postImages\\" + file.FileName;
                file.SaveAs(path);
              
                noticias.NoticiaImages = ruta + file.FileName;
            }
            else
            {
                noticias.NoticiaImages = noticias.NoticiaImages;
            }
            
            if (ModelState.IsValid)
            {
                db.Entry(noticias).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito la noticia id("+noticias.NoticiaId+")");
                return RedirectToAction("Index");
            }

            return View(noticias);
        }


        public ActionResult viewNews(int? id)
        {
            string empId;
            
            try
            {
                if(!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                {
                    empId = HttpContext.Session["empleadoId"].ToString();
                }else
                {
                    empId = "public_user";
                }
            }
            catch(Exception)
            {
                empId = "public_user";
            }

            ViewBag.access = empId;

            Noticias noticias = db.Noticias.Find(id);

            if (noticias.NoticiasUrl == "" || noticias.NoticiasUrl == "Interna")
            {

                var Vista = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == noticias.NoticiaAutor).Select(x => x.nombre).FirstOrDefault().ToLower();
                ViewBag.Autor = Vista;

                noticias.NoticiaVistas = noticias.NoticiaVistas + 1;
                db.SaveChanges();

                RegisterLogs rl = new RegisterLogs(empId, "Ingreso a la noticia id(" + noticias.NoticiaId + ")");
                return View(noticias);
            }
            else
                //RegisterLogs rl = new RegisterLogs(empId, "Ingreso a la noticia Externa);

            // RegisterLogs rl1 = new RegisterLogs(empId, "Ingreso a la noticia id(" + noticias.NoticiaId + ")"); 
            return  Redirect(noticias.NoticiasUrl);
        }


        // GET: noticias/Delete/5
        [CustomAuthorize]
        [Authorize(Roles = "NSA")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Noticias noticias = db.Noticias.Find(id);
            if (noticias == null)
            {
                return HttpNotFound();
            }

            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Delete de la noticia");
            return View(noticias);
        }

        // POST: noticias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "NSA")]
        public ActionResult DeleteConfirmed(int id)
        {
            Noticias noticias = db.Noticias.Find(id);
            db.Noticias.Remove(noticias);
            db.SaveChanges();
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Elimino la noticia id("+noticias.NoticiaId+")");
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
