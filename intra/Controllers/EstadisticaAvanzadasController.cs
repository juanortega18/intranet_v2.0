﻿using intra.Models;
using intra.Utilidades;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Web.Mvc;
using intra.Models.ViewModel;
using intra.Code;
using System.Globalization;
using System.Web.Script.Serialization;
using Rotativa;
using Rotativa.Options;

namespace intra.Controllers
{
    public class EstadisticaAvanzadasController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: EstadisticaAvanzadas
        [CustomAuthorize]
        public ActionResult Index()
        {
            return RedirectToAction("cantidades");
            //return View();
        }

        //[Authorize(Roles = "EA")]
        //[CustomAuthorize]
        public ActionResult Cantidades()
        {
            string codigo_empleado = HttpContext.Session["empleadoId"].ToString();

            string depto_empleado = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == codigo_empleado).FirstOrDefault().CodigoDepartamento;

            ViewBag.Dependencias = new SelectList(db.Sol_DependenciasRegionales, "DependenciaId", "DependenciaNombre");

         
       
            ViewBag.solicitante = (from mostrarPersonaPermiso in db.Vw_Mostrar_Personal_Permisos_PGR
                                   join registrosolicitud in db.Sol_Registro_Solicitud
                                   on mostrarPersonaPermiso.Codigo equals registrosolicitud.SolicitanteId.ToString()
                                   select new SolicitanteViewModel() { NombreCompleto = mostrarPersonaPermiso.NombreCompleto, SolicitanteId = registrosolicitud.SolicitanteId }).Distinct().ToList();

            ViewBag.responsable = (from mostrarPersonaPermiso in db.Vw_Mostrar_Personal_Permisos_PGR
                                   join registrosolicitud in db.Sol_Registro_Solicitud
                                   on mostrarPersonaPermiso.Codigo equals registrosolicitud.TecnicoId.ToString()
                                   select new TecnicoViewModel() { NombreCompleto = mostrarPersonaPermiso.NombreCompleto, TecnicoId = registrosolicitud.TecnicoId }).Distinct().ToList();

            //------Cargar tipo de servicios (Actividades)-------------------------------------------------
            List<Sol_Actividades> tipo_servicios = new List<Sol_Actividades>();

            var post_servicios = db.Sol_Actividades.Where(x => x.DepartamentoId.ToString() == depto_empleado && (x.ActividadId != 34 && x.ActividadId != 1019)).ToList();

            tipo_servicios.Add(new Sol_Actividades() { ActividadId = 0, Descripcion = "Seleccione" });

            foreach (var item in post_servicios)
            {
                tipo_servicios.Add(item);
            }

            ViewBag.TipoServicios = new SelectList(tipo_servicios, "ActividadId", "Descripcion");

            //-----Cargar dependencias------------------------------------------------------------------------
            //List<Sol_DependenciasRegionales> dependencias = new List<Sol_DependenciasRegionales>();

            //dependencias.Add(new Sol_DependenciasRegionales() { DependenciaId = 0, DependenciaNombre = "Seleccione" });

            //var listaDependencias = db.Sol_DependenciasRegionales.Where(x => x.DependenciaId == 1).ToList();
            //ViewBag.Dependencias = new SelectList(dependencias, "DependenciaId", "DependenciaNombre");


            //------Cargar áreas de servicios (SubActividades)-------------------------------------------------
            List<Sol_Sub_Actividades> area_servicios = new List<Sol_Sub_Actividades>();

            area_servicios.Add(new Sol_Sub_Actividades() { SubActividadId = 0, Descripcion = "Seleccione" });

            ViewBag.AreaServicios = new SelectList(area_servicios, "SubActividadId", "Descripcion");


            //------Cargar estados de servicios------------------------------------------------------------------
            List<Sol_Estado_Solicitud> estados_servicios = new List<Sol_Estado_Solicitud>();

            var post_estados = db.Sol_Estado_Solicitud.ToList();

            estados_servicios.Add(new Sol_Estado_Solicitud() { EstadoId = 0, Descripcion = "Seleccione" });

            foreach (var item in post_estados)
            {
                estados_servicios.Add(item);
            }

            ViewBag.EstadoSolicitud = new SelectList(estados_servicios, "EstadoId", "Descripcion");

            return View();

            #region CommentedCode
            if (Session["departamento"] == null)
                return Redirect("~/login");

            // string emp_id = (string)Session["empleadoId"];

            //DROPDOWN para filtrar los graficos por sus respectivas gerencias(Departamentos)
            ViewBag.dropDownDept = new SelectList(db.Sol_Departamento.Where(x => x.Estado == true), "Departamentoid", "descripcion");


            // select MONTH(FhFinalSolicitud) as meses  from[Sol_Registro_Solicitud] where year((select top 1 FhFinalSolicitud
            //from Sol_Registro_Solicitud order by FhFinalSolicitud desc)) = year(FhFinalSolicitud) group by MONTH(FhFinalSolicitud)

            //select YEAR(FhFinalSolicitud) as anno from Sol_Registro_Solicitud group by YEAR(FhFinalSolicitud) order by YEAR(FhFinalSolicitud) desc

            int ultimoAnno = (from t in db.Sol_Registro_Solicitud
                              orderby t.FhFinalSolicitud.Year descending
                              select t.FhFinalSolicitud.Year).FirstOrDefault();

            var mes = (from t in db.Sol_Registro_Solicitud
                       where t.FhFinalSolicitud.Year == ultimoAnno
                       group t by new { t.FhFinalSolicitud.Month } into grp
                       orderby grp.Key.Month
                       select new { meses = grp.Key.Month, mesesId = grp.Key.Month }).ToList();

            var anno = (from t in db.Sol_Registro_Solicitud
                        group t by new { t.FhFinalSolicitud.Year } into grp
                        orderby grp.Key.Year descending
                        select new { anno = grp.Key.Year, annoId = grp.Key.Year }).ToList();

            ViewBag.meses = new SelectList(mes, "meses", "mesesId");
            ViewBag.annos = new SelectList(anno, "anno", "annoId");

            //GRAFICAS DE CANTIDADES//

            //Gráfica de los Servicios de los Ultimos 7 dias//
            var datosCantGrafica1 = UtilidadesEstadistica.servUlt7Dias(db);
            ViewBag.datosCantGrafica1 = datosCantGrafica1;
            //Gráfica Tiempo Respuesta de los Servicios de los Ultimos 7 dias //
            var datosCantGrafica2 = UtilidadesEstadistica.tiempoRespuesta(db);
            ViewBag.datosCantGrafica2 = datosCantGrafica2;
            //Gráfica de los Servicios mensuales //
            var datosCantGrafica3 = UtilidadesEstadistica.solicitudesXmes(db);
            ViewBag.datosCantGrafica3 = datosCantGrafica3;
            //Gráfica del Tiempo excedido de los Tecnicos(Mensual)  //
            var datosCantGrafica4 = UtilidadesEstadistica.tiempoRespuestaXmes(db);
            ViewBag.datosCantGrafica4 = datosCantGrafica4;

            ViewBag.ruta = HttpRuntime.AppDomainAppVirtualPath;
            #endregion
        }

        public JsonResult GetSolicitante(int solicitanteId)
        {

            var query = (from solicitud in db.Sol_Registro_Solicitud
                         join permisos in db.Vw_Mostrar_Personal_Permisos_PGR
                         on solicitud.TecnicoId.ToString() equals permisos.Codigo
                         where solicitud.SolicitanteId == solicitanteId
                         select new { permisos.NombreCompleto, solicitud.TecnicoId }).Distinct().ToList();
            return Json(query, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult ChangeResponsable(int tipo_servicio, int area_servicio, int dependencia)
        {
            var resultado = (from solicitud in db.Sol_Registro_Solicitud
                             join permisos in db.Vw_Mostrar_Personal_Permisos_PGR
                             on solicitud.TecnicoId.ToString() equals permisos.Codigo
                             where (solicitud.Tipo_SolicitudId == tipo_servicio || tipo_servicio == 0) && (solicitud.Tipo_Sub_SolicitudId == area_servicio || area_servicio == 0) &&
                             (solicitud.DependenciaRegionalId == dependencia || dependencia == 0)
                             select new { permisos.Codigo, permisos.NombreCompleto }).Distinct().ToList();
            
            return Json(resultado);
        }

        [HttpPost]
        public JsonResult ChangeSolicitante(int tipo_servicio, int area_servicio, int tecnico_id, int dependencia)
        {
            var resultado = (from solicitud in db.Sol_Registro_Solicitud
                             join permisos in db.Vw_Mostrar_Personal_Permisos_PGR
                             on solicitud.SolicitanteId.ToString() equals permisos.Codigo
                             where (solicitud.Tipo_SolicitudId == tipo_servicio || tipo_servicio == 0) && (solicitud.Tipo_Sub_SolicitudId == area_servicio || area_servicio == 0) && (solicitud.TecnicoId == tecnico_id || tecnico_id == 0) &&
                             (solicitud.DependenciaRegionalId == dependencia || dependencia == 0)
                             select new { permisos.Codigo, permisos.NombreCompleto }).Distinct().ToList();

            return Json(resultado);
        }

        [HttpPost]
        public JsonResult ChangeTipoServicio(int tipoSolicitud)
        {
            var query = (from soltecnicoId in db.Sol_Registro_Solicitud
                         join actividadId in db.Sol_Actividades on soltecnicoId.Tipo_SolicitudId equals actividadId.ActividadId
                         join subactividad in db.Sol_Sub_Actividades on soltecnicoId.Tipo_Sub_SolicitudId equals subactividad.SubActividadId
                         where  (soltecnicoId.Tipo_SolicitudId == tipoSolicitud || tipoSolicitud == 0)
                         select new { Value = subactividad.SubActividadId, Text = subactividad.Descripcion }).Distinct().ToList();

            return Json(query);

        }
        
        public JsonResult GetResponsable()
        {
            var query = (from mostrarPersonaPermiso in db.Vw_Mostrar_Personal_Permisos_PGR
                         join registrosolicitud in db.Sol_Registro_Solicitud
                         on mostrarPersonaPermiso.Codigo equals registrosolicitud.SolicitanteId.ToString()
                         select new { mostrarPersonaPermiso.NombreCompleto, registrosolicitud.SolicitanteId }).Distinct().ToList();

            return Json(query, JsonRequestBehavior.AllowGet);

        }

        #region Mostrar Area de Servicios
        [HttpGet]
        public JsonResult GetAreaDeServicio(string tiposervicio) 
        {   
            if(string.IsNullOrEmpty(tiposervicio)) return Json(new[] { new { Value = "", Text = "Area de Servicio" } }, JsonRequestBehavior.AllowGet);

            int cl = int.Parse(tiposervicio);
            var areaServicio = db.Sol_Sub_Actividades.Where(x => x.ActividadId == cl).Select(a => new { Value = a.SubActividadId.ToString(), Text = a.Descripcion}).ToList();
            return Json(areaServicio, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpPost]
        public JsonResult GetStatistics(string applicant, string responsible, string state, string service_type, string service_area, string begin_date, string end_date, string dependency)
        {
            CultureInfo ci = CultureInfo.InstalledUICulture;

            var languaje_name = ci.Name;

            if (languaje_name == "en-US")
            {
                if (begin_date == "")
                {
                    begin_date = Utilities.EmbeddedDate().ToString("MM/dd/yyyy");
                }
                else
                {
                    string[] splitted_date = begin_date.Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    begin_date = english_date.ToString("MM/dd/yyyy");
                }

                if (end_date == "")
                {
                    end_date = DateTime.Now.ToString("MM/dd/yyyy");
                }
                else
                {
                    string[] splitted_date = end_date.Split('/');
                    DateTime english_date = new DateTime(Convert.ToInt32(splitted_date[2]), Convert.ToInt32(splitted_date[1]), Convert.ToInt32(splitted_date[0]));
                    end_date = english_date.ToString("MM/dd/yyyy");
                }
            }
            else if (languaje_name == "es-ES")
            {
                begin_date = begin_date == "" ? Utilities.EmbeddedDate().ToString("dd/MM/yyyy") : begin_date;
                end_date = end_date == "" ? DateTime.Now.ToString("dd/MM/yyyy") : end_date;
            }

            DateTime begin_date_date = Convert.ToDateTime(begin_date);
            DateTime end_date_date = Convert.ToDateTime(end_date);

            List<List<Code.ChartData>> total_chart_data = new List<List<Code.ChartData>>();

            string user_code = Session["empleadoId"].ToString();

            int user_department = dependency == "1" ? Convert.ToInt32(db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == user_code).FirstOrDefault().CodigoDepartamento) : 15;

            applicant = applicant == "0" ? "" : applicant;
            responsible = responsible == "0" ? "" : responsible;
            state = state == "0" ? "" : state;
            service_type = service_type == "0" ? "" : service_type;
            service_area = service_area == "0" ? "" : service_area;
            dependency = dependency == "0" ? "" : dependency;

            #region First chart data
            var previous_date = Utilities.EmbeddedDate();

            var query = db.Sol_Registro_Solicitud.ToArray().Join(db.Vw_Mostrar_Personal_Permisos_PGR, Sol_Registro_Solicitud => Sol_Registro_Solicitud.TecnicoId, Vw_Mostrar_Personal_Permisos_PGR => int.Parse(Vw_Mostrar_Personal_Permisos_PGR.Codigo), (Sol_Registro_Solicitud, Vw_Mostrar_Personal_Permisos_PGR) => new { full_name = Vw_Mostrar_Personal_Permisos_PGR.NombreCompleto, Sol_Registro_Solicitud = Sol_Registro_Solicitud }).
                
                
                Where
                
                (x => x.Sol_Registro_Solicitud.DepartamentoId == user_department 
                
                && 
                
                (x.Sol_Registro_Solicitud.FhCreacion.Date > DateTime.Now.AddDays(-30) && (string.IsNullOrEmpty(begin_date) && string.IsNullOrEmpty(end_date)) || (x.Sol_Registro_Solicitud.FhCreacion.Date >= begin_date_date && x.Sol_Registro_Solicitud.FhCreacion.Date <= end_date_date))
                
                && 
                
                (x.Sol_Registro_Solicitud.TecnicoId.ToString() == responsible || string.IsNullOrEmpty(responsible)) 
                
                && (x.Sol_Registro_Solicitud.SolicitanteId.ToString() == applicant || string.IsNullOrEmpty(applicant)) 
                
                && (x.Sol_Registro_Solicitud.EstadoId.ToString() == state || string.IsNullOrEmpty(state)) 
                
                && (x.Sol_Registro_Solicitud.Tipo_SolicitudId.ToString() == service_type || string.IsNullOrEmpty(service_type))

                && (x.Sol_Registro_Solicitud.Tipo_Sub_SolicitudId.ToString() == service_area || string.IsNullOrEmpty(service_area))

                && (x.Sol_Registro_Solicitud.DependenciaRegionalId.ToString() == dependency || string.IsNullOrEmpty(dependency))).
                
                GroupBy(x => new { x.full_name, x.Sol_Registro_Solicitud.EstadoId }).Select(x => new { full_name = x.Key.full_name, quantity = x.Count(), state_id = x.Key.EstadoId }).OrderBy(x => x.full_name).ToList();

            var first_chart_data = new List<Code.ChartData>();
            var names = new List<string>();

            for (int i = 0; i < query.Count; i++)
            {
                string item_name = query[i].full_name;

                if (i > 0)
                {
                    int repeated = 0;

                    for (int j = i - 1; j >= 0; j--)
                    {
                        if (query[j].full_name == query[i].full_name)
                        {
                            repeated++;
                        }
                    }

                    if (repeated == 0)
                    {
                        names.Add(query[i].full_name);
                    }
                }
                else if (i == 0)
                {
                    names.Add(query[i].full_name);
                }
            }

            foreach (var item in names)
            {
                var rows = query.Where(x => x.full_name == item);

                var first_chart_item = new Code.ChartData();

                foreach (var item2 in rows)
                {
                    if (item2.state_id == 1)
                    {
                        first_chart_item.pending = item2.quantity;
                    }
                    else if (item2.state_id == 2)
                    {
                        first_chart_item.closed = item2.quantity;
                    }
                    else if (item2.state_id == 3)
                    {
                        first_chart_item.canceled = item2.quantity;
                    }
                    else if (item2.state_id == 4)
                    {
                        first_chart_item.pending_evaluation = item2.quantity;
                    }
                }

                first_chart_item.name = item;
                first_chart_item.total = first_chart_item.pending + first_chart_item.closed + first_chart_item.canceled + first_chart_item.pending_evaluation;

                first_chart_data.Add(first_chart_item);
            }

            total_chart_data.Add(first_chart_data);
            #endregion

            #region Second chart data

            var previous_date2 = Utilities.EmbeddedDate();

            var query2 = db.Sol_Registro_Solicitud.ToArray().Join(db.VistaTiempoRespuesta, Sol_Registro_Solicitud => Sol_Registro_Solicitud.SolicitudId, VistaTiempoRespuesta => VistaTiempoRespuesta.SolicitudId, (Sol_Registro_Solicitud, VistaTiempoRespuesta) => new { Sol_Registro_Solicitud = Sol_Registro_Solicitud, VistaTiempoRespuesta = VistaTiempoRespuesta }).Join(db.Vw_Mostrar_Personal_Permisos_PGR, Sol_Registro_SolicitudJoin => Sol_Registro_SolicitudJoin.Sol_Registro_Solicitud.TecnicoId, Vw_Mostrar_Personal_Permisos_PGR => int.Parse(Vw_Mostrar_Personal_Permisos_PGR.Codigo), (Sol_Registro_SolicitudJoin, Vw_Mostrar_Personal_Permisos_PGR) => new { nombreCompleto = Vw_Mostrar_Personal_Permisos_PGR.NombreCompleto, sum = Sol_Registro_SolicitudJoin.VistaTiempoRespuesta.TiempoExcedido, Sol_Registro_Solicitud = Sol_Registro_SolicitudJoin.Sol_Registro_Solicitud, Sol_Registro_SolicitudJoin }).
                
                Where(x => x.Sol_Registro_Solicitud.DepartamentoId == user_department 
                
                &&

                (x.Sol_Registro_Solicitud.FhCreacion.Date > DateTime.Now.AddDays(-30) && (string.IsNullOrEmpty(begin_date) || string.IsNullOrEmpty(end_date)) || (x.Sol_Registro_Solicitud.FhCreacion.Date >= begin_date_date && x.Sol_Registro_Solicitud.FhCreacion.Date <= end_date_date)) 
                
                && 
                
                (x.Sol_Registro_Solicitud.TecnicoId.ToString() == responsible || string.IsNullOrEmpty(responsible)) 
                
                && 
                
                (x.Sol_Registro_Solicitud.SolicitanteId.ToString() == applicant || string.IsNullOrEmpty(applicant)) 
                
                && 
                
                (x.Sol_Registro_Solicitud.EstadoId.ToString() == state || string.IsNullOrEmpty(state)) 
                
                && 
                
                (x.Sol_Registro_Solicitud.Tipo_SolicitudId.ToString() == service_type || string.IsNullOrEmpty(service_type)) 
                
                && 
                
                (x.Sol_Registro_Solicitud.Tipo_Sub_SolicitudId.ToString() == service_area || string.IsNullOrEmpty(service_area))
                
                &&
                
                (x.Sol_Registro_Solicitud.DependenciaRegionalId.ToString() == dependency|| string.IsNullOrEmpty(dependency))).
                
                GroupBy(x => new { nombreCompleto = x.nombreCompleto }).Select(x => new { x.Key.nombreCompleto, sum = x.Sum(y => y.sum) }).ToList();

            List<Code.ChartData> second_chart_data = new List<Code.ChartData>();

            foreach (var item in query2)
            {
                second_chart_data.Add(new Code.ChartData()
                {
                    name = item.nombreCompleto,
                    total = item.sum
                });
            }

            total_chart_data.Add(second_chart_data);

            #endregion

            #region Third chart data

            var query3 = db.Sol_Registro_Solicitud.ToArray().Join(db.Vw_Mostrar_Personal_Permisos_PGR, Sol_Registro_Solicitud => Sol_Registro_Solicitud.TecnicoId.ToString(), Vw_Mostrar_Personal_Permisos_PGR => Vw_Mostrar_Personal_Permisos_PGR.Codigo, (Sol_Registro_Solicitud, Vw_Mostrar_Personal_Permisos_PGR) => new { nombreCompleto = Vw_Mostrar_Personal_Permisos_PGR.NombreCompleto, Sol_Registro_Solicitud = Sol_Registro_Solicitud }).
                
                
            Where(x => x.Sol_Registro_Solicitud.DepartamentoId == user_department 
            
            &&

            (x.Sol_Registro_Solicitud.FhCreacion.Date > DateTime.Now.AddDays(-30) && (string.IsNullOrEmpty(begin_date) || string.IsNullOrEmpty(end_date)) || (x.Sol_Registro_Solicitud.FhCreacion.Date >= begin_date_date && x.Sol_Registro_Solicitud.FhCreacion.Date <= end_date_date)) 
            
            && 
            
            (x.Sol_Registro_Solicitud.TecnicoId.ToString() == responsible || string.IsNullOrEmpty(responsible)) 
            
            && 
            
            (x.Sol_Registro_Solicitud.SolicitanteId.ToString() == applicant || string.IsNullOrEmpty(applicant)) 
            
            && 
            
            (x.Sol_Registro_Solicitud.EstadoId.ToString() == state || string.IsNullOrEmpty(state)) 
            
            && 
            
            (x.Sol_Registro_Solicitud.Tipo_SolicitudId.ToString() == service_type || string.IsNullOrEmpty(service_type))
            
            && 
            
            (x.Sol_Registro_Solicitud.Tipo_Sub_SolicitudId.ToString() == service_area || string.IsNullOrEmpty(service_area))

            && 
            
            (x.Sol_Registro_Solicitud.DependenciaRegionalId.ToString() == dependency || string.IsNullOrEmpty(dependency))).
            
            GroupBy(x => new { nombreCompleto = x.nombreCompleto }).Select(x => new { nombreCompleto = x.Key.nombreCompleto, cantidad = x.Count() }).ToList();

            List<Code.ChartData> third_chart_data = new List<Code.ChartData>();

            foreach (var item in query3)
            {
                third_chart_data.Add(new Code.ChartData()
                {
                    name = item.nombreCompleto,
                    total = item.cantidad
                });
            }

            total_chart_data.Add(third_chart_data);

            #endregion

            return Json(total_chart_data);
        }

        public ActionResult ReporteEstadisticaAvanzadas(string applicant, string responsible, string state, string service_type, string service_area, string begin_date, string end_date, string dependency) {

            ViewBag.applicant = applicant == "0" ? applicant : db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == applicant).FirstOrDefault().NombreCompleto;
            //ViewBag.responsible = responsible == "0" ? responsible : db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == responsible).FirstOrDefault().NombreCompleto;
            ViewBag.state = state == "0" ? state : db.Sol_Estado_Solicitud.Find(Convert.ToInt32(state)).Descripcion;
            ViewBag.service_type = service_type == "0" ? service_type : db.Sol_Actividades.Find(Convert.ToInt32(service_type)).Descripcion;
            ViewBag.service_area = service_area == "0" ? service_area : db.Sol_Sub_Actividades.Find(Convert.ToInt32(service_area)).Descripcion;
            ViewBag.begin_date = begin_date;
            ViewBag.end_date = end_date == "" ? DateTime.Now.ToString("dd/MM/yyyy") : end_date;
            ViewBag.dependency = dependency == "" ? dependency : db.Sol_DependenciasRegionales.Find(Convert.ToInt32(dependency)).DependenciaNombre;

            ViewBag.data = GetStatistics(applicant, responsible, state, service_type, service_area, begin_date, end_date, dependency).Data;

            return new ViewAsPdf("ReporteEstadisticaAvanzadas")
            {
                PageOrientation = Orientation.Portrait,
                PageSize = Rotativa.Options.Size.Letter,
                PageMargins = { Left = 0, Right = 0, Bottom = 12, Top = 10 }
            };
        }

        public ActionResult ReporteEstadisticaAvanzada(string applicant, string responsible, string state, string service_type, string service_area, string begin_date, string end_date, string dependency)
        {
            ViewBag.applicant = applicant == "0" ? applicant : db.Vw_Mostrar_Personal_Permisos_PGR.Where(x=> x.Codigo == applicant).FirstOrDefault().NombreCompleto;
            //ViewBag.responsible = responsible == "0" ? responsible : db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == responsible).FirstOrDefault().NombreCompleto;
            ViewBag.state = state == "0" ? state : db.Sol_Estado_Solicitud.Find(Convert.ToInt32(state)).Descripcion;
            ViewBag.service_type = service_type == "0" ? service_type : db.Sol_Actividades.Find(Convert.ToInt32(service_type)).Descripcion;
            ViewBag.service_area = service_area == "0" ? service_area : db.Sol_Sub_Actividades.Find(Convert.ToInt32(service_area)).Descripcion;
            ViewBag.begin_date = begin_date;
            ViewBag.end_date = end_date == "" ? DateTime.Now.ToString("dd/MM/yyyy") : end_date;
            ViewBag.dependency = dependency == "" ? dependency : db.Sol_DependenciasRegionales.Find(Convert.ToInt32(dependency)).DependenciaNombre;

            ViewBag.data = GetStatistics(applicant, responsible, state, service_type, service_area, begin_date, end_date, dependency).Data;

            return View("ReporteEstadisticaAvanzadas");
        }

        /*not working*/
        #region notworking
        public ActionResult Asignadas()
        {

            if (Session["departamento"] == null)
                return Redirect("~/Login");



            ViewBag.dropDownDept = new SelectList(db.Sol_Departamento.Where(x => x.Estado == true), "Departamentoid", "Descripcion");


            int ultimoAnno = (from t in db.Sol_Registro_Solicitud
                              orderby t.FhFinalSolicitud.Year descending
                              select t.FhFinalSolicitud.Year).FirstOrDefault();

            var mes = (from t in db.Sol_Registro_Solicitud
                       where t.FhFinalSolicitud.Year == ultimoAnno
                       group t by new { t.FhFinalSolicitud.Month } into grp
                       orderby grp.Key.Month
                       select new { meses = grp.Key.Month, mesesId = grp.Key.Month }).ToList();

            var anno = (from t in db.Sol_Registro_Solicitud
                        group t by new { t.FhFinalSolicitud.Year } into grp
                        orderby grp.Key.Year descending
                        select new { anno = grp.Key.Year, annoId = grp.Key.Year }).ToList();

            ViewBag.meses = new SelectList(mes, "meses", "mesesId");
            ViewBag.annos = new SelectList(anno, "anno", "annoId");

            //GRAFICAS DE ASIGNADAS//

            //Gráfica de las asignaciones abiertas de los tecnicos de los ultimo 7 dias//
            var datosAsignadGrafica1 = UtilidadesEstadistica.asignaUltimaSemanaAbiertas(db);
            ViewBag.datosAsignadGrafica1 = datosAsignadGrafica1;

            //Gráfica de las asignaciones cerradas de los tecnicos de los ultimo 7 dias//
            var datosAsignadGrafica2 = UtilidadesEstadistica.asignaUltimaSemanaCerradas(db);
            ViewBag.datosAsignadGrafica2 = datosAsignadGrafica2;

            ViewBag.ruta = HttpRuntime.AppDomainAppVirtualPath;

            return View();

        }


        public ActionResult Gerencias()
        {
            if (Session["departamento"] == null)
                Redirect("~/Login");

            ViewBag.dropDownDept = new SelectList(db.Sol_Departamento.Where(x => x.Estado == true), "Departamentoid", "Descripcion");

            int ultimoAnno = (from t in db.Sol_Registro_Solicitud
                              orderby t.FhFinalSolicitud.Year descending
                              select t.FhFinalSolicitud.Year).FirstOrDefault();

            var mes = (from t in db.Sol_Registro_Solicitud
                       where t.FhFinalSolicitud.Year == ultimoAnno
                       group t by new { t.FhFinalSolicitud.Month } into grp
                       orderby grp.Key.Month
                       select new { meses = grp.Key.Month, mesesId = grp.Key.Month }).ToList();

            var anno = (from t in db.Sol_Registro_Solicitud
                        group t by new { t.FhFinalSolicitud.Year } into grp
                        orderby grp.Key.Year descending
                        select new { anno = grp.Key.Year, annoId = grp.Key.Year }).ToList();

            ViewBag.meses = new SelectList(mes, "meses", "mesesId");
            ViewBag.annos = new SelectList(anno, "anno", "annoId");



            //GRAFICAS DE GERENCIAS //

            //Gráfica de las solicitudes realizadas por las gerencias//
            var datosGerenciasGrafica1 = UtilidadesEstadistica.solicitudRealizadasXGerencias(db);
            ViewBag.datosGerenciasGrafica1 = datosGerenciasGrafica1;

            //Gráfica de las solicitudes realizadas por las usuarios//
            var datosGerenciasGrafica2 = UtilidadesEstadistica.solicitudesRealizadasXUsuarios(db);
            ViewBag.datosGerenciasGrafica2 = datosGerenciasGrafica2;

            ViewBag.ruta = HttpRuntime.AppDomainAppVirtualPath;

            return View();



        }
        public ActionResult ValorHoras()
        {
            if (Session["departamento"] == null)
                Redirect("~/Loging");

            ViewBag.dropDownDept = new SelectList(db.Sol_Departamento.Where(x => x.Estado == true), "Departamentoid", "Descripcion");



            int ultimoAnno = (from t in db.Sol_Registro_Solicitud
                              orderby t.FhFinalSolicitud.Year descending
                              select t.FhFinalSolicitud.Year).FirstOrDefault();

            var mes = (from t in db.Sol_Registro_Solicitud
                       where t.FhFinalSolicitud.Year == ultimoAnno
                       group t by new { t.FhFinalSolicitud.Month } into grp
                       orderby grp.Key.Month
                       select new { meses = grp.Key.Month, mesesId = grp.Key.Month }).ToList();

            var anno = (from t in db.Sol_Registro_Solicitud
                        group t by new { t.FhFinalSolicitud.Year } into grp
                        orderby grp.Key.Year descending
                        select new { anno = grp.Key.Year, annoId = grp.Key.Year }).ToList();

            ViewBag.meses = new SelectList(mes, "meses", "mesesId");
            ViewBag.annos = new SelectList(anno, "anno", "annoId");

            //GRAFICAS DE VALOR/HORA //

            //Gráfica  de la Cantidad de horas de los ultimos 7 dias //
            var datosValorHGrafica1 = UtilidadesEstadistica.servCantidadHoraUltimaSemana(db);
            ViewBag.datosValorHGrafica1 = datosValorHGrafica1;

            //Gráfica del tiempo excedido de Respuesta por los tecnicos//
            var datosValorHGrafica2 = UtilidadesEstadistica.tiempoExcedRespuesta(db);
            ViewBag.datosValorHGrafica2 = datosValorHGrafica2;

            //Gráfica  de la Cantidad de horas mensuales //

            var datosValorHGrafica3 = UtilidadesEstadistica.servCantidadHoraMensual(db);
            ViewBag.datosValorHGrafica3 = datosValorHGrafica3;

            //Gráfica  del tiempo excedido mensual por los tecnicos //
            var datosValorHGrafica4 = UtilidadesEstadistica.tiempoExcedMensual(db);
            ViewBag.datosValorHGrafica4 = datosValorHGrafica4;

            ViewBag.ruta = HttpRuntime.AppDomainAppVirtualPath;

            return View();
        }

        [HttpPost]
        public string EstadisticaFiltro(int annosId = 0, int mesesId = 0, string nombreVista = "EstadisticaFiltro", int departamentoId = 0)
        {
            string vista = RenderizarVistaComoString(nombreVista, departamentoId, annosId, mesesId);
            return vista;
        }

        public string RenderizarVistaComoString(string NombreVista, int id, int annosId = 0, int mesesId = 0)
        {
            // create a string writer to receive the HTML code
            StringWriter stringWriter = new StringWriter();

            object model = null;

            // get the view to render
            ViewEngineResult viewResult = ViewEngines.Engines.FindView(ControllerContext,
                  "EstadisticaFiltro", null);
            // create a context to render a view based on a model
            ViewContext viewContext = new ViewContext(
                ControllerContext,
                viewResult.View,
                new ViewDataDictionary(model),
                new TempDataDictionary(),
                stringWriter
            );

            int ultimoAnno = (from t in db.Sol_Registro_Solicitud
                              orderby t.FhFinalSolicitud.Year descending
                              select t.FhFinalSolicitud.Year).FirstOrDefault();

            var mes = (from t in db.Sol_Registro_Solicitud
                       where t.FhFinalSolicitud.Year == annosId
                       group t by new { t.FhFinalSolicitud.Month } into grp
                       orderby grp.Key.Month
                       select new { meses = grp.Key.Month, mesesId = grp.Key.Month }).ToList();

            var anno = (from t in db.Sol_Registro_Solicitud
                        group t by new { t.FhFinalSolicitud.Year } into grp
                        orderby grp.Key.Year descending
                        select new { anno = grp.Key.Year, annoId = grp.Key.Year }).ToList();

            if (NombreVista == "EstadisticaFiltro")
            {

                //GRAFICAS DE CANTIDADES//
                //Gráfica de los Servicios de los Ultimos 7 dias//
                var datosCantGrafica1 = UtilidadesEstadistica.servUlt7Dias(db, id);
                viewContext.ViewBag.datosCantGrafica1 = datosCantGrafica1;
                //Gráfica Tiempo Respuesta de los Servicios de los Ultimos 7 dias //
                var datosCantGrafica2 = UtilidadesEstadistica.tiempoRespuesta(db, id);
                viewContext.ViewBag.datosCantGrafica2 = datosCantGrafica2;
                //Gráfica de los Servicios mensuales //
                var datosCantGrafica3 = UtilidadesEstadistica.solicitudesXmes(db, id, annosId, mesesId);
                viewContext.ViewBag.datosCantGrafica3 = datosCantGrafica3;
                //Gráfica del Tiempo excedido de los Tecnicos(Mensual)  //
                var datosCantGrafica4 = UtilidadesEstadistica.tiempoRespuestaXmes(db, id);//check
                viewContext.ViewBag.datosCantGrafica4 = datosCantGrafica4;

                viewContext.ViewBag.graficasCantidades = true;
                viewContext.ViewBag.graficasAsignadas = false;
                viewContext.ViewBag.graficasGerencias = false;
                viewContext.ViewBag.graficasValorHora = false;
            }
            else if (NombreVista == "EstadisticaFiltroAsignadas")
            {
                //GRAFICAS DE ASIGNADAS//

                //Gráfica de las asignaciones abiertas de los tecnicos de los ultimo 7 dias//
                var datosAsignadGrafica1 = UtilidadesEstadistica.asignaUltimaSemanaAbiertas(db, id);
                viewContext.ViewBag.datosAsignadGrafica1 = datosAsignadGrafica1;

                //Gráfica de las asignaciones cerradas de los tecnicos de los ultimo 7 dias//
                var datosAsignadGrafica2 = UtilidadesEstadistica.asignaUltimaSemanaCerradas(db, id);
                viewContext.ViewBag.datosAsignadGrafica2 = datosAsignadGrafica2;

                viewContext.ViewBag.graficasAsignadas = true;
                viewContext.ViewBag.graficasCantidades = false;
                viewContext.ViewBag.graficasGerencias = false;
                viewContext.ViewBag.graficasValorHora = false;
            }
            else if (NombreVista == "EstadisticaFiltroGerencias")
            {
                //GRAFICAS DE GERENCIAS //

                //Gráfica de las solicitudes realizadas por las gerencias//
                var datosGerenciasGrafica1 = UtilidadesEstadistica.solicitudRealizadasXGerencias(db, id);
                viewContext.ViewBag.datosGerenciasGrafica1 = datosGerenciasGrafica1;

                //Gráfica de las solicitudes realizadas por las usuarios//
                var datosGerenciasGrafica2 = UtilidadesEstadistica.solicitudesRealizadasXUsuarios(db, id);
                viewContext.ViewBag.datosGerenciasGrafica2 = datosGerenciasGrafica2;

                viewContext.ViewBag.graficasGerencias = true;
                viewContext.ViewBag.graficasCantidades = false;
                viewContext.ViewBag.graficasAsignadas = false;
                viewContext.ViewBag.graficasValorHora = false;
            }
            else
            {
                //GRAFICAS DE VALOR/HORA //

                //Gráfica  de la Cantidad de horas de los ultimos 7 dias //
                var datosValorHGrafica1 = UtilidadesEstadistica.servCantidadHoraUltimaSemana(db, id);
                viewContext.ViewBag.datosValorHGrafica1 = datosValorHGrafica1;

                //Gráfica del tiempo excedido de Respuesta por los tecnicos//
                var datosValorHGrafica2 = UtilidadesEstadistica.tiempoExcedRespuesta(db, id);
                viewContext.ViewBag.datosValorHGrafica2 = datosValorHGrafica2;

                //Gráfica  de la Cantidad de horas mensuales //

                var datosValorHGrafica3 = UtilidadesEstadistica.servCantidadHoraMensual(db, id, annosId, mesesId);
                viewContext.ViewBag.datosValorHGrafica3 = datosValorHGrafica3;

                //Gráfica  del tiempo excedido mensual por los tecnicos //
                var datosValorHGrafica4 = UtilidadesEstadistica.tiempoExcedMensual(db, id);//check
                viewContext.ViewBag.datosValorHGrafica4 = datosValorHGrafica4;

                viewContext.ViewBag.graficasValorHora = true;
                viewContext.ViewBag.graficasCantidades = false;
                viewContext.ViewBag.graficasAsignadas = false;
                viewContext.ViewBag.graficasGerencias = false;
            }

            viewContext.ViewBag.meses = new SelectList(mes, "meses", "mesesId", mesesId);
            viewContext.ViewBag.annos = new SelectList(anno, "anno", "annoId", annosId);

            // render the view to a HTML code
            viewResult.View.Render(viewContext, stringWriter);

            // return the HTML code
            return stringWriter.ToString();

        }
        #endregion
    }
}