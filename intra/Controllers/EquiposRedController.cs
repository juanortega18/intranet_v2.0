﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.Inventario;

namespace intra.Controllers
{
    public class EquiposRedController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: EquiposRed
        public ActionResult Index()
        {
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            return View(db.EquiposReds.ToList());
        }
        [CustomAuthorize]
        public PartialViewResult vEquiposRed()
        {
             ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            return PartialView(db.VistaEquiposReds.ToList());
        }

        [HttpGet]
        public JsonResult GetProvincia(int id)
        {


            List<SelectListItem> listProvincia = new List<SelectListItem>();
            if (id < 1)
            {

                RedirectToAction("Create");

            }

            var Provinces = (from r in db.Provinces
                             from i in db.Region
                             from rp in db.RegionProvincia
                             where i.RegionId == rp.RegionId
                             where r.Province_ID == rp.Province_ID
                             where i.RegionId == id
                             orderby r.Province_Name ascending
                             select new
                             {
                                 r.Province_Name,
                                 r.Province_ID

                             }).ToList();

            foreach (var Province in Provinces)
            {
                listProvincia.Add(new SelectListItem { Text = Province.Province_Name.ToString(), Value = Province.Province_ID.ToString() });

            }

            return Json(new SelectList(listProvincia, "value", "Text"), JsonRequestBehavior.AllowGet);
        }

        //Get Municipio
        public JsonResult Municipio(string Provincia)
        {
            List<SelectListItem> Municipios;

            int cl = int.Parse(Provincia);
            Municipios = db.Municipalities.Where(x => x.Province_ID == cl).Select(x => new SelectListItem() { Text = x.Municipality_Name, Value = x.Municipality_ID.ToString() }).ToList();


            return Json(new SelectList(Municipios, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        // GET: EquiposRed/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VistaEquiposReds equiposRed = db.VistaEquiposReds.Find(id);
            var calc = DiferenciaFechas(equiposRed.EquipoGarantia, equiposRed.EquipoFechaCompra);
            ViewBag.calc = calc;
            if (equiposRed == null)
            {
                return HttpNotFound();
            }
            return View(equiposRed);
        }

        [CustomAuthorize]
        // GET: EquiposRed/Create
        public ActionResult Create()
        {
            ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            ViewBag.Modelo = new SelectList(db.ModeloInventario.ToList(), "ModeloId", "ModeloDescripcion");
            ViewBag.Proveedor = new SelectList(db.ProveedorInventario.ToList(), "ProveedorId", "ProveedorNombre");
            ViewBag.Estatus = new SelectList(db.EstatusInventario.ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            return View();
        }

        // POST: EquiposRed/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "equipoId,EquipoDescripcion,RegionId,Province_ID,Municipality_ID,GeoReferencia,EquipoGarantia,EquipoFechaCompra,EquipoFechaCreacion,EquipoUsuatio,MarcaId,ModeloId,EstatusId,EquipoSerie,CaracteristicaEquipo,EquipoServiceTag,LocalidaId,ProveedorId")]  FormCollection gr, EquiposReds equiposRed)
        {
            var compra = gr["EquipoFechaCompra"];
            var garantia = gr["EquipoGarantia"];
            equiposRed.EquipoFechaCompra = DateTime.ParseExact(compra, "dd/MM/yyyy", null);
            equiposRed.EquipoGarantia = DateTime.ParseExact(garantia, "dd/MM/yyyy", null);
            equiposRed.EquipoFechaCreación = DateTime.Now;
            equiposRed.EquipoUsuario = Session["usuario"].ToString();
            if (!ModelState.IsValid)
            {
                db.EquiposReds.Add(equiposRed);
                db.SaveChanges();
                return RedirectToAction("Index", "ComputadoraInventario");
                //return RedirectToAction("Index");
            }
            ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            ViewBag.Modelo = new SelectList(db.ModeloInventario.ToList(), "ModeloId", "ModeloDescripcion");
            ViewBag.Proveedor = new SelectList(db.ProveedorInventario.ToList(), "ProveedorId", "ProveedorNombre");
            ViewBag.Estatus = new SelectList(db.EstatusInventario.ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            return View(equiposRed);
        }
        public JsonResult Modelo(string marca)
        {
            List<SelectListItem> modelos;

            int cl = int.Parse(marca);
            modelos = db.ModeloInventario.Where(x => x.MarcaId == cl).Select(x => new SelectListItem() { Text = x.ModeloDescripcion, Value = x.ModeloId.ToString() }).ToList();

            return Json(new SelectList(modelos, "Value", "Text"));
        }

        [CustomAuthorize]
        public ActionResult Localidad(string Municipio)
        {
            var muni = int.Parse(Municipio);
            var Localidad = new List<SelectListItem>();
            var localidad = db.Localidad.Where(x => x.Municipality_ID == muni).Select(x => new SelectListItem() { Text = x.LocalidadNombre, Value = x.LocalidadId.ToString() }).ToList();
            return Json(new SelectList(localidad, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        // GET: EquiposRed/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            ViewBag.Modelo = new SelectList(db.ModeloInventario.ToList(), "ModeloId", "ModeloDescripcion");
            ViewBag.Proveedor = new SelectList(db.ProveedorInventario.ToList(), "ProveedorId", "ProveedorNombre");
            ViewBag.Estatus = new SelectList(db.EstatusInventario.ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            ViewBag.Localidad = new SelectList(db.Localidad.ToList(), "LocalidadId", "LocalidadNombre");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EquiposReds equiposRed = db.EquiposReds.Find(id);
            equiposRed.EquipoFechaCompra = equiposRed.EquipoFechaCompra.Date;
            equiposRed.EquipoGarantia = equiposRed.EquipoGarantia.Date;
            if (equiposRed == null)
            {
                return HttpNotFound();
            }
            return View(equiposRed);
        }

        // POST: EquiposRed/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "equipoId,EquipoDescripcion,RegionId,Province_ID,Municipality_ID,EquipoGarantia,EquipoFechaCompra,EquipoUsuatio,MarcaId,ModeloId,EstatusId,EquipoSerie,CaracteristicaEquipo,EquipoServiceTag,EquipoFechaCreación,LocalidaId,ProveedorId")] EquiposReds equiposReds, FormCollection form)
        {
            
    
            //var a = Convert.ToDateTime(form["EquipoFechaCompra"]);
            //equiposReds.EquipoFechaCompra = a;
            //equiposReds.EquipoGarantia = Convert.ToDateTime(form["EquipoGarantia"]);

            equiposReds.EquipoUsuario = Session["usuario"].ToString();
            if (ModelState.IsValid)
            {
                db.Entry(equiposReds).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "ComputadoraInventario");
            }
            ViewBag.Marca = new SelectList(db.MarcasInventario.ToList(), "MarcaId", "MarcaDescripción");
            ViewBag.Modelo = new SelectList(db.ModeloInventario.ToList(), "ModeloId", "ModeloDescripcion");
            ViewBag.Proveedor = new SelectList(db.ProveedorInventario.ToList(), "ProveedorId", "ProveedorNombre");
            ViewBag.Estatus = new SelectList(db.EstatusInventario.ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Region = new SelectList(db.Region.ToList(), "RegionId", "RegionDescripcion");
            ViewBag.Provincia = new SelectList(db.Provinces.ToList(), "Province_ID", "Province_Name");
            ViewBag.Municipio = new SelectList(db.Municipalities.ToList(), "Municipality_ID", "Municipality_Name");
            return View(equiposReds);
        }

        [CustomAuthorize]
        // GET: EquiposRed/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VistaEquiposReds equiposRed = db.VistaEquiposReds.Find(id);
            if (equiposRed == null)
            {
                return HttpNotFound();
            }
            return View(equiposRed);
        }

        // POST: EquiposRed/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EquiposReds equiposRed = db.EquiposReds.Find(id);
            db.EquiposReds.Remove(equiposRed);
            db.SaveChanges();
            return RedirectToAction("Index", "ComputadoraInventario");
        }

        private String DiferenciaFechas(DateTime newdt, DateTime olddt)
        {
            Int32 anios;
            Int32 meses;
            Int32 dias;
            String str = "";

            anios = (newdt.Year - olddt.Year);
            meses = (newdt.Month - olddt.Month);
            dias = (newdt.Day - olddt.Day);

            if (meses < 0)
            {
                anios -= 1;
                meses += 12;
            }
            if (dias < 0)
            {
                meses -= 1;
                dias += DateTime.DaysInMonth(newdt.Year, newdt.Month);
            }

            if (anios < 0)
            {
                return "Fecha Invalida";
            }
            if (anios > 0)
                str = str + anios.ToString() + " años ";
            if (meses > 0)
                str = str + meses.ToString() + " meses ";
            if (dias > 0)
                str = str + dias.ToString() + " dias ";

            return str;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
