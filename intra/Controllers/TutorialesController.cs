﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using System.IO;
using PagedList;
using System.Web.Hosting;
using intra.Controllers;



namespace intra.Controllers
{
    public class TutorialesController : Controller
    {
        private dbIntranet db = new dbIntranet();

        //[CustomAuthorize]
        // GET: Tutoriales
        public ActionResult Index()
        {
            string EmpId = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                {
                    EmpId = HttpContext.Session["empleadoId"].ToString();
                }
                else
                {
                    EmpId = "public_user";
                }
            }
            catch (Exception)
            {
                EmpId = "public_user";
            }

            ViewBag.access = EmpId;

            //string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(EmpId, "Ingreso al Modulo de tutoriales)");
            return View(db.Tutoriales.Where(x=> x.Estado == true).ToList());
        }

        //[CustomAuthorize]
        // GET: Tutoriales/Details/5
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string EmpId = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                {
                    EmpId = HttpContext.Session["empleadoId"].ToString();
                }
                else
                {
                    EmpId = "public_user";
                }
            }
            catch (Exception)
            {
                EmpId = "public_user";
            }
            
            Tutoriales tutoriales = db.Tutoriales.Find(id);

            ViewBag.access = EmpId;

            if (tutoriales == null)
            {
                return HttpNotFound();
            }

            return View(tutoriales);
        }

        public PartialViewResult vTutoriales()
        {
            return PartialView(db.Tutoriales.Where(x=> x.Estado == true).ToList());
        }

        [CustomAuthorize]
        // GET: Tutoriales/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tutoriales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Create(HttpPostedFileBase file, FormCollection vd)
        {
            Tutoriales tutoriales = new Tutoriales();
            //ruta de para guardar videos

            string ruta = HttpRuntime.AppDomainAppVirtualPath + "/Content/videos/";
                //string.Concat(System.Configuration.ConfigurationManager.AppSettings["intra"], "Content/videos/");

              

            if (vd["Tutorial"] != "")
            {

                tutoriales.TutorialNombre = vd["Tutorial"];
                tutoriales.FechaCreacion = DateTime.Now;
                tutoriales.TutorialUsuario = HttpContext.Session["Usuario"].ToString();
                tutoriales.Estado = true;

                //tutoriales.video = vd["__contenido"];

                if (file != null && file.ContentLength > 0)
                {
                    string path = Path.Combine(Server.MapPath(ruta), Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    tutoriales.TutorialUrl = ruta + file.FileName;
                }
                else
                {
                    ViewBag.Tutoriales = "ok";
                    return View();
                }

                //if (file  != null && file.ContentLength > 0)
                //{
                //    string path = Path.Combine(Server.MapPath(ruta), Path.GetFileName(file.FileName));
                //    file.SaveAs(path);
                //    tutoriales.Portada = ruta + file.FileName;
                //}

                if (ModelState.IsValid)
                {
                    db.Tutoriales.Add(tutoriales);
                    db.SaveChanges();
                    string empId = HttpContext.Session["empleadoId"].ToString();
                    RegisterLogs rl = new RegisterLogs(empId, "Creo un Registro en el Modulo de tutoriales llamado ("+tutoriales.TutorialNombre+"))");
                    return RedirectToAction("Index");
                }


            }
            return View("Create", tutoriales);
        }


        [CustomAuthorize]
        // GET: Tutoriales/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tutoriales tutoriales = db.Tutoriales.Find(id);
            if (tutoriales == null)
            {
                return HttpNotFound();
            }
            return View(tutoriales);
        }

        // POST: Tutoriales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]

        // POST: Tutorial/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        public ActionResult Edit(HttpPostedFileBase file, FormCollection vd)
        {
            //Tutoriales tutoriales = new Tutoriales();
            //Convertir para actualizar
            int tutorialId = Convert.ToInt32(vd["TutorialId"]);
            //ruta de para guardar videos
            string ruta = string.Concat(System.Configuration.ConfigurationManager.AppSettings["intranet"], "Content/videos/");
            Tutoriales tutoriales = db.Tutoriales.Find(tutorialId);
            tutoriales.TutorialNombre = vd["TutorialNombre"];
            tutoriales.FechaCreacion = tutoriales.FechaCreacion;
            tutoriales.TutorialUsuario = HttpContext.Session["Usuario"].ToString();

            //tutoriales.video = vd["__contenido"];

            if (file != null && file.ContentLength > 0)
            {
           
                string path = Path.Combine(Server.MapPath(ruta), Path.GetFileName(file.FileName));
                file.SaveAs(path);
                tutoriales.TutorialUrl =  ruta + file.FileName;

            }
            else
            {
                tutoriales.TutorialUrl = tutoriales.TutorialUrl;
            }


            //if (file != null && file.ContentLength > 0)
            //{
            //    string path = Path.Combine(Server.MapPath(ruta), Path.GetFileName(file.FileName));
            //    file.SaveAs(path);
            //    tutoriales.Portada = ruta + file.FileName;
            //}

            if (ModelState.IsValid)
            {
                db.Entry(tutoriales).State = EntityState.Modified;
                db.SaveChanges();
         
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito un Registro en el Modulo de tutoriales llamado (" + tutoriales.TutorialNombre + "))");
                return RedirectToAction("Index");
            }
            return View("Edit", tutoriales);
        }

        [CustomAuthorize]
        // GET: Tutoriales/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tutoriales tutoriales = db.Tutoriales.Find(id);
            tutoriales.Estado = false;
            db.SaveChanges();
            ViewBag.EliminarTutorial = "ok";
            if (tutoriales == null)
            {

                return HttpNotFound();
            }
            return RedirectToAction("Index", "tutoriales");
            //return View(tutoriales);
        }

     

        [CustomAuthorize]
        public ActionResult EncuestaClima()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
