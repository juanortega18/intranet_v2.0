﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.Docs;
using intra.Code;

namespace intra.Controllers
{
    public class IntraDocumentosController : Controller
    {
        private CargaDocs db = new CargaDocs();

       // GET: IntraDocumentos
            public ActionResult Index()
        {
            return View();
        }

        //    // GET: IntraDocumentos/Details/5
        //    public ActionResult Details(int? id)
        //    {
        //        if (id == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }
        //        IntraDocumentos intraDocumentos = db.IntraDocumentos.Find(id);
        //        if (intraDocumentos == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(intraDocumentos);
        //    }

        //    // GET: IntraDocumentos/Create
        //    public ActionResult Create()
        //    {
        //        return View();
        //    }

        //    // POST: IntraDocumentos/Create
        //    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //    [HttpPost]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult Create([Bind(Include = "IntDocumentoId,IntDocumentoNombre,IntDocumentoFecha,IntCarpetaId,IntDocumentoArchivo,IntDocumentoTipo,IntDocumentoEstatus,IntDocumentoFechaCreacion")] IntraDocumentos intraDocumentos)
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            db.IntraDocumentos.Add(intraDocumentos);
        //            db.SaveChanges();
        //            return RedirectToAction("Index");
        //        }

        //        return View(intraDocumentos);
        //    }

        //    // GET: IntraDocumentos/Edit/5
        //    public ActionResult Edit(int? id)
        //    {
        //        if (id == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }
        //        IntraDocumentos intraDocumentos = db.IntraDocumentos.Find(id);
        //        if (intraDocumentos == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(intraDocumentos);
        //    }

        //    // POST: IntraDocumentos/Edit/5
        //    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //    [HttpPost]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult Edit([Bind(Include = "IntDocumentoId,IntDocumentoNombre,IntDocumentoFecha,IntCarpetaId,IntDocumentoArchivo,IntDocumentoTipo,IntDocumentoEstatus,IntDocumentoFechaCreacion")] IntraDocumentos intraDocumentos)
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            db.Entry(intraDocumentos).State = EntityState.Modified;
        //            db.SaveChanges();
        //            return RedirectToAction("Index");
        //        }
        //        return View(intraDocumentos);
        //    }

        //    // GET: IntraDocumentos/Delete/5
        //    public ActionResult Delete(int? id)
        //    {
        //        if (id == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }
        //        IntraDocumentos intraDocumentos = db.IntraDocumentos.Find(id);
        //        if (intraDocumentos == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(intraDocumentos);
        //    }

        //    // POST: IntraDocumentos/Delete/5
        //    [HttpPost, ActionName("Delete")]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult DeleteConfirmed(int id)
        //    {
        //        IntraDocumentos intraDocumentos = db.IntraDocumentos.Find(id);
        //        db.IntraDocumentos.Remove(intraDocumentos);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    protected override void Dispose(bool disposing)
        //    {
        //        if (disposing)
        //        {
        //            db.Dispose();
        //        }
        //        base.Dispose(disposing);
        //    }

        //public ActionResult Index(int id)
        //{
        //    var carpeta = db.IntraCarpeta.Where(c => c.INtCarpetaRaizId == id).OrderBy(d => d.IntCarpetaNombre).ToList();
        //    ViewBag.CarpetaId = id;
        //    List<IntraDocumentos> documentos = db.IntraDocumentos.Where(d => d.IntCarpetaId == id).OrderBy(f => f.IntDocumentoFecha).ToList();

        //    var carpetainfo = db.IntraCarpeta.Where(c => c.IntCarpetaId == id).Select(s => new { s.IntCarpetaNombre, deptoNombre = s.DeptoId.SeccionNombre, s.INtCarpetaRaizId }).FirstOrDefault();
        //    if (carpetainfo != null)
        //    {
        //        ViewBag.SeccionNombre = carpetainfo.seccionNombre;

        //        ViewBag.CarpetaNombre = carpetainfo.IntCarpetaNombre;

        //        ViewBag.carpetaRaizId = carpetainfo.INtCarpetaRaizId;
        //    }
        //    ViewBag.documentos = documentos;
        //    return View(carpeta);
        //}

        [CustomAuthorize]
        [HttpPost]
        public ActionResult SubirArchivo(IntraDocumentos documento)
        {
            if (documento.IntDocumentoFecha.ToString() == "") documento.IntDocumentoFecha = DateTime.Now;
            documento.IntDocumentoFechaCreacion = DateTime.Now;
            foreach (var archivo in documento.ArchivoVista)

            {
                var ind = 0;
                if (archivo.FileName.Contains(".pdf"))
                {
                    ind = archivo.FileName.IndexOf(".pdf");
                }
                else if (archivo.FileName.Contains(".msword"))
                {
                    ind = archivo.FileName.IndexOf(".msword");
                }

                documento.IntDocumentoArchivo = Utility.ConvertirArchivo(archivo);
                if (ind != 0)
                {
                    documento.IntDocumentoNombre = archivo.FileName.Substring(0, ind);
                }
                else
                {
                    documento.IntDocumentoNombre = archivo.FileName;
                }
                documento.IntDocumentoTipo = archivo.ContentType;
                documento.IntDocumentoEstatus = true;
                db.IntraDocumentos.Add(documento);
                db.SaveChanges();
            }


            //return RedirectToAction("Index", new { id = documento.CarpetaId });
            return View();
        }
    }
}
