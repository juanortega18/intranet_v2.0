﻿using intra.Models.Seguros;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    public class SegurosController : Controller
    {
        dbIntranet Database = new dbIntranet();

        #region INDEX

        // GET: Seguros
        public ActionResult Index()
        {
            List<Seguro> seguros = (
                from TablaSeguros in Database.Seguros
                orderby TablaSeguros.SeguroNombre ascending
                select TablaSeguros
            ).ToList();

            return View(seguros);
        }

        #endregion

        #region VER SEGURO

        public ActionResult VerSeguro(int seguroId)
        {
            Seguro seguro = (
                from TablaSeguros in Database.Seguros
                where TablaSeguros.SeguroId == seguroId
                select TablaSeguros
            ).FirstOrDefault();

            return View(seguro);
        }

        #endregion

        #region EDITAR SEGURO

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public ActionResult EditarSeguro(int seguroId)
        {
            Seguro seguro = (
                from TablaSeguros in Database.Seguros
                where TablaSeguros.SeguroId == seguroId
                select TablaSeguros
            ).FirstOrDefault();

            return View(seguro);
        }

        #endregion

        #region AGREGAR SEGURO  

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public ActionResult AgregarSeguro()
        {
            return View();
        }

        #endregion

        #region GUARDAR SEGURO

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public JsonResult GuardarSeguro(string seguroNombre, string seguroLink)
        {
            ValidadorSeguro validarSeguro = new ValidadorSeguro();

            List<string> errores = validarSeguro.ValidarCamposSeguro(seguroNombre, seguroLink, true);

            if (errores.Count() > 0)
            {
                return Json(errores, JsonRequestBehavior.AllowGet);
            }

            Seguro seguro = new Seguro();
            seguro.SeguroNombre = seguroNombre.ToUpper();
            seguro.SeguroLink = seguroLink.ToLower();
            seguro.SeguroEstado = true;

            Database.Seguros.Add(seguro);

            Database.SaveChanges();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase archivo = Request.Files[i];

                string archivoNombre = String.Join(".", archivo.FileName.Split('.').Take(archivo.FileName.Split('.').Length - 1));
                string archivoExtension = archivo.FileName.Split('.')[archivo.FileName.Split('.').Length - 1];

                string directorio = HttpContext.Server.MapPath($"~/SegurosArchivos/{seguro.SeguroNombre}");

                bool existeDirectorio = Directory.Exists(directorio);

                if (!existeDirectorio)
                {
                    Directory.CreateDirectory(directorio);
                }

                string rutaArchivo = Path.Combine(directorio, archivo.FileName);
                archivo.SaveAs(rutaArchivo);

                ArchivoSeguro archivoSeguro = new ArchivoSeguro();
                archivoSeguro.SeguroId = seguro.SeguroId;
                archivoSeguro.ArchivoSeguroNombre = archivoNombre;
                archivoSeguro.ArchivoSeguroExtension = archivoExtension;
                archivoSeguro.ArchivoSeguroRuta = directorio;
                archivoSeguro.ArchivoSeguroEstado = true;

                Database.ArchivosSeguros.Add(archivoSeguro);

                Database.SaveChanges();
            }

            return Json(new { Exito = true }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region GUARDAR SEGURO EDITADO  

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public JsonResult GuardarSeguroEditado(int seguroId, string seguroNombre, string seguroLink)
        {
            ValidadorSeguro validarSeguro = new ValidadorSeguro();

            List<string> errores = validarSeguro.ValidarCamposSeguro(seguroNombre, seguroLink, false);

            if (errores.Count() > 0)
            {
                return Json(errores, JsonRequestBehavior.AllowGet);
            }

            Seguro seguro = (
                from TablaSeguros in Database.Seguros
                where TablaSeguros.SeguroId == seguroId
                select TablaSeguros
            ).FirstOrDefault();

            seguro.SeguroNombre = seguroNombre.ToUpper();
            seguro.SeguroLink = seguroLink.ToLower();

            Database.SaveChanges();

            return Json(new { Exito = true }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region ELIMINAR SEGURO     

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public JsonResult EliminarSeguro(int seguroId)
        {
            List<ArchivoSeguro> archivosSeguro = (
                from TablaArchivosSeguros in Database.ArchivosSeguros
                where TablaArchivosSeguros.SeguroId == seguroId
                select TablaArchivosSeguros
            ).ToList();

            foreach (ArchivoSeguro archivoSeguro in archivosSeguro)
            {
                Database.ArchivosSeguros.Remove(archivoSeguro);
            }

            Seguro seguro = (
                from TablaSeguros in Database.Seguros
                where TablaSeguros.SeguroId == seguroId
                select TablaSeguros
            ).FirstOrDefault();

            string rutaArchivo = HttpContext.Server.MapPath($"~/SegurosArchivos/{seguro.SeguroNombre}");

            Directory.Delete(rutaArchivo, true);

            Database.Seguros.Remove(seguro);

            Database.SaveChanges();

            return Json(new { Exito = true });
        }

        #endregion

        #region GUARDAR ARCHIVO

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public JsonResult GuardarArchivos(int seguroId)
        {
            Seguro seguro = (
                from TablaSeguros in Database.Seguros
                where TablaSeguros.SeguroId == seguroId
                select TablaSeguros
            ).FirstOrDefault();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase archivo = Request.Files[i];

                string archivoNombre = String.Join(".", archivo.FileName.Split('.').Take(archivo.FileName.Split('.').Length - 1));
                string archivoExtension = archivo.FileName.Split('.')[archivo.FileName.Split('.').Length - 1];

                string directorio = HttpContext.Server.MapPath($"~/SegurosArchivos/{seguro.SeguroNombre}");

                bool existeDirectorio = Directory.Exists(directorio);

                if (!existeDirectorio)
                {
                    Directory.CreateDirectory(directorio);
                }

                string rutaArchivo = Path.Combine(directorio, archivo.FileName);
                archivo.SaveAs(rutaArchivo);

                ArchivoSeguro archivoSeguro = new ArchivoSeguro();
                archivoSeguro.SeguroId = seguroId;
                archivoSeguro.ArchivoSeguroNombre = archivoNombre;
                archivoSeguro.ArchivoSeguroExtension = archivoExtension;
                archivoSeguro.ArchivoSeguroRuta = directorio;
                archivoSeguro.ArchivoSeguroEstado = true;

                Database.ArchivosSeguros.Add(archivoSeguro);
                Database.SaveChanges();

            }

            return Json(new { Exito = true }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region OBTENER ARCHIVOS

        public JsonResult ObtenerArchivos(int seguroId)
        {
            var archivosSeguro = (
                from TablaArchivosSeguros in Database.ArchivosSeguros
                where TablaArchivosSeguros.SeguroId == seguroId
                select new {
                    ArchivoId = TablaArchivosSeguros.ArchivoSeguroId,
                    ArchivoNombre = TablaArchivosSeguros.ArchivoSeguroNombre,
                    ArchivoExtension = TablaArchivosSeguros.ArchivoSeguroExtension
                }
            ).ToList();

            return Json(archivosSeguro, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region DESCARGAR ARCHIVO

        public FileResult DescargarArchivo(int archivoId)
        {
            ArchivoSeguro archivoSeguro = (
                from TablaArchivosSeguros in Database.ArchivosSeguros
                where TablaArchivosSeguros.ArchivoSeguroId == archivoId
                select TablaArchivosSeguros
            ).FirstOrDefault();

            string seguroNombre = (
                from TablaSeguros in Database.Seguros
                where TablaSeguros.SeguroId == archivoSeguro.SeguroId
                select TablaSeguros
            ).FirstOrDefault().SeguroNombre;

            string rutaArchivo = HttpContext.Server.MapPath($"~/SegurosArchivos/{seguroNombre}/{archivoSeguro.ArchivoSeguroNombre}.{archivoSeguro.ArchivoSeguroExtension}");

            byte[] archivo = System.IO.File.ReadAllBytes(rutaArchivo); 

            return File(archivo, archivoSeguro.ArchivoSeguroExtension, $"{archivoSeguro.ArchivoSeguroNombre}.{archivoSeguro.ArchivoSeguroExtension}");
        }

        #endregion

        #region ELIMINAR ARCHIVO

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public JsonResult EliminarArchivo(int archivoId)
        {
            ArchivoSeguro archivoSeguro = (
                from TablaArchivosSeguros in Database.ArchivosSeguros
                where TablaArchivosSeguros.ArchivoSeguroId == archivoId
                select TablaArchivosSeguros
            ).FirstOrDefault();

            string seguroNombre = (
                from TablaSeguros in Database.Seguros
                where TablaSeguros.SeguroId == archivoSeguro.SeguroId
                select TablaSeguros
            ).FirstOrDefault().SeguroNombre;

            string rutaArchivo = HttpContext.Server.MapPath($"~/SegurosArchivos/{seguroNombre}/{archivoSeguro.ArchivoSeguroNombre}.{archivoSeguro.ArchivoSeguroExtension}");

            System.IO.File.Delete(rutaArchivo);

            Database.ArchivosSeguros.Remove(archivoSeguro);
            Database.SaveChanges();

            return Json(new { Exito = true });
        }

        #endregion
    }
}