﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models.Cumpleanos;

namespace intra.Controllers
{
    public class CumpleanosIncluirController : Controller
    {
        private dbIntranet db = new dbIntranet();

        [CustomAuthorize]
        // GET: CumpleanosIncluir
        public ActionResult Index()
        {
            int cumple = 0;
            var Incluir = db.EstadoCumpleanos.Where(x => x.Estado == cumple.ToString());
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo de Incluir Cumpleaños");
            return View(Incluir.ToList());
        }

        [CustomAuthorize]
        // GET: CumpleanosIncluir/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoCumpleanos estadoCumpleanos = db.EstadoCumpleanos.Find(id);
            if (estadoCumpleanos == null)
            {
                return HttpNotFound();
            }
            return View(estadoCumpleanos);
        }

        [CustomAuthorize]
        // GET: CumpleanosIncluir/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CumpleanosIncluir/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        ////[HttpPost]
        ////[ValidateAntiForgeryToken]
        //////public ActionResult Create([Bind(Include = "IdEstado,CodigoEmpleado,Estado,NombreCompleto,Departamento")] EstadoCumpleanos estadoCumpleanos)
        //////{
        //////    //if (ModelState.IsValid)
        //////    //{
        //////    //    db.EstadoCumpleanos.Add(estadoCumpleanos);
        //////    //    db.SaveChanges();
        //////    //    return RedirectToAction("Index");
        //////    //}

        //////    //return View(estadoCumpleanos);
        //////}

        // GET: CumpleanosIncluir/Edit/5

        [CustomAuthorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoCumpleanos estadoCumpleanos = db.EstadoCumpleanos.Find(id);
            if (estadoCumpleanos == null)
            {
                return HttpNotFound();
            }
            return View(estadoCumpleanos);
        }

        // POST: CumpleanosIncluir/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Edit(FormCollection fc)
        {
            int id = 0;
            id = Convert.ToInt16(fc["IdEstado"]);
            EstadoCumpleanos estadoCumpleanos = db.EstadoCumpleanos.Find(id);


            string estado = fc["Estado"];
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Cambio el Estado en el Modulo IncluirCumpleaños al Empleado (" + estadoCumpleanos.NombreCompleto + ") de (" + estadoCumpleanos.Estado + ") a (" + estado + ")");
            if (estado == "1")
            {

                estadoCumpleanos.Estado = "0";
            }
            else
            {
                estadoCumpleanos.Estado = "1";
            }

            if (ModelState.IsValid)
            {
                db.Entry(estadoCumpleanos).State = EntityState.Modified;
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }
            return View(estadoCumpleanos);
        }

        [CustomAuthorize]
        // GET: CumpleanosIncluir/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoCumpleanos estadoCumpleanos = db.EstadoCumpleanos.Find(id);
            if (estadoCumpleanos == null)
            {
                return HttpNotFound();
            }
            return View(estadoCumpleanos);
        }

        // POST: CumpleanosIncluir/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EstadoCumpleanos estadoCumpleanos = db.EstadoCumpleanos.Find(id);
            db.EstadoCumpleanos.Remove(estadoCumpleanos);
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Elimino del Modulo IncluirCumpleaños al empleado("+estadoCumpleanos.NombreCompleto+")");
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
