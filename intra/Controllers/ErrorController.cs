﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        public const string GENERIC_ERROR = "Ha ocurrido un error en el sistema";
        const int NOTFOUNDCODE = -2147467259;

        // GET: Error
        [AllowAnonymous]
        public ActionResult Index()
        {

            try
            {
                Exception theException = (Exception)Session["handling_errors"];

                Uri pagina_anterior = (Uri)Session["temp_url"];

                ViewBag.pagina_anterior = pagina_anterior.AbsolutePath;

                return View(theException);
            }
            catch (Exception ex)
            {
                ex.ToString();

                return View();
            }
        }

        public ActionResult Index2()
        {

            Exception theException = (Exception)Session["handling_errors"];

            // Verify that the navigation to this page was a "system request" and not an "user one". If not: redirect to "error/notfound"
            if (Session["handling_errors"] == null)
            {
                Session["temp_url"] = Request.Url;
                return RedirectToAction("notfound");
            }
            else
                Session["handling_errors"] = null;




            string temp_date = DateTime.Now.ToUniversalTime().ToString();
            int temp_code = 0;
            int log_code = 0;
            string errorLine = "";
            string actualError = "";
            string temp_url = "";

            // Error message to show to the user
            string errorMessage = GENERIC_ERROR;

            if (Session["temp_url"] != null)
            {
                temp_url = Session["temp_url"].ToString();
                Session["temp_url"] = null;
            }
            else
                return RedirectToAction("disabledcookies");
            if (Session["temp_date"] != null)
            {
                temp_date = Session["temp_date"].ToString();
                Session["temp_date"] = null;
            }
            else
                return RedirectToAction("disabledcookies");
            if (Session["temp_code"] != null)
            {
                temp_code = Convert.ToInt32(Session["temp_code"]);
                Session["temp_code"] = null;

                // Since '0' is a non existing value (due to identity starting on '1' instead of '0'), then is an "error"
                if (temp_code != 0)
                {
                    var db = new dbIntranet();

                    var result = (from t in db.CodigoError
                                  join t2 in db.LogErrores
                                  on t.CodigoErrorId equals t2.CodigoErrorId
                                  orderby t2.LogErroresId descending
                                  where t.CodigoErrorId == temp_code
                                  select new
                                  {
                                      t2.LogErroresId,
                                      t.MensajeAmigable,
                                      t2.LineaError,
                                      t.MensajeError
                                  }).First();

                    log_code = result.LogErroresId;
                    errorMessage = result.MensajeAmigable;
                    errorLine = result.LineaError;
                    actualError = result.MensajeError;
                }
            }
            else
                return RedirectToAction("disabledcookies");

            return View(new string[] { temp_code.ToString(), temp_date, errorMessage, errorLine, actualError, log_code.ToString(), temp_url });
        }

        [AllowAnonymous]
        public ActionResult NotFound()
        {
            ViewBag.exception = (Exception)Session["handling_errors"];

            var temp_url = "requerida";

            if (Session["temp_url"] != null)
            {
                temp_url = Session["temp_url"].ToString();
                //Session["temp_url"] = null;
            }

            return View((object)temp_url);
        }

        [AllowAnonymous]
        public ActionResult DisabledCookies()
        {

            if (Session["handling_errors"] != null)
                Session["handling_errors"] = null;

            return View();
        }

        [AllowAnonymous]
        public ActionResult login() {

            return View();
        }
    }
}