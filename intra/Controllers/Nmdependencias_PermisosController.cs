﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;
using intra.Models;
using System.Reflection;
using System.Data.SqlClient;
using intra.Models.GestionHumana.Permisos;
using intra.Models.vw_Models;
using intra.Models.GestionHumana;

namespace intra.Controllers
{
    [Authorize(Roles = "RRHHA")]
    public class Nmdependencias_PermisosController : Controller
    {
        private dbIntranet db = new dbIntranet();
        //private dbIntranet obj = new dbIntranet();
        private Nmdeptos_Permisos smodel = new Nmdeptos_Permisos();
        private PGRINTRANETEntities2 db2 = new PGRINTRANETEntities2();

        // GET: Nmdependencias_Permisos    
        public ActionResult Index()
        {
            return View(db.Nmdependencias_Permisos.ToList());
        }

        // GET: Nmdependencias_Permisos/Create
        public ActionResult Create(FormCollection fC)
        {
            CargarListas();

            #region UsuarioDependencia

            string usuario = string.Empty;
            string msjNotFound = string.Empty;

            usuario = fC["UsuarioLogin"];

            msjNotFound = "No Existe Este Usuario";//Esto Captura el mensaje de notfound que envia JSON

            if (string.IsNullOrEmpty(usuario))
            {
                ViewBag.Error = "El Campo Usuario Es Requerido";
                ModelState.Clear();
                return View();
            }

            if (string.Compare(usuario, msjNotFound, true) == 0)
            {
                ViewBag.ErrorNotFound = "No se encontro registro de este usuario, Consultar al Administrador de Sistemas";
                ModelState.Clear();
                return View();
            }

            var empl = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.NombreCompleto == usuario).FirstOrDefault();

            if (string.IsNullOrEmpty(empl.Cedula) || empl.Cedula.Length < 13)
            {
                ViewBag.Error = "Este Usuario No Contiene una Cedula Valida Registrada, Consulte al Administrador de Sistemas";
                ModelState.Clear();
                return View();
            }
            #endregion

            string employee2 = Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(employee2, "Ingresó al módulo de Directores y Encargados.");

            return View();
        }

        // POST: Nmdependencias_Permisos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult Create(Nmdependencias_Permisos view_object, FormCollection fc)
        {
            return View();
        }

        // GET: Nmdependencias_Permisos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nmdependencias_Permisos nmdependencias_Permisos = db.Nmdependencias_Permisos.Find(id);
            if (nmdependencias_Permisos == null)
            {
                return HttpNotFound();
            }
            return View(nmdependencias_Permisos);
        }

        #region Mostrar_Listado_Informacion_Departamentos
        //private void MostrarListadoPor_Deptos()
        //{
        //    try
        //    {

        //        var Detalles = (from A in obj.Vw_Mostrar_Personal_Permisos_PGR
        //                        orderby A.NombreCompleto ascending
        //                        select new ListadoPersonalPgrModelo
        //                        {
        //                            EmpleadoId = A.Codigo,
        //                            Descripcion = A.NombreCompleto,
        //                            Cedula = A.Cedula,
        //                        }).ToList();

        //        smodel.Listado_EmpleadosPGR = Detalles;

        //    }
        //    catch (Exception ex)
        //    {
        //        ex.Message.ToString();
        //    }
        //}

        #endregion

        // GET: Nmdependencias_Permisos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nmdependencias_Permisos nmdependencias_Permisos = db.Nmdependencias_Permisos.Find(id);
            if (nmdependencias_Permisos == null)
            {
                return HttpNotFound();
            }
            return View(nmdependencias_Permisos);
        }

        // POST: Nmdependencias_Permisos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DependenciaID,Descripcion,DirectorID")] Nmdependencias_Permisos nmdependencias_Permisos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nmdependencias_Permisos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(nmdependencias_Permisos);
        }

        public ActionResult Auxiliar(FormCollection fC)
        {
            CargarListas();

            #region UsuarioDependencia

            string usuario = string.Empty;
            string msjNotFound = string.Empty;

            usuario = fC["UsuarioLogin"];

            msjNotFound = "No Existe Este Usuario";//Esto Captura el mensaje de notfound que envia JSON

            if (string.IsNullOrEmpty(usuario))
            {
                ViewBag.Error = "El Campo Usuario Es Requerido";
                ModelState.Clear();
                return View();
            }

            if (string.Compare(usuario, msjNotFound, true) == 0)
            {
                ViewBag.ErrorNotFound = "No se encontro registro de este usuario, Consultar al Administrador de Sistemas";
                ModelState.Clear();
                return View();
            }

            var empl = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.NombreCompleto == usuario).FirstOrDefault();

            if (string.IsNullOrEmpty(empl.Cedula) || empl.Cedula.Length < 13)
            {
                ViewBag.Error = "Este Usuario No Contiene una Cedula Valida Registrada, Consulte al Administrador de Sistemas";
                ModelState.Clear();
                return View();
            }
            #endregion

            string employee2 = Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(employee2, "Ingresó al módulo de Directores y Encargados.");

            return View();
        }


        public ActionResult DependenciaTecnicos()
        {
            var TecnicosLista = db.Sol_Tecnicos_Dependencias.ToList();

            foreach (var tecnico in TecnicosLista) {
                tecnico.Dependencia = db.Sol_DependenciasRegionales.Where(x=>x.DependenciaId == tecnico.DependenciaId).FirstOrDefault();
            }
            ViewBag.Dependencias = new SelectList(db.Sol_DependenciasRegionales,"DependenciaId","DependenciaNombre");
            return View("Tecnicos",TecnicosLista);
        }

        [HttpPost]
        public JsonResult AgregarTecnicos(Sol_Tecnico_Dependencia tecnico)
        {
            var empleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == tecnico.TecnicoDependenciaEmpleadoId.ToString()).FirstOrDefault();

            if (empleado == null)
            { return Json("Usuario Inválido"); }

            if (db.Sol_Tecnicos_Dependencias.Where(x => x.TecnicoDependenciaEmpleadoId.ToString() == empleado.empleadoid 
            && x.DependenciaId == tecnico.DependenciaId && x.DependenciaId == tecnico.DependenciaId).FirstOrDefault() != null)
            { return Json("Este Usuario ya esta registado para esta dependencia"); }

            tecnico.TecnicoDependenciaEstado = true;
            tecnico.TecnicoDependenciaFhCreacion = DateTime.Now;
            tecnico.TecnicoDependenciaUsuarioCreador = Session["usuario"].ToString();
            tecnico.TecnicoDependenciaCedula = empleado.cedula;
            tecnico.TecnicoDependenciaNombre = empleado.nombre;

            db.Sol_Tecnicos_Dependencias.Add(tecnico);
            db.SaveChanges();

            return Json("Guardado");
        }

        [HttpPost]
        public JsonResult CambiarEstado(int id)
        {
            try
            {
                Sol_Tecnico_Dependencia tecnico = db.Sol_Tecnicos_Dependencias.Find(id);

                tecnico.TecnicoDependenciaEstado = !tecnico.TecnicoDependenciaEstado;
                db.SaveChanges();

                return Json(tecnico.TecnicoDependenciaEstado ? "Activado" : "Desactivado");
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json("Ha ocurrido un error procesando su consulta");
            }
        }

        [HttpPost]
        public JsonResult CambiarEstadoSolicitante(int id)
        {
            try
            {
                Sol_Tecnico_Dependencia tecnico = db.Sol_Tecnicos_Dependencias.Find(id);

                tecnico.TecnicoDependenciaCrearSolicitante = !tecnico.TecnicoDependenciaCrearSolicitante;
                db.SaveChanges();

                return Json(tecnico.TecnicoDependenciaCrearSolicitante ? "Activado" : "Desactivado");
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json("Ha ocurrido un error procesando su consulta");
            }
        }

        [HttpPost]
        public JsonResult SetInCharge(Nmdependencias_Permisos view_object, FormCollection fc)
        {
            string employee2 = Session["empleadoId"].ToString();

            if (view_object.NombreTablaAGuardar.Equals("DEPENDENCIA"))
            {
                if (view_object.DirectorDependencia != null && view_object.DirectorDependencia != "")
                {
                    var dependencia = db.Nmdependencias_Permisos.Where(x => x.DependenciaID == view_object.DependenciaID).FirstOrDefault();

                    try
                    {
                        string usuario_id = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.NombreCompleto.Trim() == view_object.DirectorDependencia.ToUpper()).FirstOrDefault().Codigo;

                        var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("exec sp_verif_Encargados {0}", usuario_id)).FirstOrDefault();

                        if (j != null)
                        {
                            if (j.Procedencia == "N/A" && j.Nombre == "N/A")
                            {
                                dependencia.DirectorID = Convert.ToInt32(usuario_id);

                                //db.Entry(dependencia).State = EntityState.Modified;
                                //db.Database.ExecuteSqlCommand(@"UPDATE Nmdependencias_Permisos SET DirectorID = {0} WHERE DependenciaID = {1}", dependencia.DirectorID, dependencia.DependenciaID);
                                //db.SaveChanges();

                                //var dependencia_success = db2.Database.ExecuteSqlCommand("sp_ActualizarEncargados @param1,@param2,@param3,@param4,@param5", new SqlParameter("param1", dependencia.DependenciaID),new SqlParameter("param2",0),new SqlParameter("param3",0), new SqlParameter("param4",0), new SqlParameter("param5", dependencia.DirectorID));
                                var dependencia_success = db2.sp_ActualizarEncargados(dependencia.DependenciaID, 0, 0, 0, dependencia.DirectorID);

                                db2.SaveChanges();

                                RegisterLogs rl = new RegisterLogs(employee2, "Agregó a " + usuario_id + " como director(a) de la dependencia " + dependencia.DependenciaID + ".");
                            }
                            else
                            {
                                return Json("Duplicated");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //CargarListas();
                        //return View();
                        var m = ex.Message;
                        return Json("Error");
                    }
                }
                else
                {
                    //CargarListas();
                    //return View();
                    return Json("Error");
                }
            }
            else if (view_object.NombreTablaAGuardar.Equals("DEPARTAMENTO"))
            {
                if (view_object.EncargadoDepartamento != null && view_object.EncargadoDepartamento != "")
                {
                    var departamento = db.Nmdeptos_Permisos.Where(x => x.DependenciaID == view_object.DependenciaID && x.DeptoID == view_object.DeptoID).FirstOrDefault();
                    //var departamento = (from t in db.Nmdeptos_Permisos
                    //                    where t.DependenciaID == view_object.DependenciaID
                    //                        && t.DeptoID == view_object.DeptoID
                    //                    select t).FirstOrDefault();

                    try
                    {
                        string usuario_id = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.NombreCompleto.Trim() == view_object.EncargadoDepartamento.ToUpper()).FirstOrDefault().Codigo;

                        var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("exec sp_verif_Encargados {0}", usuario_id)).FirstOrDefault();

                        if (j != null)
                        {
                            if (j.Procedencia == "N/A" && j.Nombre == "N/A")
                            {
                                departamento.EncargadoID = Convert.ToInt32(usuario_id);

                                //db.Database.ExecuteSqlCommand(@"UPDATE Nmdeptos_Permisos SET EncargadoID = {0} WHERE DeptoID = {1} AND DependenciaID = {2}", departamento.EncargadoID, departamento.DeptoID, departamento.DependenciaID);
                                //var departamento_success = db2.Database.ExecuteSqlCommand("sp_ActualizarEncargados @param1,@param2,@param3,@param4,@param5", new SqlParameter("param1", departamento.DependenciaID), new SqlParameter("param2", departamento.DeptoID), new SqlParameter("param3", 0), new SqlParameter("param4", 0), new SqlParameter("param5", departamento.EncargadoID));
                                var departamento_success = db2.sp_ActualizarEncargados(departamento.DependenciaID, departamento.DeptoID, 0, 0, departamento.EncargadoID);
                                //var departamento_success = db2.Database.ExecuteSqlCommand(string.Format("exec sp_ActualizarEncargados {0},{1},{2},{3},{4}", departamento.DependenciaID, departamento.DeptoID, 0, 0, departamento.EncargadoID));
                                db2.SaveChanges();

                                RegisterLogs rl = new RegisterLogs(employee2, "Agregó a " + usuario_id + " como encargado(a) del departtamento " + departamento.DependenciaID + "," + departamento.DeptoID + ".");
                            }
                            else
                            {
                                return Json("Duplicated");
                            }
                        }
                    }
                    catch (Exception)
                    {
                        //CargarListas();
                        //return View();
                        return Json("Error");
                    }
                }
                else
                {
                    //CargarListas();
                    //return View();
                    return Json("Error");
                }
            }
            else if (view_object.NombreTablaAGuardar.Equals("DIVISION"))
            {
                if (view_object.EncargadoDivision != null && view_object.EncargadoDivision != "")
                {
                    var division = db.Nmdivision_Permisos.Where(x => x.DependenciaID == view_object.DependenciaID && x.DeptoID == view_object.DeptoID && x.DivisionID == view_object.DivisionID).FirstOrDefault();

                    try
                    {
                        string usuario_id = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.NombreCompleto.Trim() == view_object.EncargadoDivision.ToUpper()).FirstOrDefault().Codigo;

                        var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("exec sp_verif_Encargados {0}", usuario_id)).FirstOrDefault();

                        if (j != null)
                        {
                            if (j.Procedencia == "N/A" && j.Nombre == "N/A")
                            {
                                if (division != null)
                                {
                                    division.EncargadoID = Convert.ToInt32(usuario_id);
                                    var division_success = db2.sp_ActualizarEncargados(division.DependenciaID, division.DeptoID, division.DivisionID, 0, division.EncargadoID);

                                    db2.SaveChanges();

                                    RegisterLogs rl = new RegisterLogs(employee2, "Agregó a " + usuario_id + " como encargado(a) de la división " + division.DependenciaID + "," + division.DeptoID + "," + division.DivisionID + ".");
                                }
                                else
                                {
                                    Nmdivision_Permisos permisos = new Nmdivision_Permisos();

                                    permisos.EncargadoID = Convert.ToInt32(usuario_id);
                                    permisos.DependenciaID = view_object.DependenciaID;
                                    permisos.DeptoID = view_object.DeptoID;
                                    permisos.DivisionID = view_object.DivisionID;
                                    db.Nmdivision_Permisos.Add(permisos);
                                    db.SaveChanges();

                                    var division1 = db.Nmdivision_Permisos.Where(x=> x.DivisionID== permisos.DivisionID && x.DependenciaID == permisos.DependenciaID).FirstOrDefault();


                                    var division_success = db2.sp_ActualizarEncargados(division1.DependenciaID, division1.DeptoID, division1.DivisionID, 0, division1.EncargadoID);

                                    db2.SaveChanges();

                                    RegisterLogs rl = new RegisterLogs(employee2, "Agregó a " + usuario_id + " como encargado(a) de la división " + division1.DependenciaID + "," + division1.DeptoID + "," + division1.DivisionID + ".");

                                }
                                //return Json("Success");
                            }

                            else
                            {
                                return Json("Duplicated");
                            }
                        }
                       
                    }
                    catch (Exception error)
                    {
                        error.ToString();
                        var message = error.Message;
                        //CargarListas();
                        //return View();
                        return Json("Error");
                    }
                }
                else
                {
                    //CargarListas();
                    //return View();
                    return Json("");
                }

            }
            else if (view_object.NombreTablaAGuardar.Equals("SECCION"))
            {
                if (view_object.EncargadoSeccion != null && view_object.EncargadoSeccion != "")
                {
                    var seccion = db.NmSecciones_Permisos.Where(x => x.DependenciaID == view_object.DependenciaID && x.DeptoID == view_object.DeptoID && x.DivisionID == view_object.DivisionID && x.SeccionID == view_object.SeccionID).FirstOrDefault();

                    try 
                    {
                        string usuario_id = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.NombreCompleto.Trim() == view_object.EncargadoSeccion.ToUpper()).FirstOrDefault().Codigo;

                        var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("exec sp_verif_Encargados {0}", usuario_id)).FirstOrDefault();

                        if (j != null)
                        {
                            if (j.Procedencia == "N/A" && j.Nombre == "N/A")
                            {
                                seccion.EncargadoID = Convert.ToInt32(usuario_id);
                                //db.Entry(seccion).State = EntityState.Modified;
                                //string query = string.Format("UPDATE NmSecciones_Permisos SET EncargadoID = {0} WHERE SeccionID = {1} AND DivisionID = {2} AND DeptoID = {3} AND DependenciaID = {4}", seccion.EncargadoID, seccion.SeccionID, seccion.DivisionID, seccion.DeptoID, seccion.DependenciaID);
                                //db.Database.ExecuteSqlCommand(query);
                                //db.SaveChanges();

                                //var seccion_success = db2.Database.ExecuteSqlCommand("sp_ActualizarEncargados @param1,@param2,@param3,@param4,@param5", new SqlParameter("param1", seccion.DependenciaID), new SqlParameter("param2", seccion.DeptoID), new SqlParameter("param3", seccion.DivisionID), new SqlParameter("param4", seccion.SeccionID), new SqlParameter("param5", seccion.EncargadoID));
                                //var seccion_success = db2.Database.ExecuteSqlCommand(string.Format("exec sp_ActualizarEncargados {0},{1},{2},{3},{4}", seccion.DependenciaID, seccion.DeptoID, seccion.DivisionID, seccion.SeccionID, seccion.EncargadoID));
                                var seccion_success = db2.sp_ActualizarEncargados(seccion.DependenciaID, seccion.DeptoID, seccion.DivisionID, seccion.SeccionID, seccion.EncargadoID);

                                db2.SaveChanges();

                                RegisterLogs rl = new RegisterLogs(employee2, "Agregó a " + usuario_id + " como encargado(a) de la sección " + seccion.DependenciaID + "," + seccion.DeptoID + "," + seccion.DivisionID + "," + seccion.SeccionID + ".");
                            }
                            else
                            {
                                return Json("Duplicated");
                            }
                        }

                    }
                    catch (Exception error)
                    {
                        var m = error.Message;

                        //CargarListas();
                        //return View();
                        return Json("Error");
                    }
                }
                else
                {
                    //CargarListas();
                    //return View();
                    return Json("Error");
                }
            }

            //CargarListas();
            //return View();
            return Json("Success");
        }

        public JsonResult SetAuxiliar(Nmdependencias_Permisos view_object)
        {
            string employee2 = Session["empleadoId"].ToString();

            if (view_object.NombreTablaAGuardar.Equals("DEPENDENCIA"))
            {
                if (view_object.AuxiliarDependencia != null && view_object.AuxiliarDependencia != "")
                {
                    var dependencia = db.Nmdependencias_Permisos.Where(x => x.DependenciaID == view_object.DependenciaID).FirstOrDefault();

                    try
                    {
                        string empleadoId = db.VISTA_EMPLEADOS.Where(x => x.nombre == view_object.AuxiliarDependencia.ToUpper()).FirstOrDefault().empleadoid;

                        var infoEncargado = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("exec sp_verif_Encargados {0}", empleadoId)).FirstOrDefault();
                        var infoAuxiliar = db.Database.SqlQuery<sp_verif_Auxiliar_Result>(string.Format("exec sp_verif_Auxiliar '{0}'", empleadoId)).FirstOrDefault();

                        if (infoEncargado.Procedencia == "N/A" && infoEncargado.Nombre == "N/A" && infoAuxiliar.Procedencia == "N/A" && infoAuxiliar.Nombre == "N/A")
                        {
                            int result = db.Database.ExecuteSqlCommand($"sp_ActualizarAuxiliar {dependencia.DependenciaID}, 0, 0, 0, {empleadoId}");

                            db2.SaveChanges();

                            RegisterLogs rl = new RegisterLogs(employee2, "Agregó a " + empleadoId + " como auxiliar de la dependencia " + dependencia.DeptoID + ".");
                        }
                        else
                        {
                            return Json("Duplicated");
                        }
                    }
                    catch (Exception ex)
                    {
                        //CargarListas();
                        //return View();
                        var m = ex.Message;
                        return Json("Error");
                    }
                }
                else
                {
                    //CargarListas();
                    //return View();
                    return Json("Error");
                }
            }
            else if (view_object.NombreTablaAGuardar.Equals("DEPARTAMENTO"))
            {
                if (view_object.AuxiliarDependencia != null && view_object.AuxiliarDependencia != "")
                {
                    var departamento = db.Nmdeptos_Permisos.Where(x => x.DependenciaID == view_object.DependenciaID && x.DeptoID == view_object.DeptoID).FirstOrDefault();

                    try
                    {
                        string empleadoId = db.VISTA_EMPLEADOS.Where(x => x.nombre == view_object.AuxiliarDepartamento.ToUpper()).FirstOrDefault().empleadoid;

                        var infoEncargado = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("exec sp_verif_Encargados {0}", empleadoId)).FirstOrDefault();
                        var infoAuxiliar = db.Database.SqlQuery<sp_verif_Auxiliar_Result>(string.Format("exec sp_verif_Auxiliar '{0}'", empleadoId)).FirstOrDefault();

                        if (infoEncargado.Procedencia == "N/A" && infoEncargado.Nombre == "N/A" && infoAuxiliar.Procedencia == "N/A" && infoAuxiliar.Nombre == "N/A")
                        {
                            int result = db.Database.ExecuteSqlCommand($"sp_ActualizarAuxiliar {departamento.DependenciaID}, {departamento.DeptoID}, 0, 0, {empleadoId}");

                            db2.SaveChanges();

                            RegisterLogs rl = new RegisterLogs(employee2, "Agregó a " + empleadoId + " como auxiliar de la dependencia " + departamento.DeptoID + ".");
                        }
                        else
                        {
                            return Json("Duplicated");
                        }
                    }
                    catch (Exception ex)
                    {
                        //CargarListas();
                        //return View();
                        var m = ex.Message;
                        return Json("Error");
                    }
                }
                else
                {
                    //CargarListas();
                    //return View();
                    return Json("Error");
                }
            }
            else if (view_object.NombreTablaAGuardar.Equals("DIVISION"))
            {
                if (view_object.AuxiliarDependencia != null && view_object.AuxiliarDependencia != "")
                {
                    var division = db.Nmdivision_Permisos.Where(x => x.DependenciaID == view_object.DependenciaID && x.DeptoID == view_object.DeptoID && x.DivisionID == view_object.DivisionID).FirstOrDefault();

                    try
                    {
                        string empleadoId = db.VISTA_EMPLEADOS.Where(x => x.nombre == view_object.AuxiliarDivision.ToUpper()).FirstOrDefault().empleadoid;

                        var infoEncargado = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("exec sp_verif_Encargados {0}", empleadoId)).FirstOrDefault();
                        var infoAuxiliar = db.Database.SqlQuery<sp_verif_Auxiliar_Result>(string.Format("exec sp_verif_Auxiliar '{0}'", empleadoId)).FirstOrDefault();

                        if (infoEncargado.Procedencia == "N/A" && infoEncargado.Nombre == "N/A" && infoAuxiliar.Procedencia == "N/A" && infoAuxiliar.Nombre == "N/A")
                        {
                            int result = db.Database.ExecuteSqlCommand($"sp_ActualizarAuxiliar {division.DependenciaID}, {division.DeptoID}, {division.DivisionID}, 0, {empleadoId}");

                            db2.SaveChanges();

                            RegisterLogs rl = new RegisterLogs(employee2, "Agregó a " + empleadoId + " como auxiliar de la dependencia " + division.DivisionID + ".");
                        }
                        else
                        {
                            return Json("Duplicated");
                        }
                    }
                    catch (Exception ex)
                    {
                        //CargarListas();
                        //return View();
                        var m = ex.Message;
                        return Json("Error");
                    }
                }
                else
                {
                    //CargarListas();
                    //return View();
                    return Json("Error");
                }
            }
            else if (view_object.NombreTablaAGuardar.Equals("SECCION"))
            {
                if (view_object.AuxiliarDependencia != null && view_object.AuxiliarDependencia != "")
                {
                    var seccion = db.NmSecciones_Permisos.Where(x => x.DependenciaID == view_object.DependenciaID && x.DeptoID == view_object.DeptoID && x.DivisionID == view_object.DivisionID && x.SeccionID == view_object.SeccionID).FirstOrDefault();

                    try
                    {
                        string empleadoId = db.VISTA_EMPLEADOS.Where(x => x.nombre == view_object.AuxiliarSeccion.ToUpper()).FirstOrDefault().empleadoid;

                        var infoEncargado = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("exec sp_verif_Encargados {0}", empleadoId)).FirstOrDefault();
                        var infoAuxiliar = db.Database.SqlQuery<sp_verif_Auxiliar_Result>(string.Format("exec sp_verif_Auxiliar '{0}'", empleadoId)).FirstOrDefault();

                        if (infoEncargado.Procedencia == "N/A" && infoEncargado.Nombre == "N/A" && infoAuxiliar.Procedencia == "N/A" && infoAuxiliar.Nombre == "N/A")
                        {
                            int result = db.Database.ExecuteSqlCommand($"sp_ActualizarAuxiliar {seccion.DependenciaID}, {seccion.DeptoID}, {seccion.DivisionID}, {seccion.SeccionID}, {empleadoId}");

                            db2.SaveChanges();

                            RegisterLogs rl = new RegisterLogs(employee2, "Agregó a " + empleadoId + " como auxiliar de la dependencia " + seccion.SeccionID + ".");
                        }
                        else
                        {
                            return Json("Duplicated");
                        }
                    }
                    catch (Exception ex)
                    {
                        //CargarListas();
                        //return View();
                        var m = ex.Message;
                        return Json("Error");
                    }
                }
                else
                {
                    //CargarListas();
                    //return View();
                    return Json("Error");
                }
            }

            //CargarListas();
            //return View();
            return Json("Success");
        }

        [HttpPost]
        public JsonResult Departamento(string dependencia)
        {
            List<SelectListItem> deptos;

            int cl = int.Parse(dependencia);
            deptos = db.vw_Departamentos.Where(x => x.DependenciasID == dependencia).Select(x => new SelectListItem() { Text = x.Descripcion, Value = x.DeptoID.ToString() }).ToList();

            vw_Dependencias dependencia2 = db.vw_Dependencias.Where(x => x.DependenciaID == cl).FirstOrDefault();
            Nmdependencias_Permisos dependencia2b = db.Nmdependencias_Permisos.Where(x => x.DependenciaID == cl).FirstOrDefault();

            intra.Code.Complete comp = new intra.Code.Complete();

            if (dependencia2.DirectorID != null)
            {
                try
                {
                    var nombre = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == dependencia2.DirectorID.ToString()).FirstOrDefault().nombre;
                    var nombre2 = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == dependencia2b.AuxiliarID.Value.ToString()).FirstOrDefault().nombre;
                    comp.chain = nombre == "JONATHAN ARIAS GUEVARA" ? "" : nombre;
                    comp.chain2 = nombre2 == "JONATHAN ARIAS GUEVARA" ? "" : nombre2;
                }
                catch(Exception)
                {
                    comp.chain = "";
                }
                
            }

            comp.selected_list = new SelectList(deptos, "Value", "Text");

            return Json(comp);
        }

        [HttpPost]
        public JsonResult Division(string depto, string dependencia)
        {
            List<SelectListItem> divisiones;

            int cl = int.Parse(depto);
            int cl2 = int.Parse(dependencia);
            
            divisiones = db.vw_Divisiones.Where(x => x.DeptoID == cl && x.DependenciaID == cl2).Select(x => new SelectListItem() { Text = x.Descripcion, Value = x.DivisionID.ToString() }).ToList();

            Nmdeptos_Permisos depto2 = db.Nmdeptos_Permisos.Where(x => x.DeptoID == cl && x.DependenciaID == cl2).FirstOrDefault();

            intra.Code.Complete comp = new intra.Code.Complete();

            if (depto2.EncargadoID != null)
            {
                try
                {
                    var nombre = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == depto2.EncargadoID.ToString()).FirstOrDefault().nombre;
                    var nombre2 = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == depto2.AuxiliarID.Value.ToString()).FirstOrDefault().nombre;
                    comp.chain = nombre == "JONATHAN ARIAS GUEVARA" ? "" : nombre;
                    comp.chain2 = nombre2 == "JONATHAN ARIAS GUEVARA" ? "" : nombre2;
                }
                catch(Exception)
                {
                    comp.chain = "";
                } 
            }

            comp.selected_list = new SelectList(divisiones, "Value", "Text");

            return Json(comp);
        }

        [HttpPost]
        public JsonResult Seccion(string division, string depto, string dependencia)
        {
            List<SelectListItem> secciones;

            int cl = int.Parse(division);
            int cl2 = int.Parse(depto);
            int cl3 = int.Parse(dependencia);

            secciones = db.vw_Secciones.Where(x => x.DivisionID == cl && x.DeptoID == depto && x.DependenciaID == cl3).Select(x => new SelectListItem() { Text = x.Descripcion, Value = x.SeccionID.ToString() }).ToList();

            Nmdivision_Permisos division2 = db.Nmdivision_Permisos.Where(x => x.DivisionID == cl && x.DeptoID == cl2 && x.DependenciaID == cl3).FirstOrDefault();

            intra.Code.Complete comp = new intra.Code.Complete();

            try
            {
                var nombre = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == division2.EncargadoID.ToString()).FirstOrDefault().NombreCompleto;
                var nombre2 = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == division2.AuxiliarID.Value.ToString()).FirstOrDefault().nombre;
                var is_him = nombre == "JONATHAN ARIAS GUEVARA";
                comp.chain = is_him ? "" : nombre;
                comp.chain2 = nombre2 == "JONATHAN ARIAS GUEVARA" ? "" : nombre2;
            }
            catch (Exception)
            {
                comp.chain = "";
                comp.chain2 = "";
            }

            comp.selected_list = new SelectList(secciones, "Value", "Text");

            return Json(comp);
        }

        [HttpPost]
        public JsonResult GetEncargadoSeccion(string division, string depto, string dependencia,string seccion)
        {
            try
            {
                int depedencia_id = Convert.ToInt32(dependencia);
                int departamento_id = Convert.ToInt32(depto);
                int division_id = Convert.ToInt32(division);
                int seccion_id = Convert.ToInt32(seccion);

                var encargado = db.NmSecciones_Permisos.Where(x => x.DependenciaID == depedencia_id && x.DeptoID == departamento_id && x.DivisionID == division_id && x.SeccionID == seccion_id).FirstOrDefault();
                int codigo_encargado = 0;

                if(encargado.EncargadoID!=null)
                {
                    codigo_encargado = encargado.EncargadoID.Value;
                }else
                {
                    return Json("NotFound");
                }
                
                string nombre_empleado = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == codigo_encargado.ToString()).FirstOrDefault().NombreCompleto;
                int? auxiliarId = db.NmSecciones_Permisos.Where(x => x.DependenciaID == depedencia_id && x.SeccionID == seccion_id).FirstOrDefault().AuxiliarID;
                string nombreAuxiliar = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == auxiliarId.ToString()).FirstOrDefault().nombre;

                return Json(new {
                    EncargadoNombre = nombre_empleado,
                    AuxiliarNombre = nombreAuxiliar
                });
            }
            catch(Exception error)
            {
                var m = error.Message;

                return Json("Error");
            }
        }

        //Llamada Ajax Para Cargar Dinamicamente Usuarios
        public JsonResult GetUsuarios(string parameter1, string parameter2, string parameter3, string parameter4, string parameter5)
        {
            List<string> ulpad = new List<string>() ;

            //if (string.IsNullOrEmpty(parameter1))
            //    parameter1 = "0";

            //if (string.IsNullOrEmpty(parameter2))
            //    parameter2 = "0";

            //if (string.IsNullOrEmpty(parameter3))
            //    parameter3 = "0";

            //if (string.IsNullOrEmpty(parameter4))
            //    parameter4 = "0";

            parameter1 = "0";
            parameter2 = "0";
            parameter3 = "0";
            parameter4 = "0";

            List<Vw_Mostrar_Personal_Permisos_PGR> usuarios_filtrados = db.Database.SqlQuery<Vw_Mostrar_Personal_Permisos_PGR>(string.Format("sp_filtro_Autocomplete {0}, {1}, {2}, {3}",
                parameter1, parameter2, parameter3, parameter4)).ToList();
            
            //List<Vw_Mostrar_Personal_Permisos_PGR> usuarios_filtrados2 = new List<Vw_Mostrar_Personal_Permisos_PGR>();

            //foreach (var item in usuarios_filtrados)
            //{
            //    //var en_dependencia = db.Nmdependencias_Permisos.Where(x => x.DirectorID.ToString() == item.Codigo).FirstOrDefault() != null ? true : false;
            //    //var en_departamento = db.Nmdeptos_Permisos.Where(x => x.EncargadoID.ToString() == item.Codigo).FirstOrDefault() != null ? true : false;
            //    //var en_division = db.Nmdivision_Permisos.Where(x => x.EncargadoID.ToString() == item.Codigo).FirstOrDefault() != null ? true : false;
            //    //var en_seccion = db.NmSecciones_Permisos.Where(x => x.EncargadoID.ToString() == item.Codigo).FirstOrDefault() != null ? true : false;

            //    var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("sp_verif_Encargados {0}", item.Codigo)).FirstOrDefault();

            //    if (j.Nombre=="N/A" && j.Procedencia=="N/A")
            //    {
            //        usuarios_filtrados2.Add(item);
            //    }
            //}

            if (!string.IsNullOrEmpty(parameter5))
                ulpad = usuarios_filtrados.Where(x => x.NombreCompleto.StartsWith(parameter5.ToUpper()) || x.Codigo.StartsWith(parameter5.ToUpper())).Select(y => y.NombreCompleto).ToList();
            
            if (ulpad.Count <= 0)
            {
                ulpad.Add("No Existe Este Usuario");
            }

            return Json(ulpad, JsonRequestBehavior.AllowGet);
        }

        public void CargarListas()
        {
            ViewBag.Dependencia = new SelectList(db.vw_Dependencias.ToList().Select(obj => new
            {
                DependenciaID = obj.DependenciaID,
                Descripcion = obj.Descripcion,
                DirectorID = (obj.DirectorID == null ? "" : obj.DirectorID.ToString())
            }), "DependenciaID", "Descripcion");

            ViewBag.Depto = new SelectList(db.vw_Departamentos.ToList().Select(obj => new
            {
                DeptoID = obj.DeptoID,
                Descripcion = obj.Descripcion,
                EncargadoID = (obj.EncargadoID == null ? "" : obj.EncargadoID.ToString())
            }), "DeptoID", "Descripcion");

            ViewBag.Division = new SelectList(db.vw_Divisiones.ToList().Select(obj => new
            {
                DivisionID = obj.DivisionID,
                DeptoID = obj.DeptoID,
                Descripcion = obj.Descripcion,
                EncargadoID = (obj.EncargadoID == null ? "" : obj.EncargadoID.ToString())
            }), "DeptoID", "Descripcion");

            ViewBag.Seccion = new SelectList(db.vw_Secciones.ToList().Select(obj => new
            {
                SeccionID = obj.SeccionID,
                DivisionID = obj.DivisionID,
                DeptoID = obj.DeptoID,
                Descripcion = obj.Descripcion,
                EncargadoID = (obj.EncargadoID == null ? "" : obj.EncargadoID.ToString())
            }), "DeptoID", "Descripcion");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
