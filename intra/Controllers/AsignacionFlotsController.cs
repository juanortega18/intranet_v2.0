﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Data.SqlClient;
using intra;
//using Microsoft.Reporting.WebForms;
//using Microsoft.ReportingServices;
//using Microsoft.ReportingServices.Diagnostics.Utilities;
using System.Globalization;
using System.IO;
using Rotativa;
using Rotativa.Options;
using intra.Models.ViewModel;
using intra.Models;
using intra.Models.FlotasExtensiones;
using intra.Code;

namespace intra.Controllers
{
    public class AsignacionFlotsController : Controller
    {
        private dbIntranet db = new dbIntranet();

        #region Metodo y Action para Generar el Reporte de Asignacion

        //Metodo para crear una vista(Entity) para el Reporte de Asignacion de Flotas
        private List<VwAsignacionFlotas> ReportAsignacion(string cedula)
        {

            var ReportQuery = (from af in db.AsignacionFlots
                               from mf in db.MarcasFlotas
                               from md in db.ModeloFlotas
                               from ms in db.EstatusFlotas
                               where af.MarcaId == mf.MarcaId.ToString()
                               where af.ModeloId == md.ModeloId.ToString()
                               where af.EstatusId == ms.EstatusId.ToString()
                               where af.AsignacionCedula == cedula
                               select new VwAsignacionFlotas
                               {
                                   AsignacionId = af.AsignacionId,
                                   AsignacionEmpleado = af.AsignacionEmpleado,
                                   AsignacionCedula = af.AsignacionCedula,
                                   AsignacionCargo = af.AsignacionCargo,
                                   AsignacionEmpleadoId = af.AsignacionEmpleadoId,
                                   AsignacionDepto = af.AsignacionDepto,
                                   AsignacionExt = af.AsignacionExt,
                                   AsignacionEstado = ms.EstatusDescripcion,
                                   MarcaDescripcion = mf.MarcaDescripcion,
                                   ModeloDescripcion = md.ModeloDescripcion,
                                   AsignacionImei = af.AsignacionImei,
                                   AsignacionSimNumeracion = af.AsignacionSimNumeracion,
                                   AsignacionNoFlota = af.AsignacionNoFlota,
                                   costo = af.AsignacionCosto,
                                   AsignacionNoReferencia = af.AsignacionNoReferencia,
                                   Fecha = af.AsignacionFechaCreacion
                               }).OrderByDescending(af=>af.AsignacionId).ToList();
            return ReportQuery;
        }

        #region FuncionFormato
        public static string FormatDouble(object dDouble)
        {
            return String.Format("{0:#,###,###,##0.00;(#,###,###,##0.00);###.##}", dDouble);
        }

        public static string FormatDouble(string sFormat, object dDouble)
        {
            return String.Format(sFormat, dDouble);
        }
        #endregion


        //Action que Genera el Reporte de Asignacion de Flota
        [HttpGet]
        public ActionResult DetailsReport(int? id) {
            if (id!=null){

                var asign = db.AsignacionFlots.Find(id);

                ViewBag.id = asign.AsignacionId;
                ViewBag.costo = asign.AsignacionCosto;
                ViewBag.fechaflota = asign.AsignacionFechaCreacion;

                if (asign == null) { return RedirectToAction("index"); }

                string cedula = asign.AsignacionCedula;

                var listaFlotas = ReportAsignacion(cedula);

                return new ViewAsPdf("Reporte", listaFlotas) {
                   PageSize = Size.Letter
                };
        }
            return RedirectToAction("index");
        }
        #endregion

        [HttpPost]
        public JsonResult ObtenerIdFlota()
        {
            var flota = db.AsignacionFlots.OrderByDescending(x => x.AsignacionId).FirstOrDefault();
            return Json(flota.AsignacionId);

        }

        [Authorize(Roles = "AFA,AFWR")]
        public ActionResult DetailsReports(int? id) {
            var asign = db.AsignacionFlots.Find(id);

            ViewBag.id = asign.AsignacionId;
            ViewBag.costo = asign.AsignacionCosto;
            ViewBag.fechaflota = asign.AsignacionFechaCreacion;

            if (asign == null) { return RedirectToAction("index"); }

            string cedula = asign.AsignacionCedula;

            var listaFlotas = ReportAsignacion(cedula);

            return View("Reporte", listaFlotas);
        }


        [HttpGet]
        [Authorize(Roles = "AFA,AFWR")]
        public ActionResult CrearExtensiones()
        {

            var asignadoEmpleado = (from a in db.Vw_Mostrar_Personal_Permisos_PGR

                                    select new Models.PersonalPgrModel
                                    {
                                        EmpleadoId = a.Codigo,
                                        Descripcion = a.NombreCompleto,
                                        Cedula = a.Cedula,

                                    }).OrderBy(x => x.Descripcion).Where(x => x.EmpleadoId != "1199701");
            ViewBag.tecnico = new SelectList(asignadoEmpleado.ToList(), "EmpleadoId", "Descripcion");
            ViewBag.DependenciaID1 = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");
            ViewBag.DeptoID1 = new SelectList(db.vw_Departamentos.ToList(), "DeptoID", "Descripcion");

            return View();
        }
        [HttpPost]
        public JsonResult BuscarCorreo(string codigoEmpleado)
        {
         string Correo =  Code.Utilities2.obtenerSp_UserPorCodigo(codigoEmpleado)?.mail;
            ViewBag.Email = Correo;

            return Json(Correo);
        }

        //Seleccionar Persona
        [HttpPost]
        public JsonResult SeleccionarPersona(string departamento, string dependencia)
        {

            var empleado = db.VISTA_EMPLEADOS.Where(x => x.departamentoid == departamento && x.dependenciaid == dependencia).OrderBy(x => x.nombre).Select(x => new Models.PersonalPgrModel
            {
                EmpleadoId = x.empleadoid,
                Descripcion = x.nombre,
                Cedula = x.cedula,

            }).ToList();
            return Json(empleado);
        }

        //Seleccionar Departamento
        [HttpPost]
        public JsonResult SeleccionarDepartamento(int DependenciaId)
        {
            var Departamento = db.vw_Departamentos.Where(x => x.DependenciasID == DependenciaId.ToString()).Distinct().ToList();
            return Json(Departamento);
        }

        [HttpPost]
        [Authorize(Roles = "AFA,AFWR")]
        public JsonResult GuardarExtensiones(string codigo, string Extension)
        {

            var extensiones = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpEmpleadoId.ToString() == codigo).FirstOrDefault();

            if (extensiones != null)
            {
                extensiones.FlotExtEmpExtesion = Extension;
                db.SaveChanges();
                return Json(new { success = true, Mensaje = "Datos Actualizados Correctamente" }, JsonRequestBehavior.AllowGet);
            }
            else if (extensiones == null)
            {
                FlotaExtensionEmpleado ext = new FlotaExtensionEmpleado();
                ext.FlotExtEmpDatosConfirmados = null;
                ext.FlotExtEmpEmpleadoId = Convert.ToInt32(codigo);
                ext.FlotExtEmpExtesion = Extension;
                ext.FlotExtEmpFlota = "";
                ext.NumeroFlotaViejo = "";
                db.FlotaExtensionEmpleado.Add(ext);
                db.SaveChanges();
                ViewBag.DependenciaID = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");
                ViewBag.DeptoID = new SelectList(db.vw_Departamentos.ToList(), "DeptoID", "Descripcion");


                return Json(new { success = true, Mensaje = "Datos Guardado Correctamente" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                ViewBag.DependenciaID = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");
                ViewBag.DeptoID = new SelectList(db.vw_Departamentos.ToList(), "DeptoID", "Descripcion");
                return Json(new { success = false, Mensaje = "Ha Ocurrido un error" }, JsonRequestBehavior.AllowGet);

            }


        }



        //Actualizar Extensiones
        [HttpGet]
        public JsonResult ActualizarExtensiones(int? id)
        {

            var empleado = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpId == id).FirstOrDefault();

            return Json(empleado, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [Authorize(Roles = "AFA,AFWR")]
        public JsonResult ActualizarExtensiones(string codigo, string Extension)
        {

            var extensiones = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpEmpleadoId.ToString() == codigo).FirstOrDefault();

            if (extensiones != null)
            {
                extensiones.FlotExtEmpExtesion = Extension;
                db.SaveChanges();
            }
            return Json(new { success = true, Mensaje = "Datos Actualizados Correctamente" }, JsonRequestBehavior.AllowGet);


        }

        //Flotas Extensiones Tab

        [HttpGet]
        public ActionResult FlotasExtensiones()
        {

            return View();
        }

        //Extensiones Detalles
        [HttpGet]
        public ActionResult Extensiones()
        {
            var asignadoEmpleado = (from a in db.Vw_Mostrar_Personal_Permisos_PGR

                                    select new Models.PersonalPgrModel
                                    {
                                        EmpleadoId = a.Codigo,
                                        Descripcion = a.NombreCompleto,
                                        Cedula = a.Cedula,

                                    }).OrderBy(x => x.Descripcion).Where(x => x.EmpleadoId != "1199701");
            ViewBag.tecnico = new SelectList(asignadoEmpleado.ToList(), "EmpleadoId", "Descripcion");
            ViewBag.DependenciaID = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");
            ViewBag.DeptoID = new SelectList(db.vw_Departamentos.ToList(), "DeptoID", "Descripcion");
            ViewBag.DependenciaID1 = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");
            ViewBag.DeptoID1 = new SelectList(db.vw_Departamentos.ToList(), "DeptoID", "Descripcion");

            return View();
        }

        //Extensiones Detalles
        //[HttpGet]
        //public ActionResult Extensiones()
        //{
        //    var asignadoEmpleado = (from a in db.Vw_Mostrar_Personal_Permisos_PGR

        //                            select new Models.PersonalPgrModel
        //                            {
        //                                EmpleadoId = a.Codigo,
        //                                Descripcion = a.NombreCompleto,
        //                                Cedula = a.Cedula,

        //                            }).OrderBy(x => x.Descripcion).Where(x => x.EmpleadoId != "1199701");
        //    ViewBag.tecnico = new SelectList(asignadoEmpleado.ToList(), "EmpleadoId", "Descripcion");
        //    ViewBag.DependenciaID = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");
        //    ViewBag.DeptoID = new SelectList(db.vw_Departamentos.ToList(), "DeptoID", "Descripcion");
        //    ViewBag.DependenciaID1 = new SelectList(db.vw_Dependencias.ToList(), "DependenciaID", "Descripcion");
        //    ViewBag.DeptoID1 = new SelectList(db.vw_Departamentos.ToList(), "DeptoID", "Descripcion");

        //    return View();
        //}

        [HttpPost]
        public JsonResult Extensiones(string dependencia, string departamento)
        {

            var extensiones = db.FlotaExtensionEmpleado.ToList();
            var empleados = (from e in db.VISTA_EMPLEADOS
                             from ext in db.FlotaExtensionEmpleado
                             where e.empleadoid == ext.FlotExtEmpEmpleadoId.ToString()
                             where e.dependenciaid == dependencia && e.departamentoid == departamento

                             select new EmpleadoExtensionesVM
                             {
                                 FlotaId = ext.FlotExtEmpId,
                                 empleadoId = e.empleadoid,
                                 nombre = e.nombre.ToUpper(),
                                 dependencia = e.dependencia.ToUpper(),
                                 dependenciaid = e.dependenciaid,
                                 departamento = e.departamento.ToUpper(),
                                 departamentoid = e.departamentoid,
                                 extensiones = ext.FlotExtEmpExtesion,
                                 cargo = e.cargo
                             }).ToList();


            ViewBag.DependenciaID = new SelectList(db.vw_Dependencias.Where(x => x.DependenciaID.ToString() == dependencia).ToList(), "DependenciaID", "Descripcion");

            ViewBag.DeptoID = new SelectList(db.vw_Departamentos.Where(x => x.DeptoID.ToString() == departamento).ToList(), "DeptoID", "Descripcion");

            if (empleados == null)
            {
                return Json(new { success = false, Mensaje = "Ocurrio un error mientras hacia solicitud, Intente mas tarde" }, JsonRequestBehavior.AllowGet);

            }

            return Json(empleados);
        }



        // GET: AsignacionFlots
        [Authorize(Roles = "AFA,AFWR")]
        public ActionResult Index()
        {
            //int asignacion = 1;
            var asignacion = db.AsignacionFlots.Where(x => x.Estado == true)
                .GroupBy(x => x.AsignacionNoFlota).Select(x => x.FirstOrDefault()).ToList();

            return View(asignacion);
        }

        public ActionResult Consulta()
        {

            var asignacion = db.AsignacionFlots.Where(x => x.Estado == true)
               .GroupBy(x => x.AsignacionNoFlota).Select(x => x.FirstOrDefault()).ToList();

            return View(asignacion);
        }


        // GET: AsignacionFlots/Details/5
        [Authorize(Roles = "AFA,AFWR")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AsignacionFlots asignacionFlots = db.AsignacionFlots.Find(id);
            if (asignacionFlots == null)
            {
                return HttpNotFound();
            }
            return View(asignacionFlots);
        }

        // GET: AsignacionFlots/Create
        [Authorize(Roles = "AFA,AFWR")]
        public ActionResult Create()
        {
            ViewBag.Estatus = new SelectList(db.EstatusFlotas.OrderBy(x=> x.EstatusDescripcion).Where(x=> x.EstatusId != 6).ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Marcas = new SelectList(db.MarcasFlotas.ToList(), "MarcaId", "MarcaDescripcion");
            ViewBag.Modelos = new SelectList(db.ModeloFlotas.ToList(), "ModeloId", "ModeloDescripcion");

            return View();
        }

        // POST: AsignacionFlots/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [Authorize(Roles = "AFA,AFWR")]
        public JsonResult Create(FormCollection CD, AsignacionFlots asignacionFlots)
        {
            try
            {
                if (!string.IsNullOrEmpty(asignacionFlots.AsignacionCedula))
                {
                    asignacionFlots.AsignacionCargo = CD["cargoEmpleado"];
                    asignacionFlots.AsignacionDepartamentoId = CD["deptoEmpleado"];
                    //asignacionFlots.AsignacionDepto = CD["depto"];
                    asignacionFlots.AsignacionFechaCreacion = DateTime.Now;
                    asignacionFlots.AsignacionUsuario = Session["usuario"].ToString();
                    asignacionFlots.Estado = true;



                    db.AsignacionFlots.Add(asignacionFlots);
                    db.SaveChanges();
                    return Json(new { success = true, Mensaje = "Datos guardado correctamente" }, JsonRequestBehavior.AllowGet);

                }
                return Json(new { success = false, Mensaje = "Ha ocurrido un error, favor verificar" });


                //else
                //{
                //    var errs = ModelState.Values.SelectMany(v => v.Errors);

                //    ViewBag.Estatus = new SelectList(db.EstatusFlotas.Where(x => x.EstatusId != 6).ToList(), "EstatusId", "EstatusDescripcion");
                //    ViewBag.Marcas = new SelectList(db.MarcasFlotas.ToList(), "MarcaId", "MarcaDescripcion");
                //    ViewBag.Modelos = new SelectList(db.ModeloFlotas.ToList(), "ModeloId", "ModeloDescripcion");
                //}
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Json(new { success = false, Mensaje = "Ha ocurrido un error, favor verificar" });
            }

           
        }
        
        public JsonResult GetEmpleado(string cedula)
        {
            var empleado = db.VISTA_EMPLEADOS.Where(c => c.cedula == cedula).FirstOrDefault();
            return Json(empleado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Modelo(string marca)
        {
            List<SelectListItem> modelos;

            int cl = int.Parse(marca);
            modelos = db.ModeloFlotas.Where(x => x.MarcaId == cl).Select(x => new SelectListItem() { Text = x.ModeloDescripcion, Value = x.ModeloId.ToString() }).ToList();


            return Json(new SelectList(modelos, "Value", "Text"));
        }

        // GET: AsignacionFlots/Edit/5
        [Authorize(Roles = "AFA,AFWR")]
        public ActionResult Edit(int? id, string empleadoId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.Estatus = new SelectList(db.EstatusFlotas.Where(x => x.EstatusId != 6).ToList(), "EstatusId", "EstatusDescripcion");
            ViewBag.Marcas = new SelectList(db.MarcasFlotas.ToList(), "MarcaId", "MarcaDescripcion");
            ViewBag.Modelos = new SelectList(db.ModeloFlotas.ToList(), "ModeloId", "ModeloDescripcion");

            var empleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == empleadoId).FirstOrDefault();

            ViewBag.CedulaEmpleado = empleado.cedula;
            ViewBag.codigoEmpleado = empleado.empleadoid;
            ViewBag.nombre = empleado.nombre;
            ViewBag.cargoEmpleado = empleado.cargo;
            ViewBag.dptoEmpleado = empleado.departamentoid;
            ViewBag.depto = empleado.departamento;

            AsignacionFlots asignacionFlots = db.AsignacionFlots.Find(id);
            if (asignacionFlots == null)
            {
                return HttpNotFound();
            }
            return View(asignacionFlots);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "AFA,AFWR")]
        public ActionResult Edit(FormCollection fc, [Bind(Include = "AsignacionId,ModeloId,MarcaId,AsignacionImei,AsignacionSimNumeracion,AsignacionNoReferencia,AsignacionDespachado,AsignacionNoFlota,AsignacionUsuario,AsignacionDepartamentoId,AsignacionFechaCreacion,AsignacionCedula,AsignacionCargo,AsignacionEmpleadoId,EstatusId,AsignacionExt,AsignacionEmpleado,AsignacionDepto,AsignacionCosto")] AsignacionFlots asignacionFlots)
        {
            if (fc["cedulaEmpleado"] != "")
            {
                asignacionFlots.AsignacionCedula = fc["cedulaEmpleado"];
                asignacionFlots.AsignacionEmpleadoId = fc["codigoEmpleado"];
                asignacionFlots.AsignacionCargo = fc["cargoEmpleado"];
                asignacionFlots.AsignacionDepartamentoId = fc["deptoEmpleado"];
                asignacionFlots.AsignacionDepto = fc["depto"];
                asignacionFlots.AsignacionEmpleado = fc["nombre"];

                asignacionFlots.AsignacionFechaCreacion = asignacionFlots.AsignacionFechaCreacion;
                asignacionFlots.AsignacionUsuario = Session["usuario"].ToString();
            }

            if (ModelState.IsValid)
            {
                db.Entry(asignacionFlots).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                var errs = ModelState.Values.SelectMany(v => v.Errors);
                ViewBag.Estatus = new SelectList(db.EstatusFlotas.Where(x => x.EstatusId != 6).ToList(), "EstatusId", "EstatusDescripcion");
                ViewBag.Marcas = new SelectList(db.MarcasFlotas.ToList(), "MarcaId", "MarcaDescripcion");
                ViewBag.Modelos = new SelectList(db.ModeloFlotas.ToList(), "ModeloId", "ModeloDescripcion");
            }
            return View(asignacionFlots);
        }

        [Authorize(Roles = "AFA,AFWR")]
        public JsonResult EliminarNumeroFlotas(int? id)
        {
            try
            {
                AsignacionFlots asignacionFlots = db.AsignacionFlots.Find(id);
                asignacionFlots.Estado = false;
                db.SaveChanges();
                return Json(new { success = true, Mensaje = "Datos eliminados correctamente" }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                ex.ToString();

                return Json(new { success = false, Menasje = "Ha ocurrido un error al procesar su solicitud" }, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: AsignacionFlots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "AFA,AFWR")]
        public ActionResult DeleteConfirmed(int id)
        {
            int asign = 0;
            AsignacionFlots asignacionFlots = db.AsignacionFlots.Find(id);
            asignacionFlots.EstatusId = asign.ToString();
            //db.AsignacionFlots.Remove(asignacionFlots);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

}

