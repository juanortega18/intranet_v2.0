﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    public class SicController : Controller
    {
        // GET: SIC
        public ActionResult Index()
        {
            return View();
        }

        // GET: SIC/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: SIC/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SIC/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SIC/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: SIC/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SIC/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SIC/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult CreateUser(string name, string reason)
        {
            try
            {
                string code = HttpContext.Session["usuario"].ToString();
                PostSICResponse(name, reason,code);

                return Json("Success");
            }
            catch (Exception)
            {
                return Json("Error");
            }
            
        }

        public async static void PostSICResponse(string name, string reason,string code)
        {
            IEnumerable<KeyValuePair<string, string>> data = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("Username",name),
                new KeyValuePair<string, string>("Reason",reason),
                new KeyValuePair<string, string>("Code",code)
            };

            HttpContent q = new FormUrlEncodedContent(data);

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = await client.PostAsync("http://localhost:58349/api/GetUser", q))
                {
                    using (HttpContent content = response.Content)
                    {
                        string mycontent = await content.ReadAsStringAsync();
                        HttpContentHeaders headers = content.Headers;

                        if(string.IsNullOrEmpty(mycontent))
                        {
                            throw new Exception();
                        }
                    }
                }
            }
        }
    }
}
