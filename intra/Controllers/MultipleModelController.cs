﻿using intra.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    public class MultipleModelController : Controller
    {
        private dbIntranet db = new dbIntranet();
        // GET: MultipleModel
        [CustomAuthorize]
        public ActionResult Index()
        {
            var model = new MultipleModel();
            model.Avisos = db.Avisos.ToList();
            model.Noticias = db.Noticias.ToList();
            return View(model);
        }
    }
}