﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models;
using System.Data.Entity;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using Microsoft.ReportingServices;
using Microsoft.ReportingServices.Diagnostics.Utilities;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Rotativa;
using intra.Code;
using intra.Models.ViewModel;
using intra.Models.Servicios;




namespace intra.Controllers
{
    public class RegistroSolicitudController : Controller
    {
        dbIntranet db = new dbIntranet();
        public static SqlConnection Enlace = new SqlConnection(ConfigurationManager.ConnectionStrings["intra"].ConnectionString);

        //Variable WebConfig
        public static string Direccion = ConfigurationManager.AppSettings["URLINTRA"];

        public static string DireccionChat = ConfigurationManager.AppSettings["URLINTRACHAT"];

        private SolicitudModel smodel = new SolicitudModel();

        private dbIntranet obj = new dbIntranet();

        public Correo ObCorreo = new Correo();

        //[CustomAuthorize]
        //public ActionResult Asignado()
        //{
        //    if (Session["empleadoId"] != null && Session["nombre"] != null)
        //    {
        //        int Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());
        //        smodel.Usurio = HttpContext.Session["nombre"].ToString();
        //        //Cdg = 18735;
        //        string dependenciaId = Session["dependenciaId"].ToString();
        //        string departamentoId = Session["departamentoId"].ToString();


        //        var asignadoEmpleado = (from a in obj.Sol_Mostrar_Personal_PGR
        //                                where a.CodigoDependencia == dependenciaId && a.CodigoDepartamento == departamentoId
        //                                orderby a.NombreCompleto ascending
        //                                select new PersonalPgrModel
        //                                {
        //                                    EmpleadoId = a.Codigo,
        //                                    Descripcion = a.NombreCompleto,
        //                                    Cedula = a.Cedula,

        //                                }).ToList();
        //        ViewBag.tecnico = asignadoEmpleado;


        //        int empleado = int.Parse(Session["empleadoId"].ToString());

        //        var solicitud = obj.DetalleSolicitud.Where(x => x.CodigoTecnico == empleado).ToList();

        //    }
        //    return View();
        //}

        #region ObtenerEtiquetaHTML
        public string DescripcionCorreoPaseProduccionAprobado(string Departamento, string NombreCompleto, string Problema, string Link, string Descripcion)
        {


            //string NombreArchivo = Server.MapPath (@"~/EtiquetasHtml/Solicitud_nuevoServicio_PGR.html");
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/PaseProduccionAprobado.html");


            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", "PASE A PRODUCCION APROBADO");
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", Descripcion);



            return Archivo;
        }
        #endregion



        //DescripcionCorreoEncuesta
        #region ObtenerEtiquetaHTML
        public string DescripcionCorreoEncuesta(string Link)
        {

            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/FormularioEncuesta.html");


            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {

                Archivo = sr.ReadToEnd();

            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", "ENCUESTA SASTIFACIÓN DE SERVICIO");
            //Archivo = Archivo.Replace("$departamento", Departamento);
            //Archivo = Archivo.Replace("$nombres", NombreCompleto);
           // Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            return Archivo;
        }
        #endregion
     
        
        
        
        //obtener usuario del dominio
        [AllowAnonymous]
        public string obtenerCorreoUsuario(string codigoEmpleado)
        {
            var cedulaEmpleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == codigoEmpleado).FirstOrDefault();
            var user = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", cedulaEmpleado.cedula)).FirstOrDefault();
            string correo = $"{user.samAccountName}@pgr.gob.do";

            return correo;
        }
        [AllowAnonymous]
        //Enviar Correo Pase Produccion
        #region SendCorreo
        private void SendCorreoPaseProduccion(string Lider, string responsable,  string Sujeto, string Descripcion)
        {


            string codigoEmpleado = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoDTI" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoDTI = obtenerCorreoUsuario(codigoEmpleado);

            string codigoEmpleadoEncargadoDesarrollo = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoDesarrollo" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoDesarrollo = obtenerCorreoUsuario(codigoEmpleadoEncargadoDesarrollo);

            string codigoEmpleadoEncargadoProyecto = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionTipo == "EncargadoProyecto" && x.EncargadoPaseProduccionEstatus == true).FirstOrDefault().EmpleadoId;
            string correoEncargadoProyecto = obtenerCorreoUsuario(codigoEmpleadoEncargadoProyecto);

            var encargadoSubir = db.EncargadoSubirProduccion.Where(x => x.EncargadoSubirProduccionEstatusId == 1).FirstOrDefault();
            string encargadoSubirCorreo = obtenerCorreoUsuario(encargadoSubir.EmpleadoId.ToString());

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

           

          
            ObCorreo.CorreoPaseProduccion(Lider,correoEncargadoDesarrollo, correoEncargadoDTI, correoEncargadoProyecto, encargadoSubirCorreo, Sujeto, Descripcion, Logo, Flecha, LogoDTI);
         
        }
        #endregion


        [AllowAnonymous]
        //Codigo Pase a Produccion
        public ActionResult PaseProduccion(int? aprobacion)
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }

            var user = Session["usuario"].ToString();
            var empleadoId = (string)Session["empleadoId"];

            AprobacioneServicios aproS = db.AprobacioneServicios.Where(x => x.AprobacionesId == aprobacion).FirstOrDefault();
           
            var registro = db.Sol_Registro_Solicitud.Where(x => x.SolicitudId == aproS.SolicitudId).FirstOrDefault();
            var Lider = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == registro.SolicitanteId.ToString()).FirstOrDefault();
            ViewBag.Lider = Lider.nombre.ToUpper();
            // ViewBag.Usuario = Session["usuario"].ToString();
            ViewBag.EmpleadoId = empleadoId;
            ViewBag.Fecha = DateTime.Now;
            ViewBag.Estado = 3;
            string nombre = Session["nombre"].ToString();

            var encargadoPaseProduccionId = (from t in db.EncargadoPaseProduccion
                                             where t.EmpleadoId == empleadoId
                                             select new
                                             {
                                                 t.EncargadoPaseProduccionId,
                                                 t.EncargadoPaseProduccionNombre,
                                                 t.EncargadoPaseProduccionCorreo
                                             }).FirstOrDefault();

            if (encargadoPaseProduccionId == null)
                return RedirectToAction("index", "home");

  ViewBag.EstaFirmada = (from a in db.AprobacionesActividad
                               where (a.EncargadoDTI == encargadoPaseProduccionId.EncargadoPaseProduccionNombre ||
                               a.EncargadoProyecto == encargadoPaseProduccionId.EncargadoPaseProduccionNombre ||
                               a.EncargadoDesarrollo == encargadoPaseProduccionId.EncargadoPaseProduccionNombre) && a.AprobacionesId == aprobacion
                               select "").FirstOrDefault();

            aproS.areasImplicadas = db.AreasAprobacionesProduccion.Where(x => x.AprobacionesId == aprobacion).ToList();
            aproS.categorias = db.CategoriasAprobacionesProduccion.Where(x => x.AprobacionesId == aprobacion).ToList();
           
            var aprobaciones = (from t in db.AprobacionesActividad
                                where t.AprobacionesId == aproS.AprobacionesId
                 
               select t).FirstOrDefault();


            //var correo = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionCorreo == user).FirstOrDefault();
            //  if(correo.EncargadoPaseProduccionTipo == "Lider")
            //{
            //    TempData["Lider"] = "Lider";
            //}


            // var enc = db.EncargadoPaseProduccion.Where(x => x.EncargadoPaseProduccionCorreo == user).FirstOrDefault();

            if (aprobaciones == null)
            {
                ViewBag.Reporte = "Ok";
            }

            return View(aproS);
        }

        [AllowAnonymous]
        public ActionResult CancelarSolicitud(int? aprobar)
        {
            var cancelar = db.AprobacioneServicios.Where(x=> x.AprobacionesId == aprobar).FirstOrDefault();
            cancelar.ApEstatusId = 2;

            return Json(new { success = true, message = "Solicitud Cancelada Correctamente" }, JsonRequestBehavior.AllowGet);

        }

        //Pase a Produccion
        public ActionResult PaseProdu(AprobacionesActividad AprobacionesActividad)
        {
            var empleado = Session["empleadoId"].ToString();
            string user = Session["usuario"].ToString();
            var aprobar = db.AprobacioneServicios.Find(AprobacionesActividad.AprobacionesId);
            var estado = db.AprobacionEstatus.Where(x => x.ApEstatusId == 1).FirstOrDefault();
            var actividad = db.Sol_Actividades.Where(y => y.ActividadId == aprobar.ActividadId).FirstOrDefault();
            var encargado = db.EncargadoPaseProduccion.Where(x => x.EmpleadoId == AprobacionesActividad.EmpleadoId.ToString()).FirstOrDefault();
            var registro = db.Sol_Registro_Solicitud.Where(x => x.SolicitudId == aprobar.SolicitudId).FirstOrDefault();
            var Lider = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == registro.SolicitanteId.ToString()).FirstOrDefault();

            // var aplicacion = db.Aplicaciones.Where(x => x.EncargadoPaseProduccionId == encargado.EncargadoPaseProduccionId).FirstOrDefault();


            var aprobacion = db.AprobacionesActividad.Where(x => x.AprobacionesId == AprobacionesActividad.AprobacionesId).FirstOrDefault();

            if (ModelState.IsValid)
            {
               
                if(aprobacion == null)

                {
                    AprobacionesActividad.ApEstatusId = 3;
                    AprobacionesActividad.usuario = user;
                    AprobacionesActividad.LiderEquipo = Lider.nombre.ToUpper();
                    AprobacionesActividad.Solicitante = aprobar.Solicitante.ToUpper();
                    AprobacionesActividad.FechaCreacion = DateTime.Now;
                    db.AprobacionesActividad.Add(AprobacionesActividad);
                    

                }
                else
                {
                    AprobacionesActividad.ApEstatusId = aprobacion.ApEstatusId;
                    AprobacionesActividad.usuario = AprobacionesActividad.usuario;
                    AprobacionesActividad.Solicitante = aprobar.Solicitante.ToUpper();
                    AprobacionesActividad.LiderEquipo = AprobacionesActividad.LiderEquipo;
                    AprobacionesActividad.FechaCreacion = AprobacionesActividad.FechaCreacion;

                }
                db.SaveChanges();


            }

           var ap = db.AprobacionesActividad.Where(x => x.AprobacionesId == AprobacionesActividad.AprobacionesId).FirstOrDefault();
            

            if (encargado.EncargadoPaseProduccionId == 3)
            {
                var EncargadoDTI = db.AprobacionesActividad.Where(x => x.AprobacionesId == ap.AprobacionesId).FirstOrDefault();
             


                if (EncargadoDTI.EncargadoDTI == "" || EncargadoDTI.EncargadoDTI == null)
                { 
                EncargadoDTI.EncargadoDTI = encargado.EncargadoPaseProduccionNombre;
                EncargadoDTI.FechaAprobacionDTI = DateTime.Now;
                EncargadoDTI.ApEstatusId = 3;
                 
              
                }

            }
         
            if (encargado.EncargadoPaseProduccionId == 4)
            {
                var EncargadoDesarrollo = db.AprobacionesActividad.Where(x => x.AprobacionesId == ap.AprobacionesId).FirstOrDefault();

                if (EncargadoDesarrollo == null)
                {
                    return Json(new { success = false, message = "Usted no tiene permisos para aprobar solicitudes" }, JsonRequestBehavior.AllowGet);
                }
                if (EncargadoDesarrollo.EncargadoDesarrollo == "" || EncargadoDesarrollo.EncargadoDesarrollo == null)
                {
                    EncargadoDesarrollo.EncargadoDesarrollo = encargado.EncargadoPaseProduccionNombre;
                    EncargadoDesarrollo.FechaAprobacionDesarrollo = DateTime.Now;
                    EncargadoDesarrollo.ApEstatusId = 3;
                        
                    

                }

            }

            if  (encargado.EncargadoPaseProduccionId == 11)
            {
                var EncargadoProyecto = db.AprobacionesActividad.Where(x => x.AprobacionesId == ap.AprobacionesId).FirstOrDefault();
                if (EncargadoProyecto == null)
                {
                    return Json(new { success = false, message = "Usted no tiene permisos para aprobar solicitudes" }, JsonRequestBehavior.AllowGet);
                }

                if (EncargadoProyecto.EncargadoProyecto == null || EncargadoProyecto.EncargadoProyecto == "")
                {
                    EncargadoProyecto.EncargadoProyecto = encargado.EncargadoPaseProduccionNombre;
                    EncargadoProyecto.FechaAprobacionProyecto = DateTime.Now;
                    EncargadoProyecto.ApEstatusId = 3;
                      

                }

            }
            
            db.SaveChanges();

            var aprobado = db.AprobacionesActividad.Where(x => x.AprobacionesId == AprobacionesActividad.AprobacionesId).FirstOrDefault();


            if (aprobado.EncargadoDTI != "" && aprobado.EncargadoDTI != null && aprobado.EncargadoDesarrollo != "" && aprobado.EncargadoDesarrollo != null &&
                       aprobado.EncargadoProyecto != "" && aprobado.EncargadoProyecto != null)
            {

                var encargadoSubir = db.EncargadoSubirProduccion.Where(x => x.EncargadoSubirProduccionEstatusId == 1).FirstOrDefault();

                var ActividadServicio = db.AprobacioneServicios.Find(AprobacionesActividad.AprobacionesId);
                ActividadServicio.ApEstatusId = 1;
                db.SaveChanges();

                var solicitudId = db.Sol_Registro_Solicitud.Where(x => x.SolicitudId == ActividadServicio.SolicitudId).FirstOrDefault();
                solicitudId.DomainUserTecnico = encargadoSubir.EncargadoSubirProduccionUsuario;

                solicitudId.TecnicoId = encargadoSubir.EmpleadoId;
                solicitudId.FhCreacion = DateTime.Now;
                solicitudId.FhModificacion = DateTime.Now;
                solicitudId.EstadoId = 1;
                db.SaveChanges();


                int actividadid = ActividadServicio.Sol_Actividades.ActividadId;
                string Problema = ActividadServicio.Sol_Actividades.Descripcion;
                string descripcion = ActividadServicio.DetallePaseProduccion;
                string NombreCompletoCo = aprobacion.Solicitante;
                var Departamento = db.VISTA_EMPLEADOS.Where(x => x.nombre == NombreCompletoCo).FirstOrDefault();
                string DepartamentoCo = Departamento.departamento;
               string dominio = "@pgr.gob.do";
                string lider = ActividadServicio.usuario + dominio;



                //CAMBIO aSIGNACION 
                //           var Supervisor = obj.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == solicitudId.DependenciaRegionalId && cd.DependenciaEstado == true).FirstOrDefault();

                var Supervisor = obj.Sol_Actividades.Where(x => x.ActividadId == actividadid).FirstOrDefault();
                var SupervisorDependencia = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == Supervisor.SupervisorId.ToString()).FirstOrDefault();

                string correo_supervisor = "";

                if (Supervisor != null)
                {
                    var Supervisor_DatosPersonales = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == Supervisor.SupervisorId.ToString()).FirstOrDefault();
                    correo_supervisor = correoUsuario(Supervisor_DatosPersonales.Cedula);
                }

                string LINK = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/RegistroSolicitud?id={solicitudId.SolicitudId}" +"#Ancla";

                EnviarCorreo(solicitudId.DomainUserSolicitante, correo_supervisor, solicitudId.DomainUserTecnico, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + SupervisorDependencia.dependencia.ToUpper(), DescripcionCorreo("REASIGNACIÓN DE SOLICITUD", DepartamentoCo, NombreCompletoCo, descripcion, LINK, Problema));
               
                //ENVIO CORREO A TODOS
                string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/RegistroSolicitud/ReportePaseProduccion?aprobado={aprobacion.AprobacionesId}";
                SendCorreoPaseProduccion(lider, solicitudId.DomainUserTecnico,   "PASE A PRODUCCION APROBADO", DescripcionCorreoPaseProduccionAprobado(DepartamentoCo, NombreCompletoCo, descripcion, url, Problema ));

            }
            else
            {
                var servicios = db.AprobacioneServicios.Find(AprobacionesActividad.AprobacionesId);
                servicios.ApEstatusId = 3;

            }


            return Json(new { success = true, message = "Solicitud Aprobada Correctamente" }, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        public ActionResult SolicitudesPaseProduccion()
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }
            string empleadoId = Session["empleadoId"].ToString();
            var user = Session["usuario"].ToString();
            ViewBag.empleadoId = (string)Session["empleadoId"];
            var Aprobaciones = db.AprobacioneServicios.OrderByDescending(x=> x.AprobacionesId).ToList();

            foreach (var item in Aprobaciones)
            {
                
                var actividad = db.AprobacionesActividad.Where(x => x.AprobacionesId == item.AprobacionesId).FirstOrDefault();

                var encargado = db.EncargadoPaseProduccion.Where(x => x.EmpleadoId == empleadoId).FirstOrDefault();

                if (encargado.EncargadoPaseProduccionTipo == "Lider" )
                {
                    ViewBag.Aprobar = "Aprobar";
                    item.EstaFirmada = false;
                    return View(Aprobaciones);
                }

                item.EstaFirmada = (from a in db.AprobacionesActividad
                                   where (a.EncargadoDTI == encargado.EncargadoPaseProduccionNombre ||
                                   a.EncargadoProyecto == encargado.EncargadoPaseProduccionNombre ||
                                   a.EncargadoDesarrollo == encargado.EncargadoPaseProduccionNombre) && a.AprobacionesId == item.AprobacionesId
                                   select "").Any();

            }

            return View(Aprobaciones);

        }

        //Reporte Pase a Produccion
        [AllowAnonymous]
        public ActionResult ReportePaseProduccion(int? aprobado)
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }

            var user = db.AprobacionesActividad.Where(x => x.AprobacionesId == aprobado).FirstOrDefault();

            if (user == null)
            {

                TempData["Reporte"] = "Ok";

                return RedirectToAction("SolicitudesPaseProduccion");
            }

            var reporte = (from a in db.AprobacionesActividad.AsEnumerable()
                           join b in db.AprobacioneServicios on a.AprobacionesId equals b.AprobacionesId
                           join sa in db.Sol_Actividades on b.ActividadId equals sa.ActividadId
                           join ssa in db.Sol_Sub_Actividades on b.SubActividadId equals ssa.SubActividadId
                           join ae in db.AprobacionEstatus on b.ApEstatusId equals ae.ApEstatusId
                           join ca in db.CategoriasAprobacionesProduccion on b.AprobacionesId equals ca.AprobacionesId
                           join areas in db.AreasAprobacionesProduccion on b.AprobacionesId equals areas.AprobacionesId
                           where b.AprobacionesId == user.AprobacionesId
                           select new PaseaProduccion
                           {
                               Solicitante = b.Solicitante,
                               FechaAprobacion = b.FechaCreacion,
                               FechaSolicitud = a.FechaCreacion,
                               Asunto = sa.Descripcion,
                               Proyecto = ssa.Descripcion,
                               Prioridad = b.Prioridad,
                               EncargadoDesarrollo = a.EncargadoDesarrollo == "null" ? "" : a.EncargadoDesarrollo,
                               EncargadoDTI = a.EncargadoDTI == "null" ? "" : a.EncargadoDTI,
                               EncargadoProyecto = a.EncargadoProyecto == null ? "" : a.EncargadoProyecto,
                               LiderEquipo = a.LiderEquipo == "null" ? "" : a.LiderEquipo,
                               FechaAprobacionDesarrollo =  a.FechaAprobacionDesarrollo == null ? "" : a.FechaAprobacionDesarrollo.ToString(),
                               FechaAprobacionDTI =  a.FechaAprobacionDTI == null ? "" : a.FechaAprobacionDTI.ToString(),
                               //FechaAprobacionLider = a.FechaAprobacionLider == null ? "" : a.FechaAprobacionLider.ToString(),
                               FechaAprobacionProyecto =  a.FechaAprobacionProyecto == null ? "" : a.FechaAprobacionProyecto.ToString(),
                               FechaAprobacionSolicitante = a.FechaAprobacionSolicitante == null ? "" : a.FechaAprobacionSolicitante.ToString(),
                               Razon = b.DetallePaseProduccion == "null" ? "" : b.DetallePaseProduccion,
                               EstadoAprobacion = b.AprobacionEstatus.DescripcionEstatus,
                               FaseProyecto = b.FaseProyecto,
                               Impacto = b.Impacto,
                               Categoria = ca.CategoriasProduccion.CategoriasProduccionDescripcion,
                               Riesgo = b.Riesgo,
                               SolicitudId = b.SolicitudId
                           }).FirstOrDefault();

            reporte.areasImplicadas = db.AreasAprobacionesProduccion.Where(x => x.AprobacionesId == aprobado).ToList();
            reporte.categoria = db.CategoriasAprobacionesProduccion.Where(x => x.AprobacionesId == aprobado).ToList();
           

            return new ViewAsPdf("ReportePaseProduccion", reporte) {
                PageSize = Rotativa.Options.Size.Letter
            };
 
    
            
        }

        public ActionResult ReportePaseProduccions(int? aprobado)
        {

            var user = db.AprobacionesActividad.Where(x => x.AprobacionesId == aprobado).FirstOrDefault();


            var reporte = (from a in db.AprobacionesActividad.AsEnumerable()
                           join b in db.AprobacioneServicios on a.AprobacionesId equals b.AprobacionesId
                           join sa in db.Sol_Actividades on b.ActividadId equals sa.ActividadId
                           join ssa in db.Sol_Sub_Actividades on b.SubActividadId equals ssa.SubActividadId
                           join ae in db.AprobacionEstatus on b.ApEstatusId equals ae.ApEstatusId
                           join ca in db.CategoriasAprobacionesProduccion on b.AprobacionesId equals ca.AprobacionesId
                           join areas in db.AreasAprobacionesProduccion on b.AprobacionesId equals areas.AprobacionesId
                           where b.AprobacionesId == aprobado
                           select new PaseaProduccion
                           {
                               Solicitante = b.Solicitante,
                               FechaAprobacion = b.FechaCreacion,
                               FechaSolicitud = a.FechaCreacion,
                               Asunto = sa.Descripcion,
                               Proyecto = ssa.Descripcion,
                               Prioridad = b.Prioridad,
                               EncargadoDesarrollo = a.EncargadoDesarrollo == null ? "" : a.EncargadoDesarrollo,
                               EncargadoDTI = a.EncargadoDTI == null ? "" : a.EncargadoDTI,
                               EncargadoProyecto = a.EncargadoProyecto == null ? "" : a.EncargadoProyecto,
                               LiderEquipo = a.LiderEquipo == null ? "" : a.LiderEquipo,
                               FechaAprobacionDesarrollo = a.FechaAprobacionDesarrollo == null ? "" : a.FechaAprobacionDesarrollo.ToString(),
                               FechaAprobacionDTI = a.FechaAprobacionDTI == null ? "" : a.FechaAprobacionDTI.ToString(),
                               //FechaAprobacionLider = a.FechaAprobacionLider == null ? "" : a.FechaAprobacionLider.ToString(),
                               FechaAprobacionProyecto = a.FechaAprobacionProyecto == null ? "" : a.FechaAprobacionProyecto.ToString(),
                               FechaAprobacionSolicitante = a.FechaAprobacionSolicitante == null ? "" : a.FechaAprobacionSolicitante.ToString(),
                               Razon = b.DetallePaseProduccion == null ? "" : b.DetallePaseProduccion,
                               EstadoAprobacion = b.AprobacionEstatus.DescripcionEstatus,
                               FaseProyecto = b.FaseProyecto,
                               Impacto = b.Impacto,
                               Riesgo = b.Riesgo,
                               SolicitudId = b.SolicitudId
                           }).FirstOrDefault();

            reporte.areasImplicadas = db.AreasAprobacionesProduccion.Where(x => x.AprobacionesId == aprobado).ToList();
            reporte.categoria = db.CategoriasAprobacionesProduccion.Where(x => x.AprobacionesId == aprobado).ToList();
            if (reporte == null)
            {
                ViewBag.Reporte = "Ok";
            }

            return View("ReportePaseProduccion", reporte);
        }

        #region correoUsuario
        private string correoUsuario(string cedula)
        {
            string Resultado = "";

            if (Enlace.State == ConnectionState.Closed)
            {
                Enlace.Open();
            }

            SqlCommand cmd = new SqlCommand("sp_users_ldapID", Enlace);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@id", cedula);
            SqlDataAdapter Sda = new SqlDataAdapter(cmd);
            DataTable Dta = new DataTable();
            Sda.Fill(Dta);

            String Datosusuario = Dta.Rows[0]["samAccountName"].ToString();

            Resultado = Datosusuario;

            return Resultado;
        }
        #endregion

        #region EnviarCorreo
        public void EnviarCorreo(string Supervisor, string Responsable, string Sujeto, string Descripcion)
        {
            string PrimerCorreo = "";

            string SegundoCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            if (Supervisor != "")
            {
                PrimerCorreo = Supervisor + "@PGR.GOB.DO";
            }
            else
            {
                PrimerCorreo = "";
            }
            
            SegundoCorreo = Responsable + "@PGR.GOB.DO";

            //ObjCorreo.EnviarCorreo(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion);

            ObCorreo.EnviarCorreo_(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion, Logo, Flecha, LogoDTI);
        }
        #endregion


        [AllowAnonymous]

        #region EncuestaCorreo
        public void EncuestaCorreo(string Solicitante, string Sujeto, string Descripcion)
        {
           // string PrimerCorreo = "";

            string SegundoCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_completar_encuesta.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");


            SegundoCorreo = Solicitante + "@PGR.GOB.DO";

            //ObjCorreo.EnviarCorreo(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion);

            ObCorreo.EncuestasCorreo(SegundoCorreo.Trim(), Sujeto, Descripcion, Logo, Flecha, LogoDTI);
        }
        #endregion


        private void EnviarCorreo(string Solicitante, string Supervisor, string Responsable, string Sujeto, string Descripcion)
        {
            string PrimerCorreo = "";

            string SegundoCorreo = "";

            string TercerCorreo = "";

            string Logo = "";

            string LogoDTI = "";

            string Flecha = "";

            Logo = Server.MapPath(@"~/EtiquetasHtml/header.jpg");

            LogoDTI = Server.MapPath(@"~/EtiquetasHtml/boton_servicio_asignado.png");

            Flecha = Server.MapPath(@"~/EtiquetasHtml/footer.jpg");

            if (Solicitante != "")
            {
                PrimerCorreo = Solicitante + "@PGR.GOB.DO";
            }
            
            if(Supervisor != "")
            {
                SegundoCorreo = Supervisor + "@PGR.GOB.DO";
            }

            if (Responsable != "")
            {
                TercerCorreo = Responsable + "@PGR.GOB.DO";
            }
            

            //ObjCorreo.EnviarCorreo(PrimerCorreo.Trim(), SegundoCorreo.Trim(), Sujeto, Descripcion);

            ObCorreo.EnviarCorreo_(PrimerCorreo.Trim(), SegundoCorreo.Trim(), TercerCorreo.Trim(), Sujeto, Descripcion, Logo, Flecha, LogoDTI);
        }
        

        #region Mostrar_Informacion_Tipo_Asistencia
        private void Mostrar_Informacion_Tipo_Asistencia()
        {
            smodel.TB_Tipo_Asistencia = obj.Sol_TipodeAsistencia.Where(cd => cd.Estado == true).ToList();
        }
        #endregion

        #region Mostrar_Informacion_Departamentos
        private void Mostrar_Informacion_Personal_PGR_Por_Deptos(string CodigoDependencia,string CodigoDeptos)
        {
            try
            {
                if(CodigoDependencia == "10001")
                {
                    var Detalles = (from A in obj.Sol_Mostrar_Personal_PGR
                                    orderby A.NombreCompleto ascending
                                    where A.CodigoDependencia == CodigoDependencia && A.CodigoDepartamento == CodigoDeptos
                                    select new PersonalPgrModel
                                    {
                                        EmpleadoId = A.Codigo,
                                        Descripcion = A.NombreCompleto,
                                        Cedula = A.Cedula,
                                    }).ToList();

                    smodel.EmpleadoPGR = Detalles;

                }
                else
                {
                    var empId = HttpContext.Session["empleadoId"].ToString();

                    var dependencia_empleado = "";

                    var empleado = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == empId).FirstOrDefault();

                    if (empleado == null)
                    {
                        var empleado2 = obj.VISTA_EMPLEADOS.Where(x => x.empleadoid == empId).FirstOrDefault();
                        dependencia_empleado = empleado2.dependenciaid;
                    }
                    else
                    {
                        dependencia_empleado = empleado.CodigoDependencia;
                    }

                    var Detalles = (from A in obj.Sol_Mostrar_Personal_PGR
                                    orderby A.NombreCompleto ascending
                                    where A.CodigoDependencia == dependencia_empleado
                                    select new PersonalPgrModel
                                    {
                                        EmpleadoId = A.Codigo,
                                        Descripcion = A.NombreCompleto,
                                        Cedula = A.Cedula,
                                    }).ToList();
                    
                    smodel.EmpleadoPGR = Detalles;
                }                
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }
        #endregion

        #region Mostrar_Listado_Informacion_Departamentos
        private void Mostrar_Informacion_Listado_Personal_PGR_Por_Deptos()
        {
            try
            {
                var Detalles = (from A in obj.Sol_Mostrar_Personal_PGR
                                orderby A.NombreCompleto ascending
                                select new ListadoPersonalPgrModel
                                {
                                    EmpleadoId = A.Codigo,
                                    Descripcion = A.NombreCompleto,
                                    Cedula = A.Cedula,
                                }).ToList();

                smodel.Listado_EmpleadoPGR = Detalles;
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        
        #endregion

        #region MostrarEstadoConsulta
        private void MostrarEstadoConsulta()
        {
            smodel.EstadoConsulta = obj.Sol_Estado_Solicitud.Select(cd => new EstadoConsultaModel
            {
                EstadoId = cd.EstadoId,
                Descripcion = cd.Descripcion
            }).ToList();

        }
        #endregion

        #region MostrarDependenciasRegionales
        public void Mostrar_Dependencias_Regionales()
        {
            smodel.ListadoDependenciasRegionales = obj.Sol_DependenciasRegionales.Select(a => new DependenciaRegionalModel { DependenciaId = a.DependenciaId, DependenciaNombre = a.DependenciaNombre }).ToList();
        }
        #endregion

        #region MostrarOtrosTecnicos
        private void MostrarOtrosTecnicos(int CodigoSolicitud)
        {
            var Lista = obj.Sol_Mostrar_Otros_Tecnicos.Where(cd => cd.SolicitudId == CodigoSolicitud && cd.Estado == true).ToList();
            
            smodel.TB_Mostrar_Otros_Tecnicos = Lista;
        }
        #endregion

        #region Validar_Usuarios
        private bool Usuario(int Solicitud)
        {
            bool Resultado;

            var Detalle = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitud == Solicitud).FirstOrDefault();

            if (Detalle.CodigoTecnico > 0)
            {
                Resultado = true;
            }
            else
            {
                Resultado = false;
            }
            return Resultado;
        }
        #endregion

        #region Old code
        //Mostrar_Registro_Solicitud
        public void Mostrar_Registro_Solicitud(int CodigoSolicitud)
        {
            Mostrar_Informacion_Tipo_Asistencia();

            var DetalleSolicitud = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitud == CodigoSolicitud).FirstOrDefault();

            smodel.DECodigoSolicitud = DetalleSolicitud.CodigoSolicitud;
            smodel.DECodigoSolicitante = DetalleSolicitud.CodigoSolicitante;
            smodel.DENombreSolicitante = DetalleSolicitud.NombreSolicitante;
            smodel.DEDepartamentoSolicitante = DetalleSolicitud.DepartamentoSolicitante;
            smodel.DEUsuarioDominioSolic = DetalleSolicitud.UsuarioDominioSolic;
            smodel.DECodigoTecnico = DetalleSolicitud.CodigoTecnico;
            smodel.DENombreTecnico = DetalleSolicitud.NombreTecnico;
            smodel.DEUsuarioDominioTecn = DetalleSolicitud.UsuarioDominioTecn;
            smodel.DECodigoDepartamento = DetalleSolicitud.CodigoDepartamento;
            smodel.DEDepartamento = DetalleSolicitud.Departamento;
            smodel.DECodigoTipoSolicitud = DetalleSolicitud.CodigoTipoSolicitud;
            smodel.DETipoSolicitud = DetalleSolicitud.TipoSolicitud;
            smodel.DEDescripcionSolicitudes = DetalleSolicitud.DescripcionSolicitud;
            smodel.DECodigoSubTipoSolicitud = DetalleSolicitud.CodigoSubTipoSolicitud;
            smodel.DESubTipoSolicitud = DetalleSolicitud.SubTipoSolicitud;
            smodel.DEDescripcionSolucion = DetalleSolicitud.DescripcionSolucion;
            smodel.DECodigoEstadoSolicitud = DetalleSolicitud.CodigoEstadoSolicitud;
            smodel.DEEstadoSolicitud = DetalleSolicitud.EstadoSolicitud;
            smodel.DEEstado = DetalleSolicitud.Estado;
            smodel.DEHoras = DetalleSolicitud.Horas;
            smodel.DETipodeAsistencia = DetalleSolicitud.TipodeAsistencia;
            smodel.DEFechaInicioSolicitud = DetalleSolicitud.FechaInicioSolicitud;
            smodel.DEFechaFinalSolicitud = DetalleSolicitud.FechaFinalSolicitud;
            smodel.DEFechaModificacion = DetalleSolicitud.FechaModificacion;
            smodel.DEFechaCreacion = DetalleSolicitud.FechaCreacion;
            smodel.ReAsignarHora = DetalleSolicitud.Horas;
            smodel.CodigoDependencia = DetalleSolicitud.DependenciaId;
            smodel.DependenciaNombre = DetalleSolicitud.DependenciaNombre;
            smodel.DepartamentoSolicitante = DetalleSolicitud.DepartamentoSolicitante;

            var Codg = HttpContext.Session["empleadoId"].ToString();
            //var Codg = "12156";

            var is_alternativo = obj.Sol_Tecnicos_Alternativos.Where(x => x.TecnicoId.ToString() == Codg).ToList();

            if(is_alternativo != null && is_alternativo.Count > 0 )
            {
                smodel.EmpleadoPGR = new List<PersonalPgrModel>();
                bool is_in_sede = false;
                bool is_dependeny = false;

                foreach (var item in is_alternativo)
                {
                    if (item.DependenciaRegionalId == null && item.ActividadId > 0 && item.SupervisorId > 0)
                    {
                        is_in_sede = true;
                    } 
                    else
                    {
                        is_dependeny = true;
                    }
                }

                if(is_in_sede)
                {
                    var Detalles = (from A in obj.Sol_Mostrar_Personal_PGR
                                    orderby A.NombreCompleto ascending
                                    where A.CodigoDependencia == "10001" && A.CodigoDepartamento == smodel.DECodigoDepartamento.ToString()
                                    select new PersonalPgrModel
                                    {
                                        EmpleadoId = A.Codigo,
                                        Descripcion = A.NombreCompleto,
                                        Cedula = A.Cedula,
                                    }).ToList();

                    foreach (var item2 in Detalles)
                    {
                        smodel.EmpleadoPGR.Add(item2);
                    }
                }

                if(is_dependeny)
                {
                    var personalDependencia = (from TablaTecnicoDependencia in obj.Sol_Tecnicos_Dependencias
                                               select new PersonalPgrModel
                                               {
                                                   EmpleadoId = TablaTecnicoDependencia.TecnicoDependenciaEmpleadoId.ToString(),
                                                   Descripcion = TablaTecnicoDependencia.TecnicoDependenciaNombre,
                                                   Cedula = TablaTecnicoDependencia.TecnicoDependenciaCedula
                                               }
                       ).ToList();

                    foreach (var item2 in personalDependencia)
                    {
                        smodel.EmpleadoPGR.Add(item2);
                    }
                }
            }
            else
            {
                var is_director = db.Database.SqlQuery<sp_verif_Encargados_Result>("exec sp_verif_Encargados {0}", Convert.ToInt32(Codg)).FirstOrDefault();

                if(is_director.Nombre != "N/A" && is_director.Procedencia != "N/A")
                {
                    var tecnico_soliitud = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == smodel.DECodigoTecnico.ToString()).FirstOrDefault();

                    var encargado_departamento = db.Nmdeptos_Permisos.Where(x => x.DependenciaID == 10001 && x.DeptoID.ToString() == tecnico_soliitud.CodigoDepartamento).FirstOrDefault();

                    if(encargado_departamento.EncargadoID == Convert.ToInt32(Codg))
                        smodel.CodigoDirector = Convert.ToInt32(Codg);
                }
                
                if (smodel.CodigoDependencia == 1)
                {
                    MostrarActividades(DetalleSolicitud.CodigoTipoSolicitud);
                }
                else
                {
                    MostrarDependenciasRegionales(DetalleSolicitud.DependenciaId);
                }

                if (smodel.CodigoDependencia == 1)
                {
                    Mostrar_Informacion_Personal_PGR_Por_Deptos("10001", DetalleSolicitud.CodigoDepartamento.ToString());
                }
                else
                {
                    Mostrar_Informacion_Personal_PGR_Dependencias(smodel.CodigoDependencia);
                }
            }
            
            MostrarEstadoConsulta();
            MostrarOtrosTecnicos(CodigoSolicitud);
            MostrarInformacionChat(CodigoSolicitud);

            //Mostrar_Informacion_Actividades(DetalleSolicitud.CodigoDepartamento, DetalleSolicitud.CodigoTipoSolicitud);
            if(smodel.CodigoDependencia == 1)
            {
                Mostrar_Informacion_Actividades();

                Mostrar_Informacion_Sub_Actividades(DetalleSolicitud.CodigoDepartamento, DetalleSolicitud.CodigoTipoSolicitud);
            }
            
            Mostrar_Informacion_Listado_Personal_PGR_Por_Deptos();

            Mostrar_Dependencias_Regionales();

            Session["CodigoSolicitud"] = CodigoSolicitud;

            if (HttpContext.Session["empleadoId"] != null)
            {
                int Cdg = 0;

                Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

                smodel.CodigoEmpleado = Cdg;
            }

            if (smodel.DECodigoTecnico > 0)
            {
                Session["CodigoTecnico"] = smodel.DECodigoTecnico.ToString();
            }
            else
            {
                Session["CodigoTecnico"] = null;
            }

        }
        #endregion

        #region MyRegion

        public void Mostrar_Informacion_Personal_PGR_Dependencias(int dependenciaId)
        {
            var personalDependencia = (
                from TablaTecnicoDependencia in obj.Sol_Tecnicos_Dependencias
                where TablaTecnicoDependencia.DependenciaId == dependenciaId
                select new PersonalPgrModel
                {
                    EmpleadoId = TablaTecnicoDependencia.TecnicoDependenciaEmpleadoId.ToString(),
                    Descripcion = TablaTecnicoDependencia.TecnicoDependenciaNombre,
                    Cedula = TablaTecnicoDependencia.TecnicoDependenciaCedula
                }
            ).ToList();

            smodel.EmpleadoPGR = personalDependencia;
        }

        #endregion

        #region MostrarReporteSolicitud
        private void MostrarReporteSolicitud(int CodigoSolicitud)
        {
            LocalReport rl = new LocalReport();

            if (Enlace.State == ConnectionState.Closed)
            {
                Enlace.Open();
            }

            SqlCommand cmd = new SqlCommand("Sp_Mostrar_Detalles_Solicitud_Reporte", Enlace);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@CodigoSolicitud", CodigoSolicitud);
            SqlDataAdapter Sda1 = new SqlDataAdapter(cmd);
            DataTable Dta1 = new DataTable();
            Sda1.Fill(Dta1);

            if ((Dta1 != null) && (Dta1.Rows.Count > 0))
            {
                //Binding Connexion
                Microsoft.Reporting.WebForms.ReportDataSource rds1 = new Microsoft.Reporting.WebForms.ReportDataSource("PGRINTRANET", Dta1);
                rl.DataSources.Add(rds1);
            }
            else
            {
                ViewBag.MSJ = "NO EXISTEN REGISTRO.";
            }
            if (Enlace.State == ConnectionState.Closed)
            {
                Enlace.Open();
            }

            SqlCommand cmd1 = new SqlCommand("Sp_Mostrar_Detalles_Chat_Solicitud_Reporte", Enlace);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.Clear();
            cmd1.Parameters.AddWithValue("@CodigoSolicitud", CodigoSolicitud);
            SqlDataAdapter Sda2 = new SqlDataAdapter(cmd1);
            DataTable Dta2 = new DataTable();
            Sda2.Fill(Dta2);

            //Enlace Connexion
            Microsoft.Reporting.WebForms.ReportDataSource rds2 = new Microsoft.Reporting.WebForms.ReportDataSource("Detalles", Dta2);
            rl.DataSources.Add(rds2);

            rl.Refresh();

            //No Usar 
            //rl.ReportPath = HttpContext.Current.Server.MapPath("~/Reportes/Reporte Solicitud.rdl");

            rl.ReportPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Reportes/ReporteSolicitud.rdl");

            rl.EnableExternalImages = true;

            //Convert .pdf

            Microsoft.Reporting.WebForms.Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;
            byte[] bytes;

            bytes = rl.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

            //other
            HttpContext.Response.Buffer = false;
            HttpContext.Response.BufferOutput = false;
            HttpContext.Response.Clear();
            HttpContext.Response.ClearContent();
            HttpContext.Response.ClearHeaders();
            Response.AppendHeader("content-disposition", "inline; filename=Solicitud.pdf");
            //Response.AppendHeader("content-disposition", "attachment; filename=ReporteCarros.pdf");
            Response.ContentType = "application/pdf";

            using (MemoryStream memoryStream = new MemoryStream(bytes))
            {
                memoryStream.Seek(0, SeekOrigin.Begin);
                var streamBytes = memoryStream.ToArray();
                Response.BinaryWrite(streamBytes);

            }

            Response.End();
        }
        #endregion

        #region MostrarIp
        public static string ObltenerDirecionIp()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No Contiene Direccion Ip!");
        }
        #endregion

        #region Insertar_en_Logs
        public void InsertarLog(int CodigoSolicitud,string Descripcion)
        {
            string IP4Address = String.Empty;

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily == AddressFamily.InterNetwork)
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            try
            {
                if (HttpContext.Session["empleadoId"]!=null)
                { 
                Sol_Logs Tb = new Sol_Logs();
                Tb.SistemaId = 1;
                Tb.SolicitudId = CodigoSolicitud; 
                Tb.logIP = IP4Address;
                Tb.logUsuario = HttpContext.Session["empleadoId"].ToString();
                Tb.logFecha = DateTime.Now;
                Tb.logAccion = Descripcion;
                obj.Sol_Logs.Add(Tb);
                obj.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            
        }
        #endregion

        #region RetornarFuncionEmpleados
        private string Padron(string Codigo)
        {
            var Resultador = obj.Sol_Mostrar_Personal_PGR.Where(cd => cd.Codigo == Codigo).FirstOrDefault();

            return Resultador.NombreCompleto;
        }
        #endregion

        #region MostrarInformacionChat
        private void MostrarInformacionChat(int CodigoSolicitud)
        {
            smodel.Tb_Chat = obj.Sol_Chat.Where(cd => cd.SolicitudId == CodigoSolicitud && cd.Estado == true).ToList();
        }   
        #endregion

        #region Chat
        private bool Chat(int CodigoSolicitud)
        {
            bool A;

            var cantidad=obj.Sol_Chat.Where(cd => cd.SolicitudId == CodigoSolicitud && cd.Estado==true).ToList();

            if (cantidad.Count > 0)
            {
                A = true;
            }
            else
            {
                A = false;
            }
            return A;
        }
        #endregion

        #region MostrarActividades
        private void MostrarActividades(int CodigoActividad)
        {
            var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CodigoActividad).FirstOrDefault();

            smodel.CodigoSupervisor=Supervisor.SupervisorId;
        }
        #endregion

        #region MostrarDependenciaRegional
        public void MostrarDependenciasRegionales(int CdgDependencia)
        {
            var Supervisor = obj.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == CdgDependencia).FirstOrDefault();

            smodel.CodigoSupervisor = Supervisor.DependenciaSupervisorId;
        }
        #endregion

        #region ValidarUsuarioChat
        private int OtrosUsuario(int CodigoUsuario, int CodigoSolicitud)
        {
            int Error=0;

            var ListaChat = obj.Sol_Mostrar_Otros_Tecnicos.Where(cd => cd.TecnicoId == CodigoUsuario && cd.SolicitudId == CodigoSolicitud && cd.Estado==true).ToList();

            if (ListaChat.Count > 0)
            {
                Error = 1;
            }
            else
            {
                Error = 2;
            }
            return Error;
        }
        #endregion

        #region Mostrar_Codigo_Tipo_Solicitud
        private int Mth_Mostrar_Codigo_Tipo_Solicitud(int CodigoSolicitud)
        {
            int Codigo = 0;

            var A=obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitud == CodigoSolicitud && cd.Estado == true).FirstOrDefault();

            Codigo = A.CodigoTipoSolicitud;

            return Codigo;
        }
        #endregion

        #region Mostrar_Cedula_Empleados
        private string Mth_Mostrar_Cedula_Empleados(string CodigoEmpleados)
        {
            string Cedula = "";

            var A = obj.Sol_Mostrar_Personal_PGR.Where(cd => cd.Codigo == CodigoEmpleados).FirstOrDefault();

            Cedula = A.Cedula;

            return Cedula;
        }
        #endregion

        #region Mostrar_Nombre_Empleados
        private string Mth_Mostrar_Nombre_Empleados(string CodigoEmpleados)
        {
            string NombreCompletos = "";

            var A = obj.Sol_Mostrar_Personal_PGR.Where(cd => cd.Codigo == CodigoEmpleados).FirstOrDefault();

            if (A == null)
                return string.Empty;

            NombreCompletos = A.NombreCompleto;

            return NombreCompletos;
        }
        #endregion

        #region Mostrar_Informacion_Actividades
        private void Mostrar_Informacion_Actividades()
        {
            //if (Enlace.State == ConnectionState.Closed)
            //{
            //    Enlace.Open();
            //}

            //SqlCommand cmd = new SqlCommand("SP_Mostrar_Dep", Enlace);
            //cmd.CommandType = CommandType.StoredProcedure;
            //SqlDataAdapter Sda = new SqlDataAdapter(cmd);
            //DataTable Dta = new DataTable();
            //Sda.Fill(Dta);
            
            //List<SelectListItem> list = new List<SelectListItem>();
            
            //smodel.Departamentos = Dta.AsEnumerable().Select(cd => new DepartamentoModel()
            //{
            //    DepartamentoId = cd.Field<int>("DepartamentoId"),
            //    Descripcion = cd.Field<string>("Descripcion"),
            //}).ToList();
            smodel.TipoActividades = obj.Sol_Actividades.Where(cd => cd.Estado == true /*&& cd.DepartamentoId==CodigoDepartamento && cd.ActividadId==CodigoActividad*/).Select(cd => new ActividadesModel()            {
                ActividadId = cd.ActividadId,
                Descripcion = cd.Descripcion
            }).ToList();
        }
        #endregion
        
        #region ObtenerEtiquetaHTML
        public string DescripcionCorreo(string Tema,string Departamento, string NombreCompleto, string Problema, string Link,string descripcion)
        {
            string NombreArchivo = Server.MapPath(@"~/EtiquetasHtml/Solicitud-nuevoServicio.html");
            
            string Archivo = "";

            using (StreamReader sr = new StreamReader(NombreArchivo))
            {
                Archivo = sr.ReadToEnd();
            }

            Archivo = Archivo.Replace("$tituloMensajeCorreo", Tema);
            Archivo = Archivo.Replace("$departamento", Departamento);
            Archivo = Archivo.Replace("$nombres", NombreCompleto);
            Archivo = Archivo.Replace("$tema", Problema);
            Archivo = Archivo.Replace("$pagina", Link);
            Archivo = Archivo.Replace("$Descripcion", descripcion);
            
            return Archivo;
        }

        #endregion

        #region Mostrar_Actividad
        private string Mth_Mostrar_Actividad(int CodigoActividad)
        {
            string Descripcion = "";

            var A = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CodigoActividad).FirstOrDefault();

            Descripcion = A.Descripcion;

            return Descripcion;
        }
        #endregion

        #region Mostrar_Tipos_Actividades_Servicios
        public JsonResult GetSubActividades(int id)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            if (id < 1)
            {
                RedirectToAction("RegistroSolicitud(");
            }
            else
            {
                //if (Enlace.State == ConnectionState.Closed)
                //{
                //    Enlace.Open();
                //}

                //SqlCommand cmd = new SqlCommand("SP_Buscar_Actividades", Enlace);
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@CdgDepartamento", id);
                //SqlDataAdapter Sda = new SqlDataAdapter(cmd);
                //DataTable Dta = new DataTable();
                //Sda.Fill(Dta);
                
                //foreach (DataRow Dr in Dta.Rows)
                //{
                //    list.Add(new SelectListItem { Text = Dr["Descripcion"].ToString(), Value = Dr["ActividadId"].ToString() });
                //}

                var Sub_Tipo_Actividades = obj.Sol_Sub_Actividades.Where(cd => cd.ActividadId == id).ToList();

                foreach (var Dr in Sub_Tipo_Actividades)
                {
                    list.Add(new SelectListItem { Text = Dr.Descripcion.ToString(), Value = Dr.SubActividadId.ToString() });
                }
            }
            
            return Json(new SelectList(list, "value", "Text"));
        }
        #endregion
        
        #region Mostrar_Informacion_Sub_Actividades
        private void Mostrar_Informacion_Sub_Actividades(int CodigoDepartamento, int CodigoActividad)
        {
            //if (Enlace.State == ConnectionState.Closed)
            //{
            //    Enlace.Open();
            //}

            //SqlCommand cmd = new SqlCommand("SP_Mostrar_Dep", Enlace);
            //cmd.CommandType = CommandType.StoredProcedure;
            //SqlDataAdapter Sda = new SqlDataAdapter(cmd);
            //DataTable Dta = new DataTable();
            //Sda.Fill(Dta);
            
            //List<SelectListItem> list = new List<SelectListItem>();
            
            //smodel.Departamentos = Dta.AsEnumerable().Select(cd => new DepartamentoModel()
            //{
            //    DepartamentoId = cd.Field<int>("DepartamentoId"),
            //    Descripcion = cd.Field<string>("Descripcion"),
            //}).ToList();

            smodel.SubTipoActividades = obj.Sol_Sub_Actividades.Where(cd => cd.Estado == true && cd.DepartamentoId == CodigoDepartamento && cd.ActividadId == CodigoActividad).Select(cd => new SubActividadesModel()
            {
                SubActividadId = cd.SubActividadId,
                Descripcion = cd.Descripcion
            }).ToList();
        }
        #endregion
        
        #region MostrarCantidaddeSolicitud
        private void MostrarCantidaddeSolicitud()
        {
            int Cdg = 0;

            Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

            var Cantidad = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

            if (Cantidad.Count > 0)
            {
                HttpContext.Session["Cantidad"] = Cantidad.Count();
            }
            else
            {
                HttpContext.Session["Cantidad"] = 0;
            }
        }
        #endregion

        #region MostrarCantidadAsignados
        private void MostrarCantidadAsignados()
        {
            int Cdg = 0;

            Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());

            var CantidadAsignado = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTecnico == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

            if (CantidadAsignado.Count > 0)
            {
                HttpContext.Session["CantidadAsignado"] = CantidadAsignado.Count();
            }
            else
            {
                HttpContext.Session["CantidadAsignado"] = 0;
            }
        }
        #endregion
        
        #region MostrarDescripcionAsistenciaSolicitud
        public string DescripcionAsistenciaSlocitud(int CdgAsistencia)
        {
            string Resultado = "";

            var A = obj.Sol_TipodeAsistencia.Where(cd => cd.TipoAsistenciaId == CdgAsistencia).FirstOrDefault();

            Resultado = A.Descripcion;

            return Resultado;
        }
        #endregion

        #region MostrarDescripcionEstadoSolicitud
        public string MostrarDescripcionEstadoSolicitud(int CdgEstado)
        {
            string Resultado = "";

            var A = obj.Sol_Estado_Solicitud.Where(cd => cd.EstadoId == CdgEstado).FirstOrDefault();

            Resultado = A.Descripcion;

            return Resultado;
        }
        #endregion 

        [HttpGet]
        [CustomAuthorize]
        public ActionResult Download(string file)
        {
            
            var filepath = System.IO.Path.Combine(Server.MapPath("~/Content/Archivos/"), file);
            
            return File(filepath, MimeMapping.GetMimeMapping(filepath), file);
        }
        
        // GET: RegistroSolicitud
        [HttpGet]
        [AllowAnonymous]
        public ActionResult RegistroSolicitud(int id)
        {
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }

            if (id == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                Mostrar_Registro_Solicitud(id);

                string codigo_solicitante = smodel.DECodigoSolicitante.ToString();

                using (dbIntranet db = new dbIntranet())
                {
                    var flota_extension = db.FlotaExtensionEmpleado.Where(x => x.FlotExtEmpEmpleadoId.ToString() == codigo_solicitante).FirstOrDefault();
                    
                    if(flota_extension!=null)
                    {
                        ViewBag.Confirmed = flota_extension.FlotExtEmpDatosConfirmados;

                        smodel.Flota = flota_extension.FlotExtEmpFlota;
                        smodel.Extension = flota_extension.FlotExtEmpExtesion;
                    }
                    else
                    {
                        smodel.Flota = "";
                        smodel.Extension = "";
                    }
                }
            }

            ViewBag.detalleEvento = obj.Eventos.Where(x => x.SolicitudId == id).ToList();//Se utiliza para que solamente el
                                                                                         //supervisor de comunicaciones pueda visualizar los detalles de las areas reservadas.//


            int Cdg = int.Parse(HttpContext.Session["empleadoId"].ToString());
            smodel.CodigoOtrosTecnicos = CdgOtrosTecnicos(Cdg);

            MostrarCantidaddeSolicitud(Cdg);

            MostrarCantidadAsignados(Cdg);

            MostrarCantidadChat(Cdg);

            return View(smodel);
        }

        #region MostrarSINOOtrosTecnicos
        private int CdgOtrosTecnicos(int CodigoOtrosTecnicos)
        {
            int Resultado = 0;

            var cantidad = obj.Sol_Tecnicos_Alternativos.Where(cd => cd.TecnicoId == CodigoOtrosTecnicos).ToList();

            if (cantidad.Count > 0)
            {
                Resultado = 1;
            }
            else
            {
                Resultado = 2;
            }

            return Resultado;
        }
        #endregion

        [HttpPost]
        public ActionResult Registro(FormCollection Fc, SolicitudModel model, HttpPostedFileBase SubirArchivo)
        {
            int CodigoSolicitud = 0;

            string NombreCompletos = "";

            string Departamentos = "";

            CodigoSolicitud = int.Parse(Session["CodigoSolicitud"].ToString());

            NombreCompletos = HttpContext.Session["nombre"].ToString();

            Departamentos = Session["departamento"].ToString();



            try
            {
                //Mostrar_Registro_Solicitud(CodigoSolicitud);

                if (Fc["Agregar"] != null)
                {
                    //MostrarEstadoConsulta();
                    //MostrarOtrosTecnicos(CodigoSolicitud);
                    //Mostrar_Informacion_Tipo_Asistencia();
                    //Mostrar_Informacion_Personal_PGR_Por_Deptos(Session["CdgDeptos"].ToString());
                    
                    if (Usuario(CodigoSolicitud) == true)
                    {
                        int A = OtrosUsuario(model.CodigoOtrosEmpleados, CodigoSolicitud);


                        if (A == 2)
                        {
                            Sol_Otros_Tecnicos Tb = new Sol_Otros_Tecnicos();
                            Tb.SolicitudId = CodigoSolicitud;
                            Tb.TecnicoId = model.CodigoOtrosEmpleados;
                            Tb.Estado = true;
                            Tb.FhModificacion = DateTime.Now;
                            Tb.Fhcreacion = DateTime.Now;
                         
                            obj.Sol_Otros_Tecnicos.Add(Tb);
                            obj.SaveChanges();

                            //INSERTAR EN LOG 

                            string NombreCompleto = Mth_Mostrar_Nombre_Empleados(Convert.ToString(model.CodigoOtrosEmpleados));

                            string descripcion = "";
                            
                            descripcion = "SE AGREGO AL CHAT EL EMPLEADO " + NombreCompleto;

                            InsertarLog(CodigoSolicitud, descripcion);

                            //Metodo EnviarCorreo
                            
                            string Cedula = Mth_Mostrar_Cedula_Empleados(Convert.ToString(model.CodigoOtrosEmpleados));
                            
                            if(model.CodigoDependencia == 1)
                            {
                                int CdgTipoSolicitud = Mth_Mostrar_Codigo_Tipo_Solicitud(CodigoSolicitud);

                                var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                                String CorreoUsuario = "";
                            

                                var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                                if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                                {
                                    CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                                }
                                else
                                {
                                    CorreoUsuario = correoUsuario(Cedula);
                                }

                                //EnviarCorreo(Supervisor.Correo, CorreoUsuario, "SOPORTE", "USTED A SIDO AGREGADO AL CHAT"+NombreCompleto);

                                EnviarCorreo(Supervisor.Correo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("INCLUSIÓN AL CHAT", Departamentos, NombreCompletos, descripcion, DireccionChat, Supervisor.Descripcion));
                            }
                            else
                            {
                                var Supervisor = obj.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == model.CodigoDependencia && cd.DependenciaEstado == true).FirstOrDefault();

                                String CorreoUsuario = "";

                                var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                                if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                                {
                                    CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                                }
                                else
                                {
                                    CorreoUsuario = correoUsuario(Cedula);
                                }

                                //EnviarCorreo(Supervisor.Correo, CorreoUsuario, "SOPORTE", "USTED A SIDO AGREGADO AL CHAT"+NombreCompleto);

                                EnviarCorreo(Supervisor.DependenciaSupervisorCorreo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.DependenciaNombre, DescripcionCorreo("INCLUSIÓN AL CHAT", "DIRECCION DE TECNOLOGIA DE LA INFORMACION", NombreCompletos, descripcion, DireccionChat, Supervisor.DependenciaNombre));
                            }
                            
                            MostrarOtrosTecnicos(CodigoSolicitud);
                        }
                        else
                        {
                            ViewBag.MSJG = "ESTE USUARIO YA HA SIDO AGREGADO AL CHAT.";
                        }
                    }
                    else
                    {
                        ViewBag.MSJ = "NO PUEDE AGREGAR USUARIO AL CHAT SIN HABER ASIGNADO UN TECNICO A LA SOLICITUD.";
                    }
                }
                else if (Fc["ActulizarFechaCompromiso"] != null)
                {
                    try
                    {
                        Sol_Registro_Solicitud Tb = new Sol_Registro_Solicitud();

                        Tb = obj.Sol_Registro_Solicitud.Find(CodigoSolicitud);
                        ViewBag.Solicitud = Tb.DescripcionSolicitud;
                        Tb.FhFinalSolicitud = model.DEFechaFinalSolicitud;
                        Tb.DescripcionSolucion =  Fc["DEDescripcionSolucion"].ToString();
                        Tb.FhModificacion = DateTime.Now;
                        obj.Entry(Tb).State = EntityState.Modified;
                        obj.SaveChanges();
                        Mostrar_Registro_Solicitud(CodigoSolicitud);
                        ViewBag.MSJG = "SE MODIFICO LA FECHA DE COMPROMISO.";

                        //INSERTAR EN LOG 

                        string descripcion = "";
                        
                        descripcion = "SE MODIFICO EL TIPO DE SOLICITUD NO:" + CodigoSolicitud;

                        InsertarLog(CodigoSolicitud, descripcion);

                        //Metodo EnviarCorreo

                        string Cedula = Mth_Mostrar_Cedula_Empleados(Convert.ToString(model.DECodigoTecnico));
                        
                        if(model.CodigoDependencia == 1)
                        {
                            int CdgTipoSolicitud = Mth_Mostrar_Codigo_Tipo_Solicitud(CodigoSolicitud);

                            var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                            String CorreoUsuario = "";

                            var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                            if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                            {
                                CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                            }
                            else
                            {
                                CorreoUsuario = correoUsuario(Cedula);
                            }

                            var Data = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitud == CodigoSolicitud).FirstOrDefault();

                            EnviarCorreo(Supervisor.Correo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("MODIFICACIÓN DE SOLICITUD", Departamentos, NombreCompletos, descripcion, Direccion + CodigoSolicitud, Supervisor.Descripcion));
                        }
                        else
                        {
                            var Supervisor = obj.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == model.CodigoDependencia && cd.DependenciaEstado == true).FirstOrDefault();

                            String CorreoUsuario = "";

                            var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                            if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                            {
                                CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                            }
                            else
                            {
                                CorreoUsuario = correoUsuario(Cedula);
                            }

                            var Data = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitud == CodigoSolicitud).FirstOrDefault();

                            EnviarCorreo(Supervisor.DependenciaSupervisorCorreo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.DependenciaNombre, DescripcionCorreo("MODIFICACIÓN DE SOLICITUD", "DIRECCION DE TECNOLOGIA DE LA INFORMACION", NombreCompletos, descripcion, Direccion + CodigoSolicitud, Supervisor.DependenciaNombre));
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }
                }
                else if (Fc["ActualizarTipoSolicitud"] != null)
                {
                    try
                    {
                        Sol_Registro_Solicitud tb = new Sol_Registro_Solicitud();
                        tb = obj.Sol_Registro_Solicitud.Find(CodigoSolicitud);
                        tb.Tipo_SolicitudId = model.ActividadId;
                        tb.Tipo_Sub_SolicitudId = model.SubActividadId;
                        tb.FhFinalSolicitud = DateTime.Now;
                        tb.FhModificacion = DateTime.Now;
                        tb.DescripcionSolucion = Fc["DEDescripcionSolucion"];
                        int tecnicoId = obj.Sol_Actividades.Where(x => x.ActividadId == model.ActividadId).Select(x => x.SupervisorId).FirstOrDefault();
                        tb.TecnicoId = tecnicoId;
                        obj.Entry(tb).State = EntityState.Modified;
                        obj.SaveChanges();
                        Mostrar_Registro_Solicitud(CodigoSolicitud);
                        ViewBag.MessageTipoSol = "SE MODIFICO EL TIPO DE SOLICITUD.";

                        //INSERTAR LOG 
                        string descripcion = "";

                        descripcion = "SE MODIFICO EL TIPO DE SOLICITUD:" + Fc["ActividadDescripcion"] + "/" + Fc["SubActividadDescripcion"];
                        InsertarLog(CodigoSolicitud, descripcion);

                        //METODO PARA ENVIAR CORREO

                        string Cedula = Mth_Mostrar_Cedula_Empleados(Convert.ToString(model.DECodigoTecnico));

                        int CdgTipoSolicitud = Mth_Mostrar_Codigo_Tipo_Solicitud(CodigoSolicitud);

                        var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                        String CorreoUsuario = "";

                        var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                        if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                        {
                            CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                        }
                        else
                        {
                            CorreoUsuario = correoUsuario(Cedula);
                        }

                        var Data = obj.Sol_Detalle_Lista.Where(cd => cd.Estado == true && cd.CodigoSolicitud == CodigoSolicitud).FirstOrDefault();

                        EnviarCorreo(Supervisor.Correo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("MODIFICACIÓN DE SOLICITUD", Departamentos, NombreCompletos, descripcion, Direccion + CodigoSolicitud, Supervisor.Descripcion));
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }
                }
                else if (Fc["Imprimir"] != null)
                {
                    return(Reporte(Convert.ToInt32(Fc["idSolicitud"])));
                }
                else if (Fc["Guardar"] != null)
                {
                    Sol_Registro_Solicitud Tb = new Sol_Registro_Solicitud();
                    
                    if (Session["CodigoTecnico"] != null)
                    {
                        try
                        {
                            Tb = obj.Sol_Registro_Solicitud.Find(CodigoSolicitud);
                            Tb.DescripcionSolucion = Fc["DEDescripcionSolucion"].ToString();

                           

                            //if(model.CodigoEstadoRB == 2 && model.DEDescripcionSolucion == null )
                            //{
                            //    ViewBag.Solucion = "Ok";
                            //    return View("RegistroSolicitud", smodel);
                            //}

                            if (model.CodigoEstadoRB == 0)
                            {
                                ViewBag.MSJG = "SELECCIONE EL ESTATUS DE LA SOLICITUD.";
                            }
                            else if (model.CodigoTipoRB == 0)
                            {
                                ViewBag.MSJG = "SELECCIONE EL TIPO DE LA SOLICITUD.";
                            }
                            else if ((model.CodigoEstadoRB == 0) || (model.CodigoTipoRB == 0))
                            {
                                ViewBag.MSJG = "FAVOR SELECCIONAR EL TIPO Y EL ESTADO DE LA SOLICITUD.";
                            }
                            else if ((model.CodigoEstadoRB > 0) && (model.CodigoTipoRB > 0) && (model.Descripcion != null) && (model.Descripcion != ""))
                            {
                                Tb.EstadoId = model.CodigoEstadoRB;
                                Tb.TipodeAsistencia = model.CodigoTipoRB;
                                Tb.DescripcionSolucion = Fc["DEDescripcionSolucion"].ToString();
                                Tb.DomainUserTecnico = model.DEUsuarioDominioTecn;
                                Tb.FhFinalSolicitud = DateTime.Now;
                                Tb.FhModificacion = DateTime.Now;
                                obj.Entry(Tb).State = EntityState.Modified;
                                obj.SaveChanges();
                                Session["CodigoTecnico"] = null;

                                var solicitados = Session["CantidadSolicitados"].ToString();
                                var asignado = Session["CantidadAsignados"].ToString();
                                solicitados.Count();
                                asignado.Count();
                                //MostrarCantidaddeSolicitud();
                                //MostrarCantidadAsignados();

                                //Mostrar_Registro_Solicitud(CodigoSolicitud);
                                //ViewBag.MSJG = "SE ACTUALIZO LA SOLICITUD.";

                                //INSERTAR EN LOG 

                                string descripcion = "";
                                
                                descripcion = "Se Actualizo La Solicitud Via " + DescripcionAsistenciaSlocitud(model.CodigoTipoRB)+ " ,Con El Estado: " + MostrarDescripcionEstadoSolicitud(model.CodigoEstadoRB)+".";

                                InsertarLog(CodigoSolicitud, descripcion);

                                //Metodo EnviarCorreo
                                
                                var A = obj.Sol_Registro_Solicitud.Where(cd => cd.SolicitudId == CodigoSolicitud).FirstOrDefault(); 

                                string Cedula = Mth_Mostrar_Cedula_Empleados(Convert.ToString(A.SolicitanteId));
                                
                                if(model.CodigoDependencia == 1)
                                {
                                    int CdgTipoSolicitud = Mth_Mostrar_Codigo_Tipo_Solicitud(CodigoSolicitud);

                                    var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                                    String CorreoUsuario = "";

                                    var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                                    if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                                    {
                                        CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                                    }
                                    else
                                    {
                                        CorreoUsuario = correoUsuario(Cedula);
                                    }

                                    EnviarCorreo(Supervisor.Correo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("ACTUALIZACIÓN DE SOLICITUD", Departamentos, NombreCompletos, descripcion, Direccion + CodigoSolicitud, Supervisor.Descripcion));
                                }
                                else
                                {
                                    var Supervisor = obj.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == model.CodigoDependencia && cd.DependenciaEstado == true).FirstOrDefault();

                                    String CorreoUsuario = "";

                                    var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                                    if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                                    {
                                        CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                                    }
                                    else
                                    {
                                        CorreoUsuario = correoUsuario(Cedula);
                                    }

                                    EnviarCorreo(Supervisor.DependenciaSupervisorCorreo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.DependenciaNombre, DescripcionCorreo("ACTUALIZACIÓN DE SOLICITUD", "DIRECCION TECNOLOGIA DE LA INFORMACION", NombreCompletos, descripcion, Direccion + CodigoSolicitud, Supervisor.DependenciaNombre));
                                }
                                
                                return RedirectToAction("Solicitados", "Solicitados");
                            }
                            else if ((model.CodigoEstadoRB > 1) && (model.CodigoTipoRB > 0))
                            {
                              
                                Tb.EstadoId = model.CodigoEstadoRB;
                                Tb.TipodeAsistencia = model.CodigoTipoRB;
                                Tb.DescripcionSolucion = Fc["DEDescripcionSolucion"].ToString();
                                Tb.DomainUserTecnico = Tb.DomainUserTecnico;
                                Tb.FhFinalSolicitud = DateTime.Now;
                                Tb.FhModificacion = DateTime.Now;
                                obj.Entry(Tb).State = EntityState.Modified;
                                obj.SaveChanges();
                                Session["CodigoTecnico"] = null;
                                var solicitados = Session["CantidadSolicitados"].ToString();
                                var asignado = Session["CantidadAsignados"].ToString();
                                solicitados.Count();
                                asignado.Count();
                                //MostrarCantidaddeSolicitud();
                                //MostrarCantidadAsignados();

                                //Mostrar_Registro_Solicitud(CodigoSolicitud);
                                //ViewBag.MSJG = "SE ACTUALIZO LA SOLICITUD.";

                                //INSERTAR EN LOG 

                                string descripcion = "";
                                
                                descripcion = "Se Actualizo La Solicitud Via " + DescripcionAsistenciaSlocitud(model.CodigoTipoRB) + " ,Con El Estado: " + MostrarDescripcionEstadoSolicitud(model.CodigoEstadoRB) + ".";

                                InsertarLog(CodigoSolicitud, descripcion);

                                //Metodo EnviarCorreo
                                
                                var A = obj.Sol_Registro_Solicitud.Where(cd => cd.SolicitudId == CodigoSolicitud).FirstOrDefault();

                                string Cedula = Mth_Mostrar_Cedula_Empleados(Convert.ToString(A.SolicitanteId));
                                
                                int CdgTipoSolicitud = Mth_Mostrar_Codigo_Tipo_Solicitud(CodigoSolicitud);

                                var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                                String CorreoUsuario = "";

                                var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                                if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                                {
                                    CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                                }
                                else
                                {
                                    CorreoUsuario = correoUsuario(Cedula);
                                }

                                EnviarCorreo(Supervisor.Correo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("ACTUALIZACIÓN DE SOLICITUD", Departamentos, NombreCompletos, descripcion, Direccion + CodigoSolicitud, Supervisor.Descripcion));
                                //Correo encuesta
                                string url = $"{HttpContext.Request.Url.Host}/{HttpRuntime.AppDomainAppVirtualPath}/FormularioEncuestas/FormularioEncuesta?id={CodigoSolicitud}";

                                EncuestaCorreo(A.DomainUserSolicitante, "ENCUESTA SERVICIO", DescripcionCorreoEncuesta( url));

                                return RedirectToAction("Asignados", "Asignados");
                            }
                            else if ((model.Descripcion == null) || (model.Descripcion == ""))
                            {
                                ViewBag.MSJG = "FAVOR DETALLAR LA SOLUCION DEL PROBLEMA.";
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                        }
                    }
                    else
                    {
                        try
                        {
                            Tb = obj.Sol_Registro_Solicitud.Find(CodigoSolicitud);

                            if (model.CodigoEmpleadosTecnicos > 0)
                            {
                                Tb.TecnicoId = model.CodigoEmpleadosTecnicos;
                                //Tb.EstadoId = model.CodigoEstadoRB;
                                //Tb.TipodeAsistencia = model.CodigoTipoRB;
                                Tb.DescripcionSolucion = Fc["DEDescripcionSolucion"].ToString();
                                Tb.FhFinalSolicitud = DateTime.Now;
                                Tb.FhModificacion = DateTime.Now;
                                obj.Entry(Tb).State = EntityState.Modified;
                                obj.SaveChanges();
                                Mostrar_Registro_Solicitud(CodigoSolicitud);
                                ViewBag.MSJG = "SE LE ASIGNO EL TECNICO A LA SOLICITUD.";

                                //INSERTAR EN LOG 

                                string descripcion = "";
                                
                                descripcion = "SE LE ASIGNO EL TECNICO A LA SOLICITUD.";

                                InsertarLog(CodigoSolicitud, descripcion);

                                //Metodo EnviarCorreo

                                string Cedula = Mth_Mostrar_Cedula_Empleados(Convert.ToString(model.CodigoEmpleadosTecnicos));
                                
                                int CdgTipoSolicitud = Mth_Mostrar_Codigo_Tipo_Solicitud(CodigoSolicitud);

                                var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                                String CorreoUsuario = "";

                                var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                                if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                                {
                                    CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                                }
                                else
                                {
                                    CorreoUsuario = correoUsuario(Cedula);
                                }

                                //EnviarCorreo(Supervisor.Correo, CorreoUsuario, "SOPORTE", "SE LE ASIGNO EL TECNICO A LA SOLICITUD A "+ CorreoUsuario);
                                EnviarCorreo(Supervisor.Correo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("ASIGNACIÓN DE TECNICO", Departamentos, NombreCompletos, descripcion, Direccion + CodigoSolicitud, Supervisor.Descripcion));
                            }
                            else
                            {
                                ViewBag.MSJG = "NO SE LE ASIGNO EL TECNICO A LA SOLICITUD.";
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                        }
                    }
                }
                else if (Fc["Volver"] != null)
                {
                    if (HttpContext.Session["RetornoSolicitud"] != null)
                    {
                        if(HttpContext.Session["RetornoSolicitud"].ToString() == "Asignados")
                        {
                            return RedirectToAction("Asignados", "Asignados");
                        }
                        else if(HttpContext.Session["RetornoSolicitud"].ToString() == "Solicitados")
                        {
                            return RedirectToAction("Solicitados", "Solicitados");
                        }
                    }
                    //else
                    //{
                    //    return RedirectToAction("Index", "Home");
                    //}
                }
                else if (Fc["Enviar"] != null)
                {
                    //Variables Usuario

                    string Cdg = "";

                    string Usuario = "";

                    string DomainUserSolicitante = "";

                    string NombreCompleto = "";

                    string Cedula = "";

                    DomainUserSolicitante = HttpContext.Session["usuario"].ToString();

                    //Variable de Secciones
                    Cdg = HttpContext.Session["empleadoId"].ToString();
                    NombreCompleto = HttpContext.Session["nombre"].ToString();
                    Cedula = HttpContext.Session["cedula"].ToString();
                    Usuario = HttpContext.Session["usuario"].ToString();

                    //Insertar Usuario
                    #region Inclucion chat
                   // var es_director = obj.Sol_Departamento.Where(x => x.DirectorId.ToString() == Cdg).FirstOrDefault();

                   // if (es_director != null)
                   // {
                   //     if (this.Usuario(CodigoSolicitud) == true)
                   //     {
                   //         int A = OtrosUsuario(model.CodigoOtrosEmpleados, CodigoSolicitud);

                   //         if (A == 2)
                   //         {
                   //             Sol_Otros_Tecnicos Tb = new Sol_Otros_Tecnicos();
                   //             Tb.SolicitudId = CodigoSolicitud;
                   //             Tb.TecnicoId = Convert.ToInt32(Cdg);
                   //             Tb.Estado = true;
                   //             Tb.FhModificacion = DateTime.Now;
                   //             Tb.Fhcreacion = DateTime.Now;
                   //             obj.Sol_Otros_Tecnicos.Add(Tb);
                   //             obj.SaveChanges();

                   //             //INSERTAR EN LOG 

                   //             string NombreCompleto2 = Mth_Mostrar_Nombre_Empleados(Convert.ToString(model.CodigoOtrosEmpleados));

                   //             string descripcion = "";

                   //             descripcion = "SE AGREGO AL CHAT EL EMPLEADO " + NombreCompleto2;

                   //             InsertarLog(CodigoSolicitud, descripcion);

                   //             //Metodo EnviarCorreo

                   //             string Cedula2 = Mth_Mostrar_Cedula_Empleados(Convert.ToString(model.CodigoOtrosEmpleados));

                   //             if (model.CodigoDependencia == 1)
                   //             {
                   //                 int CdgTipoSolicitud = Mth_Mostrar_Codigo_Tipo_Solicitud(CodigoSolicitud);

                   //                 var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                   //                 String CorreoUsuario = "";

                   //                 var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula2).FirstOrDefault();

                   //                 if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                   //                 {
                   //                     CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula2);
                   //                 }
                   //                 else
                   //                 {
                   //                     CorreoUsuario = correoUsuario(Cedula2);
                   //                 }

                   //                 //EnviarCorreo(Supervisor.Correo, CorreoUsuario, "SOPORTE", "USTED A SIDO AGREGADO AL CHAT"+NombreCompleto);

                   //                 EnviarCorreo(Supervisor.Correo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("INCLUSIÓN AL CHAT", Departamentos, NombreCompletos, descripcion, DireccionChat, Supervisor.Descripcion));
                   //             }
                   //             else
                   //             {
                   //                 var Supervisor = obj.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == model.CodigoDependencia && cd.DependenciaEstado == true).FirstOrDefault();

                   //                 String CorreoUsuario = "";

                   //                 var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula2).FirstOrDefault();

                   //                 if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                   //                 {
                   //                     CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula2);
                   //                 }
                   //                 else
                   //                 {
                   //                     CorreoUsuario = correoUsuario(Cedula2);
                   //                 }

                   //                 //EnviarCorreo(Supervisor.Correo, CorreoUsuario, "SOPORTE", "USTED A SIDO AGREGADO AL CHAT"+NombreCompleto);

                   //                 EnviarCorreo(Supervisor.DependenciaSupervisorCorreo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.DependenciaNombre, DescripcionCorreo("INCLUSIÓN AL CHAT", "DIRECCION DE TECNOLOGIA DE LA INFORMACION", NombreCompletos, descripcion, DireccionChat, Supervisor.DependenciaNombre));
                   //             }

                   //             MostrarOtrosTecnicos(CodigoSolicitud);
                   //         }
                   //         else
                   //         {
                   //             ViewBag.MSJG = "ESTE USUARIO YA HA SIDO AGREGADO AL CHAT.";
                   //         }
                   //     }
                   //     else
                   //     {
                   //         ViewBag.MSJ = "NO PUEDE AGREGAR USUARIO AL CHAT SIN HABER ASIGNADO UN TECNICO A LA SOLICITUD.";
                   //     }
                   //}

                    #endregion

                    if (Fc["DescripcionChat"] != null)
                    {
                        Sol_Chat Tb = new Sol_Chat();

                        //Subir Archivo
                        if (SubirArchivo != null && SubirArchivo.ContentLength > 0)
                        {
                            var NombreArchivo = Path.GetFileName(SubirArchivo.FileName);
                            var extension = Path.GetExtension(SubirArchivo.FileName);

                            //C:\PGRRepositorio\Intranet_PGR\intra\Content\Archivos\)
                            var path = Path.Combine(Server.MapPath("~/Content/Archivos"), NombreArchivo);
                            SubirArchivo.SaveAs(path);

                            if (model.DescripcionChat != null)
                            {
                                var Extensiones = new[] {".doc", ".xlsx", ".txt", ".jpeg"};

                                if (!Extensiones.Contains(extension))
                                {
                                    Tb.SolicitudId = CodigoSolicitud;
                                    Tb.Empleadoid = Cdg;
                                    Tb.Usuario = Usuario;
                                    Tb.Descripcion = model.DescripcionChat;
                                    Tb.Estado = true;
                                    Tb.Archivo = NombreArchivo;
                                    Tb.FhCreacion = DateTime.Now;
                                    obj.Sol_Chat.Add(Tb);
                                    obj.SaveChanges();
                                }
                                else
                                {
                                    ViewBag.MSJG = "DEBE SER UN ARCHIVO EN FORMATO DE WORD, EXCEL, TXT Y JPG";
                                }
                            }
                            else
                            {
                                ViewBag.MSJG = "DEBE DIGITAR UN MENSAJE.";
                            }
                        }
                        else
                        {
                            Tb.SolicitudId = CodigoSolicitud;
                            Tb.Empleadoid = Cdg;
                            Tb.Usuario = Usuario;
                            Tb.Descripcion = model.DescripcionChat;
                            
                            Tb.Estado = true;
                            Tb.Archivo = "NoContiene";
                            Tb.FhCreacion = DateTime.Now;
                            obj.Sol_Chat.Add(Tb);
                            obj.SaveChanges();
                        }

                        //Metodo EnviarCorreo
                        if (model.CodigoDependencia == 1)
                        {
                            #region Correo Sede Central
                            int CdgTipoSolicitud = Mth_Mostrar_Codigo_Tipo_Solicitud(CodigoSolicitud);

                            var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                            var Codigo = obj.Sol_Mostrar_Otros_Tecnicos.Where(cd => cd.Estado == true && cd.SolicitudId == CodigoSolicitud).ToList();

                            if (Codigo.Count > 0)
                            {
                                int Conteo = 0;

                                foreach (var item in Codigo)
                                {
                                    Conteo++;

                                    if (Conteo == 1)
                                    {
                                        //if (item.Count > 0)
                                        //{
                                        //EnviarCorreo(Supervisor.Correo, correoUsuario(item.U.Cedula), "INFORMACION CHAT", Fc["DescripcionChat"].ToString());
                                        //EnviarCorreo(Supervisor.Correo, correoUsuario(item.U.Cedula), "INFORMACION CHAT SOLICITUD", Fc["DescripcionChat"].ToString());
                                        EnviarCorreo(Supervisor.Correo, correoUsuario(item.Cedula), "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("INFORMACIÓN MENSAJERIA", Departamentos, NombreCompletos, Fc["DescripcionChat"].ToString(), Direccion + CodigoSolicitud, Supervisor.Descripcion));
                                        // }

                                    }
                                    else
                                    {
                                        //if (item.Count > 0)
                                        //{
                                        //EnviarCorreo("", correoUsuario(item.U.Cedula), "INFORMACION CHAT", Fc["DescripcionChat"].ToString());
                                        EnviarCorreo("", correoUsuario(item.Cedula), "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("INFORMACIÓN MENSAJERIA", Departamentos, NombreCompletos, Fc["DescripcionChat"].ToString(), Direccion + CodigoSolicitud, Supervisor.Descripcion));
                                        //}
                                    }
                                }

                                Conteo = 0;

                            }
                            else
                            {
                                string CedulaEmp = "";

                                CedulaEmp = Mth_Mostrar_Cedula_Empleados(Cdg);

                                EnviarCorreo(Supervisor.Correo, correoUsuario(CedulaEmp), "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("INFORMACIÓN MENSAJERIA", Departamentos, NombreCompletos, Fc["DescripcionChat"].ToString(), Direccion + CodigoSolicitud, Supervisor.Descripcion));
                            }
                            #endregion
                        }
                        else
                        {
                            #region Correo Dependencias Regionales
                            var Supervisor = obj.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == model.CodigoDependencia && cd.DependenciaEstado == true).FirstOrDefault();

                            var Codigo = obj.Sol_Mostrar_Otros_Tecnicos.Where(cd => cd.Estado == true && cd.SolicitudId == CodigoSolicitud).ToList();

                            if (Codigo.Count > 0)
                            {
                                int Conteo = 0;

                                foreach (var item in Codigo)
                                {
                                    Conteo++;

                                    if (Conteo == 1)
                                    {
                                        //if (item.Count > 0)
                                        //{
                                        //EnviarCorreo(Supervisor.Correo, correoUsuario(item.U.Cedula), "INFORMACION CHAT", Fc["DescripcionChat"].ToString());
                                        //EnviarCorreo(Supervisor.Correo, correoUsuario(item.U.Cedula), "INFORMACION CHAT SOLICITUD", Fc["DescripcionChat"].ToString());
                                        EnviarCorreo(Supervisor.DependenciaSupervisorCorreo, correoUsuario(item.Cedula), "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.DependenciaNombre, DescripcionCorreo("INFORMACIÓN MENSAJERIA", "DIRECCION DE TECNOLOGIA DE LA INFORMACION", NombreCompletos, Fc["DescripcionChat"].ToString(), Direccion + CodigoSolicitud, Supervisor.DependenciaNombre));
                                        // }

                                    }
                                    else
                                    {
                                        //if (item.Count > 0)
                                        //{
                                        //EnviarCorreo("", correoUsuario(item.U.Cedula), "INFORMACION CHAT", Fc["DescripcionChat"].ToString());
                                        EnviarCorreo("", correoUsuario(item.Cedula), "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.DependenciaNombre, DescripcionCorreo("INFORMACIÓN MENSAJERIA", "DIRECCION DE TECNOLOGIA DE LA INFORMACION", NombreCompletos, Fc["DescripcionChat"].ToString(), Direccion + CodigoSolicitud, Supervisor.DependenciaNombre));
                                        //}
                                    }
                                }

                                Conteo = 0;

                            }
                            else
                            {
                                string CedulaEmp = "";

                                CedulaEmp = Mth_Mostrar_Cedula_Empleados(Cdg);

                                EnviarCorreo(Supervisor.DependenciaSupervisorCorreo, correoUsuario(CedulaEmp), "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.DependenciaNombre, DescripcionCorreo("INFORMACIÓN MENSAJERIA", "DIRECCION DE TECNOLOGIA DE LA INFORMACION", NombreCompletos, Fc["DescripcionChat"].ToString(), Direccion + CodigoSolicitud, Supervisor.DependenciaNombre));
                            }
                            #endregion
                        }

                    }
                    else
                    {
                        ViewBag.MSJG = "DEBE DIGITAR UN MENSAJE.";
                    }

                    Mostrar_Registro_Solicitud(CodigoSolicitud);

                }

                else if (Fc["Tracking"] != null)
                {
                    Session["CodigoSolicitudHistorico"] = CodigoSolicitud;

                    return RedirectToAction("Historico", "Historico");
                }
                else if (Fc["ActulizarHora"] != null)
                {
                    Sol_Registro_Solicitud Tb = new Sol_Registro_Solicitud();

                    try
                    {
                        Tb = obj.Sol_Registro_Solicitud.Find(CodigoSolicitud);

                        if (model.CodigoEmpleadosTecnicos > 0)
                        {
                            Tb.Horas = model.ReAsignarHora;
                            Tb.DescripcionSolucion = Fc["DEDescripcionSolucion"].ToString();
                            Tb.FhFinalSolicitud = DateTime.Now;
                            Tb.FhModificacion = DateTime.Now;
                            obj.Entry(Tb).State = EntityState.Modified;
                            obj.SaveChanges();
                            Mostrar_Registro_Solicitud(CodigoSolicitud);
                            ViewBag.MSJG = "SE LE ASIGNO NUEVA HORA A LA SOLICITUD.";
                            
                            //INSERTAR EN LOG 

                            string descripcion = "SE LE ASIGNO NUEVA HORA A LA SOLICITUD.";
                            
                            descripcion = " ";

                            InsertarLog(CodigoSolicitud, descripcion);
                            
                            //Metodo EnviarCorreo

                            string Cedula = Mth_Mostrar_Cedula_Empleados(Convert.ToString(model.CodigoEmpleadosTecnicos));

                            if (model.CodigoDependencia == 1)
                            {
                                int CdgTipoSolicitud = Mth_Mostrar_Codigo_Tipo_Solicitud(CodigoSolicitud);

                                var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                                String CorreoUsuario = "";

                                var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                                if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                                {
                                    CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                                }
                                else
                                {
                                    CorreoUsuario = correoUsuario(Cedula);
                                }

                                //EnviarCorreo(Supervisor.Correo,CorreoUsuario, "SOPORTE", "SE LE ASIGNO NUEVA HORA A LA SOLICITUD A " + CorreoUsuario);
                                //EnviarCorreo(Supervisor.Correo, CorreoUsuario, "INFORMACION SOLICITUD", "SE LE ASIGNO NUEVA HORA A LA SOLICITUD NO: " + CodigoSolicitud);

                                EnviarCorreo(Supervisor.Correo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("ASIGNACIÓN DE TIEMPO.", Departamentos, NombreCompletos, descripcion, Direccion + CodigoSolicitud, Supervisor.Descripcion));
                            }
                            else
                            {
                                var Supervisor = obj.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == model.CodigoDependencia && cd.DependenciaEstado == true).FirstOrDefault();

                                String CorreoUsuario = "";

                                var cedula_persona = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                                if (cedula_persona.CodigoDependencia == "10001" && cedula_persona.CodigoDepartamento == "15")
                                {
                                    CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                                }
                                else
                                {
                                    CorreoUsuario = correoUsuario(Cedula);
                                }

                                //EnviarCorreo(Supervisor.Correo,CorreoUsuario, "SOPORTE", "SE LE ASIGNO NUEVA HORA A LA SOLICITUD A " + CorreoUsuario);
                                //EnviarCorreo(Supervisor.Correo, CorreoUsuario, "INFORMACION SOLICITUD", "SE LE ASIGNO NUEVA HORA A LA SOLICITUD NO: " + CodigoSolicitud);

                                EnviarCorreo(Supervisor.DependenciaSupervisorCorreo, CorreoUsuario, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.DependenciaNombre, DescripcionCorreo("ASIGNACIÓN DE TIEMPO.", "DIRECCION DE TECNOLOGIA DE LA INFORMACION", NombreCompletos, descripcion, Direccion + CodigoSolicitud, Supervisor.DependenciaNombre));
                            }
                        }
                        else
                        {
                            ViewBag.MSJG = "NO SE LE ASIGNO EL TECNICO A LA SOLICITUD.";
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }
                }
                else if (Fc["AgregarAsignacion"] != null)
                {
                    Sol_Registro_Solicitud Tb = new Sol_Registro_Solicitud();

                    try
                    {
                        if (model.CodigoEmpleadosReAsignacion > 0)
                        {
                            Tb = obj.Sol_Registro_Solicitud.Where(x => x.SolicitudId == CodigoSolicitud).FirstOrDefault();

                            Tb.TecnicoId = model.CodigoEmpleadosReAsignacion;
                            //Tb.DomainUserTecnico = model.DEUsuarioDominioTecn;

                            var tecnico_object = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == Tb.TecnicoId.ToString()).FirstOrDefault();

                            var tecnico_cedula = tecnico_object.Cedula;

                            using (PGRINTRANETEntities db = new PGRINTRANETEntities())
                            {
                                if (tecnico_object.CodigoDependencia == "10001" && tecnico_object.CodigoDepartamento == "15")
                                {
                                    Tb.DomainUserTecnico = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", tecnico_cedula)).FirstOrDefault().samAccountName;

                                    
                                }
                                else
                                {
                                    Tb.DomainUserTecnico = Code.Utilities2.ObtenerCorreoUsuario2(tecnico_cedula);
                                  //  Tb.DomainUserTecnico = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", tecnico_cedula)).FirstOrDefault().samAccountName;
                                }
                            }
                            Tb.DescripcionSolucion = Fc["DEDescripcionSolucion"].ToString();
                            Tb.TecnicoId = model.CodigoEmpleadosReAsignacion;
                            Tb.DomainUserTecnico = Tb.DomainUserTecnico;
                            Tb.FhFinalSolicitud = DateTime.Now;
                            Tb.FhModificacion = DateTime.Now;
                            obj.Entry(Tb).State = EntityState.Modified;
                            obj.SaveChanges();

                            //INSERTAR EN LOG 

                            string descripcion = "";

                            descripcion = "SE REASIGNO " + Padron(Session["CodigoTecnico"].ToString()) + " POR " + Padron(Convert.ToString(model.CodigoEmpleadosReAsignacion));

                            InsertarLog(CodigoSolicitud, descripcion);

                            //Metodo EnviarCorreo
                            string Cedula = Mth_Mostrar_Cedula_Empleados(Session["CodigoTecnico"].ToString());

                            if (model.CodigoDependencia == 1)
                            {
                                int CdgTipoSolicitud = Mth_Mostrar_Codigo_Tipo_Solicitud(CodigoSolicitud);

                                var Supervisor = obj.Sol_Mostrar_Actividades.Where(cd => cd.CodigoActividad == CdgTipoSolicitud && cd.Estado == true).FirstOrDefault();

                                string correo_supervisor = "";

                                if (Supervisor != null)
                                {
                                    if (Supervisor.CodigoDepartamento == 15)
                                    {
                                        correo_supervisor = Code.Utilities2.ObtenerCorreoUsuario2(Supervisor.Cedula);
                                    }
                                    else
                                    {
                                        correo_supervisor = correoUsuario(Supervisor.Cedula);
                                    }
                                }

                                String CorreoUsuario = "";

                                var empleado_Cedula = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                                if (empleado_Cedula != null)
                                {
                                    if (empleado_Cedula.CodigoDependencia == "10001" && empleado_Cedula.CodigoDepartamento == "15")
                                    {
                                        CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                                    }
                                    else
                                    {
                                        CorreoUsuario = correoUsuario(Cedula);
                                    }
                                }

                                var solicitante_obj = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == Tb.SolicitanteId.ToString()).FirstOrDefault();

                                string solicitante_correo = "";

                                if (solicitante_obj != null)
                                {
                                    if (solicitante_obj.CodigoDependencia == "10001" && solicitante_obj.CodigoDepartamento == "15")
                                    {
                                        solicitante_correo = Code.Utilities2.ObtenerCorreoUsuario2(solicitante_obj.Cedula);
                                    }
                                    else
                                    {
                                        solicitante_correo = correoUsuario(Cedula);
                                    }
                                }

                                EnviarCorreo(solicitante_correo, correo_supervisor, Tb.DomainUserTecnico, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.Descripcion, DescripcionCorreo("REASIGNACIÓN DE SOLICITUD", Departamentos, NombreCompletos, descripcion, Direccion + CodigoSolicitud, Supervisor.Descripcion));

                            }
                            else
                            {
                                var Supervisor = obj.Sol_DependenciasRegionales.Where(cd => cd.DependenciaId == model.CodigoDependencia && cd.DependenciaEstado == true).FirstOrDefault();

                                string correo_supervisor = "";

                                if (Supervisor != null)
                                {
                                    var Supervisor_DatosPersonales = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == Supervisor.DependenciaSupervisorId.ToString()).FirstOrDefault();

                                    correo_supervisor = correoUsuario(Supervisor_DatosPersonales.Cedula);   
                                }

                                String CorreoUsuario = "";

                                var empleado_Cedula = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Cedula == Cedula).FirstOrDefault();

                                if (empleado_Cedula != null)
                                {
                                    if (empleado_Cedula.CodigoDependencia == "10001" && empleado_Cedula.CodigoDepartamento == "15")
                                    {
                                        CorreoUsuario = Code.Utilities2.ObtenerCorreoUsuario2(Cedula);
                                    }
                                    else
                                    {
                                        CorreoUsuario = correoUsuario(Cedula);
                                    }
                                }

                                var solicitante_obj = obj.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == Tb.SolicitanteId.ToString()).FirstOrDefault();

                                string solicitante_correo = "";

                                if (solicitante_obj != null)
                                {
                                    if (solicitante_obj.CodigoDependencia == "10001" && solicitante_obj.CodigoDepartamento == "15")
                                    {
                                        solicitante_correo = Code.Utilities2.ObtenerCorreoUsuario2(solicitante_obj.Cedula);
                                    }
                                    else
                                    {
                                        solicitante_correo = correoUsuario(Cedula);
                                    }
                                }

                                EnviarCorreo(solicitante_correo, correo_supervisor, Tb.DomainUserTecnico, "USTED HA RECIBIDO UN MENSAJE DEL SERVICIO " + Supervisor.DependenciaNombre, DescripcionCorreo("REASIGNACIÓN DE SOLICITUD", Departamentos, NombreCompletos, descripcion, Direccion + CodigoSolicitud, Supervisor.DependenciaNombre));
                            }
                            
                            Mostrar_Registro_Solicitud(CodigoSolicitud);
                        }
                        else
                        {
                            ViewBag.MSJG = "SE RE ASIGNO OTRO TECNICO A LA SOLICITUD.";
                        }
                    }
                    catch (Exception Ex)
                    {
                        Ex.Message.ToString();
                    }
                }
            }
            catch (Exception Ex)
            {
                Ex.Message.ToString();
            }

            var Cdg2 = Convert.ToInt32(HttpContext.Session["empleadoId"].ToString());

            Mostrar_Registro_Solicitud(CodigoSolicitud);
            //if (Session["usuario"] != null)
            //{ 

            //var solicitado = Session["CantidadSolicitados"].ToString();
            //var asignados = Session["CantidadAsignados"].ToString();
            //solicitado.Count();
            //asignados.Count();
            //}
            MostrarCantidaddeSolicitud(Cdg2);

            MostrarCantidadAsignados(Cdg2);

            //MostrarCantidadChat(Cdg2);

            if (Fc["Extension"].ToString().Contains(','))
                smodel.Extension = Fc["Extension"].ToString().Substring(0, Fc["Extension"].ToString().IndexOf(','));
            
            if(Fc["Flota"].ToString().Contains(','))
                smodel.Flota = Fc["Flota"].ToString().TrimEnd(',').Substring(0, Fc["Flota"].ToString().IndexOf(','));

            return View("RegistroSolicitud", smodel);
        }
        
        [HttpGet]
        public ActionResult EliminarRegistro(int id)
        {
            int CodigoSolicitud = 0;

            CodigoSolicitud = int.Parse(Session["CodigoSolicitud"].ToString());

            Sol_Otros_Tecnicos Tb = new Sol_Otros_Tecnicos();

            Tb = obj.Sol_Otros_Tecnicos.Where(cd=>cd.Estado==true && cd.Otros_Id==id).FirstOrDefault();

            Tb.Estado = false;
            Tb.FhModificacion = DateTime.Now;
            if (ModelState.IsValid)
            {
                obj.Entry(Tb).State = EntityState.Modified;
                obj.SaveChanges();
                Mostrar_Registro_Solicitud(CodigoSolicitud);
            }
            return View("RegistroSolicitud", smodel);
        }

        [HttpGet]
        public ActionResult Reporte(int? id)
        {
            if(id!=null)
            {
                PGRINTRANETEntities3 db2 = new PGRINTRANETEntities3();
                
                try
                {
                    var first_data = db2.Database.SqlQuery<Sp_Mostrar_Detalles_Solicitud_Reporte_Result>(string.Format("exec Sp_Mostrar_Detalles_Solicitud_Reporte {0}", id)).FirstOrDefault();

                    ViewBag.Date = DateTime.Now.ToString("dd/MM/yyy hh:mm tt");
                    ViewBag.Reference = id;
                    ViewBag.State = first_data.EstadoSolicitud;

                    ViewBag.RequestedBy = first_data.NombreSolicitante;
                    ViewBag.RequestedDepartment = first_data.Gerencia;
                    ViewBag.RequestedDate = first_data.FechaSolicitado.ToString("dd/MM/yyyy hh:mm tt");

                    ViewBag.DependencyId = first_data.DependenciaId;
                    ViewBag.Dependecy = first_data.DependenciaNombre;

                    if(first_data.DependenciaId == 1)
                    {
                        ViewBag.RequestType = first_data.TipoSolicitud;
                    }
                    else
                    {
                        ViewBag.RequestType = first_data.DependenciaNombre;
                    }
                    
                    ViewBag.SolverBy = first_data.ResultadoPor;
                    ViewBag.SolvedDepartment = first_data.Gerencia2;
                    ViewBag.SolutionDate = first_data.FechaSolucion != null ? first_data.FechaSolucion.Value.ToString("dd/MM/yyyy hh:mm tt") : "";

                    ViewBag.RequestDescription = first_data.DescripcionSolicitud;
                    ViewBag.SolutionDescription = first_data.DescripcionSolucion;

                    var second_date = db2.Database.SqlQuery<Sp_Mostrar_Detalles_Chat_Solicitud_Reporte_Result>(string.Format("exec Sp_Mostrar_Detalles_Chat_Solicitud_Reporte {0}", id)).ToList();

                    ViewBag.ChatHistory = second_date;

                }
                catch (Exception error)
                {
                    var m = error.Message;
                }
                
                return new ViewAsPdf("Reporte")
                {
                    PageOrientation = Rotativa.Options.Orientation.Portrait,
                    PageSize = Rotativa.Options.Size.A4,
                    PageMargins = { Left = 1, Right = 1, Top = 1, Bottom = 1 }
                };
            }
            else
            {
                return RedirectToAction("~/Home/Index");
            }
        }

        public bool ValidarActividad(int CodigoSupervisor)
        {
            bool CdgActividad;


            var Valor = obj.Sol_Mostrar_Actividades.Where(cd => cd.SupervisorId == CodigoSupervisor).FirstOrDefault();

            if (Valor != null)
            {
                CdgActividad = true;
            }
            else
            {
                CdgActividad = false;
            }


            return CdgActividad;

        }

        #region MostrarCantidaddeSolicitud
        private void MostrarCantidaddeSolicitud(int CodigoEmpleado)
        {
            int Cdg = 0;

            Cdg = CodigoEmpleado;

            var Cantidad = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoSolicitante == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

            if (Cantidad.Count > 0)
            {
                HttpContext.Session["Cantidad"] = Cantidad.Count();
            }
            else
            {
                HttpContext.Session["Cantidad"] = 0;
            }
        }
        #endregion

        #region MostrarCantidadAsignados
        private void MostrarCantidadAsignados(int CodigoEmpleado)
        {
            int Cdg = 0;

            Cdg = CodigoEmpleado;
            
            if (ValidarActividad(Cdg) == true)
            {
                List<ActividadDependencia> CdgTipoSolicitud = new List<ActividadDependencia>();

                CdgTipoSolicitud = new AsignadosController().CodigoActividad2(Cdg);

                var CantidadAsignado = new List<Vw_Mostrar_Usuario_Con_Su_Descripcion>();

                foreach (var item in CdgTipoSolicitud)
                {
                    if(item.Key == "Actividad")
                    {
                        var lista_solicitudes_por_actividad = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoEstadoSolicitud == 1 && cd.CodigoTipoSolicitud == item.Value && cd.CodigoTecnico == Cdg && cd.DependenciaId == 1).OrderByDescending(cd => cd.FechaCreacion).ToList();

                        foreach (var item2 in lista_solicitudes_por_actividad)
                        {
                            CantidadAsignado.Add(item2);
                        }
                    }
                    else if(item.Key == "Dependencia")
                    {
                        var lista_solicitudes_por_dependencia = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoEstadoSolicitud == 1 && cd.DependenciaId == item.Value && cd.CodigoTecnico == Cdg).OrderByDescending(cd => cd.FechaCreacion).ToList();

                        foreach (var item2 in lista_solicitudes_por_dependencia)
                        {
                            CantidadAsignado.Add(item2);
                        }
                    }
                }

                if (CantidadAsignado.Count > 0)
                {
                    HttpContext.Session["CantidadAsignado"] = CantidadAsignado.Count();
                }
                else
                {
                    HttpContext.Session["CantidadAsignado"] = 0;
                }
            }
            else
            {
                var CantidadAsignado = obj.Sol_Detalle_Lista.Where(cd => cd.CodigoTecnico == Cdg && cd.CodigoEstadoSolicitud == 1).ToList();

                if (CantidadAsignado.Count > 0)
                {

                    HttpContext.Session["CantidadAsignado"] = CantidadAsignado.Count();
                }
                else
                {
                    HttpContext.Session["CantidadAsignado"] = 0;
                }
            }
        }
        #endregion

        #region MostrarCantidadChat
        private void MostrarCantidadChat(int CodigoEmpleado)
        {
            int Cdg = 0;

            Cdg = CodigoEmpleado;

            var CantidadChat = obj.Sol_Mostrar_Lista_de_Asignacion_Chat.Where(cd => cd.Estado == true && cd.CodigoUsuario == Cdg).ToList();


            if (CantidadChat.Count > 0)
            {

                HttpContext.Session["CantidadChat"] = CantidadChat.Count();
            }
            else
            {
                HttpContext.Session["CantidadChat"] = 0;
            }
        }
        #endregion
    }
}