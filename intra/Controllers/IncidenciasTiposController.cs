﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra;

namespace intra.Controllers
{
    public class IncidenciasTiposController : Controller
    {
        private dbIntranet db = new dbIntranet();

        // GET: IncidenciasTipos
        [CustomAuthorize]
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso al Modulo de IncidenciasTipos ");
            //return View(db.IncidenciasTipos.ToList());
            return View(db.VistaTiposIncidencias.ToList());
        }






        // GET: IncidenciasTipos/Details/5
        [CustomAuthorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncidenciasTipos incidenciasTipos = db.IncidenciasTipos.Find(id);
            if (incidenciasTipos == null)
            {
                return HttpNotFound();
            }
            return View(incidenciasTipos);
        }

        // GET: IncidenciasTipos/Create
        [CustomAuthorize]
        public ActionResult Create()
        {
            ViewBag.SuplidorTipoes = new SelectList(db.Suplidores.ToList(), "SuplidorId", "SuplidorNombre");
            return View();
        }

        // POST: IncidenciasTipos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Create(FormCollection FC, [Bind(Include = "SuplidorId")] IncidenciasTipos it)
        {
            //IncidenciasTipos it = new IncidenciasTipos();
            bool save = false;
            it.IncidenciasTipoNombre = FC["IncidenciasTipoNombre"];
            int SuplidorId = Convert.ToInt16(FC["SuplidorId"]);

            if (ModelState.IsValid)
            {
                db.IncidenciasTipos.Add(it);
                db.SaveChanges();
                save = true;
                
            }

            if (save)
            { 
            var itID = db.IncidenciasTipos.Where(x => x.IncidenciasTipoNombre == it.IncidenciasTipoNombre).First().IncidenciasTipoId;

            SuplidorTipoes sp = new SuplidorTipoes();

            sp.IncidenciasTipoId = itID;
            sp.SuplidorId = SuplidorId;

            db.SuplidorTipoes.Add(sp);
            db.SaveChanges();

            ViewData["SuplidorTipoes"] = new SelectList(db.Suplidores.ToList(), "SuplidorId", "SuplidorNombre");

                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo un registro en el Modulo de IncidenciasTipos llamado("+it.IncidenciasTipoNombre+") ");
                return RedirectToAction("Index");

            }

            return View();
        }

        // GET: IncidenciasTipos/Edit/5
        [CustomAuthorize]
        public ActionResult Edit(int? id)
        {
            ViewBag.SuplidorTipoes = new SelectList(db.Suplidores.ToList(), "SuplidorId", "SuplidorNombre");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncidenciasTipos incidenciasTipos = db.IncidenciasTipos.Find(id);
            if (incidenciasTipos == null)
            {
                return HttpNotFound();
            }
            return View(incidenciasTipos);
        }

        // POST: IncidenciasTipos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Edit([Bind(Include = "IncidenciasTipoId,IncidenciasTipoNombre,SuplidorId")] IncidenciasTipos incidenciasTipos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(incidenciasTipos).State = EntityState.Modified;
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito un registro en el Modulo de IncidenciasTipos id(" + incidenciasTipos.IncidenciasTipoId + ") ");
                return RedirectToAction("Index");
            }
            ViewBag.SuplidorTipoes = new SelectList(db.Suplidores.ToList(), "SuplidorId", "SuplidorNombre");
            return View(incidenciasTipos);
        }

        // GET: IncidenciasTipos/Delete/5
        [CustomAuthorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncidenciasTipos incidenciasTipos = db.IncidenciasTipos.Find(id);
            if (incidenciasTipos == null)
            {
                return HttpNotFound();
            }
            return View(incidenciasTipos);
        }

        // POST: IncidenciasTipos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            IncidenciasTipos incidenciasTipos = db.IncidenciasTipos.Find(id);
            db.IncidenciasTipos.Remove(incidenciasTipos);
            db.SaveChanges();
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Elimino un registro en el Modulo de IncidenciasTipos llamado(" + incidenciasTipos.IncidenciasTipoNombre + ") ");
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
