﻿using intra.Models;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using intra.Models.BienesIncautados;

namespace intra.Controllers
{
    [Authorize(Roles = "BI,BIA,BIWR")]
    public class CasosController : Controller
    {
        private inv db = new inv();

        //[Authorize(Roles = "BI,BIA,BIWR")]
        public ActionResult Index()
        {
            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs r1 = new RegisterLogs(empId, "Ingreso al Modulo de Casos de BI");

            try
            {
                return View(db.INV_CASOS.Where(ca=> ca.ESTA_ID != 12).ToList());
            }
            catch (ArgumentNullException error)
            {
                error.ToString();
                return View();
            }

        }

        //[Authorize(Roles = "BI,BIA,BIWR")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            INV_CASOS iNV_CASOS = db.INV_CASOS.Find(id);

            if (iNV_CASOS == null)
            {
                return HttpNotFound();
            }

            string empId = HttpContext.Session["empleadoId"].ToString();
            RegisterLogs rl = new RegisterLogs(empId, "Ingreso en el Modulo de BI al Detalle del caso(" + iNV_CASOS.CASO_NOMBRE + ")");

            return View(iNV_CASOS);
        }

        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult Create()
        {
            ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 2).ToList(), "ESTA_ID", "ESTA_NOMBRE");

            using (dbIntranet dbs2 = new dbIntranet())
            {
                ViewBag.Fiscalia = new SelectList(dbs2.Recintos.ToList(), "recintoId", "recintoNombre");
            }

            return View();
        }

        // POST: casos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult Create(INV_CASOS iNV_CASOS)
        {
            iNV_CASOS.USUA_ID = Session["usuario"].ToString();

            if (ModelState.IsValid)
            {
                db.INV_CASOS.Add(iNV_CASOS);
                db.SaveChanges();

                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Creo un nuevo caso en el Modulo de BI llamado (" + iNV_CASOS.CASO_NOMBRE + ")");

                return RedirectToAction("Index");
            }

            ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 2).ToList(), "ESTA_ID", "ESTA_NOMBRE");

            using (dbIntranet dbs2 = new dbIntranet())
            {
                ViewBag.Fiscalia = new SelectList(dbs2.Recintos.ToList(), "recintoId", "recintoNombre");
            }

            return View(iNV_CASOS);
        }

        //[Authorize(Roles = "BIA,BIWR")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            INV_CASOS iNV_CASOS = db.INV_CASOS.FirstOrDefault(ca=>ca.CASO_ID == id && ca.ESTA_ID != 12);
            
            if (iNV_CASOS == null)
            {
                return HttpNotFound();
            }

            iNV_CASOS.CASO_INCAUTACIONES = db.VISTA_INCAUTACIONES.Where(x => x.CASO_NOMBRE == iNV_CASOS.CASO_NOMBRE && x.INCA_ESTATUS!=12).ToList();

            ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 2).ToList(), "ESTA_ID", "ESTA_NOMBRE");

            using (dbIntranet dbs2 = new dbIntranet())
            {
                ViewBag.Fiscalia = new SelectList(dbs2.Recintos.ToList(), "recintoId", "recintoNombre");
            }
            return View(iNV_CASOS);
        }

        //[Authorize(Roles = "BIA,BIWR")]
        public ActionResult ReporteCaso(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            INV_CASOS iNV_CASOS = db.INV_CASOS.FirstOrDefault(ca => ca.CASO_ID == id && ca.ESTA_ID != 12);

            if (iNV_CASOS == null)
            {
                return HttpNotFound();
            }

            var templist = db.VISTA_INCAUTACIONES.Where(x => x.CASO_NOMBRE == iNV_CASOS.CASO_NOMBRE && x.INCA_ESTATUS!=12).ToList();

            //templist.AddRange(templist);

            iNV_CASOS.CASO_INCAUTACIONES = templist;

            //ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 2).ToList(), "ESTA_ID", "ESTA_NOMBRE");

            ViewBag.Estatus = db.INV_ESTATUS.Where(x => x.ESTA_ID == iNV_CASOS.ESTA_ID).FirstOrDefault().ESTA_NOMBRE;
            ViewBag.UsuarioImpresor = Session["usuario"].ToString();

            #region Codigo comentado para optimizar imagen
            //foreach (var item in iNV_CASOS.CASO_INCAUTACIONES)
            //{

            //    item.INV_FOTOS = db.INV_FOTOS.Where(x => x.INCA_NO == item.INCA_NO && x.FOTO_ESTATUS == true
            //    && (x.FOTO_TIPO.ToLower() == ".jpg" || x.FOTO_TIPO.ToLower() == ".jpeg"
            //    || x.FOTO_TIPO.ToLower() == ".png" || x.FOTO_TIPO.ToLower() == ".gif")).ToList();

            //    if (item.INV_FOTOS != null)
            //    {
            //        foreach (var imagen in item.INV_FOTOS)
            //        {
            //            imagen.FOTO_ARCHIVO = GetCompressedBitmap(ByteToImage(imagen.FOTO_ARCHIVO), 20L);
            //        }
            //    }
            //}
            #endregion

            using (dbIntranet dbs2 = new dbIntranet())
            {
                //ViewBag.Fiscalia = new SelectList(dbs2.Recintos.ToList(), "recintoId", "recintoNombre");
                ViewBag.Fiscalia = dbs2.Recintos.Find(iNV_CASOS.FISCALIA_ID).recintoNombre;
            }
            return new ViewAsPdf(iNV_CASOS)
            {
                PageSize = Rotativa.Options.Size.Letter,
            };
        }

        //[Authorize(Roles = "BIA,BIWR")]
        public ActionResult ReporteCasos(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            INV_CASOS iNV_CASOS = db.INV_CASOS.Find(id);

            if (iNV_CASOS == null)
            {
                return HttpNotFound();
            }

            iNV_CASOS.CASO_INCAUTACIONES = db.VISTA_INCAUTACIONES.Where(x => x.CASO_NOMBRE == iNV_CASOS.CASO_NOMBRE).ToList();

            //ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 2).ToList(), "ESTA_ID", "ESTA_NOMBRE");

            ViewBag.Estatus = db.INV_ESTATUS.Where(x => x.ESTA_ID == iNV_CASOS.ESTA_ID).FirstOrDefault().ESTA_NOMBRE;
            ViewBag.UsuarioImpresor = Session["usuario"].ToString();

            using (dbIntranet dbs2 = new dbIntranet())
            {
                //ViewBag.Fiscalia = new SelectList(dbs2.Recintos.ToList(), "recintoId", "recintoNombre");
                ViewBag.Fiscalia = dbs2.Recintos.Find(iNV_CASOS.FISCALIA_ID).recintoNombre;
            }
            return View("ReporteCaso",iNV_CASOS);
        }

        // POST: casos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "BIA,BIWR")]
        public ActionResult Edit(INV_CASOS iNV_CASOS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iNV_CASOS).State = EntityState.Modified;
                db.SaveChanges();

                string empId = HttpContext.Session["empleadoId"].ToString();
                RegisterLogs rl = new RegisterLogs(empId, "Edito en el Modulo de BI el caso id (" + iNV_CASOS.CASO_ID + ")");

                return RedirectToAction("Index");
            }

            ViewBag.Estatus = new SelectList(db.INV_ESTATUS.Where(x => x.ESTA_TIPO == 2).ToList(), "ESTA_ID", "ESTA_NOMBRE");

            using (dbIntranet dbs2 = new dbIntranet())
            {
                ViewBag.Fiscalia = new SelectList(dbs2.Recintos.ToList(), "recintoId", "recintoNombre");
            }

            return View(iNV_CASOS);
        }

        [HttpPost]
        [Authorize(Roles = "BIA")]
        public JsonResult Delete(int? id)
        {
            try
            {
                INV_CASOS INV_CASO = db.INV_CASOS.Find(id);

                INV_CASO.ESTA_ID = 12;

                var incautaciones = db.INV_INCAUTACIONES.Where(x => x.CASO_ID == INV_CASO.CASO_ID && x.INCA_ESTATUS != 12).ToList();

                foreach (var item in incautaciones) {
                    item.INCA_ESTATUS = 12;
                }

                var inca_ids = incautaciones.Select(x => x.INCA_NO).ToList();

                var fotos = db.INV_FOTOS.Where(x => inca_ids.Contains(x.INCA_NO) && x.FOTO_ESTATUS != null).ToList();

                foreach (var item in fotos)
                {
                    item.FOTO_ESTATUS = null;
                }

                db.SaveChanges();

                return Json("Eliminado");
            }
            catch (Exception e)
            {
                e.ToString();
                return Json("Error");
            }

        }

        #region Codigo para optimizar imagen
        //private ImageCodecInfo GetEncoder(ImageFormat format)
        //{
        //    ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

        //    foreach (ImageCodecInfo codec in codecs)
        //    {
        //        if (codec.FormatID == format.Guid)
        //        {
        //            return codec;
        //        }
        //    }
        //    return null;
        //}

        //public byte[] ImageToByteArray(System.Drawing.Image x)
        //{
        //    ImageConverter _imageConverter = new ImageConverter();
        //    byte[] xByte = (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));
        //    return xByte;
        //}

        //public static Image ByteToImage(byte[] archivo)
        //{
        //    using (MemoryStream mStream = new MemoryStream())
        //    {
        //        mStream.Write(archivo, 0, archivo.Length);
        //        mStream.Seek(0, SeekOrigin.Begin);

        //        Bitmap bm = new Bitmap(mStream);
        //        return bm;
        //    }
        //}

        //public byte[] GetCompressedBitmap(Image bmp, long quality)
        //{
        //    using (var mss = new MemoryStream())
        //    {
        //        Bitmap bit = new Bitmap(bmp);

        //        EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
        //        ImageCodecInfo jgpEncoder = GetEncoder(ImageFormat.Jpeg);
        //        EncoderParameters parameters = new EncoderParameters(1);
        //        parameters.Param[0] = qualityParam;
        //        bit.Save(mss, jgpEncoder, parameters);
        //        return mss.ToArray();
        //    }
        //}
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}