﻿using intra.Models.GestionHumana;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace intra.Controllers
{
    public class CulturaOrganizacionalController : Controller
    {
        private dbIntranet db = new dbIntranet();
        // GET: CulturaOrganizacional
        public ActionResult Index()
        {
            var culorgtipo = db.CulturaOrganizacional.ToList();
            ViewBag.tipos = new List<CulturaOrganizacional>();
            ViewBag.tipos.Add(new CulturaOrganizacional { CulOrgTipoId = 3, CulOrgNombre = "VALORES" });
            ViewBag.tipos.Add(new CulturaOrganizacional { CulOrgTipoId = 4, CulOrgNombre = "BENEFICIOS" });
            ViewBag.tipos.Add(new CulturaOrganizacional { CulOrgTipoId = 5, CulOrgNombre = "SERVICIOS DE GESTION HUMANA" });
            return View(culorgtipo);
        }

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public ActionResult Edit(int? Id)
        {
            if (Id == null)
            {
                return RedirectToAction("Index");
            }
            CulturaOrganizacional cultura = db.CulturaOrganizacional.Find(Id);
            if (cultura == null)
            {
                return HttpNotFound();
            }
            ViewBag.Tipos = new SelectList(db.CulturaOrganizacionalTipo.Where(x => x.CulOrgTipoId > 2).ToList(), "CulOrgTipoId", "CulOrgTipoNombre", cultura.CulOrgTipoId);
            return View(cultura);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public ActionResult Edit([Bind(Include = "CulOrgId,CulOrgNombre,CulOrgDescripcion,CulOrgTipoId,CulOrgFhCreacion")]CulturaOrganizacional cultura)
        {
            if (ModelState.IsValid)
            {
                cultura.CulOrgFhModificacion = DateTime.Now;
                db.Entry(cultura).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            cultura.CulturaOrganizacionalTipo = db.CulturaOrganizacionalTipo.Find(cultura.CulOrgTipoId);
            ViewBag.Tipos = new SelectList(db.CulturaOrganizacionalTipo.Where(x => x.CulOrgTipoId > 2).ToList(), "CulOrgTipoId", "CulOrgTipoNombre", cultura.CulOrgTipoId);
            return View(cultura);
        }

        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public ActionResult Create(int? id)
        {
            if (id == 3 || id == 4 || id == 5)
            {
                ViewBag.Id = id;
                ViewBag.Tipos = new SelectList(db.CulturaOrganizacionalTipo.Where(x => x.CulOrgTipoId > 2).ToList(), "CulOrgTipoId", "CulOrgTipoNombre", id);
                ViewBag.Nombre = db.CulturaOrganizacionalTipo.Where(x => x.CulOrgTipoId == id).FirstOrDefault().CulOrgTipoNombre;
                return View();
            }
            else if (id == null) {
                ViewBag.Tipos = new SelectList(db.CulturaOrganizacionalTipo.Where(x => x.CulOrgTipoId > 2).ToList(), "CulOrgTipoId", "CulOrgTipoNombre", id);
                ViewBag.Nombre = "AGREGAR CULTURA ORGANIZACIONAL";
                return View();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        [Authorize(Roles = "RRHHA")]
        public ActionResult Create([Bind(Include = "CulOrgId,CulOrgNombre,CulOrgDescripcion,CulOrgTipoId")]CulturaOrganizacional cultura)
        {
            if (cultura.CulOrgTipoId == 3 || cultura.CulOrgTipoId == 4 || cultura.CulOrgTipoId == 5)
            {
                if (ModelState.IsValid)
                {
                    cultura.CulOrgFhCreacion = cultura.CulOrgFhModificacion = DateTime.Now;
                    db.CulturaOrganizacional.Add(cultura);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.Id = cultura.CulOrgTipoId;
                ViewBag.Nombre = db.CulturaOrganizacionalTipo.Where(x => x.CulOrgTipoId == cultura.CulOrgTipoId).FirstOrDefault().CulOrgTipoNombre;
                ViewBag.Tipos = new SelectList(db.CulturaOrganizacionalTipo.Where(x => x.CulOrgTipoId > 2).ToList(), "CulOrgTipoId", "CulOrgTipoNombre", cultura.CulOrgTipoId);
                return View();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult Eliminar(int? id)
        {
            if (id == null || id < 3)
            {
                return Json("Error");
            }
            CulturaOrganizacional culorg = db.CulturaOrganizacional.Find(id);
            if (culorg == null)
            {
                return Json("Error");
            }
            var tipo = culorg.CulOrgTipoId;
            db.CulturaOrganizacional.Remove(culorg);
            db.SaveChanges();
            return Json(tipo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}