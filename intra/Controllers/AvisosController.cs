﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using PagedList;
using System.Xml.Linq;
using intra.Models.AvisosActividades;
using intra.Models.GestionHumana;
using intra.Models.ViewModel;
using System.Drawing;

namespace intra.Controllers
{
    public class AvisosController : Controller
    {
        private dbIntranet db = new dbIntranet();

        public ActionResult Index(int? actividad)
        {
            string EmpId = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                {
                    EmpId = HttpContext.Session["empleadoId"].ToString();
                }
                else
                {
                    EmpId = "public_user";
                }
            }
            catch (Exception)
            {
                EmpId = "public_user";
            }

            ViewBag.access = EmpId;

            var avisos_tipos = new List<AvisosTipos>();

            if (actividad != null)
            {
                avisos_tipos = db.AvisosTipos.Where(x => x.AvisoTipoId == actividad).ToList();

                if (avisos_tipos.Count == 1)
                {
                    ViewBag.avisos_tipoID = actividad;
                    ViewBag.avisos_tipo = avisos_tipos.FirstOrDefault().AvisoTipoNombre;

                    return View(avisos_tipos);
                }
            }

            avisos_tipos = db.AvisosTipos.ToList();

            return View(avisos_tipos);

        }

        public ActionResult Vista(int? page, int? actividad)
        {
            string EmpId = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                {
                    EmpId = HttpContext.Session["empleadoId"].ToString();
                }
                else
                {
                    EmpId = "public_user";
                }
            }
            catch (Exception)
            {
                EmpId = "public_user";
            }

            ViewBag.access = EmpId;

            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }

            //int pageSize = 4;
            int pageNumber = (page ?? 1);
            var avisos_tipos = db.AvisosTipos.ToList();

            if (actividad != null)
            {
                ViewBag.avisos_tipoID = actividad;
                ViewBag.avisos_tipo = db.AvisosTipos.Find(actividad).AvisoTipoNombre;
            }

            return View(avisos_tipos);
        }

        // GET: Avisos
        public ActionResult IndexViejo(int? page, int? actividad)
        {
            string EmpId = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
                {
                    EmpId = HttpContext.Session["empleadoId"].ToString();
                }
                else
                {
                    EmpId = "public_user";
                }
            }
            catch (Exception)
            {
                EmpId = "public_user";
            }

            ViewBag.access = EmpId;

            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }

            int pageSize = 4;
            int pageNumber = (page ?? 1);
            var avisos = db.Avisos.Where(x=> x.AvisosEstatus == 1 && ( x.AvisoTipoId == actividad || actividad == null) ).OrderByDescending(c => c.AvisosId);

            if (actividad != null && avisos.Count() > 0) {

                ViewBag.avisos_tipoID = actividad;
                ViewBag.avisos_tipo = db.AvisosTipos.Find(actividad).AvisoTipoNombre;

            }

            RegisterLogs rl = new RegisterLogs(EmpId, "Ingreso al Modulo de Avisos");
            return View(avisos.ToPagedList(pageNumber, pageSize));
        }

        public JsonResult CargarAviso(int AvisosId, bool Visita) {

            try
            {
                var aviso = db.Avisos.Where(x => x.AvisosId == AvisosId).FirstOrDefault();

                if (Visita) { aviso.AvisosVistas = aviso.AvisosVistas + 1; db.SaveChanges(); }

                var avisoVM = new AvisosVM()
                {
                    AvisosId = aviso.AvisosId,
                    AvisosTitulo = aviso.AvisosTitulo,
                    AvisosImagen = aviso.AvisosImagen,
                    AvisosFechaCreacion = aviso.AvisosFechaCreacion,
                    AvisosVistas = aviso.AvisosVistas,
                    AvisosCuerpo = aviso.AvisosCuerpo,
                    AvisosIcono = IconoImagen(aviso)
                };

                return Json(avisoVM);
            }
            catch (Exception ex)
            {
                ex.ToString();

                return Json("error");
            }
        }

        public JsonResult CargarAvisos(bool todos)
        {
            try
            {
                var avisos = db.Avisos.Where(x => (x.AvisosEstatus == 1 || todos)).Select(aviso => new AvisosVM() {

                    AvisosId = aviso.AvisosId,
                    AvisosTitulo = aviso.AvisosTitulo,
                    AvisosImagen = aviso.AvisosImagen,
                    AvisosFechaCreacion = aviso.AvisosFechaCreacion,
                    AvisosVistas = aviso.AvisosVistas,
                    AvisosCuerpo = aviso.AvisosCuerpo,
                    AvisosIcono = IconoImagen(aviso)

                }).ToList();

                // Load image
                Image image = Image.FromFile("my_sample_image.jpg");
                // Create graphics from image
                Graphics graphics = Graphics.FromImage(image);
                // Create font
                Font font = new Font("Times New Roman", 42.0f);
                // Create text position
                PointF point = new PointF(150, 150);


                return Json(avisos);
            }
            catch (Exception ex)
            {
                ex.ToString();

                return Json("error");
            }
        }

        [CustomAuthorize]
        public ActionResult vNovedadesGestionHumana()
        {
            string EmpId = string.Empty;


            ViewBag.access = EmpId;

            //if (Request.HttpMethod != "GET")
            //{
            //    page = 1;
            //}
            //int pageSize = 4;
            //int pageNumber = (page ?? 1);
            var avisos = db.Avisos.OrderByDescending(c => c.AvisosId).Take(5);

            RegisterLogs rl = new RegisterLogs(EmpId, "Ingreso al Modulo de Avisos");
            return View(avisos.ToList());
        }


        [CustomAuthorize]
        public PartialViewResult Novedades(/*int? page*/)
        {
            string EmpId = string.Empty;

            //try
            //{
            //    if (!string.IsNullOrEmpty(HttpContext.Session["empleadoId"].ToString()))
            //    {
            //        EmpId = HttpContext.Session["empleadoId"].ToString();
            //    }
            //    else
            //    {
            //        EmpId = "public_user";
            //    }
            //}
            //catch (Exception)
            //{
            //    EmpId = "public_user";
            //}

            ViewBag.access = EmpId;

            //if (Request.HttpMethod != "GET")
            //{
            //    page = 1;
            //}
            //int pageSize = 4;
            //int pageNumber = (page ?? 1);
            var avisos = db.Avisos.OrderByDescending(c => c.AvisosId).Take(5);
            /*string empId = HttpContext.Session["empleadoId"].ToString();*/

            RegisterLogs rl = new RegisterLogs(EmpId, "Ingreso al Modulo de Avisos");
            return PartialView(avisos.ToList());
        }

        [CustomAuthorize]
        [Authorize(Roles = "AV_RH")]
        public ActionResult Listado()
        {
            return View(db.Avisos.OrderByDescending(x=>x.AvisosId).ToList());
        }
        //Listado Tipo Novedades

        public PartialViewResult vActividadesGestionHumana()
        {
            
            return PartialView();
        }


        //[CustomAuthorize]
        public PartialViewResult ListaAvisos()
        {
            return PartialView(db.Avisos.Where(x=> x.AvisosEstatus != 0).OrderByDescending(x=>x.AvisosId).ToList());
        }

        [CustomAuthorize]
        public PartialViewResult vPartialPrueba()
        {


            WebRequest webRequest = WebRequest.Create("http://evangeliodeldia.org/rss/v2/evangelizo_rss-sp.xm");
            webRequest.Proxy = new WebProxy("http://172.18.1.9:8080/", true);

            XDocument doc = XDocument.Load("http://evangeliodeldia.org/rss/v2/evangelizo_rss-sp.xml");

            string nowFecha = DateTime.Now.ToShortDateString();
            string linkAbsoluto = "main.php?language=SP&module=readings&localdate=" + nowFecha;

            var query = (from feed in doc.Descendants("item")
                         select new Feed
                         {
                             titulo = feed.Element("title").Value,
                             link = feed.Element("link").Value + linkAbsoluto,
                             descripcion = feed.Element("description").Value,

                         }).FirstOrDefault();

            return PartialView(query);
        }

        
        public ActionResult Detalle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Avisos avisos = db.Avisos.Find(id);
            if (avisos == null)
            {
                return HttpNotFound();
            }
            return View(avisos);
        }

        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            string EmpId = string.Empty;
            if (HttpContext.Session["empleadoId"] == null) { return RedirectToAction("Index", "login", new { url = Request.Url.AbsoluteUri }); }

            try
            {
                Avisos avisos = db.Avisos.Find(id);

                ViewBag.access = EmpId;

                avisos.AvisosVistas = avisos.AvisosVistas + 1;

                RegisterLogs rl = new RegisterLogs(EmpId, "Ingreso al Detalle de Avisos llamado (" + avisos.AvisosTitulo + ")");
                db.SaveChanges();

                ViewBag.IconImage = IconoImagen(avisos);

                ViewBag.avisos_tipoID = avisos.AvisoTipoId;

                ViewBag.avisos_tipo = avisos.AvisosTipo?.AvisoTipoNombre;

                return View(avisos);
            }
            catch (Exception)
            {
                return View();
            }
        }

        [CustomAuthorize]
        public ActionResult v_NovedadesHome()
        {
            var avisos = db.Avisos.OrderByDescending(c => c.AvisosId).Take(5);
            return View(avisos);
        }




        [CustomAuthorize]
        [Authorize(Roles = "AV_RH")]
        // GET: Avisos/Create
        public ActionResult Create()
        {
            ViewBag.AvisoTipoId = new SelectList(db.AvisosTipos, "AvisoTipoId", "AvisoTipoNombre").ToList();
            

            return View();
        }
      

        // POST: Avisos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "AV_RH")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HttpPostedFileBase file,[Bind(Include = "AvisosId,AvisosTitulo,AvisosCuerpo,AvisosFechaCreacion,AvisosAutor,AvisosImagen,AvisosVistas,AvisosEstatus,AvisosDocumentos,AvisosDocExtension,DocGHArchivoVista")] Avisos avisos, FormCollection fc)
        {
            string directory = HttpRuntime.AppDomainAppVirtualPath + "/Content/Avisos/";
            try
            {
                // string directory = string.Concat(System.Configuration.ConfigurationManager.AppSettings["intranet"], "Content/Avisos/");

                //"~/Content/NewsImagenes/";
                avisos.AvisosFechaCreacion = DateTime.Now;
                avisos.AvisosAutor = Session["usuario"].ToString();
              

                if (file != null && file.ContentLength > 0)
                {
                    string path = Path.Combine(Server.MapPath(directory), Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    avisos.AvisosImagen = file.FileName;
                }

                if (avisos.DocGHArchivoVista != null)
                { 
                    avisos.AvisosDocExtension = Path.GetExtension(avisos.DocGHArchivoVista.FileName);
                     using (MemoryStream ms = new MemoryStream())
                    {
                        avisos.DocGHArchivoVista.InputStream.CopyTo(ms);
                        avisos.AvisosDocumentos = ms.GetBuffer();
                    }
                }
                else
                {
                    avisos.AvisosDocumentos = new byte[0];
                }

                    avisos.AvisoTipoId = int.Parse(fc["AvisoTipoId"]);
                    avisos.AvisosCuerpo = fc["__contenido"];
                    avisos.AvisosEstatus = 1;
                    db.Avisos.Add(avisos);
                    db.SaveChanges();
                    string empId = HttpContext.Session["empleadoId"].ToString();
                    RegisterLogs rl = new RegisterLogs(empId, "Creo un nuevo Aviso llamado(" + avisos.AvisosTitulo + ")");
                    return RedirectToAction("Index", new { actividad = avisos.AvisoTipoId });

            }catch (Exception ex)
            {
                ex.ToString();
                ViewBag.AvisoTipoId = new SelectList(db.AvisosTipos, "AvisoTipoId", "AvisoTipoNombre").ToList();
                return View();
            }

        }

        [CustomAuthorize]
        [Authorize(Roles = "AV_RH")]
        // GET: Avisos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           
            Avisos avisos = db.Avisos.Find(id);
            ViewBag.AvisoTipoId = new SelectList(db.AvisosTipos, "AvisoTipoId", "AvisoTipoNombre", avisos.AvisoTipoId).ToList();
            if (avisos == null)
            {
                return HttpNotFound();
            }

            ViewBag.IconImage = IconoImagen(avisos);
            return View(avisos);
        }
        
        // POST: Avisos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "AV_RH")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HttpPostedFileBase file,[Bind(Include = "AvisosId,AvisosTitulo,AvisosCuerpo,AvisosFechaCreacion,AvisosAutor,AvisosImagen,AvisosVistas,AvisosEstatus,DocGHArchivoVista")] Avisos avisos, FormCollection fc)
        {
            int idAvisos = int.Parse(fc["Id"]);
            var avisos1 = db.Avisos.Where(x => x.AvisosId == idAvisos).FirstOrDefault();

            string directory = HttpRuntime.AppDomainAppVirtualPath + "/Content/Avisos/";
               // string.Concat(System.Configuration.ConfigurationManager.AppSettings["intranet"], "Content/Avisos/");

            if (file != null && file.ContentLength > 0)
            {

                string path = Path.Combine(Server.MapPath(directory), Path.GetFileName(file.FileName));
                file.SaveAs(path);
                avisos1.AvisosImagen = file.FileName;
            }
            else
            {
                avisos1.AvisosImagen = avisos1.AvisosImagen;
            }


            if (avisos.DocGHArchivoVista != null)
            {
                avisos1.AvisosDocExtension = Path.GetExtension(avisos.DocGHArchivoVista.FileName);
                using (MemoryStream ms = new MemoryStream())
                {
                    avisos1.DocGHArchivoVista.InputStream.CopyTo(ms);
                    avisos1.AvisosDocumentos = ms.GetBuffer();
                }
            }
            else
            {
                avisos1.AvisosDocumentos = avisos1.AvisosDocumentos;
            }
                avisos1.AvisoTipoId = int.Parse(fc["AvisoTipoId"]);
                avisos1.AvisosCuerpo = fc["__contenido"];
                avisos1.AvisosTitulo = fc["AvisosTitulo"];
                avisos1.AvisosVistas = avisos.AvisosVistas + 1;
                avisos1.AvisosEstatus = 1;
                avisos1.AvisosFechaCreacion = DateTime.Now;
              
                db.SaveChanges();
                string empId = HttpContext.Session["empleadoId"].ToString();
            ViewBag.NovedadId = new SelectList(db.AvisosTipos, "AvisoTipoId", "AvisoTipoNombre", avisos.AvisoTipoId).ToList();

            RegisterLogs rl = new RegisterLogs(empId, "Edito el Aviso id("+ avisos1.AvisosId+")");
                return RedirectToAction("Index");
        }

        // GET: Avisos/Delete/5
        [CustomAuthorize]
        [Authorize(Roles = "AV_RH")]
        public JsonResult DesactivarAvisos(int? id)
        {
            Avisos avisos = db.Avisos.Find(id);
            if (id == null)
            {
                return Json(new { success = false, Mensaje = "No existe un aviso con este id", JsonRequestBehavior.AllowGet });
            }
           
          
            if (avisos == null)
            {
             return Json(new { success = false, Mensaje = "Error No se pudo Eliminar este Archivo", JsonRequestBehavior.AllowGet });

            }
            else
            {
                avisos.AvisosEstatus = 0;
                db.SaveChanges();
                return Json( new { success = true, Mensaje = "Registro Desactivado", JsonRequestBehavior.AllowGet });

            }
          
        }


        [CustomAuthorize]
        [Authorize(Roles = "AV_RH")]
        public JsonResult ActivarAvisos(int? id)
        {
            Avisos avisos = db.Avisos.Find(id);
            if (id == null)
            {
                return Json(new { success = false, Mensaje = "No existe un aviso con este id", JsonRequestBehavior.AllowGet });
            }


            if (avisos == null)
            {
                return Json(new { success = false, Mensaje = "Error No se pudo Eliminar este Archivo", JsonRequestBehavior.AllowGet });

            }
            else
            {
                avisos.AvisosEstatus = 1;
                db.SaveChanges();
                return Json(new { success = true, Mensaje = "Registro Activado", JsonRequestBehavior.AllowGet });

            }

        }

        

        private string IconoImagen(Avisos Avisos)
        {
                if(Avisos.AvisosDocumentos != null) { 
                string image64 = Convert.ToBase64String(Avisos.AvisosDocumentos);
                if(!string.IsNullOrEmpty(image64))
                { 
                if (Avisos.AvisosDocExtension.ToLower() == ".jpg" || Avisos.AvisosDocExtension.ToLower() == ".jpeg")
                {
                    return string.Format("data:image/jpg;base64,{0}", image64);
                }
                else if (Avisos.AvisosDocExtension.ToLower() == ".png")
                {
                    return string.Format("data:image/png;base64,{0}", image64);
                }
                else if (Avisos.AvisosDocExtension.ToLower() == ".gif")
                {
                    return string.Format("data:image/gif;base64,{0}", image64);
                }
                else if (Avisos.AvisosDocExtension.ToLower() == ".pdf")
                {
                    return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/pdf.png";
                }
                else if (Avisos.AvisosDocExtension.ToLower() == ".doc" || Avisos.AvisosDocExtension.ToLower() == ".docx")
                {
                    return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/doc.png";
                }
                else if (Avisos.AvisosDocExtension.ToLower() == ".xls" || Avisos.AvisosDocExtension.ToLower() == ".xlsx")
                {
                    return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/xls.png";
                }
                else if (Avisos.AvisosDocExtension.ToLower() == ".ppt" || Avisos.AvisosDocExtension.ToLower() == ".pptx")
                {
                    return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/ppt.png";
                }
                else if (Avisos.AvisosDocExtension.ToLower() == ".txt")
                {
                    return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/txt.png";
                }
                else if (Avisos.AvisosDocExtension.ToLower() == ".zip")
                {
                    return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/zip.png";
                }
                else
                {
                    return HttpRuntime.AppDomainAppVirtualPath + "/Content/images/BI_Images/file.png";
                }
                
            }
            else
            {
                return "";
            }
            }else
            {
                return "";

            }

        }


        [HttpGet]
        [AllowAnonymous]
        public FileResult DownloadFile(int id)
        {
            var file = db.Avisos.Find(id);

            
            return File(file.AvisosDocumentos, file.AvisosDocExtension, file.AvisosTitulo + file.AvisosDocExtension);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

