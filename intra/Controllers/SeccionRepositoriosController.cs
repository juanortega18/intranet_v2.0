﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using intra.Models.Repositorio;
using intra.Models;

namespace intra.Controllers
{
    public class SeccionRepositoriosController : Controller
    {
        private RepositorioDB db = new RepositorioDB();

        [Authorize(Roles = "REP (Admin)")]
        // GET: SeccionRepositorios
        public ActionResult Index()
        {
            var list = db.SeccionRepositorio.OrderBy(r => r.SeccionRepositorioDescripcion).ToList();
            return View(list);
            //return View(db.SeccionRepositorio.ToList());
        }

        public ActionResult Repositorio(int? id)
        {
           if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var subRepositorio = db.SubSeccionRepositorio.Where(r => r.SeccionRepositorioId == id).ToList();
            
            return View(subRepositorio);
        }

        [Authorize(Roles = "BI,BIA,BIWR")]
        // GET: SeccionRepositorios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SeccionRepositorios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SeccionRepositorioId,SeccionRepositorioDescripcion")] SeccionRepositorio seccionRepositorio)
        {
            if (ModelState.IsValid)
            {
                db.SeccionRepositorio.Add(seccionRepositorio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(seccionRepositorio);
        }

        // GET: SeccionRepositorios/Edit/5
        [Authorize(Roles = "BI,BIA,BIWR")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeccionRepositorio seccionRepositorio = db.SeccionRepositorio.Find(id);
            if (seccionRepositorio == null)
            {
                return HttpNotFound();
            }
            return View(seccionRepositorio);
        }

        // POST: SeccionRepositorios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SeccionRepositorioId,SeccionRepositorioDescripcion")] SeccionRepositorio seccionRepositorio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(seccionRepositorio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(seccionRepositorio);
        }
    }
}
