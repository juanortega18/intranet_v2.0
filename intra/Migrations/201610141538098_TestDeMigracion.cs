namespace intra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestDeMigracion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Asignacion_Flota",
                c => new
                    {
                        Id_Asignacion = c.Int(nullable: false, identity: true),
                        Modelo = c.String(maxLength: 100),
                        Marca = c.String(maxLength: 100),
                        IMEI = c.String(nullable: false, maxLength: 60),
                        SIM_Numeracion = c.String(nullable: false, maxLength: 50),
                        Despachado = c.String(nullable: false, maxLength: 200),
                        No_Flota = c.String(nullable: false, maxLength: 50),
                        Usuario = c.String(maxLength: 100),
                        Departamento = c.String(maxLength: 300),
                        Fecha_Creacion = c.DateTime(nullable: false),
                        Telefono = c.String(maxLength: 50),
                        cedula = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id_Asignacion);
            
            CreateTable(
                "dbo.IncidenciasServicios",
                c => new
                    {
                        IncidenciasId = c.Int(nullable: false, identity: true),
                        IncidenciasFechaCreacion = c.DateTime(nullable: false),
                        IncidenciasUsuario = c.String(maxLength: 80),
                        IncidenciasSolucion = c.String(maxLength: 300),
                        IncidenciasFechaSolucion = c.DateTime(nullable: false),
                        IncidenciasEstatus = c.Boolean(nullable: false),
                        SuplidorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IncidenciasId);
            
            CreateTable(
                "dbo.Noticias",
                c => new
                    {
                        NoticiaId = c.Int(nullable: false, identity: true),
                        NoticiaTitulo = c.String(),
                        NoticiaCuerpo = c.String(),
                        NoticiaImages = c.String(),
                        NoticiaPieImages = c.String(),
                        NoticiaFechaCreacion = c.DateTime(nullable: false),
                        NoticiaVistas = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.NoticiaId);
            
            CreateTable(
                "dbo.RolesUsuarios",
                c => new
                    {
                        UserName = c.String(nullable: false, maxLength: 128),
                        Rol = c.String(nullable: false, maxLength: 128),
                        ruFecha = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserName, t.Rol });
            
            CreateTable(
                "dbo.SP_USERS_LDAP",
                c => new
                    {
                        samAccountName = c.String(nullable: false, maxLength: 128),
                        employeeid = c.String(),
                        department = c.String(),
                        displayName = c.String(),
                    })
                .PrimaryKey(t => t.samAccountName);
            
            CreateTable(
                "dbo.Suplidores",
                c => new
                    {
                        SuplidorId = c.Int(nullable: false, identity: true),
                        SuplidorNombre = c.String(maxLength: 100),
                        SuplidorDireccion = c.String(maxLength: 300),
                        SuplidorTelefono = c.String(maxLength: 50),
                        SuplidorUsuario = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.SuplidorId);
            
            CreateTable(
                "dbo.VISTA_EMPLEADOS",
                c => new
                    {
                        empleadoid = c.String(nullable: false, maxLength: 128),
                        nombre = c.String(),
                        telefono1 = c.String(),
                        cedula = c.String(),
                        cargoid = c.String(),
                        departamentoid = c.String(),
                        fechaingreso = c.DateTime(nullable: false),
                        id_persona = c.Int(nullable: false),
                        cargo = c.String(),
                        dependencia = c.String(),
                        dependenciaid = c.String(),
                        departamento = c.String(),
                        salario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        estatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.empleadoid);
            
            CreateTable(
                "dbo.VISTA_LDAP",
                c => new
                    {
                        samAccountName = c.String(nullable: false, maxLength: 128),
                        employeeid = c.String(),
                        department = c.String(),
                        displayName = c.String(),
                        empleadoid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.samAccountName);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.VISTA_LDAP");
            DropTable("dbo.VISTA_EMPLEADOS");
            DropTable("dbo.Suplidores");
            DropTable("dbo.SP_USERS_LDAP");
            DropTable("dbo.RolesUsuarios");
            DropTable("dbo.Noticias");
            DropTable("dbo.IncidenciasServicios");
            DropTable("dbo.Asignacion_Flota");
        }
    }
}
