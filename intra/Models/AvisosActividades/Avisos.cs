﻿namespace intra.Models.AvisosActividades
{
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
    
    public class Avisos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AvisosId { get; set; }

        [Required(ErrorMessage = "Debe indicar el titulo")]
        [StringLength(200)]
        public string AvisosTitulo { get; set; }

        [Required(ErrorMessage = "Debe indicar el cuerpo")]
        public string AvisosCuerpo { get; set; }
        public DateTime AvisosFechaCreacion { get; set; }
        public string AvisosAutor { get; set; }

        public string AvisosImagen { get; set; }
        public int AvisosVistas { get; set; }

        public int AvisosEstatus { get; set; }
        public byte[] AvisosDocumentos { get; set; }
        public string AvisosDocExtension { get; set; }
        public int? AvisoTipoId { get; set; }

        public virtual AvisosTipos AvisosTipo { get; set; }

        [NotMapped]
        public HttpPostedFileBase DocGHArchivoVista { get; set; }
        [NotMapped]
        public string String64 { get; set; }


    }
}