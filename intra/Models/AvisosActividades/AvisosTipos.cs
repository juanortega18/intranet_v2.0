﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace intra.Models.AvisosActividades
{
    public class AvisosTipos
    {
        [Key]
        public int AvisoTipoId { get; set; }
        public string AvisoTipoNombre { get; set; }
        public virtual List<Avisos> Avisos { get; set; }
    }
}
