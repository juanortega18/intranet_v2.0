﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Models
{
    public class MyModel
    {
        public HttpPostedFileBase MyFile { get; set; }

        public string CroppedImagePath { get; set; }
    }
}