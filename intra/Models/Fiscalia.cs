namespace intra.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Fiscalia : DbContext
    {
        public Fiscalia()
            : base("name=Fiscalia")
        {
        }

        public virtual DbSet<Recinto> Recintos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Recinto>()
                .Property(e => e.recintoCodigo)
                .IsUnicode(false);

            modelBuilder.Entity<Recinto>()
                .Property(e => e.recintoNombre)
                .IsUnicode(false);

            modelBuilder.Entity<Recinto>()
                .Property(e => e.recintoTelefono1)
                .IsUnicode(false);

            modelBuilder.Entity<Recinto>()
                .Property(e => e.recintoTelefono2)
                .IsUnicode(false);

            modelBuilder.Entity<Recinto>()
                .Property(e => e.recintoJ2)
                .IsUnicode(false);

            modelBuilder.Entity<Recinto>()
                .Property(e => e.recintoJ21)
                .IsUnicode(false);

            modelBuilder.Entity<Recinto>()
                .Property(e => e.recintoPri)
                .IsUnicode(false);
        }
    }
}
