namespace intra.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Tb_Recintos")]
    public partial class Recinto
    {
        public int recintoId { get; set; }

        public string recintoCodigo { get; set; }

        public string recintoNombre { get; set; }

        public string recintoTelefono1 { get; set; }

        public string recintoTelefono2 { get; set; }

        public int? recintoTipoId { get; set; }

        public DateTime? recintoFecha { get; set; }

        public string recintoJ2 { get; set; }

        public string recintoJ21 { get; set; }

        public string recintoPri { get; set; }

        public int? distritoId { get; set; }
    }
}
