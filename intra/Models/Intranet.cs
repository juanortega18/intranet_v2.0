namespace intra
{
    using System;
    using System.Data.Entity;

    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Models;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using Models.Seguros;
    using Models.GestionHumana;
    using intra.Models.CartaProcedencia;
    using intra.Models.Encuestas;
    using intra.Models.RequisicionesCompra;
    using intra.Models.ViewModel;
    using intra.Models.Servicios;
    using intra.Models.ActualizacionDatos;
    using intra.Models.SeguridadInformatica;
    using intra.Models.PaseProduccion;
    using intra.Models.FlotasExtensiones;
    using intra.Models.PersonalSeguridad;
    using intra.Models.AvisosActividades;
    using intra.Models.GestionHumana.Permisos;
    using intra.Models.vw_Models;
    using intra.Models.Cumpleanos;
    using intra.Models.BienesIncautados;
    using intra.Models.RolesUsuarios;
    using intra.Models.Inventario;
    using intra.Models.SistemaRepatriados;
    using intra.Models.SistemaRepatriados.ViewModel;
    using intra.Models.ModuloVisitas;
    using intra.Models.GestionHumana.Sugerencias;
    using intra.Models.GestionHumana.CatalogoDescuento;
    using intra.Models.GestionHumana.ConcursoMadres;
    using intra.Models.GestionHumana.NuestroEquipo;

    public partial class dbIntranet : DbContext
    {
        public dbIntranet()
            : base("name=intra")
        {
        }

        //Requisiciones
        public virtual DbSet<vw_REQUISICION> vw_REQUISICION { get; set; }
        public virtual DbSet<vw_REQUISICIONGRUPOS> vw_REQUISICIONGRUPOS { get; set; }

        public virtual DbSet<FormularioEncuesta> FormularioEncuesta { get; set; }
        public virtual DbSet<EncuestaEstado> EncuestaEstado { get; set; }
        public virtual DbSet<PreguntasEncuesta> PreguntasEncuesta { get; set; }
        public virtual DbSet<VISTA_EMPLEADOS> VISTA_EMPLEADOS { get; set; }
        public virtual DbSet<EmpleadosContratados> empleadoscontratados { get; set; }
        //public virtual DbSet<vEmpleados> vEmpleados { get; set; }
        public virtual DbSet<SP_USERS_LDAP> SP_USERS_LDAP { get; set; }
        public virtual DbSet<VISTA_LDAP> VISTA_LDAP { get; set; }
        public virtual DbSet<Vw_Mostrar_Personal_Permisos_PGR> Vw_Mostrar_Personal_Permisos_PGR { get; set; }
        public virtual DbSet<vw_Dependencias> vw_Dependencias { get; set; }
        public virtual DbSet<vw_Departamentos> vw_Departamentos { get; set; }
        public virtual DbSet<vw_Cargos> vw_Cargos { get; set; }
        public virtual DbSet<vw_Divisiones> vw_Divisiones { get; set; }
        public virtual DbSet<vw_Secciones> vw_Secciones { get; set; }
        public virtual DbSet<vw_FormularioSolicitudServicios> vw_FormularioSolicitudServicios { get; set; }

        //public virtual DbSet<Vw_Mostrar_Informacion_Perfil_Domain> VistaLdap { get; set; }
        public virtual DbSet<RolesUsuarios> RolesUsuarios { get; set; }
        public virtual DbSet<IncidenciasServicios> IncidenciasServicios { get; set; }
        public virtual DbSet<IncidenciasTipos> IncidenciasTipos { get; set; }
        public virtual DbSet<IncidenciasAreas> IncidenciasAreas { get; set; }
        public virtual DbSet<SuplidorTipoes> SuplidorTipoes { get; set; }
        public virtual DbSet<Suplidores> Suplidores { get; set; }
        public virtual DbSet<Noticias> Noticias { get; set; }
        public virtual DbSet<Logs> Logs { get; set; }
        public virtual DbSet<MiPerfilAvatars> MiPerfilAvatars { get; set; }
        public virtual DbSet<Tutoriales> Tutoriales { get; set; }
        public virtual DbSet<Avisos> Avisos { get; set; }
        public virtual DbSet<AvisosTipos> AvisosTipos { get; set; }
        public virtual DbSet<ComplementoReporteSoporte> ComplementoReporteSoporte { get; set; }
        public virtual DbSet<AsignacionFlots> AsignacionFlots { get; set; }
        public virtual DbSet<EstatusFlotas> EstatusFlotas { get; set; }
        public virtual DbSet<Vw_Mostrar_Empleados_Cumpleanos> Vw_Mostrar_Empleados_Cumpleanos { get; set; }
        public virtual DbSet<FelicitacionesCumpleanos> FelicitacionesCumpleanos { get; set; }
        public virtual DbSet<EstadoCumpleanos> EstadoCumpleanos { get; set; }
        public virtual DbSet<ModeloFlotas> ModeloFlotas { get; set; }
        public virtual DbSet<MarcasFlotas> MarcasFlotas { get; set; }
        //public virtual DbSet<ForoInformativs> ForoInformativs { get; set; }
        public virtual DbSet<VistaIncidencias> VistaIncidencias { get; set; }
        public virtual DbSet<VistaTiposIncidencias> VistaTiposIncidencias { get; set; }
        public virtual DbSet<EstadosIncidencias> EstadosIncidencias { get; set; }
        public virtual DbSet<VelocidadInventario> VelocidadInventario { get; set; }
        public virtual DbSet<Recinto> Recintos { get; set; }
        public virtual DbSet<bible_fts_full> bible_fts_full { get; set; }
        public virtual DbSet<Citas> citas { get; set; }


        //Formulario personal de seguridad
        public virtual DbSet<FormularioPersonalSeguridads> FormularioPersonalSeguridads { get; set; }
        public virtual DbSet<Institucions> Institucions { get; set; }
        public virtual DbSet<Rangos> Rangos { get; set; }
        public virtual DbSet<RangoInstitucions> RangoInstitucions { get; set; }
        public virtual DbSet<Vista_FormularioSeguridad> Vista_FormularioSeguridad { get; set; }
        public virtual DbSet<NivelAcademico> NivelAcademico { get; set; }
        //public virtual DbSet<vw_Departamentos> vw_Departamentos { get; set; }
        //public virtual DbSet<TipoSangre> TipoSangre { get; set; }

      
        //Solicitud
        public DbSet<Aplicaciones> Aplicaciones { get; set; }
        public DbSet<EncargadoPaseProduccion> EncargadoPaseProduccion { get; set; }
        public DbSet<AreasImplicadasPaseProduccion> AreasImplicadasPaseProduccion { get; set; }
        public DbSet<CategoriasProduccion> CategoriasProduccion { get; set; }
        public DbSet<CategoriasAprobacionesProduccion> CategoriasAprobacionesProduccion { get; set; }
        public DbSet<AreasAprobacionesProduccion> AreasAprobacionesProduccion { get; set; }
        public DbSet<EncargadoSubirProduccion> EncargadoSubirProduccion { get; set; }
        public DbSet<AprobacioneServicios> AprobacioneServicios { get; set; }
        public DbSet<AprobacionesActividad> AprobacionesActividad { get; set; }
        public DbSet<AprobacionEstatus> AprobacionEstatus { get; set; }
        public DbSet<Sol_Registro_Solicitud> Sol_Registro_Solicitud { get; set; }
        public DbSet<Sol_Departamento> Sol_Departamento { get; set; }
        public DbSet<Sol_Actividades> Sol_Actividades { get; set; }
        public DbSet<Sol_Sub_Actividades> Sol_Sub_Actividades { get; set; }
        public DbSet<Sol_Tipo_Consulta_Solicitud> Sol_Tipo_Consulta_Solicitud { get; set; }
        public DbSet<Sol_Estado_Solicitud> Sol_Estado_Solicitud { get; set; }
        public DbSet<Sol_Otros_Tecnicos> Sol_Otros_Tecnicos { get; set; }
        public DbSet<Sol_TipodeAsistencia> Sol_TipodeAsistencia { get; set; }
        public DbSet<Vw_Mostrar_Personal_PGR> Sol_Mostrar_Personal_PGR { get; set; }
        public DbSet<Vw_Mostrar_Usuario_Con_Su_Descripcion> Sol_Detalle_Lista { get; set; }
        public DbSet<Vw_Mostrar_Otros_Tecnicos> Sol_Mostrar_Otros_Tecnicos { get; set; }
        public DbSet<Vw_Mostrar_Informacion_Perfil_Domain> Sol_Mostrar_Informacion_Perfil_Domain { get; set; }
        public DbSet<Wv_Mostrar_Log> Sol_Mostrar_Log { get; set; }
        public DbSet<Sol_Logs> Sol_Logs { get; set; }
        public DbSet<Sol_Chat> Sol_Chat { get; set; }
        public DbSet<Vw_Mostrar_Actividades> Sol_Mostrar_Actividades { get; set; }
        public DbSet<vw_Mostrar_Lista_Chat> Sol_Mostrar_Lista_Chat { get; set; }
        public DbSet<Vw_Mostrar_Lista_de_Asignacion_Chat> Sol_Mostrar_Lista_de_Asignacion_Chat { get; set; }
        public DbSet<Sol_Otros_Tecnicos_Por_Actividades> Sol_Otros_Tecnicos_Por_Actividades { get; set; }
        public DbSet<Sol_Tecnicos_Alternativos> Sol_Tecnicos_Alternativos { get; set; }
        public DbSet<Sol_DependenciasRegionales> Sol_DependenciasRegionales { get; set;}
        public DbSet<Sol_Tecnico_Dependencia> Sol_Tecnicos_Dependencias { get; set; }


        /*****************************Roles y Permisos**************************/
        public virtual DbSet<Sistemas> Sistemas { get; set; }
        public virtual DbSet<Grupos> Grupos { get; set; }
        public virtual DbSet<Categorias> Categorias { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }
        public virtual DbSet<GruposUsuarios> GruposUsuarios { get; set; }
        public virtual DbSet<sp_users_ldapID> sp_users_ldapID { get; set; }




        /***************************Inventario De Equipos***********************/

        public virtual DbSet<CaracteristicaInventario> CaracteristicaInventario { get; set; }
        public virtual DbSet<ComputadoraInventario> ComputadoraInventario { get; set; }
        public virtual DbSet<EquiposReds> EquiposReds { get; set; }
        public virtual DbSet<Localidad> Localidad { get; set; }
        public virtual DbSet<EstatusInventario> EstatusInventario { get; set; }
        public virtual DbSet<MarcasInventario> MarcasInventario { get; set; }
        public virtual DbSet<ModeloInventario> ModeloInventario { get; set; }
        public virtual DbSet<ProveedorInventario> ProveedorInventario { get; set; }
        public virtual DbSet<ConectividadInventario> ConectividadInventarios { get; set; }
        public virtual DbSet<RegionProvincia> RegionProvincia { get; set; }
     
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<Provinces> Provinces { get; set; }
        public virtual DbSet<Municipalities> Municipalities { get; set; }
        public virtual DbSet<VistaComputadoraInventario> VistaComputadoraInventario { get; set; }
        public virtual DbSet<VistaConectividads> VistaConectividads { get; set; }
        public virtual DbSet<VistaEquiposReds> VistaEquiposReds { get; set; }
        public virtual DbSet<Localidades> Localidades { get; set; }
        public virtual DbSet<MediosInventario> MediosInventario { get; set; }
        public virtual DbSet<Taller> Taller { get; set; }
        public virtual DbSet<VistaTaller> VistaTaller { get; set; }


        /*****************************Reporte de errores**************************/
        public virtual DbSet<LogErrores> LogErrores { get; set; }
        public virtual DbSet<CodigoError> CodigoError { get; set; }


        /*****************************Escaneo de documentos (cartas...)**************************/
        public virtual DbSet<EscanerRegistro> EscanerRegistro { get; set; }
        public virtual DbSet<EscanerArchivos> EscanerArchivos { get; set; }
        public virtual DbSet<EscanerDocumentoVinculado> EscanerDocumentoVinculado { get; set; }
        public virtual DbSet<EscanerChat> EscanerChat { get; set; }


        /************************************Estadisticas************************/
        public DbSet<VistaTiempoRespuesta> VistaTiempoRespuesta { get; set; }
        public virtual DbSet<Vw_Tiempo_Excedido_Mensual> VistaTiempoExcedidoMensual { get; set; } 
        public virtual DbSet<Vw_Tiempo_Respuesta_Mensual> VistaTiempoRespuestaMensual { get; set; }

        /************************************Permisos y Vacaciones************************/
        public virtual DbSet<Permisos> Permisos { get; set; }
        public virtual DbSet<PermisosEstados> PermisosEstados { get; set; }
        public virtual DbSet<TipoLicencia> TipoLicencia { get; set; }
        public virtual DbSet<VistaPermisos> VistaPermisos { get; set; }
        public virtual DbSet<Nmdependencias_Permisos> Nmdependencias_Permisos { get; set; }
        public virtual DbSet<Nmdeptos_Permisos> Nmdeptos_Permisos { get; set; }
        public virtual DbSet<Nmdivision_Permisos> Nmdivision_Permisos { get; set; }
        public virtual DbSet<NmSecciones_Permisos> NmSecciones_Permisos { get; set; }
        public virtual DbSet<EncargadoStatus> EncargadoStatus { get; set; }
        public virtual DbSet<DependenciaActividad> DependenciaActividad { get; set; }
        public virtual DbSet<DepartamentoActividad> DepartamentoActividad { get; set; }
        public virtual DbSet<DivisionActividad> DivisionActividad { get; set; }
        public virtual DbSet<SeccionActividad> SeccionActividad { get; set; }
        public virtual DbSet<ActividadEncargado> ActividadEncargado { get; set; }
      

        /************************************Eventos*********************************************/
        public virtual DbSet<Eventos> Eventos { get; set; }
        public virtual DbSet<TipoEventos> TipoEventos { get; set; }
        public virtual DbSet<TipoCobertura> TipoCobertura { get; set; }
        public virtual DbSet<EventoTipoCobertura> EventoTipoCobertura { get; set; }

        /************************************Contralorķa*********************************************/

        public virtual DbSet<TiposFondos> TiposFondos { get; set; }
        public virtual DbSet<Contralores> Contralores { get; set; }
        public virtual DbSet<ReposicionFondos> ReposicionFondos { get; set; }
        public virtual DbSet<vw_ReposicionFondos> vw_ReposicionFondos { get; set; }
        public virtual DbSet<Documentos> Documentos { get; set; }

        /************************************Identidad Visual Institucional*********************************************/
        public virtual DbSet<DocIdVisual> DocIdVisual { get; set; }
        public virtual DbSet<FlotaExtensionEmpleado> FlotaExtensionEmpleado { get; set; }

        /************************************Gestion Humana*********************************************/
        public virtual DbSet<Seguro> Seguros { get; set; }
        public virtual DbSet<EmpleadoComplemento> EmpleadoComplemento { get; set; }
        public  virtual DbSet<EmpleadoIntermedio> EmpleadoIntermedio { get; set; }
        public virtual DbSet<ArchivoSeguro> ArchivosSeguros { get; set; }

        public virtual DbSet<EmpleadoActualizarDatos> EmpleadoActualizarDatos { get; set; }
        public virtual DbSet<DocumentoGestionHumana> DocumentoGestionHumana { get; set; }
        public virtual DbSet<AlbumGestionHumana> AlbumGestionHumana { get; set; }
        public virtual DbSet<FotoGestionHumana> FotoGestionHumana { get; set; }
        public virtual DbSet<CulturaOrganizacional> CulturaOrganizacional { get; set; }
        public virtual DbSet<CulturaOrganizacionalTipo> CulturaOrganizacionalTipo { get; set; }
        public virtual DbSet<EmpleadoHorarioEspecial> EmpleadoHorarioEspecial { get; set; }
        public virtual DbSet<AutorLibroBiblioteca> AutorLibroBiblioteca { get; set; }
        public virtual DbSet<LibroBiblioteca> LibroBiblioteca { get; set; }
        public virtual DbSet<SeccionBiblioteca> SeccionBiblioteca { get; set; }
        public virtual DbSet<CartaProcedencia> CartaProcedencia { get; set; }
        public virtual DbSet<DiaEstudio> DiaEstudio { get; set; }
        public virtual DbSet<CarpetasDocumentosGH> CarpetasDocumentosGH { get; set; }
        public virtual DbSet<ARS> ARS { get; set; }
        public virtual DbSet<centroFormacion> centroFormacion { get; set; }
        public virtual DbSet<Empleados_Carreras> Empleados_Carreras { get; set; }
        public virtual DbSet<nivelCentro> nivelCentro { get; set; }
        public virtual DbSet<Profesiones> Profesiones { get; set; }
        public virtual DbSet<Institucion> Instituciones { get; set; }
        public virtual DbSet<vw_Ciudad> vw_Ciudad { get; set; }
        public virtual DbSet<vw_Distritos> vw_Distritos { get; set; }
        public virtual DbSet<vw_Municipios> vw_Municipios { get; set; }
        public virtual DbSet<vw_Provinces> vw_Provinces { get; set; }
        public virtual DbSet<vw_Sectores> vw_Sectores { get; set; }
        public virtual DbSet<AFP> AFP { get; set; }
        public virtual DbSet<EstadosCiviles> EstadosCiviles { get; set; }
        public  virtual DbSet<EmpleadoViaticos> EmpleadoViaticos { get; set; }
        public virtual DbSet<vw_CargosDepto> vw_CargosDepto { get; set; }

        //Modulo de Sugerencias////
        public virtual DbSet<RH_Sugerencia> RH_Sugerencia { get; set; }
        public virtual DbSet<RH_SugerenciaEstado> RH_SugerenciaEstado { get; set; }
        public virtual DbSet<RH_SugerenciaMotivo> RH_SugerenciaMotivo { get; set; }
        public virtual DbSet<RH_SugerenciaEvaluacion_log> RH_SugerenciaEvaluacion_log { get; set; }
        public virtual DbSet<RH_SugerenciaSupervisor> RH_SugerenciaSupervisor { get; set; }
        public virtual DbSet<RH_SugerenciaSupervisor_log> RH_SugerenciaSupervisor_log { get; set; }

        //Catalogo de Descuento////
        public virtual DbSet<RH_CatalogoDescuento> RH_CatalogoDescuento { get; set; }
        public virtual DbSet<RH_CatalogoDescuentoTipo> RH_CatalogoDescuentoTipo { get; set; }
        public virtual DbSet<RH_EmpresaCatalogo> RH_EmpresaCatalogo { get; set; }

        //Nuestro Equipo ////

        public virtual DbSet<RH_NuestroEquipo> RH_NuestroEquipo { get; set; }
        public virtual DbSet<RH_NuestroEquipoDepartamento> RH_NuestroEquipoDepartamento { get; set; }
        public virtual DbSet<RH_NuestroEquipoMiembro> RH_NuestroEquipoMiembro { get; set; }

        //CONCURSO MADRES
        public virtual DbSet<RH_ConcursoMadres> RH_ConcursoMadres { get; set; }
        public virtual DbSet<RH_EstadoConcursoMadres> RH_EstadoConcursoMadres { get; set; }
        public virtual DbSet<RH_VotosMadres> RH_VotosMadres { get; set; }


        ///////////////////Modulo Departamento Seguridad Informatica/////////////////////////////////
        public virtual DbSet<vw_Inactividad> vw_Inactividad { get; set; }
        public virtual DbSet<sp_InactividadInfo> sp_InactividadInfo { get; set; }
        public virtual DbSet<ActualizacionActividad> ActualizacionActividad { get; set; }
        public virtual DbSet<ActualizacionDependientes> ActualizacionDependientes { get; set; }
        public virtual DbSet<ActualizacionActividadComplemento> ActualizacionActividadComplemento { get; set; }
        public virtual DbSet<ActualizacionHijo> ActualizacionHijo { get; set; }

        //Sistema de Repatriados
        public virtual DbSet<SSR_Direcciones> SSR_Direcciones { get; set; }
        public virtual DbSet<SSR_DireccionesFamiliares> SSR_DireccionesFamiliares { get; set; }
        public virtual DbSet<SSR_DireccionesRepatriados> SSR_DireccionesRepatriados { get; set; }
        public virtual DbSet<SSR_DocumentosEstados> SSR_DocumentosEstados { get; set; }
        public virtual DbSet<SSR_DocumentosTipos> SSR_DocumentosTipos { get; set; }
        public virtual DbSet<SSR_Familiares> SSR_Familiares { get; set; }
        public virtual DbSet<SSR_Instituciones> SSR_Instituciones { get; set; }
        public virtual DbSet<Parentesco> Parentesco { get; set; }
        public virtual DbSet<SSR_Repatriados> SSR_Repatriados { get; set; }
        public virtual DbSet<SSR_Servicios> SSR_Servicios { get; set; }
        public virtual DbSet<SSR_ServiciosDocumentos> SSR_ServiciosDocumentos { get; set; }
        public virtual DbSet<SSR_ServiciosEstados> SSR_ServiciosEstados { get; set; }
        public virtual DbSet<SSR_ServiciosTipos> SSR_ServiciosTipos { get; set; }
        public virtual DbSet<SSR_ResponsableRepatriados> SSR_ResponsableRepatriados { get; set; }
        public virtual DbSet<SSR_ServiciosTiposDocumentos> SSR_ServiciosTiposDocumentos { get; set; }
        public virtual DbSet<SSR_Telefonos> SSR_Telefonos { get; set; }
        public virtual DbSet<SSR_TelefonosFamiliares> SSR_TelefonosFamiliares { get; set; }
        public virtual DbSet<SSR_TelefonosRepatriados> SSR_TelefonosRepatriados { get; set; }
        public virtual DbSet<SSR_TelefonosTipos> SSR_TelefonosTipos { get; set; }
        public virtual DbSet<Pais> Pais { get; set; }
        public virtual DbSet<Vw_Mostrar_Nacionalidades> Vw_Mostrar_Nacionalidades { get; set; }
        public virtual vmSalarioNomina vmSalarioNomina { get; set; }


        //MODULO DE VISITAS
        public DbSet<Visitas> Visitas { get; set; }
        //public virtual DbSet<TelefonoRepatriados> TelefonoRepatriados { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<EmpleadoComplemento>()
                .HasMany<ActualizacionActividad>(t => t.ActualizacionActividad)
                .WithMany(m => m.EmpleadoComplemento)
                .Map(cs =>
                {
                    cs.MapLeftKey("EmpCompId");
                    cs.MapRightKey("ActividadId");
                });

            modelBuilder.Entity<RH_EmpresaCatalogo>()
                .HasMany<RH_CatalogoDescuentoTipo>(t => t.RH_CatalogoDescuentoTipo)
                .WithMany(m => m.RH_EmpresaCatalogo)
                .Map(cs =>
                {
                    cs.MapLeftKey("EmpresaCatalogoId");
                    cs.MapRightKey("CatDescuentoTipoId");
                });
        }

    }
}

