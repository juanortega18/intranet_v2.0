﻿namespace intra.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Web;

    public partial class Logs
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int LogId { get; set; }
        [StringLength(25)]
        public string LogIp { get; set; }
        [StringLength(100)]
        public string LogUsuario { get; set; }
        public DateTime LogFecha { get; set; }
        [StringLength(300)]
        public string LogAccion { get; set; }
        public int sistemaId { get; set; }

        [NotMapped]
        public String LogUsuarioNombre { get; set; }


    }
}