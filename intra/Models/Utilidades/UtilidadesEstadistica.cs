﻿using intra.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Utilidades
{
    public static class UtilidadesEstadistica
    {
        public static List<TiempoRespuesta> servUlt7Dias(dbIntranet db, int departamentoId = 0)
        {
            var servUlt7Dias = (from t in db.Sol_Registro_Solicitud
                                join t2 in db.VISTA_EMPLEADOS
                                on t.TecnicoId.ToString() equals t2.empleadoid
                                where DateTime.Now <= System.Data.Entity.DbFunctions.AddDays(t.FhFinalSolicitud, 7) &&
                                (t.DepartamentoId == departamentoId || departamentoId == 0)
                                group new { t, t2 } by new { t.TecnicoId, t2.nombre } into tg
                                select new TiempoRespuesta() { Nombre = tg.Key.nombre, TiempoMinuto = tg.Count() }).ToList();

            return servUlt7Dias;
        }
        public static List<TiempoRespuesta> tiempoRespuesta(dbIntranet db, int departamentoId = 0)
        {
            var tiempoRespuesta = (from t in db.VistaTiempoRespuesta
                                   join t2 in db.VISTA_EMPLEADOS
                                   on t.TecnicoId.ToString() equals t2.empleadoid
                                   where DateTime.Now <= System.Data.Entity.DbFunctions.AddDays(t.FechaFinal, 7) &&
                                   (t.DepartamentoId == departamentoId || departamentoId == 0)
                                   group new { t, t2 } by new { t.TecnicoId, t2.nombre } into tg
                                   select new TiempoRespuesta() { Nombre = tg.Key.nombre, TiempoMinuto = tg.Sum(x => x.t.Cantidad) }).ToList();

            return tiempoRespuesta;

        }
        public static List<TiempoRespuesta> solicitudesXmes(dbIntranet db, int departamentoId = 0, int annos = 0, int meses = 0)
        {

            var serviciosRealXmes = (from t in db.Sol_Registro_Solicitud
                                     join t2 in db.VISTA_EMPLEADOS
                                     on t.TecnicoId.ToString() equals t2.empleadoid
                                     where DateTime.Now <= System.Data.Entity.DbFunctions.AddMonths(t.FhFinalSolicitud, 1) &&
                                     (t.FhFinalSolicitud.Year == annos || annos == 0) &&
                                     (t.FhFinalSolicitud.Month == meses || meses == -1)
                                     && (t.DepartamentoId == departamentoId || departamentoId == 0)
                                     group new { t, t2 } by new { t.TecnicoId, t2.nombre } into tg
                                     select new TiempoRespuesta() { Nombre = tg.Key.nombre, TiempoMinuto = tg.Count() }).ToList();

            return serviciosRealXmes;

        }

        public static List<TiempoRespuesta> tiempoRespuestaXmes(dbIntranet db, int departamentoId = 0)
        {
            var tiempoRespuestaXmes = (from t in db.VistaTiempoRespuestaMensual
                                       join t2 in db.VISTA_EMPLEADOS
                                       on t.TecnicoId.ToString() equals t2.empleadoid
                                       where (t.DepartamentoId == departamentoId || departamentoId == 0)
                                       select new TiempoRespuesta() { Nombre = t2.nombre, TiempoMinuto = t.TiempoRespMensual }).ToList();


            return tiempoRespuestaXmes;



        }
        public static List<TiempoRespuesta> asignaUltimaSemanaAbiertas(dbIntranet db, int departamentoId = 0)
        {
            var asignaAbiertasUltimaSemana = (from t in db.Sol_Registro_Solicitud
                                              join t2 in db.VISTA_EMPLEADOS
                                              on t.TecnicoId.ToString() equals t2.empleadoid
                                              where DateTime.Now <= System.Data.Entity.DbFunctions.AddDays(t.FhInicioSolicitud, 7) &&
                                              (t.DepartamentoId == departamentoId || departamentoId == 0 && t.EstadoId == 1)
                                              group new { t, t2 } by new { t.TecnicoId, t2.nombre } into tg
                                              select new TiempoRespuesta() { Nombre = tg.Key.nombre, TiempoMinuto = tg.Count() }).ToList();


            return asignaAbiertasUltimaSemana;
        }
        public static List<TiempoRespuesta> asignaUltimaSemanaCerradas(dbIntranet db, int departamentoId = 0)
        {

            var asignaCerradasUltimaSemana = (from t in db.Sol_Registro_Solicitud
                                              join t2 in db.VISTA_EMPLEADOS
                                              on t.TecnicoId.ToString() equals t2.empleadoid
                                              where DateTime.Now <= System.Data.Entity.DbFunctions.AddDays(t.FhInicioSolicitud, 7) &&
                                              (t.DepartamentoId == departamentoId || departamentoId == 0 && t.EstadoId == 1)
                                              group new { t, t2 } by new { t.TecnicoId, t2.nombre } into tg
                                              select new TiempoRespuesta() { Nombre = tg.Key.nombre, TiempoMinuto = tg.Count() }).ToList();


            return asignaCerradasUltimaSemana;




        }
        public static List<TiempoRespuesta> solicitudRealizadasXGerencias(dbIntranet db, int departamentoId = 0, int annos = 0, int meses = 0)
        {
            var solicitudRealizadasXGerencias = (from t in db.Sol_Registro_Solicitud
                                                 join t2 in db.Sol_Departamento
                                                 on t.DepartamentoId.ToString() equals t2.DepartamentoId.ToString()
                                                 where DateTime.Now <= System.Data.Entity.DbFunctions.AddMonths(t.FhFinalSolicitud, 1) &&
                                                (t.FhFinalSolicitud.Year == annos || annos == 0) &&
                                                (t.FhFinalSolicitud.Month == meses || meses == -1)
                                                && (t.DepartamentoId == departamentoId || departamentoId == 0)
                                                 group new { t, t2 } by new { t.SolicitanteId, t.DepartamentoId, t2.Descripcion } into tg
                                                 select new TiempoRespuesta() { Departamento = tg.Key.Descripcion, TiempoMinuto = tg.Count() }).ToList();

            return solicitudRealizadasXGerencias;
        }
        public static List<TiempoRespuesta> solicitudesRealizadasXUsuarios(dbIntranet db, int departamentoId = 0, int annos = 0, int meses = 0)
        {
            var solicitudRealizadasXUsuarios = (from t in db.Sol_Registro_Solicitud
                                                join t2 in db.VISTA_EMPLEADOS
                                               on t.SolicitanteId.ToString() equals t2.empleadoid
                                                where DateTime.Now <= System.Data.Entity.DbFunctions.AddMonths(t.FhInicioSolicitud, 1) &&
                                                (t.FhInicioSolicitud.Year == annos || annos == 0) &&
                                                (t.FhFinalSolicitud.Month == meses || meses == -1) &&
                                                (t.DepartamentoId == departamentoId || departamentoId == 0)
                                                group new { t, t2 } by new { t.SolicitanteId, t2.nombre } into tg
                                                select new TiempoRespuesta() { Nombre = tg.Key.nombre, TiempoMinuto = tg.Count() }).ToList();



            return solicitudRealizadasXUsuarios;
        }

        public static List<TiempoRespuesta> servCantidadHoraUltimaSemana(dbIntranet db, int departamentoid = 0)
        {

            var cantidadHorasUltimaSemana = (from t in db.Sol_Registro_Solicitud
                                             join t2 in db.VISTA_EMPLEADOS
                                             on t.TecnicoId.ToString() equals t2.empleadoid
                                             where DateTime.Now <= System.Data.Entity.DbFunctions.AddDays(t.FhInicioSolicitud, 7) &&
                                             (t.DepartamentoId == departamentoid || departamentoid == 0)
                                             group new { t, t2 } by new { t.TecnicoId, t2.nombre } into tg
                                             select new TiempoRespuesta() { Nombre = tg.Key.nombre, cantidadHoras = tg.Sum(x => x.t.Horas) }).ToList();


            return cantidadHorasUltimaSemana;
        }
        public static List<TiempoRespuesta> servCantidadHoraMensual(dbIntranet db, int departamentoid = 0, int annos = 0, int meses = 0)
        {

            var cantidadHorasMensual = (from t in db.Sol_Registro_Solicitud
                                        join t2 in db.VISTA_EMPLEADOS
                                        on t.TecnicoId.ToString() equals t2.empleadoid
                                        where DateTime.Now <= System.Data.Entity.DbFunctions.AddDays(t.FhFinalSolicitud, 1) &&
                                        (t.FhInicioSolicitud.Year == annos || annos == 0) &&
                                        (t.FhFinalSolicitud.Month == meses || meses == 0) &&
                                        (t.DepartamentoId == departamentoid || departamentoid == 0)
                                        group new { t, t2 } by new { t.TecnicoId, t2.nombre } into tg
                                        select new TiempoRespuesta() { Nombre = tg.Key.nombre, cantidadHoras = tg.Sum(x => x.t.Horas) }).ToList();


            return cantidadHorasMensual;
        }




        public static List<TiempoRespuesta> tiempoExcedRespuesta(dbIntranet db, int departamentoId = 0)
        {
            var tiempoExcedRespuesta = (from t in db.VistaTiempoRespuesta
                                        join t2 in db.VISTA_EMPLEADOS
                                        on t.TecnicoId.ToString() equals t2.empleadoid
                                        where (t.DepartamentoId == departamentoId || departamentoId == 0)
                                        group new { t, t2 } by new { t.TecnicoId, t2.nombre } into tg
                                        select new TiempoRespuesta() { Nombre = tg.Key.nombre, TiempoMinuto = tg.Sum(x => x.t.TiempoExcedido) }).ToList();




            return tiempoExcedRespuesta;
        }

    
        public static List<TiempoRespuesta> tiempoExcedMensual(dbIntranet db, int departamentoId = 0)
        {
            var tiempoExcedMensual = (from t in db.VistaTiempoExcedidoMensual
                                        join t2 in db.VISTA_EMPLEADOS
                                        on t.TecnicoId.ToString() equals t2.empleadoid
                                        where (t.DepartamentoId == departamentoId || departamentoId == 0)
                                        group new { t, t2 } by new { t.TecnicoId, t2.nombre } into tg
                                        select new TiempoRespuesta() { Nombre = tg.Key.nombre, TiempoMinuto = tg.Sum(x => x.t.TiempoExcMensual) }).ToList();




            return tiempoExcedMensual;
        }

  

    }
}