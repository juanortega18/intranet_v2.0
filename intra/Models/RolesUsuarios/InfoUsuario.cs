﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.RolesUsuarios
{
    //Clase Implementada para Manejar la Info del usuario
    public class InfoUsuario
    {
        [Key]
        public string Nombre { get; set; }
        public string Departamento { get; set; }
        public string Cargo { get; set; }
        public int UsuarioId { get; set; }
        public int GrupoId { get; set; }
        public string UsuarioLogin { get; set; }
        public string EmpleadoId { get; set; }
        public int PermisoId { get; set; }
        public int PermisoUsuario { get; set; }
        public DateTime FechaIngreso { get; set; }

    }
}
