﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.RolesUsuarios
{
    public partial class Sistemas
    {
        [Key]
        public int SistemaId { get; set; }
        public string SistemaNombre { get; set; }
        public string SistemaDescripcion { get; set; }
    }
}
