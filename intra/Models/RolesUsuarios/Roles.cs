﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.RolesUsuarios
{
    public partial class Roles
    {
        [Key]
        public int RolId { get; set; }
        public string RolNombre { get; set; }
        public string RolDescripcion { get; set; }
        public int GrupoId { get; set; }

    }
}
