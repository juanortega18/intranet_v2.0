﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.RolesUsuarios
{
    public partial class Usuarios
    {
        [Key]
        public int UsuarioId { get; set; }
        public string UsuarioLogin { get; set; }
        public DateTime UsuarioFechaCreacion { get; set; }
        public string UsuarioCreadoPor { get; set; }
        public string UsuarioEmpleadoId { get; set; }


    }
}
