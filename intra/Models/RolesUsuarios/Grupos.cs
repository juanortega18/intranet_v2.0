﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.RolesUsuarios
{
    public partial class Grupos
    {


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GrupoId { get; set; }
        public string GrupoNombre { get; set; }
        public string GrupoDescripcion { get; set; }
        public int SistemaId { get; set; }
        public int CategoriaId { get; set; }



    }
}
