﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.RolesUsuarios
{
    public partial class GruposUsuarios
    {

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GrupoId { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UsuarioId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SistemaId { get; set; }



    }
}
