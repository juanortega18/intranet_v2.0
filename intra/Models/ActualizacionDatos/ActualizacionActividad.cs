namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ActualizacionActividad")]
    public partial class ActualizacionActividad
    {
        [Key]
        public int ActividadId { get; set; }

        [StringLength(30)]
        public string ActividadDescripcion { get; set; }

        public virtual ICollection<EmpleadoComplemento> EmpleadoComplemento { get; set; }

    }
}
