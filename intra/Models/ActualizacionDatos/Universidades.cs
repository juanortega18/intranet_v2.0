namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Universidades
    {

        [Key]
        public int UniversidadId { get; set; }

        [Required]
        [StringLength(200)]
        public string Descripcion { get; set; }

        [Required]
        [StringLength(10)]
        public string Siglas { get; set; }

        public bool Estado { get; set; }

        public DateTime FhCreacion { get; set; }


        //public virtual Empleados_Carreras Empleados_Carreras { get; set; }
    }
}
