
namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("centroFormacion")]
    public partial class centroFormacion
    {
        [Key]
        public int codCentro { get; set; }

        [StringLength(200)]
        public string CentroDescripcion { get; set; }

        public int codTipo { get; set; }

        public int codNivel { get; set; }

        [StringLength(10)]
        public string CentroTelefono { get; set; }

        [StringLength(50)]
        public string CentroRepresentante { get; set; }

        [StringLength(50)]
        public string CentroCorreo { get; set; }

        public virtual nivelCentro nivelCentro { get; set; }
    }
}
