namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NMEMPLEADOS")]
    public partial class NMEMPLEADOS
    {
        [Key]
        [StringLength(20)]
        public string EmpleadoID { get; set; }

        //[Key]
        //[Column(Order = 1)]
        [StringLength(3)]
        public string EmpresaId { get; set; }

        [StringLength(20)]
        public string EmpleadoIDAnterior { get; set; }

        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(25)]
        public string Nombre1 { get; set; }

        [StringLength(25)]
        public string Nombre2 { get; set; }

        [StringLength(25)]
        public string Apellido1 { get; set; }

        [StringLength(25)]
        public string Apellido2 { get; set; }

        [StringLength(20)]
        public string Tarjeta { get; set; }

        [StringLength(20)]
        public string Alias { get; set; }

        public bool Encargado { get; set; }

        public int ID_Cosmo { get; set; }

        [StringLength(10)]
        public string GrupoEmpleadoId { get; set; }

        [StringLength(30)]
        public string Entrenamiento { get; set; }

        [StringLength(250)]
        public string Direccion { get; set; }

        [StringLength(10)]
        public string CodigoPostal { get; set; }

        [StringLength(20)]
        public string Telefono1 { get; set; }

        [StringLength(20)]
        public string Telefono2 { get; set; }

        [StringLength(10)]
        public string TelefonoExtensionEmp { get; set; }

        [StringLength(20)]
        public string Fax { get; set; }

        [StringLength(20)]
        public string Celular { get; set; }

        [StringLength(20)]
        public string Beeper { get; set; }

        [StringLength(80)]
        public string Email { get; set; }

        [StringLength(255)]
        public string URL { get; set; }

        [Column(TypeName = "money")]
        public decimal? Salario { get; set; }

        [StringLength(10)]
        public string CiudadID { get; set; }

        [StringLength(20)]
        public string Cedula { get; set; }

        public int EstadoCivil { get; set; }

        public int Sexo { get; set; }

        public int Estatus { get; set; }

        public int FormaPago { get; set; }

        [StringLength(20)]
        public string CargoId { get; set; }

        [StringLength(20)]
        public string DeptoID { get; set; }

        [StringLength(20)]
        public string TipoEmpleadoID { get; set; }

        public bool Pendiente { get; set; }

        public bool Planta { get; set; }

        public bool ISR { get; set; }

        public bool IDSS { get; set; }

        public bool AFP { get; set; }

        public bool ARS { get; set; }

        [StringLength(15)]
        public string UsuarioId { get; set; }

        [StringLength(20)]
        public string Carnet { get; set; }

        [StringLength(20)]
        public string CuentaBanco { get; set; }

        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Cumpleano { get; set; }

        [StringLength(30)]
        public string TituloID { get; set; }

        [StringLength(3)]
        public string MonedaID { get; set; }

        [StringLength(20)]
        public string DepartamentoIDAnt { get; set; }

        [StringLength(20)]
        public string DepartamentoId { get; set; }

        [StringLength(20)]
        public string DependenciaID { get; set; }

        [StringLength(20)]
        public string DependenciaIDAnt { get; set; }

        [StringLength(20)]
        public string SeccionIDAnt { get; set; }

        [StringLength(20)]
        public string SeccionID { get; set; }

        [StringLength(20)]
        public string DivisionIDAnt { get; set; }

        [StringLength(20)]
        public string DivisionID { get; set; }

        [StringLength(10)]
        public string Pasaporte { get; set; }

        [StringLength(10)]
        public string LicenciaConducir { get; set; }

        [StringLength(20)]
        public string PropositoId { get; set; }

        [StringLength(20)]
        public string TipoNominaID { get; set; }

        [StringLength(20)]
        public string ProvinciaID { get; set; }

        [StringLength(20)]
        public string DeptoIDProvisional { get; set; }

        [StringLength(20)]
        public string MunicipioID { get; set; }

        [StringLength(20)]
        public string Tratamiento { get; set; }

        [StringLength(20)]
        public string TarjetaAnterior { get; set; }

        public bool SolitudEmpleado { get; set; }

        public int NumeroArchivo { get; set; }

        public int CausaNombramiento { get; set; }

        public int TurnoId { get; set; }

        public bool MinisterioPublico { get; set; }

        public bool Poncha { get; set; }

        public bool Nomina { get; set; }

        public bool Incorporado { get; set; }

        public bool EmpleadoPlanta { get; set; }

        public bool NombraDesignacion { get; set; }

        public bool Fotografia { get; set; }

        public bool certificadomedico2 { get; set; }

        public bool certificadomedico { get; set; }

        public bool CertificanoDelincuencia { get; set; }

        public bool CurriculumVitae { get; set; }

        public bool CertificadoEstudiante { get; set; }

        public bool CopiaCedula { get; set; }

        public bool Retenido { get; set; }

        public bool Estado { get; set; }

        public int TipoSangre { get; set; }

        [StringLength(50)]
        public string SolicitudID { get; set; }

        [StringLength(20)]
        public string PaisID { get; set; }

        [StringLength(20)]
        public string ProvinciaIDNacimiento { get; set; }

        [StringLength(20)]
        public string MunicipioIDNacimiento { get; set; }

        [StringLength(20)]
        public string Tiponominald { get; set; }

        [StringLength(20)]
        public string PaisIDNacimiento { get; set; }

        [StringLength(30)]
        public string Sector { get; set; }

        [StringLength(50)]
        public string Referencia { get; set; }

        public DateTime? FechaNombramiento { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FechaIngreso { get; set; }

        public DateTime? FechaSalida { get; set; }

        public DateTime? FechaIngresoNomina { get; set; }

        public DateTime? FechaCuentaBanco { get; set; }

        public DateTime? FechaTomaPosesion { get; set; }

        [StringLength(20)]
        public string TipoCuentaID { get; set; }

        public DateTime? FechaCuenta { get; set; }

        [StringLength(20)]
        public string DeptoIDAuxiliar { get; set; }

        [StringLength(20)]
        public string TelefonoOficina { get; set; }

        [Required]
        [StringLength(15)]
        public string CreadoPor { get; set; }

        [Required]
        [StringLength(15)]
        public string ModificadoPor { get; set; }

        public DateTime FechaCreado { get; set; }

        public DateTime FechaModificado { get; set; }

        public int RecordID { get; set; }
     
        public int? TipoGrupoMinisterio { get; set; }

        public int? dppantes { get; set; }

        public int? deptoantes { get; set; }

        public int? ID_Persona { get; set; }
        //public virtual EMPRESAID EMPRESAID { get; set; }

        //public virtual NMCARGOS NMCARGOS { get; set; }
    
        public virtual RHNivelAcademico RHNivelAcademico { get; set; }
        [NotMapped]
        public virtual NMDEPTOs NMDEPTOs { get; set; }
     
    }
}
