﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace intra.Models.ActualizacionDatos
{
    [Table("EstadosCiviles")]
  public  class EstadosCiviles
    {
        [Key]
        public int ID_EstadoCivil { get; set; }
        public string Descripcion { get; set; }
    }
}
