namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Empleados_Carreras
    {
        [Key]
        public int CarreraId { get; set; }

        public int Empleadoid { get; set; }

        [ForeignKey("Institucion")]
        public int InstitucionId { get; set; }

        [ForeignKey("Profesiones")]
        public int ProfesionId { get; set; }

        [ForeignKey("NivelAcademico")]
        public int NivelAcademicoId { get; set; }

        public string OtraProfesion { get; set; }

        public string OtraInstitucion { get; set; }

        public bool Estado { get; set; }

        public DateTime FhModificacion { get; set; }

        public DateTime FhCreacion { get; set; }

        public virtual Profesiones Profesiones { get; set; }

        public virtual Institucion Institucion { get; set; }

        public virtual NivelAcademico NivelAcademico { get; set; }
    }

    public partial class Empleados_Carrera_Detalle
    {
        [Key]
        public int CarreraDetalleId { get; set; }

        public int Empleadoid { get; set; }

        [ForeignKey("NivelAcademico")]
        public int Nivel_AcademicoId { get; set; }

        public string OtraProfesion { get; set; }

        public string OtraInstitucion { get; set; }

        public bool Estado { get; set; }

        public DateTime FhModificacion { get; set; }

        public DateTime FhCreacion { get; set; }

        public virtual NivelAcademico NivelAcademico { get; set; }
    }

}
