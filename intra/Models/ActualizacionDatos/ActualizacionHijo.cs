namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ActualizacionHijo")]
    public partial class ActualizacionHijo
    {
        [Key]
        public int HijoId { get; set; }

        public bool Genero { get; set; }

        public byte Edad { get; set; }
    }
}
