namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Institucion")]
    public partial class Institucion
    {
        [Key]
        public int InstitucionId { get; set; }

        //[ForeignKey("CentroEducativo")]
        public int CentroId { get; set; }

        [Required]
        [StringLength(200)]
        public string Descripcion { get; set; }

        public virtual string DescripcionConSiglas { get { return string.Format("{0} ( {1} )", this.Descripcion, this.Siglas.Trim()); } }

        [Required]
        [StringLength(10)]
        public string Siglas { get; set; }

        public bool Estado { get; set; }

        public DateTime FhCreacion { get; set; }

        //public virtual CentroEducativo CentroEducativo { get; set; }

        //public virtual Empleados_Carreras Empleados_Carreras { get; set; }
    }

    public partial class CentroEducativo {

        [Key]
        public int InstitucionesId { get; set; }

        [Required]
        public string Descripcion { get; set; }

        public bool Estado { get; set; }

        public DateTime FhCreacion { get; set; }

    }
}
