namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class NMDEPTOs
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DeptoID { get; set; }


        [Column(Order = 1)]
        [StringLength(3)]
        public string Empresaid { get; set; }

        [Column(Order = 2)]
        [StringLength(20)]
        public string DependenciasID { get; set; }

        public int OrdenNum { get; set; }

        [Required]
        [StringLength(100)]
        public string Descripcion { get; set; }

        [Required]
        [StringLength(20)]
        public string DeptoAnterior { get; set; }

        [Required]
        [StringLength(15)]
        public string CreadoPor { get; set; }

        [Required]
        [StringLength(15)]
        public string ModificadoPor { get; set; }

        public DateTime FechaCreado { get; set; }

        public DateTime FechaModificado { get; set; }

        public int RecordId { get; set; }

        public int? ID_Provincia { get; set; }

        [StringLength(50)]
        public string ID_Provincia_HP { get; set; }
    }
}
