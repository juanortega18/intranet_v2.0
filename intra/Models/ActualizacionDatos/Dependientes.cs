//namespace intra.Models.ActualizacionDatos
//{
//    using System;
//    using System.Data.Entity;
//    using System.ComponentModel.DataAnnotations.Schema;
//    using System.Linq;

//    public partial class Dependientes : DbContext
//    {
//        public Dependientes()
//            : base("name=Dependientes")
//        {
//        }

//        public virtual DbSet<ActualizacionActividad> ActualizacionActividad { get; set; }
//        public virtual DbSet<ActualizacionActividadComplemento> ActualizacionActividadComplemento { get; set; }
//        public virtual DbSet<ActualizacionDependientes> ActualizacionDependientes { get; set; }
//        public virtual DbSet<ActualizacionHijo> ActualizacionHijo { get; set; }

//        protected override void OnModelCreating(DbModelBuilder modelBuilder)
//        {
//            modelBuilder.Entity<ActualizacionActividad>()
//                .Property(e => e.ActividadDescripcion)
//                .IsUnicode(false);
//        }
//    }
//}
