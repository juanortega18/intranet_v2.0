namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ActualizacionActividadComplemento")]
    public partial class ActualizacionActividadComplemento
    {
        [Key]
        public int ActividadComplementoId { get; set; }

        public int ActividadId { get; set; }

        public int EmpCompId { get; set; }
    }
}
