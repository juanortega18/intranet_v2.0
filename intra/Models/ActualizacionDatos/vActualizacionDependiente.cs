﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace intra.Models.ActualizacionDatos
{
   public class vActualizacionDependiente
    {
        [Key]
        public int DependienteId { get; set; }
        public string Genero { get; set; }
        public string ParentescoId { get; set; }
        public string EstadoCivil { get; set; }
        public string EmpleadoId { get; set; }
        public string Edad { get; set; }


    }
}
