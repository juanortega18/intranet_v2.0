namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using intra.Models.vw_Models;

    [Table("EmpleadoGestionHumana")]
    public partial class EmpleadoActualizarDatos
    {
        [Key]
        public int EmpGestHumanaId { get; set; }
        public int? EmpleadoCodigo { get; set; }
        public int DeptoActual { get; set; }
        public int DepenActual { get; set; }
        public int CargoActual { get; set; }
        public int Supervisor { get; set; }
        [NotMapped]
        public  vw_Dependencias vw_Dependencias { get; set; }
        [NotMapped]
        public  vw_Cargos vw_Cargos { get; set; }
        [NotMapped]
        public vw_Departamentos vw_Departamentos { get; set; }
    }
}
