﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace intra.Models.ActualizacionDatos
{
  public  class vw_Sectores
    {
        [Key]
        public int SectorId { get; set; }
        public int CiudadId { get; set; }
  
        public string SectorDescripcion { get; set; }
        public virtual vw_Ciudad vw_Ciudad { get; set; }

    }
}
