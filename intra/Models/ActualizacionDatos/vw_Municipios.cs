namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_Municipios
    {
        [Key]
        public int? MunicipioId { get; set; }

        public int? ProvinciaId { get; set; }
        [StringLength(50)]
        public string MunicipioDescripcion { get; set; }
    }
}
