//namespace intra.Models.Empleados
//{
//    using System;
//    using System.Data.Entity;
//    using System.ComponentModel.DataAnnotations.Schema;
//    using System.Linq;

//    public partial class Departamento : DbContext
//    {
//        public Departamento()
//            : base("name=Departamento")
//        {
//        }

//        public virtual DbSet<NMDEPENDENCIAS> NMDEPENDENCIAS { get; set; }
//        public virtual DbSet<NMDEPTOs> NMDEPTOs { get; set; }

//        protected override void OnModelCreating(DbModelBuilder modelBuilder)
//        {
//            modelBuilder.Entity<NMDEPENDENCIAS>()
//                .Property(e => e.Empresaid)
//                .IsUnicode(false);

//            modelBuilder.Entity<NMDEPENDENCIAS>()
//                .Property(e => e.Descripcion)
//                .IsUnicode(false);

//            modelBuilder.Entity<NMDEPENDENCIAS>()
//                .Property(e => e.CreadoPor)
//                .IsUnicode(false);

//            modelBuilder.Entity<NMDEPENDENCIAS>()
//                .Property(e => e.ModificadoPor)
//                .IsUnicode(false);

//            modelBuilder.Entity<NMDEPTOs>()
//                .Property(e => e.Empresaid)
//                .IsUnicode(false);

//            modelBuilder.Entity<NMDEPTOs>()
//                .Property(e => e.DependenciasID)
//                .IsUnicode(false);

//            modelBuilder.Entity<NMDEPTOs>()
//                .Property(e => e.Descripcion)
//                .IsUnicode(false);

//            modelBuilder.Entity<NMDEPTOs>()
//                .Property(e => e.DeptoAnterior)
//                .IsUnicode(false);

//            modelBuilder.Entity<NMDEPTOs>()
//                .Property(e => e.CreadoPor)
//                .IsUnicode(false);

//            modelBuilder.Entity<NMDEPTOs>()
//                .Property(e => e.ModificadoPor)
//                .IsUnicode(false);

//            modelBuilder.Entity<NMDEPTOs>()
//                .Property(e => e.ID_Provincia_HP)
//                .IsUnicode(false);
//        }
//    }
//}
