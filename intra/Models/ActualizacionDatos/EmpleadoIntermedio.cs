﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace intra.Models.ActualizacionDatos
{
    [Table("EmpleadoIntermedio")]
    public class EmpleadoIntermedio
      
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmpleadoIntId { get; set; }
        [Required]
        public int EmpleadoCodigo { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Direccion { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public int? EstadoCivil { get; set; } 
        public DateTime FhCreacion { get; set; }
        public string Sector { get; set; }

    }
}