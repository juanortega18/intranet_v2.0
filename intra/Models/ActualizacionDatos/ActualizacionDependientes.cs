namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ActualizacionDependientes
    {
        [Key]
        public int DependienteId { get; set; }

        public bool? Genero { get; set; }
        public int EstadoCivil { get; set; }
        public byte Edad { get; set; }
        public int EmpleadoId { get; set; }
        public int ParentescoId { get; set; }
        public bool Estado { get; set; }
    }
}
