namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RHNivelAcademico")]
    public partial class RHNivelAcademico
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string NivelAcademicoID { get; set; }

  
        [Column(Order = 1)]
        [StringLength(3)]
        public string EmpresaID { get; set; }

        [Required]
        [StringLength(50)]
        public string Descripcion { get; set; }

        public DateTime FechaCreado { get; set; }

        [Required]
        [StringLength(15)]
        public string CreadoPor { get; set; }

        [Required]
        [StringLength(15)]
        public string ModificadoPor { get; set; }

        public DateTime FechaModificado { get; set; }

        public int RecordId { get; set; }
    }
}
