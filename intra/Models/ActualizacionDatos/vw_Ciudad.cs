namespace intra.Models.ActualizacionDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_Ciudad
    {
        [Key]
        public int? CiudadId { get; set; }

        public int? DistritoId { get; set; }

 
        [StringLength(50)]
        public string CiudadDescripcion { get; set; }
    }
}
