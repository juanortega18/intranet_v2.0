﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using intra.Models.vw_Models;

namespace intra.Models.ActualizacionDatos
{
  public  class vDatosEmpleados
    {
        public string EmpleadoId { get; set; }
        public int Sexo { get; set; }
        public string Direccion { get; set; }
        public string Celular { get; set; }
        public string Sector { get; set; }
        public string Telefono { get; set; }
        public string Nombre { get; set; }
        public string CargoAnterior { get; set; }
        public string DepartamentoAnterior { get; set; }
        public string DependenciaAnterior { get; set; }
        public string CargoActual { get; set; }
        public string Cedula { get; set; }
        public string Supervisor { get; set; }
        public int DepartamentoActual { get; set; }
        public int DependenciaActual { get; set; }
        [NotMapped]
        public  vw_Departamentos vw_Departamentos { get; set; }
        [NotMapped]
        public vw_Cargos vw_Cargos { get; set; }
        [NotMapped]
        public  vw_Dependencias vw_Dependencias { get; set; }

    }
}
