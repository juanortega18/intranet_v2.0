﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace intra.Models.ActualizacionDatos
{
    [Table("AFP")]
  public  class AFP
    {
        [Key]
        public int AfpId { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public DateTime FhCreacion { get; set; }
    }
}
