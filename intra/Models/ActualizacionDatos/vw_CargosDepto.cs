﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.ActualizacionDatos
{
  public class vw_CargosDepto
    {
        [Key]
        public string CargoId { get; set; }
        public Int32 DeptoID { get; set; }
        public string Cargos { get; set; }
        public string Departamento { get; set; }
        public Int32 DependenciaID { get; set; }

    }
}
