﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using intra.Models.vw_Models;

namespace intra.Models.ActualizacionDatos
{
    [Table("EmpleadoComplemento")]
   public class EmpleadoComplemento
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmpCompId { get; set; }
        public int EmpleadoId { get; set; }
        public string Profesion { get; set; }
        public int? Dependiente { get; set; }
        public int? ArsId { get; set; }
        public int? AfpId { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
        public DateTime FhCreacion { get; set; }
        public DateTime FhModificacion { get; set; }
        public string Observaciones { get; set; }
        public int NivelAcademicoId { get; set; }
        public int? ProvinciaId { get; set; }
        public int? MunicipioId { get; set; }
        public int? CiudadId { get; set; }
        public int? SectorId { get; set; }
        public int? DistritoId { get; set; }
        public string Numero { get; set; }
        public string Calle { get; set; }

        public virtual ICollection<ActualizacionActividad> ActualizacionActividad { get; set; }

        [NotMapped]
        public IEnumerable<NivelAcademico> NivelAcademico { get; set; }

        
        [NotMapped]
        public virtual vw_Dependencias vw_Dependencias { get; set; }
        [NotMapped]
        public virtual vw_Cargos vw_Cargos { get; set; }
        [NotMapped]
        public virtual vw_Departamentos vw_Departamentos { get; set; }

    

    }
}
