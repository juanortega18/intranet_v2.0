﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace intra.Models.ActualizacionDatos
{
  public  class GestionHumanaViewModel
    {
        public string EmpleadoId { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public string  Sexo { get; set; }
        public string Dependencia { get; set; }
        public string Departamento { get; set; }
        public string Cargo { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string EstadoCivil { get; set; }
        public string GradoAcademico { get; set; }
        public string TiempoEnCargo { get; set; }
        public DateTime?  FechaIngreso { get; set; }
        public string TelefonoResidencial { get; set; }
        public string Celular { get; set; }
        public string TelefonoCelular { get; set; }
        public string CorreoPersonal { get; set; }
        public string DireccionActual { get; set; }
        public string Sector { get; set; }


    }
}
