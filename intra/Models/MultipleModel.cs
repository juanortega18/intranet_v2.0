﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using intra.Models;
using intra.Models.AvisosActividades;

namespace intra.Models
{
    public class MultipleModel
    {
        public IEnumerable<Avisos> Avisos { get; set; }
        public IEnumerable<Noticias> Noticias { get; set; }
    }
}