namespace intra.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class fisc : DbContext
    {
        public fisc()
            : base("name=fisc")
        {
        }

        public virtual DbSet<Enclosures> Enclosures { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Enclosures>()
                .Property(e => e.Enclosure_Code)
                .IsUnicode(false);

            modelBuilder.Entity<Enclosures>()
                .Property(e => e.Enclosure_Name)
                .IsUnicode(false);

            modelBuilder.Entity<Enclosures>()
                .Property(e => e.Enclosure_Description)
                .IsUnicode(false);

            modelBuilder.Entity<Enclosures>()
                .Property(e => e.Enclosure_Title)
                .IsUnicode(false);
        }
    }
}
