﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models
{
    [Table("Aplicaciones")]
  public  class Aplicaciones
    {
        [Key]
        public int AplicationId { get; set; }
        public string AplicacionNombre { get; set; }
        public int EncargadoPaseProduccionId { get; set; }
    }
}
