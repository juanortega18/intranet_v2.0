﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace intra.Models
{
    [Table("Vw_Mostrar_Usuario_Con_Su_Descripcion")]
    public  class DetalleSolicitud
    {
     
        [Key]
        public int CodigoSolicitud { get; set; }
        public int CodigoSolicitante { get; set; }
        public string NombreSolicitante { get; set; }
        public string DepartamentoSolicitante { get; set; }
        public string UsuarioDominioSolic { get; set; }
        public int CodigoTecnico { get; set; }
        public string NombreTecnico { get; set; }
        public string UsuarioDominioTecn { get; set; }
        public int CodigoDepartamento { get; set; }
        public string Departamento { get; set; }
        public string NombreSupervisor { get; set; }
        public string DepartamentoSupervisor { get; set; }
        public int CodigoTipoSolicitud { get; set; }
        public string TipoSolicitud { get; set; }
        public int CodigoSubTipoSolicitud { get; set; }
        public string SubTipoSolicitud { get; set; }
        public string DescripcionSolicitud { get; set; }
        public string DescripcionSolucion { get; set; }
        public int CodigoEstadoSolicitud { get; set; }
        public string EstadoSolicitud { get; set; }
        public bool Estado { get; set; }
        public int Horas { get; set; }
        public int TipodeAsistencia { get; set; }
        public DateTime FechaInicioSolicitud { get; set; }
        public DateTime FechaFinalSolicitud { get; set; }
        public DateTime FechaModificacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int DependenciaId { get; internal set; }
        public string DependenciaNombre { get; internal set; }
    }
}
