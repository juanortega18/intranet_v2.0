﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models
{
    public class EstadisticasViewModel
    {
        //public List<EstadisticasFechas> AñosMeses { get; set; }
        public List<string> Años { get; set; }
        public List<string> Meses { get; set; }
       public List<string> Modificacion { get; set; }
        public List<int> Tiempo{ get; set; }
        // Keep track of the relation between Months and Years
        public Dictionary<string, string> MesesPorAños { get; set; }
        public Dictionary<string, List<string[]>> CantidadesPorMeses { get; set; }
        public List<string[]> CantidadesUltimaSemana { get; set; }
        public List<EstadisticasCantidades> ListaCantidades { get; set; }
        public Dictionary<string, List<string[]>> infoGerenciaPorMeses { get; set; }
        public Dictionary<string, List<string[]>> infoUsusarioPorMeses { get; set; }
        public List<VistaTiempoRespuesta> ListaTiempoRespuesta { get; set; }
        public List<VistaTiempoRespuesta> ListaTiempoExcedido { get; set; }
        public List<string> ListaTiempExcedMensual { get; set; }
        public List<string> ListaTiempRespMensual { get; set; }

    }

    public class EstadisticasCantidades
    {
        public string IdTecnico { get; set; }
        public string Nombre { get; set; }
        //public int CantidadSolicitudes { get; set; }
        public DateTime InicioSolicitud { get; set; }
        public DateTime FinalSolicitud { get; set; }
     
        

    }

    public class EstadisticasAsignadas
    {
        public List<string[]> CantidadesAbiertas { get; set; }
        public List<string[]> CantidadesCerradas { get; set; }
    }

    public class EstadisticasGerencia
    {
        public List<string[]> infoPorGerencia { get; set; }
        public List<string[]> infoPorUsusario { get; set; }
    }
    [Table("VistaTiempoRespuesta")]
    public class VistaTiempoRespuesta
    {
        [Key]
        public int SolicitudId { get; set; }
        public int TecnicoId { get; set; }
        public int Horas { get; set; }
        [NotMapped]
        public string Nombre { get; set; }
        public DateTime FhInicioSolicitud { get; set; }
        public DateTime FhFinalSolicitud { get; set; }
        public DateTime FechaFinal { get; set; }
        public int Cantidad { get; set; }
        public int TiempoExcedido { get; set; }
        public int DepartamentoId { get; set; }

        internal object ToList()
        {
            throw new NotImplementedException();
        }
    }

   [Table("Vw_Tiempo_Excedido_Mensual")]

   public class Vw_Tiempo_Excedido_Mensual

    {
        [Key]
        public int TecnicoId { get; set; }
      
        public int TiempoExcMensual { get; set; }     
        public string Nombre { get; set;  }
        public int DepartamentoId { get; set; }

    }

    [Table("Vw_Tiempo_Respuesta_Mensual")]
    public class Vw_Tiempo_Respuesta_Mensual
    {
        [Key]

        public int TecnicoId { get; set; }
        public int TiempoRespMensual { get; set; }
        public int DepartamentoId { get; set; }


    }
    public class TiempoRespuesta
    {
        [Key]
        public int ID { get; set; }
        public string Nombre { get; set; }
        public int TiempoMinuto { get; set; }
        public string Departamento { get; set; }
        public int cantidadHoras { get; set; }
    }




}