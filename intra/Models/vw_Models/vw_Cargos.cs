﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace intra.Models.vw_Models
{
    public class vw_Cargos
    {
        [Key]
        public string CargoId { get; set; }
        public string Descripcion { get; set; }
    }
}
