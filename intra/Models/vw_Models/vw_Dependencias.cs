﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.vw_Models
{
    public class vw_Dependencias
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DependenciaID { get; set; }

        //[Key]
        //[Column(Order = 1)]
        //[StringLength(3)]
        //public string Empresaid { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(100)]
        public string Descripcion { get; set; }

        public int? DirectorID { get; set; }

        public int? AuxiliarID { get; set; }
    }
}