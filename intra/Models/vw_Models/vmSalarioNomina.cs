﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace intra.Models.vw_Models
{
    public class vmSalarioNomina
    {
        public int NominaId { get; set; }
        public string NominaMesNombre { get; set; }
        public int NominaAnio { get; set; }
        public int NominaMes { get; set; }
        public string NominaIngresoBruto { get; set; }
        public string NominaOtrosIngresos { get; set; }
        public string NominaAfp { get; set; }
        public string NominaISR { get; set; }
        public string NominaISF { get; set; }
        public string NominaOtrosDescuentos { get; set; }
        public string NominaTotalIngresos { get; set; }
        public string NominaTotalDescuentos { get; set; }
        public string NominaIngresoNeto { get; set; }
        public string NominaCedula { get; set; }

    }
}
