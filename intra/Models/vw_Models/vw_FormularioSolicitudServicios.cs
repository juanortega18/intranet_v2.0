namespace intra.Models.vw_Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_FormularioSolicitudServicios
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodigoSolicitud { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodigoSolicitante { get; set; }

        [StringLength(50)]
        public string NombreSolicitante { get; set; }

        [StringLength(100)]
        public string DepartamentoSolicitante { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(20)]
        public string UsuarioDominioSolic { get; set; }

        public int CodigoTecnico { get; set; }

        [StringLength(50)]
        public string NombreTecnico { get; set; }

        [StringLength(20)]
        public string UsuarioDominioTecn { get; set; }

        [StringLength(20)]
        public string CodigoDepartamento { get; set; }

        [StringLength(100)]
        public string Departamento { get; set; }

        [StringLength(50)]
        public string NombreSupervisor { get; set; }

        [StringLength(100)]
        public string DepartamentoSupervisor { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodigoTipoSolicitud { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(100)]
        public string TipoSolicitud { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodigoSubTipoSolicitud { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(100)]
        public string SubTipoSolicitud { get; set; }

        [Key]
        [Column(Order = 7)]
        public string DescripcionSolicitud { get; set; }

        public string DescripcionSolucion { get; set; }

        [Key]
        [Column(Order = 8)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodigoEstadoSolicitud { get; set; }

        [Key]
        [Column(Order = 9)]
        [StringLength(30)]
        public string EstadoSolicitud { get; set; }

        [Key]
        [Column(Order = 10)]
        public bool Estado { get; set; }

        public int? Horas { get; set; }

        public int? TipodeAsistencia { get; set; }

        [Key]
        [Column(Order = 11)]
        public DateTime FechaInicioSolicitud { get; set; }

        public DateTime? FechaFinalSolicitud { get; set; }

        [Key]
        [Column(Order = 12)]
        public DateTime FechaModificacion { get; set; }

        [Key]
        [Column(Order = 13)]
        public DateTime FechaCreacion { get; set; }

        [StringLength(100)]
        public string DependenciaNombre { get; set; }

        [Key]
        [Column(Order = 14)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DependenciaId { get; set; }
    }
}
