﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.vw_Models
{
    public class vw_Divisiones
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DivisionID { get; set; }

        //[Key]
        //[Column(Order = 1)]
        //[StringLength(3)]
        //public string Empresaid { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DeptoID { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DependenciaID { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(100)]
        public string Descripcion { get; set; }

        public int? EncargadoID { get; set; }

        public int? AuxiliarID { get; set; }
    }
}