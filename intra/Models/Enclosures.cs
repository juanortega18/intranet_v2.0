namespace intra.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Enclosures
    {
        [Key]
        public int Enclosure_ID { get; set; }

        public int EnclosuresGroup_ID { get; set; }

        [Required]
        [StringLength(12)]
        public string Enclosure_Code { get; set; }

        [Required]
        [StringLength(90)]
        public string Enclosure_Name { get; set; }

        [Required]
        [StringLength(2048)]
        public string Enclosure_Description { get; set; }

        public int Address_ID { get; set; }

        public bool Enclosure_Active { get; set; }

        public int Enclosure_Order { get; set; }

        public int EnclosureDistrict_ID { get; set; }

        public int EnclosureType_ID { get; set; }

        [Required]
        [StringLength(128)]
        public string Enclosure_Title { get; set; }

        public int EnclosureSubType_ID { get; set; }
    }
}
