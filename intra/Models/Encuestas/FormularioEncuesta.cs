﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using intra.Models.Servicios;

namespace intra.Models.Encuestas
{
    [Table("FormularioEncuesta")]
   public class FormularioEncuesta
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormularioEncuestaId { get; set; }
        public int PreguntasEncuestaId { get; set; }
        public int SolicitudId { get; set; }
        public int EstadoEncuestaId { get; set; }

        public DateTime FechaCreacion { get; set; }
        
      public virtual Sol_Registro_Solicitud Sol_Registro_Solicitud { get; set; }
        public virtual PreguntasEncuesta PreguntasEncuesta { get; set; }
        public virtual EncuestaEstado EncuestaEstado { get; set; }
      
    }
}
