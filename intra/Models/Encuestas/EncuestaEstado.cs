﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Encuestas
{
    [Table("EncuestaEstado")]
    public class EncuestaEstado
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EstadoEncuestaId { get; set; }
        public string EstadoEncuestaDescripcion { get; set; }
    }
}