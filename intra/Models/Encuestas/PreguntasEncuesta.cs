﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Encuestas
{
    [Table("PreguntasEncuesta")]
    public class PreguntasEncuesta
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PreguntasEncuestaId { get; set; }
        public string PreguntasEncuestaDescripcion { get; set; }
    }
}