﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace intra
{
    public class Noticias
    {
        [Key]
        public int NoticiaId { get; set; }
        [Required(ErrorMessage = "El Titulo es Requerido")]
        public string NoticiaTitulo { get; set; }
        //[Required(ErrorMessage = "El Contenido es Requerido")]
        public string NoticiaCuerpo { get; set; }
        public string NoticiaImages { get; set; }
        public string NoticiaPieImages { get; set; }
        public DateTime NoticiaFechaCreacion { get; set; }
        public int NoticiaVistas { get; set; }
        public string NoticiaAutor { get; set; }
        public string NoticiasUrl { get; set; } 

    }
}