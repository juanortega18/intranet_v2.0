﻿namespace intra.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Contralores
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int contralorId { get; set; }

        [Required]
        [StringLength(100)]
        public string contralorNombres { get; set; }

        [Required]
        [StringLength(60)]
        public string contralorApellido1 { get; set; }

        [StringLength(60)]
        public string contralorApellido2 { get; set; }

        public bool? contralorEstado { get; set; }

        public DateTime? contralorFechaRegistro { get; set; }

    }

    [Table("ReposicionFondos")]
    public partial class ReposicionFondos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int fondoId { get; set; }

        public int? fiscaliaId { get; set; }

        [StringLength(70)]
        public string fondoTitular { get; set; }

        public int? tipoFondoId { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? fondoMontoReposicion { get; set; }

        [StringLength(70)]
        public string fondoResponsable { get; set; }

        [StringLength(70)]
        public string fondoAuditor { get; set; }

        [StringLength(255)]
        public string fondoObjetivo { get; set; }

        [StringLength(255)]
        public string fondoAlcance { get; set; }

        public string fondoHallazgos { get; set; }

        public string fondoLeyes { get; set; }

        public string fondoMejoras { get; set; }

        public int? contralorId { get; set; }

        public int? fondoAprobado { get; set; }

        public DateTime? fondoFechaAprobacion { get; set; }

        public DateTime? fondoFechaCreacion { get; set; }

        public string usuario { get; set; }

        [NotMapped]
        public string[] hallazgo_array { get; set; }

        [NotMapped]
        public string[] recomendacion_array { get; set; }
    }

    public partial class TiposFondos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int tipoFondoId { get; set; }
        
        [StringLength(100)]
        public string tipoFondoNombre { get; set; }

        public bool? tipoFondoEstado { get; set; }
        
    }
    
    public partial class vw_ReposicionFondos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        
        public int fondoId { get; set; }

        public int? fiscaliaId { get; set; }
        
        public string FiscaliaNombre { get; set; }
        public string fondoTitular { get; set; }

        public int? tipoFondoId { get; set; }
        public string tipoFondoNombre { get; set; }
        
        public decimal? fondoMontoReposicion { get; set; }
        
        public string fondoResponsable { get; set; }
        
        public string fondoAuditor { get; set; }
        
        public string fondoObjetivo { get; set; }
        
        public string fondoAlcance { get; set; }

        public string fondoHallazgos { get; set; }

        public string fondoLeyes { get; set; }

        public string fondoMejoras { get; set; }

        public int? contralorId { get; set; }

        public string fondoAprobado { get; set; }

        public DateTime? fondoFechaAprobacion { get; set; }

        public DateTime? fondoFechaCreacion { get; set; }

        public string usuario { get; set; }
    }
}

