namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_DocumentosTipos
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DocumentoTipoId { get; set; }

        [StringLength(100)]
        public string Descripcion { get; set; }

        public bool? Estado { get; set; }

        public DateTime? FechaCreacion { get; set; }

        //public virtual SSR_ServiciosDocumentos SSR_ServiciosDocumentos { get; set; }

        //public virtual SSR_ServiciosTiposDocumentos SSR_ServiciosTiposDocumentos { get; set; }
    }
}
