﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace intra.Models.SistemaRepatriados.ViewModel
{
      public class TelefonoRepatriados
    {
        public string tipoTelefono { get; set; }
        public string NumeroTelefono { get; set; }
        public int TelefonoRepatriadoId { get; set; }
        public int RepatriadoId { get; set; }
    }
}
