﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.SistemaRepatriados.ViewModel
{
   public class FamiliaresRepatriadosVM
    {
        public int Repatriado { get; set; }
        public string Parentesco { get; set; }
        public string Nombres { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Nacionalidad { get; set; }
        public string Nota { get; set; }
        public string TipoTelefono { get; set; }
        public string Telefono { get; set; }
        public string Provincia { get; set; }
        public string Municipio { get; set; }
        public string Distrito { get; set; }
        public string Ciudad { get; set; }
        public string Sectores { get; set; }
        public string Calle { get; set; }
        public string NumeroCasa { get; set; }
      public string NombreCompleto { get; set; }

       
      //  public virtual string NombreCompleto { get{ return this.Nombres + " " + this.Apellido1 + " " + this.Apellido2; } }

        


    }
}
