namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_ServiciosEstados
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServicioEstadoId { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }
        //public virtual SSR_Servicios SSR_Servicios { get; set; }
    }
}
