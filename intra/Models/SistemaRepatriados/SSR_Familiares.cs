namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_Familiares
    {
      
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FamiliarId { get; set; }

        public int ParentescoId { get; set; }

        public int RepatriadoId { get; set; }

        [StringLength(20)]
        public string NumeroIdentificacion { get; set; }

        [StringLength(100)]
        public string Nombres { get; set; }

        [StringLength(60)]
        public string Apellido1 { get; set; }

        [StringLength(60)]
        public string Apellido2 { get; set; }

        public int? NacionalidadId { get; set; }

        public bool? Vive { get; set; }

        public string Notas { get; set; }

        public DateTime? FechaVinculacion { get; set; }

        [StringLength(100)]
        public string UsuarioCreador { get; set; }

        //public virtual SSR_DireccionesFamiliares SSR_DireccionesFamiliares { get; set; }

        public virtual Parentesco Parentesco { get; set; }

        public virtual SSR_Repatriados SSR_Repatriados { get; set; }

       // public virtual SSR_TelefonosFamiliares SSR_TelefonosFamiliares { get; set; }
    }
}
