namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_ServiciosDocumentos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServicioDocumentoId { get; set; }

        public int ServicioId { get; set; }

        public int DocumentoTipoId { get; set; }

        public int? DocumentoEstadoId { get; set; }

        public virtual SSR_DocumentosEstados SSR_DocumentosEstados { get; set; }

        public virtual SSR_DocumentosTipos SSR_DocumentosTipos { get; set; }

        public virtual SSR_Servicios SSR_Servicios { get; set; }
    }
}
