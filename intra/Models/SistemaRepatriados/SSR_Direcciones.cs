namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_Direcciones
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DireccionId { get; set; }

        public int ProvinciaId { get; set; }

        public int MunicipioId { get; set; }

        public int CiudadId { get; set; }

        public int SectorId { get; set; }
        public int DistritoId { get; set; }

        [StringLength(255)]
        public string Calle { get; set; }

        [StringLength(10)]
        public string Numero { get; set; }

        public DateTime? FechaCreacion { get; set; }


        //public virtual SSR_DireccionesRepatriados SSR_DireccionesRepatriados { get; set; }

        //public virtual SSR_DireccionesFamiliares SSR_DireccionesFamiliares { get; set; }
    }
}
