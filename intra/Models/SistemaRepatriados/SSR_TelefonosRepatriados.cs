namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_TelefonosRepatriados
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TelefonoRepatriadoId { get; set; }

        public int TelefonoId { get; set; }

        public int RepatriadoId { get; set; }

        public bool? Estado { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public virtual SSR_Repatriados SSR_Repatriados { get; set; }

        public virtual SSR_Telefonos SSR_Telefonos { get; set; }
    }
}
