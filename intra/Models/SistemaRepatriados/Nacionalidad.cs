namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Nacionalidad : DbContext
    {
        public Nacionalidad()
            : base("name=Nacionalidad")
        {
        }

        public virtual DbSet<vw_Nacionalidades> vw_Nacionalidades { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<vw_Nacionalidades>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);
        }
    }
}
