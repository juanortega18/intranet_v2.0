namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_TelefonosTipos
    { 
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TelefonoTipoId { get; set; }

        [StringLength(20)]
        public string Descripcion { get; set; }
        //public virtual SSR_Telefonos SSR_Telefonos { get; set; }
    }
}
