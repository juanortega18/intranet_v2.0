namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_TelefonosFamiliares
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TelefonoFamiliarId { get; set; }

        public int TelefonoId { get; set; }

        public int FamiliarId { get; set; }

        public bool? Estado { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public virtual SSR_Familiares SSR_Familiares { get; set; }

        public virtual SSR_Telefonos SSR_Telefonos { get; set; }
        //public virtual SSR_TelefonosTipos TipoTelefonos { get; set; }
    }
}
