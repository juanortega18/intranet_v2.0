namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SistemaRepatriados : DbContext
    {
        public SistemaRepatriados()
            : base("name=Repatriados")
        {
        }

        public virtual DbSet<SSR_Direcciones> SSR_Direcciones { get; set; }
        public virtual DbSet<SSR_DireccionesFamiliares> SSR_DireccionesFamiliares { get; set; }
        public virtual DbSet<SSR_DireccionesRepatriados> SSR_DireccionesRepatriados { get; set; }
        public virtual DbSet<SSR_DocumentosEstados> SSR_DocumentosEstados { get; set; }
        public virtual DbSet<SSR_DocumentosTipos> SSR_DocumentosTipos { get; set; }
        public virtual DbSet<SSR_Familiares> SSR_Familiares { get; set; }
        public virtual DbSet<SSR_Instituciones> SSR_Instituciones { get; set; }
        public virtual DbSet<Parentesco> Parentesco { get; set; }
        public virtual DbSet<SSR_Repatriados> SSR_Repatriados { get; set; }
        public virtual DbSet<SSR_Servicios> SSR_Servicios { get; set; }
        public virtual DbSet<SSR_ServiciosDocumentos> SSR_ServiciosDocumentos { get; set; }
        public virtual DbSet<SSR_ServiciosEstados> SSR_ServiciosEstados { get; set; }
        public virtual DbSet<SSR_ServiciosTipos> SSR_ServiciosTipos { get; set; }
        public virtual DbSet<SSR_ServiciosTiposDocumentos> SSR_ServiciosTiposDocumentos { get; set; }
        public virtual DbSet<SSR_Telefonos> SSR_Telefonos { get; set; }
        public virtual DbSet<SSR_TelefonosFamiliares> SSR_TelefonosFamiliares { get; set; }
        public virtual DbSet<SSR_TelefonosRepatriados> SSR_TelefonosRepatriados { get; set; }
        public virtual DbSet<SSR_TelefonosTipos> SSR_TelefonosTipos { get; set; }
        public virtual DbSet<Pais> Pais { get; set; }


    }
}
