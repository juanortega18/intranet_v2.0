namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    

    public partial class Vw_Mostrar_Nacionalidades
    {
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Int16 ID_Nacionalidad { get; set; }

      
        [StringLength(50)]
        public string Descripcion { get; set; }

        public string Pais { get; set; }
    }
}
