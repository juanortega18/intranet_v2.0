namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_Instituciones
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InstitucionId { get; set; }

        [StringLength(100)]
        public string Descripcion { get; set; }

        public bool? Estado { get; set; }

        [StringLength(100)]
        public string UsuarioCreador { get; set; }

        public DateTime? FechaCreacion { get; set; }
       // public virtual SSR_Servicios SSR_Servicios { get; set; }
    }
}
