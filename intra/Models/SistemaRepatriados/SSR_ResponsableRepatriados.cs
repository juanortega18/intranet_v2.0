﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.SistemaRepatriados
{
    public partial class SSR_ResponsableRepatriados
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ResponsableRepatriadoId { get; set; }

        public int ServicioTipoId { get; set; }

        public string CodigoResponsable { get; set; }

        public string UsuarioResponsable { get; set; }

        public string CorreoResponsable { get; set; }

        public bool Estado { get; set; }

        public DateTime? FechaCreacion { get; set; }

        //public virtual SSR_Servicios SSR_Servicios { get; set; }
    }
}