namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_DireccionesFamiliares
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DireccionFamiliarId { get; set; }

        public int DireccionId { get; set; }

        public int FamiliarId { get; set; }

        public bool? Estado { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public virtual SSR_Direcciones SSR_Direcciones { get; set; }

        public virtual SSR_Familiares SSR_Familiares { get; set; }
    }
}
