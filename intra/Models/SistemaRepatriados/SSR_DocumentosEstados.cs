namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_DocumentosEstados
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DocumentoEstadoId { get; set; }

        [StringLength(60)]
        public string Descripcion { get; set; }

       // public virtual SSR_ServiciosDocumentos SSR_ServiciosDocumentos { get; set; }
    }
}
