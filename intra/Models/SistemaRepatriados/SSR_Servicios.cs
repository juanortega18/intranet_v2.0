namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_Servicios
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServicioId { get; set; }

        public int? ServicioTipoId { get; set; }

        public int RepatriadoId { get; set; }

        public int InstitucionId { get; set; }

        public int? ServicioEstadoId { get; set; }

        public string Notas { get; set; }

        public string Conclusion { get; set; }

        [StringLength(100)]
        public string UsuarioCreador { get; set; }

        [StringLength(10)]
        public string CodigoCreador { get; set; }

        [StringLength(100)]
        public string UsuarioResponsable { get; set; }

        [StringLength(10)]
        public string CodigoResponsable { get; set; }

        public DateTime? FechaFin { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public virtual SSR_Instituciones SSR_Instituciones { get; set; }

        public virtual SSR_Repatriados SSR_Repatriados { get; set; }

        public virtual SSR_ServiciosEstados SSR_ServiciosEstados { get; set; }

        //public virtual SSR_ServiciosDocumentos SSR_ServiciosDocumentos { get; set; }

        public virtual SSR_ServiciosTipos SSR_ServiciosTipos { get; set; }
    }
}
