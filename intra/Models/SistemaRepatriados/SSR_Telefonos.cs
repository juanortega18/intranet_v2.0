namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_Telefonos
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TelefonoId { get; set; }

        public int? TelefonoTipoId { get; set; }

        [StringLength(20)]
        public string Numero { get; set; }

        //public virtual SSR_TelefonosTipos SSR_TelefonosTipos { get; set; }
        //public virtual SSR_TelefonosRepatriados SSR_TelefonosRepatriados { get; set; }
        //public virtual SSR_TelefonosFamiliares SSR_TelefonosFamiliares { get; set; }
    }
}
