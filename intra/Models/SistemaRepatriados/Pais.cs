﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.SistemaRepatriados
{
    public class Pais
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Paisid { get; set; }
        public string PaisDescripcion { get; set; }
        public string PaisGentilicio { get; set; }
        public DateTime fhCreacion { get; set; }

    }
}