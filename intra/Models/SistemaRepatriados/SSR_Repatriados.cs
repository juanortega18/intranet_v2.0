namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_Repatriados
    {
        

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RepatriadoId { get; set; }

        [Required]
        [StringLength(50)]
        public string NumeroIdentificador { get; set; }

        public DateTime FechaRetorno { get; set; }

        [StringLength(100)]
        public string Nombres { get; set; }

        [StringLength(60)]
        public string Apellido1 { get; set; }

        [StringLength(60)]
        public string Apellido2 { get; set; }

        public int? TipoIdentificacionId { get; set; }

        [StringLength(20)]
        public string NumeroIdentificacion { get; set; }

        public DateTime? FechaNacimiento { get; set; }

        public bool? Sexo { get; set; }

        public int? PaisId { get; set; }

        public bool? Estado { get; set; }

        [StringLength(100)]
        public string UsuarioCreador { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public string MotivoRepatriacion { get; set; }
        public virtual string NombreCompleto { get { return this.Nombres + " " + this.Apellido1 + " " + this.Apellido2; } }
        public virtual Pais PaisProcedencia { get; set; }
        
       // public virtual SSR_DireccionesRepatriados SSR_DireccionesRepatriados { get; set; }

     //   public virtual SSR_Familiares SSR_Familiares { get; set; }

      //  public virtual SSR_Servicios SSR_Servicios { get; set; }

       // public virtual SSR_TelefonosRepatriados SSR_TelefonosRepatriados { get; set; }
    }
}
