namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_ServiciosTiposDocumentos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServicioTipoDocumentoId { get; set; }

        public int? ServicioTipoId { get; set; }

        public int? DocumentoTipoId { get; set; }

        public bool? Estado { get; set; }

        public virtual SSR_DocumentosTipos SSR_DocumentosTipos { get; set; }

        public virtual SSR_ServiciosTipos SSR_ServiciosTipos { get; set; }
    }
}
