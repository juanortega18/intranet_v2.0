namespace intra.Models.SistemaRepatriados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SSR_DireccionesRepatriados
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DireccionRepatriadoId { get; set; }

        public int DireccionId { get; set; }

        public int RepatriadoId { get; set; }

        public bool? Estado { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public virtual SSR_Direcciones SSR_Direcciones { get; set; }

        public virtual SSR_Repatriados SSR_Repatriados { get; set; }
    }
}
