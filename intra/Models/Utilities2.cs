﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace intra.Models
{
    public class Utilities2
    {
        public List<SelectListItem> select_list { get; set; }
        public string chain { get; set; }
        public string hidden_dependency { get; set; }
        public string hidden_department { get; set; }
        public string hidden_division { get; set; }
        public string hidden_section { get; set; }
    }
}