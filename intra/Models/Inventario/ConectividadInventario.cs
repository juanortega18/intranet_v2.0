﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{
    public partial class ConectividadInventario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConectividadId { get; set; }
        public string ConectividadServicio { get; set; }
        public string ConectividadNoContrato { get; set; }
        public string ConectividadTecnicoAsignado { get; set; }
        public string ConectividadUsuario { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime ConectividadFecha { get; set; }
        public int ProveedorId { get; set; }
        public int LocalidadId { get; set; }
        public int RegionId { get; set; }
        public int Province_ID { get; set; }
        public int Municipality_ID { get; set; }
        public int MedioId { get; set; }
        public int VelocidadId { get; set; }

    }
}
