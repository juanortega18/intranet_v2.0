﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{
    public partial class Municipalities
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Municipality_ID { get; set; }
        public int Province_ID { get; set; }
        public string Municipality_Code { get; set; }
        public string Municipality_Name { get; set; }
    }
}
