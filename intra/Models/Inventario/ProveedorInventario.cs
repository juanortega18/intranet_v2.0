﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{
    public partial class ProveedorInventario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProveedorId { get; set; }
        public string ProveedorNombre { get; set; }
        public string ProveedorTelefono { get; set; }
        public string ProveedorDireccion { get; set; }
        public string ProveedorUsuario { get; set; }
        public DateTime ProveedorFecha { get; set; }

    }
}
