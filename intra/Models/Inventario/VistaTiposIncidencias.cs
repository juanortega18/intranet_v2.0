﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{
    [Table("VistaTiposIncidencias")]
    public partial class VistaTiposIncidencias
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IncidenciasTipoId { get; set; }
        public string IncidenciasTipoNombre { get; set; }
        public int SuplidorId { get; set; }
        public string SuplidorNombre { get; set; }

    }
}
