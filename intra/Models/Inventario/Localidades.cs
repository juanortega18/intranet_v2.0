﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{
    [Table("Localidades")]
    public partial class Localidades
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LocalidadId { get; set; }
        public string LocalidadNombre { get; set; }
        public string LocalidadTelefono { get; set; }
        public string LocalidadDireccion { get; set; }
        public int Province_ID { get; set; }
        public string Province_Name { get; set; }
        public int Municipality_ID { get; set; }
        public string Municipality_Name { get; set; }
        public string LocalidadContacto { get; set; }

    }
}
