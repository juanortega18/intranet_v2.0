﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{
    [Table("MarcasInventarios")]
    public partial class MarcasInventario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MarcaId { get; set; }
        [Required(ErrorMessage = "Marca Descripción es un campo obligatorio")]
        public string MarcaDescripción { get; set; }
        public DateTime MarcaFecha { get; set; }
        public string MarcaUsuario { get; set; }
        [Required(ErrorMessage = "Tipo Equipo es un campo obligatorio")]
        public string MarcaTipo { get; set; }

    }
}
