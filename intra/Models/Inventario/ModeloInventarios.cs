﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{

    [Table("ModeloInventarios")]
    public partial class ModeloInventario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ModeloId { get; set; }
        [Required(ErrorMessage = "Se requiere un tipo de modelo para la marca")]
        public string ModeloDescripcion { get; set; }
        public DateTime? ModeloFecha { get; set; }
        public string ModeloUsuario { get; set; }
        [ForeignKey("MarcasInventario")]
        [Required(ErrorMessage = "Se requiere seleccionar una marca")]
        public int MarcaId { get; set; }

        public virtual MarcasInventario MarcasInventario { get; set; }
    }
}
