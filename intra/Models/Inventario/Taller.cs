﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{
    [Table("Taller")]
    public partial class Taller
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TallerId { get; set; }
        public DateTime TallerFhCreacion { get; set; }
        [Display(Name = "Fecha de Solución")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime TallerFhSolucion { get; set; }
        [Display(Name = "Fecha de Entrega")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime TallerFhEntrega { get; set; }
        [Display(Name = "Razón de Entrada")]
        public string TallerRazonEntrada { get; set; }
        [Display(Name = "Usuario Recibe")]
        public string UsuarioRecibe { get; set; }
        [Display(Name = "Descripcion Solucion")]
        public string TallerDescripcionSolucion { get; set; }
        [Required(ErrorMessage = "El campo Serie es obligatorio.")]
        public string Serie { get; set; }
        public string TallerRazonSalida { get; set; }
        public int MarcaId { get; set; }
        public int ModeloId { get; set; }
        [Display(Name = "Localidad")]
        public int? LocalidadId { get; set; }
        [Display(Name = "Región")]
        public int? RegionId { get; set; }
        [Display(Name = "Provincia")]
        public int? Province_ID { get; set; }
        [Display(Name = "Municipio")]
        public int? Municipality_ID { get; set; }
        public string TecnicoOResponsable { get; set; }
        [Display(Name = "Estatus")]
        public int EstatusId { get; set; }
        public int? DepartamentoId { get; set; }
        public int DeptoLocal { get; set; }
        [StringLength(15)]
        public string ActivoFijo { get; set; }

    }
}
