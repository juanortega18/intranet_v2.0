﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{
    public partial class VistaConectividads
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConectividadId { get; set; }
        public string ConectividadServicio { get; set; }
        public string ConectividadNoContrato { get; set; }
        public string ConectividadTecnicoAsignado { get; set; }
        public DateTime ConectividadFecha { get; set; }
        public string ConectividadUsuario { get; set; }
        public int LocalidadId { get; set; }
        public string LocalidadNombre { get; set; }
        public int ProveedorId { get; set; }
        public string ProveedorNombre { get; set; }
        public int Province_ID { get; set; }
        public string Province_Name { get; set; }
        public string RegionDescripcion { get; set; }
        public int RegionId { get; set; }
        public int Municipality_ID { get; set; }
        public string Municipality_Name { get; set; }

    }

   
}
