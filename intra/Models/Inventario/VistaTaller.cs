﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{
    [Table("VistaTaller")]
    public partial class VistaTaller
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TallerId { get; set; }
        public DateTime TallerFhSolucion { get; set; }
        public DateTime TallerFhEntrega { get; set; }
        public DateTime TallerFhCreacion { get; set; }
        public string TallerRazonEntrada { get; set; }
        public string TallerRazonSalida { get; set; }
        public string UsuarioRecibe { get; set; }
        public string TallerDescripcionSolucion { get; set; }
        public string Serie { get; set; }
        public int MarcaId { get; set; }
        public string MarcaDescripción { get; set; }
        public int ModeloId { get; set; }
        public string ModeloDescripcion { get; set; }
        public int? LocalidadId { get; set; }
        public string LocalidadNombre { get; set; }
        public int? RegionId { get; set; }
        public string RegionDescripcion { get; set; }
        public int? Province_ID { get; set; }
        public string Province_Name { get; set; }
        public int? Municipality_ID { get; set; }
        public string Municipality_Name { get; set; }
        public string TecnicoOResponsable { get; set; }
        public int EstatusId { get; set; }
        public string EstatusDescripcion { get; set; }
        public int? DepartamentoId { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual Sol_Departamento Sol_Departamento { get; set; }
        public int DeptoLocal { get; set; }

    }

}
