﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{
    public partial class VistaComputadoraInventario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ComputadoraId { get; set; }
        public string ComputadoraSerie { get; set; }
        public string ComputadoraServiceTag { get; set; }
        public DateTime ComputadoraGarantia { get; set; }
        public DateTime ComputadoraFechaCompra { get; set; }
        public DateTime ComputadoraFechaCreación { get; set; }
        public string ComputadoraUsuario { get; set; }
        public int MarcaId { get; set; }
        public string MarcaDescripción { get; set; }
        public int ModeloId { get; set; }
        public string ModeloDescripcion { get; set; }
        public int EstatusId { get; set; }
        public string EstatusDescripcion { get; set; }
        public string CaracteristicaComputadora { get; set; }
        public int LocalidadId { get; set; }
        public string LocalidadNombre { get; set; }
        public string ComputadoraNombreRed { get; set; }
        public string RegionDescripcion { get; set; }
        public int RegionId { get; set; }
        public int Province_ID { get; set; }
        public string Province_Name { get; set; }
        public int Municipality_ID { get; set; }
        public string Municipality_Name { get; set; }
        public int ProveedorId { get; set; }
        public string ProveedorNombre { get; set; }
    }
}
