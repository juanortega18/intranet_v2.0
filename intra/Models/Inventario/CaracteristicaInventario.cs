﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{
    public partial class CaracteristicaInventario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CaracteristicaId { get; set; }
        public string CaracteristicaDescripcion { get; set; }
        public string CaracteristicaUsuario { get; set; }
        public DateTime CaracteristicaFecha { get; set; }
        public int EstatusId { get; set; }

    }
}
