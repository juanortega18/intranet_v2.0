﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{

    [Table("Localidad")]
    public partial class Localidad
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LocalidadId { get; set; }
        public string LocalidadNombre { get; set; }
        public string LocalidadDireccion { get; set; }
        public string LocalidadTelefono { get; set; }
        public int Municipality_ID { get; set; }
        public string LocalidadContacto { get; set; }

    }
}
