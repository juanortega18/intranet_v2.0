﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Inventario
{
    public partial class VistaEquiposReds
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EquipoId { get; set; }
        public string EquipoDescripcion { get; set; }
        public DateTime EquipoGarantia { get; set; }
        public DateTime EquipoFechaCompra { get; set; }
        public DateTime EquipoFechaCreación { get; set; }
        public string EquipoUsuario { get; set; }
        public int MarcaId { get; set; }
        public string MarcaDescripción { get; set; }
        public int ModeloId { get; set; }
        public string ModeloDescripcion { get; set; }
        public int EstatusId { get; set; }
        public string EstatusDescripcion { get; set; }
        public string EquipoSerie { get; set; }
        public int LocalidadId { get; set; }
        public string LocalidadNombre { get; set; }
        public string CaracteristicaEquipo { get; set; }
        public string EquipoServiceTag { get; set; }
        public string RegionDescripcion { get; set; }
        public int RegionId { get; set; }
        public int Province_ID { get; set; }
        public string Province_Name { get; set; }
        public int Municipality_ID { get; set; }
        public string Municipality_Name { get; set; }
        public int ProveedorId { get; set; }
        public string ProveedorNombre { get; set; }

    }
}
