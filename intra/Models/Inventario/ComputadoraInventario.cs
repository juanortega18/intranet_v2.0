﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace intra.Models.Inventario
{
    public partial class ComputadoraInventario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ComputadoraId { get; set; }
        public string ComputadoraSerie { get; set; }
        public string ComputadoraServiceTag { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime ComputadoraGarantia { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime ComputadoraFechaCompra { get; set; }
        public DateTime ComputadoraFechaCreación { get; set; }
        public string ComputadoraUsuario { get; set; }
        public int MarcaId { get; set; }
        public int ModeloId { get; set; }
        public int EstatusId { get; set; }
        public string CaracteristicaComputadora { get; set; }
        public int LocalidadId { get; set; }
        public int ProveedorId { get; set; }
        public string ComputadoraNombreRed { get; set; }
        public int RegionId { get; set; }
        public int Province_ID { get; set; }
        public int Municipality_ID { get; set; }
    }
}
