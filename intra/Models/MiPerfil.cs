﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using intra.Models.Nomina;

namespace intra.Models
{
    public class MiPerfil
    {
        public MiPerfil()
        {
            ponches = new List<sp_report_ponches_Result>();
        }

        /*Ponches properties*/
        [NotMapped]
        public List<sp_report_ponches_Result> ponches { get; set; }


        /*Nomina properties*/
        [NotMapped]
        public int nomina_value { get; set; }

        [NotMapped]
        public SelectList NominaDropDownValues { get; set; }

        [NotMapped]
        public string ShowTable { get; set; }

        [NotMapped]
        public NominaIntranet CurrentNomina { get; set; }
        
    }
}