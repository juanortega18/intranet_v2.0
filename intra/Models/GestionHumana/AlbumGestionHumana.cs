﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.GestionHumana
{
    [Table("AlbumGestionHumana")]
    public class AlbumGestionHumana
    {
        public AlbumGestionHumana()
        {
            FotoGestionHumana = new List<FotoGestionHumana>();
            FotoVista = new List<HttpPostedFileBase>();
        }

        [Key]
        public int AlbumGHId { get; set; }

        [StringLength(100)]
        public string AlbumGHNombre { get; set; }

        public DateTime? AlbumGHFhCreacion { get; set; }

        public int? AlbumGHUsuarioCreador { get; set; }
        public bool AlbumEstado { get; set; }

        public IEnumerable<FotoGestionHumana> FotoGestionHumana { get; set; }

        [NotMapped]
        public List<HttpPostedFileBase> FotoVista { get; set; }
    }
}