﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.GestionHumana
{
    [Table("CarpetasDocumentosGH")]
    public class CarpetasDocumentosGH
    {
        public CarpetasDocumentosGH(){
            DocumentosGestionHumana = new List<DocumentoGestionHumana>();
        }

        [Key]
        public int CarpetaId { get; set; }
        public int? CarpetaRaizId { get; set; }
        [Required(ErrorMessage = "El Nombre de la Carpeta es obligatorio.")]
        public string CarpetaNombre { get; set; }
        public bool Estado { get; set; }
        public List<DocumentoGestionHumana> DocumentosGestionHumana { get; set; }
    }
}