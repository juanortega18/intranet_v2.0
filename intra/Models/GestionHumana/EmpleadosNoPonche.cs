namespace intra.Models.GestionHumana
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EmpleadosNoPonche")]
    public partial class EmpleadosNoPonche
    {
        [Key]
        public int EmpNoPoncheId { get; set; }

        public int EmpNoPoncheCodigo { get; set; }

        public int? EmpNoPoncheEstatus { get; set; }

        public string EmpNoPoncheDescripcion { get; set; }

        [NotMapped]
        public string EmpNoPoncheEmpleado { get; set; }

        [NotMapped]
        public string jomh { get; set; }

        //.
    }
}
