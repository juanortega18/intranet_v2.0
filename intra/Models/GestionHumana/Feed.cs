﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana
{
    public class Feed
    {
        public string titulo { get; set; }
        [Key]
        public string link { get; set; }
        public string descripcion { get; set; }
    }
}
