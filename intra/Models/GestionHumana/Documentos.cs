﻿namespace intra.Models.GestionHumana
{
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
    public class Documentos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DocId { get; set; }
        public string DocTitulo { get; set; }
        public byte[] DocArchivo { get; set; }
        public string DocTipo { get; set; }

        [NotMapped]
        public HttpPostedFileBase ArchivoVista { get; set; }
        //[NotMapped]
        //public string String64 { get; set; }
    }
}
