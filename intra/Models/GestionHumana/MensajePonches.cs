namespace intra.Models.GestionHumana
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MensajePonches
    {
        public int MensajePonchesId { get; set; }

        [StringLength(50)]
        public string MensajePonchesTipo { get; set; }

        public string MensajePonchesDescripcion { get; set; }
    }
}
