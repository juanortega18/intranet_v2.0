﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.GestionHumana
{
    [Table("FotoGestionHumana")]
    public class FotoGestionHumana
    {
        [Key]
        public int FotoGHId { get; set; }

        [StringLength(100)]
        public string FotoGHNombre { get; set; }

        [StringLength(10)]
        public string FotoGHExtension { get; set; }

        public byte[] FotoGHArchivo { get; set; }
        public DateTime? FotoGHFhSubida { get; set; }
        public decimal? FotoGHAncho { get; set; }
        public decimal? FotoGHAlto { get; set; }
        public int AlbumGHId { get; set; }
        
        [StringLength(100)]
        public string FotoGHDescripcion { get; set; }
        public DateTime? FotoGHFhCreacion { get; set; }
        public int? FotoGHUsuarioCreador { get; set; }
        public virtual AlbumGestionHumana AlbumGestionHumana { get; set; }
        public bool FotoEstado { get; set; }

        [NotMapped]
        public List<HttpPostedFileBase> FotoVista { get; set; }
    }
}