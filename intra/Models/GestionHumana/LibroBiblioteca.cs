﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace intra.Models.GestionHumana
{
    [Table("LibroBiblioteca")]
    public class LibroBiblioteca
    {
        [Key]
        public int LibroBibliotecaId { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Nombre del Libro")]
        public string LibroBibliotecaNombre { get; set; }

        [Display(Name = "Peso del Libro")]
        public int? LibroBibliotecaPeso { get; set; }

        public string LibroBibliotecaRuta { get; set; }

        public bool? LibroBibliotecaExterno { get; set; }

        public DateTime? LibroBibliotecaFhCreacion { get; set; }

        public byte[] LibroBibliotecaImagen { get; set; }

        [Display(Name = "Sección")]
     
        public int? SeccionBibliotecaId { get; set; }

        [Display(Name = "Autor")]
    
        public int? AutorLBId { get; set; }

        public virtual AutorLibroBiblioteca AutorLibroBiblioteca { get; set; }

        public virtual SeccionBiblioteca SeccionBiblioteca { get; set; }
        [NotMapped]
        public HttpPostedFileBase LibroVista { get; set; }
        [NotMapped]
        public HttpPostedFileBase LibroArchivo { get; set; }
    }
}