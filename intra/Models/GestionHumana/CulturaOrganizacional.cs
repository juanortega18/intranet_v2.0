﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.GestionHumana
{
    [Table("CulturaOrganizacional")]
    public class CulturaOrganizacional
    {
        [Key]
        public int CulOrgId { get; set; }

        [StringLength(50)]
        [Required]
        [Display(Name = "Nombre")]
        public string CulOrgNombre { get; set; }

        [Display(Name = "Descripción")]
        public string CulOrgDescripcion { get; set; }

        [Display(Name = "Fecha de Creación")]
        public DateTime? CulOrgFhCreacion { get; set; }

        [Display(Name = "Ultima Modificación")]
        public DateTime? CulOrgFhModificacion { get; set; }

        [Required]
        [Display(Name = "Tipo")]
        public int CulOrgTipoId { get; set; }

        public virtual CulturaOrganizacionalTipo CulturaOrganizacionalTipo { get; set; }
    }
}