namespace intra.Models.GestionHumana
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DFMPModel : DbContext
    {
        public DFMPModel()
            : base("name=intra")
        {
        }

        public virtual DbSet<DiasFeriados> DiasFeriados { get; set; }
        public virtual DbSet<MensajePonches> MensajePonches { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
