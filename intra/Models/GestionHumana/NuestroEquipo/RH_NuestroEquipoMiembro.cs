﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.GestionHumana.NuestroEquipo
{
    public class RH_NuestroEquipoMiembro
    {
        [Key]
        public int MiembroID { get; set; }
        public int EmpleadoID { get; set; }
        public string Ext { get; set; }
        public string Flota { get; set; }
        public int NuestroEquipoDepartamentoID { get; set; }
        public bool Estado { get; set; }
        public DateTime FhCreacion { get; set; }
        [NotMapped]
        public VISTA_EMPLEADOS empleado { get; set; }
        [ForeignKey("NuestroEquipoDepartamentoID")]
        public virtual RH_NuestroEquipoDepartamento NuestroEquipoDepartamento { get; set; }
    }
}