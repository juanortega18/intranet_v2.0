﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace intra.Models.GestionHumana.NuestroEquipo
{
    public class RH_NuestroEquipo
    {
        [Key]
        public int NuestroEquipoID { get; set; }
        public string NuestroEquipoDescripcion { get; set; }
        public string NuestroEquipoFoto { get; set; }
        public DateTime NuestroEquipoFhModificacion { get; set; }
    }
}