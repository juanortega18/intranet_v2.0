﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace intra.Models.GestionHumana.NuestroEquipo
{
    public class RH_NuestroEquipoDepartamento
    {
        [Key]
        public int NuestroEquipoDepartamentoID { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public DateTime FhCreacion { get; set; }
        public virtual List<RH_NuestroEquipoMiembro> NuestroEquipoMiembro { get; set; }
    }
}