﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana
{
   [Table("EmpleadoViaticos")]
   public class EmpleadoViaticos
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int EmpleadoViaticoId { get; set; }
        public int SolicitudId { get; set; }
        public int EmpleadoId { get; set; }
        public decimal? MontoViaticos { get; set; }
        public DateTime? FechaSolicitud { get; set; }

        //public List<VISTA_EMPLEADOS>EquipoViaticos { get; set; }

    }
}
