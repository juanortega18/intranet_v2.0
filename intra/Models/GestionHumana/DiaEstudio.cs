﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.GestionHumana
{
    [Table("DiaEstudio")]
    public class DiaEstudio
    {
        [Key]
        public int DiaEstudioId { get; set; }
        public int DiaEstudioPermisoId { get; set; }
        public string DiaEstudioNombre { get; set; }
        public string DiaEstudioHoraInicio { get; set; }
        public string DiaEstudioHoraFin { get; set; }
        public bool DiaEstudioEstado { get; set; }
    }
}