﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using intra.Models.vw_Models;
using intra.Models.ActualizacionDatos;

namespace intra.Models.GestionHumana
{
    public partial class VISTA_EMPLEADOS
    {
        [Key]
        public string empleadoid { get; set; }
        public string nombre { get; set; }
        public string telefono1 { get; set; }
        public string cedula { get; set; }
        public string cargoid { get; set; }
        public string departamentoid { get; set; }
        public DateTime fechaingreso { get; set; }
        public int id_persona { get; set; }
        public string cargo { get; set; }
        public string dependencia { get; set; }
        public string dependenciaid { get; set; }
        public string departamento { get; set; }
        public decimal salario { get; set; }
        public int estatus { get; set; }
        public string Sexo { get; set; }
        public string Direccion { get; set; }
        public DateTime Cumpleano { get; set; }
        public string EstadoCivil { get; set; }
        public string Sector { get; set; }
        public string Email { get; set; }
        public string TiposSangre { get; set; }
        public string Telefono2 { get; set; }
        public string Celular { get; set; }
        public string Profesion { get; set; }
        public int? Dependiente { get; set; }
        public int? ArsId { get; set; }
        public int? AfpId { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
        public string Observaciones { get; set; }
        public int? NivelAcademicoId { get; set; }
        public int? ProvinciaId { get; set; }
        public int? MunicipioId { get; set; }
        public int? CiudadId { get; set; }
        public int? SectorId { get; set; }
        public int? DistritoId { get; set; }
        [NotMapped]
        public vw_Dependencias vw_Dependencias { get; set; }
        [NotMapped]
        public vw_Cargos vw_Cargos { get; set; }
        [NotMapped]
        public vw_Departamentos vw_Departamentos { get; set; }

        public virtual vw_Provinces vw_Provinces { get; set; }
       
        public virtual vw_Municipios vw_Municipios { get; set; }
       
       // public virtual vw_Distritos vw_Distritos { get; set; }
   
        public virtual vw_Ciudad vw_Ciudad { get; set; }
    
        public virtual ARS ARS { get; set; }
 
        public virtual AFP AFP { get; set; }

        public virtual NivelAcademico Nivel_Academico { get; set; }
        
        public virtual vw_Sectores vw_Sectores { get; set; }
        //public virtual Profesiones Profesiones { get; set; }
    }

}
