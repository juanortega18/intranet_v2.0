namespace intra.Models.GestionHumana
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DiasFeriados
    {
        public int DiasFeriadosId { get; set; }

        //[DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime DiasFeriadosFecha { get; set; }

        [StringLength(60)]
        public string DiasFeriadosDescripcion { get; set; }

        [StringLength(10)]
        public string DiasFeriadosCodigoUsuario { get; set; }

        [StringLength(20)]
        public string DiasFeriadosNombreUsuario { get; set; }

        //Property for use the salt
        [NotMapped]
        public string qken { get; set; }
    }
}
