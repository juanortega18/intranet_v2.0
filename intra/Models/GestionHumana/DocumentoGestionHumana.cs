﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.GestionHumana
{
    [Table("DocumentoGestionHumana")]
    public class DocumentoGestionHumana
    {
        [Key]
        public int DocGHId { get; set; }

        [Required(ErrorMessage = "El campo Descripción es obligatorio.")]
        [StringLength(100)]
        public string DocGHDescripcion { get; set; }

        [StringLength(10)]
        public string DocGHExtension { get; set; }
        public bool Estado { get; set; }

        public DateTime? DocGHFhCreacion { get; set; }

        public int? DocGHUsuarioCreador { get; set; }

        public int? CarpetaId { get; set; }

        [StringLength(60)]
        public string DocGHNombreUsuarioCreador { get; set; }

        public DateTime? DocGHFhModificacion { get; set; }

        public int? DocGHUsuarioModificador { get; set; }

        [StringLength(60)]
        public string DocGHNombreUsuarioModificador { get; set; }

        public byte[] DocGHArchivo { get; set; }

        [StringLength(100)]
        public string DocGHArchivoNombre { get; set; }

        public virtual CarpetasDocumentosGH Carpeta { get; set; }

        [NotMapped]
        public HttpPostedFileBase DocGHArchivoVista { get; set; }
        [NotMapped]
        public string String64 { get; set; }
    }
}