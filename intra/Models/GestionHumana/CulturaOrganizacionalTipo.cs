﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.GestionHumana
{
    [Table("CulturaOrganizacionalTipo")]
    public class CulturaOrganizacionalTipo
    {
        public CulturaOrganizacionalTipo()
        {
            CulturaOrganizacional = new List<CulturaOrganizacional>();
        }
        [Key]
        public int CulOrgTipoId { get; set; }

        public string CulOrgTipoNombre { get; set; }

        public ICollection<CulturaOrganizacional> CulturaOrganizacional { get; set; }

    }
}