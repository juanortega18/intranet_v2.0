﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana
{

    public partial class sp_users_ldapID
    {
        public string employeeid { get; set; }
        public string department { get; set; }
        public string displayName { get; set; }
        [Key]
        public string samAccountName { get; set; }
        public string mail { get; set; }
    }

}
