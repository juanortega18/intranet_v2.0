﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace intra.Models.GestionHumana
{
    [Table("SeccionBiblioteca")]
    public class SeccionBiblioteca : IValidatableObject
    {
        public SeccionBiblioteca()
        {
            LibroBiblioteca = new List<LibroBiblioteca>();
        }

        [Key]
        public int SeccionBibliotecaId { get; set; }
        [Required]
        [Display(Name = "Descripción de la Seccíon")]
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string SeccionBibliotecaDescripcion { get; set; }
        public byte[] SeccionBibliotecaImagen { get; set; }
        public ICollection<LibroBiblioteca> LibroBiblioteca { get; set; }

        [NotMapped]
        public HttpPostedFileBase SeccionVista { get; set; }

        // Validacion Unique
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validacion)
        {
            dbIntranet db = new dbIntranet();
            var validacionNombre = db.SeccionBiblioteca.FirstOrDefault
            (x => x.SeccionBibliotecaDescripcion == SeccionBibliotecaDescripcion && x.SeccionBibliotecaId != SeccionBibliotecaId);
            if (validacionNombre != null)
            {
                ValidationResult errorMessage = new ValidationResult
                ("Esta sección ya esta creada.", new[] { "SeccionBibliotecaDescripcion" });
                yield return errorMessage;
            }
            else
            {
                yield return ValidationResult.Success;
            }
        }
    }
}