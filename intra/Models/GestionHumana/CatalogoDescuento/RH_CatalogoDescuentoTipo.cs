namespace intra.Models.GestionHumana.CatalogoDescuento
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RH_CatalogoDescuentoTipo
    {

        [Key]
        public int CatDescuentoTipoId { get; set; }

        public bool Estado { get; set; }

        public DateTime FechaCreacion { get; set; }

        [StringLength(100)]
        public string CatDescuentoDescripcion { get; set; }

        [StringLength(500)] // Remover columna si no la vuelven a pedir
        public string CatInformacionAdicional { get; set; }

        [StringLength(100)]
        public string DescuentoTipoIcono { get; set; }

        public virtual List<RH_CatalogoDescuento> CatalogoDescuentos { get; set; }

        public virtual ICollection<RH_EmpresaCatalogo> RH_EmpresaCatalogo { get; set; }
    }
}
