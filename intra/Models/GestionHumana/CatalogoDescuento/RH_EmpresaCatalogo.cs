namespace intra.Models.GestionHumana.CatalogoDescuento
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RH_EmpresaCatalogo
    {
        [Key]
        public int EmpresaCatalogoId { get; set; }

        public bool Estado { get; set; }

        public DateTime FechaCreacion { get; set; }

        [StringLength(100)]
        public string EmpresaNombre { get; set; }

        [StringLength(250)]
        public string EmpresaLogo { get; set; }

        public virtual List<RH_CatalogoDescuento> RH_CatalogoDescuento { get; set; }

        public virtual ICollection<RH_CatalogoDescuentoTipo> RH_CatalogoDescuentoTipo { get; set; }
    }
}
