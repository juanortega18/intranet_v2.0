namespace intra.Models.GestionHumana.CatalogoDescuento
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RH_CatalogoDescuento
    {
        [Key]
        public int CatDescuentoId { get; set; }

        public int CatUsuarioId { get; set; }

        public int EmpresaCatalogoId { get; set; }

        public int CatDescuentoTipoId { get; set; }

        public bool Estado { get; set; }

        public DateTime FechaCreacion { get; set; }

        public DateTime FechaExpiracion { get; set; }

        public string CatDescripcion { get; set; }

        public string CatInformacionSucursal { get; set; }

        public virtual RH_EmpresaCatalogo RH_EmpresaCatalogo { get; set; }

        public virtual RH_CatalogoDescuentoTipo CatalogoDescuentoTipo { get; set; }
    }
}
