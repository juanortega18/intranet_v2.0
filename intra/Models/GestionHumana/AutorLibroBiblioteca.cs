﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.GestionHumana
{
    [Table("AutorLibroBiblioteca")]
    public class AutorLibroBiblioteca : IValidatableObject
    {
        public AutorLibroBiblioteca()
        {
            LibroBiblioteca = new List<LibroBiblioteca>();
        }

        [Key]
        public int AutorLBId { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Nombre del Autor")]
        public string AutorLBNombre { get; set; }
        public ICollection<LibroBiblioteca> LibroBiblioteca { get; set; }

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validacion)
        {
            dbIntranet db = new dbIntranet();
            var validacionNombre = db.AutorLibroBiblioteca.FirstOrDefault
            (x => x.AutorLBNombre == AutorLBNombre && x.AutorLBId != AutorLBId);
            if (validacionNombre != null)
            {
                ValidationResult errorMessage = new ValidationResult
                ("Este autor ya ha sido ingresado", new[] { "AutorLBNombre" });
                yield return errorMessage;
            }
            else
            {
                yield return ValidationResult.Success;
            }
        }
    }
}