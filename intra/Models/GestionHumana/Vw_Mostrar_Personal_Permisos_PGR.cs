﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana
{
    public partial class Vw_Mostrar_Personal_Permisos_PGR
    {
        public Vw_Mostrar_Personal_Permisos_PGR()
        {
            ponches = new List<sp_report_ponches_Result>();
        }

        [Key]
        public string Codigo { get; set; }
        public string NombreCompleto { get; set; }
        public string Cedula { get; set; }
        public string CodigoDependencia { get; set; }
        public string Dependencia { get; set; }
        public string CodigoDepartamento { get; set; }
        public string Departamento { get; set; }
        public string CodigoDivision { get; set; }
        public string Division { get; set; }
        public string CodigoSeccion { get; set; }
        public string Seccion { get; set; }

        [NotMapped]
        public List<sp_report_ponches_Result> ponches { get; set; }

        [NotMapped]
        public string FechaInicio { get; set; }

        [NotMapped]
        public string FechaFin { get; set; }
        [NotMapped]
        public int TecnicoId { get; set; }

    }

}
