﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace intra.Models.GestionHumana
{
    [Table("EmpleadoExcepcion")]
   public class EmpleadoHorarioEspecial
    {
    
        [Key]
        //public int EmpleadoExcepcionId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmpleadoId { get; set; }
        [StringLength(100)]
        [Display(Name ="Nombre")]
        public string EmpExeNombre { get; set; }
        [StringLength(20)]
        [Display(Name ="HorarioEntrada")]
        public string EmpExeHoraEntrada { get; set; }
        [StringLength(20)]
        [Display(Name ="HorarioSalida")]
        public string EmpExeHoraSalida { get; set; }
    }
}
