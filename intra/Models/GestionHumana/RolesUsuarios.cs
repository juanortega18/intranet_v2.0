﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana
{

    public partial class RolesUsuarios
    {
        [Key]
        [Column(Order = 0)]
        public string UserName { get; set; }
        //[Key]
        //[Column(Order = 1)]
        public string Rol { get; set; }
        public DateTime ruFecha { get; set; }
    }
}


