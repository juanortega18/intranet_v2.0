namespace intra.Models.GestionHumana.ConcursoMadres
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using intra.Models.vw_Models;

    public partial class RH_ConcursoMadres
    {
        [Key]
        
        public int ConcursanteId { get; set; }

        [Required]
        [StringLength(13)]
        public string Cedula { get; set; }

        public int EmpleadoId { get; set; }

        [Required]
        [StringLength(70)]
        public string Nombres { get; set; }

        public int? CantidadHijos { get; set; }

        public int Dependencia { get; set; }

        public int Departamento { get; set; }

        [Required]
        [StringLength(20)]
        public string NumeroTelefono { get; set; }

        [Required]
        [StringLength(50)]
        public string Correo { get; set; }

        [Required]
        public string Descripcion { get; set; }

        [Required]
        [StringLength(200)]
        public string Imagen { get; set; }

        public int EstadoConcursoId { get; set; }

        public DateTime FechaCreacion { get; set; }
        public int? ConteoFinal { get; set; }
        public int Año { get; set; }
        public string MotivoCancelacion { get; set; }
        public virtual RH_EstadoConcursoMadres RH_EstadoConcursoMadres { get; set; }

        public virtual List<RH_VotosMadres> Votos { get; set; }

        
    }
}
