namespace intra.Models.GestionHumana.ConcursoMadres
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RH_EstadoConcursoMadres
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RH_EstadoConcursoMadres()
        {
            RH_ConcursoMadres = new HashSet<RH_ConcursoMadres>();
        }

        [Key]
        public int EstadoConcursoId { get; set; }

        [Required]
        [StringLength(50)]
        public string EstadoConcursoDescripcion { get; set; }

        public DateTime FechaCreacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RH_ConcursoMadres> RH_ConcursoMadres { get; set; }
    }
}
