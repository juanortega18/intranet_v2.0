namespace intra.Models.GestionHumana.ConcursoMadres
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RH_VotosMadres
    {
        [Key]
        public int VotoId { get; set; }

        public int ConcursanteId { get; set; }

        public int VotoConteo { get; set; }

        public int UsuarioId { get; set; }
    }
}
