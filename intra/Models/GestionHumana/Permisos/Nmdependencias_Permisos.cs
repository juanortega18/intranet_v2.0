﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana.Permisos
{

    [Table("Nmdependencias_Permisos")]
    public class Nmdependencias_Permisos
    {
        [Key]
        public int ID { get; set; }
        public int DependenciaID { get; set; }
        //public string Descripcion { get; set; }
        public int? DirectorID { get; set; }

        public int? AuxiliarID { get; set; }

        #region Not mapped fields
        //Not mapped fields
        [NotMapped]
        public int? PoncheStatus { get; set; }
        [NotMapped]
        public int? PermisosVacacionesStatus { get; set; }
        //public DateTime FechaModificacion { get; set; }

        [NotMapped]
        public string DirectorDependencia { get; set; }

        [NotMapped]
        public string AuxiliarDependencia { get; set; }

        [NotMapped]
        public int DeptoID { get; set; }

        [NotMapped]
        public string EncargadoDepartamento { get; set; }

        [NotMapped]
        public string AuxiliarDepartamento { get; set; }

        [NotMapped]
        public int DivisionID { get; set; }

        [NotMapped]
        public string EncargadoDivision { get; set; }

        [NotMapped]
        public string AuxiliarDivision { get; set; }

        [NotMapped]
        public int SeccionID { get; set; }

        [NotMapped]
        public string EncargadoSeccion { get; set; }

        [NotMapped]
        public string AuxiliarSeccion { get; set; }

        [NotMapped]
        public string NombreTablaAGuardar { get; set; }

        [NotMapped]
        public int PonchesResponsible { get; set; }

        [NotMapped]
        public int PermisosVacacionesResponsible { get; set; }

        [NotMapped]
        public int CurrentPonchesResponsible { get; set; }

        [NotMapped]
        public int CurrentPermisosVacacionesResponsible { get; set; }
        #endregion
    }

}
