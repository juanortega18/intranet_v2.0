﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana.Permisos
{

    [Table("DependenciaActividad")]
    public class DependenciaActividad
    {
        [Key]
        public int DependenciaActividadID { get; set; }
        public int DependenciaID { get; set; }
        public int ActividadEncargadoID { get; set; }
        public int EncargadoStatusID { get; set; }
    }
}
