﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana.Permisos
{

    [Table("DivisionActividad")]
    public class DivisionActividad
    {
        [Key]
        public int DivisionActividadID { get; set; }
        public int DependenciaID { get; set; }
        public int DepartamentoID { get; set; }
        public int DivisionID { get; set; }
        public int ActividadEncargadoID { get; set; }
        public int EncargadoStatusID { get; set; }
    }
}
