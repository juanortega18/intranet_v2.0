﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana.Permisos
{
    [Table("NmSecciones_Permisos")]
    public class NmSecciones_Permisos
    {
        [Key]
        public int ID { get; set; }
        public int SeccionID { get; set; }
        public int DivisionID { get; set; }
        public int DeptoID { get; set; }
        public int DependenciaID { get; set; }
        //public string Descripcion { get; set; }
        public int? EncargadoID { get; set; }
        public int? AuxiliarID { get; set; }
        //public DateTime FechaModificacion { get; set; }

        [NotMapped]
        public int? PoncheStatus { get; set; }

        [NotMapped]
        public int? PermisosVacacionesStatus { get; set; }
    }
}
