﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace intra.Models.GestionHumana.Permisos
{
    public class VistaPermisos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermisoId { get; set; }
        public string EmpleadoId { get; set; }
        public string PermisoUsuario { get; set; }
        public int TipoLicenciaId { get; set; }
        public string PermisoObservacion { get; set; }
        public int PermisoEstado { get; set; }
        //public string PermisoEstadoNombre { get; set; }
        //public int PermisoAprobacion { get; set; }
        public DateTime PermisoFechaInicio { get; set; }
        public DateTime PermisoFechaFin { get; set; }
        public DateTime PermisoFechaRegistro { get; set; }
        public string TipoDescripcion { get; set; }
    }
}
