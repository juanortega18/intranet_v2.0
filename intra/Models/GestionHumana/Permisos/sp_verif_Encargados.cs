﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana.Permisos
{
    public class sp_verif_Encargados
    {
        public string Nombre { get; set; }
        public string Procedencia { get; set; }
    }
}
