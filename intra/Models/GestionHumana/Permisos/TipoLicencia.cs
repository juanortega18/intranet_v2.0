﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana.Permisos
{

    [Table("TipoLicencia")]
    public class TipoLicencia
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TipoLicenciaId { get; set; }
        public string TipoDescripcion { get; set; }
        public DateTime TipoFechaCreacion { get; set; }
        public string TipoUsuario { get; set; }
    }
}
