﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana.Permisos
{
    [Table("ActividadEncargado")]
    public class ActividadEncargado
    {
        [Key]
        public int ActividadEncargadoID { get; set; }

        [StringLength(100)]
        public string ActividadEncargadoDescripcion { get; set; }
    }
}
