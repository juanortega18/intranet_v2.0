﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana.Permisos
{
    public class InfoEmpleado
    {
        [Key]
        public string Nombre { get; set; }
        public string Departamento { get; set; }
        public string Cargo { get; set; }
        public int PermisoId { get; set; }
        public int PermisoUsuario { get; set; }
        public string EmpleadoId { get; set; }

    }
}
