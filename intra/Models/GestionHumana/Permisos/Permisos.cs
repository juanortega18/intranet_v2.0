﻿namespace intra.Models.GestionHumana.Permisos
{
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


    public class Permisos
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermisoId { get; set; }
        public string EmpleadoId { get; set; }
        public string PermisoUsuario { get; set; }
        public int TipoLicenciaId { get; set; }
        public string PermisoObservacion { get; set; }
        [ForeignKey("PermisosEstados")]
        public int PermisoEstado { get; set; }
        //public int PermisoAprobacion { get; set; }
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime? PermisoFechaInicio { get; set; }

        //[DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime? PermisoFechaFin { get; set; }

        //[DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime? PermisoFechaRegistro { get; set; }

        //[DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime? PermisoFechaAprobado { get; set; }

        public int EncargadoId { get; set; }

        public string UsuarioEncargado { get; set; }

        [NotMapped]
        public string EmpleadoNombre { get; set; }

        public virtual PermisosEstados PermisosEstados { get; set; }

        public virtual TipoLicencia TipoLicencia { get; set; }
    }
  
}
