﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana.Permisos
{
    [Table("EncargadoStatus")]
    public class EncargadoStatus
    {
        [Key]
        public int EncargadoStatusId { get; set; }

        [StringLength(30)]
        public string EncargadoStatusDescripcion { get; set; }
    }
}
