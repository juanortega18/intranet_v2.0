﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana
{
    [Table("EmpleadosContratados")]
    public partial class EmpleadosContratados
    {
        [Key]
        //[Required]
        public int id_persona { get; set; }
        //[Key]
        //[Required(ErrorMessage = "El Codigo de empleado es requerido")]
        public string Empleadoid { get; set; }
        [Required(ErrorMessage = "El Nombre es requerido")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "La Cédula es requerida")]
        public string Cedula { get; set; }
        public string Cargoid { get; set; }
        public string Cargo { get; set; }
        public string Telefono1 { get; set; }
        public DateTime? Fechaingreso { get; set; }
        public string DependenciaId { get; set; }
        public string Dependencia { get; set; }
        public string departamentoid { get; set; }
        public string Departamento { get; set; }
        public Decimal Salario { get; set; }
        public int Estatus { get; set; }
        public DateTime? Cumpleano { get; set; }
        public string Sexo { get; set; }
        public int? EstadoCivil { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
        public string TelefonoResidencial { get; set; }
    }
}
