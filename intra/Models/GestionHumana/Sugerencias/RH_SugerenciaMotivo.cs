﻿using System;
using System.ComponentModel.DataAnnotations;

namespace intra.Models.GestionHumana.Sugerencias
{
    public class RH_SugerenciaMotivo
    {
        [Key]
        public int MotivoID { get; set; }
        public string Descripcion { get; set; }
    }
}