﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana.Sugerencias
{
    public class RH_SugerenciaSupervisor_log
    {
        [Key]
        public int SugerenciaSupervisor_logID { get; set; }
        public int SugerenciaSupervisorID { get; set; }
        public int EmpleadoID { get; set; }
        public DateTime FechaCreacion { get; set; }
        [NotMapped]
        public VISTA_EMPLEADOS VISTA_EMPLEADO { get; set; }
        //RH_SugerenciaSupervisor_log
    }
}