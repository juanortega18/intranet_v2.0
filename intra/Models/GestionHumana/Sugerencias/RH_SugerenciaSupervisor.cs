﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana.Sugerencias
{
    public class RH_SugerenciaSupervisor
    {
        [Key]
        public int SugerenciaSupervisorID { get; set; }
        public int EmpleadoID { get; set; }
        public bool Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        //RH_SugerenciaSupervisor_log
    }
}