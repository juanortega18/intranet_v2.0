﻿using intra.Models.GestionHumana.Sugerencias;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models
{
    public class RH_SugerenciaEvaluacion_log
    {
        [Key]
        public int SugerenciaEvaluacionID { get; set; }
        public int SugerenciaID { get; set; }
        public int SugerenciaEstadoID { get; set; }
        public int EvaluadoPor { get; set; }
        public DateTime FechaCreacion { get; set; }
        public virtual RH_Sugerencia Sugerencia { get; set; }
        public virtual RH_SugerenciaEstado SugerenciaEstado { get; set; }
    }
}