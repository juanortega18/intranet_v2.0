﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.GestionHumana.Sugerencias
{
    public class RH_Sugerencia
    {
        [Key]
        public int SugerenciaID { get; set; }
        public int MotivoID { get; set; }
        [ForeignKey("Dependencia")]
        public int RegionID { get; set; }
        public int SugerenciaEstadoID { get; set; }
        public bool Estado { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public virtual RH_SugerenciaMotivo SugerenciaMotivo { get; set; }
        public virtual RH_SugerenciaEstado SugerenciaEstado { get; set; }
        public virtual Sol_DependenciasRegionales Dependencia { get; set; }
        public virtual List<RH_SugerenciaLike> RH_SugerenciaLikes { get; set; }
    }
}