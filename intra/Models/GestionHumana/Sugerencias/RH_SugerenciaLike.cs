﻿using System;
using System.ComponentModel.DataAnnotations;

namespace intra.Models.GestionHumana.Sugerencias
{
    public class RH_SugerenciaLike
    {
        [Key]
        public int SugerenciaLikeID { get; set; }
        public int SugerenciaID { get; set; }
        public int EmpleadoID { get; set; }
        public bool EstadoID { get; set; }
        public DateTime FechaLike { get; set; }
        public DateTime FechaModificacion { get; set; }
        public virtual RH_Sugerencia RH_Sugerencia { get; set; }
    }
}