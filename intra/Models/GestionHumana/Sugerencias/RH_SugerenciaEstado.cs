﻿using System;
using System.ComponentModel.DataAnnotations;

namespace intra.Models.GestionHumana.Sugerencias
{
    public class RH_SugerenciaEstado
    {
        [Key]
        public int SugerenciaEstadoID { get; set; }
        public string Descripcion { get; set; }
    }
}