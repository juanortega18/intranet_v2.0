﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.GestionHumana
{
    [Table("DocIdVisual")]
    public class DocIdVisual
    {
        [Key]
        public int DocIdVisualId { get; set; }

        [StringLength(100)]
        public string DocIdVisualNombre { get; set; }

        [StringLength(10)]
        public string DocIdVisualExtension { get; set; }
        public DateTime? DocIdVisualFhCreacion { get; set; }
        public DateTime? DocIdVisualFhModificacion { get; set; }
        public int? DocIdVisualUsuarioCreador { get; set; }

        [StringLength(30)]
        public string DocIdVisualUsuarioCreadorNombre { get; set; }
        public int? DocIdVisualUsuarioModificador { get; set; }

        [StringLength(30)]
        public string DocIdVisualUsuarioModificadorNombre { get; set; }
        public byte[] DocIdVisualArchivo { get; set; }

        [NotMapped]
        public HttpPostedFileBase ArchivoSubido { get; set; }
    }
}