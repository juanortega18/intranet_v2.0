﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace intra.Models.ViewModel
{
   public class EmpleadoExtensionesVM
    {
        public int FlotaId { get; set; }
        public string empleadoId { get; set; }
        public string nombre { get; set; }
        public string dependencia { get; set; }
        public string dependenciaid { get; set; }
        public string departamento { get; set; }
        public string departamentoid { get; set; }
        public string extensiones { get; set; }
        public string cargo { get; set; }
    }
}
