﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.BienesIncautados
{
    public class HistorialCustodioVM
    {
        public INV_CUSTODIO Custodio { get; set; }
        public Logs Log { get; set; }
    }
}