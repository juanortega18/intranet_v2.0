﻿using intra.Models.TallerPgr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Models.ViewModel
{
    public class MarcaTipoViewModel
    {
        public MarcaEquipo MarcaEquipo { get; set; }
        public List<TipoEquipo> ListaTipoEquipo { get; set; }
        private List<int> _tiposeleccionados { get; set; }
        public List<int> TipoSelecionados {
            get {
                if (_tiposeleccionados == null) {
                    _tiposeleccionados = MarcaEquipo.TipoEquipo.Select(l => l.TipoEquipoId).ToList();
                }
                return _tiposeleccionados;
            }

            set { _tiposeleccionados = value; } }
    }
}