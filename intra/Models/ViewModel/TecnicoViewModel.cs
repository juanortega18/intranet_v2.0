﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Models.ViewModel
{
    public class TecnicoViewModel
    {
        public int TecnicoId { get; set; }
        public string NombreCompleto { get; set; }
    }

    public class SolicitanteViewModel
    {
        public int SolicitanteId { get; set; }
        public string NombreCompleto { get; set; }
    }
}