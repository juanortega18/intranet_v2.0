﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace intra.Models.ViewModel
{
  public  class AreasProduccionVM
    {
        public int AreaId { get; set; }
        public string AreasDescripcion { get; set; }
    }
}
