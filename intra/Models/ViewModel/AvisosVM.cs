﻿using System;

namespace intra.Models.ViewModel
{
    internal class AvisosVM
    {
        public int AvisosId { get; set; }
        public string AvisosTitulo { get; set; }
        public string AvisosImagen { get; set; }
        public DateTime AvisosFechaCreacion { get; set; }
        public int AvisosVistas { get; set; }
        public string AvisosCuerpo { get; set; }
        public string AvisosIcono { get; internal set; }
    }
}