﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace intra.Models.ViewModel
{
    public partial class  Citas
    {
        [Key]
        public int citaId { get; set; }
        public string cita { get; set; }
        public string autor { get; set; }
    }
}