﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Models.ViewModel
{
    public class TipoSolicitudVM
    {
        public Sol_Actividades Actividad { get; set; }
        public List<TecnicoDTO> Tecnicos {get; set;}
    }
}