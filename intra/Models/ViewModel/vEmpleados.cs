﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.ViewModel
{
   public class vEmpleados
    {
        [Key]
        public string  CedulaAD { get; set; }
        public string DepartamentoAD { get; set; }
        public string UsuarioAD { get; set; }
        public string NombreAD { get; set; }
        public string   CedulaVE { get; set; }
        public string  NombreVE { get; set; }
        public string DepartamentoVE { get; set; }
        public string CodigoVE { get; set; }
        public string DependenciaVE { get; set; }
        public string CargoVE { get; set; }

    }
}
