﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using intra.Models.GestionHumana;

namespace intra.Models.ViewModel
{
   public class vServiciosIntranet
    {
        public string Solicitante { get; set; }
        public int SolicitanteId { get; set; }
        public int? DependenciaId { get; set; }
        public string Dependencia { get; set; }
        public string Responsable { get; set; }
        public int ResponsableId { get; set; }
        public int? SolicitudId { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime FechaSolicitud { get; set; }
        public string Estatus { get; set; }
        public int EstadoId { get; set; }
        public string DescripcionSolicitud { get; set; }
        public string TipoSolicitud { get; set; }
        public string SubActividad { get; set; }
        public int TipoSolicitudId { get; set; }
        public int CodigoSubactividad { get; set; }
        public int DepartamentoId { get; set; }
        public string Departamento { get; set; }
        public string Solucion { get; set; }
        public List<EmpleadoViaticos> EmpleadoViaticos { get; set; }

        public virtual VISTA_EMPLEADOS ViewEmpleados { get; set; }
       // public virtual Sol_DependenciasRegionales Sol_DependenciasRegionales { get; set; }
    }
}
