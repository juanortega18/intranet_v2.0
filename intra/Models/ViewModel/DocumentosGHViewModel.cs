﻿using intra.Models.GestionHumana;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Models.ViewModel
{
    public class DocumentosGHViewModel
    {
        public DocumentosGHViewModel() {
            CarpetasDocumentosGHs = new List<CarpetasDocumentosGH>();
            DocumentoGestionHumana = new List<GestionHumana.DocumentoGestionHumana>();
        }
        public CarpetasDocumentosGH CarpetasDocumentosGH { get; set; }
        public List<CarpetasDocumentosGH> CarpetasDocumentosGHs { get; set; }
        public List<DocumentoGestionHumana> DocumentoGestionHumana { get; set; }
    }
}