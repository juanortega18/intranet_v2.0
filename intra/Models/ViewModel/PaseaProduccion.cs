﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using intra.Models.PaseProduccion;

namespace intra.Models.ViewModel
{
    public class PaseaProduccion
    {
        [Key]
        public string Asunto { get; set; }
        public string Solicitante { get; set; }
        public string Proyecto { get; set; }
        public DateTime FechaAprobacion { get; set; }
        public string Prioridad { get; set; }
        public string Categoria { get; set; }
        public string Razon { get; set; }
        public string EstadoAprobacion { get; set; }
        public int Aprobaciones { get; set; }
        public DateTime FechaSolicitud { get; set; }
        public string EncargadoDTI { get; set; }
        public string EncargadoProyecto { get; set; }
        public string EncargadoDesarrollo { get; set; }
        public string LiderEquipo { get; set; }
        public string usuarioAprobador { get; set; }
        public string FechaAprobacionDTI { get; set; }
        public string FechaAprobacionDesarrollo { get; set; }
        public string FechaAprobacionProyecto { get; set; }
        public string FechaAprobacionLider { get; set; }
        public string FechaAprobacionSolicitante { get; set; }
        public string  AreasImplicadas { get; set; }
        public string Riesgo { get; set; }
        public string Impacto { get; set; }
        public string FaseProyecto { get; set; }
        public int SolicitudId { get; set; }
        public string EncargadoOperaciones { get; set; }
        public string FechaEncargadoOperaciones { get; set; }
        public string EncargadoSeguridadDTI { get; set; }
        public string FechaEncargadoSeguridadDTI { get; set; }
        public int SubActividadId { get; set; }
        public List<AprobacionesActividad> AprobacionesActividad  { get; set; }
        public List<FirmaPaseProduccion> Firmas { get; set; }
        public List<CategoriasAprobacionesProduccion> categoria { get; set; }
        public List<AreasAprobacionesProduccion> areasImplicadas { get; set; }

        //public string Firma2 { get; set; }
        //public string Firma3 { get; set; }
        //public string Firma4 { get; set; }

    }
}
