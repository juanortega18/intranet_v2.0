﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace intra.Models.ViewModel
{
  public class vmEmpleadosActualizados
    {
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public string Cedula { get; set; }
        public string Dependencia { get; set; }
        public string Departamento { get; set; }
        public string Cargo { get; set; }
        public string DependenciaId { get; set; }
        public string  DepartamentoId { get; set; }


        public string Celular { get; set; }
        public string Telefono { get; set; }
        public string FechaIngreso { get; set; }
        public string FechaNacimiento { get; set; }
        public string Sexo { get; set; }
        public string Direccion { get; set; }
        public string Sector { get; set; }
        public string EstadoCivil { get; set; }
        public string TipoSangre { get; set; }
        public string Email { get; set; }
        public string Profesion { get; set; }
        public string Ars { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string AFP { get; set; }
        public string Twitter { get; set; }
        public string Observaciones { get; set; }
        public string NivelAcademico { get; set; }
        public string Provincia { get; set; }
        public string Municipio { get; set; }
       // public string Ciudad { get; set; }
        public string Localidad { get; set; }
        public string CantidadDependientes { get; set; }

    }
}
