﻿
namespace intra.Models.GestionHumana.Sugerencias
{
    public class RH_SugerenciaReporteVM
    {
        public string DEPENDENCIAS { get; set; }
        public int SUGERENCIAS { get; set; }
        public int QUEJAS { get; set; }
        public int MEJORAS { get; set; }
        public int TOTAL_EN_DEPENDENCIAS { get; set; }
    }
}