﻿using System;

namespace intra.Models.ViewModel
{
    public class FiltroVM
    {
        public FiltroVM() {
            Clases = new int[] { 0 };
        }

        public int Casos { get; set; }
        public int[] Clases { get; set; }
        public Int16 Tipos { get; set; }
        public int Colores { get; set; }
        public int ProvInv { get; set; }
        public int ProvInc { get; set; }
        public int Custodios { get; set; }
        public int Estatus { get; set; }
        public int FechaTipo { get; set; }
        public string INCA_FHINICIAL { get; set; }
        public string INCA_FHFINAL { get; set; }
        public DateTime INCA_DT_FHINICIAL { get; set; }
        public DateTime INCA_DT_FHFINAL { get; set; }
    }
}
