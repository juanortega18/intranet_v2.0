﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace intra.Models.ViewModel
{
   public class CategoriaProduccioVM
    {
        public int CategoriaId { get; set; }
        public string CategoriaDescripcion { get; set; }
    }
}
