﻿using intra.Models.GestionHumana.NuestroEquipo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Models.ViewModel
{
    public class NuestroEquipoVM
    {
        public RH_NuestroEquipo NuestroEquipo { get; set; }
        public List<RH_NuestroEquipoDepartamento> NuestroEquipoDepartamentos { get; set; }
    }
}