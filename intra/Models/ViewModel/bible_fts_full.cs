﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace intra.Models.ViewModel
{
    public partial class bible_fts_full
    {
        [Key]
        public int docid { get; set; }

        public int c0bookid { get; set; }

        public string c0book { get; set; }

        public string c1chapter { get; set; }

        public string c2verse { get; set; }

        public string c3content { get; set; }
    }
}