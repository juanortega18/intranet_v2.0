﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace intra.Models.ViewModel
{
   public class EmpleadosVM
    {
        [Key]
        public string EmpleadoId { get; set; }
        public string Nombre { get; set; }

    }
}
