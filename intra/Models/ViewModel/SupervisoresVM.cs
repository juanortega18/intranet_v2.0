﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace intra.Models.ViewModel
{
    public class SupervisoresVM
    {
        [Key]
        public int ActividadId { get; set; }
        public int SupervisorId { get; set; }
        public string Usuario { get; set; }

    }
}
