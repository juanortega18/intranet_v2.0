﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Models
{
    public class Persona
    {
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Nombres { get; set; }
        public string Numero { get; set; }
        public string Respuesta { get; set; }
    }
}