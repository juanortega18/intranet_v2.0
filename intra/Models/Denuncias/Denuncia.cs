namespace intra.Models.Denuncias
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Denuncia
    {
        public int DenunciaId { get; set; }

        public int? DenuncianteId { get; set; }

        public int? DenunciadoId { get; set; }

        public string DenunciaDescripcion { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public int? TipoDenunciaId { get; set; }

        public virtual Denunciado Denunciado { get; set; }

        public virtual Denunciante Denunciante { get; set; }

        public virtual TiposDenuncia TiposDenuncia { get; set; }
    }
}
