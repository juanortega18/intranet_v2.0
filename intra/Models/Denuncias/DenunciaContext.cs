namespace intra.Models.Denuncias
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DenunciaContext : DbContext
    {
        public DenunciaContext()
            : base("name=DenunciaContext")
        {
        }

        public virtual DbSet<Denunciado> Denunciados { get; set; }
        public virtual DbSet<Denunciante> Denunciantes { get; set; }
        public virtual DbSet<Denuncia> Denuncias { get; set; }
        public virtual DbSet<TiposDenuncia> TiposDenuncias { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Denunciado>()
                .Property(e => e.NumeroDocumento)
                .IsUnicode(false);

            modelBuilder.Entity<Denunciado>()
                .Property(e => e.DenunciadoNombres)
                .IsUnicode(false);

            modelBuilder.Entity<Denunciado>()
                .Property(e => e.DenunciadoApellido1)
                .IsUnicode(false);

            modelBuilder.Entity<Denunciado>()
                .Property(e => e.DenunciadoApellido2)
                .IsUnicode(false);

            modelBuilder.Entity<Denunciado>()
                .Property(e => e.DenunciadoAlias)
                .IsUnicode(false);

            modelBuilder.Entity<Denunciado>()
                .Property(e => e.DenunciadoCalle)
                .IsUnicode(false);

            modelBuilder.Entity<Denunciante>()
                .Property(e => e.NumeroDocumento)
                .IsUnicode(false);

            modelBuilder.Entity<Denunciante>()
                .Property(e => e.DenuncianteNombres)
                .IsUnicode(false);

            modelBuilder.Entity<Denunciante>()
                .Property(e => e.DenuncianteApellido1)
                .IsUnicode(false);

            modelBuilder.Entity<Denunciante>()
                .Property(e => e.DenuncianteApellido2)
                .IsUnicode(false);

            modelBuilder.Entity<Denunciante>()
                .Property(e => e.DenuncianteCorreo)
                .IsUnicode(false);

            modelBuilder.Entity<Denunciante>()
                .Property(e => e.DenuncianteCalle)
                .IsUnicode(false);

            modelBuilder.Entity<Denuncia>()
                .Property(e => e.DenunciaDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<TiposDenuncia>()
                .Property(e => e.TipoDenunciaNombre)
                .IsUnicode(false);

            modelBuilder.Entity<TiposDenuncia>()
                .Property(e => e.UsuarioEncargado)
                .IsUnicode(false);
        }
    }
}
