namespace intra.Models.Denuncias
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Denunciante
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Denunciante()
        {
            Denuncias = new HashSet<Denuncia>();
        }

        public int DenuncianteId { get; set; }

        public int TipoIdentificacionId { get; set; }

        [Required]
        [StringLength(15)]
        public string NumeroDocumento { get; set; }
        [Required]
        [StringLength(60)]
        public string DenuncianteNombres { get; set; }

        [Required]
        [StringLength(30)]
        public string DenuncianteApellido1 { get; set; }

        [StringLength(30)]
        public string DenuncianteApellido2 { get; set; }

        [StringLength(255)]
        public string DenuncianteCorreo { get; set; }

        [StringLength(55)]
        public string DenuncianteCalle { get; set; }

        public int? SectorId { get; set; }

        public int? MunicipioId { get; set; }

        public int? CiudadId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Denuncia> Denuncias { get; set; }
    }
}
