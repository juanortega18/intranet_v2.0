namespace intra.Models.Denuncias
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Denunciados")]
    public partial class Denunciado
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Denunciado()
        {
            Denuncias = new HashSet<Denuncia>();
        }

        public int DenunciadoId { get; set; }

        public int? TipoIdentificacionId { get; set; }

        [StringLength(15)]
        public string NumeroDocumento { get; set; }

        [StringLength(60)]
        public string DenunciadoNombres { get; set; }

        [StringLength(30)]
        public string DenunciadoApellido1 { get; set; }

        [StringLength(30)]
        public string DenunciadoApellido2 { get; set; }

        [StringLength(60)]
        public string DenunciadoAlias { get; set; }

        [StringLength(55)]
        public string DenunciadoCalle { get; set; }

        public int? SectorId { get; set; }

        public int? MunicipioId { get; set; }

        public int? CiudadId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Denuncia> Denuncias { get; set; }
    }
}
