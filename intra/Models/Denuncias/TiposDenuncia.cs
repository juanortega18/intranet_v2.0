namespace intra.Models.Denuncias
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TiposDenuncia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TiposDenuncia()
        {
            Denuncias = new HashSet<Denuncia>();
        }

        [Key]
        public int TipoDenunciaId { get; set; }

        [Required]
        [StringLength(70)]
        public string TipoDenunciaNombre { get; set; }

        [StringLength(255)]
        public string UsuarioEncargado { get; set; }

        public bool? TipoDenunciaEstatus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Denuncia> Denuncias { get; set; }
    }
}
