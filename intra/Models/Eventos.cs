﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;

namespace intra.Models
{
    [Table("Eventos")]
    public class Eventos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventoId { get; set; }
        [Display(Name = "Tipo de Evento")]
        public int TipoEventoId { get; set; }
        public int SubActividadId { get; set; }
        [Display(Name = "Tipo de Cobertura")]
        public int TipoCoberturaId { get; set; }
        [Display(Name = "Fecha de inicio")]
        [Required(ErrorMessage = "Debe completar la {0} ")]
        public DateTime EventoFhInicio { get; set; }
        [Display(Name = "Fecha final")]
        [Required(ErrorMessage = "Debe completar este campo")]
        public DateTime EventoFhFinal { get; set; }
        [Display(Name = "Hora de inicio")]
        [Required(ErrorMessage = "Debe completar este campo")]
        //[DataType(DataType.Time)]
        public string EventoHoraInicio { get; set; }
        [Display(Name = "Hora final")]
        [Required(ErrorMessage = "Debe completar este campo")]
        //[DataType(DataType.Time)]
        public string EventoHoraFinal { get; set; }
        [Display(Name = "Nombre de la Actividad")]
        [Required(ErrorMessage = "Debe completar este campo")]
        public string EventoNombreActividad { get; set; }
        [Display(Name = "Responsable")]
        [Required(ErrorMessage = "Debe completar este campo")]
        public string EventoResponsable { get; set; }
        [Display(Name = "Teléfono/Ip")]
        [Required(ErrorMessage = "Debe completar este campo")]
        public string EventoTelefonoIP { get; set; }
        [Display(Name = "Correo")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Debe de ingresar su correo")]
        public string EventoEmail { get; set; }
        [Display(Name = "Detalles/Observaciones")]
        [DataType(DataType.MultilineText)]
        public string EventoDetalles { get; set; }
        [Display(Name = "Cantidad de Personas")]
        [Required(ErrorMessage = "Debe completar este campo")]
        public string EventoCantidadPersonas { get; set; }
        public int SolicitudId { get; set; }
        [NotMapped]
        public List<TipoCobertura> tipoCoberturas { get; set; }
        public IEnumerable<SelectListItem> items { get; set; }

        public virtual TipoEventos TipoEventos { get; set; }
        public virtual ICollection<EventoTipoCobertura> EventoTipoCobertura { get; set; }
    }
    [Table("TipoEventos")]
    public class TipoEventos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int TipoEventoId { get; set; }
        public int SubActividadId { get; set; }
        public string TipoEventoDescripcion { get; set; }
    }
    [Table("TipoCobertura")]
    public class TipoCobertura
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TipoCoberturaId { get; set; }
        public int SubActividadId { get; set; }
        public string TipoCoberturaDescripcion { get; set; }

    }
    //Tabla intermedia///
    [Table("EventoTipoCobertura")]
    public class EventoTipoCobertura
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventTipCobId { get; set; }
        public int TipoCoberturaId { get; set; }
        public int EventoId { get; set; }
        // relaciones

        public virtual TipoCobertura TipoCobertura { get; set; }
        public virtual Eventos Eventos { get; set; }

    }


}