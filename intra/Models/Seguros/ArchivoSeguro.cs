﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.Seguros
{
    [Table("ArchivoSeguro")]
    public class ArchivoSeguro
    {
        public int ArchivoSeguroId { get; set; }

        public int SeguroId { get; set; }

        [StringLength(100)]
        public string ArchivoSeguroNombre { get; set; }

        [StringLength(10)]
        public string ArchivoSeguroExtension { get; set; }

        [StringLength(100)]
        public string ArchivoSeguroRuta { get; set; }

        public bool ArchivoSeguroEstado { get; set; }

        public virtual Seguro Seguro { get; set; }
    }
}