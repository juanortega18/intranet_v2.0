﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.Seguros
{
    [Table("Seguro")]
    public class Seguro
    {
        public int SeguroId { get; set; }

        [StringLength(100)]
        public string SeguroNombre { get; set; }

        [StringLength(100)]
        public string SeguroLink { get; set; }
        public bool SeguroEstado { get; set; }

        public virtual ICollection<ArchivoSeguro> ArchivosSeguro { get; set; }
    }
}