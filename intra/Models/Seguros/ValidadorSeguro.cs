﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Models.Seguros
{
    /// <summary>
    /// Esta clase solamente se utiliza en el módulo de seguros del Intranet.
    /// </summary>
    /// <remarks>
    /// Los métodos utilizados en esta clase retornan un array de string con los errores que se puedan dar al llenar los campos
    /// de un seguro.
    /// </remarks>
    public class ValidadorSeguro
    {
        /// <summary>
        /// Este struct define los diferentes errores que se puedan dar al llenar los campos.
        /// </summary>
        private struct MensajesErrores
        {
            public string NombreVacio { get; set; }
            public string LinkVacio { get; set; }
            public string NombreExiste { get; set; }
            public string LinkExiste { get; set; }
        }

        private MensajesErrores _mensajesErrores;
        private dbIntranet Database = new dbIntranet();

        public ValidadorSeguro()
        {
            _mensajesErrores.NombreVacio = "Introduzca un nombre";
            _mensajesErrores.LinkVacio = "Introduzca un link";
            _mensajesErrores.NombreExiste = "El nombre ya existe";
            _mensajesErrores.LinkExiste = "El link ya existe";
        }

        /// <summary>
        /// Recibe los campos a validar de un seguro y retorna una lista de errores.
        /// </summary>
        /// <param name="seguroNombre">Cadena.</param>
        /// <param name="seguroLink">Cadena.</param>
        /// <param name="validarExistencia">Booleano. Validar si existe un seguro.</param>
        /// <returns>Lista de errores.</returns>
        public List<string> ValidarCamposSeguro(string seguroNombre, string seguroLink, bool validarExistencia)
        {
            seguroNombre = seguroNombre.Trim().ToUpper();
            seguroLink = seguroLink.Trim().ToLower();

            List<string> errores = new List<string>();

            if (string.IsNullOrEmpty(seguroNombre))
            {
                errores.Add(_mensajesErrores.NombreVacio);
            }

            if (string.IsNullOrEmpty(seguroLink))
            {
                errores.Add(_mensajesErrores.LinkVacio);
            }

            if (validarExistencia)
            {
                bool existeNombre = Database.Seguros.Any(s => s.SeguroNombre == seguroNombre);
                bool existeLink = Database.Seguros.Any(s => s.SeguroLink == seguroLink);

                if (existeNombre)
                {
                    errores.Add(_mensajesErrores.NombreExiste);
                }

                if (existeLink)
                {
                    errores.Add(_mensajesErrores.LinkExiste);
                }
            }

            return errores;
        }
    }
}