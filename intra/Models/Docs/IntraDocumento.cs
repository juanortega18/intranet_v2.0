namespace intra.Models.Docs
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web;
    using System.Data.Entity.Spatial;

    [Table("IntraDocumentos")]
    public class IntraDocumentos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IntDocumentoId { get; set; }

        [Required]
        [StringLength(200)]
        public string IntDocumentoNombre { get; set; }

        public DateTime IntDocumentoFecha { get; set; }

        public int IntCarpetaId { get; set; }

        [Required]
        public byte[] IntDocumentoArchivo { get; set; }

        [Required]
        [StringLength(200)]
        public string IntDocumentoTipo { get; set; }

        public bool IntDocumentoEstatus { get; set; }

        public DateTime? IntDocumentoFechaCreacion { get; set; }

        [NotMapped]
        public string String64 { get; set; }

        [NotMapped]
        public List<HttpPostedFileBase> ArchivoVista { get; set; }
    }
}
