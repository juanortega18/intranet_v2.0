namespace intra.Models.Docs
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public  class IntraTipo_Archivo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IntTipoId { get; set; }

        [Required]
        [StringLength(50)]
        public string IntDescripcion { get; set; }

        public bool IntEstado { get; set; }

        public DateTime IntFhCreacion { get; set; }
    }
}
