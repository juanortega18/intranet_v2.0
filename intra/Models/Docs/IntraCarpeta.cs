namespace intra.Models.Docs
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public  class IntraCarpeta
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IntCarpetaId { get; set; }

        public int? INtCarpetaRaizId { get; set; }

        public int DeptoId { get; set; }

        [Required]
        public string IntCarpetaNombre { get; set; }
    }
}
