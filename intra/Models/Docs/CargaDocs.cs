namespace intra.Models.Docs
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CargaDocs : DbContext
    {
        public CargaDocs()
            : base("name=CargaDocs")
        {
        }

        public virtual DbSet<IntraCarpeta> IntraCarpeta { get; set; }
        public virtual DbSet<IntraDocumentos> IntraDocumentos { get; set; }
        public virtual DbSet<IntraTipo_Archivo> IntraTipo_Archivo { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IntraCarpeta>()
                .Property(e => e.IntCarpetaNombre)
                .IsUnicode(false);

            modelBuilder.Entity<IntraDocumentos>()
                .Property(e => e.IntDocumentoNombre)
                .IsUnicode(false);

            modelBuilder.Entity<IntraDocumentos>()
                .Property(e => e.IntDocumentoTipo)
                .IsUnicode(false);

            modelBuilder.Entity<IntraTipo_Archivo>()
                .Property(e => e.IntDescripcion)
                .IsUnicode(false);
        }
    }
}
