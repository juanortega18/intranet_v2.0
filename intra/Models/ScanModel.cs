﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Models
{
    public class ScanLinkedDocs
    {
        public int ScanRegisterId { get; set; }
        public string Razon { get; set; }
        public string FechaPublicacion { get; set; }
        public string NombreUsuario { get; set; }
        public string LugarProcedencia { get; set; }
        public string EscanEmisor { get; set; }

    }

    public class ScanViewModel
    {
        //public List<ScanViewDocs> viewDocs { get; set; }
        public ScanViewDocs viewDoc { get; set; }
        //public List<string> linkedUsers { get; set; }
        public List<ScanChatModel> scanChat { get; set; }
        public List<string[]> RutasArchivos { get; set; }
        public List<ScanEmpleados> LinkedUsers { get; set; }
    }

    public class ScanChatModel
    {
        public int IdUsuario { get; set; }
        public string Usuario { get; set; }
        public string Descripcion { get; set; }
        public string Archivo { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Avatar { get; set; }
    }

    public class ScanViewDocs
    {
        public int ScanRegisterId { get; set; }
        public string NombreDepartamento { get; set; }
        public int IdDepartamento { get; set; }
        public int IdUsuario { get; set; }
        public string Razon { get; set; }
        public string NombreUsuario { get; set; }
        public string Avatar { get; set; }
        public string FechaPublicacion { get; set; }
        public string Detalle { get; set; }
        public string OwnerAvatar { get; set; }
        public string LugarProcedencia { get; set; }
        public string EscanEmisor { get; set; }

    }

    public class ScanListaEmpleados
    {
        public string label { get; set; }
        public string value { get; set; }
        public string code { get; set; }
    }

    public class ScanEmpleados
    {
        public int IdEmpleado { get; set; }
        public string NombreEmpleado { get; set; }
    }
}