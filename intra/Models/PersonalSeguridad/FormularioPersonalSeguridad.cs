﻿namespace intra.Models.PersonalSeguridad
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using intra.Models.GestionHumana.Permisos;


    public partial class FormularioPersonalSeguridads

    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormId { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cedula { get; set; }
        public int RangoId { get; set; }
        public string EstadoAsignacion { get; set; }
        public int InstitucionID { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public System.Nullable<DateTime> FechaNacimiento { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string TipoSangre { get; set; }
        public string Direccion { get; set; }
        public string TelefonoEmergencia { get; set; }
        public string Designacion { get; set; }
        public int? DependenciaID { get; set; }
        public int? DeptoID { get; set; }
        public string ArmaFuego { get; set; }
        public string ArmaMarca { get; set; }
        public string ArmaModelo { get; set; }
        public string ArmaSerie { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public System.Nullable<DateTime> FechaIngreso { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public System.Nullable<DateTime> FechaSalida { get; set; }
        public int Estado { get; set; }
        public string ObservacionGeneral { get; set; }
        public string Imagen { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual Institucions Institucions { get; set; }
        public virtual Rangos Rangos { get; set; }
        //public virtual Nmdependencias_Permisos Nmdependencias_Permisos { get; set; }

        //public virtual Nmdeptos_Permisos Nmdeptos_Permisos { get; set; }

    }

  



}
