﻿namespace intra.Models.PersonalSeguridad
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public partial class RangoInstitucions
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RanID { get; set; }
        public int RangoId { get; set; }
        public int InstitucionID { get; set; }



    }

}
