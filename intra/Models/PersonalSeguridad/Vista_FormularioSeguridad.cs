﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.PersonalSeguridad
{
    public class Vista_FormularioSeguridad
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormId { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cedula { get; set; }
        public string RangoNombre { get; set; }
        public string EstadoAsignacion { get; set; }
        public string InstitucionNombre { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string TipoSangre { get; set; }
        public string Direccion { get; set; }
        public string TelefonoEmergencia { get; set; }
        public string Designacion { get; set; }
        public string ArmaFuego { get; set; }
        public string ArmaMarca { get; set; }
        public string ArmaModelo { get; set; }
        public string ArmaSerie { get; set; }
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaSalida { get; set; }
        public string Estado { get; set; }
        public string ObservacionGeneral { get; set; }
        public string Imagen { get; set; }

    }

}
