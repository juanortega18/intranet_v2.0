﻿namespace intra.Models.PersonalSeguridad
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public partial class Institucions
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Institucions()
        {
            FormularioPersonalSeguridads = new HashSet<FormularioPersonalSeguridads>();
            Rangos = new List<Rangos>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InstitucionID { get; set; }
        public string InstitucionNombre { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormularioPersonalSeguridads> FormularioPersonalSeguridads { get; set; }
        public virtual ICollection<Rangos> Rangos { get; set; }

    }

  
}
