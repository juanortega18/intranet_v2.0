﻿namespace intra.Models.PersonalSeguridad
{ 
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

        public partial class Rangos
        {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Rangos()
        {
            Institucions = new HashSet<Institucions>();
            FormularioPersonalSeguridads = new HashSet<FormularioPersonalSeguridads>();
        }
            [Key]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int RangoId { get; set; }
            public string RangoNombre { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormularioPersonalSeguridads> FormularioPersonalSeguridads { get; set; }
        public virtual ICollection<Institucions> Institucions { get; set; }

    }
    }

