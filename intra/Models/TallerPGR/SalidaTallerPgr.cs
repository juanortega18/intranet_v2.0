namespace intra.Models.TallerPgr
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SalidaTallerPgr")]
    public partial class SalidaTallerPgr
    {
        [Key]
        public int SalidaId { get; set; }

        [StringLength(50)]
        public string EmpleadoTallerId { get; set; }

        public int? EquipoId { get; set; }

        [StringLength(200)]
        public string SalidaTecnicoResponsable { get; set; }

        public string SalidaRazon { get; set; }

        public DateTime? SalidaFecha { get; set; }

        [StringLength(40)]
        public string SalidaAppUsuario { get; set; }

        [StringLength(150)]
        public string SalidaPersonaResponsable { get; set; }

        [StringLength(30)]
        public string SalidaPersonaResponsableDocumento { get; set; }

        [StringLength(35)]
        public string SalidaPersonaResponsableTelefono { get; set; }

        public int? LocalidadId { get; set; }

        public int? MunicipioId { get; set; }

        public int? ProvinciaId { get; set; }

        public int? RegionId { get; set; }

        public int? DepartamentoId { get; set; }

        [StringLength(30)]
        public string ActivoFijo { get; set; }

        public bool? EntradaEstatus { get; set; }
    }
}
