﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using intra.Models.GestionHumana;

namespace intra.Models.TallerPgr
{
    [Table("TecnicoTaller")]
    public class TecnicoTaller
    {
        [Key]
        public int TecnicoTallerId { get; set; }

        public string TecnicoCodigo { get; set; }

        public string TecnicoNombre { get; set; }

        public string TecnicoUsuario { get; set; }

        public bool Estado { get; set; }

        [NotMapped]
        public VISTA_EMPLEADOS Empleado { get; set; }
    }
}