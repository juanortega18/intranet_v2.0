namespace intra.Models.TallerPgr
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using intra.Models.GestionHumana;

    [Table("EntradaTallerPgr")]
    public partial class EntradaTallerPgr
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EntradaTallerPgr()
        {
            TallerPgr = new HashSet<TallerPgr>();
        }

        [Key]
        public int EntradaId { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Debe ingresar el codigo del empleado")]
        public string EmpleadoTallerId { get; set; }

        [Required(ErrorMessage ="Favor de Ingresar el Equipo")]
        public int? EquipoId { get; set; }

        [StringLength(200)]
        public string EntradaTecnicoResponsable { get; set; }

        [Required(ErrorMessage = "Debe ingresar la raz�n de la entrada")]
        public string EntradaRazon { get; set; }

        [DisplayFormat(DataFormatString = "dd/MM/yyyy")]
        public DateTime? EntradaFecha { get; set; }

        [StringLength(40)]
        public string EntradaAppUsuario { get; set; }

        [StringLength(150)]
        public string EntradaPersonaResponsable { get; set; }

        [StringLength(30)]
        public string EntradaPersonaResponsableDocumento { get; set; }

        [StringLength(35)]
        public string EntradaPersonaResponsableTelefono { get; set; }

        [StringLength(200)]
        public string Ubicacion { get; set; }

        public int? LocalizacionId { get; set; }

        public int? DepartamentoId { get; set; }

        [Range(1,int.MaxValue,ErrorMessage = "Debe ActivoFijo es demasiado largo"),Required(ErrorMessage ="Debe ingresar el activo fijo")]
        
        public int ActivoFijo { get; set; }

        [NotMapped]
        public string ActivoFijoView { get { return string.Format("{0:0000000000000000000000}", ActivoFijo); } }

        public bool? EntradaEstatus { get; set; }

        [NotMapped]
        public VISTA_EMPLEADOS Empleado { get; set; }

        [NotMapped]
        public TecnicoTaller Tecnico { get; set; }

        public virtual Departamento Departamento { get; set; }

        public virtual Equipo Equipo { get; set; }

        public virtual localizacion localizacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TallerPgr> TallerPgr { get; set; }
    }
}
