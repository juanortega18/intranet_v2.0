namespace intra.Models.TallerPgr
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public partial class TallerPgrDM : DbContext
    {
        public TallerPgrDM()
            : base("name=TallerPgrDM")
        {
        }

        public virtual DbSet<Departamento> Departamento { get; set; }
        public virtual DbSet<EntradaTallerPgr> EntradaTallerPgr { get; set; }
        public virtual DbSet<Equipo> Equipo { get; set; }
        public virtual DbSet<localizacion> localizacion { get; set; }
        public virtual DbSet<MarcaEquipo> MarcaEquipo { get; set; }
        public virtual DbSet<ModeloEquipo> ModeloEquipo { get; set; }
        public virtual DbSet<Municipio> Municipio { get; set; }
        public virtual DbSet<Provincia> Provincia { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<SalidaTallerPgr> SalidaTallerPgr { get; set; }
        public virtual DbSet<TallerPgr> TallerPgr { get; set; }
        public virtual DbSet<TipoEquipo> TipoEquipo { get; set; }
        public virtual DbSet<TecnicoTaller> TecnicoTaller { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<MarcaEquipo>()
                .HasMany<TipoEquipo>(t => t.TipoEquipo)
                .WithMany(m => m.MarcaEquipo)
                .Map(cs =>
                {
                    cs.MapLeftKey("MarcaEquipoId");
                    cs.MapRightKey("TipoEquipoId");
                    cs.ToTable("TipoEquipoMarcaEquipoes");
                });

            modelBuilder.Entity<Departamento>()
                .Property(e => e.DepartamentoDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<EntradaTallerPgr>()
                .Property(e => e.EmpleadoTallerId)
                .IsUnicode(false);

            modelBuilder.Entity<EntradaTallerPgr>()
                .Property(e => e.EntradaTecnicoResponsable)
                .IsUnicode(false);

            modelBuilder.Entity<EntradaTallerPgr>()
                .Property(e => e.EntradaRazon)
                .IsUnicode(false);

            modelBuilder.Entity<EntradaTallerPgr>()
                .Property(e => e.EntradaAppUsuario)
                .IsUnicode(false);

            modelBuilder.Entity<EntradaTallerPgr>()
                .Property(e => e.EntradaPersonaResponsable)
                .IsUnicode(false);

            modelBuilder.Entity<EntradaTallerPgr>()
                .Property(e => e.EntradaPersonaResponsableDocumento)
                .IsUnicode(false);

            modelBuilder.Entity<EntradaTallerPgr>()
                .Property(e => e.EntradaPersonaResponsableTelefono)
                .IsUnicode(false);

            modelBuilder.Entity<Equipo>()
                .Property(e => e.EquipoSerie)
                .IsUnicode(false);

            modelBuilder.Entity<localizacion>()
                .Property(e => e.LocalizacionDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<localizacion>()
                .Property(e => e.LocalizacionTelefono)
                .IsUnicode(false);

            modelBuilder.Entity<MarcaEquipo>()
                .Property(e => e.MarcaEquipoDescripcion)
                .IsUnicode(false);

            //modelBuilder.Entity<MarcaEquipo>()
            //    .HasMany(e => e.TipoMarcaEquipo)
            //    .WithRequired(e => e.MarcaEquipo)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<ModeloEquipo>()
                .Property(e => e.ModeloEquipoDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Municipio>()
                .Property(e => e.MunicipioDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Provincia>()
                .Property(e => e.ProvinciaDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Region>()
                .Property(e => e.RegionDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<SalidaTallerPgr>()
                .Property(e => e.EmpleadoTallerId)
                .IsUnicode(false);

            modelBuilder.Entity<SalidaTallerPgr>()
                .Property(e => e.SalidaTecnicoResponsable)
                .IsUnicode(false);

            modelBuilder.Entity<SalidaTallerPgr>()
                .Property(e => e.SalidaRazon)
                .IsUnicode(false);

            modelBuilder.Entity<SalidaTallerPgr>()
                .Property(e => e.SalidaAppUsuario)
                .IsUnicode(false);

            modelBuilder.Entity<SalidaTallerPgr>()
                .Property(e => e.SalidaPersonaResponsable)
                .IsUnicode(false);

            modelBuilder.Entity<SalidaTallerPgr>()
                .Property(e => e.SalidaPersonaResponsableDocumento)
                .IsUnicode(false);

            modelBuilder.Entity<SalidaTallerPgr>()
                .Property(e => e.SalidaPersonaResponsableTelefono)
                .IsUnicode(false);

            modelBuilder.Entity<SalidaTallerPgr>()
                .Property(e => e.ActivoFijo)
                .IsUnicode(false);

            modelBuilder.Entity<TallerPgr>()
                .Property(e => e.TallerPgrAvance)
                .IsUnicode(false);

            modelBuilder.Entity<TipoEquipo>()
                .Property(e => e.TipoEquipoDescripcion)
                .IsUnicode(false);
        }
    }
}
