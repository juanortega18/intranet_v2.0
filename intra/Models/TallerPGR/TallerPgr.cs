namespace intra.Models.TallerPgr
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TallerPgr")]
    public partial class TallerPgr
    {
        public int TallerPgrId { get; set; }

        public int? EntradaId { get; set; }

        [StringLength(300)]
        public string TallerPgrAvance { get; set; }

        public DateTime TallerPgrFecha { get; set; }

        public virtual EntradaTallerPgr EntradaTallerPgr { get; set; }
    }
}
