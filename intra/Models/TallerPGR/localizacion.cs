namespace intra.Models.TallerPgr
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("localizacion")]
    public partial class localizacion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public localizacion()
        {
            EntradaTallerPgr = new HashSet<EntradaTallerPgr>();
        }

        public int LocalizacionId { get; set; }

        public int? MunicipioId { get; set; }

        [StringLength(200)]
        public string LocalizacionDescripcion { get; set; }

        [StringLength(200)]
        public string LocalizacionTelefono { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EntradaTallerPgr> EntradaTallerPgr { get; set; }

        public virtual Municipio Municipio { get; set; }
    }
}
