namespace intra.Models.TallerPgr
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;

    [Table("TipoEquipo")]
    public partial class TipoEquipo : IValidatableObject
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoEquipo()
        {
            Equipo = new HashSet<Equipo>();
        }

        public int TipoEquipoId { get; set; }

        [StringLength(100),Required(ErrorMessage = "El nombre del tipo de equipo es obligatorio.")]
        public string TipoEquipoDescripcion { get; set; }

        public virtual ICollection<MarcaEquipo> MarcaEquipo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Equipo> Equipo { get; set; }

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validacion)
        {
            TallerPgrDM db = new TallerPgrDM();
            var validacionNombre = db.TipoEquipo.FirstOrDefault
            (x => x.TipoEquipoDescripcion == TipoEquipoDescripcion && x.TipoEquipoId != TipoEquipoId);
            if (validacionNombre != null)
            {
                ValidationResult errorMessage = new ValidationResult
                ("Esta tipo ya esta creada.", new[] { "TipoEquipoDescripcion" });
                yield return errorMessage;
            }
            else
            {
                yield return ValidationResult.Success;
            }
        }
    }
}
