namespace intra.Models.TallerPgr
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;

    [Table("MarcaEquipo")]
    public partial class MarcaEquipo
    {
        public MarcaEquipo()
        {
            Equipo = new HashSet<Equipo>();
            TipoEquipo = new HashSet<TipoEquipo>();
        }

        public int MarcaEquipoId { get; set; }

        [StringLength(100),Required(ErrorMessage = "El nombre de la marca es obligatorio.")]
        public string MarcaEquipoDescripcion { get; set; }

        public virtual ICollection<Equipo> Equipo { get; set; }

        public virtual ICollection<TipoEquipo> TipoEquipo { get; set; }
    }
}
