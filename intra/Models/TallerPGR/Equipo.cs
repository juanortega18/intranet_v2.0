namespace intra.Models.TallerPgr
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Equipo")]
    public partial class Equipo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Equipo()
        {
            EntradaTallerPgr = new HashSet<EntradaTallerPgr>();
        }
        
        public int EquipoId { get; set; }

        public int? TipoEquipoId { get; set; }

        public int? MarcaEquipoId { get; set; }

        public int? ModeloEquipoId { get; set; }

        [StringLength(30)]
        public string EquipoSerie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EntradaTallerPgr> EntradaTallerPgr { get; set; }

        public virtual MarcaEquipo MarcaEquipo { get; set; }

        public virtual ModeloEquipo ModeloEquipo { get; set; }

        public virtual TipoEquipo TipoEquipo { get; set; }
    }
}
