namespace intra.Models.TallerPgr
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Municipio")]
    public partial class Municipio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Municipio()
        {
            localizacion = new HashSet<localizacion>();
        }

        public int MunicipioId { get; set; }

        public int? ProvinciaId { get; set; }

        [StringLength(200)]
        public string MunicipioDescripcion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<localizacion> localizacion { get; set; }

        public virtual Provincia Provincia { get; set; }
    }
}
