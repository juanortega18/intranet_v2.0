namespace intra.Models.TallerPgr
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ModeloEquipo")]
    public partial class ModeloEquipo
    {
        public ModeloEquipo()
        {
            Equipo = new List<Equipo>();
        }

        public int ModeloEquipoId { get; set; }

        [StringLength(100)]
        [Required]
        public string ModeloEquipoDescripcion { get; set; }

        [Required]
        public int TipoEquipoId { get; set; }

        [Required]
        public int MarcaEquipoId { get; set; }

        public virtual TipoEquipo TipoEquipo { get; set; }

        public virtual MarcaEquipo MarcaEquipo { get; set; }

        public List<Equipo> Equipo { get; set; } 

        //public virtual VISTA_TIPOMARCAEQUIPO TipoMarcaEquipo { get; set; }
    }
}
