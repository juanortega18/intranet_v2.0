//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace intra.Models
{
    using System;
    
    public partial class Sp_Mostrar_Detalles_Solicitud_Reporte_Result
    {
        public int CodigoSolicitud { get; set; }
        public string EstadoSolicitud { get; set; }
        public string NombreSolicitante { get; set; }
        public string Gerencia { get; set; }
        public System.DateTime FechaSolicitado { get; set; }
        public string ResultadoPor { get; set; }
        public string Gerencia2 { get; set; }
        public Nullable<System.DateTime> FechaSolucion { get; set; }
        public string DescripcionSolicitud { get; set; }
        public string DescripcionSolucion { get; set; }
        public string TipoSolicitud { get; set; }
        public string DependenciaNombre { get; set; }
        public int DependenciaId { get; set; }
    }
}
