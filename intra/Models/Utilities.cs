﻿using intra.Models.GestionHumana;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using intra.Models.BienesIncautados;

namespace intra.Models
{
    public class Utilities : IDisposable
    {
        public static HashSet<INV_FOTOS> UploadPhoto(List<HttpPostedFileBase> photo_list, string bi_case_number)
        {
            string extension_file_name = string.Empty;
            string pic_name = string.Empty;
            string path = string.Empty;

            HashSet<INV_FOTOS> objetos_resultado = new HashSet<INV_FOTOS>();

            foreach (var item in photo_list)
            {
                if (item != null)
                {
                    pic_name = Path.GetFileName(item.FileName);
                    extension_file_name = Path.GetExtension(item.FileName);
                    path = Path.Combine(HttpRuntime.AppDomainAppPath + "\\Content\\images\\BI_Images\\", pic_name);
                    item.SaveAs(path);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        item.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();

                        objetos_resultado.Add(new INV_FOTOS
                        {
                            FOTO_ARCHIVO = array,
                            FOTO_FHCREACION = DateTime.Now,
                            FOTO_TIPO = extension_file_name,
                            FOTO_NOMBRE = pic_name,
                            INCA_NO = bi_case_number,
                            FOTO_PESO = (array.Length / 1024).ToString() + "KB",
                            FOTO_ESTATUS = true
                        });
                    }
                }
            }
            
            return objetos_resultado;
        }

        public static List<FotoGestionHumana> UploadPhotoGH(List<HttpPostedFileBase> photo_list)
        {
            string extension_file_name = string.Empty;
            string pic_name = string.Empty;
            string path = string.Empty;

            List<FotoGestionHumana> objetos_resultado = new List<FotoGestionHumana>();

            foreach (var item in photo_list)
            {
                if (item != null)
                {
                    pic_name = Path.GetFileName(item.FileName);
                    extension_file_name = Path.GetExtension(item.FileName);
                    path = Path.Combine(HttpRuntime.AppDomainAppPath + "\\Content\\images\\GH_IMAGES\\", pic_name);
                    item.SaveAs(path);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        item.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();

                        var img = System.Drawing.Image.FromStream(item.InputStream, true, true);

                        objetos_resultado.Add(new FotoGestionHumana
                        {
                            FotoGHArchivo = array,
                            FotoGHFhSubida = DateTime.Now,
                            FotoGHExtension = extension_file_name,
                            FotoGHNombre = pic_name,
                            FotoGHAlto = img.Height,
                            FotoGHAncho = img.Width,
                            FotoEstado = true
                        });
                    }
                }
            }
            return objetos_resultado;
        }

        #region Commented Code
        //public static HashSet<Documentos> Upload(List<HttpPostedFileBase> photo_list)
        //{
        //    string extension_file_name = string.Empty;
        //    string pic_name = string.Empty;
        //    string path = string.Empty;

        //    HashSet<Documentos> objetos_resultado = new HashSet<Documentos>();

        //    foreach (var item in photo_list)
        //    {
        //        if (item != null)
        //        {
        //            pic_name = Path.GetFileName(item.FileName);
        //            extension_file_name = Path.GetExtension(item.FileName);
        //            path = Path.Combine(HttpRuntime.AppDomainAppPath + "\\Content\\images\\BI_Images\\", pic_name);
        //            item.SaveAs(path);

        //            using (MemoryStream ms = new MemoryStream())
        //            {
        //                item.InputStream.CopyTo(ms);
        //                byte[] array = ms.GetBuffer();

        //                objetos_resultado.Add(new Documentos
        //                {
        //                    DocArchivo = array,
        //                    DocTipo = extension_file_name,
        //                    DocTitulo = pic_name
        //                });
        //            }
        //        }
        //    }

        //    return objetos_resultado;
        //}
        #endregion

        public static string UrlWebService(string cedula)
        {
            var url = System.Configuration.ConfigurationManager.AppSettings["UrlWebServicePersona"]
                    + System.Configuration.ConfigurationManager.AppSettings["QueryStringWebServicePersona"]
                    + "?tipoDocumento=1&numero=" + cedula + "&codigo="
                    + System.Configuration.ConfigurationManager.AppSettings["CodigoWebServicePersona"]
                    + "&sysname="
                    + System.Configuration.ConfigurationManager.AppSettings["SysnameWebServicePersona"];

            return url;
        }

        public static bool Has31(int month)
        {
            switch(month)
            {
                case 1:
                    return true;
                    

                case 2:
                    return false;
                    

                case 3:
                    return true;
                    

                case 4:
                    return false;
                    

                case 5:
                    return true;
                    

                case 6:
                    return false;
                    

                case 7:
                    return true;
                    

                case 8:
                    return true;
                    

                case 9:
                    return false;
                    

                case 10:
                    return true;
                    

                case 11:
                    return false;
                    

                case 12:
                    return true;
                    

                default:
                    return false;
                    
            }
        }

        public static DateTime EmbeddedDate()
        {
         
            DateTime fechaInicio = new DateTime();

            if (DateTime.Now.Month == 1)
            {
                fechaInicio = new DateTime(DateTime.Now.Year - 1, 12, DateTime.Now.AddDays(1).Day);
            }
            else
            {
                if (DateTime.Now.Month - 1 == 2)
                {
                    if (DateTime.Now.Day == 29 && (DateTime.IsLeapYear(DateTime.Now.Year)))
                    {
                        if (DateTime.Now.Day == 30 || DateTime.Now.Day == 31)
                        {
                            fechaInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, 29);
                        }
                        else
                        {
                            fechaInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day+1);
                        }
                    }
                    else
                    {
                        if (DateTime.IsLeapYear(DateTime.Now.Year))
                        {
                            if (DateTime.Now.Day == 30 || DateTime.Now.Day == 31)
                            {
                                fechaInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, 29);
                            }
                            else
                            {
                                fechaInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day+1);
                            }
                        }
                        else if (!DateTime.IsLeapYear(DateTime.Now.Year))
                        {
                            if (DateTime.Now.Day == 30 || DateTime.Now.Day == 31 || DateTime.Now.Day == 29)
                            {
                                fechaInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, 28);
                            }
                            else
                            {
                                fechaInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day+1);
                            }
                        }
                    }
                }
                else if (!Has31(DateTime.Now.Month - 1))
                {
                    if (DateTime.Now.Day == 31)
                    {
                        fechaInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day-1);
                    }
                    else if(DateTime.Now.Day == 30)
                    {
                        fechaInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day);
                    }
                    else
                    {
                        fechaInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day+1);
                    }
                }
                else
                {
                    fechaInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day+1);
                }
            }

            return fechaInicio;
        }

        public static string GetMonthName(int month)
        {
            switch (month)
            {
                case 1:
                    return "ENERO";
                    
                case 2:
                    return "FEBRERO";

                case 3:
                    return "MARZO";

                case 4:
                    return "ABRIL";

                case 5:
                    return "MAYO";

                case 6:
                    return "JUNIO";

                case 7:
                    return "JULIO";

                case 8:
                    return "AGOSTO";

                case 9:
                    return "SEPTIEMBRE";

                case 10:
                    return "OCTUBRE";

                case 11:
                    return "NOVIEMBRE";

                case 12:
                    return "DICIEMBRE";

                default:
                    return "";
            }
        }

        public static Code.StringSuccess ValidateExtension(string extension)
        {
            Code.StringSuccess result = new Code.StringSuccess();

            if(!extension.Contains("0") && !extension.Contains("1") && !extension.Contains("2") && !extension.Contains("3") && !extension.Contains("4") && !extension.Contains("5") && !extension.Contains("6") && !extension.Contains("7") && !extension.Contains("8") && !extension.Contains("9"))
            {
                result.IsValid = false;
            }

            return null;
        }

        public void Dispose()
        {

        }
    }
}