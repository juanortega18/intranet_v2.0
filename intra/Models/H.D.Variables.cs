﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using intra.Models;
using System.ComponentModel.DataAnnotations;

namespace intra.Models
{
    public class SolicitudModel
    {

        //Variables Departamento

        public string Usurio { get; set; }

        public string Deptos { get; set; }
        
        public int DepartamentoId { get; set; }

        public int ActividadId { get; set; }
        
        public int Actividades { get; set; }

        public int SubActividadId { get; set; }

        public int SubActividades { get; set; }

        public string Descripcion { get; set; }

        public string DescripcionChat { get; set; }

        public int CodigoTipoConsulta { get; set; }

        public int CodigoEstadoConsulta { get; set; }

        public string DescripcionSolicitud { get; set; }

        public string MensajeError { get; set; }

        public int CodigoSupervisor { get; set; }

        public int CodigoDirector { get; set; }

        public int CodigoOtrosTecnicos { get; set; }

        public int CodigoEmpleado { get; set; }

        public int ReAsignarHora { get; set; }
        
        public string DepartamentoSolicitante { get; set; }

        public int CodigoDependencia { get; set; }

        public string DependenciaNombre { get; set; }

        //Variables Filtros Consulta


        public List<DepartamentoModel> Departamentos { get; set; }

        public List<ActividadesModel> TipoActividades { get; set; }

        public List<SubActividadesModel> SubTipoActividades { get; set; }

        public List<TipoConsultaModel> TipoConsulta { get; set; }

        public List<EstadoConsultaModel> EstadoConsulta { get; set; }

        public List<PersonalPgrModel> EmpleadoPGR { get; set; }

        public List<ListadoPersonalPgrModel> Listado_EmpleadoPGR { get; set; }

        public List<DependenciaRegionalModel> ListadoDependenciasRegionales { get; set; }


        //Tablas

        public List<Sol_Departamento> TB_Departamento { get; set; }

        public List<Sol_Actividades> TB_Actividades { get; set; }

        public List<Sol_Estado_Solicitud> TB_Estado_Solicitud { get; set; }

        public List<Sol_Otros_Tecnicos> TB_Sol_Otros_Tecnicos { get; set; }

        public List<Vw_Mostrar_Usuario_Con_Su_Descripcion> TB_Detalle_Lista { get; set; }

        public List<Sol_Tipo_Consulta_Solicitud> TB_Tipo_Consulta_Solicitud { get; set; }

        public List<Sol_TipodeAsistencia> TB_Tipo_Asistencia { get; set; }

        public List<Vw_Mostrar_Personal_PGR> TB_Mostrar_Personal_PGR { get; set; }

        public List<Vw_Mostrar_Otros_Tecnicos> TB_Mostrar_Otros_Tecnicos { get; set; }

        public List<Vw_Mostrar_Informacion_Perfil_Domain> TB_Mostrar_Informacion_Perfil_Domain { get; set; }

        public List<Wv_Mostrar_Log> TB_Mostrar_Logs { get; set; }

        public List<Sol_Chat> Tb_Chat { get; set; }

        public List<Vw_Mostrar_Actividades> Tb_Mostrar_Actividades { get; set; }

        public List<vw_Mostrar_Lista_Chat> Tb_Mostrar_Lista_Chat { get; set; }

        public List<Vw_Mostrar_Lista_de_Asignacion_Chat> Tb_Vw_Mostrar_Lista_de_Asignacion_Chat { get; set; }

        public List<Sol_Tecnicos_Alternativos> TB_Tecnicos_Alternativos { get; set; }

        //Variables Filtros Solicitados

        public DateTime  FechaInicial { get; set; }

        public DateTime  FechaFinal { get; set; }

        public string FiltrarPorUsuario { get; set; }

        public string FiltrarPorResponsable { get; set; }

        public int CodigoSolicitud { get; set; }

        public int CodigoEmpleados { get; set; }

        public int CodigoEmpleadosReAsignacion { get; set; }

        public int CodigoOtrosEmpleados { get; set; }

        public int CodigoEmpleadosTecnicos { get; set; }

        public string Cedula { get; set; }

        //Variable RadioButton 

        public string RadioTipodeAsistencia { get; set; }

        public string RadioEstatus { get; set; }

        //Variables DetallesSolicitud 

        public int      DECodigoSolicitud { get; set; }
        public int      DECodigoSolicitante { get; set; }
        public string   DENombreSolicitante { get; set; }
        public string   DEDepartamentoSolicitante { get; set; }
        public string   DEUsuarioDominioSolic { get; set; }
        public int      DECodigoTecnico { get; set; }
        public string   DENombreTecnico { get; set; }
        public string   DEUsuarioDominioTecn { get; set; }
        public int      DECodigoDepartamento { get; set; }
        public string   DEDepartamento { get; set; }
        public int      DECodigoTipoSolicitud { get; set; }
        public string   DETipoSolicitud { get; set; }
        public int      DECodigoSubTipoSolicitud { get; set; }
        public string   DESubTipoSolicitud { get; set; }
        public string   DEDescripcionSolicitudes { get; set; }
        public string   DEDescripcionSolucion { get; set; }
        public int      DECodigoEstadoSolicitud { get; set; }
        public string   DEEstadoSolicitud { get; set; }
        public bool     DEEstado { get; set; }
        public int      DEHoras { get; set; }
        public int      DETipodeAsistencia { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DEFechaInicioSolicitud { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DEFechaFinalSolicitud { get; set; }

        public DateTime DEFechaModificacion { get; set; }
        public DateTime DEFechaCreacion { get; set; }

        //Variable Historico 

        public int CodigoSolicitudHistorico { get; set; }

        //Radio button 

        public int CodigoEstadoRB { get; set; }

        public int CodigoTipoRB { get; set; }

        //Otros Tecnicos Por Actividad 

        public int Codigo_Otro_Tecnicos_Por_Actividad { get; set; }
        public int Codigo_Actividad { get; set; }
        public int Codigo_Tecnico { get; set; }
        public bool Estado { get; set; }
        public DateTime FhCreacion { get; set; }

        [Required]
        public string Flota { get; set; }
        [Required]
        public string Extension { get; set; }
    }
    
    public class DepartamentoModel
    {
        public int DepartamentoId { get; set; }
        public string Descripcion { get; set; }
    }

    public class ActividadesModel
    {
        public int ActividadId { get; set; }
        public string Descripcion { get; set; }
    }
    
    public class SubActividadesModel
    {
        public int SubActividadId { get; set; }
        public string Descripcion { get; set; }
    }
    public class PersonalPgrModel
    {
        public string EmpleadoId { get; set; }
        public string Descripcion { get; set; }
        public string Cedula { get; set; }
    }

    public class ListadoPersonalPgrModel 
    {
        public string EmpleadoId { get; set; }
        public string Descripcion { get; set; }
        public string Cedula { get; set; }
    }

    public class DependenciaRegionalModel
    {
        public int DependenciaId { get; set; }
        public string DependenciaNombre { get; set; }
    }

    public class TipoConsultaModel
    {
        public int ConsultaId { get; set; }
        public string Descripcion { get; set; }
    }

    public class EstadoConsultaModel
    {
        public int EstadoId { get; set; }
        public string Descripcion { get; set; }
    }

  

}