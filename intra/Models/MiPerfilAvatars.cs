﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using intra;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models
{
    public class MiPerfilAvatars
    {
        [Key]
        public int id { get; set; }
        public string codigoEmpleado { get; set; }
        public string avatar { get; set; }

    }
}