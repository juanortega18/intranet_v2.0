namespace intra.Models.Repositorio
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoContenido")]
    public partial class TipoContenido
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoContenido()
        {
            ElementoRepositorio = new HashSet<ElementoRepositorio>();
        }

        public int TipoContenidoId { get; set; }

        [StringLength(150)]
        public string TipoContenidoDescripcion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElementoRepositorio> ElementoRepositorio { get; set; }
    }
}
