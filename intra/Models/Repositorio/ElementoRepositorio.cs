namespace intra.Models.Repositorio
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ElementoRepositorio")]
    public partial class ElementoRepositorio
    {
        public int ElementoRepositorioId { get; set; }

        public int? SubSeccionRepositorioId { get; set; }

        [StringLength(100)]
        public string ElementoRepositorioRuta { get; set; }

        public byte[] ElementoRepositorioBinario { get; set; }

        public int? ElementoRepositorioMadreId { get; set; }

        [StringLength(100)]
        public string ElementoRepositorioNombre { get; set; }

        [StringLength(20)]
        public string ElementoRepositorioExtension { get; set; }

        public bool? ElementoRepositorioTipo { get; set; }

        public int? TipoContenidoId { get; set; }

        public int SeccionRepositorioId { get; set; }

        public virtual SubSeccionRepositorio SubSeccionRepositorio { get; set; }

        public virtual TipoContenido TipoContenido { get; set; }
    }
}
