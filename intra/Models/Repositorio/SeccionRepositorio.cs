namespace intra.Models.Repositorio
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SeccionRepositorio")]
    public partial class SeccionRepositorio
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SeccionRepositorioId { get; set; }

        [Required]
        [StringLength(100)]
        public string SeccionRepositorioDescripcion { get; set; }
    }
}
