namespace intra.Models.Repositorio
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class RepositorioDB : DbContext
    {
        public RepositorioDB()
            : base("name=RepositorioDB")
        {
        }

        public virtual DbSet<ElementoRepositorio> ElementoRepositorio { get; set; }
        public virtual DbSet<SeccionRepositorio> SeccionRepositorio { get; set; }
        public virtual DbSet<SubSeccionRepositorio> SubSeccionRepositorio { get; set; }
        public virtual DbSet<TipoContenido> TipoContenido { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ElementoRepositorio>()
                .Property(e => e.ElementoRepositorioRuta)
                .IsUnicode(false);

            modelBuilder.Entity<ElementoRepositorio>()
                .Property(e => e.ElementoRepositorioNombre)
                .IsUnicode(false);

            modelBuilder.Entity<ElementoRepositorio>()
                .Property(e => e.ElementoRepositorioExtension)
                .IsUnicode(false);

            modelBuilder.Entity<SeccionRepositorio>()
                .Property(e => e.SeccionRepositorioDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<SubSeccionRepositorio>()
                .Property(e => e.SubSeccionRepositorioDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<SubSeccionRepositorio>()
                .HasMany(e => e.ElementoRepositorio)
                .WithOptional(e => e.SubSeccionRepositorio)
                .HasForeignKey(e => new { e.SubSeccionRepositorioId, e.SeccionRepositorioId });

            modelBuilder.Entity<TipoContenido>()
                .Property(e => e.TipoContenidoDescripcion)
                .IsUnicode(false);
        }
    }
}
