namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VISTA_INCAUTACIONES
    {
        [Key]
        [Column(Order = 0)]
        public int INCA_ID { get; set; }

        public int CASO_ID { get; set; }

        public Int16 TIPO_ID { get; set; }

        public int? COLOR_ID { get; set; }

        public int CUST_ID { get; set; }

        [Column(Order = 1)]
        [StringLength(50)]
        public string ESTA_NOMBRE { get; set; }


        [StringLength(25)]
        public string INCA_NO { get; set; }

        //[Key]
        [Column(Order = 2)]
        public DateTime INCA_FHENTRADA { get; set; }

        //[Key]
        [Column(Order = 3)]
        public DateTime INCA_FHCREACION { get; set; }

        public string ProvinciaDestino { get; set; }
        public int INCA_LOCALIDADINV { get; set; }

        public string ProvinciaOrigen { get; set; }
        public int INCA_LOCALIDADINC { get; set; }

        public string INCA_CHASIS { get; set; }

        public string INCA_MATRICULA { get; set; }

        public string INCA_PLACA { get; set; }

        public string INCA_MODELO { get; set; }

        public string INCA_MARCA { get; set; }

        [StringLength(200)]
        public string INCA_DIRECCION { get; set; }

        public string INCA_DESCRIPCION { get; set; }

        [StringLength(255)]
        public string USUA_ID { get; set; }

        //[Key]
        [Column(Order = 4)]
        public int INCA_VALOR { get; set; }

        public int INCA_ACUERDO { get; set; }

        public int CLAS_ID { get; set; }

        public string INCA_NOTA { get; set; }

        //[Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short INCA_ESTATUS { get; set; }

        //[Key]
        [Column(Order = 6)]
        [StringLength(50)]
        public string TIPO_NOMBRE { get; set; }

        public string COLOR_NOMBRE { get; set; }

        public string INCA_TELEDEVCONTACTO { get; set; }

        public Int16? INCA_ANO { get; set; }

        public string CUST_CEDULA { get; set; }

        public string CUST_TELRES { get; set; }

        public string CUST_TELCEL { get; set; }

        public string CUST_TELOFI { get; set; }

        public string INCA_CASO { get; set; }
        
        public string CASO_NOMBRE { get; set; }

        public string CUST_NOMBRE { get; set; }

        public string CUST_TIPO { get; set; }

        public string CUST_DEPARTAMENTO { get; set; }

        public string INCA_REMITIDO { get; set; }

        public string INCA_DEVOLUCION { get; set; }

        public string CUST_RELACIONADO { get; set; }

        public string CUST_RELACIONADO_CONTACTO { get; set; }

        [NotMapped]
        public List<INV_FOTOS> INV_FOTOS { get; set; }
    }
}
