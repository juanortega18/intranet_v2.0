namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INV_INC_HISTORICO
    {
        [Key]
        public int HIST_ID { get; set; }

        public int INCA_ID { get; set; }

        public DateTime HIST_FECHA { get; set; }

        public int PROV_ID { get; set; }

        [Required]
        [StringLength(300)]
        public string HIST_DIRECCION { get; set; }

        [Required]
        [StringLength(200)]
        public string HIST_CUSTODIO { get; set; }

        public int? ENTI_ID { get; set; }

        [Required]
        [StringLength(25)]
        public string HIST_IDENTIFICACION { get; set; }
    }
}
