﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.BienesIncautados
{
    public class INV_INCALOGS
    {
        public INV_INCALOGS()
        {
            Logs = new List<Models.Logs>();
            INV_INCASLOGS = new List<INV_INCALOGS>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int INCALOGS_ID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int INCA_ID { get; set; }

        public int LogId { get; set; }

        public int? CUST_ID { get; set; }

        public bool CUST_CAMBIO { get; set; }

        public virtual INV_CUSTODIO Custodio { get; set; }

        [NotMapped]
        public virtual List<INV_INCALOGS> INV_INCASLOGS { get; set; }

        [NotMapped]
        public virtual List<Logs> Logs { get; set; }
    }
}