namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INV_ANEXOS
    {
        [Key]
        public int ANEX_ID { get; set; }

        public int ANEX_TIPO { get; set; }

        public int INCA_ID { get; set; }

        public string ANEX_NOMBRE { get; set; }

        public DateTime ANEX_FECHA { get; set; }

        public DateTime ANEX_FHCREACION { get; set; }

        public byte[] ANEX_DOCUMENTO { get; set; }
    }
}
