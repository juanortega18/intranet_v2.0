namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VISTA_INCAUTACIONES1
    {
        [Key]
        public int INCA_ID { get; set; }

        
        public int INCA_VALOR { get; set; }

        [StringLength(25)]
        public string INCA_NO { get; set; }

        public DateTime INCA_FHCREACION { get; set; }

        public DateTime? INCA_FHENTRADA { get; set; }

        [StringLength(25)]
        public string INCA_CASO { get; set; }

        public short INCA_ESTATUS { get; set; }

        [StringLength(200)]
        public string INCA_SUJETO { get; set; }

        [StringLength(25)]
        public string INCA_CEDULA { get; set; }

        [StringLength(255)]
        public string USUA_ID { get; set; }

        [StringLength(50)]
        public string ESTA_NOMBRE { get; set; }

        [StringLength(50)]
        public string TIPO_NOMBRE { get; set; }
    }
}
