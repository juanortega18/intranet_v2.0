namespace intra.Models.BienesIncautados
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using intra.Models.BienesIncautados;

    public partial class inv : DbContext
    {
        public inv()
            : base("name=inv")
        {
        }

        public virtual DbSet<INV_ANEXOS> INV_ANEXOS { get; set; }
        public virtual DbSet<INV_CASOS> INV_CASOS { get; set; }
        public virtual DbSet<INV_CLASES> INV_CLASES { get; set; }
        public virtual DbSet<INV_COLORES> INV_COLORES { get; set; }
        public virtual DbSet<INV_ESTATUS> INV_ESTATUS { get; set; }
        public virtual DbSet<INV_FOTOS> INV_FOTOS { get; set; }
        public virtual DbSet<INV_INC_HISTORICO> INV_INC_HISTORICO { get; set; }
        public virtual DbSet<INV_INCAUTACIONES> INV_INCAUTACIONES { get; set; }
        public virtual DbSet<INV_MOVIMIENTOS> INV_MOVIMIENTOS { get; set; }
        public virtual DbSet<INV_PROVINCIAS> INV_PROVINCIAS { get; set; }
        public virtual DbSet<INV_TIPOS> INV_TIPOS { get; set; }
        public virtual DbSet<VISTA_INCAUTACIONES1> VISTA_INCAUTACIONES1 { get; set; }
        public virtual DbSet<VISTA_INCAUTACIONES> VISTA_INCAUTACIONES { get; set; }
        public virtual DbSet<INV_INCALOGS> INV_INCALOGS { get; set; }
        public virtual DbSet<INV_CUSTODIO> INV_CUSTODIO { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<INV_INCAUTACIONES>()
                .Property(e => e.INCA_NOTA)
                .IsUnicode(false);
        }
    }
}
