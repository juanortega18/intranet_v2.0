namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INV_CLASES
    {
        [Key]
        public int CLAS_ID { get; set; }

        [Required]
        [StringLength(60)]
        public string CLAS_NOMBRE { get; set; }
    }
}
