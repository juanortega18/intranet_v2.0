namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web;

    public partial class INV_INCAUTACIONES
    {
        [Key]
        public int INCA_ID { get; set; }

        public int? INCA_ID_MAIN { get; set; }

        [StringLength(50)]
        public string INCA_NO { get; set; }

        public DateTime INCA_FHCREACION { get; set; }

        public DateTime INCA_FHENTRADA { get; set; }

        public string INCA_CASO { get; set; }

        public string INCA_CHASIS { get; set; }

        public string INCA_MATRICULA { get; set; }

        public string INCA_PLACA { get; set; }

        public int CASO_ID { get; set; }

        public short INCA_CASO_ESTATUS { get; set; }

        public short TIPO_ID { get; set; }

        [StringLength(100)]
        public string INCA_MODELO { get; set; }

        [StringLength(200)]
        public string INCA_MARCA { get; set; }

        public short? INCA_ANO { get; set; }

        public short INCA_ESTATUS { get; set; }

        public int CLAS_ID { get; set; }
        
        public int INCA_VALOR { get; set; }

        public int INCA_ACUERDO { get; set; }

        [Column(TypeName = "text")]
        public string INCA_NOTA { get; set; }

        [StringLength(255)]
        public string USUA_ID { get; set; }

        public int INCA_LOCALIDADINC { get; set; }

        public int INCA_LOCALIDADINV { get; set; }

        [StringLength(100)]
        public string INCA_REMITIDO { get; set; }
        
        public DateTime? INCA_FHVENCSEGURO { get; set; }

        [StringLength(100)]
        public string INCA_DEVOLUCION { get; set; }

        [StringLength(20)]
        public string INCA_TELEDEVCONTACTO { get; set; }

        public DateTime? INCA_FHDEVOLUCION { get; set; }

        public int? INCA_PAGOMANT { get; set; }

        public int? CUST_ID { get; set; }

        public int? COLOR_ID { get; set; }

        [StringLength(200)]
        public string INCA_DIRECCION { get; set; }

        [StringLength(100)]
        public string INCA_LATITUD { get; set; }

        [StringLength(100)]
        public string INCA_LONGITUD { get; set; }
        
        [StringLength(30)]
        public string INCA_CORREOEVN { get; set; }

        public string INCA_DESCRIPCION { get; set; }

        [NotMapped]
        public string[] fotoDescripcion { get; set; }
        [NotMapped] 
        public List<INV_FOTOS> INV_FOTOS { get; set; }
        [NotMapped]
        public List<HttpPostedFileBase> ARCHIVO_VISTA { get; set; }

        [NotMapped]
        public virtual INV_CUSTODIO INV_CUSTODIO { get; set; }

        [NotMapped]
        public virtual List<INV_FOTOS> INV_SOLO_FOTOS { get; set; }
    }
}