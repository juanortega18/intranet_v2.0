namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INV_ESTATUS
    {
        [Key]
        public short ESTA_ID { get; set; }

        [Required]
        [StringLength(50)]
        public string ESTA_NOMBRE { get; set; }

        public short ESTA_TIPO { get; set; }
    }
}
