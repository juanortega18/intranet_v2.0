namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INV_PROVINCIAS
    {
        [Key]
        public int ID_Provincia { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }
    }
}
