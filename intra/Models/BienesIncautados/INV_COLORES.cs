namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INV_COLORES
    {
        [Key]
        public int COLOR_ID { get; set; }

        [StringLength(300)]
        public string COLOR_NOMBRE { get; set; }
    }
}
