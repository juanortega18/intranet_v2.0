namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INV_MOVIMIENTOS
    {
        [Key]
        public int MOV_ID { get; set; }

        public int MOV_TIPO { get; set; }

        public int INCA_ID { get; set; }

        public string MOV_DESCRIPCION { get; set; }

        public string MOV_RESPONSABLE { get; set; }

        public DateTime MOV_FECHA { get; set; }
    }
}
