namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web;

    public partial class INV_FOTOS
    {
        [Key]
        public int FOTO_ID { get; set; }

        [StringLength(100)]
        public string FOTO_DESCRIPCION { get; set; }

        [StringLength(30)]
        public string FOTO_TIPO { get; set; }

        public DateTime? FOTO_FHCREACION { get; set; }

        [StringLength(25)]
        public string INCA_NO { get; set; }

        public byte[] FOTO_ARCHIVO { get; set; }

        [StringLength(50)]
        public string FOTO_PESO { get; set; }

        [StringLength(100)]
        public string FOTO_NOMBRE{ get; set; }

        public bool? FOTO_ESTATUS { get; set; }

        [NotMapped]
        public string IMAGE_SRC { get; set; }

        [NotMapped]
        public string IMAGE_IMAGE { get; set; }
    }
}
