namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INV_TIPOS
    {
        [Key]
        public int TIPO_ID { get; set; }

        [Required]
        [StringLength(50)]
        public string TIPO_NOMBRE { get; set; }

        public short TIPO_PEDECEDERO { get; set; }

        public int CLAS_ID { get; set; }
    }
}
