namespace intra.Models.BienesIncautados
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INV_CASOS
    {
        public INV_CASOS()
        {
            CASO_INCAUTACIONES = new List<VISTA_INCAUTACIONES>();
        }

        [Key]
        public int CASO_ID { get; set; }

        public string CASO_NOMBRE { get; set; }

        public string CASO_NO { get; set; }

        [StringLength(300)]
        public string USUA_ID { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime CASO_FECHA { get; set; }

        public string CASO_NOTA { get; set; }

        public short ESTA_ID { get; set; }
        public int FISCALIA_ID { get; set; }

        [NotMapped]
        public List<VISTA_INCAUTACIONES> CASO_INCAUTACIONES { get; set; }
    }
}
