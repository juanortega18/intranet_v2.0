﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.BienesIncautados
{
    public partial class INV_CUSTODIO
    {
        [Key]
        public int CUST_ID { get; set; }

        public bool? CUST_ESTADO { get; set; }

        //public int? MONEDA_TIPO_ID { get; set; }

        //public Decimal? CUST_VALOR { get; set; }

        [StringLength(100)]
        public string CUST_NOMBRE { get; set; }

        [StringLength(20)]
        public string CUST_TELRES { get; set; }

        [StringLength(20)]
        public string CUST_TELCEL { get; set; }

        [StringLength(20)]
        public string CUST_TELOFI { get; set; }

        [StringLength(15)]
        public string CUST_CEDULA { get; set; }

        [StringLength(300)]
        public string CUST_DIRECCION { get; set; }

        [StringLength(30)]
        public string CUST_CORREO { get; set; }

        [StringLength(50)]
        public string USUA_ID { get; set; }

        [StringLength(30)]
        public string CUST_TIPO { get; set; }
        
        [StringLength(60)]
        public string CUST_DEPARTAMENTO { get; set; }

        [StringLength(60)]
        public string CUST_DIRECTORDPT { get; set; }

        public string CUST_RELACIONADO { get; set; }

        public string CUST_RELACIONADO_CONTACTO { get; set; }

        [NotMapped]
        public bool isModal { get; set; }
    }
}