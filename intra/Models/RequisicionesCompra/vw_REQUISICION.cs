﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.RequisicionesCompra
{
   
  public  class vw_REQUISICION
    {
        [Key]
        public string NO_REQUISICION { get; set; }
        public string PRODUCTO { get; set; }
        public string DESCRIPCIÓN { get; set; }

    }
}
