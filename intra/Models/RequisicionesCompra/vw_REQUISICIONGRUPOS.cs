﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.RequisicionesCompra
{
    public class vw_REQUISICIONGRUPOS
    {
        [Key]
        public string NO_REQUISICION { get; set; }
        [DisplayFormat(DataFormatString ="{0:dd-MM-yyyy}")]
        public DateTime FECHA_REQUISICION { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime FECHA_ENTRADA_SISTEMA { get; set; }
        public string ESTADO_PROCESO { get; set; }
        public string DEPTO { get; set; }
        public string EMPLEADO { get; set; }
        public string TIPO_DE_COMPRA { get; set; }
    }
}
