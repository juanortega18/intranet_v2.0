﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Cumpleanos
{
    public class MostraFelicitaciones
    {
        [Key]

        public string NombreEmpleado { get; set; }
        public string Mensaje { get; set; }
        public DateTime Fecha { get; set; }

    }
}
