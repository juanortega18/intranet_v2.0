﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Cumpleanos
{
    public partial class EstadoCumpleanos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdEstado { get; set; }
        public string CodigoEmpleado { get; set; }
        public string Estado { get; set; }
        public string NombreCompleto { get; set; }
        public string Departamento { get; set; }


    }
}
