﻿namespace intra.Models.Cumpleanos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Web;

    public partial class Vw_Mostrar_Empleados_Cumpleanos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string  Codigo  { get; set; }
        public string NombreCompleto { get; set; }
        public string CodigoDependencia { get; set; }
        public string Dependencia { get; set; }
        public string CodigoDepartamento { get; set; }
        public string Departamento { get; set; }
        public System.Nullable <DateTime> FechaNacimiento { get; set; }
        public int Dia { get; set; }
        public int Mes { get; set; }
        public string Estado { get; set; }
        public int IdEstado { get; set; }
        public string CodigoEmpleado { get; set; }

    }
  
  
   

}