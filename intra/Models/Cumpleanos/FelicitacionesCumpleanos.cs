﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.Cumpleanos
{
    public partial class FelicitacionesCumpleanos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int CodigoEmpleado { get; set; }
        public int CodigoFestejado { get; set; }
        public string Mensaje { get; set; }
        public DateTime Fecha { get; set; }

    }

}
