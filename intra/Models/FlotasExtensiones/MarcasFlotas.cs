﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.FlotasExtensiones
{
    public partial class MarcasFlotas
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int MarcaId { get; set; }
        [Display(Name = "Marca")]
        [Required(ErrorMessage = "Debe indicar la descripcion")]
        [StringLength(200)]
        public string MarcaDescripcion { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime MarcaFhCreacion { get; set; }
        [Display(Name = "Usuario Creador")]
        [StringLength(200)]
        public string MarcaUsuario { get; set; }
    }
}
