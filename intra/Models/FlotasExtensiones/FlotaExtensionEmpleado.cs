﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.FlotasExtensiones
{
    [Table("FlotaExtensionEmpleado")]
    public partial class FlotaExtensionEmpleado
    {
        [Key]
        public int FlotExtEmpId { get; set; }

        [StringLength(15)]
        public string FlotExtEmpFlota { get; set; }

        [StringLength(30)]
        public string FlotExtEmpExtesion { get; set; }

        public int? FlotExtEmpEmpleadoId { get; set; }

        public bool? FlotExtEmpDatosConfirmados { get; set; }

        [NotMapped]
        public string NumeroFlotaViejo { get; set; }
    }
}