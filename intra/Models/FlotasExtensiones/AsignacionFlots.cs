﻿namespace intra.Models.FlotasExtensiones
{
using System;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


    public class AsignacionFlots
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AsignacionId { get; set; }
        
        public string ModeloId { get; set; }
        
        public string MarcaId { get; set; }

        //[Required(ErrorMessage = "Debe indicar el IMEI")]
 
        public string AsignacionImei { get; set; }
        
        public string AsignacionSimNumeracion { get; set; }

        //[Required(ErrorMessage = "Debe indicar por quien fue despachado")]
  
        public string AsignacionDespachado { get; set; }

        //[Required(ErrorMessage = "Debe indicar el numero de la flota")]
   
        public string AsignacionNoFlota { get; set; }

        public string AsignacionUsuario { get; set; }
        public bool Estado { get; set; }

        public DateTime AsignacionFechaCreacion { get; set; }

        [Required(ErrorMessage = "El campo Cédula es obligatorio")]
        [StringLength(13,ErrorMessage = "Cedula inválida")]
        [MinLength(13, ErrorMessage = "Cedula inválida")]
        public string AsignacionCedula { get; set; }

        public string AsignacionCargo{ get; set; }

        [Required(ErrorMessage = "El código de empleado es obligatorio")]
        public string AsignacionEmpleadoId { get; set; }
        
        [Required(ErrorMessage = "El nombre del empleado es obligatorio")]
        public string AsignacionEmpleado { get; set; }
        
        public string AsignacionDepartamentoId { get; set; }

        [Required(ErrorMessage = "El Departamento es obligatorio")]
        public string AsignacionDepto { get; set; }
        
        public string EstatusId { get; set; }
        
        public string AsignacionExt { get; set; }
        
        public string AsignacionCosto { get; set; }

        public string AsignacionNoReferencia { get; set; }

        [NotMapped]
        public string NumeroFlotaViejo { get; set; }
        //public virtual MarcasFlotas MarcasFlotas { get; set; }
        //public virtual ModeloFlotas ModeloFlotas { get; set; }

    }

  
  


 

}