﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.FlotasExtensiones
{

    public partial class ModeloFlotas
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int ModeloId { get; set; }

        [Required(ErrorMessage = "Debe indicar la descripcion")]
        [Display(Name = "Modelo")]
        [StringLength(100)]
        public string ModeloDescripcion { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ModeloFhCreacion { get; set; }
        [Display(Name = "Usuario Creador")]
        [StringLength(100)]
        public string ModeloUsuario { get; set; }

        public int MarcaId { get; set; }
        public virtual MarcasFlotas MarcasFlotas { get; set; }
    }

}
