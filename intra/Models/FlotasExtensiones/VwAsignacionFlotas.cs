﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace intra.Models.FlotasExtensiones
{
    public class VwAsignacionFlotas
    {
        public int AsignacionId { get; set; }
        public string AsignacionEmpleado { get; set; }
        public string AsignacionCedula { get; set; }
        public string AsignacionCargo { get; set; }
        public string AsignacionEmpleadoId { get; set; }
        public string AsignacionDepto { get; set; }
        public string AsignacionExt { get; set; }
        public string MarcaDescripcion { get; set; }
        public string ModeloDescripcion { get; set; }
        public string AsignacionImei { get; set; }
        public string AsignacionSimNumeracion { get; set; }
        public string AsignacionNoFlota { get; set; }
        public string AsignacionNoReferencia { get; set; }
        public string AsignacionEstado { get; set; }
        public DateTime Fecha { get; set; }
        public string costo { get; set; }
    }
}
