﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra.Models.FlotasExtensiones
{
    public partial class EstatusFlotas
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EstatusId { get; set; }

        [StringLength(100)]
        public string EstatusDescripcion { get; set; }
    }

}
