﻿namespace intra
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Web;
    public partial class Tutoriales
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int TutorialId { get; set; }
        
        public string TutorialNombre { get; set; }
      
        public string TutorialUsuario { get; set; }
       
        public DateTime FechaCreacion { get; set; }
      
        public string TutorialUrl { get; set; }
        public string Portada { get; set; }
        public bool Estado { get; set; }

    }
}