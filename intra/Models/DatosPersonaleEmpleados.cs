﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace intra.Models
{
   public class DatosPersonaleEmpleados
    {
        public string EmpleadoID { get; set; }
        public string EmpresaId { get; set; }
        public string EmpleadoIDAnterior { get; set; }
        public string Nombre { get; set; }
        public string Nombre1 { get; set; }
        public string Nombre2 { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Tarjeta { get; set; }
        public string Alias { get; set; }
        public bool Encargado { get; set; }
        public string GrupoEmpleadoId { get; set; }
        public string Entrenamiento { get; set; }
        public string Direccion { get; set; }
        public string CodigoPostal { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string TelefonoExtensionEmp { get; set; }
        public string Fax { get; set; }
        public string Celular { get; set; }
        public string Beeper { get; set; }
        public string Email { get; set; }
        public string URL { get; set; }
        public decimal Salario { get; set; }
        public string CiudadID { get; set; }
        public string Cedula { get; set; }
        public int EstadoCivil { get; set; }
        public int Sexo { get; set; }
        public int Estatus { get; set; }
        public int FormaPago { get; set; }
        public string CargoId { get; set; }
        public string DeptoID { get; set; }
        public string TipoEmpleadoID { get; set; }
        public bool Pendiente { get; set; }
        public bool Planta { get; set; }
        public bool ISR { get; set; }
        public bool IDSS { get; set; }
        public bool AFP { get; set; }
        public bool ARS { get; set; }
        public string UsuarioId { get; set; }
        public string Carnet { get; set; }
        public string CuentaBanco { get; set; }
        public DateTime Cumpleano { get; set; }
        public string TituloID { get; set; }
        public string MonedaID { get; set; }
        public string DepartamentoIDAnt { get; set; }
        public string DepartamentoId { get; set; }
        public string DependenciaID { get; set; }
        public string DependenciaIDAnt { get; set; }
        public string SeccionIDAnt { get; set; }
        public string SeccionID { get; set; }
        public string DivisionIDAnt { get; set; }
        public string DivisionID { get; set; }
        public string Pasaporte { get; set; }
        public string LicenciaConducir { get; set; }
        public string PropositoId { get; set; }
        public string TipoNominaID { get; set; }
        public string ProvinciaIDProvinciaID { get; set; }
        public string DeptoIDProvisional { get; set; }
        public string MunicipioID { get; set; }
        public string Tratamiento { get; set; }
        public string TarjetaAnterior { get; set; }
        public bool SolitudEmpleado { get; set; }
        public int NumeroArchivo { get; set; }
        public int CausaNombramiento { get; set; }
        public int TurnoId { get; set; }


        public bool MinisterioPublico { get; set; }
        public bool Poncha { get; set; }
        public bool Nomina { get; set; }
        public bool Incorporado { get; set; }
        public bool EmpleadoPlanta { get; set; }
        public bool NombraDesignacion { get; set; }
        public bool Fotografia { get; set; }
        public bool certificadomedico2 { get; set; }
        public bool certificadomedico { get; set; }
        public bool CertificanoDelincuencia { get; set; }
        public bool CurriculumVitae { get; set; }
        public bool CertificadoEstudiante { get; set; }
        public bool CopiaCedula { get; set; }
        public bool Retenido { get; set; }
        public bool Estado { get; set; }
        public int TipoSangre { get; set; }
        public string SolicitudID { get; set; }
        public string PaisID { get; set; }
        public string ProvinciaIDNacimiento { get; set; }
        public string MunicipioIDNacimiento { get; set; }
        public string Tiponominald { get; set; }
        public string PaisIDNacimiento { get; set; }
        public string Sector { get; set; }
        public string Referencia { get; set; }

        public DateTime FechaNombramiento { get; set; }
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaSalida { get; set; }
        public DateTime FechaIngresoNomina { get; set; }
        public DateTime FechaCuentaBanco { get; set; }
        public DateTime FechaTomaPosesion { get; set; }
        public string TipoCuentaID { get; set; }
        public DateTime FechaCuenta { get; set; }
        public string DeptoIDAuxiliar { get; set; }
        public string TelefonoOficina { get; set; }
        public string CreadoPor { get; set; }
        public string ModificadoPor { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }
        public int RecordID { get; set; }
        public int TipoGrupoMinisterio { get; set; }
        public int dppantes { get; set; }
        public int deptoantes { get; set; }
        public int ID_Persona { get; set; }
      




    }
}
