﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using intra.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace intra
{
    //public class Sol_Registro_Solicitud
    //{
    //    [Key]
    //    public int SolicitudId { get; set; }
    //    public int SolicitanteId { get; set; }
    //    public string DomainUserSolicitante { get; set; }
    //    public int TecnicoId { get; set; }
    //    public string DomainUserTecnico { get; set; }
    //    public int DepartamentoId { get; set; }
    //    public int Tipo_SolicitudId { get; set; }
    //    public int? Tipo_Sub_SolicitudId { get; set; }
    //    public string DescripcionSolicitud { get; set; }
    //    public string DescripcionSolucion { get; set; }
    //    public int EstadoId { get; set; }
    //    public bool Estado { get; set; }
    //    public int Horas { get; set; }
    //    public int TipodeAsistencia { get; set; }
    //    public DateTime FhInicioSolicitud { get; set; }
    //    public DateTime FhFinalSolicitud { get; set; }
    //    public DateTime FhModificacion { get; set; }
    //    public DateTime FhCreacion { get; set; }
    //    public int? DependenciaRegionalId { get; set; }
    //    public virtual ICollection<Eventos> Eventos { get; set; }
    //    //public virtual Sol_Actividades Sol_Actividades { get; set; }
    //    //public virtual Sol_Sub_Actividades Sol_Sub_Actividades { get; set; }
    //    //public virtual VISTA_EMPLEADOS VISTA_EMPLEADOS { get; set; }
    //    public virtual Sol_Estado_Solicitud Sol_Estado_Solicitud { get; set; }


    //}

    public class Sol_DependenciasRegionales
    {
        [Key]
        public int DependenciaId { get; set; }
        [Required(ErrorMessage = "El nombre de la dependencia es requerido")]
        public string DependenciaNombre { get; set; }
        [Required(ErrorMessage ="El codigo del supervisor es requerido")]
        public int DependenciaSupervisorId { get; set; }
        public string DependenciaSupervisorCorreo { get; set; }
        public bool DependenciaEstado { get; set; }
        public DateTime DependenciaFhCreacion { get; set; }
        public string DependenciaUsuarioCreador { get; set; }
        [NotMapped]
        public string supervisor { get; set; }

    }

    public class Sol_Departamento
    {
        [Key]
        public int DepartamentoId { get; set; }
        public int DependenciaId { get; set; }
        public int DirectorId  { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public DateTime Fhcreacion { get; set; }

    }

    public class Sol_Actividades
    {

        [Key]
        public int ActividadId { get; set; }
        public int DepartamentoId { get; set; }
        public int SupervisorId { get; set; }
        public string Correo { get; set; }
        public string Descripcion { get; set; }
        public int Horas { get; set; }
        public bool Estado { get; set; }
        public DateTime Fhcreacion { get; set; }
        public Sol_Departamento Departamento { get; set; }
        public List<Sol_Sub_Actividades> SubActividades { get; set; }

    }

    public class Sol_Sub_Actividades
    {
        [Key]
        public int SubActividadId { get; set; }
        public int ActividadId { get; set; }
        public int DepartamentoId { get; set; }
        public string Descripcion { get; set; }
        public int Hora { get; set; }
        public bool Estado { get; set; }
        public DateTime FhCreacion { get; set; }
    }

    public class Vw_Mostrar_Actividades
    {
        [Key]
        public int CodigoActividad { get; set; }
        public int CodigoDepartamento { get; set; }
        public int SupervisorId { get; set; }
        public string Correo { get; set; }
        public string Cedula { get; set; }
        public string Descripcion { get; set; }
        public int Horas { get; set; }
        public bool Estado { get; set; }
        public DateTime FhCreacion { get; set; }
    }




    public class Sol_Tipo_Consulta_Solicitud
    {
        [Key]
        public int ConsultaId { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public DateTime Fhcreacion { get; set; }

    }


    public class Sol_Estado_Solicitud
    {
        [Key]
        public int EstadoId { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public DateTime Fhcreacion { get; set; }
    }

    public class Sol_Otros_Tecnicos
    {
        [Key]
        public int Otros_Id { get; set; }
        public int SolicitudId { get; set; }
        public int TecnicoId { get; set; }
        public bool Estado { get; set; }
        public DateTime FhModificacion { get; set; }
        public DateTime Fhcreacion { get; set; }
    }
    
    public class Sol_Logs
    {
        [Key]
        public int logId { get; set; }
        public int SistemaId { get; set; }
        public int SolicitudId { get; set; }
        public string logIP { get; set; }
        public string logUsuario { get; set; }
        public DateTime logFecha { get; set; }
        public string logAccion { get; set; }

    }


    public class Wv_Mostrar_Log
    {
        [Key]
        public int CodigoLog { get; set; }
        public int CodigoSolicitud { get; set; }
        public int CodigoSistema { get; set; }
        public string IP { get; set; }
        public string CodigoResponsable { get; set; }
        public string Responsable { get; set; }
        public string Departamento { get; set; }
        public DateTime logFecha { get; set; }
        public string logAccion { get; set; }
    }
    
    public class Sol_Chat
    {
        [Key]
        public int ChatId { get; set; }
        public int SolicitudId { get; set; }
        public string Empleadoid { get; set; }
        public string Usuario { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public string Archivo { get; set; }
        public DateTime FhCreacion { get; set; }

    }
    
    public class Vw_Mostrar_Usuario_Con_Su_Descripcion
    {
        [Key]
        public int CodigoSolicitud { get; set; }
        public int CodigoSolicitante { get; set; }
        public string NombreSolicitante { get; set; }
        public string DepartamentoSolicitante { get; set; }
        public string UsuarioDominioSolic { get; set; }
        public int CodigoTecnico { get; set; }
        public string NombreTecnico { get; set; }
        public string UsuarioDominioTecn { get; set; }       
        public int CodigoDepartamento { get; set; }   
        public string Departamento { get; set; }
        public string NombreSupervisor { get; set; }
        public string DepartamentoSupervisor { get; set; }
        public int CodigoTipoSolicitud { get; set; }
        public string TipoSolicitud { get; set; }
        public int CodigoSubTipoSolicitud { get; set; }
        public string SubTipoSolicitud { get; set; }
        public string DescripcionSolicitud { get; set; }
        public string DescripcionSolucion { get; set; }
        public int CodigoEstadoSolicitud { get; set; }
        public string EstadoSolicitud { get; set; }
        public bool Estado { get; set; }
        public int Horas { get; set; }
        public int TipodeAsistencia { get; set; }
        public DateTime FechaInicioSolicitud { get; set; }
        public DateTime FechaFinalSolicitud { get; set; }
        public DateTime FechaModificacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int DependenciaId { get; internal set; }
        public string DependenciaNombre { get; internal set; }

    }
    public class Vw_Mostrar_Personal_PGR
    {
        [Key]
        public string Codigo { get; set; }
        public string NombreCompleto { get; set; }
        public string Cedula { get; set; }
        public string CodigoDependencia { get; set; }
        public string Dependencia { get; set; }
        public string CodigoDepartamento { get; set; }
        public string Departamento { get; set; }
    }
    public class Vw_Mostrar_Otros_Tecnicos
    {
        [Key]
        public int Otros_Id { get; set; }
        public int SolicitudId { get; set; }
        public int TecnicoId { get; set; }
        public string NombreTecnico { get; set; }
        public string Cedula { get; set; }
        public bool Estado { get; set; }
        public DateTime FhCreacion { get; set; }
    }


    public class Vw_Mostrar_Informacion_Perfil_Domain
    {
        [Key]
        public string Cedula { get; set; }
        public string Departamento { get; set; }
        public string NombreCompleto { get; set; }
        public string Usuario { get; set; }
        public string Correo { get; set; }
    }

    public class vw_Mostrar_Lista_Chat
    {
        [Key]
        public int CodigoSolicitud { get; set; }
        public string Motivo { get; set; }
        public string EstadoSolicitud { get; set; }
        public string CodigoEmpleados { get; set; }
        public string Usuario { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public string Archivo { get; set; }
        public DateTime FhCreacion { get; set; }
    }

    public class Vw_Mostrar_Lista_de_Asignacion_Chat
    {
        [Key]
        public int CodigoSolicitud { get; set; }
        public int CodigoTecnico { get; set; }
        public string TecnicoUsuario { get; set; }
        public int CodigoUsuario { get; set; }
        public string Usuario { get; set; }
        public string  Cedula { get; set; }
        public string  Motivo { get; set; }
        public bool Estado { get; set; }
        public DateTime FhCreacion { get; set; }
    }


    public class Sol_TipodeAsistencia
    {
        [Key]
        public int TipoAsistenciaId { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
    }

    public class Sol_Otros_Tecnicos_Por_Actividades 
    {
        [Key]
        public int Otro_Tecnicos_Por_ActividadId { get; set; }
        public int ActividadId { get; set; }
        public int TecnicoId { get; set; }
        public bool Estado { get; set; }
        public DateTime FhCreacion { get; set; }

    }
    public class Sol_Tecnicos_Alternativos
    {
        [Key]
        public int OtrosTecnicos { get; set; }
        public int ActividadId { get; set; }
        public int SupervisorId { get; set; }
        public int TecnicoId { get; set; }
        public bool Estado { get; set; }
        public DateTime FhCreacion { get; set; }
        public int? DependenciaRegionalId { get; set; }
        public bool? AgregarServicio { get; set; }
        public virtual Sol_Actividades Sol_Actividades { get; set; }
    }



    [System.ComponentModel.DataAnnotations.Schema.Table("LogErrores")]
    public class LogErrores
    {
        [Key]
        public int LogErroresId { get; set; }
        public string Usuario { get; set; }
        public string Ip { get; set; }
        public string LineaError { get; set; }
        public DateTime Fecha { get; set; }
        public int CodigoErrorId { get; set; }
    }

    [System.ComponentModel.DataAnnotations.Schema.Table("CodigoError")]
    public class CodigoError
    {
        [Key]
        public int CodigoErrorId { get; set; }
        public string MensajeError { get; set; }
        public string MensajeAmigable { get; set; }
        public string MensajeErrorInterno { get; set; }
    }

    [System.ComponentModel.DataAnnotations.Schema.Table("EscanerRegistro")]
    public class EscanerRegistro
    {
        [Key]
        public int EscanerRegistroId { get; set; }
        public int EmpleadoId { get; set; }
        public string EmpleadoNombre { get; set; }
        public DateTime EscanerRegistroFechaPublicacion { get; set; }
        public int DepartamentoId { get; set; }
        public string EscanerRegistroDetalle { get; set; }
        public string EscanerRegistroRazon { get; set; }
        public string EscanerLugarProcedencia { get; set; }
        public string EscanerEmisor { get; set; }
    }

    [System.ComponentModel.DataAnnotations.Schema.Table("EscanerArchivos")]
    public class EscanerArchivos
    {
        [Key]
        public int EscanerArchivosId { get; set; }
        public string EscanerArchivosRuta { get; set; }
        public int EscanerRegistroId { get; set; }
    }

    [System.ComponentModel.DataAnnotations.Schema.Table("EscanerDocumentoVinculado")]
    public class EscanerDocumentoVinculado
    {
        [Key]
        public int EscanerDocumentoVinculadoId { get; set; }
        public int EmpleadoId { get; set; }
        public string EmpleadoNombre { get; set; }
        public int EscanerDocumentoVinculadoEstado { get; set; }
        public int EscanerRegistroId { get; set; }
    }

    [System.ComponentModel.DataAnnotations.Schema.Table("EscanerChat")]
    public class EscanerChat
    {
        [Key]
        public int EscanerChatId { get; set; }
        public int EmpleadoId { get; set; }
        public string EmpleadoNombre { get; set; }
        public string EscanerChatDescripcion { get; set; }
        public short EscanerChatEstado { get; set; }
        public string EscanerChatArchivo { get; set; }
        public DateTime EscanerChatFechaCreacion { get; set; }
        public int EscanerRegistroId { get; set; }
    }

    public class Sol_Tecnico_Dependencia
    {
        [Key]
        public int TecnicoDependenciaId { get; set; }
        public int TecnicoDependenciaEmpleadoId { get; set; }
        public string TecnicoDependenciaNombre { get; set; }
        public string TecnicoDependenciaCedula { get; set; }
        public int DependenciaId { get; set; }
        public bool TecnicoDependenciaEstado { get; set; }
        public bool TecnicoDependenciaCrearSolicitante { get; set; }
        public DateTime TecnicoDependenciaFhCreacion { get; set; }
        public string TecnicoDependenciaUsuarioCreador { get; set; }
        public Sol_DependenciasRegionales Dependencia { get; set; }
    }
}