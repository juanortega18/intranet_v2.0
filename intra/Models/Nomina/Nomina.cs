namespace intra.Models.Nomina
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Nomina : DbContext
    {
        public Nomina()
            : base("name=Nomina")
        {
        }

        public virtual DbSet<ArchivoNominaIntranet> ArchivoNominaIntranet { get; set; }
        public virtual DbSet<NominaIntranet> NominaIntranet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaNombre)
                .IsUnicode(false);

            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaCargo)
                .IsUnicode(false);

            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaCedula)
                .IsUnicode(false);

            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaIngresoBruto)
                .HasPrecision(19, 4);

            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaOtrosIngresos)
                .HasPrecision(19, 4);

            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaTotalIngresos)
                .HasPrecision(19, 4);

            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaAFP)
                .HasPrecision(19, 4);

            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaISR)
                .HasPrecision(19, 4);

            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaSFS)
                .HasPrecision(19, 4);

            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaOtrosDescuentos)
                .HasPrecision(19, 4);

            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaTotalDescuentos)
                .HasPrecision(19, 4);

            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaIngresoNeto)
                .HasPrecision(19, 4);

            modelBuilder.Entity<NominaIntranet>()
                .Property(e => e.NominaUsuario)
                .IsUnicode(false);
        }
    }
}
