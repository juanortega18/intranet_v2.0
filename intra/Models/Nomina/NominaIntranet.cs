namespace intra.Models.Nomina
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NominaIntranet")]
    public partial class NominaIntranet
    {
        [Key]
        public int NominaId { get; set; }

        [Required]
        [StringLength(80)]
        public string NominaNombre { get; set; }

        [Required]
        [StringLength(50)]
        public string NominaCargo { get; set; }

        [Required]
        [StringLength(20)]
        public string NominaCedula { get; set; }

        [Column(TypeName = "money")]
        public decimal NominaIngresoBruto { get; set; }

        [Column(TypeName = "money")]
        public decimal NominaOtrosIngresos { get; set; }

        [Column(TypeName = "money")]
        public decimal NominaTotalIngresos { get; set; }

        [Column(TypeName = "money")]
        public decimal NominaAFP { get; set; }

        [Column(TypeName = "money")]
        public decimal NominaISR { get; set; }

        [Column(TypeName = "money")]
        public decimal NominaSFS { get; set; }

        [Column(TypeName = "money")]
        public decimal NominaOtrosDescuentos { get; set; }

        [Column(TypeName = "money")]
        public decimal NominaTotalDescuentos { get; set; }

        [Column(TypeName = "money")]
        public decimal NominaIngresoNeto { get; set; }

        public int? NominaAnio { get; set; }

        public int? NominaMes { get; set; }

        [StringLength(30)]
        public string NominaUsuario { get; set; }

        [NotMapped]
        public string NominaMesNombre { get; set; }
    }
}
