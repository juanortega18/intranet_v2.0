namespace intra.Models.Nomina
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web;

    [Table("ArchivoNominaIntranet")]
    public partial class ArchivoNominaIntranet
    {
        [Key]
        public int ArchNomId { get; set; }
             
       
        public int? ArchNomMes { get; set; }

        public int?  ArchNomAnio { get; set; }

        public byte[] ArchNomArchivo { get; set; }

        [NotMapped]
        public HttpPostedFileBase ArchivoSubido {get;set; }

        [NotMapped]
        public string ArchNomMesNombre { get; set; }

        [NotMapped]
        public string Jmnx { get; set; }
    }
}
