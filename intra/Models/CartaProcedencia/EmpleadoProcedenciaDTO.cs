﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Models.CartaProcedencia
{
    public class EmpleadoProcedenciaDTO
    {
        public int CodigoEmpleado { get; set; }
        public string Nombre { get; set; }
        public string Departamento { get; set; }
        public List<ProcedenciaDTO> CartasProcedencia { get; set; }
    }
}