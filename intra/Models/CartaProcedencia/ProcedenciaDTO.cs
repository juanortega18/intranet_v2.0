﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Models.CartaProcedencia
{
    public class ProcedenciaDTO
    {
        public int Codigo { get; set; }
        public int CodigoEmpleado { get; set; }
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaSalida { get; set; }
        public int Tiempo { get; set; }
        public string DocumentoNombre { get; set; }
        public bool Verificado { get; set; }
        public string NombreEmpleado { get; set; }
        //public VISTA_EMPLEADOS Empleado { get; set; } 
    }
}