﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace intra.Models.CartaProcedencia
{
    [Table("CartaProcedencia")]
    public class CartaProcedencia
    {
        [Key]
        public int CartaProcId { get; set; }
        public int CartaProcCodigoEmpleado { get; set; }
        public DateTime CartaProcFechaIngreso { get; set; }
        public DateTime CartaProcFechaSalida { get; set; }
        public int CartaProcTiempo { get; set; }
        public bool CartaProcEstado { get; set; }
        public byte[] CartaProcDocumento { get; set; }
        public string CartaProcDocumentoNombre { get; set; }
        public bool CartaProcVerificado { get; set; }

    }
}