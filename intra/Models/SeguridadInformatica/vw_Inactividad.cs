namespace intra.Models.SeguridadInformatica
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_Inactividad
    {
        [Key]
        public string CodigoEmpleado { get; set; }
        public string NombreCompleto { get; set; }
        public string Dependencia { get; set; }
        public string Departamento { get; set; }
        public string Cargo { get; set; }
        public string Cedula { get; set; }
        public string EstadoEmpleado { get; set; }
        public bool InactividadTemporal { get; set; }
        public string JustificacionInactividad { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
    }
}
