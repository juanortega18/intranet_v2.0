﻿namespace intra.Models.SeguridadInformatica
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    

    public partial class sp_InactividadInfo
    {
        [Key]
        public string CodigoEmpleado { get; set; }
        public string NombreCompleto { get; set; }
        public string Dependencia { get; set; }
        public string Departamento { get; set; }
        public string Cargo { get; set; }
        public string Cedula { get; set; }
        public string EstadoEmpleado { get; set; }
        public bool InactividadTemporal { get; set; }
        public string JustificacionInactividad { get; set; }
        public DateTime FechaInicio { get; set; }
        public string FechaInicioString { get { return this.FechaInicio.ToString("dd/MM/yyyy"); } }
        public DateTime FechaFin { get; set; }
        public string FechaFinString { get { return this.FechaFin.ToString("dd/MM/yyyy"); } }

    }
}
