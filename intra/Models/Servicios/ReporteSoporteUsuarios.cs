﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace intra.Models.Servicios
{
   public class ReporteSoporteUsuarios
    {
        public DateTime Fecha { get; set; }
        public string Extension { get; set; }
        public string Departamento { get; set; }
        public string Tecnico { get; set; }
        public string Solicitante { get; set; }
        public string SolicitanteId { get; set; }
        public int SolicitudId { get; set; }
        public string TecnicoId { get; set; }
        
        public string DescripcionSolicitud { get; set; }
    }
}
