namespace intra.Models.Servicios
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using intra.Models.TallerPgr;

    [Table("ComplementoReporteSoporte")]
    public partial class ComplementoReporteSoporte
    {
        [Key]
        public int ReporteSoporteId { get; set; }

        public int? SolicitudId { get; set; }

        public int TipoEquipoId { get; set; }

        public int MarcaEquipoId { get; set; }

        public int ModeloEquipoId { get; set; }
        public string SistemaOperativo { get; set; }
        public string Procesador { get; set; }
        public string MemoriaRam { get; set; }
        public string Causa { get; set; }
        public string Observaciones { get; set; }
        [NotMapped]
        public virtual Sol_Registro_Solicitud Sol_Registro_Solicitud { get; set; }
        [NotMapped]
        public virtual MarcaEquipo MarcaEquipo { get; set; }
        [NotMapped]
        public virtual ModeloEquipo ModeloEquipo { get; set; }
        [NotMapped]
        public virtual Equipo Equipo { get; set; }
    }
}
