﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using intra.Models;
using intra.Models.GestionHumana;

namespace intra.Models.Servicios
{
    public class Sol_Registro_Solicitud
    {
        [Key]
        public int SolicitudId { get; set; }
        public int SolicitanteId { get; set; }
        public string DomainUserSolicitante { get; set; }
        public int TecnicoId { get; set; }
        public string DomainUserTecnico { get; set; }
        public int DepartamentoId { get; set; }
        public int Tipo_SolicitudId { get; set; }
        public int? Tipo_Sub_SolicitudId { get; set; }
        public string DescripcionSolicitud { get; set; }
        public string DescripcionSolucion { get; set; }
        public int EstadoId { get; set; }
        public bool Estado { get; set; }
        public int Horas { get; set; }
        public int TipodeAsistencia { get; set; }
        public DateTime FhInicioSolicitud { get; set; }
        public DateTime FhFinalSolicitud { get; set; }
        public DateTime FhModificacion { get; set; }
        public DateTime FhCreacion { get; set; }
        public int? DependenciaRegionalId { get; set; }
        public virtual ICollection<Eventos> Eventos { get; set; }
        //public virtual Sol_Actividades Sol_Actividades { get; set; }
        //public virtual Sol_Sub_Actividades Sol_Sub_Actividades { get; set; }
        [NotMapped]
        public virtual VISTA_EMPLEADOS VISTA_EMPLEADOS { get; set; }
        public virtual Sol_Estado_Solicitud Sol_Estado_Solicitud { get; set; }


    }
}
