namespace intra.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using intra.Models.Nomina;
    using intra.Models.GestionHumana;

    public partial class intra2 : DbContext
    {
        public intra2()
            : base("name=intra2")
        {
        }

        //.

        public virtual DbSet<EmpleadosNoPonche> EmpleadosNoPonche { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EmpleadosNoPonche>()
                .Property(e => e.EmpNoPoncheDescripcion)
                .IsUnicode(false);
        }

        public DbSet<ArchivoNominaIntranet> ArchivoNominaIntranets { get; set; }
    }

    public partial class intra3 : DbContext
    {
        public intra3()
            : base("name=intra3")
        {
        }
        public virtual DbSet<SP_USERS_LDAP> SP_USERS_LDAP { get; set; }

    }
}
