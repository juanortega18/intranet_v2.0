﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Models
{
    public class TecnicoDTO
    {
        public int TecnicoID { get; set; }
        public string Nombre { get; set; }
        public int Codigo { get; set; }
        public DateTime FhCreacion  { get; set; }
        public bool Estado { get; set; }
    }
}