﻿namespace intra
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Web;

    public partial class IncidenciasServicios
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IncidenciasId { get; set; }
        public DateTime IncidenciasFechaCreacion { get; set; }
        [StringLength(80)]
        public string IncidenciasUsuario { get; set; }
        [Column(TypeName = "text")]
        public string IncidenciasSolucion { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public System.Nullable<DateTime> IncidenciasFechaSolucion { get; set; }
        public bool IncidenciasEstatus { get; set; }
        public int SuplidorId { get; set; }
        public int IncidenciasTipoId { get; set; }
        public int LocalidadId { get; set; }
        [Column (TypeName = "text")]
        public string IncidenciasObservaciones { get; set; }
        public string IncidenciasNoReporte { get; set; }
        public string IncidenciasTecnicoNoReporte { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public System.Nullable<DateTime> IncidenciasTiempoCompromiso { get; set; }
        public int Municipality_ID { get; set; }
        public int RegionId { get; set; }
        public int Province_ID { get; set; }
        public int EstadoId { get; set; }
        public int ConectividadId { get; set; }
        public int MedioId { get; set; }

    }
    public partial class IncidenciasTipos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IncidenciasTipoId { get; set; }
        [StringLength(100)]
        public string IncidenciasTipoNombre { get; set; }
        public int SuplidorId { get; set; }

    }
    public partial class IncidenciasAreas
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IncidenciasAreaId { get; set; }
        [StringLength(100)]
        public string IncidenciasAreaNombre { get; set; }
    }

    [Table("VelocidadInventario")]
    public partial class VelocidadInventario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VelocidadId { get; set; }
        [StringLength(100)]
        public string VelocidadDescripcion { get; set; }
    }


    public partial class EstadosIncidencias
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EstadoId { get; set; }
        public string EstadoDescripcion { get; set; }
    }
    public partial class Suplidores
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SuplidorId { get; set; }
        [StringLength(100)]
        public string SuplidorNombre { get; set; }
        [Column(TypeName = "text")]
        public string SuplidorDireccion { get; set; }
        [StringLength(50)]
        public string SuplidorTelefono { get; set; }
        [StringLength(100)]
        public string SuplidorUsuario { get; set; }
        [StringLength(100)]
        public string SuplidorContacto { get; set; }
    }

    public partial class SuplidorTipoes
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int SuplidorId { get; set; }
        public int IncidenciasTipoId { get; set; }
    }

    public partial class VistaIncidencias
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IncidenciasId { get; set; }
        public DateTime IncidenciasFechaCreacion { get; set; } 
        public string IncidenciasUsuario { get; set; }
        public string IncidenciasSolucion { get; set; }
        public DateTime IncidenciasFechaSolucion { get; set; }
        public bool IncidenciasEstatus { get; set; }
        public int SuplidorId { get; set; }
        public int IncidenciasTipoId { get; set; }
        public int LocalidadId { get; set; }
        public string IncidenciasObservaciones { get; set; }
        public string LocalidadNombre { get; set; }
        public string SuplidorNombre { get; set; }
        public string IncidenciasTipoNombre { get; set; }
        public string LocalidadTelefono { get; set; }
    }


}