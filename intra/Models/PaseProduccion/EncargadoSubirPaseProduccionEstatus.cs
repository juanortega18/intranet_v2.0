﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace intra.Models.PaseProduccion
{
    [Table("EncargadoSubirPaseProduccionEstatus")]
   public class EncargadoSubirPaseProduccionEstatus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EncargadoSubirProduccionEstatusId { get; set; }
        public string EncargadoSubirProduccionDescripcion { get; set; }
        
    }
}
