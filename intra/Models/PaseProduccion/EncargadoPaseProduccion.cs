﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace intra.Models.PaseProduccion
{
    [Table("EncargadoPaseProduccion")]
 public  class EncargadoPaseProduccion
    {
        [Key]
        public int EncargadoPaseProduccionId { get; set; }
        public string EncargadoPaseProduccionNombre { get; set; }
        public string EmpleadoId { get; set; }

        public string EncargadoPaseProduccionTipo { get; set; }
        public bool EncargadoPaseProduccionEstatus { get; set; }
        public string EncargadoPaseProduccionCorreo { get; set; }
        public string EncargadoArea { get; set; }
    }
}
