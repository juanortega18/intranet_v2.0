﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace intra.Models.PaseProduccion
{
    [Table("CategoriasAprobacionesProduccion")]
   public class CategoriasAprobacionesProduccion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoriasAprobacionesProduccionId { get; set; }
        public int AprobacionesId { get; set; }
        public int CategoriasProduccionId { get; set; }

        public virtual CategoriasProduccion CategoriasProduccion { get; set; }
        public virtual AprobacioneServicios AprobacioneServicios { get; set; }
    }
}
