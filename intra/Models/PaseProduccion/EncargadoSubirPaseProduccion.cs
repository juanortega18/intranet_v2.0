﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;



namespace intra.Models.PaseProduccion
{
    [Table("EncargadoSubirProduccion")]
   public class EncargadoSubirProduccion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EncargadoSubirProduccionId { get; set; }
        public int EmpleadoId { get; set; }
        public string EncargadoSubirProduccionUsuario { get; set; }
        public string EncargadoSubirProduccionNombre { get; set; }
        public int EncargadoSubirProduccionEstatusId { get; set; }
        public string EncargadoSubirTipo { get; set; }
        public virtual EncargadoSubirPaseProduccionEstatus EncargadoSubirPaseProduccionEstatus { get; set; }
    }
}
