﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace intra.Models.PaseProduccion
{
    [Table("AreasImplicadasPaseProduccion")]
   public class AreasImplicadasPaseProduccion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AreasImplicadasId { get; set; }
        public string AreasImplicadasDescripcion { get; set; }

    }
}
