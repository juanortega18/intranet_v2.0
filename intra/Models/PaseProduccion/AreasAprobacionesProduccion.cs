﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace intra.Models.PaseProduccion
{
    [Table("AreasAprobacionesProduccion")]
   public class AreasAprobacionesProduccion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AreasAprobacionesId { get; set; }
        public int AreasImplicadasId { get; set; }
        public int AprobacionesId { get; set; }
        public virtual AreasImplicadasPaseProduccion AreasImplicadasPaseProduccion { get; set; }
        public virtual AprobacioneServicios AprobacioneServicios { get; set; }


    }
}
