﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace intra.Models.PaseProduccion
{
    [Table("AprobacionesActividad")]
   public class AprobacionesActividad
    {
        public int AprobacionesActividadId  { get; set; }
        public int AprobacionesId { get; set; }
        public string usuario { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int ApEstatusId { get; set; }
        public string EmpleadoId { get; set; }
        public string  EncargadoDTI { get; set; }
        public string Solicitante1 { get; set; }
        public string EncargadoDesarrollo { get; set; }
        public string LiderEquipo { get; set; }
        public string Solicitante { get; set; }
        public DateTime? FechaAprobacionDTI { get; set; }
        public DateTime? FechaAprobacionDesarrollo { get; set; }
        public DateTime? FechaAprobacionSolicitante1 { get; set; }
        public DateTime? FechaAprobacionLider { get; set; }
        public DateTime? FechaAprobacionSolicitante { get; set; }
        public string EncargadoProyecto { get; set; }
        public DateTime? FechaAprobacionProyecto { get; set; }
        public DateTime? FechaAprobacionEncargadoOperaciones { get; set; }
        public DateTime? FechaAprobacionEncargadoSeguridadTI { get; set; }
        public string EncargadoOperaciones { get; set; }
        public string EncargadoSeguridadTI { get; set; }
        //  public virtual EncargadoPaseProduccion EncargadoPaseProduccion { get; set; }
        public virtual AprobacionEstatus AprobacionEstatus { get; set; }




    }
}
