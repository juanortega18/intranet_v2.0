﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using intra.Models.Servicios;


namespace intra.Models.PaseProduccion
{
    [Table("AprobacioneServicios")]
    public class AprobacioneServicios
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int AprobacionesId  { get; set; }
        public string usuario { get; set; }
        public int? SubActividadId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string DetallePaseProduccion { get; set; }
        public int ActividadId { get; set; }
        public int ApEstatusId { get; set; }
        public string Prioridad { get; set; }
        public string Riesgo { get; set; }
        public string Impacto { get; set; }
        public string FaseProyecto { get; set; }
        public int SolicitudId { get; set; }
        public string Solicitante { get; set; }
        public int SolicitanteId { get; set; }
        public int? CodigoEmpLider { get; set; }
        public int? ConteoFinal { get; set; }
        public virtual Sol_Registro_Solicitud Sol_Registro_Solicitud { get; set; }
        public virtual Sol_Sub_Actividades Sol_Sub_Actividades { get; set; }
        public virtual Sol_Actividades Sol_Actividades { get; set; }
        public virtual AprobacionEstatus AprobacionEstatus { get; set; }
        //public virtual AprobacionesActividad Actividad { get; set; }

        public List<AreasAprobacionesProduccion>areasImplicadas { get; set; }
        public List<CategoriasAprobacionesProduccion> categorias { get; set; }

        public List<AprobacionesActividad> AprobacionesActividad { get; set; }

        /// <summary>
        /// Campo no mapeable para saber si la solicitud esta formada
        /// </summary>
        [NotMapped]
        public bool EstaFirmada { get; set; }


    }
}
