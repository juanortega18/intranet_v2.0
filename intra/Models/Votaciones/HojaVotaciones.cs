namespace intra.Models.Votaciones
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class HojaVotaciones : DbContext
    {
        public HojaVotaciones()
            : base("name=HojaVotaciones")
        {
        }

        public virtual DbSet<Hoja> Hoja { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
