namespace intra.Models.Votaciones
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Hoja")]
    public partial class Hoja
    {
        [Key]
        public int NO { get; set; }

        [StringLength(255)]
        public string CODIGO { get; set; }

        [StringLength(255)]
        public string PERSONA { get; set; }

        [StringLength(255)]
        public string CEDULA { get; set; }

        [StringLength(255)]
        public string LOCALIDAD { get; set; }

        [StringLength(255)]
        public string PROCURADURIA { get; set; }

        [StringLength(255)]
        public string CARGO { get; set; }

        [StringLength(255)]
        public string CORREO { get; set; }
    }
}
