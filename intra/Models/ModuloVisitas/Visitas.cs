namespace intra.Models.ModuloVisitas
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Visitas
    {
        [Key]
        public int VisitaId { get; set; }

        [Required]
        [StringLength(13)]
        public string VisitaCedula { get; set; }

        [Required]
        [StringLength(50)]
        public string VisitaNombre { get; set; }

        [Required]
        public string VisitaImagen { get; set; }

        public DateTime VisitaFecha { get; set; }

        public int VisitaDepartamentoId { get; set; }
    }
}
