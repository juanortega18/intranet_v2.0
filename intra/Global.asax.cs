﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.SqlClient;
using System.Web.Security;
using System.Security.Principal;
using System.Security.Cryptography;
using System.Net;
using System.Net.Sockets;
using intra.Code;
using Quartz;
using System.Web.Optimization;
using intra.App_Start;

namespace intra
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            
            //Enabling Bundles
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = true;

            //PonchesMailSender.Start();
        }

        protected void Application_AuthenticateRequest()
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie == null) return;
            
            try
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                IIdentity identity = new GenericIdentity(ticket.Name);
                IPrincipal principal = new GenericPrincipal(identity, ticket.UserData.Split('|'));

                HttpContext.Current.User = principal;
            }
            catch (CryptographicException cex)
            {
                cex.ToString();
                FormsAuthentication.SignOut();
            }
            catch (Exception cex)
            {
                cex.ToString();
                FormsAuthentication.SignOut();
            }
        }

        protected void Application_Error() {

            if (Context.Session == null)
                return;

            var theException = Server.GetLastError();

            Session["handling_errors"] = theException;

            Session["temp_url"] = Request.Url;

            if (Session["empleadoId"] == null) { Response.Redirect("~/Error/NotFound"); return; }

            try
            {
                var db = new dbIntranet();

                var error = db.CodigoError.Where(x => x.MensajeError == theException.Message).FirstOrDefault();

                if (error == null) {

                    var temp_table = new CodigoError();
                    temp_table.MensajeError = theException.Message;
                    //temp_table.MensajeAmigable = Controllers.ErrorController.GENERIC_ERROR;
                    if (theException.InnerException != null)
                        temp_table.MensajeErrorInterno = theException.InnerException.Message;
                    db.CodigoError.Add(temp_table);
                    db.SaveChanges();

                    error = temp_table;
                }

                var temp_table2 = new LogErrores();
                if (Session["usuario"] != null)
                    temp_table2.Usuario = Session["usuario"].ToString();
                temp_table2.Ip = Code.UtilityMethods.GetIp();
                temp_table2.LineaError = System.Text.RegularExpressions.Regex.Match(theException.StackTrace, "[\\S][^\n]+[\\s][\\d]+").Value;
                temp_table2.Fecha = DateTime.Now;
                temp_table2.CodigoErrorId = error.CodigoErrorId;

                db.LogErrores.Add(temp_table2);
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            const int NOTFOUNDCODE = -2147467259;

            if (theException.HResult == NOTFOUNDCODE)
            {
                Response.Redirect("~/Error/NotFound");
            }
            else {

                Response.Redirect("~/Error");

            }
        }

        protected void Application_Errors()
        {
            if (Context.Session == null)
                return;

            var theException = Server.GetLastError();

            if (theException == null)
                return;

            const int NOTFOUNDCODE = -2147467259;
            // The anti-forgery cookie token and form field token do not match.
            const string COOKIES_DONTMATCH = "The anti-forgery cookie token and form field token do not match.";
            // The controller for path '/intra/Views/favicon.png' was not found or does not implement IController.
            const string RESOURCENOTFOUND = "was not found or does not implement IController.";
            // The required anti-forgery cookie "__RequestVerificationToken_L2ludHJh0" is not present.
            const string COOKIES_ERROR = "__RequestVerificationToken_";

            // Only to to hold a value in the next condition
            bool? _dummyBoolean_cookierror = null;

            if (Request.QueryString["AcceptsCookies"] == null && theException.HResult == NOTFOUNDCODE
                && ((bool)(_dummyBoolean_cookierror = theException.Message.Contains(COOKIES_ERROR)) ||
                    !(bool)(_dummyBoolean_cookierror = !theException.Message.Contains(COOKIES_DONTMATCH))))
            {
                if (_dummyBoolean_cookierror == false)
                    Response.Redirect(Request.Url.ToString());
                else // if _dummyBoolean_cookierror == true
                    Response.Redirect("~/error/disabledcookies");
                return;
            }

            Exception theInnerException = theException.InnerException;

            while (theInnerException != null && theInnerException.InnerException != null)
                theInnerException = theInnerException.InnerException;

            var currentUTC = DateTime.Now.ToUniversalTime();

            // Variable to know in the "ErrorController" that the system is processing an error
            Session["handling_errors"] = true;

            try
            {
                #region Handle Exceptions
                // The required anti-forgery cookie "__RequestVerificationToken_L2ludHJh0" is not present.
                //const string COOKIES_ERROR = "__RequestVerificationToken_";
                // The controller for path '/intra/Views/favicon.png' was not found or does not implement IController.
                //const string RESOURCENOTFOUND = "was not found or does not implement IController.";

                // Clear the error from the server
                Server.ClearError();

                if (theException.HResult == NOTFOUNDCODE)
                {
                    /***if (theException.Message.Contains(COOKIES_ERROR))
                        Response.Redirect("~/error/disabledcookies");
                    // The cookie token do not match: "refresh" the page
                    else if (theException.Message == COOKIES_DONTMATCH)
                        Response.Redirect(Request.Url.ToString());
                    // Throw a "404 Not Found" Exception
                    else
                    {
                        Session["temp_url"] = Request.Url;
                        Response.Redirect("~/error/notfound");
                    }//*/

                    // Throw a "404 Not Found" Exception
                    Session["temp_url"] = Request.Url;
                    Response.Redirect("~/error/notfound");
                    return;
                }
                #endregion

                #region Report Exceptions
                // Format of the log's content
                //var logContent = "UTC-Time: {0} | Message: {1} | StackTrace: {2}";

                // Find the coincidence for the given regular expression (the one "line" containing the info about the exception)
                var exceptionLine = System.Text.RegularExpressions.Regex.Match(theException.StackTrace, "[\\S][^\n]+[\\s][\\d]+").Value;
                //Convert.ToInt32("Walala");
                var db = new dbIntranet();

                int errorCode = (from t in db.CodigoError
                                 where t.MensajeError == theException.Message
                                 select t.CodigoErrorId).FirstOrDefault();

                // If this kind of error have not happened already:
                if (errorCode == 0)
                {
                    var temp_table = new CodigoError();
                    temp_table.MensajeError = theException.Message;
                    temp_table.MensajeAmigable = Controllers.ErrorController.GENERIC_ERROR;
                    if (theInnerException != null)
                        temp_table.MensajeErrorInterno = theInnerException.Message;
                    db.CodigoError.Add(temp_table);
                    db.SaveChanges();

                    // Get the last inserted register
                    errorCode = (from t in db.CodigoError
                                 orderby t.CodigoErrorId descending
                                 select t.CodigoErrorId).First();

                    // A problem occurred
                    if (errorCode == 0)
                        throw new ArgumentNullException("No error code found for the message");
                    //{
                    //    Session["temp_date"] = currentUTC.ToString();
                    //    Session["temp_code"] = -1;
                    //    Response.Redirect("~/error");
                    //    return;
                    //}
                }

                var temp_table2 = new LogErrores();
                if (Session["usuario"] != null)
                    temp_table2.Usuario = Session["usuario"].ToString();
                temp_table2.Ip = Code.UtilityMethods.GetIp();
                temp_table2.LineaError = exceptionLine;
                temp_table2.Fecha = currentUTC;
                temp_table2.CodigoErrorId = errorCode;

                db.LogErrores.Add(temp_table2);
                db.SaveChanges();


                //if (exceptionLine == "")
                //    logContent = string.Format(logContent.Substring(0, logContent.LastIndexOf("|") - 1), currentUTC.ToShortTimeString(), theException.Message);
                //else
                //    logContent = string.Format(logContent, currentUTC.ToShortTimeString(), theException.Message, exceptionLine);


                /***using (var writer = new System.IO.StreamWriter(Server.MapPath(string.Format("~/ExceptionLogs/UTC_{0:00}-{1:00}-{2}.log", currentUTC.Day, currentUTC.Month, currentUTC.Year)), true, System.Text.Encoding.UTF8))
                    writer.WriteLine(string.Format(logContent, currentUTC.ToShortTimeString(), theException.Message, exceptionLine));//*/
                #endregion


                #region RESET THE SESSION VARIABLE
                // Clear all the Session variables when "departamento" is set but "empleadoId" isn't
                if (Session["departamento"] != null && Session["empleadoId"] == null)
                    Session.Clear();
                // Reset the variable to know in the "ErrorController" that the system is processing an error
                Session["handling_errors"] = true;
                #endregion

                Session["temp_date"] = currentUTC.ToString();//string.Format("{0:00}-{1:00}-{2} {3:00}:{4:00}", currentUTC.Day, currentUTC.Month, currentUTC.Year, currentUTC.Hour, currentUTC.Minute);
                Session["temp_code"] = errorCode;
                Session["temp_url"] = Request.Url;

                Response.Redirect("~/error");
            }
            catch (Exception ex)
            {
                // Try to report the failure and the exception of the page to a file
                try
                {
                    var pathToReportFolder = Server.MapPath("~/ExceptionLogs/");
                    var pathToReport = pathToReportFolder + "ReportedExceptions.arr";
                    var filteredFailedMessage = System.Text.RegularExpressions.Regex.Match(theException.StackTrace, "[\\S][^\n]+[\\s][\\d]+");
                    var filteredMessage = System.Text.RegularExpressions.Regex.Match(ex.StackTrace, "[\\S][^\n]+[\\s][\\d]+");

                    /*
                        0 -> currentUTC
                        1 -> theException.Message
                        2 -> filteredFailedMessage
                        3 -> (theInnerException != null ? theInnerException.Message : null)
                        4 -> ex.Message
                        5 -> filteredMessage
                        6 -> Code.UtilityMethods.GetIp()

                        // Ejemplo para Junior:
                        throw new Exception("El texto de error");
                     */
                    var reportFormat = "UTC-Time: {0} | Fail-Exception-Message: {1} | Fail-Exception-Line: {2} | Fail-Inner-Exception: {3} | Actual-Exception-Message: {4} | Actual-Exception-Line: {5} | Requester-Ip: {6}";

                    reportFormat = string.Format
                    (
                        reportFormat,
                        // {0}
                        currentUTC,
                        // {1}
                        theException.Message,
                        // {2}
                        filteredFailedMessage,
                        // {3}
                        (theInnerException != null ? theInnerException.Message : null),
                        // {4}
                        ex.Message,
                        // {5}
                        filteredMessage,
                        // {6}
                        Code.UtilityMethods.GetIp()
                    );

                    if (!System.IO.Directory.Exists(pathToReportFolder))
                        System.IO.Directory.CreateDirectory(pathToReportFolder);

                    using (var writer = new System.IO.StreamWriter(pathToReport, true))
                        writer.WriteLine(reportFormat);
                }
                catch
                {
                    // Try to report via e-mail
                    try
                    {

                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        Session["temp_date"] = currentUTC.ToString();
                        Session["temp_code"] = -1;
                        Response.Redirect("~/error");
                    }
                    catch { }
                }
            }

            #region Handle Exceptions (OLD)
            /*
            //string urlToRedirectTo = "error";
            // The required anti-forgery cookie "__RequestVerificationToken_L2ludHJh0" is not present.
            const string COOKIES_ERROR = "__RequestVerificationToken_";//L2ludHJh0"; // Just in case this last code changes
            // The controller for path '/intra/JsLibraries/file-upload/jquery.iframe-transport.js' was not found or does not implement IController.
            const string JQUERY_ERROR = "/intra/JsLibraries/file-upload/jquery.iframe-transport.js";


            // Specific "Not Found" exception
            if (theException.HResult == -2147467259)
            {
                // "Disabled cookies" exception
                if(theException.Message.Contains(COOKIES_ERROR))
                {
                    Server.ClearError();
                    Response.Redirect("~/error/disabledcookies");
                    return;
                }
                // If the error message doesn't contain any of the following, then it MUST be a "404 Not Found"
                if(!theException.Message.Contains(JQUERY_ERROR))
                {
                    Session["temp_url"] = Request.RawUrl;
                    Server.ClearError();
                    Response.Redirect("~/error/notfound");
                }

                return;
            }

            var currentUTC = DateTime.Now.ToUniversalTime();

            // Format of the log's content
            var logContent = "UTC-Time: {0} | Message: {1} | StackTrace: {2}";

            // Find the coincidence for the given regular expression (the one "line" containing the info about the exception)
            var exceptionLine = System.Text.RegularExpressions.Regex.Match(theException.StackTrace, "[\\S][^\n]+[\\s][\\d]+").Value;

            if (exceptionLine == "")
                logContent = string.Format(logContent.Substring(0, logContent.LastIndexOf("|") - 1), currentUTC.ToShortTimeString(), theException.Message);
            else
                logContent = string.Format(logContent, currentUTC.ToShortTimeString(), theException.Message, exceptionLine);


            using (var writer = new System.IO.StreamWriter(Server.MapPath(string.Format("~/ExceptionLogs/UTC_{0:00}-{1:00}-{2}.log", currentUTC.Day, currentUTC.Month, currentUTC.Year)), true, System.Text.Encoding.UTF8))
                writer.WriteLine(string.Format(logContent, currentUTC.ToShortTimeString(), theException.Message, exceptionLine));
            //*/
            #endregion

            #region Report Exceptions (OLD)
            /***
            // Handle the "Reported Exceptions File" //

            var pathToReport = Server.MapPath("~/ExceptionLogs/ReportedExceptions.arr");
            bool exists = false;
            // Remove "specific names" from the error message (to leave it generic)
            var filteredMessage = System.Text.RegularExpressions.Regex.Replace(theException.Message, "\"[\\w\\W]+\"", "\\\"{0}\\\"");
            var newArrayPos = -1;

            if (System.IO.File.Exists(pathToReport))
            {
                string temp_str = null;

                using (var lector = new System.IO.StreamReader(pathToReport))
                    exists = (temp_str = lector.ReadToEnd()).Contains(filteredMessage);

                if (!exists)
                    // Divide over 2 because the order of "ReportedExceptions.arr" is two lines per entry
                    newArrayPos = temp_str.Count((obj) => obj == '\n') / 2;
                else
                {

                    // Half of the '\n' quantity minus 1 (because of the "index" status)

                    // Substring to the position where the <filteredMessage> "appeared"
                    var temp = temp_str.Substring(0, temp_str.IndexOf(filteredMessage) + filteredMessage.Length);
                    // Count the "newlines" to the substring
                    var temp2 = temp.Count((obj) => obj == '\n');
                    // If odd number: make it even adding 1
                    temp2 = temp2 % 2 == 1 ? temp2 + 1 : temp2;
                    // Divide over 2 because the order of "ReportedExceptions.arr" is two lines per entry - 1 to make it an "index"
                    var temp3 = temp2 / 2 - 1;
                    newArrayPos = temp3;
                }

                temp_str = null;
            }

            if (!exists)
                using (var writer = new System.IO.StreamWriter(pathToReport, true))
                    writer.WriteLine(string.Format("\t// {0}\n\t\"{1}\",", newArrayPos, filteredMessage));//*/
            #endregion
        }
    }
}
