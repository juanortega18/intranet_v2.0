﻿using System.Web;
using System.Web.Optimization;

namespace intra.App_Start
{

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Datatables Styles
            bundles.Add(new StyleBundle("~/datatables/css").Include(
            "~/Content/datatables/css/responsive.dataTables.css",
            "~/Content/datatables/css/jquery.dataTables.min.css"));

            //Datadables scripts
            bundles.Add(new ScriptBundle("~/datatables/js").Include(
                      "~/Content/datatables/js/jquery.dataTables.min.js",
                      "~/Content/datatables/js/dataTables.responsive.min.js",
                      "~/Content/datatables/js/traslator.js"));      //Traslation for datatables text

        }
    }
}