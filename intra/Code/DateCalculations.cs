﻿using intra.Models;
using intra.Models.CartaProcedencia;
using intra.Models.GestionHumana;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using intra.Models.GestionHumana.Permisos;


namespace intra.Code
{
    public class DateCalculations : Page
    {
        DFMPModel dbIntranet = new DFMPModel();
        dbIntranet db = new dbIntranet();

        private DateTime _fechaIngreso;
        private double _aniosOtrasInstituciones;
        private List<DateTime[]> _fechasVacaciones;
        private DateTime _aniversarioFechaIngreso;
        private string empleadoId = string.Empty;

        public DateCalculations(DateTime fechaIngreso, List<DateTime[]> fechasVacaciones)
        {
            _fechaIngreso = fechaIngreso;

            _aniosOtrasInstituciones = ObtenerAniosOtrasInstituciones();

            if((int) _aniosOtrasInstituciones > 0)
            {
                _fechaIngreso = RecalcularFechaIngreso();
            }

            _fechasVacaciones = fechasVacaciones;
            _aniversarioFechaIngreso = new DateTime(DateTime.Now.Year, _fechaIngreso.Month, _fechaIngreso.Day).Date;
        }

        public DateCalculations()
        {

        }

        public DateCalculations(string empleadoId = "")
        {
            this.empleadoId = empleadoId;
        }

        #region RECALCULACION FECHA DE INGRESO

        private DateTime RecalcularFechaIngreso()
        {
            DateTime nuevaFechaIngreso = _fechaIngreso.AddDays(-(_aniosOtrasInstituciones * 365));

            return nuevaFechaIngreso;
        }

        #endregion

        #region ANIOS OTRAS INSTITUCIONES DEL ESTADO

        private double ObtenerAniosOtrasInstituciones()
        {
            int codigoEmpleado = Convert.ToInt32(Session["empleadoId"].ToString());
            double anios = 0;

            List<CartaProcedencia> cartasProcedencia = db.CartaProcedencia.Where(cp => cp.CartaProcCodigoEmpleado == codigoEmpleado).ToList();

            foreach (CartaProcedencia cartaProcedencia in cartasProcedencia)
            {
                anios += ObtenerAniosIntervalo(cartaProcedencia.CartaProcFechaIngreso, cartaProcedencia.CartaProcFechaSalida);
            }

            return anios;
        }

        #endregion

        #region ANIOS EN INTERVALO

        private double ObtenerAniosIntervalo(DateTime fechaInicio, DateTime fechaFin)
        {
            double dias = (fechaFin - fechaInicio).TotalDays;
            double anios = dias / 365;

            return anios;
        }

        #endregion

        #region DIAS ACUMULADOS

        public List<Intervalo> ObtenerDiasAcumulados()
        {
            List<Intervalo> intervalos = _obtenerIntervalos();

            return _evaluarIntervalos(intervalos);
        }

        #endregion

        #region EVALUACION DE INTERVALOS

        private List<Intervalo> _evaluarIntervalos(List<Intervalo> intervalos)
        {
            foreach (Intervalo intervalo in intervalos)
            {
                int posicion = intervalos.IndexOf(intervalo);

                if (posicion == 0)
                {
                    Intervalo intervaloActual = intervalo;
                    intervaloActual.DiasAcumulados = intervaloActual.DiasCorrespondientes - intervaloActual.DiasTomados;
                    intervaloActual.DiasAnio = intervaloActual.LimiteInferior.Year;
                }
                else if (posicion >= 1)
                {
                    Intervalo intervaloActual = intervalo;
                    Intervalo intervaloAnterior = intervalos[posicion - 1];

                    intervaloActual.DiasAnio = intervaloActual.LimiteInferior.Year;

                    if (intervaloActual.DiasTomados == 0)
                    {
                        intervaloActual.DiasAcumulados = intervaloActual.DiasCorrespondientes;
                    }
                    else
                    {
                        int diferencia = intervaloAnterior.DiasAcumulados - intervaloActual.DiasTomados;

                        if (diferencia >= 0)
                        {
                            intervaloActual.DiasAcumulados = intervaloActual.DiasCorrespondientes;
                        }
                        else if (diferencia < 0)
                        {
                            intervaloActual.DiasAcumulados = intervaloActual.DiasCorrespondientes - Math.Abs(intervaloAnterior.DiasAcumulados - intervaloActual.DiasTomados);

                            if (intervaloActual.DiasAcumulados < 0)
                            {
                                intervaloActual.DiasAcumulados = 0;
                            }
                        }
                    }
                }
            }

            List<Intervalo> intervalosObservacion = new List<Intervalo>();

            if (intervalos.Count() < 2)
            {
                intervalosObservacion.Add(intervalos.LastOrDefault());
            }
            else
            {
                intervalosObservacion.Add(intervalos[intervalos.Count() - 1]);
                intervalosObservacion.Add(intervalos[intervalos.Count() - 2]);
            }

            return intervalosObservacion;
        }

        #endregion

        #region DIAS TOMADOS

        private int ObtenerDiasTomados(DateTime fechaInicio, DateTime fechaFin)
        {
            DateTime fechaVariante = fechaInicio;
            int diasTomados = 0;

            foreach (DateTime[] vacacion in _fechasVacaciones)
            {
                if (vacacion[0] >= fechaInicio && vacacion[0] <= fechaFin)
                {
                    diasTomados += ObtenerDiasLaborables(vacacion[0], vacacion[1]);
                }
            }

            return diasTomados;
        }

        #endregion

        #region DIAS LABORABLES

        public int ObtenerDiasLaborables(DateTime fechaInicio, DateTime fechaFin)
        {
            int diasLaborables = 0;
            DateTime fechaVariante = fechaInicio;

            while (fechaVariante <= fechaFin)
            {
                if (!EsDiaFeriado(fechaVariante) && fechaVariante.DayOfWeek != DayOfWeek.Saturday && fechaVariante.DayOfWeek != DayOfWeek.Sunday)
                {
                    diasLaborables++;
                }

                fechaVariante = fechaVariante.AddDays(1);
            }

            return diasLaborables;
        }

        #endregion

        #region CALCULO DE INTERVALOS

        private List<Intervalo> _obtenerIntervalos()
        {
            List<Intervalo> intervalos = new List<Intervalo>();
            DateTime limiteInferior = _fechaIngreso.AddYears(1);
            DateTime limiteSuperior = limiteInferior.AddYears(1);
            DateTime final = DateTime.Now.Date;

            while (limiteInferior < final)
            {
                Intervalo intervalo = new Intervalo();
                intervalo.LimiteInferior = limiteInferior;
                intervalo.LimiteSuperior = limiteSuperior > DateTime.Now.Date ? DateTime.Now.Date : limiteSuperior;
                intervalo.DiasCorrespondientes = ObtenerDiasCorrespondientes(ObtenerTiempoLaborando(limiteInferior)[0]);
                intervalo.DiasTomados = ObtenerDiasTomados(intervalo.LimiteInferior, intervalo.LimiteSuperior);

                intervalos.Add(intervalo);

                limiteInferior = limiteInferior.AddYears(1);
                limiteSuperior = limiteInferior.AddYears(1);
            }

            return intervalos;
        }

        #endregion

        #region DIAS CORRESPONDIENTES AÑOS LABORADOS

        public int ObtenerDiasCorrespondientes(int aniosLaborados)
        {
            int diasCorrespondientes;

            if (aniosLaborados >= 15)
            {
                diasCorrespondientes = 30;
            }
            else if (aniosLaborados >= 10)
            {
                diasCorrespondientes = 25;
            }
            else if (aniosLaborados >= 5)
            {
                diasCorrespondientes = 20;
            }
            else if (aniosLaborados > 0)
            {
                diasCorrespondientes = 15;
            }
            else
            {
                diasCorrespondientes = 0;
            }

            return diasCorrespondientes;
        }

        #endregion

        #region FECHA FIN VACACION

        public DateTime ObtenerFechaFin(DateTime fechaInicioVacacion, int dias)
        {
            DateTime fechaFinVacacion = fechaInicioVacacion;

            while (ObtenerDiasLaborables(fechaInicioVacacion, fechaFinVacacion) < dias)
            {
                fechaFinVacacion = fechaFinVacacion.AddDays(1);
            }

            return fechaFinVacacion;
        }

        #endregion

        #region MES EN LETRAS

        public string ObtenerMesEnLetras(int numero, bool enMayusculas = false)
        {
            string[] MESES = new string[] { "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };
            string mesEnLetras = MESES[numero - 1];

            return enMayusculas ? mesEnLetras.ToUpper() : mesEnLetras;
        }

        #endregion

        #region NOMECLATURAS 

        public struct TiempoNomeclaturas
        {
            public static string Dia = "Día";
            public static string Dias = "Días";
            public static string Mes = "Mes";
            public static string Meses = "Meses";
            public static string Anio = "Año";
            public static string Anios = "Años";
        }

        #endregion

        #region OBSERVACION PERMISO

        public string ObtenerObservacionPermiso(int permisoId)
        {
            Permisos permiso = db.Permisos.Where(p => p.PermisoId == permisoId).FirstOrDefault();

            int dias = 0;
            int horas = 0;

            dias =  (int)(permiso.PermisoFechaFin.Value - permiso.PermisoFechaInicio.Value).TotalDays;
            horas =  (int)(permiso.PermisoFechaFin.Value - permiso.PermisoFechaInicio.Value).TotalHours;

            DateTime fechaInicio = permiso.PermisoFechaInicio.Value;
            DateTime fechaFin = permiso.PermisoFechaFin.Value;

            UtilidadNumeroALetra utilidadNumeroALetra = new UtilidadNumeroALetra();

            string permisoNombre = db.TipoLicencia.Where(t => t.TipoLicenciaId == permiso.TipoLicenciaId).FirstOrDefault().TipoDescripcion;

            string diasEnLetras = utilidadNumeroALetra.Convertir(dias.ToString(), true);
            string horasEnLetras = utilidadNumeroALetra.Convertir(horas.ToString(), true);

            string tiempo = dias > 0 ? $"{diasEnLetras} ({(dias < 10 ? "0" : "")}{dias}) " +
                                $"{(dias > 1 ? "DIAS" : "DIA")}" : 
                            horas > 0 ? $"{horasEnLetras} ({(horas < 10 ? "0" : "")}{horas}) " +
                                $"{(horas > 1 ? "HORAS" : "HORA" )}" : "";

            string comentarioEstudio = "";

            if(permiso.TipoLicenciaId == 4)
            {
                DiaEstudio diaEstudio = db.DiaEstudio.Where(d => d.DiaEstudioPermisoId == permiso.PermisoId).FirstOrDefault();

                comentarioEstudio = $"LOS DIAS {diaEstudio.DiaEstudioNombre.ToUpper()} DE {diaEstudio.DiaEstudioHoraInicio} HASTA {diaEstudio.DiaEstudioHoraFin}";
            }

            string intervaloFecha = dias > 0 ? $"DEL { fechaInicio.Day }  DE { ObtenerMesEnLetras(fechaInicio.Month, true) } DEL AÑO { fechaInicio.Year } HASTA EL { fechaFin.Day } DE { ObtenerMesEnLetras(fechaFin.Month, true) } DEL AÑO { fechaFin.Year }" : $"EN FECHA { fechaInicio.Day } DE { ObtenerMesEnLetras(fechaInicio.Month, true) } DEL AÑO { fechaInicio.Year }";

            string observacion = $"APROBACION DE SOLICITUD DE PERMISO {permisoNombre} DE {tiempo} {intervaloFecha} {comentarioEstudio}.";

            return observacion;
        }

        #endregion

        #region OBSERVACION VACACION

        public string ObtenerObservacionVacacion(Vacacion vacacion)
        {
            string observacion = "";

            Intervalo primerIntervalo = null;
            Intervalo segundoIntervalo = null;
            int dias = vacacion.Dias;
            string diasLetras = new UtilidadNumeroALetra().Convertir(dias.ToString(), true);
            DateTime fechaSalida = vacacion.FechaInicio;
            DateTime fechaFin = new DateCalculations().ObtenerFechaFin(fechaSalida, dias);

            if (vacacion.IntervalosObservacion.Count() < 2)
            {
                primerIntervalo = vacacion.IntervalosObservacion[0];
                observacion = $"SOLICITUD DE { diasLetras } ({ dias }) DIAS DE VACACIONES CORRESPONDIENTES A ({ dias }) DIAS DEL AÑO { primerIntervalo.LimiteInferior.Year }, DEL { fechaSalida.Day }  DE { ObtenerMesEnLetras(fechaSalida.Month, true) } DEL AÑO { fechaSalida.Year } HASTA EL { fechaFin.Day } DE { ObtenerMesEnLetras(fechaFin.Month, true) } DEL AÑO { fechaSalida.Year }.";
            }
            else {
                primerIntervalo = vacacion.IntervalosObservacion[0];
                segundoIntervalo = vacacion.IntervalosObservacion[1];

                int diferencia = segundoIntervalo.DiasAcumulados - dias;

                if (diferencia >= 0)
                {
                    observacion = $"SOLICITUD DE { diasLetras } ({ dias }) DIAS DE VACACIONES CORRESPONDIENTES A ({ dias }) DIAS DEL AÑO { segundoIntervalo.LimiteInferior.Year }, DEL { fechaSalida.Day }  DE { ObtenerMesEnLetras(fechaSalida.Month, true) } DEL AÑO { fechaSalida.Year } HASTA EL { fechaFin.Day } DE { ObtenerMesEnLetras(fechaFin.Month, true) } DEL AÑO { fechaSalida.Year }.";
                }
                else if (diferencia < 0)
                {
                    string notaSegundoIntervalo = segundoIntervalo.DiasAcumulados > 0 ? $"({segundoIntervalo.DiasAcumulados}) DIAS DEL AÑO {segundoIntervalo.LimiteInferior.Year} Y " : "";

                    observacion = $"SOLICITUD DE { diasLetras } ({ dias }) DIAS DE VACACIONES CORRESPONDIENTES A {notaSegundoIntervalo}({ Math.Abs(diferencia) }) DIAS DEL AÑO { primerIntervalo.LimiteInferior.Year }, DEL { fechaSalida.Day }  DE { ObtenerMesEnLetras(fechaSalida.Month, true) } DEL AÑO { fechaSalida.Year } HASTA EL { fechaFin.Day } DE { ObtenerMesEnLetras(fechaFin.Month, true) } DEL AÑO { fechaSalida.Year }.";
                }
            }

            return observacion;
        }

        #endregion

        #region TIEMPO LABORANDO NUMERICO

        public int[] ObtenerTiempoLaborando(DateTime fechaActual)
        {
            int diasTotales = (int)(fechaActual.Date - _fechaIngreso.Date).TotalDays;
            int anios = diasTotales / 365;
            int meses = (int)(12 * (((double)diasTotales / 365) - anios));

            return new int[] { anios, meses };
        }

        #endregion

        #region TIEMPO LABORANDO EN TEXTO

        public string ObtenerTextoTiempoLaborando()
        {
            float diasTiempoLaborando = (float)Math.Floor((DateTime.Now.Date - _fechaIngreso.Date).TotalDays);
            int anios = (int)(diasTiempoLaborando / 365);
            int meses = (int)Math.Floor(12 * ((diasTiempoLaborando / 365) - anios));

            string tiempoLaborando = "";
            tiempoLaborando += anios > 0 ? (anios > 1 ? $"{anios} {TiempoNomeclaturas.Anios} " : $"{anios} {TiempoNomeclaturas.Anio} ") : "";
            tiempoLaborando += meses > 0 ? (meses > 1 ? $"{meses} {TiempoNomeclaturas.Meses} " : $"{meses} {TiempoNomeclaturas.Mes} ") : "";

            return tiempoLaborando;
        }

        #endregion

        #region VERIFICAR SI ES DIA FERIADO

        public bool EsDiaFeriado(DateTime fechaDia)
        {
            var esDiaFeriado = dbIntranet.DiasFeriados.Any(x => x.DiasFeriadosFecha == fechaDia.Date);

            return esDiaFeriado;
        }

        #endregion

        #region VALIDAR SOLICITUD DE VACACION

        public int ValidarVacacionSolicitud(DateTime fechaSalida, int dias)
        {
            // VALORES DEL ESTATUS
            // 0 : fechaSalida y días vacíos
            // 1 : días vacíos
            // 2 : fechaSalida vacía
            // 3 : fecha salida atrazada
            // 4 : fecha salida inválida
            // 5 : todo correcto

            int estatus;

            if (fechaSalida == new DateTime(0001, 1, 1) && dias == 0)
            {
                estatus = 0;
            }
            else if (dias == 0)
            {
                estatus = 1;
            }
            else if (fechaSalida == new DateTime(0001, 1, 1))
            {
                estatus = 2;
            }
            else if (fechaSalida.Date <= DateTime.Now.Date)
            {
                estatus = 3;
            }
            else if (EsDiaFeriado(fechaSalida) || fechaSalida.DayOfWeek == DayOfWeek.Saturday || fechaSalida.DayOfWeek == DayOfWeek.Sunday)
            {
                estatus = 4;
            }
            else
            {
                estatus = 5;
            }

            return estatus;
        }

        #endregion

        #region VALIDAR SOLICITUD PERMISO

        public List<string> ValidadPermisoSolicitud(int tipoPermiso, DateTime fechaInicio, DateTime fechaFin, int diaEstudioDiaId, string diaEstudioHoraInicio, string diaEstudioHoraFin, string observacion)
        {
            // TIPOS DE ERRORES
            // 0 : tipo permiso no seleccionado
            // 1 : fecha inicio vacia
            // 2 : fecha fin vacia
            // 3 : fecha inicio anteriot a fecha actual
            // 4 : fecha inicio igual o mayor que fecha fin
            // 5 : fecha inicio es un dia feriado, Sábado o Domingo
            // 6 : fecha fin igual o menor que fecha inicio
            // 7 : fecha fin igual o menor que fecha actual
            // 8 : observación vacía

            string[] textosErrores = new string[] {
                "Seleccione un tipo de permiso", /*0*/
                "Seleccione una fecha de inicio", /*1*/
                "Seleccione una fecha de fin", /*2*/
                "La fecha de inicio no puede ser anterior a la fecha actual", /*3*/
                "La fecha de inicio no puede ser igual o mayor que la fecha de fin", /*4*/
                "La fecha de inicio no puede ser un día feriado, Sábado o Domingo", /*5*/
                "La fecha de fin no puede ser igual o menor a la fecha de inicio", /*6*/
                "La fecha de fin no puede ser igual o menor a la fecha actual", /*7*/ 
                "Escriba una observación", /*8*/
                "Ya exite un permiso para este horario", /*9*/
                "Solo se puede perdir permisos en horarios laborables", /*10*/
                "Selecciones un día", /*11*/
                "Seleccione una hora de inicio", /*12*/
                "Seleccione una hora de fin", /*13*/
            };

            List<string> errores = new List<string>() { };

            if (tipoPermiso == 4)
            {
                if (tipoPermiso < 1 || tipoPermiso > 26) errores.Add(textosErrores[0]);

                if (fechaInicio.Date == new DateTime(0001, 1, 1).Date) errores.Add(textosErrores[1]);

                if (fechaFin.Date == new DateTime(0001, 1, 1).Date) errores.Add(textosErrores[2]);

                if (fechaInicio < DateTime.Now && fechaInicio.Date != new DateTime(0001, 1, 1).Date) errores.Add(textosErrores[3]);

                if (fechaInicio >= fechaFin && fechaFin.Date != new DateTime(0001, 1, 1).Date) errores.Add(textosErrores[4]);

                if (EsDiaFeriado(fechaInicio) || fechaInicio.DayOfWeek == DayOfWeek.Saturday || fechaInicio.DayOfWeek == DayOfWeek.Sunday) errores.Add(textosErrores[5]);

                if (fechaFin <= fechaInicio && fechaFin.Date != new DateTime(0001, 1, 1).Date) errores.Add(textosErrores[6]);

                if (fechaFin <= DateTime.Now && fechaFin.Date != new DateTime(0001, 1, 1).Date) errores.Add(textosErrores[7]);

                if (String.IsNullOrEmpty(observacion)) errores.Add(textosErrores[8]);


                if (diaEstudioDiaId < 1) errores.Add(textosErrores[11]);

                if (String.IsNullOrEmpty(diaEstudioHoraInicio)) errores.Add(textosErrores[12]);

                if (String.IsNullOrEmpty(diaEstudioHoraFin)) errores.Add(textosErrores[13]);

                return errores;
            }

            if (tipoPermiso < 1 || tipoPermiso > 26) errores.Add(textosErrores[0]);

            if (fechaInicio.Date == new DateTime(0001, 1, 1).Date) errores.Add(textosErrores[1]);

            if (fechaFin.Date == new DateTime(0001, 1, 1).Date) errores.Add(textosErrores[2]);

            if (fechaInicio < DateTime.Now && fechaInicio.Date != new DateTime(0001, 1, 1).Date) errores.Add(textosErrores[3]);

            if (fechaInicio >= fechaFin && fechaFin.Date != new DateTime(0001, 1, 1).Date) errores.Add(textosErrores[4]);

            if (EsDiaFeriado(fechaInicio) || fechaInicio.DayOfWeek == DayOfWeek.Saturday || fechaInicio.DayOfWeek == DayOfWeek.Sunday) errores.Add(textosErrores[5]);

            if (fechaFin <= fechaInicio && fechaFin.Date != new DateTime(0001, 1, 1).Date) errores.Add(textosErrores[6]);

            if (fechaFin <= DateTime.Now && fechaFin.Date != new DateTime(0001, 1, 1).Date) errores.Add(textosErrores[7]);

            if (String.IsNullOrEmpty(observacion)) errores.Add(textosErrores[8]);

            if (CompararFechas(fechaInicio, fechaFin)) errores.Add(textosErrores[9]);

            if (fechaInicio.TimeOfDay < new DateTime(2018, 1, 1, 8, 0, 0).TimeOfDay || fechaInicio.TimeOfDay > new DateTime(2018, 1, 1, 16, 30, 0).TimeOfDay ||
             fechaFin.TimeOfDay < new DateTime(2018, 1, 1, 8, 0, 0).TimeOfDay || fechaFin.TimeOfDay > new DateTime(2018, 1, 1, 16, 30, 0).TimeOfDay) errores.Add(textosErrores[10]);

            return errores;
        }
        #endregion

        #region VALIDAR VACACION PENDIENTE Y VACACION EN PROCESO

        public int ValidarVacacionPendienteVacacionEnProceso(int estado, DateTime vacacionInicio, DateTime vacacionFin)
        {
            // VALORES DEL ESTATUS
            // 0 : solicitud pendiente de aprobación
            // 1 : vacacion aprovada
            // 2 : no solicitudes pendientes ni vacaciones en proceso

            int estatus;

            if (estado == 1)
            {
                estatus = 0;
            }
            else if (vacacionFin.Date >= DateTime.Now.Date && estado == 3)
            {
                estatus = 1;
            }
            else
            {
                estatus = 2;
            }

            return estatus;
        }

        #endregion

        #region COMPARAR CON FECHAS DE REGISTROS ANTERIORES

        public bool CompararFechas(DateTime fechaInicio, DateTime fechaFin)
        {
            var consultar_db = db.Permisos.Where(x => (x.EmpleadoId == empleadoId) && (x.PermisoEstado != 2) && (x.TipoLicencia.TipoLicenciaId != 4) &&
                ((fechaInicio >= x.PermisoFechaInicio && fechaInicio <= x.PermisoFechaFin) ||
                (fechaFin >= x.PermisoFechaInicio && fechaFin <= x.PermisoFechaFin))
            ).ToList();

            if (consultar_db.Count > 0)
                return true;
            
            return false;
        }

        #endregion
    }
}