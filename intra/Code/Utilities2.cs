﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using intra.Models;
using intra;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Quartz;
using System.Security;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Globalization;
using PdfSharp;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using intra.Models.GestionHumana;
using System.Threading;
using System.Net;
using System.Threading.Tasks;
using Excel;

namespace intra.Code
{
    
    public class Utilities2 : IJob
    {
        public dbIntranet db = new dbIntranet();
        private intra2 db4 = new intra2();
        private RHLogsEntities2 db2 = new RHLogsEntities2();
        private static SqlConnection link = new SqlConnection(ConfigurationManager.ConnectionStrings["intra"].ConnectionString);
        private List<Hierachy> hierarchies = new List<Hierachy>();
        private List<sp_ultimos_Ponches_Result> tardiness = new List<sp_ultimos_Ponches_Result>();
        private DFMPModel db6 = new DFMPModel();

        public  Utilities2()
        {
            db = new dbIntranet();
            db4 = new intra2();
            db2 = new RHLogsEntities2();
            hierarchies = new List<Hierachy>();
            db6 = new DFMPModel();
        }

        public static string GetUserName(string document)
        {
            string result = "";

            sp_users_ldapID db_result = new sp_users_ldapID();

            using (PGRINTRANETEntities db5 = new PGRINTRANETEntities())
            {
                db_result = db5.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", document)).FirstOrDefault();
            }

            if (db_result != null)
            {
                if (!string.IsNullOrEmpty(db_result.samAccountName))
                {
                    result = db_result.samAccountName;
                }
                else
                {
                    result = "SINREGISTRODOMAIN";
                }
            }

            return result;
        }


        public static string ObtenerPersonaSic(string cedula, int tipoIdentificacion)
        {
            string Persona = "";
       

            var myThread = new Thread(() =>
            {
                // Por si falla la conexión con el Web Service
                try
                {
                    using (var request = new WebClient())
                    {
                        var result = request.DownloadString(new Uri("http://api.uavvg.pgr.gob.do/wsic/api/personas?tipodocumento="+tipoIdentificacion+"&numero=" + cedula.Replace("-", "") + "&codigo=E1196C2AE8FD1C56AD43AA4A30769FC4EB58C421&sysname=Intranet"));

                        result = System.Text.Encoding.UTF8.GetString(System.Text.Encoding.Default.GetBytes(result));

                        var obj = Newtonsoft.Json.JsonConvert.DeserializeObject(result, typeof(Newtonsoft.Json.Linq.JObject)) as Newtonsoft.Json.Linq.JObject;

                        Persona = obj.ToString();

                        //string primerNombre = obj.Value<string>("Nombres").Split(' ')[0].ToLower();
                        //string primerApellido = obj.Value<string>("Apellido1").Replace(" ", "").ToLower();

                        //correo = $"{primerNombre}.{primerApellido}";
                    }
                }
                catch { }
            });

            myThread.Start();

            myThread.Join(6000);

            return Persona;
        }

        public static  string obtenerCorreoUsuario(string codigoEmpleado)
        {
            using(dbIntranet db = new dbIntranet()) {
            //codigoEmpleado = HttpContext.Current.Session["empleadoId"].ToString();            
            var cedulaEmpleado = db.VISTA_EMPLEADOS.Where(x => x.cedula == codigoEmpleado).FirstOrDefault();
            var user =  db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", codigoEmpleado)).FirstOrDefault();

                string correo;
              
                if (user == null)
                {
                    correo = "NO TIENE";
                    return correo;
                }
              
                if (cedulaEmpleado ==  null)
                {
                    //user.samAccountName = "NO TIENE";
                    correo = "NO TIENE";
                    return correo;
                }
                if (cedulaEmpleado != null && user != null)
                {
                    correo = $"{user.samAccountName}";
                    
                }

                if(user != null)
                {
                    correo = $"{user.samAccountName}";
                    return correo;
                }

                return correo = $"{user.samAccountName}";
            }
        }

        public static SP_USERS_LDAP obtenerSp_UserPorUsuario(string usuario)
        {
            using (dbIntranet db = new dbIntranet())
            {
                var user = db.Database.SqlQuery<SP_USERS_LDAP>(string.Format("exec sp_users_ldap '{0}'", usuario)).FirstOrDefault();

                return user;
            }
        }

        public static sp_users_ldapID obtenerSp_UserPorCodigo(string codigo)
        {
            using (dbIntranet db = new dbIntranet())
            {
                var empleado = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == codigo.Trim()).FirstOrDefault();

                if (empleado == null) return null; 

                var user = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", empleado.cedula.Trim())).FirstOrDefault();

                return user;
            }
        }

        public static sp_users_ldapID obtenerSp_UserPorCedula(string cedula)
        {
            //Cedula con guiones
            using (dbIntranet db = new dbIntranet())
            {
                var empleado = db.VISTA_EMPLEADOS.Where(x => x.cedula == cedula.Trim()).FirstOrDefault();

                if (empleado == null) return null;

                var user = db.Database.SqlQuery<sp_users_ldapID>(string.Format("exec sp_users_ldapID '{0}'", cedula.Trim())).FirstOrDefault();

                return user;
            }
        }

        public static int CalcularEdad(DateTime fechadenacimiento)
        {
            int año = DateTime.Now.Year - fechadenacimiento.Year;

            if ((fechadenacimiento.Month > DateTime.Now.Month) || (fechadenacimiento.Month == DateTime.Now.Month && fechadenacimiento.Day > DateTime.Now.Day))
                año--;

            return año;
        }

        public static string GetRange(Vw_Mostrar_Personal_Permisos_PGR employee)
        {
            string range = "N/A";

            if ((string.IsNullOrEmpty(employee.CodigoSeccion) || employee.CodigoSeccion == "0") && string.IsNullOrEmpty(employee.Seccion) && (string.IsNullOrEmpty(employee.CodigoDivision) || employee.CodigoDivision == "0") && string.IsNullOrEmpty(employee.Division)
                && (string.IsNullOrEmpty(employee.CodigoDepartamento) || employee.CodigoDepartamento == "0") && string.IsNullOrEmpty(employee.Departamento))
            {
                range = "DEPENDENCY";
            }
            else if ((string.IsNullOrEmpty(employee.CodigoSeccion) || employee.CodigoSeccion == "0") && string.IsNullOrEmpty(employee.Seccion) && (string.IsNullOrEmpty(employee.CodigoDivision) || employee.CodigoDivision == "0") && string.IsNullOrEmpty(employee.Division))
            {
                range = "DEPARTMENT";
            }
            else if ((string.IsNullOrEmpty(employee.CodigoSeccion) || employee.CodigoSeccion == "0") && string.IsNullOrEmpty(employee.Seccion))
            {
                range = "DIVISION";
            }
            else if ((!string.IsNullOrEmpty(employee.CodigoSeccion) && employee.CodigoSeccion != "0") && !string.IsNullOrEmpty(employee.Seccion) && (!string.IsNullOrEmpty(employee.CodigoDivision) && employee.CodigoDivision != "0") && !string.IsNullOrEmpty(employee.Division)
                && (!string.IsNullOrEmpty(employee.CodigoDepartamento) && employee.CodigoDepartamento != "0") && !string.IsNullOrEmpty(employee.Departamento) && (!string.IsNullOrEmpty(employee.CodigoDependencia) && employee.CodigoDependencia != "0") && !string.IsNullOrEmpty(employee.Dependencia))
            {
                range = "SECTION";
            }

            return range;
        }

        public string GenerateHTMLBody(sp_ultimos_Ponches_Result ponche, Vw_Mostrar_Personal_Permisos_PGR employee,string rrhh_message)
        {
            //string html_file_name = new Utilities2Controller().GetHtmlFileName();
            string html_file_name = HttpRuntime.AppDomainAppPath + "Views\\sp_report_ponches_Result\\ReportPDF.html";
            string html_file = "";

            string pdf_link = "localhost/intra/sp_report_ponches_Result/Report?a=" + employee.Codigo + "&b=" + ponche.poncheFecha.ToString("dd/MM/yyyy") + "&c=" + ponche.poncheFecha.ToString("dd/MM/yyyy") + "&is_from_email=true";

            using (StreamReader sr = new StreamReader(html_file_name))
            {
                html_file = sr.ReadToEnd();
            }

            html_file = html_file.Replace("$Codigo", employee.Codigo);
            html_file = html_file.Replace("$Nombre", employee.NombreCompleto);
            html_file = html_file.Replace("$Departamento", employee.Departamento);
            html_file = html_file.Replace("$Division", employee.Division);
            html_file = html_file.Replace("$Fecha", ponche.poncheFecha.ToString("dd/MM/yyyy"));

            string entrace = "";

            if (ponche.poncheENTRADA != null)
            {
                entrace = ponche.poncheENTRADA.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture);
            }

            html_file = html_file.Replace("$Entrada", entrace);

            string exit = "";

            if (ponche.poncheSALIDA != null)
            {
                exit = ponche.poncheSALIDA.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture);
            }

            html_file = html_file.Replace("$Salida", exit);

            html_file = html_file.Replace("$Observacion", ponche.observacion);

            html_file = html_file.Replace("$_link",pdf_link);

            html_file = html_file.Replace("$_rrhh", rrhh_message);
            
            //var pdf = PdfGenerator.GeneratePdf(html_file, PdfSharp.PageSize.A4);
            ////var pdf = PdfGenerator.GeneratePdf(html_file, pdfsharp);
            ////var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html_file, PdfSharp.PageSize.A4);

            //string file_path = HttpRuntime.AppDomainAppPath + "Content\\Archivos\\ReporteTardanza\\" + employee.NombreCompleto + "-" + employee.Codigo + "-" + ponche.poncheFecha.ToString("dd-MM-yyyy") + "-ReporteTardanza.pdf";

            //pdf.Save(file_path);

            return html_file;

            #region iTextSharp
            //try
            //{
            //    PdfPTable pdfTable3 = new PdfPTable(10);

            //    pdfTable3.WidthPercentage = 95f;

            //    System.Drawing.Font fontH1 = new System.Drawing.Font("Currier", 8);

            //    string imageURL = HttpRuntime.AppDomainAppPath + "Content\\images\\logo.png";
            //    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageURL);

            //    jpg.ScaleToFit(140f, 120f);
            //    jpg.SpacingBefore = 10f;
            //    jpg.SpacingAfter = 1f;
            //    jpg.Alignment = Element.ALIGN_CENTER;

            //    pdfTable3.SpacingBefore = 20;

            //    //Table headers
            //    pdfTable3.AddCell(new Phrase("Código "));
            //    pdfTable3.AddCell(new Phrase("Nombre "));
            //    pdfTable3.AddCell(new Phrase("Dependencia "));
            //    pdfTable3.AddCell(new Phrase("Departamento "));
            //    pdfTable3.AddCell(new Phrase("División "));
            //    pdfTable3.AddCell(new Phrase("Sección "));
            //    pdfTable3.AddCell(new Phrase("Fecha "));
            //    pdfTable3.AddCell(new Phrase("Hora de entrada "));
            //    pdfTable3.AddCell(new Phrase("Hora de salida "));
            //    pdfTable3.AddCell(new Phrase("Obvervación "));

            //    //Table info
            //    pdfTable3.AddCell(new Phrase(employee.Codigo));
            //    pdfTable3.AddCell(new Phrase(employee.NombreCompleto));
            //    pdfTable3.AddCell(new Phrase(employee.Dependencia));
            //    pdfTable3.AddCell(new Phrase(employee.Departamento));
            //    pdfTable3.AddCell(new Phrase(employee.Division));
            //    pdfTable3.AddCell(new Phrase(employee.Seccion));
            //    pdfTable3.AddCell(new Phrase(ponche.poncheFecha.ToString("dd/MM/yyyy")));

            //    string entrace = "";

            //    if(ponche.poncheENTRADA!=null)
            //        entrace = ponche.poncheENTRADA.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture);

            //    pdfTable3.AddCell(new Phrase(entrace));

            //    string exit = "";

            //    if(ponche.poncheSALIDA!=null)
            //        exit = ponche.poncheSALIDA.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture);

            //    pdfTable3.AddCell(new Phrase(exit));

            //    pdfTable3.AddCell(new Phrase(string.IsNullOrEmpty(ponche.observacion)?"":ponche.observacion));

            //    string folderPath = HttpRuntime.AppDomainAppPath + "Content\\Archivos\\ReporteTardanza";

            //    if (!Directory.Exists(folderPath))
            //    {
            //        Directory.CreateDirectory(folderPath);
            //    }

            //    int fileCount = Directory.GetFiles(folderPath).Length;
            //    string strFileName = employee.Codigo + "-" + ponche.poncheFecha.ToString("dd-MM-yyyy") + "-ReporteTardanza.pdf";

            //    using (FileStream stream = new FileStream(folderPath + "\\" + strFileName, FileMode.Create))
            //    {
            //        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            //        PdfWriter.GetInstance(pdfDoc, stream);
            //        pdfDoc.Open();

            //        pdfDoc.Add(jpg);
            //        pdfDoc.Add(pdfTable3);
            //        pdfDoc.NewPage();

            //        pdfDoc.Close();
            //        stream.Close();
            //    }

            //    System.Diagnostics.Process.Start(folderPath + "\\" + strFileName);

            //    return folderPath + "\\" + strFileName;
            //}
            //catch(Exception error)
            //{
            //    var error_message = error.Message;
            //    return null;
            //}
            #endregion
        }

        #region Controlador comentado

        //public class Utilities2Controller : Controller
        //{
        //    // GET: Utilities2
        //    public ActionResult Index()
        //    {
        //        return View();
        //    }

        //    public string GetHtmlFileName()
        //    {
        //        string file = Server.MapPath(@"~/Views/sp_report_ponches_Result/ReportPDF.html");
        //        return file;
        //    }
        //}

        #endregion

        public string GenerateInChargeHTMLBody(List<PoncheEmployee> junior_employees)
        {
            string html_file_name = HttpRuntime.AppDomainAppPath + "Views\\sp_report_ponches_Result\\ReportPDFInCharge.html";
            string html_file = "";

            using (StreamReader sr = new StreamReader(html_file_name))
            {
                html_file = sr.ReadToEnd();
            }

            string inner_body = "";

            foreach (var item in junior_employees)
            {
                string pdf_link =  "localhost/intra/sp_report_ponches_Result/Report?a=" + item.employee.Codigo + "&b=" + item.ponche.poncheFecha.ToString("dd/MM/yyyy") + "&c=" + item.ponche.poncheFecha.ToString("dd/MM/yyyy") + "&is_from_email=true";

                string ponche_date = item.ponche.poncheFecha != null ? item.ponche.poncheFecha.ToString("dd-MM-yyyy") : "N/A";
                string entrace = item.ponche.poncheENTRADA != null ? item.ponche.poncheENTRADA.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A";
                string exit = item.ponche.poncheSALIDA != null ? item.ponche.poncheSALIDA.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture) : "N/A";

                if(string.IsNullOrEmpty(inner_body))
                {
                    inner_body = "<tr class=\"tr_tb2\"><td class=\"tr_tb2\">" + item.employee.Codigo + "</td><td class=\"tr_tb2\">" + item.employee.NombreCompleto + "</td><td class=\"tr_tb2\">" + item.employee.Departamento + "</td><td class=\"tr_tb2\">" + item.employee.Division + "</td><td class=\"td_tb2\">" + ponche_date + "</td><td class=\"td_tb2\">" + entrace + "</td><td class=\"td_tb2\">" + exit + "</td><td class=\"td_tb2\">" + item.ponche.observacion + "</td><td class=\"td_tb2\"><a href=\"" + pdf_link + "\">PDF</a></td></tr>";
                }
                else
                {
                    inner_body = inner_body + "<tr class=\"tr_tb2\"><td class=\"tr_tb2\">" + item.employee.Codigo + "</td><td class=\"tr_tb2\">" + item.employee.NombreCompleto + "</td><td class=\"tr_tb2\">" + item.employee.Departamento + "</td><td class=\"tr_tb2\">" + item.employee.Division + "</td><td class=\"td_tb2\">" + ponche_date + "</td><td class=\"td_tb2\">" + entrace + "</td><td class=\"td_tb2\">" + exit + "</td><td class=\"td_tb2\">" + item.ponche.observacion + "</td><td class=\"td_tb2\"><a href=\"" + pdf_link + "\">PDF</a></td></tr>";
                }
            }

            html_file = html_file.Replace("$_allPonches", inner_body);

            #region PDFGeneration
            //var pdf = PdfGenerator.GeneratePdf(html_file, PdfSharp.PageSize.A4);
            ////var pdf = PdfGenerator.GeneratePdf(html_file, pdfsharp);
            ////var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html_file, PdfSharp.PageSize.A4);

            //string file_path = HttpRuntime.AppDomainAppPath + "Content\\Archivos\\ReporteTardanza\\" + employee.NombreCompleto + "-" + employee.Codigo + "-" + ponche.poncheFecha.ToString("dd-MM-yyyy") + "-ReporteTardanza.pdf";

            //pdf.Save(file_path);
            #endregion

            return html_file;
        }

        public int UserEmail()
        {
            var employees_document = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => (x.Cedula != "" && x.Cedula != "--") && x.CodigoDependencia == "10001" && /*(x.Codigo=="10584" || x.Codigo == "18096")*/x.CodigoDepartamento == "15").ToList();

            foreach (var item in employees_document)
            {
                string result = "";
                if (item.CodigoDepartamento == "15" || item.CodigoDepartamento == "15")
                {
                    result = obtenerCorreoUsuario(item.Cedula);
                }
                else
                {
                    result = GetUserName(item.Cedula);
                }
                
                var j = db.Database.SqlQuery<sp_verif_Encargados_Result>(string.Format("exec sp_verif_Encargados {0}", item.Codigo)).FirstOrDefault();

                /*If the person is not in charge*/
                if (j.Procedencia == "N/A" && j.Nombre == "N/A")
                {
                    var last_ponche = db2.Database.SqlQuery<sp_ultimos_Ponches_Result>(string.Format("sp_ultimos_Ponches {0}", item.Codigo)).FirstOrDefault();

                    if (last_ponche != null)
                    {
                        var last_ponche_date = new DateTime(last_ponche.poncheFecha.Year, last_ponche.poncheFecha.Month, last_ponche.poncheFecha.Day);

                        if (db6.DiasFeriados.Where(x => x.DiasFeriadosFecha == last_ponche_date).FirstOrDefault() == null)
                        {
                            if (!last_ponche.observacion.StartsWith("Justificado") && !last_ponche.observacion.Equals(""))
                            {
                                var privileged_employee = db4.EmpleadosNoPonche.Where(x => x.EmpNoPoncheCodigo.ToString() == item.Codigo).FirstOrDefault();

                                //If the employee has privileges
                                if (privileged_employee != null)
                                {
                                    if (last_ponche.poncheFecha.Date < DateTime.Now.Date)
                                    {
                                        //But him/her state is 0. 0 is equals to disabled
                                        if (privileged_employee.EmpNoPoncheEstatus == 0)
                                        {
                                            tardiness.Add(last_ponche);

                                            string rank = GetRange(item);

                                            Hierachy hierachy = new Hierachy();

                                            switch (rank)
                                            {
                                                case "DEPENDENCY":
                                                    hierachy.type = rank;
                                                    hierachy.name = item.Dependencia;
                                                    hierachy.dependency_id = Convert.ToInt32(item.CodigoDependencia);
                                                    hierachy.department_id = 0;
                                                    hierachy.division_id = 0;
                                                    hierachy.section_id = 0;
                                                    break;

                                                case "DEPARTMENT":
                                                    hierachy.type = rank;
                                                    hierachy.name = item.Departamento;
                                                    hierachy.dependency_id = Convert.ToInt32(item.CodigoDependencia);
                                                    hierachy.department_id = Convert.ToInt32(item.CodigoDepartamento);
                                                    hierachy.division_id = 0;
                                                    hierachy.section_id = 0;
                                                    break;

                                                case "DIVISION":
                                                    hierachy.type = rank;
                                                    hierachy.name = item.Division;
                                                    hierachy.dependency_id = Convert.ToInt32(item.CodigoDependencia);
                                                    hierachy.department_id = Convert.ToInt32(item.CodigoDepartamento);
                                                    hierachy.division_id = Convert.ToInt32(item.CodigoDivision);
                                                    hierachy.section_id = 0;
                                                    break;

                                                case "SECTION":
                                                    hierachy.type = rank;
                                                    hierachy.name = item.Seccion;
                                                    hierachy.dependency_id = Convert.ToInt32(item.CodigoDependencia);
                                                    hierachy.department_id = Convert.ToInt32(item.CodigoDepartamento);
                                                    hierachy.division_id = Convert.ToInt32(item.CodigoDivision);
                                                    hierachy.section_id = Convert.ToInt32(item.CodigoSeccion);
                                                    break;
                                            }

                                            if (hierarchies.Contains(hierachy))
                                            {
                                                var hierachy2 = hierarchies.Where(x => x.type == hierachy.type && x.name == hierachy.name && x.dependency_id == hierachy.dependency_id &&
                                                x.department_id == hierachy.department_id && x.division_id == hierachy.division_id && x.section_id == hierachy.section_id).FirstOrDefault();

                                                hierarchies.Remove(hierachy2);

                                                hierachy2.employees_info.Add(Convert.ToInt32(item.Codigo), item.Cedula);

                                                hierarchies.Add(hierachy2);
                                            }
                                            else
                                            {
                                                hierachy.employees_info.Add(Convert.ToInt32(item.Codigo), item.Cedula);

                                                hierarchies.Add(hierachy);
                                            }
                                        }
                                    }
                                }
                                //If the employee has no privileges
                                else
                                {
                                    if (last_ponche.poncheFecha.Date < DateTime.Now.Date)
                                    {
                                        tardiness.Add(last_ponche);

                                        string rank = GetRange(item);

                                        Hierachy hierachy = new Hierachy();

                                        switch (rank)
                                        {
                                            case "DEPENDENCY":
                                                hierachy.type = rank;
                                                hierachy.name = item.Dependencia;
                                                hierachy.dependency_id = Convert.ToInt32(item.CodigoDependencia);
                                                hierachy.department_id = 0;
                                                hierachy.division_id = 0;
                                                hierachy.section_id = 0;
                                                break;

                                            case "DEPARTMENT":
                                                hierachy.type = rank;
                                                hierachy.name = item.Departamento;
                                                hierachy.dependency_id = Convert.ToInt32(item.CodigoDependencia);
                                                hierachy.department_id = Convert.ToInt32(item.CodigoDepartamento);
                                                hierachy.division_id = 0;
                                                hierachy.section_id = 0;
                                                break;

                                            case "DIVISION":
                                                hierachy.type = rank;
                                                hierachy.name = item.Division;
                                                hierachy.dependency_id = Convert.ToInt32(item.CodigoDependencia);
                                                hierachy.department_id = Convert.ToInt32(item.CodigoDepartamento);
                                                hierachy.division_id = Convert.ToInt32(item.CodigoDivision);
                                                hierachy.section_id = 0;
                                                break;

                                            case "SECTION":
                                                hierachy.type = rank;
                                                hierachy.name = item.Seccion;
                                                hierachy.dependency_id = Convert.ToInt32(item.CodigoDependencia);
                                                hierachy.department_id = Convert.ToInt32(item.CodigoDepartamento);
                                                hierachy.division_id = Convert.ToInt32(item.CodigoDivision);
                                                hierachy.section_id = Convert.ToInt32(item.CodigoSeccion);
                                                break;
                                        }

                                        var hierachy2 = hierarchies.Where(x => x.type == hierachy.type && x.name == hierachy.name && x.dependency_id == hierachy.dependency_id &&
                                            x.department_id == hierachy.department_id && x.division_id == hierachy.division_id && x.section_id == hierachy.section_id).FirstOrDefault();

                                        if (hierachy2 != null)
                                        {
                                            hierarchies.Remove(hierachy2);

                                            hierachy2.employees_info.Add(Convert.ToInt32(item.Codigo), item.Cedula);

                                            hierarchies.Add(hierachy2);
                                        }
                                        else
                                        {
                                            hierachy.employees_info.Add(Convert.ToInt32(item.Codigo), item.Cedula);

                                            hierarchies.Add(hierachy);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return 1;
        }

        public void FillAndSendMail(int number)
        {
            string employee_email = "";
            string subject = "";
            string body = "";

            foreach (var item in hierarchies)
            {
                switch (item.type)
                {
                    case "DEPENDENCY":
                        List<PoncheEmployee> reports = new List<PoncheEmployee>();

                        foreach (var item2 in item.employees_info)
                        {
                            var employee_last_ponche = tardiness.Where(x => x.empleadoId == item2.Key).FirstOrDefault();
                            var employee_data = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == item2.Key.ToString()).FirstOrDefault();

                            var employee_username = "";

                            if(item.department_id==15&&item.dependency_id==10001)
                            {
                                employee_email = obtenerCorreoUsuario(item2.Value);
                            }
                            else
                            {
                                employee_username = GetUserName(item2.Value);
                                
                            }

                            employee_email = employee_username + "@PGR.GOB.DO";

                            if (employee_last_ponche.observacion.ToUpper() == "AUSENCIA INJUSTIFICADA")
                            {
                                subject = "REPORTE DE AUSENCIA INJUSTIFICADA";
                                body = GenerateHTMLBody(employee_last_ponche, employee_data, db6.MensajePonches.Find(4).MensajePonchesDescripcion);
                            }
                            else if (employee_last_ponche.observacion.ToUpper() == "PONCHE INCOMPLETO")
                            {
                                subject = "REPORTE DE PONCHE INCOMPLETO";
                                body = GenerateHTMLBody(employee_last_ponche, employee_data, db6.MensajePonches.Find(1).MensajePonchesDescripcion);
                            }
                            else if (employee_last_ponche.poncheENTRADA > new DateTime(employee_last_ponche.poncheENTRADA.Value.Year, employee_last_ponche.poncheENTRADA.Value.Month, employee_last_ponche.poncheENTRADA.Value.Day, 8, 10, 0))
                            {
                                subject = "REPORTE DE TARDANZA";
                                body = GenerateHTMLBody(employee_last_ponche, employee_data, db6.MensajePonches.Find(2).MensajePonchesDescripcion);
                            }
                            else if (employee_last_ponche.poncheSALIDA < new DateTime(employee_last_ponche.poncheENTRADA.Value.Year, employee_last_ponche.poncheENTRADA.Value.Month, employee_last_ponche.poncheENTRADA.Value.Day, 16, 30, 0))
                            {
                                subject = "REPORTE DE SALIDA TEMPRANA";
                                body = GenerateHTMLBody(employee_last_ponche, employee_data, db6.MensajePonches.Find(3).MensajePonchesDescripcion);
                            }
                            
                            new Mail().SendMail(employee_email, subject, body, DateTime.Now.Date.ToString("dd/MM/yyyy"));
                            reports.Add(new PoncheEmployee() { employee = employee_data, ponche = employee_last_ponche });
                        }

                        var dependency = db.Nmdependencias_Permisos.Where(x => x.DependenciaID == item.dependency_id).FirstOrDefault();
                        var in_charge = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == dependency.DirectorID.ToString()).FirstOrDefault();
                        
                        if (in_charge != null)
                        {
                            var in_charge_username = "";

                            if (in_charge.CodigoDepartamento=="15"&&in_charge.CodigoDependencia=="10001")
                            {
                                in_charge_username = obtenerCorreoUsuario(in_charge.Cedula);
                            }
                            else
                            {
                                in_charge_username = GetUserName(in_charge.Cedula);
                            }

                            var assistant_code = db.Nmdependencias_Permisos.Where(x => x.DependenciaID == item.dependency_id).FirstOrDefault().AuxiliarID;

                            var assistan_username = "";

                            if (assistant_code!=null)
                            {
                                var assistant_info = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == assistant_code.Value.ToString()).FirstOrDefault();

                                if (assistant_info.CodigoDepartamento == "15" && assistant_info.CodigoDependencia == "10001")
                                {
                                    assistan_username = obtenerCorreoUsuario(assistant_info.Cedula);
                                }
                                else
                                {
                                    assistan_username = GetUserName(assistant_info.Cedula);
                                }
                            }
                            
                            var assistant_email = assistan_username + "@PGR.GOB.DO";
                            employee_email = in_charge_username + "@PGR.GOB.DO";
                            subject = "REPORTES DE PONCHES";
                            body = GenerateInChargeHTMLBody(reports);

                            /*Here goes the assistant email*/

                            new Mail().SendMail(employee_email,assistant_email,subject, body, DateTime.Now.Date.ToString("dd/MM/yyyy"));
                        }

                        break;

                    case "DEPARTMENT":
                        List<PoncheEmployee> reports2 = new List<PoncheEmployee>();

                        foreach (var item2 in item.employees_info)
                        {
                            var employee_last_ponche = tardiness.Where(x => x.empleadoId == item2.Key).FirstOrDefault();
                            var employee_data = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == item2.Key.ToString()).FirstOrDefault();
                            
                            var employee_username = "";

                            if(item.dependency_id==10001&&item.department_id==15)
                            {
                                employee_username = obtenerCorreoUsuario(item2.Value);
                            }
                            else
                            {
                                employee_username = GetUserName(item2.Value);
                            }
                            
                            employee_email = employee_username + "@PGR.GOB.DO";

                            if (employee_last_ponche.observacion.ToUpper() == "AUSENCIA INJUSTIFICADA")
                            {
                                subject = "REPORTE DE AUSENCIA INJUSTIFICADA";
                                body = GenerateHTMLBody(employee_last_ponche,employee_data, db6.MensajePonches.Find(4).MensajePonchesDescripcion);
                            }
                            else if (employee_last_ponche.observacion.ToUpper() == "PONCHE INCOMPLETO")
                            {
                                subject = "REPORTE DE PONCHE INCOMPLETO";
                                body = GenerateHTMLBody(employee_last_ponche,employee_data,db6.MensajePonches.Find(1).MensajePonchesDescripcion);
                            }
                            else if (employee_last_ponche.poncheENTRADA > new DateTime(employee_last_ponche.poncheENTRADA.Value.Year, employee_last_ponche.poncheENTRADA.Value.Month, employee_last_ponche.poncheENTRADA.Value.Day, 8, 10, 0))
                            {
                                subject = "REPORTE DE TARDANZA";
                                body = GenerateHTMLBody(employee_last_ponche,employee_data,db6.MensajePonches.Find(2).MensajePonchesDescripcion);
                            }
                            else if (employee_last_ponche.poncheSALIDA < new DateTime(employee_last_ponche.poncheENTRADA.Value.Year, employee_last_ponche.poncheENTRADA.Value.Month, employee_last_ponche.poncheENTRADA.Value.Day, 16, 30, 0))
                            {
                                subject = "REPORTE DE SALIDA TEMPRANA";
                                body = GenerateHTMLBody(employee_last_ponche,employee_data,db6.MensajePonches.Find(3).MensajePonchesDescripcion);
                            }
                            
                            new Mail().SendMail(employee_email, subject, body, DateTime.Now.Date.ToString("dd/MM/yyyy"));
                            reports2.Add(new PoncheEmployee() {employee = employee_data, ponche = employee_last_ponche });
                        }

                        var department = db.Nmdeptos_Permisos.Where(x => x.DependenciaID == item.dependency_id && x.DeptoID == item.department_id).FirstOrDefault();
                        var in_charge2 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == department.EncargadoID.ToString()).FirstOrDefault();

                        if (in_charge2 != null)
                        {
                            var in_charge_username2 = "";

                            if (in_charge2.CodigoDepartamento == "15" && in_charge2.CodigoDependencia == "10001")
                            {
                                in_charge_username2 = obtenerCorreoUsuario(in_charge2.Cedula);
                            }
                            else
                            {
                                in_charge_username2 = GetUserName(in_charge2.Cedula);
                            }

                            var assistant_code2 = db.Nmdeptos_Permisos.Where(x => x.DependenciaID == item.dependency_id && x.DeptoID == item.department_id).FirstOrDefault().AuxiliarID;

                            var assistan_username2 = "";

                            if (assistant_code2 != null)
                            {
                                var assistant_info2 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == assistant_code2.Value.ToString()).FirstOrDefault();

                                if (assistant_info2.CodigoDepartamento == "15" && assistant_info2.CodigoDependencia == "10001")
                                {
                                    assistan_username2 = obtenerCorreoUsuario(assistant_info2.Cedula);
                                }
                                else
                                {
                                    assistan_username2 = GetUserName(assistant_info2.Cedula);
                                }
                            }

                            var assistant_email2 = assistan_username2 + "@PGR.GOB.DO";
                            employee_email = in_charge_username2 + "@PGR.GOB.DO";
                            subject = "REPORTES DE PONCHES";
                            body = GenerateInChargeHTMLBody(reports2);

                            new Mail().SendMail(employee_email, assistant_email2, subject, body, DateTime.Now.Date.ToString("dd/MM/yyyy"));
                        }

                        break;

                    case "DIVISION":
                        List<PoncheEmployee> reports3 = new List<PoncheEmployee>();

                        foreach (var item2 in item.employees_info)
                        {
                            var employee_last_ponche = tardiness.Where(x => x.empleadoId == item2.Key).FirstOrDefault();
                            var employee_data = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == item2.Key.ToString()).FirstOrDefault();
                            
                            var employee_username = "";

                            if (item.dependency_id == 10001 && item.department_id == 15)
                            {
                                employee_username = obtenerCorreoUsuario(item2.Value);
                            }
                            else
                            {
                                employee_username = GetUserName(item2.Value);
                            }

                            employee_email = employee_username + "@PGR.GOB.DO";

                            if (employee_last_ponche.observacion.ToUpper() == "PONCHE INCOMPLETO")
                            {
                                subject = "REPORTE DE PONCHE INCOMPLETO";
                                body = GenerateHTMLBody(employee_last_ponche, employee_data,db6.MensajePonches.Find(1).MensajePonchesDescripcion);
                            }
                            else if (employee_last_ponche.observacion.ToUpper() == "AUSENCIA INJUSTIFICADA")
                            {
                                subject = "REPORTE DE AUSENCIA INJUSTIFICADA";
                                body = GenerateHTMLBody(employee_last_ponche, employee_data, db6.MensajePonches.Find(4).MensajePonchesDescripcion);
                            }
                            else if (employee_last_ponche.poncheENTRADA > new DateTime(employee_last_ponche.poncheENTRADA.Value.Year, employee_last_ponche.poncheENTRADA.Value.Month, employee_last_ponche.poncheENTRADA.Value.Day, 8, 10, 0))
                            {
                                subject = "REPORTE DE TARDANZA";
                                body = GenerateHTMLBody(employee_last_ponche, employee_data, db6.MensajePonches.Find(2).MensajePonchesDescripcion);
                            }
                            else if (employee_last_ponche.poncheSALIDA < new DateTime(employee_last_ponche.poncheENTRADA.Value.Year, employee_last_ponche.poncheENTRADA.Value.Month, employee_last_ponche.poncheENTRADA.Value.Day, 16, 30, 0))
                            {
                                subject = "REPORTE DE SALIDA TEMPRANA";
                                body = GenerateHTMLBody(employee_last_ponche, employee_data, db6.MensajePonches.Find(3).MensajePonchesDescripcion); 
                            }

                            new Mail().SendMail(employee_email, subject, body, DateTime.Now.Date.ToString("dd/MM/yyyy"));
                            reports3.Add(new PoncheEmployee() { employee = employee_data, ponche = employee_last_ponche });
                        }

                        var division = db.Nmdivision_Permisos.Where(x => x.DependenciaID == item.dependency_id && x.DeptoID == item.department_id && x.DivisionID == item.division_id).FirstOrDefault();
                        var in_charge3 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == division.EncargadoID.ToString()).FirstOrDefault();

                        if (in_charge3 != null)
                        {
                            var in_charge_username3 = "";

                            if (in_charge3.CodigoDepartamento == "15" && in_charge3.CodigoDependencia == "10001")
                            {
                                in_charge_username3 = obtenerCorreoUsuario(in_charge3.Cedula);
                            }
                            else
                            {
                                in_charge_username3 = GetUserName(in_charge3.Cedula);
                            }

                            var assistant_code3 = db.Nmdivision_Permisos.Where(x => x.DependenciaID == item.dependency_id && x.DeptoID == item.department_id && x.DivisionID == item.division_id).FirstOrDefault().AuxiliarID;

                            var assistan_username3 = "";

                            if (assistant_code3 != null)
                            {
                                var assistant_info3 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == assistant_code3.Value.ToString()).FirstOrDefault();

                                if (assistant_info3.CodigoDepartamento == "15" && assistant_info3.CodigoDependencia == "10001")
                                {
                                    assistan_username3 = obtenerCorreoUsuario(assistant_info3.Cedula);
                                }
                                else
                                {
                                    assistan_username3 = GetUserName(assistant_info3.Cedula);
                                }
                            }

                            var assistant_email3 = assistan_username3 + "@PGR.GOB.DO";

                            employee_email = in_charge_username3 + "@PGR.GOB.DO";
                            subject = "REPORTES DE PONCHES";
                            body = GenerateInChargeHTMLBody(reports3);

                            new Mail().SendMail(employee_email,assistant_email3, subject, body, DateTime.Now.Date.ToString("dd/MM/yyyy"));
                        }

                        break;

                    case "SECTION":
                        List<PoncheEmployee> reports4 = new List<PoncheEmployee>();

                        foreach (var item2 in item.employees_info)
                        {
                            var employee_last_ponche = tardiness.Where(x => x.empleadoId == item2.Key).FirstOrDefault();
                            var employee_data = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == item2.Key.ToString()).FirstOrDefault();
                            
                            var employee_username = "";

                            if (item.dependency_id == 10001 && item.department_id == 15)
                            {
                                employee_username = obtenerCorreoUsuario(item2.Value);
                            }
                            else
                            {
                                employee_username = GetUserName(item2.Value);
                            }

                            employee_email = employee_username + "@PGR.GOB.DO";

                            if (employee_last_ponche.observacion.ToUpper() == "PONCHE INCOMPLETO")
                            {
                                subject = "REPORTE DE PONCHE INCOMPLETO";
                                body = GenerateHTMLBody(employee_last_ponche, employee_data, db6.MensajePonches.Find(1).MensajePonchesDescripcion);
                            }
                            else if (employee_last_ponche.observacion.ToUpper() == "AUSENCIA INJUSTIFICADA")
                            {
                                subject = "REPORTE DE AUSENCIA INJUSTIFICADA";
                                body = GenerateHTMLBody(employee_last_ponche, employee_data, db6.MensajePonches.Find(4).MensajePonchesDescripcion);
                            }
                            else if (employee_last_ponche.poncheENTRADA > new DateTime(employee_last_ponche.poncheENTRADA.Value.Year, employee_last_ponche.poncheENTRADA.Value.Month, employee_last_ponche.poncheENTRADA.Value.Day, 8, 10, 0))
                            {
                                subject = "REPORTE DE TARDANZA";
                                body = GenerateHTMLBody(employee_last_ponche, employee_data, db6.MensajePonches.Find(2).MensajePonchesDescripcion);
                            }
                            else if (employee_last_ponche.poncheSALIDA < new DateTime(employee_last_ponche.poncheENTRADA.Value.Year, employee_last_ponche.poncheENTRADA.Value.Month, employee_last_ponche.poncheENTRADA.Value.Day, 16, 30, 0))
                            {
                                subject = "REPORTE DE SALIDA TEMPRANA";
                                body = GenerateHTMLBody(employee_last_ponche, employee_data, db6.MensajePonches.Find(3).MensajePonchesDescripcion);
                            }
                            
                            new Mail().SendMail(employee_email, subject, body, DateTime.Now.Date.ToString("dd/MM/yyyy"));
                            reports4.Add(new PoncheEmployee() { employee = employee_data, ponche = employee_last_ponche });
                        }

                        var section = db.NmSecciones_Permisos.Where(x => x.DependenciaID == item.dependency_id && x.DeptoID == item.department_id && x.DivisionID == item.division_id && x.SeccionID == item.section_id).FirstOrDefault();
                        var in_charge4 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == section.EncargadoID.ToString()).FirstOrDefault();

                        if (in_charge4 != null)
                        {
                            var in_charge_username4 = "";

                            if(in_charge4.CodigoDepartamento=="15"&&in_charge4.CodigoDependencia== "10001")
                            {
                                in_charge_username4 = obtenerCorreoUsuario(in_charge4.Cedula);
                            }
                            else
                            {
                                in_charge_username4 = GetUserName(in_charge4.Cedula);
                            }

                            var assistant_code4 = db.NmSecciones_Permisos.Where(x => x.DependenciaID == item.dependency_id && x.DeptoID == item.department_id && x.DivisionID == item.division_id && x.SeccionID == item.section_id).FirstOrDefault().AuxiliarID;

                            var assistan_username4 = "";

                            if (assistant_code4 != null)
                            {
                                var assistant_info4 = db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == assistant_code4.Value.ToString()).FirstOrDefault();

                                if (assistant_info4.CodigoDepartamento == "15" && assistant_info4.CodigoDependencia == "10001")
                                {
                                    assistan_username4 = obtenerCorreoUsuario(assistant_info4.Cedula);
                                }
                                else
                                {
                                    assistan_username4 = GetUserName(assistant_info4.Cedula);
                                }
                            }

                            var assistant_email4 = assistan_username4 + "@PGR.GOB.DO";

                            employee_email = in_charge_username4 + "@PGR.GOB.DO";
                            subject = "REPORTES DE PONCHES";
                            body = GenerateInChargeHTMLBody(reports4);

                            new Mail().SendMail(employee_email, assistant_email4, subject, body, DateTime.Now.Date.ToString("dd/MM/yyyy"));
                        }

                        break;
                }
            }

            Console.WriteLine("Proccess Successfully");
        }

        public static bool convertirStringAfecha(string fechaEnString, out DateTime FechaConvertida) {
            return DateTime.TryParseExact(fechaEnString, "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out FechaConvertida);
        }

        public void Execute(IJobExecutionContext context)
        {
            FillAndSendMail(UserEmail());
        }

        Task IJob.Execute(IJobExecutionContext context)
        {
            throw new NotImplementedException();
        }
    }
}