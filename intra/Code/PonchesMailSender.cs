﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using System.Threading.Tasks;

namespace intra.Code
{
    public class PonchesMailSender
    {
        public static void Start()
        {
            IScheduler scheduler = (IScheduler)StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<Utilities2>().Build();

            ITrigger trigger = TriggerBuilder.Create().StartNow().
               WithSimpleSchedule(x => x.WithIntervalInHours(23).RepeatForever()).Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}