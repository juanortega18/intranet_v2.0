﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Helpers
{
    public class ElementoHijo
    {
        public int ElementoHijoID  { get; set; }
        public int? ElementoHijoMadreID { get; set; }
        public string ElementoHijoNombre { get; set; }
        public string ElementoHijoExtension { get; set; }
        public string ElementoHijoRuta { get; set; }
        public bool? ElementoHijoTipo { get; set; }
    }
}