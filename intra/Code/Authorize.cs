﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc.Properties;
using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using System.Web.Routing;
using System.Configuration;
using System.Data.SqlClient;


namespace intra
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        private readonly object _typeId = new object();

        private string _roles;
        private string[] _rolesSplit = new string[0];
        private string _users;
        private string[] _usersSplit = new string[0];

      

      
        public override object TypeId
        {
            get { return _typeId; }
        }

      


        public   override void  OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if (OutputCacheAttribute.IsChildActionCacheActive(filterContext))
            {
                // If a child action cache block is active, we need to fail immediately, even if authorization
                // would have succeeded. The reason is that there's no way to hook a callback to rerun
                // authorization before the fragment is served from the cache, so we can't guarantee that this
                // filter will be re-run on subsequent requests.
                throw new InvalidOperationException();
            }

            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true)
                                     || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true);

            if (skipAuthorization)
            {
                return;
            }

            if (AuthorizeCore(filterContext.HttpContext))
            {
                HttpCachePolicyBase cachePolicy = filterContext.HttpContext.Response.Cache;
                cachePolicy.SetProxyMaxAge(new TimeSpan(0));
                cachePolicy.AddValidationCallback(CacheValidateHandler, null /* data */);
            }
            else
            {
                HandleUnauthorizedRequest(filterContext);
            }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }
            
            
            if (!httpContext.User.Identity.IsAuthenticated || httpContext.Session["nombre"] == null)
            {
                bool pasa = false;
                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower(); //httpContext.User.Identity.Name.ToLower();
                 usuario = httpContext.Request.ServerVariables["LOGON_USER"].ToLower();
                
                if (!string.IsNullOrEmpty(usuario))
                {
                   

                    if (usuario.IndexOf(ConfigurationManager.AppSettings["dominio"].ToLower()) > -1) // es un usuario que esta dentro del dominio
                    {
                        usuario = usuario.Replace(ConfigurationManager.AppSettings["dominio"].ToLower(), "");
                        bool pasaAuth = false;
                        string pusuario = ""; string departamento = ""; string cedula = ""; string idempleado = ""; 
                        using (dbIntranet rh = new dbIntranet())
                        {
                    
                           
                              
                            SqlParameter param1 = new SqlParameter("ulpad", usuario);
                            var ulpad = rh.SP_USERS_LDAP.SqlQuery("sp_users_ldap @ulpad", param1).FirstOrDefault();
                            if(ulpad != null)
                            {
                                pusuario = ulpad.displayName;
                                cedula = ulpad.employeeid;
                                if (string.IsNullOrEmpty(ulpad.department))
                                    departamento = "No Tiene Asignado en AD";
                                else
                                    departamento = ulpad.department;
                                var empl = rh.VISTA_EMPLEADOS.Where(x => x.cedula == cedula).FirstOrDefault();
                                if (empl != null)
                                {
                                    idempleado = empl.empleadoid.ToString();
                                    departamento = empl.departamento;
                        
                                   
                                }

                                pasaAuth = true;
                            }
                        }
                            if (pasaAuth)
                            {

                            setAuthIntra au = new intra.setAuthIntra(httpContext, usuario);

                                httpContext.Session["nombre"] = pusuario;
                                httpContext.Session["cedula"] = cedula;
                                httpContext.Session["empleadoId"] = idempleado;
                                httpContext.Session["usuario"] = usuario;
                                httpContext.Session["departamento"] = departamento;

                              
                            pasa = true;
                            }
                        }
                  
                   

                }
               

                if(!pasa)
                  return pasa;
            }

            if (_usersSplit.Length > 0 && !_usersSplit.Contains(httpContext.User.Identity.Name, StringComparer.OrdinalIgnoreCase))
            {
                return false;
            }

            if (_rolesSplit.Length > 0 && !_rolesSplit.Any(httpContext.User.IsInRole))
            {
                return false;
            }

            return true;
        }

        private void CacheValidateHandler(HttpContext context, object data, ref HttpValidationStatus validationStatus)
        {
            validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "login" }));
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "login" }));
            }
        }

        internal static string[] SplitString(string original)
        {
            if (String.IsNullOrEmpty(original))
            {
                return new string[0];
            }

            var split = from piece in original.Split(',')
                        let trimmed = piece.Trim()
                        where !String.IsNullOrEmpty(trimmed)
                        select trimmed;
            return split.ToArray();
        }
    }
}