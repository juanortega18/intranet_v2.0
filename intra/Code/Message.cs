﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;


    public static class Message
    {
        public static void sendMessage(string correo,string[] valoresRelleno = null)
        {

            SmtpClient client = new SmtpClient("");
            client.Port = 25;
            client.EnableSsl = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("intranet", "Cven7777");
            client.Host = "pgrpx03.pgr.local";
            string body = "";
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress("intranet@pgr.gob.do");
            mail.To.Add(correo);
            mail.Subject = "Notificación de Contravención";
            mail.IsBodyHtml = true;
            mail.Body = "PRUEBA";
            try
            {
                using (var lector = new System.IO.StreamReader(HttpRuntime.AppDomainAppPath + "\\PlantillaCorreo\\Solicitud_nuevoServicio_PGR.html"))
                    body = lector.ReadToEnd();
            }
            catch
            {

            }

            //var holders = GetValuePair(body, valoresRelleno);
            //foreach (var a in holders)
            //    body = body.Replace(a.Key, a.Value);

            mail.Body = body;

    //        AlternateView htmlView =
    //             AlternateView.CreateAlternateViewFromString(body,
    //                                     Encoding.UTF8,
    //                                     MediaTypeNames.Text.Html);

    //        LinkedResource img =
    //new LinkedResource(@"C:\Users\jpadilla\Documents\Proyecto VS\Agente-X\Agente X\Content\images\avatar.png",
    //                   "image/png");
    //        img.ContentId = "imagenImail";

    //        htmlView.LinkedResources.Add(img);
    //        mail.AlternateViews.Add(htmlView);


            try
            {
                client.Send(mail);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
                        System.Threading.Thread.Sleep(5000);
                        client.Send(mail);
                    }
                    else
                    {
                        Console.WriteLine("No se pudo entregar el mensaje a {0}",
                            ex.InnerExceptions[i].FailedRecipient);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
                        ex.ToString());
            }

        }

        public static void sendMessageError(string[] valoresRelleno = null)
        {

            SmtpClient client = new SmtpClient("");
            client.Port = 25;
            client.EnableSsl = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("cesarlachapelle05@gmail.com", "");
            client.Host = "pgrpx03.pgr.local";

            //     client.UseDefaultCredentials = false; client.Credentials = new NetworkCredential("myAccount", "myPassword", "myDomain");
            string body = "";
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress("jean@gobiernoe.com");
            mail.To.Add("jpadilla@pgr.gob.do");
            mail.Subject = "Notificación de Contravención";
            mail.IsBodyHtml = true;
            mail.Body = "NOTIFICACION ERROR - AGENTE X";
            try
            {
                // Get the message format
                using (var lector = new System.IO.StreamReader(HttpRuntime.AppDomainAppPath + "\\PlantillaCorreo\\NotificacionError.html"))
                    body = lector.ReadToEnd();
            }
            catch
            {

            }

            var holders = GetValuePair(body, valoresRelleno);

            // Replace the holders
            foreach (var a in holders)
                body = body.Replace(a.Key, a.Value);

            mail.Body = body;

            AlternateView htmlView =
                 AlternateView.CreateAlternateViewFromString(body,
                                         Encoding.UTF8,
                                         MediaTypeNames.Text.Html);

          //  LinkedResource img =
    ///new LinkedResource(@"C:\Users\jpadilla\Documents\Proyecto VS\Agente-X\Agente X\Content\images\avatar.png",
   //                    "image/png");
     //       img.ContentId = "imagenImail";

            // Lo incrustamos en la vista HTML...

     //       htmlView.LinkedResources.Add(img);

            // Por último, vinculamos ambas vistas al mensaje...

     //       mail.AlternateViews.Add(htmlView);
            try
            {
                client.Send(mail);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("La entrega no - volver a intentar dentro de 5 segundos.");
                        System.Threading.Thread.Sleep(5000);
                        client.Send(mail);
                    }
                    else
                    {
                        Console.WriteLine("No se pudo entregar el mensaje a {0}",
                            ex.InnerExceptions[i].FailedRecipient);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepción capturada en estado Reintentar Si ocupado...(): {0}",
                        ex.ToString());
            }

        }

        private static KeyValuePair<string, string>[] GetValuePair(string mailTextBody, params string[] values)
        {
            var holders = new Dictionary<string, string>();

            var placeholders = System.Text.RegularExpressions.Regex.Matches(mailTextBody, @"\$[\w\-]+").Cast<System.Text.RegularExpressions.Match>().Select((obj) => obj.Value).Distinct().ToArray();

            if (placeholders.Length != values.Length)
            {
                var error = new Exception(string.Format("La cantidad de \"contenedores\" y \"valores\" no coinciden. Cantidad de valores = {0}; cantidad de contenedores = {1}", values.Length, placeholders.Length));

                throw error;

                //var placeText = "";

                //error.InnerException = new Exception("Contenedores recibidos ()");
            }

            for (int x = 0; x < placeholders.Length; x++)
                holders.Add(placeholders[x], values[x]);
            return holders.ToArray();
        }




    }






