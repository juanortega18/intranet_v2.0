﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using intra.Models;
using intra.Models.GestionHumana;

namespace intra.Code
{
    public class PoncheEmployee
    {
        public sp_ultimos_Ponches_Result ponche { get; set; }
        public Vw_Mostrar_Personal_Permisos_PGR employee { get; set; }
    }
}