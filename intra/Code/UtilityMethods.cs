﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Globalization;
using intra.Models.GestionHumana;

namespace intra.Code
{
    public static class UtilityMethods
    {
        /// <summary>
        /// Aplicar el algoritmo SHA1 al texto especificado
        /// </summary>
        /// <param name="texto">Texto a codificar</param>
        /// <returns>Texto codificado</returns>
        public static string SetSHA1(string texto)
        {
            SHA1 sha1 = SHA1.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha1.ComputeHash(encoding.GetBytes(texto));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

        /// <summary>
        /// Get the requester IP
        /// </summary>
        /// <returns>The machine's IP</returns>
        public static string GetIp()
        {
            string result = "";

            //foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            //{
            //    if (IPA.AddressFamily == AddressFamily.InterNetwork)
            //    {
            //        result = IPA.ToString();
            //        break;
            //    }
            //}

            result = HttpContext.Current.Request.UserHostAddress;

            return result;
        }


        #region Enviar Email
        /// <summary>
        /// Metodo para enviar Emails
        /// </summary>
        /// <param name="fileName">Archivo HTML</param>
        /// <param name="subject">Título del mensaje</param>
        /// <param name="valoresRelleno">Valores para reemplazar los contenedores (placeholders) de la plantilla HTML</param>
        /// <param name="mailTo">Destinatario del mensaje</param>
        /// <returns>Si se envia el mensaje'TRUE', de lo contrario 'FALSE'</returns>
        public static bool SendMail(string fileName, string subject, string[] valoresRelleno, params string[] mailTo)
        {
            if (mailTo.Length == 0)
                return false;

            string body;
            var msg = new MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["Direccion"]);
            msg.Subject = subject;
            msg.IsBodyHtml = true;

            // Add all the desired receivers e-mails
            foreach (var a in mailTo)
                msg.To.Add(new MailAddress(a));

            try
            {
                // Get the message format
                using (var lector = new System.IO.StreamReader(fileName))
                    body = lector.ReadToEnd();
            }
            catch
            {
                return false;
            }

            var holders = GetValuePair(body, valoresRelleno);

            // Replace the holders
            foreach (var a in holders)
                body = body.Replace(a.Key, a.Value);

            msg.Body = body;
            var smtp = new SmtpClient();
            int port;
            smtp.Port = int.TryParse(ConfigurationManager.AppSettings["Port"], out port) ? port : 25;

            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["Usuario"], ConfigurationManager.AppSettings["Contrasena"]);
            smtp.Host = ConfigurationManager.AppSettings["Host"];

            try
            {
                smtp.Send(msg);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }


        /// <summary>
        /// Relacionar el par Llave-Valor de los contenedores y valores de relleno de la plantilla HTML
        /// </summary>
        /// <param name="mailTextBody">Cuerpo del mensaje (plantilla HTML)</param>
        /// <param name="values">Colección de valores para rellenar los contenedores</param>
        /// <returns>Colección de Llaves-Valores relacionando los contenedores con sus respectivos rellenos</returns>
        /// <exception cref="Exception"></exception>
        private static KeyValuePair<string, string>[] GetValuePair(string mailTextBody, params string[] values)
        {
            var holders = new Dictionary<string, string>();

            var placeholders = System.Text.RegularExpressions.Regex.Matches(mailTextBody, @"\$[\w\-]+").Cast<System.Text.RegularExpressions.Match>().Select((obj) => obj.Value).Distinct().ToArray();

            if (placeholders.Length != values.Length)
            {
                var error = new Exception(string.Format("La cantidad de \"contenedores\" y \"valores\" no coinciden. Cantidad de valores = {0}; cantidad de contenedores = {1}", values.Length, placeholders.Length));

                throw error;

                //var placeText = "";

                //error.InnerException = new Exception("Contenedores recibidos ()");
            }

            for (int x = 0; x < placeholders.Length; x++)
                holders.Add(placeholders[x], values[x]);

            return holders.ToArray();
        }
        #endregion

        #region Obtener Encargado

        private struct Info
        {
            public int? EncargadoID;
            public int? AuxiliarID;
            public int? PermisosVacacionesStatus;
        }

        public static int? buscarEncargado(Vw_Mostrar_Personal_Permisos_PGR Empleado)
        {
            Info info = _obtenerInfoEncargado(Empleado.CodigoSeccion, Empleado.CodigoDivision, Empleado.CodigoDepartamento, Empleado.CodigoDependencia);

            if (info.EncargadoID.ToString() == Empleado.Codigo && !String.IsNullOrEmpty(Empleado.CodigoSeccion) && !String.IsNullOrEmpty(Empleado.CodigoDivision) && !String.IsNullOrEmpty(Empleado.CodigoDepartamento) && !String.IsNullOrEmpty(Empleado.CodigoDependencia))
            {
                info = _obtenerInfoEncargado(null, Empleado.CodigoDivision, Empleado.CodigoDepartamento, Empleado.CodigoDependencia);
            }
            else if (info.EncargadoID.ToString() == Empleado.Codigo && String.IsNullOrEmpty(Empleado.CodigoSeccion) && !String.IsNullOrEmpty(Empleado.CodigoDivision) && !String.IsNullOrEmpty(Empleado.CodigoDepartamento) && !String.IsNullOrEmpty(Empleado.CodigoDependencia))
            {
                info = _obtenerInfoEncargado(null, null, Empleado.CodigoDepartamento, Empleado.CodigoDependencia);
            }
            else if (info.EncargadoID.ToString() == Empleado.Codigo && String.IsNullOrEmpty(Empleado.CodigoSeccion) && String.IsNullOrEmpty(Empleado.CodigoDivision) && !String.IsNullOrEmpty(Empleado.CodigoDepartamento) && !String.IsNullOrEmpty(Empleado.CodigoDependencia))
            {
                info = _obtenerInfoEncargado(null, null, null, Empleado.CodigoDependencia);
            }

            return info.EncargadoID;
        }

        private static Info _obtenerInfoEncargado(string codigoSeccion, string codigoDivision, string codigoDepartamento, string codigoDependencia)
        {
            if (!String.IsNullOrEmpty(codigoSeccion) && !String.IsNullOrEmpty(codigoDivision) && !String.IsNullOrEmpty(codigoDepartamento) && !String.IsNullOrEmpty(codigoDependencia))
            {
                using (var db = new dbIntranet())
                {
                    var query = (
                        from seccion
                        in db.NmSecciones_Permisos
                        where
                            seccion.SeccionID.ToString() == codigoSeccion       &&
                            seccion.DivisionID.ToString() == codigoDivision     &&
                            seccion.DeptoID.ToString() == codigoDepartamento    &&
                            seccion.DependenciaID.ToString() == codigoDependencia
                        select seccion
                    ).FirstOrDefault();

                    Info info = new Info();
                    info.EncargadoID = query.EncargadoID;
                    info.AuxiliarID = query.AuxiliarID;
                    info.PermisosVacacionesStatus = query.PermisosVacacionesStatus;

                    return info;
                }
            }

            if (String.IsNullOrEmpty(codigoSeccion) && !String.IsNullOrEmpty(codigoDivision) && !String.IsNullOrEmpty(codigoDepartamento) && !String.IsNullOrEmpty(codigoDependencia))
            {
                using (var db = new dbIntranet())
                {
                    var query = (
                        from division
                        in db.Nmdivision_Permisos
                        where
                            division.DivisionID.ToString() == codigoDivision        &&
                            division.DeptoID.ToString() == codigoDepartamento       &&
                            division.DependenciaID.ToString() == codigoDependencia
                        select division
                    ).FirstOrDefault();

                    Info info = new Info();
                    info.EncargadoID = query.EncargadoID;
                    info.AuxiliarID = query.AuxiliarID;
                    info.PermisosVacacionesStatus = query.PermisosVacacionesStatus;

                    return info;
                }
            }

            if (String.IsNullOrEmpty(codigoSeccion) && String.IsNullOrEmpty(codigoDivision) && !String.IsNullOrEmpty(codigoDepartamento) && !String.IsNullOrEmpty(codigoDependencia))
            {
                using (var db = new dbIntranet())
                {
                    var query = (
                        from departamento
                        in db.Nmdeptos_Permisos
                        where
                            departamento.DeptoID.ToString() == codigoDepartamento       &&
                            departamento.DependenciaID.ToString() == codigoDependencia
                        select departamento
                    ).FirstOrDefault();

                    Info info = new Info();
                    info.EncargadoID = query.EncargadoID;
                    info.AuxiliarID = query.AuxiliarID;
                    info.PermisosVacacionesStatus = query.PermisosVacacionesStatus;

                    return info;
                }
            }

            if (String.IsNullOrEmpty(codigoSeccion) && String.IsNullOrEmpty(codigoDivision) && String.IsNullOrEmpty(codigoDepartamento) && !String.IsNullOrEmpty(codigoDependencia))
            {
                using (var db = new dbIntranet())
                {
                    var query = (
                        from dependencia
                        in db.Nmdependencias_Permisos
                        where
                            dependencia.DependenciaID.ToString() == codigoDependencia
                        select dependencia
                    ).FirstOrDefault();

                    Info info = new Info();
                    info.EncargadoID = query.DirectorID;
                    info.AuxiliarID = query.AuxiliarID;
                    info.PermisosVacacionesStatus = query.PermisosVacacionesStatus;

                    return info;
                }
            }

            return new Info();
        }
        #endregion

        public static DateTime ConvertirFechaStringADateTime(string fecha)
        {
            CultureInfo ci = CultureInfo.InstalledUICulture;

            string nombre_idioma = ci.Name;

            DateTime fecha_date = new DateTime();

            if (nombre_idioma == "en-US")
            {
                if (fecha != "")
                {
                    string[] splitted_date = fecha.ToString().Split('-');
                    string[] splitted_time = splitted_date[2].Split(' ');
                    string[] final_split = { splitted_date[0], splitted_date[1], splitted_time[0],
                        splitted_time[1] + " " + splitted_time[2] };
                    fecha_date = new DateTime(Convert.ToInt32(final_split[2]), Convert.ToInt32(final_split[1]),
                        Convert.ToInt32(final_split[0]), DateTime.Parse(final_split[3]).TimeOfDay.Hours,
                        DateTime.Parse(final_split[3]).TimeOfDay.Minutes, 0);
                }
            }
            else if(nombre_idioma == "es-ES")
            {
                fecha_date = Convert.ToDateTime(fecha);
            }

            return fecha_date;
        }


        /// <summary>
        /// Controlar las rutas virtuales absolutas de la aplicación
        /// </summary>
        /// <param name="str">Texto a modificar</param>
        /// <param name="endWithSlash">Retornar el texto con un slash al final?</param>
        /// <returns>Texto controlado para evitar problemas en rutas absolutas</returns>
        public static string ToMyPath(this string str, bool endWithSlash = false)
        {
            // Si el texto debe terminar con "slash" y no es nulo
            if (endWithSlash && str != null)
                str = !str.EndsWith("/") ? str + "/" : str;

            // Si el texto es un "slash", asignar un texto vacío; sino, retornar el texto recibido
            str = str == "/" ? "" : str;

            return str;
        }


        /// <summary>
        /// Obtener la jerarquía de navegación de la sesión actual
        /// </summary>
        /// <param name="controllerName">Nombre del controlador actual</param>
        /// <param name="actionName">Nombre de la acción actual</param>
        /// <returns>Colección conteniendo la etiqueta y la URL correspondiente a cada rastro de navegación</returns>
        public static BreadcrumbsModel[] GetBreadcrumbHierarchy(string controllerName, string actionName)
        {
            var temp = (controllerName + "-" + actionName).ToLower();

            var gestion_humana = new BreadcrumbsModel { Label = "Dirección de Recursos Humanos", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/avisos/vActividadesGestionHumana" };

            switch ((controllerName + "-" + actionName).ToLower())
            {
                #region BLOQUE: MI PERFIL
                case "miperfil-index":  return new BreadcrumbsModel[] 
                {
                    new BreadcrumbsModel { Label = "Mi Perfil" }
                };
                #endregion

                #region BLOQUE: ACTUALIZACIÓN DE DATOS
                case "actualizardatos-index": return new BreadcrumbsModel[] 
                {
                    new BreadcrumbsModel { Label = "Actualización de Datos", URL = "" }
                };
                #endregion

                #region BLOQUE: SERVICIOS SOLICITADOS
                case "solicitados-solicitados":
                    return new BreadcrumbsModel[]
                {
                    new BreadcrumbsModel { Label = "Servicios Solicitados", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/solicitados/solicitados" }
                };
                #endregion

                #region BLOQUE: SERVICIOS ASIGNADOS
                case "asignados-asignados":
                    return new BreadcrumbsModel[] 
                {
                    new BreadcrumbsModel { Label = "Servicios Asignados", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/asignados/asignados"},
                };
                case "registrosolicitud-registrosolicitud":
                    return new BreadcrumbsModel[]
                    {
                        new BreadcrumbsModel { Label = "Servicios Asignados", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/asignados/asignados"},
                        new BreadcrumbsModel { Label = "Detalle de Solicitud", URL = "" }
                    };
                #endregion

                #region BLOQUE: SOLICITUD DE SERVICIOS
                case "helpdesk-solicitud": return new BreadcrumbsModel[] { new BreadcrumbsModel { Label = "Solicitud de Servicios" } };
                case "solicitudesservicios-crearservicio": return new BreadcrumbsModel[] { new BreadcrumbsModel { Label = "Solicitud de Servicios", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/SolicitudesServicios/CrearServicio" } };
                #endregion

                #region BLOQUE: FLOTAS Y EXTENSIONES
                case "asignacionflots-consulta": return new BreadcrumbsModel[] { new BreadcrumbsModel { Label = "Flotas y Extensiones" } };
                case "asignacionflots-flotasextensiones": return new BreadcrumbsModel[] { new BreadcrumbsModel { Label = "Flotas y Extensiones", URL = "" } };
                #endregion

                #region BLOQUE: GESTIÓN HUMANA
                case "avisos-vactividadesgestionhumana":
                    return new BreadcrumbsModel[] 
                    {
                        gestion_humana
                    };
                case "rh_empresacatalogo-index":
                    return new BreadcrumbsModel[]
                    {
                         gestion_humana
                        ,new BreadcrumbsModel { Label = "Catálogo de descuentos", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/Beneficios" }
                    };
                case "rh_empresacatalogo-empresas":
                    return new BreadcrumbsModel[]
                    {
                        gestion_humana
                       ,new BreadcrumbsModel { Label = "Empresas", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/Beneficios/Empresas" }
                    };
                case "albumgestionhumana-index":
                    return new BreadcrumbsModel[]
                    {
                        gestion_humana
                       ,new BreadcrumbsModel { Label = "Álbumes Fotográficos", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/AlbumGestionHumana" }
                    };
                case "rh_nuestroequipo-index":
                    return new BreadcrumbsModel[]
                    {
                        gestion_humana
                       ,new BreadcrumbsModel { Label = "Conoce Nuestro Equipo", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/RH_NuestroEquipo" }
                    };
                case "diasferiados-diasferiados":
                    return new BreadcrumbsModel[]
                    {
                        gestion_humana
                       ,new BreadcrumbsModel { Label = "Días Feriados", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/DiasFeriados/DiasFeriados" }
                    };
                case "documentogestionhumana-documentos":
                    return new BreadcrumbsModel[]
                    {
                        gestion_humana
                       ,new BreadcrumbsModel { Label = "Formularios de Gestión Humana", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/DocumentoGestionHumana/Documentos" }
                    };
                case "avisos-index":
                    return new BreadcrumbsModel[]
                    {
                        gestion_humana
                       ,new BreadcrumbsModel { Label = "Novedades", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/avisos/vActividadesGestionHumana" }
                    };
                case "avisos-vista":
                    return new BreadcrumbsModel[]
                    {
                        gestion_humana
                       ,new BreadcrumbsModel { Label = "Novedades", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/avisos/vista" }
                    };
                case "avisos-details":
                    return new BreadcrumbsModel[]
                    {
                        gestion_humana
                       ,new BreadcrumbsModel { Label = "Novedades", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/avisos/vActividadesGestionHumana" }
                       ,new BreadcrumbsModel { Label = "Detalle", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/avisos/vActividadesGestionHumana" }
                    };
                case "rh_sugerencia-index":
                    return new BreadcrumbsModel[]
                    {
                        new BreadcrumbsModel { Label = "Sugerencias", URL = HttpRuntime.AppDomainAppVirtualPath.ToMyPath() + "/RH_Sugerencia" }
                    };
                #endregion

                default:
                    return null;
            }
        }


        /// <summary>
        /// Modelo para almacenar la etiqueta del rastro de navegación y su respectivo URL
        /// </summary>
        public struct BreadcrumbsModel
        {
            /// <summary>
            /// Etiqueta del rastro de navegación
            /// </summary>
            public string Label { get; set; }
            /// <summary>
            /// URL del rastro de navegación
            /// </summary>
            public string URL { get; set; }
            /// <summary>
            /// Lista de roles permitidos para mostrar el link
            /// </summary>
            public string[] AllowedRoles { get; set; }
        }

    }
}