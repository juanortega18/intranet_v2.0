﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Code
{
    public partial class ChartData
    {
        public string name { get; set; }
        public int total { get; set; }
        public int pending { get; set; }
        public int closed { get; set; }
        public int canceled { get; set; }
        public int pending_evaluation { get; set; }
    } 
}