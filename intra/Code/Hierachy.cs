﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Code
{
    public class Hierachy
    {
        public Hierachy()
        {
            employees_info = new Dictionary<int, string>();
        }

        public string type { get; set; }
        public string name { get; set; }
        public int dependency_id { get; set; }
        public int department_id { get; set; }
        public int division_id { get; set; }
        public int section_id { get; set; }
        public Dictionary<int,string> employees_info { get; set; }
    }
}