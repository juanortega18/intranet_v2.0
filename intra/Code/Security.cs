﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections;
using System.Web.Security;
using System.Security.Principal;

namespace intra
{

    public class setAuthIntra
    {
        public  setAuthIntra(HttpContextBase httpContext, string usuario)
        {
            string Roles = "";
            using (dbIntranet dbi = new intra.dbIntranet())
            {
                //var ru = dbi.RolesUsuarios.Where(x => x.UserName == usuario).Select(x=> x.Rol).ToList();
                var ru = (from gu in dbi.GruposUsuarios
                            join r in dbi.Roles on gu.GrupoId equals r.GrupoId
                            join u in dbi.Usuarios on gu.UsuarioId equals u.UsuarioId
                            where  gu.SistemaId == 1 && u.UsuarioLogin == usuario
                            select r.RolNombre).ToList();
                
                Roles = string.Join("|", ru);
            }
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                                   1,                                     // ticket version
                                                   usuario,                              // authenticated username
                                                   DateTime.Now,                          // issueDate
                                                   DateTime.Now.AddMinutes(15),           // expiryDate
                                                   true,                          // true to persist across browser sessions
                                                   Roles,                              // can be used to store additional user data
                                                   FormsAuthentication.FormsCookiePath);  // the path for the cookie

            string encryptedTicket = FormsAuthentication.Encrypt(ticket);

            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            cookie.HttpOnly = true;

            httpContext.Response.Cookies.Add(cookie);

            /*
            IIdentity id = new FormsIdentity(ticket);

            string[] roles = ticket.UserData.Split(new char[] { '|' });
            IPrincipal principal = new GenericPrincipal(id, roles);

            httpContext.User = principal;
            */
        }
    }
   
}