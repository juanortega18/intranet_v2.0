﻿using intra.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;

namespace intra
{
    public class RegisterLogs
    {
        public Logs returned_log { get; set; }

        public RegisterLogs(string UserId, string Action)
        {
            string IP4Address = String.Empty;

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily == AddressFamily.InterNetwork)
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            try
            {
                if (UserId != null)
                {
                    using (dbIntranet db = new dbIntranet())
                    {
                        Logs logs = new Logs();

                        logs.LogIp = IP4Address;
                        logs.LogUsuario = UserId;
                        logs.LogFecha = DateTime.Now;
                        logs.LogAccion = Action;
                        logs.sistemaId = 1;
                        db.Logs.Add(logs);
                        db.SaveChanges();

                        returned_log = db.Logs.Where(m => m.LogId == logs.LogId).FirstOrDefault();

                    }
                }
            }

            catch (Exception ex)
            {
                ex.Message.ToString();
            }

        }
    }
}