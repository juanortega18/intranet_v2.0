﻿using intra.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using intra.Models.FlotasExtensiones;

namespace intra.Code
{
    public class Mail
    {
        public static string direction = "intranet@pgr.gob.do";
        public static string user = "Intranet";
        public static string password = "Cven7777";
        public static string host = "pgrpx03.pgr.local";

        public Mail()
        {

        }

        //To the in charge
        public void SendMail(string incharge_email, string assistant_email, string subject, string body, string date)
        {
            SmtpClient client = new SmtpClient(host, 25);
            MailMessage email = new MailMessage();

            email.From = new MailAddress(direction, user);
            email.IsBodyHtml = true;
            email.Body = body;
            email.Subject = subject;
            email.To.Add(incharge_email);
            email.CC.Add(assistant_email);

            // "gabriel.liberato@pgr.gob.do"

            //foreach (var item in pdf_reports)
            //{
            //    email.Attachments.Add(new Attachment(item));
            //}

            client.Credentials = new NetworkCredential(user, password);
            client.EnableSsl = false;
            client.Send(email);
        }

        //To the in charge
        public void EnviarCorreoPermisoVacacion(string correo, string subject, string body, string date, string Logo, string LogoDTI, string Flecha)
        {
            SmtpClient client = new SmtpClient(host, 25);
            MailMessage email = new MailMessage();

            email.From = new MailAddress(direction, user);
            email.IsBodyHtml = true;
            email.Subject = subject;
            email.CC.Add(correo);

            client.Credentials = new NetworkCredential(user, password);
            client.EnableSsl = false;

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, "text/html");

            LinkedResource inline1 = new LinkedResource(Logo);
            inline1.ContentId = "header";
            avHtml.LinkedResources.Add(inline1);

            LinkedResource inline2 = new LinkedResource(Flecha);
            inline2.ContentId = "footer";
            avHtml.LinkedResources.Add(inline2);

            LinkedResource inline3 = new LinkedResource(LogoDTI);
            inline3.ContentId = "bonton_servicio_asignado";
            avHtml.LinkedResources.Add(inline3);

            email.AlternateViews.Add(avHtml);

            client.Send(email);
        }

        public string ObtenerFormatoHTML(string departamento, string nombreCompleto, string titulo, string nombreArchivo, string link, string observacion)
        {
            string textoHTML = "";

            using (StreamReader sr = new StreamReader(nombreArchivo))
            {
                textoHTML = sr.ReadToEnd();
            }

            textoHTML = textoHTML.Replace("$Titulo", titulo);
            textoHTML = textoHTML.Replace("$NombreCompleto", nombreCompleto);
            textoHTML = textoHTML.Replace("$Departamento", departamento);
            textoHTML = textoHTML.Replace("$Observacion", observacion);
            textoHTML = textoHTML.Replace("$Link", link);

            return textoHTML;
        }

        public string ObtenerFormatoHTMLRespuesta(string departamento, string nombreEncargado, string tipoSolicitud, string estado, string nombreArchivo)
        {
            string textoHTML = "";

            using (StreamReader sr = new StreamReader(nombreArchivo))
            {
                textoHTML = sr.ReadToEnd();
            }

            string colorEstado = "#04B404";

            if (estado == "Cancelado")
            {
                colorEstado = "#DF0101";
                estado = "Rechazado";
            }

            textoHTML = textoHTML.Replace("$Titulo", tipoSolicitud);
            textoHTML = textoHTML.Replace("$NombreEncargado", nombreEncargado);
            textoHTML = textoHTML.Replace("$Departamento", departamento);
            textoHTML = textoHTML.Replace("$Estado", estado);
            textoHTML = textoHTML.Replace("$ColorEstado", colorEstado);

            if (estado == "Rechazado")
            {
                textoHTML = textoHTML.Replace("$Comentario", "Para saber las razones por las cuales se rechazó su solicitud debe de comunicarse con su encargado.");
            }
            else
            {
                textoHTML = textoHTML.Replace("$Comentario", "Ninguno.");
            }

            return textoHTML;
        }

        //To the employee
        public void SendMail(string employee_email, string subject, string body, string date)
        {
            if (employee_email != "@PGR.GOB.DO")
            {
                SmtpClient client = new SmtpClient(host, 25);
                MailMessage email = new MailMessage();

                email.From = new MailAddress(direction, user);
                email.IsBodyHtml = true;
                email.Body = body;
                email.Subject = subject;
                email.To.Add(employee_email);
                //email.Attachments.Add(new Attachment(pdf_report));

                client.Credentials = new NetworkCredential(user, password);
                client.EnableSsl = false;
                client.Send(email);
            }
        }

        public void EnviarPaseProduccion(string employee_email, string subject, string body, string date)
        {
           
                SmtpClient client = new SmtpClient(host, 25);
                MailMessage email = new MailMessage();

                email.From = new MailAddress(direction, user);
                email.IsBodyHtml = true;
                email.Body = body;
                email.Subject = subject;
                email.To.Add("maria.rodriguez@pgr.gob.do");
                //email.Attachments.Add(new Attachment(pdf_report));

                client.Credentials = new NetworkCredential(user, password);
                client.EnableSsl = false;
                client.Send(email);
        }


        public void SendFlotaMail(FlotaExtensionEmpleado flota, string html_file, string _case)
        {
            string file = "";

            dbIntranet db = new dbIntranet();

            var employee_info = db.VISTA_EMPLEADOS.Where(x => x.empleadoid == flota.FlotExtEmpEmpleadoId.ToString()).FirstOrDefault();

            using (StreamReader sr = new StreamReader(html_file))
            {
                file = sr.ReadToEnd();
            }

            file = file.Replace("$_case", _case);

            if(_case == "Creación de nueva flota".ToUpper() && string.IsNullOrEmpty(flota.NumeroFlotaViejo))
            {
                file = file.Replace("$_hide", "none");
            }else
            {
                file = file.Replace("$_hide", "block");
                file = file.Replace("$_oldTelephoneNumber", flota.NumeroFlotaViejo);
            }

            file = file.Replace("$_newTelephoneNumber", flota.FlotExtEmpFlota);
            file = file.Replace("$_employeeName", employee_info.nombre);
            file = file.Replace("$_employeeCode", flota.FlotExtEmpEmpleadoId.ToString());
            file = file.Replace("$_employeeDepartment", employee_info.departamento);
            file = file.Replace("$_employeeCharge", employee_info.cargo);
            file = file.Replace("$_flotaObjectCode", flota.FlotExtEmpId.ToString());

            string flotas_in_charge_mail = "";
            
            int flotas_in_charge_code = db.Sol_Actividades.Where(x => x.Descripcion == "FLOTAS").FirstOrDefault().SupervisorId;

            flotas_in_charge_mail = Utilities2.obtenerCorreoUsuario(db.Vw_Mostrar_Personal_Permisos_PGR.Where(x => x.Codigo == flotas_in_charge_code.ToString()).FirstOrDefault().Cedula);
            
            SmtpClient client = new SmtpClient(host, 25);
            MailMessage email = new MailMessage();

            email.From = new MailAddress(direction, user);
            email.IsBodyHtml = true;
            email.Body = file;
            email.Subject = _case;
            email.To.Add(flotas_in_charge_mail);
            
            client.Credentials = new NetworkCredential(user, password);
            client.EnableSsl = false;
            client.Send(email);
        }
    }
}