﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Code
{
    public class Vacacion
    {
        public DateTime FechaInicio { get; set; }
        public int Dias { get; set; }
        public List<Intervalo> IntervalosObservacion { get; set; }
    }
}