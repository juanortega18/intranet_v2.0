﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intra.Code
{
    public class Intervalo
    {
        public DateTime LimiteInferior { get; set; }
        public DateTime LimiteSuperior { get; set; }
        public int DiasCorrespondientes { get; set; }
        public int DiasTomados { get; set; }
        public int DiasAcumulados { get; set; }
        public int DiasAnio { get; set; }
    }
}