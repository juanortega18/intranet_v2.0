﻿
    $(document).ready(function () {
     
        $("#ProvinciaId").change(function () {
            var id = $("#ProvinciaId").val();
            $("#MunicipioId").prop('disabled', false);
            $.ajax({
                type: 'Post',
                url: 'SeleccionMunicipio',
                data: { provinciaId: id },
                success: function (data) {
                    // var municipio = data;
                    $("#MunicipioId").empty();
                    $("#MunicipioId").append('<option value>' + "Seleccionar" + '</option>')

                    $.each(data, function (i, municipio) {
                        $("#MunicipioId").append('<option value="' + municipio.MunicipioId + '">' + municipio.MunicipioDescripcion + '</option>')
                    })
                    $("#MunicipioId").trigger("chosen:updated");
                    
                   
                }
            });
        });

    });

//Distritos
$(document).ready(function () {

    $("#MunicipioId").change(function () {

        //function CargarMunicipios () {

        var id = $("#MunicipioId").val();
        $("#DistritoId").prop('disabled', false);
        $.ajax({
            type: 'Post',
            url: 'SeleccionarDistricto',
            data: { MunicipioId: id },
            success: function (data) {
                // var municipio = data;
                $("#DistritoId").empty();
                $("#DistritoId").append('<option value>' + "Seleccionar" + '</option>')

                $.each(data, function (i, distrito) {
                    $("#DistritoId").append('<option value="' + distrito.DistritoId + '">' + distrito.DistritoDescripcion + '</option>')
                })
                $("#DistritoId").trigger("chosen:updated");
                    

            }
        });
    });

});
//Ciudades
$(document).ready(function () {
    $("#DistritoId").change(function () {

        //function CargarMunicipios () {
        var id = $("#DistritoId").val();
        $("#CiudadId").prop('disabled', false);
        $.ajax({
            type: 'Post',
            url: 'SeleccionarCiudad',
            data: { DistritoId: id },
            success: function (data) {
                // var municipio = data;
                $("#CiudadId").empty();
                $("#CiudadId").append('<option value>' + "Seleccionar" + '</option>')

                $.each(data, function (i, ciudad) {
                    $("#CiudadId").append('<option value="' + ciudad.CiudadId + '">' + ciudad.CiudadDescripcion + '</option>')
                })
                $("#CiudadId").trigger("chosen:updated");
                    

            }
        });
    });

});


$(document).ready(function () {
    $("#CiudadId").change(function () {
        var id = $("#CiudadId").val();
        $("#SectorId").prop('disabled', false);
        $.ajax({
            type: 'Get',
            url:  'SeleccionarSector',
            data: { CiudaId: id },
            success: function (data) {
                $("#SectorId").empty();
                $("#SectorId").append('<option value>' + "Seleccionar" + '</option>')
                $.each(data, function (i, sector) {
                    $("#SectorId").append('<option value="' + sector.SectorId + '">' + sector.SectorDescripcion + '</option>')
                })
                $("#SectorId").trigger("chosen:updated");
                    

            }
        });
    });

});


