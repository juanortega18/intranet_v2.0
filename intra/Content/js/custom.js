// custom js

/*SHOW-HIDE JQUERY PLUGIN*/
jQuery(document).ready(function(){
	//menu mobile
	jQuery("#boton_menu").click(function(){
	  jQuery(".menu_movil").slideToggle();
	});
	//menu mobile, sub menu nivel 2
	jQuery(".menu_movil ul.menu > li.menu-item-has-children").click(function(){
	  jQuery(".menu_movil ul.menu > li.menu-item-has-children > ul.sub-menu").slideToggle();
	});

	//ENLACES SELECT
	jQuery('#ans2, #ans3, #ans4, #ans5, #ans6, #ans7, #ans8, #ans9, #ans10').hide();
	jQuery("#choices").bind("change", function () {
	    var answer = jQuery('#choices').val();
	    jQuery('#' + answer).fadeIn(500).siblings().fadeOut(0);
	});
	//END ENLACES SELECT
});