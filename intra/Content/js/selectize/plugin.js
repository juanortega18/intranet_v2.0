/**
 * Plugin: "restore_on_backspace" (selectize.js)
 * Copyright (c) 2013 Brian Reavis & contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *
 * @author Brian Reavis <brian@thirdroute.com>
 */

Selectize.define('clear_on_type', function (options) {
    var self = this;
    var IS_MAC = /Mac/.test(navigator.userAgent);
    var KEY_BACKSPACE = 8;

    this.onKeyDown = (function () {
        var original = self.onKeyDown;
        return function (e) {
            var originalCode = e.keyCode;
            if ((self.isFull() || self.isInputHidden) && !(IS_MAC ? e.metaKey : e.ctrlKey)) {
                if (self.isOpen && self.$activeOption) {
                    e.keyCode = KEY_BACKSPACE;
                    self.deleteSelection(e);
                    e.keyCode = originalCode;
                }
            }
            return original.apply(this, arguments);
        };
    })();
});
