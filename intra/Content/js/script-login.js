var VerticalCarousel = function(element){
  this.slideUp = function(){
    window.setInterval(function(){
      $(element+' .remove-next').remove();
      $(element+' .post-active').clone().insertAfter("ul li:last").toggleClass('post-active');
      $(element+' .post-active').toggleClass('post-active remove-next');
      $(element+' .active').toggleClass('active post-active').next().addClass('active');
    }, 3000);
  }
};

var c = new VerticalCarousel('#carousel');
c.slideUp();
