﻿toastr.options.progressBar = true;
toastr.options.extendedTimeOut = 3000;
    function showMessage(type, message) {
        setTimeout(function () {
            if (message != "") {
                switch (type) {
                    case "success":
                        toastr.success(message)
                        break;
                    case "info":
                        toastr.info(message)
                        break;
                    case "warning":
                        toastr.warning(message)
                        break;
                    case "error":
                        toastr.error(message)
                        break;

                    default:
                        toastr.success(message)
                }
            }

        }, 600);
    }