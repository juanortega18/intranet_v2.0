﻿
//Grafica Asignadas 1 //
 var grafAsign1 = {

     labels: [arrayDatosGraficaCategoria1],
     series: [
         {
             data: arrayDatosGrafica1
         }
     ]
 };
 var options = {
     chartPadding: {
         right: 50
     },
     height: 350,
     horizontalBars: true,
     reverseData: true,
     axisY: {
         offset: 135,
     },
 };
 $(document).ready(function () {
     new Chartist.Line('#Chart1', grafAsign1, options);

 });


//Grafica Asignadas 2 //
 var grafAsign2 = {

     labels: [arrayDatosGraficaCategoria2],
     series: [
         {
             data: arrayDatosGraficaData2
         }
     ]
 };
 var options = {
     chartPadding: {
         right: 50
     },
     height: 350,
     horizontalBars: true,
     reverseData: true,
     axisY: {
         offset: 135,
     },
 };
 $(document).ready(function () {
     new Chartist.Line('#Chart2', grafAsign1, options);

 });

//Grafica Gerencias 1 //

var grafGeren1 = {
    labels: [arrayDatosGraficaCategoria1],
    series: [
        {
            data: arrayDatosGrafica1
        }
    ]

};

var options = {
    chartPadding: {
        right: 50
    },
    height: 350,
    horizontalBars: true,
    reverseData: true,
    axisY: {
        offset: 135,
    },
};

 $(document).ready(function () {
     new Chartist.Bar('#Chart1', grafGeren1, options);

 });

////Grafica Gerencias 2 //
 var grafGeren2 = {
     labels: [arrayDatosGraficaCategoria2],
     series: [
         {
             data: arrayDatosGraficaData2
         }
     ]

 };
 var options = {
     chartPadding: {
         right: 50
     },
     height: 350,
     horizontalBars: true,
     reverseData: true,
     axisY: {
         offset: 135,
     },
 };

 $(document).ready(function () {
     new Chartist.Bar('#Chart2', grafGeren2, options);

 });
