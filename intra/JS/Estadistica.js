﻿$(document).ready(function () {
    $('#dropDownDept').change(function () {
        $.post(ruta + "/EstadisticaAvanzadas/EstadisticaFiltro/", {
            nombreVista: $("#nombreVista").val(), departamentoId: $("#dropDownDept").val(),
            annosId: $("#annos").val(), mesesId: $("#meses").val()
        }, function (data) {
            $("#contenidoEstadistica").html(data);
        })
    });

    $("#contenidoEstadistica").on("change", "#annos", function () {
        $.post(ruta + "/EstadisticaAvanzadas/EstadisticaFiltro/", {
            nombreVista: $("#nombreVista").val(), departamentoId: $("#dropDownDept").val(),
            annosId: $("#annos").val(), mesesId: $("#meses").val()
        }, function (data) {
            $("#contenidoEstadistica").html(data);
        })
    });
    $("#contenidoEstadistica").on("change", "#meses", function () {
        $.post(ruta + "/EstadisticaAvanzadas/EstadisticaFiltro/", {
            nombreVista: $("#nombreVista").val(), departamentoId: $("#dropDownDept").val(),
            annosId: $("#annos").val(), mesesId: $("#meses").val()
        }, function (data) {
            $("#contenidoEstadistica").html(data);
        })
    });

    // Reportes cantidades

    $('body').on("click", "#reportServUlt7Dias", function (e) {
        // alert(ruta + "/ReportesEstadisticos/reportServUlt7Dias/?departamentoId=" + $("#dropDownDept").val());
        location.href = ruta + "/ReportesEstadisticos/reportServUlt7Dias/?departamentoId=" + $("#dropDownDept").val();
        e.preventDefault();
    })

    $('body').on("click", "#reportTiempoRespuesta", function (e) {
        location.href = ruta + "/ReportesEstadisticos/reportTiempoRespuesta/?departamentoId=" + $("#dropDownDept").val();
        e.preventDefault();
    })

    $('body').on("click", "#reportSolicitudesXmes", function (e) {
        location.href = ruta + "/ReportesEstadisticos/reportSolicitudesXmes/?departamentoId=" + $("#dropDownDept").val() + "&mes=" + $("#meses").val() + "&anno=" + $("#annos").val();
        e.preventDefault();
    })

    $('body').on("click", "#reportTiempoRespuestaXmes", function (e) {
        location.href = ruta + "/ReportesEstadisticos/reportTiempoRespuestaXmes/?departamentoId=" + $("#dropDownDept").val() + "&mes=" + $("#meses").val() + "&anno=" + $("#annos").val();
        e.preventDefault();
    })

    // Reportes asignadas

    $('body').on("click", "#reportAsignaUltimaSemanaAbiertas", function (e) {
        // alert(ruta + "/ReportesEstadisticos/reportAsignaUltimaSemanaAbiertas/?departamentoId=" + $("#dropDownDept").val());
        location.href = ruta + "/ReportesEstadisticos/reportAsignaUltimaSemanaAbiertas/?departamentoId=" + $("#dropDownDept").val();
        e.preventDefault();
    })

    $('body').on("click", "#reportAsignaUltimaSemanaCerradas", function (e) {
        location.href = ruta + "/ReportesEstadisticos/reportAsignaUltimaSemanaCerradas/?departamentoId=" + $("#dropDownDept").val();
        e.preventDefault();
    })

    // Reportes Gerencia

    $('body').on("click", "#reportSolicitudRealizadasXGerencias", function (e) {
        location.href = ruta + "/ReportesEstadisticos/reportSolicitudRealizadasXGerencias/?departamentoId=" + $("#dropDownDept").val();
        e.preventDefault();
    })

    $('body').on("click", "#reportSolicitudesRealizadasXUsuarios", function (e) {
        location.href = ruta + "/ReportesEstadisticos/reportSolicitudesRealizadasXUsuarios/?departamentoId=" + $("#dropDownDept").val();
        e.preventDefault();
    })

    // Reportes ValorHoras

    $('body').on("click", "#reportServCantidadHoraUltimaSemana", function (e) {
        location.href = ruta + "/ReportesEstadisticos/reportServCantidadHoraUltimaSemana/?departamentoId=" + $("#dropDownDept").val();
        e.preventDefault();
    })

    $('body').on("click", "#reportTiempoExcedRespuesta", function (e) {
        location.href = ruta + "/ReportesEstadisticos/reportTiempoExcedRespuesta/?departamentoId=" + $("#dropDownDept").val();
        e.preventDefault();
    })

    $('body').on("click", "#reportServCantidadHoraMensual", function (e) {
        ruta + "/ReportesEstadisticos/reportServCantidadHoraMensual/?departamentoId=" + $("#dropDownDept").val() + "&mes=" + $("#meses").val() + "&anno=" + $("#annos").val();
        location.href = ruta + "/ReportesEstadisticos/reportServCantidadHoraMensual/?departamentoId=" + $("#dropDownDept").val() + "&mes=" + $("#meses").val() + "&anno=" + $("#annos").val();
        e.preventDefault();
    })

    $('body').on("click", "#reportTiempoExcedMensual", function (e) {
        location.href = ruta + "/ReportesEstadisticos/reportTiempoExcedMensual/?departamentoId=" + $("#dropDownDept").val() + "&mes=" + $("#meses").val() + "&anno=" + $("#annos").val();
        e.preventDefault();
    })

});