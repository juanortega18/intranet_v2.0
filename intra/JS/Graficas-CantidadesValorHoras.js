﻿//DESIGN 1
//// Grafica Cantidades 1 //
var grafCant1 = {
    labels: [arrayDatosGraficaCategoria1],
    series: [
        {
            data: arrayDatosGrafica1
        }
    ]
};
var options = {
    chartPadding: {
        right: 50
    },
    height: 350,
    axisY: {
        offset: 135,
    }
};

$(document).ready(function () {
    new Chartist.Bar('#Chart1', grafCant1, options);

});
//// Grafica Cantidades 2 //

var grafCant2 = {
    labels: [arrayDatosGraficaCategoria2],
    series: [
        {
            data: arrayDatosGraficaData2
        }
    ]
};
var options = {
    low: 0,
    showArea: true
};

$(document).ready(function () {
    new Chartist.Line('#Chart2', grafCant2, options);

});

// Grafica Cantidades 3//
var grafCant3 = {
    labels: [arrayDatosGraficaCategoria3],
    series: [
        {
            data: arrayDatosGrafica3
        }
    ]

};
var options = {
    chartPadding: {
        right: 50
    },
    height: 350,
    horizontalBars: true,
    reverseData: true,
    axisY: {
        offset: 135,
    },
};

$(document).ready(function () {
    new Chartist.Bar('#Chart3', grafCant3, options);

});
// Grafica Cantidades 4 //
var grafCant4 = {

    labels: [arrayDatosGraficaCategoria4],
    series: [
        {
            data: arrayDatosGrafica4
        }
    ]
};
var options = {
    chartPadding: {
        right: 50
    },
    height: 350,
    horizontalBars: true,
    reverseData: true,
    axisY: {
        offset: 135,
    },
};
$(document).ready(function () {
    new Chartist.Line('#Chart4', grafCant4, options);

});

////Grafica Valor/Horas 1 //
var grafVH1 = {
    labels: [arrayDatosGraficaCategoria1],
    series: [
     {
         data: arrayDatosGrafica1
     }


    ]
};
var options = {
    chartPadding: {
        right: 50
    },
    height: 350,
    horizontalBars: true,
    reverseData: true,
    axisY: {
        offset: 135,
    },
};

$(document).ready(function () {
    new Chartist.Bar('#Chart1', grafVH1, options);

});

//Grafica Valor/Horas 2 //
var grafVH2 = {
    labels:[arrayDatosGraficaCategoria2],
    series: [
     {
         data: arrayDatosGraficaData2
     }
    ]

};
var options = {
    chartPadding: {
        right: 50
    },
    height: 350,
    horizontalBars: true,
    reverseData: true,
    axisY: {
        offset: 135,
    },
};
$(document).ready(function () {
    new Chartist.Line('#Chart2', grafVH2, options);
});
//Grafica Valor/Horas 3 //

var grafVH3 = {
    labels:[arrayDatosGraficaCategoria3],
    series: [
     {
         data:arrayDatosGrafica3
     }
    ]

};
var options = {
    chartPadding: {
        right: 50
    },
    height: 350,
    horizontalBars: true,
    reverseData: true,
    axisY: {
        offset: 135,
    },
};
$(document).ready(function () {
    new Chartist.Line('#Chart3', grafVH3, options);
});

////Grafica Valor/Horas 4 //

var grafVH4 = {
    labels: [arrayDatosGraficaCategoria4],
    series: [
     {
         data: arrayDatosGrafica4
     }
    ]

};
var options = {
    chartPadding: {
        right: 50
    },
    height: 350,
    horizontalBars: true,
    reverseData: true,
    axisY: {
        offset: 135,
    },
};
$(document).ready(function () {
    new Chartist.Line('#Chart4', grafVH4, options);
});


