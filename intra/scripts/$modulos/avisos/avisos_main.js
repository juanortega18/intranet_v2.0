﻿
var link = $("masterlink").text();

var novedades_revisadas = [];

$('.date-picker').datepicker({
    dateFormat: 'dd/mm/yy'
});

$("#openCity-1").css("display", "block");

function openCity(evt, cityName) {
    var i, tabcontent, tablinks, id;

    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    id = cityName.substring(cityName.indexOf('-')+1);

    $(".tgl").removeClass("tgl-active");

    $(".tgl-" + id).addClass("tgl-active");

    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}


$("[class='carousel-cell']").on('click', function () {

    CargarAvisoDetalleModal($(this).attr("codigo"));

});

$(".btnCerrarModal").on('click', function () {

    $(".br-modal").modal('hide');

    $("body").css("padding-right", "0px");

});

function CargarAvisoDetalleModal(id) {

    AnimacionCarga.show();

    var contar_visita = false;

    if (!novedades_revisadas.includes(id)) {
    
        contar_visita = true;
        novedades_revisadas.push(id);

    }

    $.ajax({
        url: link + "/Avisos/CargarAviso",
        method: "POST",
        data: { AvisosId: id, Visita: contar_visita },
        success: function (result) {

            if (result == "error") {
            
                swal("Ha ocurrido un error interno", 'error');
            
            } else {

                //console.log(result);

                $('.DetalleModalTitulo').text(result.AvisosTitulo);
                $('.img-aviso-modal').attr('src', link + "/Content/Avisos/" + result.AvisosImagen);
                $('.DetalleModalArchivoDescargar').attr('href', link + "/Avisos/DownloadFile/" + result.AvisosId);
                $('.DetalleModalArchivoDescargar').attr('title', result.AvisosTitulo);
                $('.DetalleModalArchivoIcono').attr('src', result.AvisosIcono);
                $('.DetalleModalFechaCreacion').text(moment(result.AvisosFechaCreacion).format("DD/MM/YYYY"));
                $('.DetalleModalVisitas').text(result.AvisosVistas + " Visitas");
                $('.DetalleModalCuerpo').html($.parseHTML(decodeURI(result.AvisosCuerpo)));

                if (result.AvisosIcono) {
                    $(".OcultarDocumento").show();
                    $(".br-avisos-feed").attr('class', "col-lg-8 noticias br-avisos-feed br-avisos-feed-int");
                    $(".col-cerrar").attr('class', "col-md-1 col-cerrar");
                    $(".br-avisos-feed-doc").attr('class', "col-lg-2 br-avisos-feed-doc");
                    $(".br-avisos-feed-cross").attr('class', "br-avisos-feed-cross");

                } else {
                    $(".OcultarDocumento").hide();
                    $(".br-avisos-feed").attr('class', "noticias br-avisos-feed br-avisos-feed-int col-lg-10");
                    $(".col-cerrar").attr('class', "col-md-1 col-cerrar width-zero");
                    $(".br-avisos-feed-doc").attr('class', "br-avisos-feed-doc");
                    $(".br-avisos-feed-cross").attr('class', "br-avisos-feed-cross posicion-cerrar");
                }

                $('.DetalleModal').modal();

            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}