﻿
var tbl_sugerencias = $('.tbl_sugerencias').DataTable({
    "order": [],
    "language": idioma_esp,
    responsive: true
})

$(".tbl_sugerencias tbody").on('click', 'td.EditarEmpresa', function () {

    $(".image-holder2").html("");
    $(".EmpresaTituloModal").text("Editar Empresa");
    CargarEmpresaEditar($(this).parent().attr('codigo'));

});

$(".tbl_sugerencias tbody").on('click', 'button.CambiarEstadoEmpresa', function () {

    var EmpresaID = $(this).parent().parent().attr("codigo");

    CambiarEstadoEmpresa(EmpresaID, function (valor) {

    $('.tbl_sugerencias').dataTable().fnUpdate(
                            "<button class='button_white CambiarEstadoEmpresa'>" + (valor == "Activo" ? "Activo" : "Inactivo") + "</button>"
                            , '[codigo=' + EmpresaID + ']', 3, false);
    });

});

function CargarEmpresaEditar(EmpresaCatalogoId) {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/CargarEmpresa",
        method: "POST",
        data: { EmpresaCatalogoId : EmpresaCatalogoId },
        success: function (result) {

            $("#EmpresaFormModal")[0].reset();

            console.log(result);

            if (result.EmpresaLogo) {

                var image_prevista = $(".imagen-prevista-modalempresa");
                image_prevista.empty();

                $("<img />", {
                    "src": link + '/Content/images/GH_IMAGES/EmpresasLogo/' + result.EmpresaLogo,
                    "class": "thumb-image-size"
                }).appendTo(image_prevista);

                $(".image-holder-input-div-modalempresa").hide();
                $(".imagen-prevista-div-modalempresa").show();

            } else {

                $(".image-holder-input-div-modalempresa").show();
                $(".imagen-prevista-div-modalempresa").hide();

            }

            $("#EmpresaCatalogoId").val(result.EmpresaCatalogoId);
            $("#EmpresaNombre").val(result.EmpresaNombre);

            $.each(result.RH_CatalogoDescuentoTipo, function (index, element) {

                $(".descuentotipos#" + element)[0].checked = true;

            });

            $('.EmpresaModal').modal();

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function ActualizarLista() {

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/RemoverDescuentoCatalogo",
        method: "POST",
        data: { CatDescuentoId: id },
        success: function (result) {

            if (result == "removido") {

                // remover targeta el carousel // 

            } else if (result == "error") {

                swal("Ha ocurrido un error interno", 'error');

            } else {

                swal(result);
            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function ListarEmpresas(Empresas) {

    tbl_sugerencias.clear().draw();

    for (var r in Empresas) {

        var empresa = Empresas[r];

        tbl_sugerencias.row.add({
            0: "<img style='width: 100px;' class='btnEditarEmpresa' title="+ empresa.EmpresaNombre +" src='~/Content/images/GH_IMAGES/EmpresasLogo/" + empresa.EmpresaLogo + "' />",
            1: empresa.RH_CatalogoDescuentoTipo,
            2: moment(empresa.FechaCreacion).format("DD/MM/YYYY"),
            3: empresa.Estado
        }).draw(false);
    }
}