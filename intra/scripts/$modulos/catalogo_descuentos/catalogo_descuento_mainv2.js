﻿
var link = $("masterlink").text();

$('.date-picker').datepicker({
    dateFormat: 'dd/mm/yy'
});

$("#openCity-1").css("display", "block");

//$("#btn-1").addClass("active");
//$(".tgl-1").show();

//"openCity-item.CatDescuentoTipoId"
//function openElement(div) {
//    // $('#comprobanteButtonID').removeClass('tablinks');
//    $('#btngenerales').addClass(' active');
//    $('#btnServicios').addClass(' active');
//    $('#btnBandeja').addClass(' active');
//    $('#btndocumentos').addClass(' active');
//    $('#btngenerales').addClass(' active');
//    $("#generales").css("display", "block");
//}

function openCity(evt, cityName) {
    var i, tabcontent, tablinks, id;

    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    id = cityName.substring(cityName.indexOf('-')+1);

    $(".tgl").removeClass("tgl-active");

    $(".tgl-" + id).addClass("tgl-active");

    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

//$(document).ready(function () {
//    $('.tblSolicitados,.tblAsignados').DataTable().order([[0, 'desc']]).draw();
//    $("#btnServicios").on("click", { opcion: "solicitados", tabla : tablaSolicitados, servicioEstadoId : 1 }, cargarServiciosRepatriados);
//    $("#btnBandeja").on("click", { opcion: "asignados", tabla: tablaAsignados, servicioEstadoId : 1 }, cargarServiciosRepatriados);
//    $("#btnConsultar").on("click", {
//        opcion: "asignados",
//        tabla: tablaAsignados,
//        servicioEstadoId: $(".estado").val(),
//        codigoUsuario: $(".usuarioCreador").val(),
//        codigoResponsable: $(".codigoResponsable").val(),
//        repatriado: $(".repatriadosSeleccionado").val(),
//        tipoServicioId: $(".tiposervicio").val(),
//        institucionId: $(".institucion").val(),
//        fechainicio_string: $(".fecha_inicio").val(),
//        fechafinal_string: $(".fecha_final").val()
//    } , cargarServiciosRepatriados);
//    CargarServicioModal = function (id) {
//        $(".RegistroServicioModal").modal();
//        //$.ajax({
//        //    url: link + "/ServiciosRepatriados/CargarServicioRepatraido",
//        //    method: 'POST',
//        //    dataType: 'json',
//        //    data: { id: id },
//        //    success: function (ServicioRepatriado, status, xhr) {
//        //        $(".ServicioIdEditModal").val(ServicioRepatriado.ServicioId);
//        //        $(".PersonaRepatriadaModal").val(ServicioRepatriado.SSR_Repatriados.NombreCompleto);
//        //        $("#ServicioEstadoIdModal").val(ServicioRepatriado.ServicioEstadoId);
//        //        $(".Notas").val(ServicioRepatriado.Notas);
//        //        $(".conclusion").val(ServicioRepatriado.Conclusion);
//        //        @if (User.IsInRole("SRA"))
//        //        {
//        //            <text>
//        //            $("#ServicioTipoIdModal").val(ServicioRepatriado.ServicioTipoId);
//        //            $("#InstitucionIdModal").val(ServicioRepatriado.InstitucionId);
//        //            $(".FechaFinModal").val(moment(ServicioRepatriado.FechaFin).format("DD/MM/YYYY"));
//        //            $(".CodigoCreadorEditModal").val(ServicioRepatriado.CodigoCreador);
//        //            $("#CodigoResponsableModal").val(ServicioRepatriado.CodigoResponsable);
//        //            $("#CodigoResponsableModal").trigger('chosen:updated');
//        //            </text>
//        //        } else
//        //        {
//        //            <text>
//        //            $(".ServicioTipoModal").val(ServicioRepatriado.SSR_ServiciosTipos.Descripcion);
//        //            $(".InstitucionModal").val(ServicioRepatriado.SSR_Instituciones.Descripcion);
//        //            </text>
//        //        }
//        //    },
//        //    error: function (jqXhr, textStatus, errorMessage) {
//        //        swal('Un Error a ocurrido', '', 'error');
//        //        AnimacionCarga.hide();
//        //    }
//        //});
//    }
//});

var tbl_tipo_descuento = $('.tbl_tipo_descuento').DataTable({
    "ordering": false,
    "language": idioma_esp,
    "pageLength": 5,
    "lengthMenu": [5, 25, 50, 75, 100],
    responsive: true
});

var tbl_descuento = $('.tbl_descuento').DataTable({
    "ordering": false,
    "language": idioma_esp,
    "pageLength": 3,
    "lengthMenu": [3, 15, 30, 60],
    responsive: true
});

$(".tbl_tipo_descuento tbody").on('click', 'button.btnActivarTipoDescuento', function () {

    ActivarTipoDescuento($(this).parent().parent().attr("codigo"), RefrescarPagina);

});

$(".tbl_tipo_descuento tbody").on('click', 'button.btnRemoverCatalogoTipoModal', function () {

    RemoverCatalogoTipo($(this).parent().parent().attr('codigo'));

});

$(".tbl_descuento tbody").on('click', 'button.btnActivarDescuentoEstado', function () {

    ActivarDescuentoEmpresa($(this).attr("codigo"));

});

$(".tbl_descuento tbody").on('click', 'button.btnRemoverDescuentoModal', function () {

    RemoverDescuento($(this).attr("codigo"), function () { $(".btnCerrarModal").trigger("click"); });

});

$(".btnAbrirCrearCatalogoTipoModal").on('click', function () {

    $("#CatalogoTipoFormModal")[0].reset();

    $(".CatalogoTipoTituloModal").text("Nuevo Catálogo de Descuentos");

    $(".image-holder").empty();
    $("#CatDescuentoTipoId").val("");
    $('.CatalagoTipoModal').modal();

    $(".image-holder-input-div-modal").show();
    $(".imagen-prevista-div-modal").hide();

});

$("[class='carousel-cell']").on('click', function () {

    CargarDescuentoDetalleModal($(this).attr("codigo"));

    //$('.EmpresaDescuentoDetalleModal').modal();


});

$(".btnEditarCatalogoTipo").on('click', function () {

    $("#CatalogoTipoFormModal")[0].reset();
    $(".CatalogoTipoTituloModal").text("Editar Catálogo de Beneficios");
    $(".image-holder").empty();
    $("#CatDescuentoTipoId").val($(this).attr("codigo"));
    CargarDescuentoTipo($(this).attr("codigo"));
});

$(".btnEditarDescuentoEmpresa").on('click', function () {

    EditarDescuentoEmpresa($(this).attr("codigo"));

});

$(".btnRemoverEmpresaDescuento").on('click', function () {

    RemoverDescuento($("#CatDescuentoId_detalle").val());
    
});

$(".btnRemoverCatalogoTipo").on('click', function () {

    RemoverCatalogoTipo($(this).attr("codigo"));

});

//$(".btnActivarTipoDescuento").on('click', function () {

//    ActivarTipoDescuento($(this).parent().parent().attr("codigo"), function () { RefrescarPagina(); });

//});

//$(".btnAgregarCatalogoTipo").on('click', function () { console.log(($("#CatDescuentoTipoId").val() != "")); });

$(".btnAgregarEmpresa").on('click', funcionAgregarEmpresa);

$(".btnAgregarEmpresaDescuento").on('click', CrearDescuentoCatalogo);

$(".btnActualizarDescuentoEmpresa").on('click', ActualizarDescuentoEmpresa);

$(".btnAbrirCrearEmpresaModal").on('click', function () {

    $("#EmpresaFormModal")[0].reset();
    $("#EmpresaCatalogoId").val("");
    $(".image-holder2").html("");
    $(".EmpresaTituloModal").text("Agregar Empresa");

    $(".image-holder-input-div-modalempresa").show();
    $(".imagen-prevista-div-modalempresa").hide();

    if ($(this).attr("codigo")) { $(".descuentotipos#" + $(this).attr("codigo"))[0].checked = true; }

    //$("#CatDescuentoTipoId").val($(this).attr("codigo"));

    //CargarDescuentoTipos();

    $('.EmpresaModal').modal();

});

$(".btnAbrirCrearDescuentoEmpresaModal").on('click', function () {

    $("#EmpresaDescuentoFormModal")[0].reset();
    $(".EmpresaDescuentoTituloModal").text("Agregar Beneficio al Catálogo");

    var CatDescuentoTipoId = $(this).attr("codigo");

    $("#CatDescuentoTipoId_descuento_modal").val(CatDescuentoTipoId);

    CargarListaEmpresaModal(CatDescuentoTipoId);

    $('.EmpresaDescuentoModal').modal();

    $("#EmpresaDetalleModal")[0].reset();
    $(".jqte").jqteVal("");

    $("#EmpresaCatalogoIdModal, .btnAgregarEmpresaDescuento").show();
    $("#EmpresaCatalogo_mostrar, .btnActualizarDescuentoEmpresa, .btnVolverDescuentoDetalle").hide();

});

$(".btnAbrirDescuentoListaModal").on('click', function () {

    CargarDescuentosModal($(this).attr("codigo"));

    $('.DescuentoListaModal').modal();

});

$(".btnAbrirDescuentoTipoListaModal").on('click', function () {

    $('.DescuentoTipoListaModal').modal();

});

$(".btnEditarEmpresaDescuentoModal").on('click', function (){

    var CatDescuentoId = $("#CatDescuentoId_detalle").val();

    var CatDescuentoTipoId = $("#CatDescuentoId_detalle").val();

    $(".EmpresaDescuentoTituloModal").text("Editar Beneficio al Catálogo");

    $(".EmpresaDescuentoDetalleModal").modal('hide');

    CargarDescuentoEditarModal(CatDescuentoId);

    //CargarListaEmpresaModal(CatDescuentoTipoId);

    $('.EmpresaDescuentoModal').modal();

    $("#EmpresaCatalogoIdModal, .btnAgregarEmpresaDescuento").hide();
    $("#EmpresaCatalogo_mostrar, .btnActualizarDescuentoEmpresa, .btnVolverDescuentoDetalle").show();

    //$("#EmpresaCatalogoIdModal option[value='7']").prop('selected', true);
    //document.getElementById("EmpresaCatalogoIdModal").selectedIndex = "2";
    //$('#EmpresaCatalogoIdModal').val(7);

});

$(".btnCerrarModal").on('click', function () {

    $(".br-modal").modal('hide');

    $("body").css("padding-right", "0px");

});

$(".btnVolverDescuentoDetalle").on('click', function () {

    $(".EmpresaDescuentoModal").modal('hide');
    $(".EmpresaDescuentoDetalleModal").modal();

});

//$(".btnEmpresaDescuentoCerrar").on('click', function () {

//    $(".EmpresaDescuentoModal").modal('hide');

//});

$(".switch-image-holder").on('click', function () {

    $(".image-holder-input-div-modal").show();
    $(".imagen-prevista-div-modal").hide();

});

$(".switch-image-holder-modalempresa").on('click', function () {

    $(".image-holder-input-div-modalempresa").show();
    $(".imagen-prevista-div-modalempresa").hide();

});

function CrearCatalogoTipo() {

    var formInput = document.getElementById('image-holder-input');
    var CatDescuentoDescripcion = $("#CatDescuentoDescripcion").val();
    //var CatInformacionAdicional = $("#CatInformacionAdicional").val();

    //FormData Formulario
    //var formulario = $("#CatalogoTipoFormModal").serialize();
    var formData = new FormData();
    var archivo = formInput.files[0];

    formData.append('file', archivo);
    formData.append('CatDescuentoDescripcion', CatDescuentoDescripcion);
    //formData.append('CatInformacionAdicional', CatInformacionAdicional);

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/CrearCatalogoTipo",
        method: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (result) {

            if (result == "guardado") {


                $(".CatalagoTipoModal").modal('hide');

                swal_fn("Guardado exitosamente", 'success', function () { location.reload(); });

            } else if (result == "error") {

                swal("Ha ocurrido un error interno", 'error');

            } else {

                swal(result);
            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error",'error');
            }
    });

}

function CrearEmpresa(funcion) {

    var formInput = document.getElementById('image-holder-input2');
    var EmpresaNombre = $("#EmpresaNombre").val();
    var CatDescuentoTipoId = $("#CatDescuentoTipoId").val();

    var DescuentoTipos = new Array();

    $('input[name="descuentotipos"]:checked').each(function () {
        DescuentoTipos.push($(this).attr("id"));
    });

    //FormData Formulario
    //var formulario = $("#EmpresaFormModal").serialize();
    var formData = new FormData();
    var archivo = formInput.files[0];

    formData.append('file', archivo);
    formData.append('EmpresaNombre', EmpresaNombre);
    formData.append('CatDescuentoTipoId', CatDescuentoTipoId);
    formData.append('descuentotipos', DescuentoTipos)

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/CrearCatalogoEmpresa",
        method: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (result) {

            if (result == "guardado") {

                $(".EmpresaModal").modal('hide');

                if (funcion) {

                    swal_fn("Guardado exitosamente", 'success', funcion);

                } else {

                    swal('', "Guardado exitosamente", 'success');
                }

                $("#EmpresaFormModal")[0].reset();

            } else if (result == "error") {

                swal("Ha ocurrido un error interno", 'error');

            } else {

                swal(result);
            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function EditarEmpresa(funcion) {

    var formInput = document.getElementById('image-holder-input2');

    var DescuentoTipos = new Array();

    $('input[name="descuentotipos"]:checked').each(function () {
        DescuentoTipos.push($(this).attr("id"));
    });

    //FormData Formulario
    //var formulario = $("#EmpresaFormModal").serialize();
    var formData = new FormData();

    formData.append('EmpresaCatalogoId', $("#EmpresaCatalogoId").val());
    formData.append('file', formInput.files[0]);
    formData.append('EmpresaNombre', $("#EmpresaNombre").val());
    formData.append('descuentotipos', DescuentoTipos)

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/ActualizarCatalogoEmpresa",
        method: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (result) {

            if (result == "actualizado") {

                $(".EmpresaModal").modal('hide');

                if (funcion) {

                    swal_fn("Guardado exitosamente", 'success', funcion);

                } else {

                    swal('', "Guardado exitosamente", 'success');
                }

                $("#EmpresaFormModal")[0].reset();

            } else if (result == "error") {

                swal("Ha ocurrido un error interno", 'error');

            } else {

                swal(result);
            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function CambiarEstadoEmpresaviejo(EmpresaCatalogoId, funcion) {

    swal_confirm('Seguro(a) de que quiere remover esta empresa?', 'info', 'Si, Remover', function () {

        AnimacionCarga.show();

        $.ajax({
            url: link + "/RH_EmpresaCatalogo/CambiarEstadoEmpresa",
            method: "POST",
            data: { EmpresaCatalogoId: EmpresaCatalogoId },
            success: function (result) {

                if (result == "Activo" || result == "Inactivo") {

                    swal('', "Empresa " + ((result == "Activo") ? "activada" : "inactivada") + " exitosamente", 'success');

                    if (funcion) { funcion(result); };

                } else {

                    swal(result);
                }

                AnimacionCarga.hide();
            },
            error: function (ex) {
                console.log(ex);
                AnimacionCarga.hide();
                swal("Ha ocurrido un error", 'error');
            }
        });

    })

}

function CambiarEstadoEmpresa(id, funcion) {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/CambiarEstadoEmpresa",
        method: "POST",
        data: { EmpresaCatalogoId: id },
        success: function (result) {

            if (result == "error") {

                swal("Ha ocurrido un error interno", 'error');

            } else {

                AnimacionCarga.hide();

                console.log((result > 1));

                var mensaje = (result > 0) ?
                    'Esta empresa tiene ' + result + ' beneficio' + ((result > 1) ? 's' : '') + ' activo' + ((result > 1) ? 's' : '') + ', desea continuar?' :
                    'Seguro(a) de que quiere remover esta empresa?';

                swal_confirm(mensaje, 'info', 'Si, Remover',

                function () {

                    AnimacionCarga.show();

                    $.ajax({
                        url: link + "/RH_EmpresaCatalogo/CambiarEstadoEmpresaConfirmado",
                        method: "POST",
                        data: { EmpresaCatalogoId: id },
                        success: function (result) {

                            if (result == "Activo" || result == "Inactivo") {

                                swal('', "Empresa " + ((result == "Activo") ? "activada" : "inactivada") + " exitosamente", 'success');

                                if (funcion) { funcion(result); };

                            } else {

                                swal(result);
                            }

                            AnimacionCarga.hide();
                        },
                        error: function (ex) {
                            console.log(ex);
                            AnimacionCarga.hide();
                            swal("Ha ocurrido un error", 'error');
                        }
                    });
                });
            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });
}

function CrearDescuentoCatalogo() {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/CrearDescuentoEmpresa",
        method: "POST",
        data: $("#EmpresaDescuentoFormModal").serialize(),
        success: function (result) {

            if (result == "guardado") {

                $(".EmpresaDescuentoModal").modal('hide');

                swal_fn("Guardado exitosamente", 'success', function () { location.reload(); });

            } else if (result == "error") {

                swal('',"Ha ocurrido un error interno", 'error');

            } else if (result == "fecha invalidas") {

                swal('',"Formato de fecha inválida", 'error');

            } else if (result == "fecha expirada") {

                swal('', "La fecha de expiración no puede ser menor a la actual", 'error');

            } else if (result == "empresa invalida") {

                swal('', "Seleccione una Empresa", 'info');

            } else {

                swal('', result, 'info');
            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function EditarCatalogoTipo(id) {

    var formInput = document.getElementById('image-holder-input');
    var CatDescuentoTipoId = $("#CatDescuentoTipoId").val();
    var CatDescuentoDescripcion = $("#CatDescuentoDescripcion").val();
    var CatInformacionAdicional = $("#CatInformacionAdicional").val();

    //FormData Formulario
    //var formulario = $("#CatalogoTipoFormModal").serialize();
    var formData = new FormData();
    var archivo = formInput.files[0];

    formData.append('file', archivo);
    formData.append('CatDescuentoTipoId', CatDescuentoTipoId);
    formData.append('CatDescuentoDescripcion', CatDescuentoDescripcion);
    formData.append('CatInformacionAdicional', CatInformacionAdicional);

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/ActualizarDescuentoTipo",
        method: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (result) {

            if (result == "actualizado") {

                $(".CatalagoTipoModal").modal('hide');

                swal_fn("Guardado exitosamente", 'success', function () { location.reload(); });

            } else if (result == "error") {

                swal("Ha ocurrido un error interno", 'error');

            } else {

                swal(result);
            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function ActivarDescuentoEmpresa(id) {

    swal_confirm('Seguro(a) de que quiere reactivar este beneficio?', 'info', 'Si', function () {

        AnimacionCarga.show();

        $.ajax({
            url: link + "/RH_EmpresaCatalogo/ActivarDescuentoCatalogo",
            method: "POST",
            data: { CatDescuentoId: id },
            success: function (result) {

                if (result == "activo") {

                    swal_fn("Activado exitosamente", 'success', RefrescarPagina);

                } else if (result == "repetido") {

                    swal('', "Existe un beneficio con esta empresa en esta catálogo", 'warning');

                } else if (result == "error") {

                    swal("Ha ocurrido un error interno", 'error');

                } else {

                    swal(result);
                }

                AnimacionCarga.hide();
            },
            error: function (ex) {
                console.log(ex);
                AnimacionCarga.hide();
                swal("Ha ocurrido un error", 'error');
            }
        });

    })

}

function RemoverDescuento(id, funcion) {

    swal_confirm('Seguro(a) de que quiere remover este beneficio?', 'info', 'Si, Remover', function () {

        AnimacionCarga.show();

        $.ajax({
            url: link + "/RH_EmpresaCatalogo/RemoverDescuentoCatalogo",
            method: "POST",
            data: { CatDescuentoId: id },
            success: function (result) {

                if (result == "removido") {

                    //remover targeta el carousel // 

                    $carousel.flickity('remove', $("[codigo='" + id + "']"));

                    $(".EmpresaDescuentoDetalleModal").modal('hide');

                    if (funcion) {
                        swal_fn("Removido exitosamente", 'success', funcion);
                    } else {
                        swal('', "Removido exitosamente", 'success');
                    }

                } else if (result == "error") {

                    swal("Ha ocurrido un error interno", 'error');

                } else {

                    swal(result);
                }

                AnimacionCarga.hide();
            },
            error: function (ex) {
                console.log(ex);
                AnimacionCarga.hide();
                swal("Ha ocurrido un error", 'error');
            }
        });

    })

}

function RemoverCatalogoTipo(id) {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/RemoverCatalogoTipo",
        method: "POST",
        data: { CatDescuentoTipoId: id },
        success: function (result) {

            if (result == "error") {

                swal("Ha ocurrido un error interno", 'error');

            } else {

                AnimacionCarga.hide();

                var mensaje = (result.descuentos > 0 || result.empresas > 0) ? 
                    'Esta catálogo contiene ' + ((result.descuentos > 0) ? result.descuentos + ' beneficios activos' : '') +
                    ((result.descuentos > 0 && result.empresas > 0) ? ' y ' : '') +
                    ((result.empresas > 0) ? result.empresas + ' empresas asignadas' : '') + ', desea continuar?' :
                    'Seguro(a) de que quiere remover esta catálogo?';

                swal_confirm(mensaje, 'info', 'Si, Remover',

                    function () {

                        AnimacionCarga.show();

                        $.ajax({
                            url: link + "/RH_EmpresaCatalogo/RemoverCatalogoTipoConfirmado",
                            method: "POST",
                            data: { CatDescuentoTipoId: id },
                            success: function (result) {

                                if (result == "removido") {

                                    swal_fn("Removido exitosamente", 'success', function () { location.reload(); });

                                } else if (result == "error") {

                                    swal("Ha ocurrido un error interno", 'error');

                                } else {

                                    swal(result);
                                }

                                AnimacionCarga.hide();
                            },
                            error: function (ex) {
                                console.log(ex);
                                AnimacionCarga.hide();
                                swal("Ha ocurrido un error", 'error');
                            }
                        });
                    });
            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

    //swal_confirm('Seguro(a) de que quiere remover esta sección?', 'info', 'Si, Remover', function () {
    //    AnimacionCarga.show();
    //    $.ajax({
    //        url: link + "/RH_EmpresaCatalogo/RemoverCatalogoTipo",
    //        method: "POST",
    //        data: { CatDescuentoTipoId: id },
    //        success: function (result) {
    //            if (result == "removido") {
    //                swal_fn("Removido exitosamente", 'success', function () { location.reload(); });
    //            } else if (result == "error") {
    //                swal("Ha ocurrido un error interno", 'error');
    //            } else {
    //                var mensaje = 'Seguro(a) de que quiere remover esta sección?';
    //                swal_confirm('Esta sección contiene ' + result.descuentos + ' descuentos activos y ' + result.empresas + ' empresas asignadas, desea continuar?', 'info', 'Si, Remover',
    //                    function () {
    //                        $.ajax({
    //                            url: link + "/RH_EmpresaCatalogo/RemoverCatalogoTipoConfirmado",
    //                            method: "POST",
    //                            data: { CatDescuentoTipoId: id },
    //                            success: function (result) {
    //                                if (result == "removido") {
    //                                    swal_fn("Removido exitosamente", 'success', function () { location.reload(); });
    //                                } else if (result == "error") {
    //                                    swal("Ha ocurrido un error interno", 'error');
    //                                } else {
    //                                    swal(result);
    //                                }
    //                                AnimacionCarga.hide();
    //                            },
    //                            error: function (ex) {
    //                                console.log(ex);
    //                                AnimacionCarga.hide();
    //                                swal("Ha ocurrido un error", 'error');
    //                            }
    //                        });
    //                    });
    //                swal(result);
    //            }
    //            AnimacionCarga.hide();
    //        },
    //        error: function (ex) {
    //            console.log(ex);
    //            AnimacionCarga.hide();
    //            swal("Ha ocurrido un error", 'error');
    //        }
    //    });
    //})

}

function ActivarTipoDescuento(CatDescuentoTipoId, funcion) {

    swal_confirm('Seguro(a) de que quiere reactivar esta catálogo?', 'info', 'Si', function () {

        AnimacionCarga.show();

        $.ajax({
            url: link + "/RH_EmpresaCatalogo/ActivarTipoDescuento",
            method: "POST",
            data: { CatDescuentoTipoId: CatDescuentoTipoId },
            success: function (result) {

                if (result == "activo") {

                    swal_fn("Catálogo activada exitosamente", 'success', funcion);

                } else {

                    swal(result);
                }

                AnimacionCarga.hide();
            },
            error: function (ex) {
                console.log(ex);
                AnimacionCarga.hide();
                swal("Ha ocurrido un error", 'error');
            }
        });

    })

}

function ActualizarDescuentoEmpresa() {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/ActualizarDescuentoCatalogo",
        method: "POST",
        data: $("#EmpresaDescuentoFormModal").serialize(),
        success: function (result) {

            console.log(result);

            if (result.success) {

                $(".card-text-" + result.registro.CatDescuentoId).html(result.registro.CatInformacionSucursal);

                $(".EmpresaDescuentoModal").modal('hide');

                CargarDescuentoDetalleModal(result.registro.CatDescuentoId);

                swal('', 'Beneficio actualizado con exito', 'success')

            } else if (result == "error editar") {

                swal('', "Ha ocurrido un error interno", 'error');

            } else if (result == "La información de encabezado no puede ser mayor que 150 caracteres") {

                swal('', "Ha ocurrido un error interno", 'error');

            } else if (result == "fecha invalidas") {

                swal('', "Formato de fecha inválida", 'error');

            } else if (result == "fecha expirada") {

                swal('', "La fecha de expiración no puede ser menor a la actual", 'error');

            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });
}

function CargarListaEmpresaModal(id) {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/CargarEmpresas",
        method: "POST",
        data: { CatDescuentoTipoId: id },
        success: function (ListaEmpresas) {

            $('#EmpresaCatalogoIdModal').html("");
            $('#EmpresaCatalogoIdModal').append('<option value=0>SELECCIONE</option>');

            if (ListaEmpresas.length > 0) {

                $.each(ListaEmpresas, function (i, empresa) {
                    $('#EmpresaCatalogoIdModal').append('<option value="' + empresa.Value + '">' + empresa.Text + '</option>');
                });

            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });


}

function CargarDescuentoDetalleModal(id) {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/CargarDescuento",
        method: "POST",
        data: { CatDescuentoId: id },
        success: function (result) {

            //console.log(result);

            $("#EmpresaDetalleModal")[0].reset();

            $('.empresa_detalle').attr('src', link + '/Content/images/GH_IMAGES/EmpresasLogo/' + (result.EmpresaLogo == null ? 'beneficios-pgr.svg' : result.EmpresaLogo));
            $('#CatDescuentoId_detalle').val(result.CatDescuentoId);
            $('#FechaExpiracion_detalle').val(result.FechaExpiracion);
            $('#CatInformacionSucursal_detalle').val(result.CatInformacionSucursal);

            //var html = $.parseHTML(result.CatDescripcion);

            $('.contenido_detalle').html(result.CatDescripcion);

            $('.EmpresaDescuentoDetalleModal').modal();

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function CargarDescuentoEditarModal(id) {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/CargarDescuento",
        method: "POST",
        data: { CatDescuentoId: id },
        success: function (result) {

            console.log(result);

            $("#EmpresaDescuentoFormModal")[0].reset();

            //CargarListaEmpresaModal(result.CatDescuentoTipoId);

            $('#CatDescuentoId').val(result.CatDescuentoId);
            $('#FechaExpiracion').val(result.FechaExpiracion);
            $('#CatInformacionSucursal').val(result.CatInformacionSucursal);
            $("#EmpresaCatalogo_mostrar").val(result.EmpresaNombre);

            $(".jqte").jqteVal(result.CatDescripcion);

            //var html = $.parseHTML(result.CatDescripcion);

            //$('#__contenido').html($.parseHTML(decodeURI(result.CatDescripcion)));

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function CargarDescuentoTipo(CatDescuentoTipoId) {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/CargarDescuentoTipo",
        data: { CatDescuentoTipoId: CatDescuentoTipoId },
        method: "POST",
        success: function (DescuentoTipo) {

            console.log(DescuentoTipo);

            if (DescuentoTipo.DescuentoTipoIcono) {

                var image_prevista = $(".imagen-prevista");
                image_prevista.empty();

                $("<img />", {
                    "src": link + '/Content/images/GH_IMAGES/CatalogoTipoIconos/' + DescuentoTipo.DescuentoTipoIcono,
                    "class": "thumb-image-size"
                }).appendTo(image_prevista);

                $(".image-holder-input-div-modal").hide();
                $(".imagen-prevista-div-modal").show();

            } else {

                $(".image-holder-input-div-modal").show();
                $(".imagen-prevista-div-modal").hide();

            }

            $("#CatDescuentoTipoId").val(CatDescuentoTipoId)
            $("#CatDescuentoDescripcion").val(DescuentoTipo.CatDescuentoDescripcion);
            $("#CatInformacionAdicional").val(DescuentoTipo.CatInformacionAdicional);

            $('.CatalagoTipoModal').modal();

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function CargarDescuentosModal(CatDescuentoTipoId) {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_EmpresaCatalogo/CargarDescuentos",
        method: "POST",
        data: { CatDescuentoTipoId: CatDescuentoTipoId },
        success: function (ListaDescuentos) {

            console.log(ListaDescuentos);

            ListarDescuentos(ListaDescuentos)

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function ListarDescuentos(Descuentos) {

    tbl_descuento.clear().draw();

    for (var r in Descuentos) {

        var descuento = Descuentos[r];

        tbl_descuento.row.add({
            0: descuento.Empresa,
            1: moment(descuento.FhCreacion).format("DD/MM/YYYY"),
            2: moment(descuento.FhExpiracion).format("DD/MM/YYYY"),
            3: (descuento.Estado) ? '<button class="button_white btnRemoverDescuentoModal" codigo=' + descuento.CatDescuentoId + '>Activo</button>'
                : '<button class="button_white btnActivarDescuentoEstado" codigo=' + descuento.CatDescuentoId + '>Inactivo</button>',
            4: descuento.Encabezado,
            5: descuento.Descripcion
        }).draw(false);
    }
}