﻿var link = $("masterlink").text();

$(document).ready(function () {

    $(".chosen").chosen();

    $('.date-picker').datepicker({
        dateFormat: 'dd/mm/yy'
    });

    //$("#btnServicios").click(cargarRepatriados);

    $('.CrearServicioModal').on('click', function () {
        cargarRepatriados();
        $('.ServicioModal').modal();
    });

    $(".CerrarModal").click(function () {

        $(".ServicioModal,.RegistroServicioModal").modal('hide');

    });

    $(".GuardarServicioModal").on('click', function () {

        AnimacionCarga.show();

        $.ajax({
            url: link + "/ServiciosRepatriados/CrearModal",
            method: 'POST',
            data: $("#crear_servicio").serialize(),
            dataType: 'json',
            success: function (resultado, status, xhr) {

                if (resultado == "Guardado") {

                    $('.ServicioModal').modal('hide');

                    cargarServiciosRepatriados({ data: { opcion: "solicitados", tabla: tablaSolicitados, servicioEstadoId: 1 } });

                    success("Servicio registrado");

                } else if (resultado == "Error") {
                    AnimacionCarga.hide();
                    error("Ha ocurrido un error con la solicitud, por favor refresque la página");

                } else if (resultado == "ErrorDeCorreo") {
                    AnimacionCarga.hide();
                    alert("Se ha guardado la solicitud pero hubo un error al enviar el correo, por favor verificar con el responsable");

                } else if (resultado.length > 0) {

                    var errores = "";

                    $.each(resultado, function (index, value) {
                        errores = errores + '<li class="swal-li">' + value + '</li>'
                    });

                    alert('<ul>' + errores + '</ul>');
                }

            },
            error: function (jqXhr, textStatus, errorMessage) {
                swal('Un Error a ocurrido', '', 'error');
                AnimacionCarga.hide();

            }
        });

    });

    $(".ActualizarServicioModal").on('click', function () {

        if ($(".conclusion").val() == "" && $("#ServicioEstadoIdModal").val() == 2) {

            alert("Conclusión vacia");

        } else {

            AnimacionCarga.show();

            $.ajax({
                url: link + "/ServiciosRepatriados/ActualizarServicioModal",
                method: 'POST',
                data: $("#editar_servicio_modal").serialize(),
                dataType: 'json',
                success: function (resultado, status, xhr) {

                    if (resultado == "Actualizado") {

                        $('.RegistroServicioModal').modal('hide');

                        cargarServiciosRepatriados({ data: { opcion: $(".TablaSeleccionada").val(), tabla: ($(".TablaSeleccionada").val() == "asignados") ? tablaAsignados : tablaSolicitados, servicioEstadoId: 1 } });

                        success("Servicio actualizado");

                    } else {
                        swal('Un Error a ocurrido', '', 'error');
                    }

                },
                error: function (jqXhr, textStatus, errorMessage) {
                    swal('Un Error a ocurrido', '', 'error');
                    AnimacionCarga.hide();

                }
            });
        }

    });

    $(".AparecerFiltroSolicitados").on("click", { div: $(".accordion-solicitados") }, FiltroSlider);

    $(".AparecerFiltroAsigandos").on("click", { div: $(".accordion-asignados") }, FiltroSlider);

    var repatriado = $("#IdRepatriados_input");

    repatriado.on('keypress', function (e) {

        keys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        return keys.indexOf(event.key) > -1
    })

    repatriado.textext({
        plugins: 'tags autocomplete filter'
    })
       
    repatriado.bind('getSuggestions', function (e, data) {

        $.ajax({
            url: link + '/EntradaTallerPgr/GetEntrada',
            data: { activofijo: data.query },
            dataType: 'json',
            cacheResults: false,
            success: function (result) {
                repatriado.textext()[0].tags()._formData.forEach(function (element) {
                    var index = result.indexOf(element);
                    if (index > -1) {
                        result.splice(index, 1);
                    }
                });
                   
                textext = $(e.target).textext()[0],
                    query = (data ? data.query : '') || '';
                repatriado.trigger(
                    'setSuggestions',
                    { result: textext.itemManager().filter(result, query) }
                );

            }

        });

    });

    $(".btnGenerarSalida").click(function () {
        repatriado.textext()[0].tags()._formData
        var seleccionados = repatriado.textext()[0].tags()._formData;
        $("#activosfijosval").val(seleccionados.toString());
    });

    function FiltroSlider(event) {
        event.data.div.slideToggle();
    }

    function cargarRepatriados() {

        AnimacionCarga.show();

        $.ajax({
            url: link + "/ServiciosRepatriados/CargarRepatriados",
            method: 'POST',
            dataType: 'json',
            success: function (ListaRepatriados, status, xhr) {

                $("#IdRepatriados").html("");
                //console.log(ListaRepatriados);
                $("#IdRepatriados").append('<option value>SELECCIONE LA PERSONA REPATRIADA</option>');
                $.each(ListaRepatriados, function (i, repatriado) {
                    $("#IdRepatriados").append('<option value="' + repatriado.Value + '">' + repatriado.Text + '</option>');
                });
                $("#IdRepatriados").trigger('chosen:updated');
                AnimacionCarga.hide();

            },
            error: function (jqXhr, textStatus, errorMessage) {
                swal('Un Error a ocurrido', '', 'error');
                AnimacionCarga.hide();
            }
        });
    }

});

$("#CrearServicio").click(function () {

    //$("#crear_servicio #RepatriadoId").val("");
    //$("#crear_servicio #RepatriadoId").trigger("chosen:updated");

    AnimacionCarga.show();

    $.ajax({
        url: link + "/ServiciosRepatriados/Crear",
        method: 'POST',
        data: $("#crear_servicio").serialize(),
        dataType: 'json',
        success: function (resultado, status, xhr) {

            if (resultado == "Guardado") {

                success("Servicio registrado");

                //window.location = link + "/ServiciosRepatriados";

            } else if (resultado == "Error") {
                AnimacionCarga.hide();
                error("Ha ocurrido un error con la solicitud, por favor refresque la página");

            } else if (resultado == "ErrorDeCorreo") {
                AnimacionCarga.hide();
                alert("Se ha guardado la solicitud pero hubo un error al enviar el correo, por favor verificar con el responsable");

            } else if (resultado.length > 0) {

                var errores = "";

                $.each(resultado, function (index, value) {
                    errores = errores + '<li class="swal-li">' + value + '</li>'
                });

                alert('<ul>' + errores + '</ul>');
            }

        },
        error: function (jqXhr, textStatus, errorMessage) {
            swal('Un Error a ocurrido', '', 'error');
            AnimacionCarga.hide();

        }
    });

});

$(".EditarServicio").click(function () {

    if (isEmptyOrSpaces($("#conclusion").val()) && $("#ServicioEstadoId").val() == 2) {

        alert("Conclusión vacia");

    } else {

        EditarServicio();
    }

});

function EditarServicio() {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/ServiciosRepatriados/RegistroServicio",
        type: 'Post',
        data: $('#editar_servicio').serialize(),
        success: function (data) {
            successRedirect(data);
        },
        error: function (data) { console.log(data); error("Ha ocurrido un error"); }

    });

}
function GetUsuarioCreadores() {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/ServiciosRepatriados/GetUsuarioCreadores",
        type: 'Post',
        success: function (data) {
            console.log(data);
            AnimacionCarga.hide();
        },
        error: function (data) { console.log(data); error("Ha ocurrido un error"); }

    });

}
function success(mensaje) {
    swal({
        title: mensaje,
        type: 'success',
        confirmButtonText: 'OK'
    }).then(function () {
        //location.href = link + "/ServiciosRepatriados";
        $("#ServicioTipoId,#IdRepatriados,#InstitucionId,#Notas").val("");
        $("#IdRepatriados").trigger("chosen:updated");
    })
}
function successRedirect(mensaje) {
    AnimacionCarga.hide();
    swal({
        title: mensaje,
        type: 'success',
        confirmButtonText: 'OK'
    })
    .then(function () {
        location.href = link + "/SistemaRepatriados";
    })
}
function error(mensaje) {
    AnimacionCarga.hide();
    swal('Error', mensaje, 'error');
}
function alert(mensaje) {
    AnimacionCarga.hide();
    swal('Por favor validar', mensaje, 'warning');
}
function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}