﻿
var link = $("masterlink").text();

$(document).ready(function () {
    var $select = $('.selectize').selectize({
        plugins: ['clear_on_type']
   , placeholder: "SELECCIONE"
   , maxOptions: 30000
    });

    var control = $select[0].selectize;

    //$('#nombre-selectized').attr('autocomplete', 'nope');

});

$(".btnAbrirEditarNuestroEquipoModal").on('click', function () {

    $('.EditarNuestroEquipoModal').modal();

});

$(".btnAgregarDepartamentoModal").on('click', function () {

    $('.DepartamentoTituloModal').text('AGREGAR DEPARTAMENTO');

    $('.btnAgregarDepartamento').show();
    $('.btnActualizarDepartamento').hide();

    $('.NuestroEquipo_Descripcion').val("");

    $('.DepartamentoModal').modal();

});

$(".btnEditarDepartamentoModal").on('click', function () {

    $('.DepartamentoTituloModal').text('EDITAR EL DEPARTAMENTO');

    $('.btnActualizarDepartamento').show();
    $('.btnAgregarDepartamento').hide();

    $('#NuestroEquipoDepartamentoID').val($(this).attr("departamentoID"));

    $('.DepartamentoModal').modal();

    CargarDepartamento($(this).attr("departamentoID"));

});

$(".btnAbrirAgregarMiembroModal").on('click', function () {

    $('.MiembroTituloModal').text('AGREGAR MIEMBRO AL DEPARTAMENTO');

    $('.div_nombre_miembro').hide();
    $('.select_nombre_miembro').show();

    $('.btnAgregarMiembro').show();
    $('.btnActualizarMiembro').hide();

    $("#Ext").val("");
    $("#NuestroEquipo_DepartamentoID").val($(this).attr("departamentoID"));

    $('.MiembroModal').modal();

});

$(".btnEditarMiembroModal").on('click', function () {

    $('.MiembroTituloModal').text('EDITAR MIEMBRO DEL DEPARTAMENTO');

    $('.div_nombre_miembro').show();
    $('.select_nombre_miembro').hide();

    $('.btnAgregarMiembro').hide();
    $('.btnActualizarMiembro').show();

    $("#Nombre").val($(".miembro_empleado_" + $(this).attr("div_Miembro")).text());

    $("#Ext").val($("#miembro_ext_" + $(this).attr("div_Miembro")).text());
    $("#flota").val($("#miembro_flota_" + $(this).attr("div_Miembro")).text());

    $("#MiembroID").val($(this).attr("div_Miembro"));

    $('.MiembroModal').modal();

});

$(".btnActualizarNuestroEquipo").on('click', ActualizarNuestroEquipo);

$(".btnAgregarDepartamento").on('click', AgregarDepartamento);

$(".btnActualizarDepartamento").on('click', ActualizarDepartamento);

$(".btnAgregarMiembro").on('click', AgregarMiembro);

$(".btnActualizarMiembro").on('click', ActualizarMiembro);

$(".btnRemoverDepartamentoModal").click(function () {

    CambiarEstadoDepartamento($(this).attr("departamentoID"));

});

$(".btnRemoverMiembroModal").click(function () {

    CambiarEstadoMiembro($(this).attr("div_Miembro"));

});

$(".btnCerrarModal").on('click', function () {

    $(".br-modal").modal('hide');

    $("body").css("padding-right", "0px");

});

function CambiarEstadoDepartamento(DepartamentoId) {

    swal_confirm('Seguro(a) de que quiere remover esta departamento?', 'info', 'Si, Remover', function () {

        AnimacionCarga.show();

        $.ajax({
            url: link + "/RH_NuestroEquipo/CambiarEstadoDepartamento",
            method: "POST",
            data: { DepartamentoId: DepartamentoId },
            success: function (result) {

                if (result == "Activo" || result == "Inactivo") {

                    swal_fn("Departamento " + ((result == "Activo") ? "activada" : "inactivada") + " exitosamente", 'success', function () {

                        AnimacionCarga.show();
                        RefrescarPagina();

                    });

                } else {

                    swal(result);
                }

                AnimacionCarga.hide();
            },
            error: function (ex) {
                console.log(ex);
                AnimacionCarga.hide();
                swal("Ha ocurrido un error", 'error');
            }
        });

    })

}

function CambiarEstadoMiembro(MiembroID) {

    swal_confirm('Seguro(a) de que quiere remover esta miembro?', 'info', 'Si, Remover', function () {

        AnimacionCarga.show();

        $.ajax({
            url: link + "/RH_NuestroEquipo/CambiarEstadoMiembro",
            method: "POST",
            data: { MiembroID: MiembroID },
            success: function (result) {

                if (result == "Activo" || result == "Inactivo") {

                    swal_fn("Miembro " + ((result == "Activo") ? "activada" : "inactivada") + " exitosamente", 'success', function () {

                        AnimacionCarga.show();
                        RefrescarPagina();

                    });

                } else {

                    swal(result);
                }

                AnimacionCarga.hide();
            },
            error: function (ex) {
                console.log(ex);
                AnimacionCarga.hide();
                swal("Ha ocurrido un error", 'error');
            }
        });

    })

}

function ActualizarNuestroEquipo() {

    var formInput = document.getElementById('image-holder-input2');
    var NuestroEquipoDescripcion = $("#NuestroEquipoDescripcion").val();

    //FormData Formulario
    //var formulario = $("#NuestroEquipoFormModal").serialize();
    var formData = new FormData();
    var archivo = formInput.files[0];

    formData.append('file', archivo);
    formData.append('NuestroEquipoDescripcion', NuestroEquipoDescripcion);

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_NuestroEquipo/ActualizarNuestroEquipo",
        method: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (result) {

            console.log(result);

            if (result == "Actualizado") {

                $(".btnCerrarModal").trigger("click");

                swal_fn('Actualizado exitosamente', 'success', function () {

                    AnimacionCarga.show();
                    RefrescarPagina();

                });

            } else  {

                swal('', "Ha ocurrido un error interno", 'error');

            } 

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function AgregarDepartamento() {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_NuestroEquipo/AgregarDepartamento",
        method: "POST",
        data: $("#DepartamentoFormModal").serialize(),
        success: function (result) {

            if (result == "Actualizado") {

                $(".btnCerrarModal").trigger("click");

                swal_fn('Actualizado exitosamente', 'success', function () {

                    AnimacionCarga.show();
                    RefrescarPagina();

                });

            } else if (result == "campo vacio") {

                swal('', "Favor de ingresar el nombre de la descripción", 'warning');

            } else if (result == "duplicado") {

                swal('', "Esta departamento ya fue registrado", 'warning');

            } else {

                swal('', "Ha ocurrido un error interno", 'error');

            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function ActualizarDepartamento() {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_NuestroEquipo/ActualizarDepartamento",
        method: "POST",
        data: $("#DepartamentoFormModal").serialize(),
        success: function (result) {

            if (result.success == "Actualizado") {

                $(".btnCerrarModal").trigger("click");

                $(".Descripcion_departamento_" + $("#NuestroEquipoDepartamentoID").val()).text(result.nombre_nuevo);

                swal('', "Actualizado exitosamente", 'success');

            } else if (result.success == "intercambiado") {

                swal_fn('Actualizado exitosamente', 'success', function () {

                    AnimacionCarga.show();
                    RefrescarPagina();

                });

            } else if (result == "campo vacio") {

                swal('', "Favor de ingresar el nombre de la descripción", 'warning');

            } else if (result == "duplicado") {

                swal('', "Esta departamento ya fue registrado", 'warning');

            } else {

                swal('', "Ha ocurrido un error interno", 'error');

            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function AgregarMiembro() {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_NuestroEquipo/AgregarMiembro",
        method: "POST",
        data: $("#MiembroFormModal").serialize(),
        success: function (result) {

            if (result == "Actualizado") {

                $(".btnCerrarModal").trigger("click");

                swal_fn('Actualizado exitosamente', 'success', function () {

                    AnimacionCarga.show();
                    RefrescarPagina();

                });

            } else if (result == "campo vacio") {

                swal('', "Favor de ingresar el empleado a registrar", 'warning');

            } else if (result == "duplicado") {

                swal('', "Este empleado ya fue registrado en este departamento", 'warning');

            } else {

                swal('', "Ha ocurrido un error interno", 'error');

            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function ActualizarMiembro() {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_NuestroEquipo/ActualizarMiembro",
        method: "POST",
        data: $("#MiembroFormModal").serialize(),
        success: function (result) {

            if (result == "Actualizado") {

                $(".btnCerrarModal").trigger("click");

                $('#miembro_ext_' + $('#MiembroID').val()).text($('#Ext').val());
                $('#miembro_flota_' + $('#MiembroID').val()).text($('#flota').val());

                swal('', 'Actualizado exitosamente', 'success');

            } else if (result == "campo vacio") {

                swal('', "Favor de ingresar el empleado a registrar", 'warning');

            } else if (result == "duplicado") {

                swal('', "Este empleado ya fue registrado en este departamento", 'warning');

            } else {

                swal('', "Ha ocurrido un error interno", 'error');

            }

            AnimacionCarga.hide();
        },
        error: function (ex) {
            console.log(ex);
            AnimacionCarga.hide();
            swal("Ha ocurrido un error", 'error');
        }
    });

}

function CargarDepartamento(NuestroEquipoDepartamentoID) {

    $(".NuestroEquipo_Descripcion").val($(".Descripcion_departamento_" + NuestroEquipoDepartamentoID).text());

}

function GetTecnico(id) {

    $.ajax({
        url: link + '/TipoSolicitudes/GetEmpleado',
        data: { codigoEmpleado: id },
        dataType: 'json',
        method: 'POST',
        success: function (data, status, xhr) {
            console.log(data);
            $("#Nombre").val(data.displayName);
            $("#Correo").val(data.samAccountName + "@@pgr.gob.do");
        },
        error: function (jqXhr, textStatus, errorMessage) {
            console.log('Error' + errorMessage);
        }
    });

}

$("#EmpleadoID").on("blur", function () {
    GetTecnico($("#EmpleadoID").val());
});