﻿var link = $("masterlink").text();

$(".btnSugerenciaModal").click(function () {
    $(".SugerenciaModal").modal();
});

function EnviarSugerencia() {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/RH_Sugerencia/Agregar",
        method: 'POST',
        dataType: 'json',
        data: $(".FormularioSugerencia").serialize(),
        success: function (data, status, xhr) {

            console.log(data);

            if (data == 'guardado') {

                $(".FormularioSugerencia")[0].reset();
                swal('', 'Su sugerencia fue enviada exitosamente', 'success');
                OcultarModal();

            } else if (data == 'error') {

                swal('Error', 'Ha ocurrido un error', 'error');
            
            } else {

                var errores = "";

                $.each(data, function (index, value) {
                    errores = errores + '<li class="swal-li">' + value + '</li>'
                });

                swal('Por favor', errores, 'warning');
            }
            AnimacionCarga.hide();
        },
        error: function (jqXhr, textStatus, errorMessage) {
            swal('Error', 'Ha ocurrido un error', 'error');
            AnimacionCarga.hide();
        }
    });

}

function OcultarModal() {

    $(".SugerenciaModal").modal('hide');

}