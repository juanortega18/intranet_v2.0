﻿var link = $("masterlink").text();

$('.date-picker').datepicker({
    dateFormat: 'dd/mm/yy'
});

$('.tbl_sugerencias tbody').on('click', '.aprobar_sugerencia, .rechazar_sugerencia, .reevaluar_sugerencia', function () {

    //var decision = $(this).is('.aprobar_sugerencia') ? 2 : 3;
    var decision = $(this).is('.aprobar_sugerencia') ? 2 : ( $(this).is('.rechazar_sugerencia') ? 3 : 1 );

    console.log($(this).attr('codigo') + " " + decision);

    tbl_EvaluarSugerencia($(this).attr('codigo'), decision, $(this));

    //var row = tbl_sugerencias.row($(this).parents('tr').prev('tr'));
    //var rowNode = row.node();
    //row.remove().draw(false);
});

$('.btn_aprobar_sugerencia, .btn_rechazar_sugerencia').on('click', function () {

    var decision = $(this).is('.btn_aprobar_sugerencia') ? 2 : 3;

    tbl_EvaluarSugerencia($(this).attr('codigo'), decision);

});

$('.VerSugerencias').on('click', function () {

    CargarSugerencias();

});

$("#SwitchSugenrencias").click(function () {

    var Switch = $(this);

    Switch.val(Switch.val() == 0 ? 1 : 0);

    if (Switch.val() == 1) {

        CargarSugerencias(0, 0, 0);

    }
    else {

        CargarSugerencias(0, 0, 1);

    }

});

$(".sugerecia_filtro").click(function () {

    $(".sugerencia_filtro_container").slideToggle();

});

//$(".rechasar_sugerencia").click(function () {
//    EvaluarSugerencia($(this).attr('codigo'), 3);
//});

$(".sugerencia_filtrar").click(function () {

    if (ValidarAmbasFechas($(".FechaInicial").val(), $(".FechaFinal").val())) {

        CargarSugerencias(
        ($("#MotivoID").val() == '' ? 0 : $("#MotivoID").val()),
        ($("#RegionID").val() == '' ? 0 : $("#RegionID").val()), 0,
        $(".FechaInicial").val(),
        $(".FechaFinal").val()
        );

    }

});

$(".sugerencia_pdf").click(function () {

    if (ValidarAmbasFechas($(".FechaInicial").val(), $(".FechaFinal").val())) {

        var motivo = $("#MotivoID").val() == '' ? 0 : $("#MotivoID").val();
        var region = $("#RegionID").val() == '' ? 0 : $("#RegionID").val();

        window.open(link + "/RH_Sugerencia/ReporteSugerenciasPDF/?MotivoID=" + motivo +
        "&RegionID=" + region + "&fechainicio_string=" + $('.FechaInicial').val() +
        "&fechafinal_string=" + $('.FechaFinal').val() + "&tipo_reporte=" + $('.ReporteTipo').val());

    }

});

$(".sugerencia_excel").click(function () {

    if (ValidarAmbasFechas($(".FechaInicial").val(), $(".FechaFinal").val())) {

        var motivo = $("#MotivoID").val() == '' ? 0 : $("#MotivoID").val();
        var region = $("#RegionID").val() == '' ? 0 : $("#RegionID").val();

        window.location = link + "/RH_Sugerencia/ReporteSugerenciasExcel/?MotivoID=" + motivo +
        "&RegionID=" + region + "&fechainicio_string=" + $('.FechaInicial').val() +
        "&fechafinal_string=" + $('.FechaFinal').val() + "&tipo_reporte=" + $('.ReporteTipo').val();

    }

});

function tbl_EvaluarSugerencia(id, valor, fila) {

    $("#loading-animation-container").show();

    $.ajax({
        url: link + "/RH_Sugerencia/EvaluarSugerencia",
        method: 'POST',
        dataType: 'json',
        data: {
            SugerenciaID: id,
            Decision: valor
        },
        success: function (data, status, xhr) {

            if (data == "guardado") {


                var mensaje = (valor == 2) ? 'Sugerencia aprobada y publicada' : ((valor == 3) ? 'Sugerencia rechazada' : 'Sugerencia movida a reevaluacion');

                if (fila) {

                    tbl_EliminarSugerencia(fila);

                    swal('', mensaje, 'success');

                } else {

                    swal_fn(mensaje, 'success', function () {

                        window.location.href = link + "/RH_Sugerencia";

                    });

                }


            } else if (data == "error") {

                swal('', 'Un Error a ocurrido', 'error');

            } else {


                if (fila) {

                    tbl_EliminarSugerencia(fila);

                    swal('', 'Actualizado');

                } else {

                    swal_fn('Actualizado', 'success', function () {

                        window.location.href = link + "/RH_Sugerencia";

                    });
                }

            }

            $("#loading-animation-container").hide();

        },
        error: function (jqXhr, textStatus, errorMessage) {
            swal('', 'Un Error a ocurrido', 'error');
            $("#loading-animation-container").hide();
        }
    });

}

function EvaluarSugerencia(id, valor) {

    $("#loading-animation-container").show();

    $.ajax({
        url: link + "/RH_Sugerencia/EvaluarSugerencia",
        method: 'POST',
        dataType: 'json',
        data: {
            SugerenciaID: id,
            Decision: valor
        },
        success: function (data, status, xhr) {

            if (data == "guardado") {

                tbl_EliminarSugerencia(fila);

                swal('', (valor == 2) ? 'Sugerencia aprobada y publicada' : ((valor == 3) ? 'Sugerencia rechazada' : 'Sugerencia movida a reevaluacion'), 'success');

            } else if (data == "error") {

                swal('', 'Un Error a ocurrido', 'error');

            } else {

                tbl_EliminarSugerencia(fila);

                swal('', 'Actualizado');

            }

            $("#loading-animation-container").hide();

        },
        error: function (jqXhr, textStatus, errorMessage) {
            swal('', 'Un Error a ocurrido', 'error');
            $("#loading-animation-container").hide();
        }
    });

}

function CargarSugerencias(MotivoID, RegionID, SugerenciaEstadoID, fechainicio_string, fechafinal_string) {

    $("#loading-animation-container").show();

    var data = {
        MotivoID: MotivoID,
        RegionID: RegionID,
        SugerenciaEstadoID: SugerenciaEstadoID,
        fechainicio_string: fechainicio_string,
        fechafinal_string: fechafinal_string
    }

    console.log(data);

    $.ajax({
        url: link + "/RH_Sugerencia/CargarSugerencias",
        method: 'POST',
        dataType: 'json',
        data: {
            MotivoID: MotivoID,
            RegionID: RegionID,
            SugerenciaEstadoID: SugerenciaEstadoID,
            fechainicio_string: fechainicio_string,
            fechafinal_string: fechafinal_string
        },
        success: function (data, status, xhr) {

            console.log(data);

            if (data == 'fecha final invalida') {
                swal('', 'La fecha final no puede ser menor que la fecha inicial', 'error');
            } else if (data == 'fecha invalidas') {
                swal('', 'Las fechas ingresadas no son válidas', 'error');
            } else {
                ListarSugenrencias(data);
            }

            $("#loading-animation-container").hide();

        },
        error: function (jqXhr, textStatus, errorMessage) {
            swal('', 'Un Error a ocurrido', 'error');
            $("#loading-animation-container").hide();
        }
    });

}

function ListarSugenrencias(Sugerencias) {

    tbl_sugerencias.clear().draw();

    for (var r in Sugerencias) {

        var sugerencia = Sugerencias[r];

        tbl_sugerencias.row.add({
            0: sugerencia.Region_Descripcion,
            1: sugerencia.Motivo_Descripcion,
            //2: sugerencia.Estado_Descripcion,
            2: moment(sugerencia.FechaCreacion).format("DD/MM/YYYY"),
            3: sugerencia.Descripcion,
            //5: get_btn(sugerencia.SugerenciaEstadoID, sugerencia.SugerenciaID)
            //5: '<a class="button_white aprobar_sugerencia" codigo=' + sugerencia.SugerenciaID + '>Aprobar</a><a class="button_white rechasar_sugerencia" codigo=' + sugerencia.SugerenciaID + '>Rechazar</a>'
        }).draw();

    }

}

function tbl_EliminarSugerencia(fila) {

    var row = tbl_sugerencias.row(fila.parents('tr').prev('tr'));
    var rowNode = row.node();
    row.remove().draw(false);

}

function get_btn(estado, id) {

    switch (estado) {

        case 1:

            var aprobar = btn_aprobar(id);
            var rechazar = btn_rechazar(id);

            return aprobar + rechazar;

            break;
        case 2:

            var reevaluacion = btn_reevaluacion(id);
            var rechazar = btn_rechazar(id);

            return rechazar + reevaluacion;
            break;
        case 3:

            var reevaluacion = btn_reevaluacion(id);

            return reevaluacion;
            break;

    }
}

function btn_aprobar(id){

    return '<a class="button_white aprobar_sugerencia" codigo=' + id + '>Aprobar</a>';
}

function btn_rechazar(id) {

    return '<a class="button_white rechazar_sugerencia" codigo=' + id + '>Rechazar</a>';
}

function btn_reevaluacion(id) {

    return '<a class="button_white reevaluar_sugerencia" codigo=' + id + '>Reevaluar</a>';
}