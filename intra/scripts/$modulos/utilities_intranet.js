﻿
$(".image-holder-input").on('change', function () {

    //Get count of selected files
    var countFiles = $(this)[0].files.length;

    var imgPath = $(this)[0].value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    var image_holder = $(".image-holder");
    image_holder.empty();

    if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg" || extn == "svg") {
        if (typeof (FileReader) != "undefined") {

            //loop for each file selected for uploaded.
            for (var i = 0; i < countFiles; i++) {

                var reader = new FileReader();
                reader.onload = function (e) {
                    $("<img />", {
                        "src": e.target.result,
                        "class": "thumb-image thumb-image-size"
                    }).appendTo(image_holder);
                }

                image_holder.show();
                reader.readAsDataURL($(this)[0].files[i]);
            }

        } else {
            alert("This browser does not support FileReader.");
        }
    } else {
        alert("Pls select only images");
    }
});

$(".image-holder-input2").on('change', function () {

    //Get count of selected files
    var countFiles = $(this)[0].files.length;

    var imgPath = $(this)[0].value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    var image_holder = $(".image-holder2");
    image_holder.empty();

    if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg" || extn == "svg") {
        if (typeof (FileReader) != "undefined") {

            //loop for each file selected for uploaded.
            for (var i = 0; i < countFiles; i++) {

                var reader = new FileReader();
                reader.onload = function (e) {
                    $("<img />", {
                        "src": e.target.result,
                        "class": "thumb-image thumb-image-size"
                    }).appendTo(image_holder);
                }

                image_holder.show();
                reader.readAsDataURL($(this)[0].files[i]);
            }

        } else {
            alert("This browser does not support FileReader.");
        }
    } else {
        alert("Pls select only images");
    }
});

function swal_custom(mensaje, tipo) {
    swal({
        title: mensaje,
        type: tipo,
        confirmButtonText: 'OK'
    })
}

function swal_success(mensaje) {
    swal({
        title: mensaje,
        type: 'success',
        confirmButtonText: 'OK'
    })
}

function swal_fn(mensaje, tipo, funcion) {
    swal({
        title: mensaje,
        type: tipo,
        confirmButtonText: 'OK'
    })
    .then(funcion)
}

function swal_confirm(mensaje, tipo, mensaje_confirmar, funcion) {

    swal({
        title: mensaje,
        type: tipo,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: mensaje_confirmar,
        cancelButtonText: 'Cancelar'
    }).then(funcion);
}

function swal_error_fn(mensaje) {
    swal('Error', mensaje, 'error');
}

function swal_warning(mensaje) {
    swal('Por favor validar', mensaje, 'warning');
}

function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}

function ValidarAmbasFechas(FechaInicial, FechaFinal) {

    if ((FechaInicial == '' && FechaFinal != '') ||
    (FechaFinal == '' && FechaInicial != '')
    ) { swal('', 'Debe ingresar ambas fechas para la consulta', 'warning'); } else { return true; }

}

function RefrescarPagina() { location.reload(); }