﻿
// ==========
// Author: Wilkin Vásquez
// Date: August 29th, 2017
// ==========

$(document).ready(function () {
    var emptyField = $('.empty-field');
    var emptyFieldText = $('.empty-field-text');

    $('.empty-field-container').css('position', 'relative');

    emptyField.css({
        'outline': 'none'
    });

    emptyFieldText.text('Campo obligatorio');
    emptyFieldText.css({
        'color': '#ff0000',
        'position': 'absolute',
        'right': '0',
        'bottom': '-17px',
        'font-size': '9pt'
    });

    emptyField.on('input', function () {
        changeBehavior($(this));
    });

    emptyField.on('dp.change', function () {
        changeBehavior($(this));
    });

    function changeBehavior(element) {
        if (element.val().trim() != '' && element.val() != "0") {
            element.css('border', '1px solid #31B404');
            element.parent().find('.empty-field-text').css('display', 'none');
        }
        else {
            element.css('border', '1px solid #7E7E7E');
            element.parent().find('.empty-field-text').css('display', 'initial');
        }
    }
});