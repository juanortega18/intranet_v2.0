﻿$(document).ready(function(){
    $(".SaveServicio").on('click', function () {

  //  $("#loading-animation-container").show();
    $.ajax({
        url: '/ServiciosRepatriados/Crear',
        type: 'POST',
        data: $("#crear_servicio").serialize(),
       
        success: function (resultado) {
            if (resultado == true) {
                swal({
                    title: 'Guardado',
                    text: data.Mensaje,
                    type: 'success',
                    confirmButtonColor: '#18212f',
                    confirmButtonText: 'Aceptar'
                }).then(function () {
                    $('.ServicioModal').modal('hide');

                    cargarServiciosRepatriados({ data: { opcion: "solicitados", tabla: tablaSolicitados, servicioEstadoId: 1 } });
                });
                $("#loading-animation-container").hide();
            }
            else {
                swal({
                    title: 'Error',
                    text: data.Mensaje,
                    type: 'error',
                    confirmButtonColor: '#18212f',
                    confirmButtonText: 'Aceptar'
                });

                $("#loading-animation-container").hide();
            }
        },
        error: function (jqXhr, textStatus, errorMessage) {
            swal('Ha ocurrido un error al procesar su solicitud ', '', 'error');
            $("#loading-animation-container").hide();

        }
    })
});


})