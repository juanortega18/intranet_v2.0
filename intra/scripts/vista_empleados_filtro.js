﻿function Inicio(FiltrarEmpleados, getDepartamento , VistaDetalles) {

    var tabla = $('#tableGrid');

    $('#tableGrid_wrapper').hide()

    $('.chosen-select').chosen();

    var filtro = $('#filtro');

    var cargo = $('.cargo');
    var cargos = $('#cargos');

    var dependencia = $('.dependencia');
    var dependencias = $('#dependencias');

    var departamento = $('.departamento');
    var departamentos = $('#departamentos');

    var actualizados = $('#someSwitchOptionDefault');
    var actualizadosDiv = $('.actualizados');

    filtro.on("change", function () {

        switch (filtro.val()) {

            case "1":
                cargo.show();
                dependencia.hide();
                departamento.hide();
                actualizadosDiv.show();
                dependencias.val("");
                $("#dependencias").trigger("chosen:updated");

                break;
            case "2":
                dependencia.show();
                cargo.hide();
                cargos.val("");
                actualizadosDiv.show();
                $("#cargos").trigger("chosen:updated");

                break;

            case "3":
                dependencia.show();
                cargo.hide();
                departamento.hide();
                actualizadosDiv.show();
                cargos.val("");
                departamento.val("");
                $("#cargos").trigger("chosen:updated");
                $("#departamentos").trigger("chosen:updated");

                break;

            case "4":
                dependencia.hide();
                cargo.hide();
                departamento.hide();
                cargos.val("");
                actualizadosDiv.hide();
                dependencias.val("");
                $("#cargos,#dependencias").trigger("chosen:updated");
                cargarEmpleadosTodos(true /*actualizados.val()*/, null, null, null)

                break;

            default:
                dependencia.hide();
                cargo.hide();
                departamento.hide();
                actualizadosDiv.hide();
        }
    });

    dependencias.on("change", function () {

        var dependenciaselecionada = $('#dependencias').val();

        if (filtro.val() == "3") {
            cargarEmpleadosTodos(actualizados.val(), dependenciaselecionada, null, null);
            //function cargarEmpleadosTodos(actualizados, dependencia, departamento, cargo)
        }
        else
        {
            if (dependenciaselecionada != "") {

                cargarDepartamentos(dependenciaselecionada);
                departamento.show();

            } else {

                departamento.hide();

            }
        }




    });

    cargos.on("change", function () {

        if (cargos.val() != "") {
            cargarEmpleadosTodos(actualizados.val(), null, null, cargos.val())
        }

    });

    departamentos.on("change", function () {

        cargarEmpleadosTodos(actualizados.val(), dependencias.val(), departamentos.val(), null)

    });

    actualizados.on("change", function (e) {
        this.value = this.checked ? true : false;

        if (tabla.DataTable().rows().count() > 0 || this.value == "false") {

            if (cargos.val() != "") {
                cargarEmpleadosTodos(actualizados.val(), null, null, cargos.val())
            } else if (dependencias.val() != "" && departamentos.val() != "") {
                cargarEmpleadosTodos(actualizados.val(), dependencias.val(), departamentos.val(), null)
            } //else if (filtro.val() == "3") { descomentar para ver todos los empleados sin filtros
            //    cargarEmpleadosTodos(actualizados.val(), null, null, null)
            //}
        }
    })

    if (filtro.val() != "") {

        filtro.trigger("change");

    }

    if (cargos.val() != "") {

        cargos.trigger("change");

    }

    if (dependencias.val() != "") {

        dependencias.trigger("change");

    }

    function cargarDepartamentos(dependencia) {

        $.ajax({
            url: getDepartamento,
            method: 'POST',
            data: { dependencia: dependencia },
            dataType: 'json',
            success: function (departamentos, status, xhr) {

                $('#departamentos').html("");
                $('#departamentos').append('<option value>Seleccione el Dependencia</option>');
                $.each(departamentos, function (i, departamento) {
                    $('#departamentos').append('<option value="' + departamento.Value + '">' + departamento.Text + '</option>');
                });
                $('#departamentos').trigger('chosen:updated');

            },
            error: function (jqXhr, textStatus, errorMessage) {
                swal('Un Error a ocurrido', '', 'error');
            }
        });
    }

    function cargarEmpleadosTodos(actualizados, dependencia, departamento, cargo) {

        $("#loading-animation-container").show();

        $.ajax({
            url: FiltrarEmpleados,
            method: 'POST',
            data: { actualizados: actualizados, dependencia: dependencia, departamento: departamento, cargo: cargo },
            dataType: 'json',
            success: function (empleados, status, xhr) {

                ListarEmpleados(empleados);
                $("#tableGrid_wrapper").show();
                $("#loading-animation-container").hide();

            },
            error: function (jqXhr, textStatus, errorMessage) {
                swal('Un Error a ocurrido', '', 'error');
                $("#loading-animation-container").hide();

            }
        });
    }

    function ListarEmpleados(Empleados) {

        tabla.DataTable().clear().draw();

        for (var i in Empleados) {
            var empleado = Empleados[i];

            tabla.DataTable().row.add({
                0: empleado.Nombre,
                1: empleado.Codigo,
                2: empleado.Cedula,
                3: empleado.Cargo,
                4: '<a class="button_white" href="' + VistaDetalles +'/VISTA_EMPLEADOS/Details/' + empleado.Codigo + '">Detalles</a>',
                5: empleado.Departamento,
                6: empleado.Dependencia
            }).draw();
        }
    }
}