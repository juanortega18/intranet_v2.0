﻿
function Iniciar(IniciarSistemaUrl, CrearCarpetaUrl, ObtenerElementosHijosUrl, EliminarElementoUrl, RenombrarElementoUrl, GuardarArchivoUrl, DescargarArchivoUrl, VirtualPath) {

    // VARIABLES

    var CarpetaActualID = "";
    var CarpetaActualNombre = "";
    var CarpetaActualRuta = "";

    var HistorialRutas = [];

    // BOTONES

    var CarpetaBotonCrear = $("#carpeta-boton-crear");
    var CarpetaBotonCerrar = $("#carpeta-boton-cerrar");
    var ArchivoBotonCerrar = $("#archivo-boton-cerrar");

    // BOTONES CONTROLES

    var ControlesBotonCrearCarpeta = $("#controles-boton-crear-carpeta");
    var ControlesBotonSubirArchivo = $("#controles-boton-subir-archivo");

    // CAMPOS

    var CarpetaCampoNombre = $("#carpeta-campo-nombre");

    // CONTENEDORES

    var ControlesCrearSubir = $("#controles-crear-subir");
    var CarpetaCrear = $("#carpeta-crear");
    var CarpetaHijos = $("#carpeta-hijos");
    var InfoCarpetaActual = $("#info-carpeta-actual");
    var SubirArchivo = $("#archivo-subir");
    var ControlesNavegacion = $("#controles-navegacion");
    var AnimacionCarga = $("#loading-animation-container");
    var AlertaExisteNombre = $("#alerta-existe-nombre");

    // ELEMENTOS DE CONTENIDO VARIABLE

    var InfoCarpetaActualNombre = $("#info-carpeta-actual-nombre");

    // NAVEGACION

    var VolverAtras = $("#volver-atras i");
    var VolverHome = $("#volver-home i");

    // BOOTSTRAP FILE INPUT

    EjecutarBootstrapFileInput();

    // ==========
    // INICIAR SISTEMA
    // ==========

    function IniciarSistema() {
        var request = $.ajax({
            url: IniciarSistemaUrl,
            method: "POST"
        });

        request.done(function (Data) {
            CarpetaActualID = Data.CarpetaActualID;
            CarpetaActualNombre = Data.CarpetaActualNombre;
            CarpetaActualRuta = Data.CarpetaActualRuta;

            InfoCarpetaActualNombre.text(CarpetaActualNombre);
            ObtenerElementosHijos();

            console.log("Sistema de archivos iniciado.");
        });

        request.fail(function (jqXHR, textStatus) {
            console.log("Falló iniciar sistema de archivos, " + textStatus);
        });
    }

    IniciarSistema();

    // ==========
    // CREAR CARPETA
    // ==========

    ControlesBotonCrearCarpeta.click(function () {
        CarpetaCrear.show();
        ControlesCrearSubir.hide();
    });

    CarpetaBotonCerrar.click(function () {
        CarpetaCampoNombre.val("");
        CarpetaCrear.hide();
        ControlesCrearSubir.show();
    });

    CarpetaBotonCrear.click(function () {
        AnimacionCarga.show();

        var carpetaNombre = CarpetaCampoNombre.val().trim();

        var request = $.ajax({
            url: CrearCarpetaUrl,
            method: "POST",
            data: {
                carpetaMadreID: CarpetaActualID,
                carpetaNombre: carpetaNombre,
                carpetaMadreRuta: CarpetaActualRuta.trim()
            }
        });

        request.done(function (Data) {
            AnimacionCarga.hide();

            if (Data.Estatus) {
                var funcionCallback = function () {
                    swal(
                        '',
                        '!Carpeta creada!',
                        'success'
                    );

                    console.log("Carpeta creada.");
                }

                ObtenerElementosHijos(funcionCallback);
            }
            else {
                swal(
                    '', 
                    Data.MensajeValidacion,
                    'error'
                )

                console.log(Data.MensajeValidacion);
            }
        });

        request.fail(function (jqXHR, textStatus) {
            console.log("Falló crear carpeta, " + textStatus);
        });
    });

    // ==========
    // SUBIR ARCHIVO
    // ==========

    ControlesBotonSubirArchivo.click(function () {
        SubirArchivo.show();
        ControlesCrearSubir.hide();
    });

    ArchivoBotonCerrar.click(function () {
        $("#subir-archivo").fileinput("clear");

        SubirArchivo.hide();
        ControlesCrearSubir.show();
    });

    // ==========
    // OBTENER ELEMENTOS HIJOS
    // ==========

    function ObtenerElementosHijos(funcionCallback) {
        AnimacionCarga.show();

        var request = $.ajax({
            url: ObtenerElementosHijosUrl,
            method: "POST",
            data: {
                carpetaMadreID: CarpetaActualID
            }
        });

        request.done(function (Data) {
            CargarElementosHijos(Data);

            AnimacionCarga.hide();

            console.log("Los elementos hijos fueron obtenidos.");

            try {
                funcionCallback();
            } catch (error){
                console.log("Funcion callback no disponible.");
            }
        });

        request.fail(function (jqXHR, textStatus) {
            console.log("Falló obtener elementos hijos, " + textStatus);
        });
    }

    // ==========
    // CARGAR ELEMENTOS HIJOS
    // ==========

    function CargarElementosHijos(ElementosHijos) {
        var elementos = "";

        for (var i = 0; i < ElementosHijos.length; i++){
            var elementoExtension = ElementosHijos[i].ElementoHijoExtension;
            var elementoHijoIcono = `<i class="elemento-hijo-icono-carpeta fa fa-folder fa-4x" aria-hidden="true"></i>`;
            var elementoIconoDescarga = '<i class="elemento-boton-descargar fa fa-download fa-2x" aria-hidden="true"></i>';

            if (elementoExtension == null) {
                elementoIconoDescarga = '';
            }
            else {
                elementoHijoIcono = ObtenerElementoHijoIcono(elementoExtension);
            }

            elementos += `
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="elemento-hijo col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="elemento-hijo-icono col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            `+ elementoHijoIcono +`
                        </div>
                        <div class="elemento-centro col-lg-7 col-md-7 col-sm-7 col-xs-7">
                            <div class="elemento-nombre col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                ` + ElementosHijos[i].ElementoHijoNombre + `
                            </div>

                           <div class="elemento-contenedor-renombrar">                            
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <input class="elemento-campo-renombrar" />
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <button class="elemento-boton-renombrar-guardar">Renombrar</button>
                                </div>
                            </div>
                        </div>

                        <div class="elemento-botones col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <i class="elemento-boton-renombrar fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                            <i class="elemento-boton-eliminar fa fa-trash fa-2x" aria-hidden="true"></i>
                            ` + elementoIconoDescarga + `
                        </div>

                        <span class="elemento-data elemento-id">
                            ` + ElementosHijos[i].ElementoHijoID + `
                        </span>

                        <span class="elemento-data elemento-ruta">
                            ` + ElementosHijos[i].ElementoHijoRuta + `
                        </span>
                    </div>
                    
                </div>
                
            `;
        }

        CarpetaHijos.empty().append(elementos);

        $(".elemento-hijo-icono-carpeta").click(function () {
            AbrirCarpeta($(this).parent().parent());
        });

        $(".elemento-boton-renombrar").click(function () {
            $(this).parent().parent().find(".elemento-contenedor-renombrar").toggle(0, function () {
                $(this).parent().parent().find(".elemento-boton-renombrar").toggleClass("elemento-boton-toggle-color");
            });
        });

        $(".elemento-boton-renombrar-guardar").click(function () {
            RenombrarElemento($(this).parent().parent().parent().parent());
        });

        $(".elemento-boton-eliminar").click(function () {
            EliminarElemento($(this).parent().parent());
        });

        $(".elemento-boton-descargar").click(function () {
            DescargarArchivo($(this).parent().parent());
        });
    }

    function ObtenerElementoHijoIcono(ElementoExtension) {
        var icono = ``;
        var color = ``;

        switch (ElementoExtension){
            case "pdf":
                icono = `pdf`;
                break;
            case "doc":
                icono = `doc`;
                break;
            case "docx":
                icono = `doc`;
                break;
            case "xls":
                icono = `xls`;
                break;
            case "xlsx":
                icono = `xls`;
                break;
            case "ppt":
                icono = `ppt`;
                break;
            case "pptx":
                icono = `ppt`;
                break;
            case "txt":
                icono = `txt`;
                break;
            case "htm":
                icono = `html`;
                break;
            case "html":
                icono = `html`;
                break;
            case "cs":
                icono = `cs`;
                break;
            case "js":
                icono = `js`;
                break;
            case "mp3":
                icono = `mp3`;
                break;
            case "mp4":
                icono = `mp4`;
                break;
            case "avi":
                icono = `avi`;
                break;
            case "flv":
                icono = `flv`;
                break;
            case "mov":
                icono = `mov`;
                break;
            case "mpg":
                icono = `mpg`;
                break;
            case "wmv":
                icono = `wmv`;
                break;
            case "png":
                icono = `png`;
                break;
            case "jpg":
                icono = `jpg`;
                break;
            default:
                icono = `file`;
        }
                //<img src="`+ VirtualPath + `Content/images/BI_Images/` + icono + `.png" />

        var elementoIcono = `
            <div class="elemento-hijo-icono-archivo" >
                <img src="`+ VirtualPath + `/Content/images/BI_Images/` + icono + `.png" />
            </div>
        `;

        return elementoIcono;
    }

    // ==========
    // ABRIR CARPETA
    // ==========

    function AbrirCarpeta(Elemento) {
        ControlesNavegacion.show();

        var CarpetaAnterior = {
            CarpetaAnteriorID: +CarpetaActualID,
            CarpetaAnteriorNombre: CarpetaActualNombre,
            CarpetaAnteriorRuta: CarpetaActualRuta
        };

        HistorialRutas.push(CarpetaAnterior);

        CarpetaActualID = Elemento.find(".elemento-id").text().trim();
        CarpetaActualNombre = Elemento.find(".elemento-nombre").text().trim();
        CarpetaActualRuta = Elemento.find(".elemento-ruta").text().trim() + CarpetaActualNombre + "/";

        InfoCarpetaActualNombre.text(CarpetaActualNombre);

        $("#subir-archivo").fileinput("clear");
        SubirArchivo.hide();
        ControlesCrearSubir.show();

        CarpetaCampoNombre.val("");
        CarpetaCrear.hide();
        ControlesCrearSubir.show();

        ObtenerElementosHijos();
    }

    // ==========
    // RENOMBRAR ELEMENTO
    // ==========

    function RenombrarElemento(Elemento) {
        swal({
            title: '',
            text: "¿Está seguro/a que quiere renombrar este elemento?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí',
            cancelButtonText: "NO"
        }).then(function () {
            AnimacionCarga.show();

            var request = $.ajax({
                url: RenombrarElementoUrl,
                method: "POST",
                data: {
                    elementoID: Elemento.find(".elemento-id").text().trim(),
                    elementoNombre: Elemento.find(".elemento-nombre").text().trim(),
                    elementoNuevoNombre: Elemento.find(".elemento-campo-renombrar").val().trim(),
                    elementoRuta: Elemento.find(".elemento-ruta").text().trim(),
                    elementoMadreID: CarpetaActualID,
                    elementoMadreRuta: CarpetaActualRuta
                }
            });

            request.done(function (Data) {
                AnimacionCarga.hide();

                if (Data.Estatus) {
                    Elemento.find(".elemento-nombre").text(Data.ElementoNombre);
                    Elemento.find(".elemento-campo-renombrar").val("");

                    console.log("Elemento renombrado.");

                    swal(
                        '',
                        '!Elemento renombrado!',
                        'success'
                    )
                }
                else {
                    swal(
                        '',
                        Data.MensajeValidacion,
                        'error'
                    )

                    console.log(Data.MensajeValidacion);
                }
            });

            request.fail(function (jqXHR, textStatus) {
                console.log("Falló renombrar elemento, " + textStatus);
            });
        });
    }

    // ==========
    // ELIMINAR ELEMENTO
    // ==========

    function EliminarElemento(Elemento) {
        swal({
            title: '',
            text: "¿Está seguro/a que quiere eliminar este elemento?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí',
            cancelButtonText: "NO"
        }).then(function () {
            AnimacionCarga.show();

            var request = $.ajax({
                url: EliminarElementoUrl,
                method: "POST",
                data: {
                    elementoID: Elemento.find(".elemento-id").text().trim(),
                    elementoNombre: Elemento.find(".elemento-nombre").text().trim(),
                    elementoRuta: Elemento.find(".elemento-ruta").text().trim()
                }
            });

            request.done(function (Data) {
                Elemento.remove();

                console.log("Elemento eliminado.");

                AnimacionCarga.hide();

                swal(
                    '',
                    '!Elemento eliminado!',
                    'success'
                )
            });

            request.fail(function (jqXHR, textStatus) {
                console.log("Falló eliminar elemento, " + textStatus);
            });
        });
    }

    // ==========
    // DESCARGAR ARCHIVO
    // ==========

    function DescargarArchivo(Elemento) {
        window.location.href = DescargarArchivoUrl + "?archivoID=" + Elemento.find(".elemento-id").text().trim();
    }

    // ==========
    // VOLVER ATRAS
    // ==========

    VolverAtras.click(function () {
        if (HistorialRutas.length != 0) {
            var carpetaActual = HistorialRutas[HistorialRutas.length - 1];

            CarpetaActualID = carpetaActual.CarpetaAnteriorID;
            CarpetaActualNombre = carpetaActual.CarpetaAnteriorNombre;
            CarpetaActualRuta = carpetaActual.CarpetaAnteriorRuta;

            InfoCarpetaActualNombre.text(CarpetaActualNombre);
        }

        HistorialRutas.pop();

        if (HistorialRutas.length == 0) {
            ControlesNavegacion.hide();
        }

        CarpetaCampoNombre.val("");
        CarpetaCrear.hide();
        ControlesCrearSubir.show();

        $("#subir-archivo").fileinput("clear");
        SubirArchivo.hide();
        ControlesCrearSubir.show();

        ObtenerElementosHijos();
    });

    // ==========
    // VOLVER HOME
    // ==========

    VolverHome.click(function () {
        CarpetaActualID = HistorialRutas[0].CarpetaAnteriorID;
        CarpetaActualNombre = HistorialRutas[0].CarpetaAnteriorNombre;
        CarpetaActualRuta = HistorialRutas[0].CarpetaAnteriorRuta;

        HistorialRutas = [];

        InfoCarpetaActualNombre.text(CarpetaActualNombre);
        ControlesNavegacion.hide();

        $("#subir-archivo").fileinput("clear");
        SubirArchivo.hide();
        ControlesCrearSubir.show();

        CarpetaCampoNombre.val("");
        CarpetaCrear.hide();
        ControlesCrearSubir.show();

        ObtenerElementosHijos();
    });

    // ==========
    // EJECUTAR BOOTSTRAP FILE INPUT
    // ==========

    function EjecutarBootstrapFileInput() {
        $("#subir-archivo").fileinput({
            language: "es",
            maxFileSize: 100000,
            uploadAsync: false,
            uploadUrl: GuardarArchivoUrl,
            uploadExtraData: function () {
                return { carpetaMadreID: CarpetaActualID, carpetaMadreRuta: CarpetaActualRuta }
            },
            fileActionSettings: {
                showRemove: true,
                showUpload: false,
                showZoom: false,
                showDrag: false,
            },
            previewFileIcon: '<i class="fa fa-file"></i>',
            preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
            previewFileIconSettings: { // configure your icon file extensions
                'doc': '<i class="fa fa-file-word-o text-primary"></i>',
                'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'htm': '<i class="fa fa-file-code-o text-info"></i>',
                'txt': '<i class="fa fa-file-text-o text-info"></i>',
                'mov': '<i class="fa fa-file-movie-o text-warning"></i>',
                'mp3': '<i class="fa fa-file-audio-o text-warning"></i>',
                // note for these file types below no extension determination logic
                // has been configured (the keys itself will be used as extensions)
                'jpg': '<i class="fa fa-file-photo-o text-danger"></i>',
                'gif': '<i class="fa fa-file-photo-o text-muted"></i>',
                'png': '<i class="fa fa-file-photo-o text-primary"></i>'
            },
            previewFileExtSettings: { // configure the logic for determining icon file extensions
                'doc': function (ext) {
                    return ext.match(/(doc|docx)$/i);
                },
                'xls': function (ext) {
                    return ext.match(/(xls|xlsx)$/i);
                },
                'ppt': function (ext) {
                    return ext.match(/(ppt|pptx)$/i);
                },
                'zip': function (ext) {
                    return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
                },
                'htm': function (ext) {
                    return ext.match(/(htm|html)$/i);
                },
                'txt': function (ext) {
                    return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
                },
                'mov': function (ext) {
                    return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
                },
                'mp3': function (ext) {
                    return ext.match(/(mp3|wav)$/i);
                }
            }
        });

        $("#subir-archivo").on('fileuploaderror', function (event, data, msg) {
            swal(
                '',
                '!El archivo o uno de los archivos sobrepasa el tamaño máximo permitio de 100MB!',
                'error'
            )
        });

        $("#subir-archivo").on('filebatchpreupload', function (event, data, previewId, index) {
            ArchivoBotonCerrar.prop('disabled', true);
        });

        $("#subir-archivo").on('filebatchuploadsuccess', function (event, data, previewId, index) {
            ArchivoBotonCerrar.prop('disabled', false);

            $("#subir-archivo").fileinput("clear");

            SubirArchivo.hide();
            ControlesCrearSubir.show();

            var cantidadArchivosSubidos = data.filescount;
            var mensaje = "";

            if (cantidadArchivosSubidos == 0) {
                mensaje = "No se guardó el archivo o los archivos";

                swal(
                    '',
                    '!' + mensaje + '!',
                    'error'
                )
            }
            else if (cantidadArchivosSubidos == 1) {
                mensaje = "Se guardó el archivo";

                swal(
                    '',
                    '!' + mensaje + '!',
                    'success'
                )
            }
            else if (cantidadArchivosSubidos > 1) {
                mensaje = "Se guardaron los archivos";

                swal(
                    '',
                    '!' + mensaje + '!',
                    'success'
                )
            }

            ObtenerElementosHijos();
        });
    }
}
