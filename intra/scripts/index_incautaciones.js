﻿$(document).ready(function () {

    var provincias = $("#provincias");
    var tipo = $("#tipo");
    var localizacionDiv = $("#localizacion");
    var vehiculoDiv = $("#vehiculo");
    var provincialabel = $("#provincialabel");
    var vehiculolabel = $("#vehiculolabel");
    var consulta = $("#Consultar");
    var cuerpoTabla = $("#tableGrid tbody");
    var vehiculo = $("#vehiculoinput");
    var cuerpoTabla = $("#tableGrid tbody");

    var tabla = $('#tableGrid').DataTable({
        //"columnDefs": [{ "visible": false,"targets": 5, }],
        "language": {
            "processing": "...Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "No se encontraron registros disponibles",
            "emptyTable": "No hay Datos Disponibles en esta Tabla",
            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "infoPostFix": "",
            "search": "Buscar:",
            "url": "",
            "infoThousands": ",",
            "loadingRecords": "...Cargando...",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        responsive: true
    })

    $(function () {
        tipo.change(function () {
            if ($(this).val() == 1) {
                localizacionDiv.show();
                provincialabel.text("Localización");
                vehiculoDiv.hide();
            }
            else if ($(this).val() == 2) {
                localizacionDiv.show();
                provincialabel.text("Lugar de Origen");
                vehiculoDiv.hide();
            }
            else if ($(this).val() == 3) {
                localizacionDiv.hide();
                vehiculoDiv.show();
                vehiculolabel.text("Chasis");
            }
            else if ($(this).val() == 4) {
                localizacionDiv.hide();
                vehiculoDiv.show();
                vehiculolabel.text("Matrícula");
            }
            else if ($(this).val() == 5) {
                localizacionDiv.hide();
                vehiculoDiv.show();
                vehiculolabel.text("Placa");
            }
            else if ($(this).val() == 6) {
                localizacionDiv.hide();
                vehiculoDiv.show();
                vehiculolabel.text("Modelo");
            }
            else if ($(this).val() == 7) {
                localizacionDiv.hide();
                vehiculoDiv.show();
                vehiculolabel.text("Marca");
            }
            else {
                localizacionDiv.hide();
                vehiculoDiv.hide();
            }
        });
    });

    consulta.click(function () {

        //alert($("#tipo").val());

        if (tipo.val() == 1) {
            if (provincias.val() != "") {

                $.ajax({
                    url: "Incautaciones/INC_Filtrar",
                    data: { destino: provincias.val() },
                    dataType: 'json',
                    method: 'POST',
                    success: function (result) {

                        Listar(result);

                    },
                    error: function (ex) {
                        swal("", "Ha ocurrido un error", "error");
                        console.log(ex);
                    }
                });
            }
        }
        else if (tipo.val() == 2) {
            if (provincias.val() != "") {

                $.ajax({
                    url: "Incautaciones/INC_Filtrar",
                    data: { origen: provincias.val() },
                    dataType: 'json',
                    method: 'POST',
                    success: function (result) {

                        Listar(result);

                    },
                    error: function (ex) {
                        swal("", "Ha ocurrido un error", "error");
                        console.log(ex);
                    }
                });

            }
        }
        else if (tipo.val() == 3) {
            if (provincias.val() != "") {

                $.ajax({
                    url: "Incautaciones/INC_Filtrar",
                    data: { chasis: vehiculo.val() },
                    dataType: 'json',
                    method: 'POST',
                    success: function (result) {

                        Listar(result);

                    },
                    error: function (ex) {
                        swal("", "Ha ocurrido un error", "error");
                        console.log(ex);
                    }
                });
            }
        }
        else if (tipo.val() == 4) {
            if (provincias.val() != "") {

                $.ajax({
                    url: "Incautaciones/INC_Filtrar",
                    data: { matricula: vehiculo.val() },
                    dataType: 'json',
                    method: 'POST',
                    success: function (result) {

                        Listar(result);

                    },
                    error: function (ex) {
                        swal("", "Ha ocurrido un error", "error");
                        console.log(ex);
                    }
                });
            }
        } else if (tipo.val() == 5) {
            if (provincias.val() != "") {

                $.ajax({
                    url: "Incautaciones/INC_Filtrar",
                    data: { placa: vehiculo.val() },
                    dataType: 'json',
                    method: 'POST',
                    success: function (result) {

                        Listar(result);

                    },
                    error: function (ex) {
                        swal("", "Ha ocurrido un error", "error");
                        console.log(ex);
                    }
                });
            }
        } else if (tipo.val() == 6) {
            if (provincias.val() != "") {

                $.ajax({
                    url: "Incautaciones/INC_Filtrar",
                    data: { modelo: vehiculo.val() },
                    dataType: 'json',
                    method: 'POST',
                    success: function (result) {

                        Listar(result);

                    },
                    error: function (ex) {
                        swal("", "Ha ocurrido un error", "error");
                        console.log(ex);
                    }
                });
            }
        } else if (tipo.val() == 7) {
            if (provincias.val() != "") {

                $.ajax({
                    url: "Incautaciones/INC_Filtrar",
                    data: { marca: vehiculo.val() },
                    dataType: 'json',
                    method: 'POST',
                    success: function (result) {

                        Listar(result);

                    },
                    error: function (ex) {
                        swal("", "Ha ocurrido un error", "error");
                        console.log(ex);
                    }
                });
            }
        } else {

                   $.ajax({
                       url: "Incautaciones/INC_Filtrar",
                       dataType: 'json',
                       method: 'POST',
                       success: function (result) {

                            Listar(result);

                        },
                       error: function (ex) {
                            swal("", "Ha ocurrido un error", "error");
                            console.log(ex);
                        }
                   });
               }
    });

    function Listar(incas) {

        tabla.clear().draw();

        for (var i in incas) {
            var inca = incas[i];

            tabla.row.add({
                0: `<td> <a href=Incautaciones/Edit/` + inca.INCA_ID + `>` + inca.INCA_NO + `</a> </td>`,
                1: moment(inca.INCA_FHENTRADA).format("DD/MM/YYYY"),
                2: inca.CASO_NOMBRE,
                3: inca.TIPO_NOMBRE,
                4: inca.ESTA_NOMBRE,
                5: inca.INCA_DIRECCION,
                6: inca.INCA_VALOR
            }).draw();
        }
    }
});