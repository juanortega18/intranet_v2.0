﻿
function IniciarScriptSeguros(ROOT_PATH, SEGURO_ID, RRHHA) {
    // ==========
    // GUARDAR SEGURO
    // ==========

    $("#boton-guardar-seguro").click(function () {
        $(".fileinput-upload-button").trigger("click");

        //$("#loading-animation-container").show();

        //console.log(ROOT_PATH);

        //var request = $.ajax({
        //    url: ROOT_PATH + "/Seguros/GuardarSeguro",
        //    method: "POST",
        //    data: {
        //        SeguroNombre: $("#seguro-nombre").val(),
        //        SeguroLink: $("#seguro-link").val()
        //    },
        //    dataType: "json"
        //});

        //request.done(function (Resultado) {
        //    $("#loading-animation-container").hide();

        //    if (Resultado.Exito == true) {
        //        swal(
        //            '',
        //            '¡ El seguro se guardó con exito !',
        //            'success'
        //        ).then(function () {
        //            $("#loading-animation-container").show();

        //            window.location.href = ROOT_PATH + "/Seguros/Index"
        //        });
        //    }
        //    else {
        //        var mesajeErrores = "";

        //        for (var i in Resultado) {
        //            mesajeErrores += "¡ " + Resultado[i] + " ! <br/>"
        //        }

        //        swal(
        //            '',
        //            mesajeErrores,
        //            'error'
        //        );
        //    }
        //});

        //request.fail(function (jqXHR, textStatus) {
        //    console.log("Falló guardar seguro: " + textStatus);
        //});
    });

    // ==========
    // GUARDAR SEGURO EDITADO
    // ==========

    $("#boton-editar-seguro").click(function () {
        swal({
            title: '',
            text: '¿ Seguro/a que desea editar este seguro ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then(function () {
            $("#loading-animation-container").show();

            var request = $.ajax({
                url: ROOT_PATH + "/Seguros/GuardarSeguroEditado",
                method: "POST",
                data: {
                    SeguroId: $("#boton-editar-seguro").val(),
                    SeguroNombre: $("#seguro-nombre").val(),
                    SeguroLink: $("#seguro-link").val()
                },
                dataType: "json"
            });

            request.done(function (Resultado) {
                $("#loading-animation-container").hide();

                if (Resultado.Exito == true) {
                    swal(
                        '',
                        '¡ El seguro se guardó con éxito !',
                        'success'
                    ).then(function () {
                        $("#loading-animation-container").show();

                        window.location.href = ROOT_PATH + "/Seguros/Index"
                    });
                }
                else {
                    var mesajeErrores = "";

                    for (var i in Resultado) {
                        mesajeErrores += "¡ " + Resultado[i] + " ! <br/>"
                    }

                    swal(
                        '',
                        mesajeErrores,
                        'error'
                    );
                }
            });

            request.fail(function (jqXHR, textStatus) {
                console.log("Falló guardar seguro: " + textStatus);
            });
        })
    });

    // ==========
    // ELIMINAR SEGURO 
    // ==========

    $(".boton-eliminar-seguro").click(function () {
        var seguroId = $(this).parent().next().text().trim();

        swal({
            title: '',
            text: '¿ Seguro/a que desea eliminar ? También se eliminarán todos los archivos asociados a este seguro.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then(function () {
            $("#loading-animation-container").show();

            var request = $.ajax({
                url: ROOT_PATH + "/Seguros/EliminarSeguro",
                method: "POST",
                data: {
                    SeguroId: seguroId
                },
                dataType: "json"
            });

            request.done(function (Respuesta) {
                $("#loading-animation-container").hide();

                swal(
                    '',
                    '¡ El seguro se eliminó con éxito !',
                    'success'
                ).then(function () {
                    $("#loading-animation-container").show();

                    window.location.reload();
                });
            });

            request.fail(function (jqXHR, textStatus) {
                console.log("Falló eliminar seguro: " + textStatus);
            });
        });
    });

    // ==========
    // GARGAR ARCHIVOS
    // ==========

    CargarArchivos();

    function CargarArchivos() {
        var request = $.ajax({
            url: ROOT_PATH + "/Seguros/ObtenerArchivos",
            method: "POST",
            data: {
                SeguroId: SEGURO_ID
            },
            dataType: "json"
        });

        request.done(function (Resultado) {
            for (var i in Resultado) {
                var botonEliminar = ``;

                if (RRHHA == "True") {
                    botonEliminar = `
                        <div class="seguro-archivo-boton-eliminar col-lg-6 col-md-6">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </div>
                    `;
                }

                var archivo = `
                    <div class="seguro-archivo col-lg-12 col-md-12">
                        <div class="seguro-archivo-imagen col-lg-3 col-md-3">
                            <img class="img-responsive" src="` + ROOT_PATH + `/Content/images/BI_Images/` + Resultado[i].ArchivoExtension + `.png" />
                        </div>

                        <div class="seguro-archivo-nombre col-lg-6 col-md-6">
                            ` + Resultado[i].ArchivoNombre  + `
                        </div>

                        <div class="seguro-archivo-contenedor-botones col-lg-3 col-md-3">
                            <div class="seguro-archivo-boton-descargar col-lg-6 col-md-6">
                                <a href="` + ROOT_PATH + `/Seguros/DescargarArchivo?archivoId=` + Resultado[i].ArchivoId + `">
                                    <i class="fa fa-download" aria-hidden="true"></i>
                                </a>
                            </div>

                            ` + botonEliminar + `

                            <div hidden>
                                ` + Resultado[i].ArchivoId + `
                            </div>
                        </div>
                    </div>
                `;

                $("#contenedor-archivos").append(archivo);
            }

            $(".seguro-archivo-boton-eliminar i").click(function () {
                var archivoId = $(this).parent().next().text().trim();

                EliminarArchivo(archivoId);
            });
        });

        request.fail(function (jqXHR, textStatus) {
            console.log("Falló obtener archivos: " + textStatus);
        });
    }

    // ==========
    // ELIMINAR ARCHIVO
    // ==========

    function EliminarArchivo(archivoId) {
        swal({
            title: '',
            text: '¿ Seguro/a que desea eliminar este archivo ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then(function () {
            $("#loading-animation-container").show();

            var request = $.ajax({
                url: ROOT_PATH + "/Seguros/EliminarArchivo",
                method: "POST",
                data: {
                    ArchivoId: archivoId
                },
                dataType: "json"
            });

            request.done(function (Respuesta) {
                $("#loading-animation-container").hide();

                swal(
                    '',
                    '¡ El archivo se eliminó con éxito !',
                    'success'
                ).then(function () {
                    $("#loading-animation-container").show();

                    window.location.reload();
                });
            });

            request.fail(function (jqXHR, textStatus) {
                console.log("Falló eliminar archivo: " + textStatus);
            });
        });
    }
}