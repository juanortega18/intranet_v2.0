﻿var link = $("masterlink").text();
var dependencias = $("#DependenciaId");
var departamentos = $("#DepartamentoId");
var cargos = $("#Cargoid");

$(document).ready(function () {
    
    $("#Cedula").mask('999-9999999-9');
    $("#Telefono1").mask('(999) 999-9999');
});

$("#Cargoid,#DependenciaId,#DepartamentoId").chosen();

$('.date-picker').datepicker({
    dateFormat: 'dd/mm/yy'
});

$("#Salario").val(String($("#Salario").val()).replace(",", "."));

$('#btnGuardar').click(function (e) {
    console.log($("#Salario"));
    var str = $("#Salario").maskMoney('unmasked')[0];
    console.log(String(str).replace(".", ","));
    $("#Salario").val(String($("#Salario").maskMoney('unmasked')[0]).replace(".", ","));
});

$('#boton-validar').click(function () {

    if ($("#Cedula").val() != "" && $("#Cedula").val() != null) {

        AnimacionCarga.show();

        $.ajax({
            //url: "../Custodio/ValidarCedula", EmpleadosContratados/Edit/ValidarCedula 
            url: link + "/EmpleadosContratados/ValidarCedula",
            method: "post",
            data: { cedula: $('#Cedula').val() },
            success: function (data) {

                if (data.Respuesta == "OK") {
                    $('#DisplayNombre,#Nombre').val(data.Nombres + " " + data.Apellido1 + " " + data.Apellido2).attr("readonly", true);
                    $('#CedulaValidada').val($('#Cedula').val());
                    console.log(data);
                } else {
                    swal("", data, "error");
                }
                AnimacionCarga.hide();
            },
            error: function (data) { console.log(data); AnimacionCarga.hide(); }

        });

    } else {
        swal("Por favor ingrese la cédula");
    }
});

$("#formModal").submit(function (e) {
    if ($('#Cedula').val() != $('#CedulaValidada').val()) {
        e.preventDefault();
        swal("", "Favor de validar cécula", "warning");
    }
    if ($("#DepartamentoId").val() == "" || $("#DepartamentoId").val() == null) {
        e.preventDefault();
        swal("", "Favor de selecionar un departamento válido", "warning");
        return;
    }
    if ($("#Cargoid").val() == "" || $("#Cargoid").val() == null) {
        e.preventDefault();
        swal("", "Favor de selecionar un cargo válido", "warning");
    }
});

dependencias.on("change", function () {

    if (dependencias.val() != "") {

        cargarDepartamentos(dependencias.val());
    }
});

departamentos.on("change", function () {

    console.log(dependencias.val() + " " + departamentos.val());

    if (departamentos.val() != "") {

        cargarCargos(dependencias.val(), departamentos.val());

    }
});

function cargarDepartamentos(dependencia, departamento) {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/VISTA_EMPLEADOS/GetDepartamentos",
        method: 'POST',
        data: { dependencia: dependencia },
        dataType: 'json',
        success: function (ListaDepartamentos, status, xhr) {

            if (ListaDepartamentos.length > 0) {
                $('#DepartamentoId').html("");
                //$('#departamentos').append('<option value>Seleccione el Dependencia</option>');
                $.each(ListaDepartamentos, function (i, departamento) {
                    $('#DepartamentoId').append('<option value="' + departamento.Value + '">' + departamento.Text + '</option>');
                    //console.log((departamento.Text.match("^ ")) ? departamento.Text.substring(1) : departamento.Text);
                    //console.log(departamento.Text);
                    //$('#DepartamentoId').append('<option value="' + departamento.Value + '">' + (departamento.Text.match("^ ")) ? departamento.Text.substring(1) : departamento.Text + '</option>');
                });
                $('#DepartamentoId').trigger('chosen:updated');
                if (departamento != undefined) {
                    $("#DepartamentoId").val(departamento);
                    cargarCargos(dependencias.val(), departamento);
                    $('#DepartamentoId,#Cargoid').trigger("chosen:updated");
                } else {
                    cargarCargos(dependencias.val(), departamentos.val());
                }
                AnimacionCarga.hide();

            } else {

                $('#DepartamentoId,#Cargoid').html("").trigger('chosen:updated');
                AnimacionCarga.hide();
            }

        },
        error: function (jqXhr, textStatus, errorMessage) {
            swal('Un Error a ocurrido', '', 'error');
            AnimacionCarga.hide();

        }
    });
}

function cargarCargos(dependencia, departamento, cargo) {

    AnimacionCarga.show();

    $.ajax({
        url: link + "/VISTA_EMPLEADOS/GetCargos",
        method: 'POST',
        data: { dependencia: dependencia, departamento: departamento, todos: false },
        dataType: 'json',
        success: function (ListaCargos, status, xhr) {

            cargos.html("");
            $.each(ListaCargos, function (i, cargo) {
                cargos.append('<option value="' + cargo.Value + '">' + cargo.Text + '</option>');
            });
            cargos.trigger('chosen:updated');
            console.log(cargo);
            if (cargo != undefined) {
                $("#Cargoid").val(cargo);
                $("#Cargoid").trigger("chosen:updated");
            }
            AnimacionCarga.hide();

        },
        error: function (jqXhr, textStatus, errorMessage) {
            swal('Un Error a ocurrido', '', 'error');
            AnimacionCarga.hide();

        }
    });
}

$("#Salario").maskMoney({ prefix: 'RD$ ', thousands: ',', decimal: '.' });

//document.getElementById('Telefono1').addEventListener('blur', function (e) {
//    var x = e.target.value.replace(/\D/g, '').match(/(\d{3})(\d{3})(\d{4})/);
//    e.target.value = '(' + x[1] + ') ' + x[2] + '-' + x[3];
//});

//document.getElementById('Cedula').addEventListener('blur', function (e) {
//    var x = e.target.value.replace(/\D/g, '').match(/(\d{3})(\d{7})(\d{1})/);
//    e.target.value = x[1] + '-' + x[2] + '-' + x[3];
//});