﻿$(function () {
    $('#dropArea').filedrop({
        url: '@Url.Action("UploadedFiles")',
        allowedfiletypes: ['image/jpeg', 'image/png', 'image/gif'],
        allowedflieextension: ['.jpg', '.jpeg', '.png', '.gif'],
        paramname: 'files',
        dragOver: function () {
            $('#dropArea').addClass('active-drop');
        },
        dragLeave: function () {
            $('#dropArea').removeClass('active-drop');
        },
        drop: function () {
            $('#dropArea').removeClass('active-drop');
        },
        afterAll: function (e) {
            $('#dropArea').html("Archivo(s) cargados satisfactoriamente.")
        },
        uploadFinished: function(i,file,response,time){
            $('#uploadList').append('<li class="list-group item">' +
                file.name + '</li>')
        }
    })
})