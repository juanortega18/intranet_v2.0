﻿class Localizacion {

    constructor(callback) {
        if (navigator.geolocation) {
            
        } else {
            alert("Su navegador no soporta geolocalización");
        }

        callback();
    }
}


google.maps.event.addDomListener(window, "load", function () {

    const ubicacion = new Localizacion(() => {

        this.myLatitude = -1;
        this.myLongitude = -1;

        if (document.querySelector("#INCA_LONGITUD").value != "" && document.querySelector("#INCA_LATITUD").value != "")
        {
            this.myLongitude = +document.querySelector("#INCA_LONGITUD").value;
            this.myLatitude = +document.querySelector("#INCA_LATITUD").value;
        }
       

        var myOptions = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };

        function success(pos) {
            var crd = pos.coords;
            
            if (this.myLatitude === -1 && this.myLongitude === -1) {
                this.myLatitude = crd.latitude;
                this.myLongitude = crd.longitude;
            }
            const myLatLng = { lat: this.myLatitude, lng: this.myLongitude };

            const options = {
                center: myLatLng,
                zoom: 14
            }

            var map = document.getElementById('map');

            const mapa = new google.maps.Map(map, options);

            const marcador = new google.maps.Marker({
                position: myLatLng,
                map: mapa
            });

            var informacion = new google.maps.InfoWindow();

            marcador.addListener('click', function () {
                informacion.open(mapa, marcador);
            });

            var autocomplete = document.getElementById('INCA_DIRECCION');

            const search = new google.maps.places.Autocomplete(autocomplete);

            search.bindTo("bounds", mapa);
            
            setTimeout(function () {
                document.getElementById('INCA_DIRECCION').value = document.getElementById('MiDireccion').value
            }, 500);


            search.addListener('place_changed', function () {

                informacion.close();
                marcador.setVisible(false);

                var place = search.getPlace();

                if(!place.geometry.viewport) {
                    window.alert("Error al mostrar el lugar.");
                    return;
                }

                if(place.geometry.viewport) {
                    mapa.fitBounds(place.geometry.viewport);
                } else {
                    mapa.setCenter(place.geometry.location);
                    mapa.setZoom(18);
                }

                marcador.setPosition(place.geometry.location);
                marcador.setVisible(true);

                var Latitud = place.geometry.location.lat();
                var Longitud = place.geometry.location.lng();

                document.querySelector("#INCA_LATITUD").value = Latitud;
                document.querySelector("#INCA_LONGITUD").value = Longitud;

            });
        };

        function error(err) {
            console.warn('ERROR(' + err.code + '): ' + err.message);
        };

        navigator.geolocation.getCurrentPosition(success, error, myOptions);
    });
});