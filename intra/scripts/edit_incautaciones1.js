﻿function load_tipos() {
    $("#TIPO_ID").empty();
    $.ajax({
        type: 'POST',
        url: '@Url.Action("dbTipos")',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: '{ clase: ' + $("#CLAS_ID").val() + ' }',
        success: function (states) {
            $.each(states, function (i, state) {
                $("#TIPO_ID").append('<option value="' + state.Value + '">' +
                 state.Text + '</option>');
            });
        },
        error: function (ex) {
            alert('No pudo cargar los tipos: ' + ex);
        }
    });

    return;
}

$(document).ready(function () {

    if ($('#CLAS_ID').val() == "2") {
        $('#INCA_MODELO_DIV').show();
        $('#INCA_MARCA_DIV').show();
        $('#INCA_ANO_DIV').show();
        $('#INCA_CHASIS_DIV').show();
        $('#INCA_COLOR_DIV').show();
    } else {
        $('#INCA_MODELO_DIV').hide();
        $('#INCA_MARCA_DIV').hide();
        $('#INCA_ANO_DIV').hide();
        $('#INCA_CHASIS_DIV').hide();
        $('#INCA_COLOR_DIV').hide();
    }


    $("#CLAS_ID").change(function () {

        load_tipos();

        if ($('#CLAS_ID').val() == "2") {
            $('#INCA_MODELO_DIV').show();
            $('#INCA_MARCA_DIV').show();
            $('#INCA_ANO_DIV').show();
            $('#INCA_CHASIS_DIV').show();
            $('#INCA_COLOR_DIV').show();
        } else {
            $('#INCA_MODELO_DIV').hide();
            $('#INCA_MARCA_DIV').hide();
            $('#INCA_ANO_DIV').hide();
            $('#INCA_CHASIS_DIV').hide();
            $('#INCA_COLOR_DIV').hide();
        }

        return false;
    })

    $('#INCA_FHENTRADA').datepicker({ dateFormat: 'dd/mm/yy' });
});